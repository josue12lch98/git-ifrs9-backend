package sim.persistencia;

public class AjustePdProyec {
	private Integer fecha_proyeccion;
	private Double pd_calculado;
	private Double pd_final;
	private Boolean flag_ajustado;
	
	public AjustePdProyec() {}

	public Integer getFecha_proyeccion() {
		return fecha_proyeccion;
	}

	public void setFecha_proyeccion(Integer fecha_proyeccion) {
		this.fecha_proyeccion = fecha_proyeccion;
	}

	public Double getPd_calculado() {
		return pd_calculado;
	}

	public void setPd_calculado(Double pd_calculado) {
		this.pd_calculado = pd_calculado;
	}

	public Boolean getFlag_ajustado() {
		return flag_ajustado;
	}

	public void setFlag_ajustado(Boolean flag_ajustado) {
		this.flag_ajustado = flag_ajustado;
	}

	public Double getPd_final() {
		return pd_final;
	}

	public void setPd_final(Double pd_final) {
		this.pd_final = pd_final;
	}

}
