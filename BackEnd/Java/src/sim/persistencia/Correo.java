package sim.persistencia;

import java.util.ArrayList;

import sim.persistencia.dic.TipoAmbito;

public class Correo {
	private Integer id_correo;
	private String nombre_correo;
	private TipoAmbito tipo_ambito;
	private ArrayList<Notificacion> notificaciones;

	public Correo() {
		this.tipo_ambito = new TipoAmbito();
		this.notificaciones = new ArrayList<Notificacion>();
	}

	public Integer getId_correo() {
		return id_correo;
	}

	public void setId_correo(Integer id_correo) {
		this.id_correo = id_correo;
	}

	public String getNombre_correo() {
		return nombre_correo;
	}

	public void setNombre_correo(String nombre_correo) {
		this.nombre_correo = nombre_correo;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}

	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}

	public ArrayList<Notificacion> getNotificaciones() {
		return notificaciones;
	}

	public void setNotificaciones(ArrayList<Notificacion> notificaciones) {
		this.notificaciones = notificaciones;
	}

}
