package sim.persistencia;

public class FicheroInput {
	private Integer id_fichero;
	private String nombre_fichero;
	
	public FicheroInput() {		
	}

	public Integer getId_fichero() {
		return id_fichero;
	}
	public void setId_fichero(Integer id_fichero) {
		this.id_fichero = id_fichero;
	}
	public String getNombre_fichero() {
		return nombre_fichero;
	}
	public void setNombre_fichero(String nombre_fichero) {
		this.nombre_fichero = nombre_fichero;
	}
}
