package sim.notificacion;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import sim.controlador.Controlador;

public class ModuloCorreo {
	
	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(Controlador.class);
	private static final String smtp_auth = "true";
	private static final String starttls = "true";
	private static String host;
	private static String port;
	private static String username;
	private static String encriptado;
	private static Properties props;
	private static Session session;
	
	public ModuloCorreo(Properties properties) {
		LOGGER.info("Creando ModuloCorreo");
		//se capturan las properties
		host = properties.getProperty("SMTP_HOST");
		port = properties.getProperty("SMTP_PORT");
		username = properties.getProperty("SMTP_EMAIL");
		encriptado = properties.getProperty("SMTP_ENCRIPTADO");
		
		props = new Properties();
		props.put("mail.smtp.auth", smtp_auth);
		props.put("mail.smtp.starttls.enable", starttls);
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.ssl.trust", host);
		
		session = Session.getInstance(props,
				  new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, encriptado);
					}
				  }); 
		
		LOGGER.info("ModuloCorreo creado");
	}
	
	public void pruebaSMTP() {
		LOGGER.info("entra a pruebaSMTP");

		try {

			Message msg = new MimeMessage(session);
			
			String to = "hchumpisuca@bcp.com.pe";
            InternetAddress[] address = InternetAddress.parse(to, true);
            //Setting the recepients from the address variable
            msg.setRecipients(Message.RecipientType.TO, address);
            String timeStamp = new SimpleDateFormat("yyyymmdd_hh-mm-ss").format(new Date());
            msg.setSubject("Notificacion NIIF9");
            msg.setSentDate(new Date());
			msg.setText("Este es un correo de prueba de notificacion: " + timeStamp);
            msg.setHeader("XPriority", "1");
            LOGGER.info("establece propiedades del mensaje");
            
            Transport.send(msg);

			LOGGER.info("envia el mensaje");

		} catch (MessagingException e) {
			LOGGER.error("Error al enviar el correo: " + e.getMessage());
		}
	}
	
	public void enviarCorreo(String destinatario, String mensaje) {
		LOGGER.info("Enviando correo de notificacion a:" + destinatario);
		try {
			Message msg = new MimeMessage(session);
			
            InternetAddress[] address = InternetAddress.parse(destinatario, true);
            //Setting the recepients from the address variable
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject("Notificacion NIIF9");
            msg.setSentDate(new Date());
			msg.setText("Este es un correo de notificación de la solución NIIF9: \n\r" 
					+ mensaje
					+ " \n\r\n\r" 
					+ "------------------------------------------------------------ \n\r"
					+ "Buzón de notificación automático de NIIF9, no responder a este correo.");
            msg.setHeader("XPriority", "1");
            
            Transport.send(msg);

		} catch (MessagingException e) {
			LOGGER.error("Error al enviar el correo: " + e.getMessage());
		}
	}
}