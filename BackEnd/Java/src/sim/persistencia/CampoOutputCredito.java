package sim.persistencia;

public class CampoOutputCredito {
	private String nombre_campo;
	private Integer tipo_dato; //0: numerico, 1: texto
	private Boolean visualizar;
	private Boolean filtrar;
	private String valor_filtro;
	
	public CampoOutputCredito() {		
	}

	public String getNombre_campo() {
		return nombre_campo;
	}

	public void setNombre_campo(String nombre_campo) {
		this.nombre_campo = nombre_campo;
	}

	public Integer getTipo_dato() {
		return tipo_dato;
	}

	public void setTipo_dato(Integer tipo_dato) {
		this.tipo_dato = tipo_dato;
	}

	public String getValor_filtro() {
		return valor_filtro;
	}

	public void setValor_filtro(String valor_filtro) {
		this.valor_filtro = valor_filtro;
	}

	public Boolean getVisualizar() {
		return visualizar;
	}

	public void setVisualizar(Boolean visualizar) {
		this.visualizar = visualizar;
	}

	public Boolean getFiltrar() {
		return filtrar;
	}

	public void setFiltrar(Boolean filtrar) {
		this.filtrar = filtrar;
	}
}
