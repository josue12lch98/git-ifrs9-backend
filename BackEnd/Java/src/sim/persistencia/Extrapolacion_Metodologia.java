package sim.persistencia;
import java.util.ArrayList;

public class Extrapolacion_Metodologia extends EstructuraMetodologica{
	private ArrayList<Integer> metodologias_extrapolacion;

	public ArrayList<Integer> getMetodologias_extrapolacion() {
		return metodologias_extrapolacion;
	}

	public void setMetodologias_extrapolacion(ArrayList<Integer> metodologias_extrapolacion) {
		this.metodologias_extrapolacion = metodologias_extrapolacion;
	}
	
}
