package sim.persistencia;
import java.sql.Date;

import sim.persistencia.dic.TipoAmbito;


public class Fecha {

	private Integer id_fecha;
	private Integer codmes;
	private Date fechaInicio;
	private Date fechaFin;
	private Integer fase;
	private TipoAmbito tipo_ambito;
	
	public Fecha() {		
	}	

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Integer getFase() {
		return fase;
	}

	public void setFase(Integer fase) {
		this.fase = fase;
	}

	public Integer getId_fecha() {
		return id_fecha;
	}

	public void setId_fecha(Integer id_fecha) {
		this.id_fecha = id_fecha;
	}

	public Integer getCodmes() {
		return codmes;
	}

	public void setCodmes(Integer codmes) {
		this.codmes = codmes;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}

	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}

}
