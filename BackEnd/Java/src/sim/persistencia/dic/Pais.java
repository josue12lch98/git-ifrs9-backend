package sim.persistencia.dic;

public class Pais {
	private Integer id_pais;
	private String nombre_pais;
	
	public Pais() {
	}
	
	public Integer getId_pais() {
		return id_pais;
	}
	public void setId_pais(Integer id_pais) {
		this.id_pais = id_pais;
	}
	public String getNombre_pais() {
		return nombre_pais;
	}
	public void setNombre_pais(String nombre_pais) {
		this.nombre_pais = nombre_pais;
	}


}
