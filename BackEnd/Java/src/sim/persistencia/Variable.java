package sim.persistencia;

public class Variable {

	private Cartera cartera;
	
	private Integer id_variable_calculada;
	private String nombre_variable;
	private String variable_sas;
	
	public Variable() {
	}
	
	public Cartera getCartera() {
		return cartera;
	}
	public void setCartera(Cartera cartera) {
		this.cartera = cartera;
	}
	public Integer getId_variable_calculada() {
		return id_variable_calculada;
	}
	public void setId_variable_calculada(Integer id_variable_calculada) {
		this.id_variable_calculada = id_variable_calculada;
	}
	public String getNombre_variable() {
		return nombre_variable;
	}
	public void setNombre_variable(String nombre_variable) {
		this.nombre_variable = nombre_variable;
	}
	public String getVariable_sas() {
		return variable_sas;
	}
	public void setVariable_sas(String variable_sas) {
		this.variable_sas = variable_sas;
	}
	
}
