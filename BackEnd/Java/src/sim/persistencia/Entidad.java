
package sim.persistencia;

import sim.persistencia.dic.Pais;
import sim.persistencia.dic.TipoAmbito;

public class Entidad {
	
	private Integer id_entidad;
	private final Integer id_matriz = 1; //Es unica que indica el grupo corporativo
	private String nombre_entidad;
	private String nombre_variablemotor;
	private Pais pais;
	private TipoAmbito tipo_ambito;

	public Entidad() {
	}

	public Integer getId_entidad() {
		return id_entidad;
	}

	public void setId_entidad(Integer id_entidad) {
		this.id_entidad = id_entidad;
	}

	public String getNombre_entidad() {
		return nombre_entidad;
	}

	public void setNombre_entidad(String nombre_entidad) {
		this.nombre_entidad = nombre_entidad;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public Integer getId_matriz() {
		return id_matriz;
	}

	public String getNombre_variablemotor() {
		return nombre_variablemotor;
	}

	public void setNombre_variablemotor(String nombre_variablemotor) {
		this.nombre_variablemotor = nombre_variablemotor;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}

	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}
	
}
