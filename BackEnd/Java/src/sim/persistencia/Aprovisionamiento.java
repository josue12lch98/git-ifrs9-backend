package sim.persistencia;

import java.util.ArrayList;

import sim.persistencia.dic.TipoAmbito;

public class Aprovisionamiento {
	private Integer id_aprovisionamiento;
	private TipoAmbito tipo_ambito;
	private String separador;
	private String nombre_fichero;
	private String nombre_archivo;
	private Boolean flag_obligatorio;
	private Boolean flag_operacion;
	private Boolean flag_fechaautomatica;
	private ArrayList<CampoAprovisionamiento> campos_aprovisionamiento;// Campos del aprovisionamiento
	private ArrayList<CampoFichero> campos_fichero; // Campos necesarios al momento de leer los archivos
	private ArrayList<Cartera> carteras; // Carteras vinculadas por mapeo a los aprovisioamientos
	private ArrayList<SegmentoAprovisionamiento> segmentos; // Indica los segmentos de la tabla para aplicar reglas de

	private String ruta;
	// calidad

	// Backend
	private boolean flag_tiene_aprov;// Indica si el fichero ha sido aprovisionado anteriormente, solo se usa para el
										// backend
	private boolean flag_campos_difer;// Indica si el fichero tiene campos diferentes al ultimo aprovisionamiento,
										// solo se usa para el backend

	// FrontEnd Identificar Ficheros: Observacion
	private boolean flag_warn_definicion; // Indica si se debe definir los campos del archivo
	private boolean flag_error_separador; // Indica si el archivo presenta error porque no tiene el separador
											// configurado
	private boolean flag_error_cabeceras; // Indica si el archivo presenta error porque las cabeceras tienen caracteres
											// indebidos (solo letras del alfabeto y numeros)

	// FrontEnd Procesar Ficheros: Observacion
	private boolean flag_error_inconsistencia; // Indica si el archivo presenta error porque los numeros de campos no
												// son los mismos en todas las lineas

	// FrontEnd Iconos
	private boolean flag_correcto; // Indica si el archivo no tiene ni errores ni warnings
	private boolean flag_warning; // Indica si el archivo tiene warnings
	private boolean flag_error; // Indica si el archivo tiene errores
	private String observacion;

	// FrontEnd Nuevos Campos
	private Boolean flag_nuevos_campos; // Indica si el aprovisionamiento genera nuevos campos en una tabla input

	// BackEnd Reglas Calidad
	private Boolean flag_segmentos_distintos; // Indica si el aprovisionamiento tiene segmentos distintos al ultimo
												// aprov
	// FrontEnd Reglas Calidad
	private Boolean flag_aplican_reglas; // Indica si el aprovisionamiento aplican reglas de calidad

	// FrontEnd y BackEnd Reportes Calidad
	private Boolean flag_conforme; // Indica si se esta conforme con el aprovisionamiento del fichero

	// FrontEnd Tabla Final
	private Boolean flag_definir_tabla; // Indica si el aprovisionamiento requiere definir la tabla input final
	private Integer ultimo_orden; // Indica el ultimo orden del registro de la tabla, sin importar fecha

	// Comparacion de Arrays
	private Boolean flag_metadata_existente;
	
	// cambios en los campos llave y segmentos al momento de hacer un update
	private Boolean reconstruir_segmentos_calidad;
	
	// Constructor
	public Aprovisionamiento() {
		super();
		tipo_ambito = new TipoAmbito();
		campos_aprovisionamiento = new ArrayList<CampoAprovisionamiento>();
	}

	public void setFlag_error_inconsistencia(boolean flag_error_inconsistencia) {
		this.flag_error_inconsistencia = flag_error_inconsistencia;
		this.flag_error = flag_error_inconsistencia;
	}

	public void setFlag_error_separador(boolean flag_error_separador) {
		this.flag_error_separador = flag_error_separador;
		this.flag_error = flag_error_separador;
	}

	public void setFlag_warn_definicion(boolean flag_warn_definicion) {
		this.flag_warn_definicion = flag_warn_definicion;
		this.flag_warning = flag_warn_definicion;
	}

	public String getNombre_archivo() {
		return nombre_archivo;
	}

	public void setNombre_archivo(String nombre_archivo) {
		this.nombre_archivo = nombre_archivo;
	}

	public void setFlag_error_cabeceras(boolean flag_error_cabeceras) {
		this.flag_error_cabeceras = flag_error_cabeceras;
		this.flag_error = flag_error_cabeceras;
	}

	// Getters y Setters
	public Integer getId_aprovisionamiento() {
		return id_aprovisionamiento;
	}

	public void setId_aprovisionamiento(Integer id_aprovisionamiento) {
		this.id_aprovisionamiento = id_aprovisionamiento;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}

	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}

	public String getSeparador() {
		return separador;
	}

	public void setSeparador(String separador) {
		this.separador = separador;
	}

	public void agregarCampoAprov(CampoAprovisionamiento CA) {
		this.campos_aprovisionamiento.add(CA);
	}

	// Obtener solamente la lista de nombres de segmentos
	public ArrayList<String> obtenerNombresSegmentos() throws Exception {
		ArrayList<String> nombres = new ArrayList<String>();
		for (SegmentoAprovisionamiento segmento : this.segmentos) {
			nombres.add(segmento.getDescripcion());
		}
		return nombres;
	}

	public ArrayList<SegmentoAprovisionamiento> getSegmentos() {
		return segmentos;
	}

	public void setSegmentos(ArrayList<SegmentoAprovisionamiento> segmentos) {
		this.segmentos = segmentos;
	}

	public ArrayList<CampoFichero> getCampos_fichero() {
		return campos_fichero;
	}

	public void setCampos_fichero(ArrayList<CampoFichero> campos_fichero) {
		this.campos_fichero = campos_fichero;
	}

	// Obtener solamente la lista de nombres de campos aprovisionamiento
	public ArrayList<String> obtenerNombresCamposAprov() throws Exception {
		ArrayList<String> nombres = new ArrayList<String>();
		for (CampoAprovisionamiento campo : this.campos_aprovisionamiento) {
			nombres.add(campo.getNombre_campo());
		}
		return nombres;
	}

	public ArrayList<String> obtenerNombresCamposAprov(Boolean solo_seleccionados, Boolean solo_original)
			throws Exception {
		ArrayList<String> nombres = new ArrayList<String>();
		for (CampoAprovisionamiento campo : this.campos_aprovisionamiento) {
			if (solo_seleccionados) {
				if (campo.getFlag_aprovisionar()) {
					if (solo_original) {
						if (campo.getFlag_campo_original()) {
							nombres.add(campo.getNombre_campo());
						}
					} else {
						nombres.add(campo.getNombre_campo());
					}
				}
			} else {
				if (solo_original) {
					if (campo.getFlag_campo_original()) {
						nombres.add(campo.getNombre_campo());
					}
				} else {
					nombres.add(campo.getNombre_campo());
				}
			}
		}
		return nombres;
	}

	// Obtener solamente la lista de nombres de campos aprovisionamiento con tipo
	public ArrayList<String> obtenerNombresCamposAprovconTipo() throws Exception {
		ArrayList<String> nombres = new ArrayList<String>();
		for (CampoAprovisionamiento campo : this.campos_aprovisionamiento) {
			if (campo.getFlag_aprovisionar()) {
				nombres.add(campo.getNombre_campo() + campo.getTipo_campo().getId_tipo_campo());
			}
		}
		return nombres;
	}

	// Obtener solamente la lista de nombres de campos
	public ArrayList<String> obtenerNombresCamposFichero() throws Exception {
		ArrayList<String> nombres = new ArrayList<String>();
		for (CampoFichero campo : this.campos_fichero) {
			nombres.add(campo.getNombre_campo());
		}
		return nombres;
	}

	public ArrayList<CampoAprovisionamiento> getCampos_aprovisionamiento() {
		return campos_aprovisionamiento;
	}

	public ArrayList<CampoAprovisionamiento> getCampos_aprovisionamiento(Boolean solo_original) {
		ArrayList<CampoAprovisionamiento> lista = new ArrayList<CampoAprovisionamiento>();
		for (CampoAprovisionamiento campo : campos_aprovisionamiento) {	
			if (solo_original) {
				if (campo.getFlag_campo_original()) {
					lista.add(campo);
				}
			} else {
				lista.add(campo);
			}
		}
		return lista;
	}

	public void setCampos_aprovisionamiento(ArrayList<CampoAprovisionamiento> campos_aprovisionamiento) {
		this.campos_aprovisionamiento = campos_aprovisionamiento;
	}

	public String getNombre_fichero() {
		return nombre_fichero;
	}

	public void setNombre_fichero(String nomre_fichero) {
		this.nombre_fichero = nomre_fichero;
	}

	public Boolean getFlag_obligatorio() {
		return flag_obligatorio;
	}

	public void setFlag_obligatorio(Boolean flag_obligatorio) {
		this.flag_obligatorio = flag_obligatorio;
	}

	public Boolean getFlag_fechaautomatica() {
		return flag_fechaautomatica;
	}

	public void setFlag_fechaautomatica(Boolean flag_fechaautomatica) {
		this.flag_fechaautomatica = flag_fechaautomatica;
	}

	public boolean isFlag_tiene_aprov() {
		return flag_tiene_aprov;
	}

	public void setFlag_tiene_aprov(boolean flag_tiene_aprov) {
		this.flag_tiene_aprov = flag_tiene_aprov;
	}

	public boolean isFlag_campos_difer() {
		return flag_campos_difer;
	}

	public void setFlag_campos_difer(boolean flag_campos_difer) {
		this.flag_campos_difer = flag_campos_difer;
	}

	public boolean isFlag_correcto() {
		return flag_correcto;
	}

	public void setFlag_correcto(boolean flag_correcto) {
		this.flag_correcto = flag_correcto;
	}

	public boolean isFlag_warning() {
		return flag_warning;
	}

	public void setFlag_warning(boolean flag_warning) {
		this.flag_warning = flag_warning;
	}

	public boolean isFlag_error() {
		return flag_error;
	}

	public void setFlag_error(boolean flag_error) {
		this.flag_error = flag_error;
	}

	public Boolean getFlag_nuevos_campos() {
		return flag_nuevos_campos;
	}

	public void setFlag_nuevos_campos(Boolean flag_nuevos_campos) {
		this.flag_nuevos_campos = flag_nuevos_campos;
	}

	public Boolean getFlag_segmentos_distintos() {
		return flag_segmentos_distintos;
	}

	public void setFlag_segmentos_distintos(Boolean flag_segmentos_distintos) {
		this.flag_segmentos_distintos = flag_segmentos_distintos;
	}

	public Boolean getFlag_aplican_reglas() {
		return flag_aplican_reglas;
	}

	public void setFlag_aplican_reglas(Boolean flag_aplican_reglas) {
		this.flag_aplican_reglas = flag_aplican_reglas;
	}

	public Boolean getFlag_conforme() {
		return flag_conforme;
	}

	public void setFlag_conforme(Boolean flag_conforme) {
		this.flag_conforme = flag_conforme;
	}

	public Boolean getFlag_definir_tabla() {
		return flag_definir_tabla;
	}

	public void setFlag_definir_tabla(Boolean flag_definir_tabla) {
		this.flag_definir_tabla = flag_definir_tabla;
	}

	public Integer getUltimo_orden() {
		return ultimo_orden;
	}

	public void setUltimo_orden(Integer ultimo_orden) {
		this.ultimo_orden = ultimo_orden;
	}

	public Boolean getFlag_metadata_existente() {
		return flag_metadata_existente;
	}

	public void setFlag_metadata_existente(Boolean flag_metadata_existente) {
		this.flag_metadata_existente = flag_metadata_existente;
	}

	public boolean isFlag_warn_definicion() {
		return flag_warn_definicion;
	}

	public boolean isFlag_error_separador() {
		return flag_error_separador;
	}

	public boolean isFlag_error_cabeceras() {
		return flag_error_cabeceras;
	}

	public boolean isFlag_error_inconsistencia() {
		return flag_error_inconsistencia;
	}

	public ArrayList<Cartera> getCarteras() {
		return carteras;
	}

	public void setCarteras(ArrayList<Cartera> carteras) {
		this.carteras = carteras;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public Boolean getFlag_operacion() {
		return flag_operacion;
	}

	public void setFlag_operacion(Boolean flag_operacion) {
		this.flag_operacion = flag_operacion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Boolean getReconstruir_segmentos_calidad() {
		return reconstruir_segmentos_calidad;
	}

	public void setReconstruir_segmentos_calidad(Boolean reconstruir_segmentos_calidad) {
		this.reconstruir_segmentos_calidad = reconstruir_segmentos_calidad;
	}



}