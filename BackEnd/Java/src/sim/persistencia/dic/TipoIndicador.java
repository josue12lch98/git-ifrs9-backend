package sim.persistencia.dic;

public class TipoIndicador {
	private Integer id_tipo_indicador;
	private String nombre_indicador;
	
	//Constructor
	public TipoIndicador() {
	}
	
	public TipoIndicador(Integer id_tipo) {
		this.id_tipo_indicador= id_tipo;
	}
	
	public TipoIndicador(Integer id_tipo_indicador, String nombre_indicador) {
		this.id_tipo_indicador = id_tipo_indicador;
		this.nombre_indicador = nombre_indicador;
	}

	//Getters y Setters
	public Integer getId_tipo_indicador() {
		return id_tipo_indicador;
	}
	public void setId_tipo_indicador(Integer id_tipo_indicador) {
		this.id_tipo_indicador = id_tipo_indicador;
	}
	
	public String getNombre_indicador() {
		return nombre_indicador;
	}
	public void setNombre_indicador(String nombre_indicador) {
		this.nombre_indicador = nombre_indicador;
	}

}
