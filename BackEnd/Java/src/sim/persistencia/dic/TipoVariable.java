package sim.persistencia.dic;


public class TipoVariable {
	private Integer id_tipo_variable;
	private String nombre_tipo_variable;
	
	public TipoVariable() {
	}
	
	public TipoVariable(Integer id_tipo_variable, String nombre_tipo_variable) {
		this.id_tipo_variable = id_tipo_variable;
		this.nombre_tipo_variable = nombre_tipo_variable;
	}
	public Integer getId_tipo_variable() {
		return id_tipo_variable;
	}
	public void setId_tipo_variable(Integer id_tipo_variable) {
		this.id_tipo_variable = id_tipo_variable;
	}
	public String getNombre_tipo_variable() {
		return nombre_tipo_variable;
	}
	public void setNombre_tipo_variable(String nombre_tipo_variable) {
		this.nombre_tipo_variable = nombre_tipo_variable;
	}	
}
