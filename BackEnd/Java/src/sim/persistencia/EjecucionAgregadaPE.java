package sim.persistencia;

public class EjecucionAgregadaPE {
	private Integer codmes; 
	private JuegoEscenarios_version juego_escenarios_version;
	private Integer estado_ejecucion;	
	private Integer porcentaje_avance;
	private String log_motor;
	private Long datetime_ejecucion;	
	private EjecucionPrimerPaso ejecucion_modMacro;	
	private EjecucionPrimerPaso ejecucion_repricing;
	private MapeoCargasModMacEjec mapeo_cargasModMac_ejecucion;
	
	private Integer tipo_ajusteManual; //-1: No requiere modelo macro
										//0: Asociado a resultado de mod macro
										//1: Solo ajuste manual (sin resultado)
										//2: Solo ajuste manual, doble (sin resultado)
	
	private Boolean validacionSolicitada; //unicamente para simulaciones
	
	private GrupoMapeosDelta grupo_mapeos_delta; //unicamente para fase 2
	
	//Se requiere para analizar consistencia
	private Integer estado_validacion;	
	
	//Solo se usa en simulaciones para inversiones
	private Integer fase;
	
	public EjecucionAgregadaPE() {		
	}
	
	public Integer getCodmes() {
		return codmes;
	}
	public void setCodmes(Integer codmes) {
		this.codmes = codmes;
	}
	public Long getDatetime_ejecucion() {
		return datetime_ejecucion;
	}
	public void setDatetime_ejecucion(Long datetime_ejecucion) {
		this.datetime_ejecucion = datetime_ejecucion;
	}
	public Integer getEstado_ejecucion() {
		return estado_ejecucion;
	}
	public void setEstado_ejecucion(Integer estado_ejecucion) {
		this.estado_ejecucion = estado_ejecucion;
	}
	
	public JuegoEscenarios_version getJuego_escenarios_version() {
		return juego_escenarios_version;
	}

	public void setJuego_escenarios_version(JuegoEscenarios_version juego_escenarios_version) {
		this.juego_escenarios_version = juego_escenarios_version;
	}

	public Boolean getValidacionSolicitada() {
		return validacionSolicitada;
	}

	public void setValidacionSolicitada(Boolean validacionSolicitada) {
		this.validacionSolicitada = validacionSolicitada;
	}

	public GrupoMapeosDelta getGrupo_mapeos_delta() {
		return grupo_mapeos_delta;
	}

	public void setGrupo_mapeos_delta(GrupoMapeosDelta grupo_mapeos_delta) {
		this.grupo_mapeos_delta = grupo_mapeos_delta;
	}

	public MapeoCargasModMacEjec getMapeo_cargasModMac_ejecucion() {
		return mapeo_cargasModMac_ejecucion;
	}

	public void setMapeo_cargasModMac_ejecucion(MapeoCargasModMacEjec mapeo_cargasModMac_ejecucion) {
		this.mapeo_cargasModMac_ejecucion = mapeo_cargasModMac_ejecucion;
	}

	public EjecucionPrimerPaso getEjecucion_modMacro() {
		return ejecucion_modMacro;
	}

	public void setEjecucion_modMacro(EjecucionPrimerPaso ejecucion_modMacro) {
		this.ejecucion_modMacro = ejecucion_modMacro;
	}

	public Integer getPorcentaje_avance() {
		return porcentaje_avance;
	}

	public void setPorcentaje_avance(Integer porcentaje_avance) {
		this.porcentaje_avance = porcentaje_avance;
	}

	public EjecucionPrimerPaso getEjecucion_repricing() {
		return ejecucion_repricing;
	}

	public void setEjecucion_repricing(EjecucionPrimerPaso ejecucion_repricing) {
		this.ejecucion_repricing = ejecucion_repricing;
	}

	public Integer getTipo_ajusteManual() {
		return tipo_ajusteManual;
	}

	public void setTipo_ajusteManual(Integer tipo_ajusteManual) {
		this.tipo_ajusteManual = tipo_ajusteManual;
	}

	public String getLog_motor() {
		return log_motor;
	}

	public void setLog_motor(String log_motor) {
		this.log_motor = log_motor;
	}

	public Integer getEstado_validacion() {
		return estado_validacion;
	}

	public void setEstado_validacion(Integer estado_validacion) {
		this.estado_validacion = estado_validacion;
	}

	public Integer getFase() {
		return fase;
	}

	public void setFase(Integer fase) {
		this.fase = fase;
	}		
}
