package sim.persistencia;

import sim.persistencia.dic.TipoAlerta;

public class Alerta {
	private String nombre_objeto;
	private TipoAlerta tipo_alerta;
	
	public String getNombre_objeto() {
		return nombre_objeto;
	}
	public void setNombre_objeto(String nombre_objeto) {
		this.nombre_objeto = nombre_objeto;
	}
	public TipoAlerta getTipo_alerta() {
		return tipo_alerta;
	}
	public void setTipo_alerta(TipoAlerta tipo_alerta) {
		this.tipo_alerta = tipo_alerta;
	}
}
