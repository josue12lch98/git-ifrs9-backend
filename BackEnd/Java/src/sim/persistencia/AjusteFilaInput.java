package sim.persistencia;

import java.util.ArrayList;

public class AjusteFilaInput {
	private String tabla;
	private Integer orden;
	private Integer id_cargaFichero;
	private ArrayList<AjusteCampoInput> ajustesCampoInput;

	public AjusteFilaInput() {
		this.ajustesCampoInput = new ArrayList<AjusteCampoInput>();
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public ArrayList<AjusteCampoInput> getAjustesCampoInput() {
		return ajustesCampoInput;
	}

	public void setAjustesCampoInput(ArrayList<AjusteCampoInput> ajustesCampoInput) {
		this.ajustesCampoInput = ajustesCampoInput;
	}

	public String getTabla() {
		return tabla;
	}

	public void setTabla(String tabla) {
		this.tabla = tabla;
	}

	public Integer getId_cargaFichero() {
		return id_cargaFichero;
	}

	public void setId_cargaFichero(Integer id_cargaFichero) {
		this.id_cargaFichero = id_cargaFichero;
	}

}
