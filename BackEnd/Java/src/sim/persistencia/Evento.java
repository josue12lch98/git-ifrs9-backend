package sim.persistencia;



import sim.persistencia.dic.TipoAccion;
import sim.persistencia.dic.TipoObjeto;

public class Evento {
	private Integer id_evento;
	private Long fecha_evento;
	private TipoObjeto tipo_objeto;
	private TipoAccion tipo_accion;
	private String nombre_objeto;
	private String objeto_json;
	private String actualizado_json;
	private String agregado_json;
	private String borrado_json;
	private String id_usuario;
	private String nombre_usuario;
	private Perfil perfil_usuario;
	
	//Adicional
	private String local_ip;
	
	public Evento() {		
	}
	
	public Integer getId_evento() {
		return id_evento;
	}
	public void setId_evento(Integer id_evento) {
		this.id_evento = id_evento;
	}
	
	public String getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getNombre_usuario() {
		return nombre_usuario;
	}

	public void setNombre_usuario(String nombre_usuario) {
		this.nombre_usuario = nombre_usuario;
	}

	public String getNombre_objeto() {
		return nombre_objeto;
	}

	public void setNombre_objeto(String nombre_objeto) {
		this.nombre_objeto = nombre_objeto;
	}

	public Long getFecha_evento() {
		return fecha_evento;
	}

	public void setFecha_evento(Long fecha_evento) {
		this.fecha_evento = fecha_evento;
	}

	public TipoObjeto getTipo_objeto() {
		return tipo_objeto;
	}

	public void setTipo_objeto(TipoObjeto tipo_objeto) {
		this.tipo_objeto = tipo_objeto;
	}

	public TipoAccion getTipo_accion() {
		return tipo_accion;
	}

	public void setTipo_accion(TipoAccion tipo_accion) {
		this.tipo_accion = tipo_accion;
	}

	public String getActualizado_json() {
		return actualizado_json;
	}

	public void setActualizado_json(String actualizado_json) {
		this.actualizado_json = actualizado_json;
	}

	public String getAgregado_json() {
		return agregado_json;
	}

	public void setAgregado_json(String agregado_json) {
		this.agregado_json = agregado_json;
	}

	public String getBorrado_json() {
		return borrado_json;
	}

	public void setBorrado_json(String borrado_json) {
		this.borrado_json = borrado_json;
	}

	public String getObjeto_json() {
		return objeto_json;
	}

	public void setObjeto_json(String objeto_json) {
		this.objeto_json = objeto_json;
	}

	public Perfil getPerfil_usuario() {
		return perfil_usuario;
	}

	public void setPerfil_usuario(Perfil perfil_usuario) {
		this.perfil_usuario = perfil_usuario;
	}

	public String getLocal_ip() {
		return local_ip;
	}

	public void setLocal_ip(String local_ip) {
		this.local_ip = local_ip;
	}	
}
