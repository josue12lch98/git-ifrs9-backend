package sim.persistencia;

public class FiltroProgreso {
	private Fecha fecha;
	private Integer id_tipo_ambito;


	public Integer getId_tipo_ambito() {
		return id_tipo_ambito;
	}

	public void setId_tipo_ambito(Integer id_tipo_ambito) {
		this.id_tipo_ambito = id_tipo_ambito;
	}

	public Fecha getFecha() {
		return fecha;
	}

	public void setFecha(Fecha fecha) {
		this.fecha = fecha;
	}

}
