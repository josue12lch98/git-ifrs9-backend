package sim.persistencia;

public class AjusteRegistro {
	private String descripcion_ajuste;	
	private String cod_operacion;
	private String cod_cliente;
	private Double valorPE_original;
	private Double valorPE_ajustado;
	private Integer valorStage_original;
	private Integer valorStage_ajustado;
	private Double valorPD_original;
	private Double valorPD_ajustado;
	
	public AjusteRegistro() {		
	}

	public String getDescripcion_ajuste() {
		return descripcion_ajuste;
	}

	public void setDescripcion_ajuste(String descripcion_ajuste) {
		this.descripcion_ajuste = descripcion_ajuste;
	}

	public String getCod_operacion() {
		return cod_operacion;
	}

	public void setCod_operacion(String cod_operacion) {
		this.cod_operacion = cod_operacion;
	}

	public String getCod_cliente() {
		return cod_cliente;
	}

	public void setCod_cliente(String cod_cliente) {
		this.cod_cliente = cod_cliente;
	}

	public Double getValorPE_original() {
		return valorPE_original;
	}

	public void setValorPE_original(Double valorPE_original) {
		this.valorPE_original = valorPE_original;
	}

	public Double getValorPE_ajustado() {
		return valorPE_ajustado;
	}

	public void setValorPE_ajustado(Double valorPE_ajustado) {
		this.valorPE_ajustado = valorPE_ajustado;
	}	

	public Double getValorPD_original() {
		return valorPD_original;
	}

	public void setValorPD_original(Double valorPD_original) {
		this.valorPD_original = valorPD_original;
	}

	public Double getValorPD_ajustado() {
		return valorPD_ajustado;
	}

	public void setValorPD_ajustado(Double valorPD_ajustado) {
		this.valorPD_ajustado = valorPD_ajustado;
	}

	public Integer getValorStage_original() {
		return valorStage_original;
	}

	public void setValorStage_original(Integer valorStage_original) {
		this.valorStage_original = valorStage_original;
	}

	public Integer getValorStage_ajustado() {
		return valorStage_ajustado;
	}

	public void setValorStage_ajustado(Integer valorStage_ajustado) {
		this.valorStage_ajustado = valorStage_ajustado;
	}
}
