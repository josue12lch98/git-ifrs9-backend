package sim.persistencia;

public class EjecucionVariacion {
	
	private Integer id_ejecucion;
	
	private EjecucionPE ejecucion;
	private Boolean flag_regulatorio; //Solo se activa cuando la ejec. actual es de fase2
	private EjecucionPE ejecucionAnt_modMac;
	
	private Integer estado_ejecucion;	
	private Integer porcentaje_avance;
	private Long datetime_ejecucion;		
	
	public EjecucionVariacion() {		
	}

	public Integer getEstado_ejecucion() {
		return estado_ejecucion;
	}

	public void setEstado_ejecucion(Integer estado_ejecucion) {
		this.estado_ejecucion = estado_ejecucion;
	}

	public Long getDatetime_ejecucion() {
		return datetime_ejecucion;
	}

	public void setDatetime_ejecucion(Long datetime_ejecucion) {
		this.datetime_ejecucion = datetime_ejecucion;
	}

	public Integer getPorcentaje_avance() {
		return porcentaje_avance;
	}

	public void setPorcentaje_avance(Integer porcentaje_avance) {
		this.porcentaje_avance = porcentaje_avance;
	}

	public Integer getId_ejecucion() {
		return id_ejecucion;
	}

	public void setId_ejecucion(Integer id_ejecucion) {
		this.id_ejecucion = id_ejecucion;
	}

	public EjecucionPE getEjecucion() {
		return ejecucion;
	}

	public void setEjecucion(EjecucionPE ejecucion) {
		this.ejecucion = ejecucion;
	}

	public EjecucionPE getEjecucionAnt_modMac() {
		return ejecucionAnt_modMac;
	}

	public void setEjecucionAnt_modMac(EjecucionPE ejecucionAnt_modMac) {
		this.ejecucionAnt_modMac = ejecucionAnt_modMac;
	}

	public Boolean getFlag_regulatorio() {
		return flag_regulatorio;
	}

	public void setFlag_regulatorio(Boolean flag_regulatorio) {
		this.flag_regulatorio = flag_regulatorio;
	}

		
}
