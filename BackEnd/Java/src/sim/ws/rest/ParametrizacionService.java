package sim.ws.rest;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import sim.ws.rest.JwtRequiereToken;
import sim.ws.rest.JwtRefrescaToken;

import sim.Contenedor;
import sim.persistencia.*;

@Path("/parametrizacion")
public class ParametrizacionService {

	private static Contenedor contenedor = Contenedor.obtenerInstancia();

	// CRUD Version Cartera
	@GET
	@Path("/obtenerEscenarios")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Escenario> obtenerEscenarios() throws Exception {
		return contenedor.obtenerEscenarios();
	}

	@POST
	@Path("/crearCarteraVersion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearCarteraVersion(CarteraVersion_staging cartera_version) throws Exception {
		//contenedor.validarInjectObjeto(cartera_version, false, true);
		contenedor.crearCarteraVersion(cartera_version);
	}

	@PUT
	@Path("/editarCarteraVersion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarCarteraVersion(CarteraVersion_staging cartera_version) throws Exception {
		//contenedor.validarInjectObjeto(cartera_version, false, true);
		contenedor.editarCarteraVersion(cartera_version);
	}
	
	@POST
	@Path("/replicarCarteraVersion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void replicarCarteraVersion(CarteraVersion_staging cartera_version) throws Exception {
		//contenedor.validarInjectObjeto(cartera_version, false, true);
		contenedor.replicarCarteraVersion(cartera_version);
	}
	
	@DELETE
	@Path("/borrarCarteraVersion/{id}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarVersionCartera(@PathParam("id") Integer id_version_cartera) throws Exception {
		contenedor.borrarCarteraVersion(id_version_cartera);
	}

	@GET
	@Path("/obtenerCarterasVersionStaging")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<CarteraVersion_staging> obtenerCarterasVersionStaging() throws Exception {
		return contenedor.obtenerCarterasVersionStaging();
	}

	@GET
	@Path("/obtenerCarterasVersionStagingPorIdCartera/{id_cartera}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<CarteraVersion_staging> obtenerCarterasVersionStagingPorIdCartera(
			@PathParam("id_cartera") Integer id_cartera) throws Exception {
		return contenedor.obtenerCarterasVersionStagingPorIdCartera(id_cartera);
	}
	
	// CRUD JuegoVersionesEscenario
	@GET
	@Path("/obtenerJuegosConEscenariosPorCarteraVersion/{id_cartera_version}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<JuegoEscenarios_version> obtenerJuegosConEscenariosPorCarteraVersion(
			@PathParam("id_cartera_version") Integer id_cartera_version) throws Exception {
		return contenedor.obtenerJuegosConEscenariosPorCarteraVersion(id_cartera_version);
	}

	// CRUD Escenario_Version por Version Cartera id
	@GET
	@Path("/obtenerEscenariosVersionPorCarteraVersion/{id_cartera_version}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Escenario_version> obtenerEscenariosVersionPorCarteraVersion(
			@PathParam("id_cartera_version") Integer id_cartera_version) throws Exception {
		return contenedor.obtenerEscenariosVersionPorCarteraVersion(id_cartera_version);
	}

	@PUT
	@Path("/editarEscenarioVersion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarEscenarioVersion(Escenario_version escenario_version) throws Exception {
		contenedor.validarInjectObjeto(escenario_version, false, true);
		contenedor.editarEscenarioVersion(escenario_version);
	}

	@DELETE
	@Path("/borrarEscenarioVersion/{id}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarVersionEscenario(@PathParam("id") Integer id_version) throws Exception {
		contenedor.borrarEscenarioVersion(id_version);
	}

	// Bloques parametrizados
	@GET
	@Path("/obtenerBloquesParametrizadoPorCarteraVersion/{id_version}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<BloqueParametrizado> obtenerBloquesParametrizadoPorCarteraVersion(
			@PathParam("id_version") Integer id_version) throws Exception {
		return contenedor.obtenerBloquesParametrizadoPorCarteraVersion(id_version);
	}

	@POST
	@Path("/editarBloqueParametrizado")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public BloqueParametrizado editarBloqueParametrizado(BloqueParametrizado bloqueParam) throws Exception {
		//contenedor.validarInjectObjeto(bloqueParam, false, true);
		return contenedor.editarBloqueParametrizado(bloqueParam);
	}

	@POST
	@Path("/editarBloquesParametrizados")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarBloquesParametrizados(ArrayList<BloqueParametrizado> bloquesParam) throws Exception {
		//contenedor.validarInjectObjeto(bloquesParam, false, true);
		contenedor.editarBloquesParametrizados(bloquesParam);
	}

	@GET
	@Path("/obtenerTablasInput")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TablaInput> obtenerTablasInput() throws Exception {
		return contenedor.obtenerTablasInput();
	}

	@GET
	@Path("/obtenerTablasInputPorIdCartera/{id_cartera}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TablaInput> obtenerTablasInputPorIdCartera(@PathParam("id_cartera") Integer id_cartera)
			throws Exception {
		return contenedor.obtenerTablasInputPorIdCartera(id_cartera);
	}

	@GET
	@Path("/obtenerResultadosVariablesMacrosPorEstructura/{id_metodologia}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<ResultadoVariableMacro> obtenerResultadosVariablesMacrosPorEstructura(
			@PathParam("id_metodologia") Integer id_metodologia) throws Exception {
		return contenedor.obtenerResultadosVariablesMacrosPorEstructura(id_metodologia);
	}

	// --------------------- Parametrización Staging ----------------------
	// --------------------- Made by Yisus --------------------------------
	@PUT
	@Path("/editarStagingCarteraVersion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarStagingCarteraVersion(CarteraVersion_staging cartera_version) throws Exception {
		contenedor.editarStagingCarteraVersion(cartera_version);
	}

	// ---------------------- Expresiones ----------------------------------
	@GET
	@Path("/obtenerExpresionesStagingPorCarteraVersion/{id_cartera_version}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Expresion_Staging> obtenerExpresionesStagingPorCarteraVersion(
			@PathParam("id_cartera_version") Integer id_cartera_version) throws Exception {
		return contenedor.obtenerExpresionesStagingPorCarteraVersion(id_cartera_version);
	}

	@POST
	@Path("/editarExpresionesStaging")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarExpresionesStaging(ArrayList<Expresion_Staging> expresiones) throws Exception {
		contenedor.editarExpresionesStaging(expresiones);
	}

	// ---------------------- Reglas -------------------------------------
	@GET
	@Path("/obtenerReglasStagingPorCarteraVersion/{id_cartera_version}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Regla_Staging> obtenerReglasStagingPorCarteraVersion(
			@PathParam("id_cartera_version") Integer id_cartera_version) throws Exception {
		return contenedor.obtenerReglasStagingPorCarteraVersion(id_cartera_version);
	}

	@POST
	@Path("/editarReglasStaging")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarReglasStaging(ArrayList<Regla_Staging> reglas) throws Exception {
		contenedor.editarReglasStaging(reglas);
	}
	/// Buckets

	@GET
	@Path("/obtenerBloqueBuckets/{id_cartera_version}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public BloqueParametrizado obtenerBloqueBuckets(@PathParam("id_cartera_version") Integer id_cartera_version)
			throws Exception {
		return contenedor.obtenerBloqueBuckets(id_cartera_version);
	}

	// ---------------------- Primeros pasos -------------------------------------
	@GET
	@Path("/obtenerPrimerosPasos")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<PrimerPaso> obtenerPrimerosPasos() throws Exception {
		return contenedor.obtenerPrimerosPasos();
	}

	@GET
	@Path("/obtenerEntidades")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Entidad> obtenerEntidades() throws Exception {
		return contenedor.obtenerEntidades();
	}

	@GET
	@Path("/obtenerCarterasPorIdEntidad/{id_entidad}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Cartera> obtenerCarterasPorIdEntidad(@PathParam("id_entidad") Integer id_entidad)
			throws Exception {
		return contenedor.obtenerCarterasPorIdEntidad(id_entidad);
	}

	@GET
	@Path("/obtenerEstructurasPorIdCartera/{id_cartera}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EstructuraMetodologica> obtenerEstructurasPorIdCartera(@PathParam("id_cartera") Integer id_cartera)
			throws Exception {
		return contenedor.obtenerEstructurasPorIdCartera(id_cartera);
	}
	
	@GET
	@Path("/obtenerEstructurasCalcPePorIdCartera/{id_cartera}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EstructuraMetodologica> obtenerEstructurasCalcPePorIdCartera(@PathParam("id_cartera") Integer id_cartera)
			throws Exception {
		return contenedor.obtenerEstructurasCalcPePorIdCartera(id_cartera);
	}

	@GET
	@Path("/obtenerPrimerPasoParametrizado/{id_primerPaso}/{id_metodologia}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public PrimerPasoParametrizado obtenerPrimerPasoParametrizado(@PathParam("id_primerPaso") Integer id_primerPaso,
			@PathParam("id_metodologia") Integer id_metodologia) throws Exception {
		return contenedor.obtenerPrimerPasoParametrizado(id_primerPaso, id_metodologia);
	}

	@PUT
	@Path("/parametrizarPrimerPaso")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void parametrizarPrimerPaso(PrimerPasoParametrizado primerPasoParam) throws Exception {
		//contenedor.validarInjectObjeto(primerPasoParam, false, true);
		contenedor.parametrizarPrimerPaso(primerPasoParam);
	}

}
