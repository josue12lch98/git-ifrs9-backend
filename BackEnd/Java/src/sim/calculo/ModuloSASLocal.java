package sim.calculo;

import sim.persistencia.AjustePdProyec;
import sim.persistencia.BloqueParametrizado;
import sim.persistencia.CampoParametrizado;
import sim.persistencia.CargaFichero;
import sim.persistencia.Cartera;
import sim.persistencia.EjecucionAgregadaPE;
import sim.persistencia.EjecucionHistorica;
import sim.persistencia.EjecucionMotor;
import sim.persistencia.EjecucionPrimerPasoMotor;
import sim.persistencia.Escenario;
import sim.persistencia.Expresion_Staging;
import sim.persistencia.GrupoBloquesEscenario;
import sim.persistencia.MapeoCargasModMacEscVer;
import sim.persistencia.Regla_Staging;
import sim.persistencia.Stage;
import sim.persistencia.bd.ModuloDatos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import java.util.Properties;

public class ModuloSASLocal {

	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(ModuloSAS.class);
	// Datos para la conexion de JAVA con SAS
	private static String SAS_rutaRaiz;
	private static String SAS_codigoCalidad;
	private static String SAS_separador_ruta;
	private static String SAS_codigoInicial;
	private static String SAS_log_completo;

	// Datos para la conexion de SAS con BBDD
	private static String SAS_JDBC_port;
	private static String SAS_JDBC_host;
	private static String SAS_JDBC_protocol;
	private static String SAS_JDBC_serverType;
	private static String SAS_JDBC_serviceName;
	private static String SAS_JDBC_userName;
	private static String SAS_JDBC_encriptado;
	private static String SAS_JDBC_schema;

	private static String SAS_local;

	private ModuloDatos md;


	public ModuloSASLocal(Properties properties, ModuloDatos md) throws Exception {
		LOGGER.info("Creando ModuloSAS");
		SAS_rutaRaiz = properties.getProperty("SAS_rutaRaiz");
		SAS_codigoCalidad = properties.getProperty("SAS_codigoCalidad");
		SAS_separador_ruta = properties.getProperty("SAS_separador_ruta");
		SAS_codigoInicial = properties.getProperty("SAS_codigoInicial");
		SAS_log_completo = properties.getProperty("SAS_log_completo");

		SAS_JDBC_host = properties.getProperty("JDBC_host");
		SAS_JDBC_port = properties.getProperty("JDBC_port");
		SAS_JDBC_protocol = properties.getProperty("JDBC_protocol");
		SAS_JDBC_serverType = properties.getProperty("JDBC_serverType");
		SAS_JDBC_serviceName = properties.getProperty("JDBC_serviceName");
		SAS_JDBC_userName = properties.getProperty("JDBC_userName");
		SAS_JDBC_encriptado = properties.getProperty("JDBC_encriptado");
		SAS_JDBC_schema = properties.getProperty("JDBC_schema");

		SAS_local = properties.getProperty("SAS_local");

		this.md = md;

		LOGGER.info("ModuloSAS creado");
	}

	public void ejecutarCalidadDatos(CargaFichero cargaFichero) throws Exception {

		ArrayList<String> lineasSubmit = new ArrayList<String>();
		ArrayList<String> lineasOutput = null;
		String[] lineasSubmitArr = null;

		Calendar c = Calendar.getInstance();

		String tiempo_actual = String.valueOf(c.get(Calendar.YEAR)) + ".";
		tiempo_actual += String.valueOf((c.get(Calendar.MONTH) + 1)) + ".";
		tiempo_actual += String.valueOf(c.get(Calendar.DAY_OF_MONTH)) + "_";
		tiempo_actual += String.valueOf(c.get(Calendar.HOUR_OF_DAY)) + ".";
		tiempo_actual += String.valueOf(c.get(Calendar.MINUTE)) + ".";
		tiempo_actual += String.valueOf(c.get(Calendar.SECOND));

		String nombre_carpeta = tiempo_actual + "_" + cargaFichero.getTablaInput().getNombre_fichero();
		String ruta_bd = "";

		if (!SAS_log_completo.equalsIgnoreCase("SI")) {
			lineasSubmit.add("options nonotes nosource;");
		}

		lineasSubmit.add("%let slash=%str(" + SAS_separador_ruta + ");");

		// se envia datos de la entidad aprovisionamiento
		lineasSubmit.add("%let codAprovisionamiento=" + cargaFichero.getTablaInput().getId_aprovisionamiento() + ";");
		lineasSubmit.add("%let nbrFichero=" + cargaFichero.getTablaInput().getNombre_fichero() + ";");

		lineasSubmit.add("%let carpeta_calidad=Calidad;");
		lineasSubmit.add("%let carpeta_codigos_calidad=Codigos;");
		lineasSubmit.add("%let carpeta_logs_calidad=Logs;");

		// se rescata la carpeta del codigo y el nombre del codigo (se utiliza la ruta
		// parametrizada)
		lineasSubmit.add("%let nombre_raiz=" + SAS_rutaRaiz + ";");
		lineasSubmit.add("%let nombre_codigo_calidad= " + SAS_codigoCalidad + ";");

		// se crean las rutas que incluye el codigo de calidad y logs
		lineasSubmit.add("%let string_nombre_raiz=%sysfunc(Trim(%Str(&comilla.)&nombre_raiz.%Str(&comilla.)));");
		lineasSubmit.add("%let raiz_calidad=&nombre_raiz.&slash.&carpeta_calidad.;");

		lineasSubmit.add("%let nombre_carpeta=" + nombre_carpeta + ";");
		lineasSubmit.add("%let string_nombre_carpeta=%sysfunc(Trim(%Str(&comilla.)&nombre_carpeta.%Str(&comilla.)));");

		lineasSubmit.add("%let ruta_raiz_log=&raiz_calidad.&slash.&carpeta_logs_calidad.;");
		lineasSubmit.add("%let string_ruta_raiz_log=%sysfunc(Trim(%Str(&comilla.)&ruta_raiz_log.%Str(&comilla.)));");

		lineasSubmit.add("data _null_;ruta_out=dcreate(&string_nombre_carpeta.,&string_ruta_raiz_log.); RUN;");
		lineasSubmit.add("%let ruta_log=&ruta_raiz_log.&slash.&nombre_carpeta.;");
		lineasSubmit.add("%let string_ruta_log=%sysfunc(Trim(%Str(&comilla.)&ruta_log.%Str(&comilla.)));");

		lineasSubmit.add("%let archivo_log=%sysfunc(Trim(%Str(&comilla.)&ruta_log.&slash.Log_Calidad_&nbrFichero..log%Str(&comilla.)));");
		lineasSubmit.add("filename ifrs9_L &archivo_log.;");

		lineasSubmit.add("proc printto log=ifrs9_L; RUN;");

		// si es local, se utiliza la carpeta del log como base
		if (SAS_local.equalsIgnoreCase("SI")) {
			ruta_bd = SAS_rutaRaiz + SAS_separador_ruta + "Calidad" + SAS_separador_ruta + "Logs" + SAS_separador_ruta
					+ nombre_carpeta + SAS_separador_ruta;
			lineasSubmit.add("Libname mymdb &string_ruta_log.;");

		} else {
			lineasSubmit.add("%let usuario_oracle=%sysfunc(Trim(%Str(&comilla.)" + SAS_JDBC_userName + "%Str(&comilla.)));");
			lineasSubmit.add("%let contrasena_oracle=%sysfunc(Trim(%Str(&comilla.)" + SAS_JDBC_encriptado + "%Str(&comilla.)));");
			String path_oracle = "(DESCRIPTION = (ADDRESS = (PROTOCOL=" + SAS_JDBC_protocol + ") (HOST=" + SAS_JDBC_host
					+ ") (PORT=" + SAS_JDBC_port + ")) (CONNECT_DATA = (SERVER = " + SAS_JDBC_serverType
					+ ") (SERVICE_NAME = " + SAS_JDBC_serviceName + ")))";
			lineasSubmit.add("%let path_oracle=%sysfunc(Trim(%Str(&comilla.)" + path_oracle + "%Str(&comilla.)));");
			lineasSubmit.add("Libname sche1 oracle user=&usuario_oracle. encriptado=&contrasena_oracle. path=&path_oracle. schema=&usuario_oracle;");
			lineasSubmit.add("Libname sche2 oracle user=&usuario_oracle. encriptado=&contrasena_oracle. path=&path_oracle. schema="
							+ SAS_JDBC_schema + ";");
			lineasSubmit.add("Libname mymdb (sche1 sche2);");
		}

		lineasSubmit.add("%let LibInput = mymdb;");

		// ejecutamos parcialmente, para asegurarnos que se creen las carpetas
		// requeridas
		lineasOutput = new ArrayList<String>(lineasSubmit);
		lineasSubmit.add("proc sql;");
		for (int i = 0; i < lineasOutput.size(); i++) {
			lineasSubmit.add("insert into inicializacionJavaPrev values (\'" + lineasOutput.get(i) + "\');");
		}
		lineasSubmit.add("quit;");
		lineasSubmitArr = new String[lineasSubmit.size()];
		lineasSubmitArr = lineasSubmit.toArray(lineasSubmitArr);
		lineasSubmitArr = null;
		lineasSubmit = new ArrayList<String>();

		// nombres de tablas a usar;
		String nombre_tabla_fichero = "he_" + cargaFichero.getTablaInput().getNombre_fichero() + "_V"
				+ cargaFichero.getTablaInput().getVersion_metadata();

		lineasSubmit.add("%let tabla_campoaprovisionamiento = he_campoaprovisionamiento;");
		lineasSubmit.add("%let tabla_cargafichero = he_cargafichero;");
		lineasSubmit.add("%let tabla_reglacalidad = he_reglacalidad;");
		lineasSubmit.add("%let tabla_segmento = he_segmento;");
		lineasSubmit.add("%let tabla_tablainput = he_tablainput;");
		lineasSubmit.add("%let tabla_tipoindicador = me_tipoindicador;");
		lineasSubmit.add("%let tabla_fichero = " + nombre_tabla_fichero + ";");

		// se envia datos de la entidad carga fichero
		lineasSubmit.add("%let codCargaFichero=" + cargaFichero.getId_cargafichero() + ";");

		// se envia datos de la entidad aprovisionamiento
		lineasSubmit.add("%let codAprovisionamiento=" + cargaFichero.getTablaInput().getId_aprovisionamiento() + ";");
		lineasSubmit.add("%let nbrFichero=" + cargaFichero.getTablaInput().getNombre_fichero() + ";");

		// se envia datos de la entidad tablaInput
		lineasSubmit.add("%let codTablaInput=" + cargaFichero.getTablaInput().getId_tablainput() + ";");
		lineasSubmit.add("%let numVersionMetadata=" + cargaFichero.getTablaInput().getVersion_metadata() + ";");

		// se calcula la fecha del mes anterior
		String fechaActual = cargaFichero.getFecha().getCodmes().toString();
		lineasSubmit.add("%let fechaActual=" + fechaActual + ";");
		lineasSubmit.add("%let codFechaActual=" + cargaFichero.getFecha().getId_fecha() + ";");
		lineasSubmit.add("%let faseEjecucion=" + cargaFichero.getFecha().getFase() + ";");
		c.set(Integer.valueOf(fechaActual.substring(0, 4)), Integer.valueOf(fechaActual.substring(4, 6)) - 1, 1);
		c.add(Calendar.MONTH, -1);

		// se envia la fecha del mes anterior
		Integer fechaAnterior = (c.get(Calendar.YEAR) * 100) + (c.get(Calendar.MONTH) + 1);
		lineasSubmit.add("%let fechaAnterior=" + fechaAnterior + ";");

		// se envia nombres de las tablas del modelo a utilizar
		lineasSubmit.add("%let output_calidad=HE_OutputFicheroReglaCalidad;");
		lineasSubmit.add("%let output_totales=HE_OutputFicheroTotal;");
		lineasSubmit.add("%let output_evidencia=HE_OutputEvidCalidad;");
		lineasSubmit.add("%let output_histograma=HE_OutputHistoCalidad;");

		lineasSubmit.add("%let tabla_campoaprovisionamiento = he_campoaprovisionamiento;");
		lineasSubmit.add("%let tabla_cargafichero = he_cargafichero;");
		lineasSubmit.add("%let tabla_reglacalidad = he_reglacalidad;");
		lineasSubmit.add("%let tabla_segmento = he_segmento;");
		lineasSubmit.add("%let tabla_fecha = he_fecha;");
		lineasSubmit.add("%let tabla_tablainput = he_tablainput;");
		lineasSubmit.add("%let tabla_tipoindicador = me_tipoindicador;");
		lineasSubmit.add("%let tabla_fichero = " + nombre_tabla_fichero + ";");

		// generamos las tablas que necesita SAS en la ruta local
		if (SAS_local.equalsIgnoreCase("SI")) {
			md.escribirCSV(ruta_bd, "he_campoaprovisionamiento", "he_campoaprovisionamiento", true);
			md.escribirCSV(ruta_bd, "he_cargafichero", "he_cargafichero", true);
			md.escribirCSV(ruta_bd, "he_reglacalidad", "he_reglacalidad", true);
			md.escribirCSV(ruta_bd, "he_segmento", "he_segmento", true);
			md.escribirCSV(ruta_bd, "he_fecha", "he_fecha", true);
			md.escribirCSV(ruta_bd, "he_tablainput", "he_tablainput", true);
			md.escribirCSV(ruta_bd, "me_tipoindicador", "me_tipoindicador", true);
			md.escribirCSV(ruta_bd, SAS_JDBC_userName + "." + nombre_tabla_fichero, nombre_tabla_fichero, true);
		}

		// enlazamos al codigo de ejecucion
		lineasSubmit.add(
				"%let ruta_inicio = %sysfunc(Trim(%Str(&comilla.)&raiz_calidad.&slash.&carpeta_codigos_calidad.&slash.&nombre_codigo_calidad.%Str(&comilla.)));");
		lineasSubmit.add("%include &ruta_inicio;");

		// iniciamos la ejecución
		if (SAS_local.equalsIgnoreCase("SI")) {
			// importamos las tablas que necesita SAS
			lineasSubmit.add("%importarFichero(&ruta_log.,&tabla_campoaprovisionamiento.);");
			lineasSubmit.add("%importarFichero(&ruta_log.,&tabla_cargafichero.);");
			lineasSubmit.add("%importarFichero(&ruta_log.,&tabla_reglacalidad.);");
			lineasSubmit.add("%importarFichero(&ruta_log.,&tabla_segmento.);");
			lineasSubmit.add("%importarFichero(&ruta_log.,&tabla_fecha.);");
			lineasSubmit.add("%importarFichero(&ruta_log.,&tabla_tablainput.);");
			lineasSubmit.add("%importarFichero(&ruta_log.,&tabla_tipoindicador.);");
			lineasSubmit.add("%importarFichero(&ruta_log.,&tabla_fichero.);");
		}
		lineasSubmit.add("%calidadAprovisionamiento();");

		if (SAS_local.equalsIgnoreCase("SI")) {
			lineasSubmit.add("%exportarFichero(&ruta_log.,&output_calidad.);");
			lineasSubmit.add("%exportarFichero(&ruta_log.,&output_totales.);");
			lineasSubmit.add("%exportarFichero(&ruta_log.,&output_evidencia.);");
			lineasSubmit.add("%exportarFichero(&ruta_log.,&output_histograma.);");
		}

		lineasOutput = new ArrayList<String>(lineasSubmit);
		lineasSubmit.add("proc sql;");
		for (int i = 0; i < lineasOutput.size(); i++) {
			lineasSubmit.add("insert into inicializacionJavaPrev values (\'" + lineasOutput.get(i) + "\');");
		}
		lineasSubmit.add("quit;");
		lineasSubmit.add("%crearFicheroSAS();");

		if (SAS_local.equalsIgnoreCase("SI")) {
			//lineasSubmit.add("%limpiarTablasVariables(&LibInput.);");
		} else {
			//lineasSubmit.add("%limpiarTablasVariables();");
		}
		lineasSubmit.add("libname sche1 clear;");
		lineasSubmit.add("libname sche2 clear;");
		lineasSubmit.add("libname &LibInput. clear;");

		// se envia el codigo generado a ejecutar y se deja el control a SAS
		lineasSubmitArr = new String[lineasSubmit.size()];
		lineasSubmitArr = lineasSubmit.toArray(lineasSubmitArr);


		if (SAS_local.equalsIgnoreCase("SI")) {
			md.leerCSV(ruta_bd, "HE_OutputFicheroReglaCalidad", "HE_OutputFicheroReglaCalidad", false);
			md.leerCSV(ruta_bd, "HE_OutputFicheroTotal", "HE_OutputFicheroTotal", false);
			md.leerCSV(ruta_bd, "HE_OutputHistoCalidad", "HE_OutputHistoCalidad", false);
			md.leerCSV(ruta_bd, "HE_OutputEvidCalidad", "HE_OutputEvidCalidad", false);
		}

	}

	public void ejecutarCalculo(EjecucionMotor ejecucion, Boolean esEjecVariac, Boolean esRegulatorio) throws Exception {
		
		Calendar c = Calendar.getInstance();
		ArrayList<String> lineasSubmit = new ArrayList<String>();

		String tiempo_actual = String.valueOf(c.get(Calendar.YEAR)) + ".";
		tiempo_actual += String.valueOf((c.get(Calendar.MONTH) + 1)) + ".";
		tiempo_actual += String.valueOf(c.get(Calendar.DAY_OF_MONTH)) + "_";
		tiempo_actual += String.valueOf(c.get(Calendar.HOUR_OF_DAY)) + ".";
		tiempo_actual += String.valueOf(c.get(Calendar.MINUTE)) + ".";
		tiempo_actual += String.valueOf(c.get(Calendar.SECOND));

		String nombreEntidad = ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia()
				.getCartera().getEntidad().getNombre_variablemotor();
		String nombreCartera = ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia()
				.getCartera().getNombre_variablemotor();

		String nombre_carpeta_log = "log_" + tiempo_actual;
		String nombre_carpeta_cartera = "log_" + nombreCartera;
		String nombre_carpeta_entidad = "log_" + nombreEntidad;
		String ruta_bd = "";

		//if (md.obtenerNroEstadosEjecucion(1) > SAS_maxWorkspace) {
			//LOGGER.info("No hay conexiones disponibles, se queda la ejecucion  de " + nombreCartera + "en cola.");
			//return;
		//}

		// comienzo de la creacion del codigo SAS inicialización
		ArrayList<BloqueParametrizado> bloquesBD = new ArrayList<BloqueParametrizado>();


		// ruta de los codigos
		lineasSubmit.add("%let slash=%str(" + SAS_separador_ruta + ");");
		ruta_bd = ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia().getCodigo_estructura();
		lineasSubmit.add("%let ruta_codigos=" + ruta_bd + ";");

		// variables generales con el nombre de la entidad y cartera
		lineasSubmit.add("%let entidad = " + nombreEntidad + ";");
		lineasSubmit.add("%let codigo_entidad = " + ejecucion.getJuego_escenarios_version().getCartera_version()
				.getMetodologia().getCartera().getEntidad().getId_entidad() + ";");
		lineasSubmit.add("%let cartera = " + nombreCartera + ";");
		Integer codcartera = ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
				.getId_cartera();
		lineasSubmit.add("%let codigo_cartera = " + codcartera + ";");

		// fecha de ejecución
		String fechaActual = ejecucion.getCodmes().toString();
		lineasSubmit.add("%let fecha_referencia = " + fechaActual + ";");
		
		c.set(Integer.valueOf(fechaActual.substring(0, 4)), Integer.valueOf(fechaActual.substring(4, 6)) - 1, 1);
		
		c.add(Calendar.MONTH, 1);
		String fechaSiguiente = String.valueOf(c.get(Calendar.YEAR) * 100 + (c.get(Calendar.MONTH) + 1));
		lineasSubmit.add("%let fecha_siguiente = " + fechaSiguiente + ";");
		
		c.add(Calendar.MONTH, -2);
		String fechaAnterior = String.valueOf(c.get(Calendar.YEAR) * 100 + (c.get(Calendar.MONTH) + 1));
		lineasSubmit.add("%let fecha_anterior = " + fechaAnterior + ";");

		// tipo de ejecución y codigos de versiones
		Integer tipo_ejecucion = ejecucion.getTipo_ejecucion().getId_tipo_ejecucion(); //1:fase1 2:fase2
		Integer flag_variaciones = (esEjecVariac == true ? 1 : 0);
		Integer flag_regulatorio = (esRegulatorio == true ? 1 : 0);
		
		lineasSubmit.add("%let codigo_tipo_ejecucion = " + tipo_ejecucion + ";");
		lineasSubmit.add("%let flag_variaciones = " + flag_variaciones + ";");
		lineasSubmit.add("%let flag_variaciones = " + flag_regulatorio + ";");
		lineasSubmit.add("%let codigo_cartera_version = "
				+ ejecucion.getJuego_escenarios_version().getCartera_version().getId_cartera_version() + ";");
		lineasSubmit.add("%let codigo_juego_escenario = "
				+ ejecucion.getJuego_escenarios_version().getId_juego_escenarios() + ";");

		// creamos la jerarquia de carpetas para el log
		
		lineasSubmit.add("%let carpeta_sas=&ruta_codigos.&slash.;");
		lineasSubmit.add("%let nombre_codigo_inicial= " + SAS_codigoInicial + ";");
		
		
		//sasLanguage.Submit("%let archivo_log=%sysfunc(Trim(%Str(&comilla.)&ruta_log.&slash.Log_" + nombreCartera + "_" + fechaActual  + ".log%Str(&comilla.)));");
		//sasLanguage.Submit("filename ifrs9_L &archivo_log.;");
		//sasLanguage.Submit("proc printto log=ifrs9_L; RUN;");

		//LOGGER.info("SAS ejecucion PE: Jerarquia de carpetas Logs.");
		
		// libname a la base de datos
		if (SAS_local.equalsIgnoreCase("SI")) {

			if (SAS_local.equalsIgnoreCase("SI")) {
				ruta_bd = ruta_bd + SAS_separador_ruta + nombre_carpeta_entidad + SAS_separador_ruta
						+ nombre_carpeta_cartera + SAS_separador_ruta + nombre_carpeta_log + SAS_separador_ruta;
			}
			lineasSubmit.add("Libname mymdb &string_ruta_log.;");

		} else {
			lineasSubmit.add("%let usuario_oracle=%sysfunc(Trim(%Str(&comilla.)" + SAS_JDBC_userName + "%Str(&comilla.)));");
			lineasSubmit.add("%let contrasena_oracle=%sysfunc(Trim(%Str(&comilla.)" + SAS_JDBC_encriptado + "%Str(&comilla.)));");
			String path_oracle = "(DESCRIPTION = (ADDRESS = (PROTOCOL=" + SAS_JDBC_protocol + ") (HOST=" + SAS_JDBC_host
					+ ") (PORT=" + SAS_JDBC_port + ")) (CONNECT_DATA = (SERVER = " + SAS_JDBC_serverType
					+ ") (SERVICE_NAME = " + SAS_JDBC_serviceName + ")))";
			lineasSubmit.add("%let path_oracle=%sysfunc(Trim(%Str(&comilla.)" + path_oracle + "%Str(&comilla.)));");
			lineasSubmit.add("Libname sche1 oracle user=&usuario_oracle. encriptado=&contrasena_oracle. path=&path_oracle. schema=&usuario_oracle;");
			lineasSubmit.add("Libname sche2 oracle user=&usuario_oracle. encriptado=&contrasena_oracle. path=&path_oracle. schema="
							+ SAS_JDBC_schema + ";");
			lineasSubmit.add("Libname mymdb (sche1 sche2);");
		}
		lineasSubmit.add("%put libreria mymdb creada;");
		lineasSubmit.add("%let lib_input = mymdb;");
		lineasSubmit.add("%let lib_bases = work;");
		lineasSubmit.add("%let lib_salida = mymdb;");

		//LOGGER.info("SAS ejecucion PE: Librerias input.");
		
		
		// ejecutamos parcialmente, para asegurarnos que se creen las carpetas
		// requeridas

//		sasLanguage.Submit("proc sql;");
//		for (int i = 0; i < lineasSubmit.size(); i++) {
//			sasLanguage.Submit("insert into inicializacionJavaPrev values (\'" + lineasSubmit.get(i) + "\');");
//		}
//		sasLanguage.Submit("quit;");
//		
//		lineasSubmitArr = new String[lineasSubmit.size()];
//		lineasSubmitArr = lineasSubmit.toArray(lineasSubmitArr);
//		sasLanguage.SubmitLines(lineasSubmitArr);
//		lineasSubmitArr = null;
//		lineasSubmit = new ArrayList<String>();
		
		
		// variables a nivel metodologia de la cartera
		Integer tipo_obtencion_resultados = ejecucion.getJuego_escenarios_version().getCartera_version()
				.getMetodologia().getMetodo_resultados().getId_tipo_obtencion_resultados();
		lineasSubmit.add("%let tipo_obtencion_resultado = " + tipo_obtencion_resultados + ";");
		
		Integer lifeTime=0;
		if(tipo_obtencion_resultados==1 && flag_variaciones==0) {
			lifeTime = ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia().getLife_time();
			lineasSubmit.add("%let lifetime = " + lifeTime + ";");
			lineasSubmit.add("%let periodicidad = " + ejecucion.getJuego_escenarios_version().getCartera_version()
					.getMetodologia().getPeriodicidad().getValor_periodicidad() + ";");
			lineasSubmit.add("%let maximo_plazo = "
					+ ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia().getNro_maxPlazo()
					+ ";");
		}
		//LOGGER.info("SAS ejecucion PE: Variables a nivel cartera enviadas.");
		
		lineasSubmit.add("%let flag_minorista = " + (ejecucion.getJuego_escenarios_version().getCartera_version()
				.getMetodologia().getCartera().getTipo_magnitud().getId_tipo_magnitud() == 2 ? 1 : 0) + ";");
		
		Integer flgMayorista = (ejecucion.getJuego_escenarios_version().getCartera_version()
				.getMetodologia().getCartera().getTipo_magnitud().getId_tipo_magnitud() == 1 ? 1 : 0);
		lineasSubmit.add("%let flag_mayorista = " + flgMayorista + ";");
		
		Integer flgRevolvente = (ejecucion.getJuego_escenarios_version().getCartera_version()
				.getMetodologia().getCartera().getTipo_cartera().getId_tipo_cartera() == 1 ? 1 : 0);
		lineasSubmit.add("%let flag_revolvente = " + flgRevolvente + ";");
		
		lineasSubmit.add("%let flag_hipotecas = " + (ejecucion.getJuego_escenarios_version().getCartera_version()
				.getMetodologia().getCartera().getTipo_cartera().getId_tipo_cartera() == 2 ? 1 : 0) + ";");
		
		Integer flgRefinanciado = (ejecucion.getJuego_escenarios_version().getCartera_version()
				.getMetodologia().getCartera().getTipo_cartera().getId_tipo_cartera() == 3 ? 1 : 0);
		lineasSubmit.add("%let flag_refinanciado = " + flgRefinanciado + ";");
		
		lineasSubmit.add("%let flag_consumo = " + (ejecucion.getJuego_escenarios_version().getCartera_version()
				.getMetodologia().getCartera().getTipo_cartera().getId_tipo_cartera() == 4 ? 1 : 0) + ";");
		
		lineasSubmit.add("%let flag_bolsonPyme = " + (ejecucion.getJuego_escenarios_version().getCartera_version()
				.getMetodologia().getCartera().getTipo_cartera().getId_tipo_cartera() == 5 ? 1 : 0) + ";");
		
		Integer flag_CFSF = (ejecucion.getJuego_escenarios_version().getCartera_version()
				.getMetodologia().getCartera().getFlag_pd6meses() ? 1 : 0) ;
		lineasSubmit.add("%let flag_CFSF = " + flag_CFSF + ";");
		
		//LOGGER.info("SAS ejecucion PE: Tipos de carteras enviadas.");

		if(tipo_obtencion_resultados==1 && flag_variaciones==0) {
			lineasSubmit.add("%let metodologia_PD = " + ejecucion.getJuego_escenarios_version().getCartera_version()
					.getMetodologia().getMetodologia_PD().getId_tipo_metodologia() + ";");
			if (!ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia().getFlag_lgdUnica()) {
				lineasSubmit.add("%let metodologia_LGD_BE = " + ejecucion.getJuego_escenarios_version().getCartera_version()
						.getMetodologia().getMetodologia_LGD_BE().getId_tipo_metodologia() + ";");
				lineasSubmit.add("%let metodologia_LGD_WO = " + ejecucion.getJuego_escenarios_version().getCartera_version()
						.getMetodologia().getMetodologia_LGD_WO().getId_tipo_metodologia() + ";");
			} else {
				lineasSubmit.add("%let metodologia_LGD = " + ejecucion.getJuego_escenarios_version().getCartera_version()
						.getMetodologia().getMetodologia_LGD().getId_tipo_metodologia() + ";");
			}
			lineasSubmit.add("%let metodologia_EAD = " + ejecucion.getJuego_escenarios_version().getCartera_version()
					.getMetodologia().getMetodologia_EAD().getId_tipo_metodologia() + ";");
			
			//LOGGER.info("SAS ejecucion PE: Variables a de metodología enviadas.");
		}
		
		// nombres de tablas del modelo de datos a usar;
		lineasSubmit.add("%let HE_OutputProvisionesCred = HE_OutputProvisionesCred;");

		// otras variables
		lineasSubmit.add("%let lista_factores = ;");
		lineasSubmit.add("%let lista_nombre_PD = ;");
		lineasSubmit.add("%let flag_ajustado = 0;");
		
		// Reglas y Expresiones de Staging
		ArrayList<String> mesesCura = new ArrayList<String>();

		if ((tipo_obtencion_resultados==1 || tipo_obtencion_resultados==2) && tipo_ejecucion==1 && flag_variaciones==0) {
			lineasSubmit.add("%let flag_stage_interno = " + (ejecucion.getStage_interno() ? 1 : 0) + ";");
			lineasSubmit.add("%let flag_stage_externo = " + (ejecucion.getStage_externo() ? 1 : 0) + ";");
			//en el caso de variaciones y mayorista, para no entrar a cura
			if (flgMayorista!=1) {
				lineasSubmit.add("%let flag_stage_historico = " + (ejecucion.getStage_sensibilidad() ? 1 : 0) + ";");
			}else {
				lineasSubmit.add("%let flag_stage_historico = 0;");
			}
	
			for (int i = 0; i < ejecucion.getMeses_sensibilidad(); i++) {
				//se comienza ya con el mes anterior
				if (i > 0) {
					c.add(Calendar.MONTH, -1);
				}
				mesesCura.add(String.valueOf(c.get(Calendar.YEAR) * 100 + (c.get(Calendar.MONTH) + 1)));
			}
	
			lineasSubmit.add("%let lista_meses_anteriores = " + String.join("*", mesesCura) + ";");
	
			ArrayList<String> listaReglas = new ArrayList<String>();
			String condicion_cae_por = "";
			String condicion_staging_prelacion = "";
	
			for (int i = 1; i <= ejecucion.getReglasStaging().size(); i++) {
				listaReglas.add("regla_" + i);
				condicion_cae_por = condicion_cae_por + transformarReglasCaePor(ejecucion.getReglasStaging().get(i),
						"regla_" + i, ejecucion.getExpresionesStaging());
			}
			lineasSubmit.add("%let condicion_staging_regla = " + condicion_cae_por + ";");
			lineasSubmit.add("%let lista_reglas_staging = " + String.join("*", listaReglas) + ";");
	
			if (ejecucion.getReglasStaging().size() > 0) {
				condicion_staging_prelacion = transformarReglasPrelacion(ejecucion.getReglasStaging().get(1),
						ejecucion.getExpresionesStaging(), ejecucion.getReglasStaging(), ejecucion.getStagesStaging());
			}
			lineasSubmit.add("%let condicion_staging_prelacion = " + condicion_staging_prelacion + ";");
			
			//LOGGER.info("SAS ejecucion PE: Variables de staging enviadas.");
			
		}else {
			lineasSubmit.add("%let flag_stage_historico = 0;");
		}

		
		// Bloques
		ArrayList<String> bucketsScoreValores = new ArrayList<String>();
		ArrayList<String> bucketsScoreNombres = new ArrayList<String>();
		Integer numBucket = 0;
		String keep_bloque = "";
		
		if ((tipo_obtencion_resultados==1 || tipo_obtencion_resultados==2)) {
			for (BloqueParametrizado bloque : ejecucion.getBloquesParam_carteraVersion()) {
	
				// Bloque con la información de buckets score
				if (bloque.getTipo_bloque().getFlag_bucket() && bloque.getCampos_parametrizado().size()>0) {
					for (int i = 1; i < bloque.getCampos_parametrizado().size(); i++) {
						if (i % 2 != 0) {
							bucketsScoreValores.add(String.valueOf(bloque.getCampos_parametrizado().get(i).getNumValor()));
							numBucket = (int) (Math.ceil(i / 2) + 1);
							bucketsScoreNombres.add("bucket_" + numBucket);
						}
					}
					//para revolvente incluimos el bucket para inactivos
					if (flgRevolvente == 1) {
						numBucket++;
						bucketsScoreNombres.add("bucket_" + numBucket);
					}
					
					bucketsScoreValores.remove(bucketsScoreValores.size() - 1);
					lineasSubmit.add("%let bucket_pd=bucket_menos1;");
					lineasSubmit.add("%let bucket_pc=bucket_0;");
					lineasSubmit.add("%let primer_bucket=bucket_inicial;");
	
					lineasSubmit.add("%let lista_limite_score = " + String.join("*", bucketsScoreValores) + ";");
					lineasSubmit.add("%let lista_nombre_bucket = " + String.join("*", bucketsScoreNombres)
							+ "*&bucket_pc.*&bucket_pd." + ";");	
					
					//LOGGER.info("SAS ejecucion PE: Variables de bloque buckets enviadas.");
					
				// bloques con la información de tablas
				}else if (bloque.getFlag_tabla_input()) {
					lineasSubmit.add("%let " + bloque.getVariable_motor() + "=" + bloque.getTabla_input().getNombre_fichero() + ";");
					lineasSubmit.add("%let " + "where_" + bloque.getVariable_motor() + "= "
							+ (bloque.getFiltro_tabla() != null ? "(" + bloque.getFiltro_tabla().replace("'", "\"") + ")" : "")
							+ ";");
					keep_bloque = "";
					for (CampoParametrizado campo : bloque.getCampos_parametrizado()) {
						if (campo.getCampo_input().getNombre_campo() != null) {
							lineasSubmit.add("%let " + campo.getCampo_bloque().getVariable_motor() + "="
									+ campo.getCampo_input().getNombre_campo() + ";");
							keep_bloque = keep_bloque + campo.getCampo_input().getNombre_campo() + " ";
						} else {
							lineasSubmit.add("%let " + campo.getCampo_bloque().getVariable_motor() + "= ;");
						}
					}
					lineasSubmit.add("%let " + "keep_" + bloque.getVariable_motor() + "=" + keep_bloque + ";");
	
					// guardamos los bloques input
					bloquesBD.add(bloque);
	
					// bloques con la información manual
				} else {
					for (CampoParametrizado campo : bloque.getCampos_parametrizado()) {
						if (campo.getCampo_bloque().getTipo_campo().getId_tipo_campo() == 1) {
	
							if (campo.getNumValor() % 1 == 0) {
								lineasSubmit.add("%let " + campo.getCampo_bloque().getVariable_motor() + "="
										+ campo.getNumValor().intValue() + ";");
							} else {
								lineasSubmit.add("%let " + campo.getCampo_bloque().getVariable_motor() + "="
										+ campo.getNumValor() + ";");
							}
						} else if (campo.getCampo_bloque().getTipo_campo().getId_tipo_campo() == 2) {
							lineasSubmit.add("%let " + campo.getCampo_bloque().getVariable_motor() + "="
									+ campo.getTxtValor() + ";");
						}
					}
				}
			}
		}
		//LOGGER.info("SAS ejecucion PE: Variables de bloques enviadas.");

		//ruta de los codigos
		lineasSubmit.add("%let ruta_inicio = %sysfunc(Trim(%Str(&comilla.)&ruta_codigos.&slash.&nombre_codigo_inicial.%Str(&comilla.)));");
		
		// ejecutamos parcialmente, para poder hacer el include correctamente
//		sasLanguage.Submit("proc sql;");
//		for (int i = 0; i < lineasSubmit.size(); i++) {
//			sasLanguage.Submit("insert into inicializacionJavaPrev values (\'" + lineasSubmit.get(i) + "\');");
//		}
//		sasLanguage.Submit("quit;");
//		
//		lineasSubmitArr = new String[lineasSubmit.size()];
//		lineasSubmitArr = lineasSubmit.toArray(lineasSubmitArr);
//		sasLanguage.SubmitLines(lineasSubmitArr);
//		lineasSubmitArr = null;
//		lineasSubmit = new ArrayList<String>();
		
//		// enlazamos al codigo de ejecucion
//		sasLanguage.Submit("%include &ruta_inicio / lrecl=500;");
//		
		//LOGGER.info("SAS ejecucion PE: Enlace la codigo del motor enviado.");
	
		//ejecuciones de primeros pasos asociadas
		if(tipo_obtencion_resultados==1 && flag_variaciones==0 && ejecucion.getEjecucion_modMacro()!=null) {
			lineasSubmit.add("%let cod_ejecucion_modmacro = " + ejecucion.getEjecucion_modMacro().getId_ejecucion() + ";");
			//LOGGER.info("SAS ejecucion PE: Datos de primeros pasos enviados.");
		}else {
			lineasSubmit.add("%let cod_ejecucion_modmacro = -1;");
		}
		
		
		// Identificamos las tablas a nivel escenario
		if ((tipo_obtencion_resultados==1 || tipo_obtencion_resultados==2) && tipo_ejecucion==1 && flag_variaciones==0) {
			for (GrupoBloquesEscenario grupo : ejecucion.getGrupos_bloquesParamEscenario()) {
				if (grupo.getFlag_ejecutar()) {
					for (BloqueParametrizado bloqueParam : grupo.getBloquesParam_escenarioVersion()) {
						if (bloqueParam.getFlag_tabla_input()) {
							
							lineasSubmit.add("%let " + bloqueParam.getVariable_motor() + "="
									+ bloqueParam.getTabla_input().getNombre_fichero() + ";");
							lineasSubmit.add("%let " + "where_" + bloqueParam.getVariable_motor() + "="
									+ (bloqueParam.getFiltro_tabla() != null ? "(" + bloqueParam.getFiltro_tabla().replace("'", "\"") + ")" : "")
									+ ";");
							keep_bloque = "";
							for (CampoParametrizado campo : bloqueParam.getCampos_parametrizado()) {
								if (campo.getCampo_input().getNombre_campo() != null) {
									lineasSubmit.add("%let " + campo.getCampo_bloque().getVariable_motor() + "="
											+ campo.getCampo_input().getNombre_campo() + ";");
									keep_bloque = keep_bloque + campo.getCampo_input().getNombre_campo() + " ";
								} else {
									lineasSubmit.add("%let " + campo.getCampo_bloque().getVariable_motor() + "= ;");
								}
							}
							lineasSubmit.add("%let " + "keep_" + bloqueParam.getVariable_motor() + "=" + keep_bloque + ";");
						
	
							bloquesBD.add(bloqueParam);						
							
						}
					}
				}
			}
			//LOGGER.info("SAS ejecucion PE: Variables de bloque a nivel escenario enviadas.");
		}
		// Para el caso de local, creamos las tablas en SAS, dado que no tenemos el
		// interface to BD
		String nombre_tabla_fichero = "";

		// Creamos las tablas desde la BD a SAS
		ArrayList<String> listaCargas = null;
		for (BloqueParametrizado bloque_tabla : bloquesBD) {
			listaCargas=new ArrayList<String>();
			
			//validamos que exista una carga asociada
			if (bloque_tabla.getCarga()!=null) {
				//recuperamos la carga de las ejecucion actual
				listaCargas.add(bloque_tabla.getCarga().getId_cargafichero().toString());
				
				//recuperamos las cargas de las ejecuciones historicas 
				//en fase 1 para todos menos para mayoristas y refinanciados
				//en fase 2 solo para revolventes
				if ((tipo_ejecucion == 1 && flgMayorista!=1) || (tipo_ejecucion == 2 && flgRevolvente==1)) {
					if (bloque_tabla.getTipo_bloque().getId_tipo_bloque()==11 ) {
						for(EjecucionHistorica ejecucion_hist : ejecucion.getEjecuciones_hist() ) {			
							listaCargas.add(ejecucion_hist.getCargaBloqPrincipal_hist().getId_cargafichero().toString());
						}
					}
				}
				nombre_tabla_fichero = "he_" + bloque_tabla.getTabla_input().getNombre_fichero() + "_V"
						+ bloque_tabla.getTabla_input().getVersion_metadata();
				lineasSubmit.add("%CreaTablaNoNormalizada(&lib_input.," + nombre_tabla_fichero + ","
						+ bloque_tabla.getTabla_input().getNombre_fichero() + ","
						+ bloque_tabla.getTabla_input().getId_tablainput() + ","
						+ String.join("*", listaCargas) + ","
						+ "&keep_" + bloque_tabla.getVariable_motor() + ".,"
						+ "&where_" + bloque_tabla.getVariable_motor() + "."
						+ ");");
			}
		}
		//LOGGER.info("SAS ejecucion PE: creacion de tablas input enviadas.");
		
		// Comienza la ejecución, un envio por cada escenario de la cartera
		ArrayList<String> listaMacro1 = null;
		ArrayList<String> listaMacro2 = null;
		ArrayList<String> codEjecucionesExtrapol = null;
		ArrayList<String> codEjecucionesHistEntidad = null;
		ArrayList<String> codEjecucionesHistCartera = null;
		ArrayList<String> codEjecucionesHistCartera2 = null;
		ArrayList<String> codEjecucionesActual = null;
		ArrayList<String> codEjecucionesAnterior = null;
		ArrayList<String> nombreCarterasEjecucion = null;
		
		Integer codEjecucionActual = -1;
		Integer codEjecucionAnterior = -1;
		Integer codEscenario = 0;
		Integer codEjecucion = 0;
		
		lineasSubmit.add("%let tbl_pe_historica_ent = TABLON_HISTORICO_E;");
		lineasSubmit.add("%let tbl_pe_historica_cart = TABLON_HISTORICO_C;");
		lineasSubmit.add("%let tbl_pe_historica_var = TABLON_HISTORICO_V;");
		lineasSubmit.add("%let tbl_pe_historica_extrapol = TABLON_HISTORICO_EXTPL;");
		lineasSubmit.add("%let tbl_pe_historica_f2 = TABLON_HISTORICO_F2;");
		lineasSubmit.add("%let tbl_pe_historica_del = TABLON_HISTORICO_DELTAS;");
		
		lineasSubmit.add("%let cmp_pe_cod_ejecucion = CODEJECUCION;");
		lineasSubmit.add("%let cmp_pe_fecha_ejecucion = FECEJECUCION;");
		lineasSubmit.add("%let cmp_pe_id_contrato = NBRCODOPERACION;");
		lineasSubmit.add("%let cmp_pe_id_cliente = NBRCODCLIENTE;");
		lineasSubmit.add("%let cmp_pe_escenario = CODESCENARIO;");
		lineasSubmit.add("%let cmp_pe_stage_mes = NUMSTAGEMES;");
		lineasSubmit.add("%let cmp_pe_stage = NUMSTAGE;");
		lineasSubmit.add("%let cmp_pe_pd = NUMPD;");
		lineasSubmit.add("%let cmp_pe_lgd = NUMLGD;");
		lineasSubmit.add("%let cmp_pe_ead = NUMEAD;");	
		lineasSubmit.add("%let cmp_pe_perdida_esperada = NUMPE;");	
		lineasSubmit.add("%let cmp_pe_saldo = NUMSALDOBALANCE;");	
		lineasSubmit.add("%let cmp_pe_saldo_contable = NUMSALDOCONTABLE;");
		lineasSubmit.add("%let cmp_pe_linea_credito = NUMLINEA;");
		lineasSubmit.add("%let cmp_pe_bucket = NUMBUCKET;");
		lineasSubmit.add("%let cmp_pe_maduracion = NUMMADURACION;");
		lineasSubmit.add("%let cmp_pe_cod_moneda = NBRCODMONEDA;");
		lineasSubmit.add("%let cmp_pe_dias_atraso = NUMDIASATRASO;");
		lineasSubmit.add("%let cmp_pe_tipo_bloque_may = NBRTIPOBLOQUEMAY;");
		lineasSubmit.add("%let cmp_pe_tipo_banca_may = NBRTIPOBANCA;");
		lineasSubmit.add("%let cmp_pe_tipo_plazo_may = NBRTIPOPLAZO;");
		lineasSubmit.add("%let cmp_pe_regla_principal = NBRREGLAPRINCIPALSTAGE;");
		
		lineasSubmit.add("%let keep_historica = x;");
		//lineasSubmit.add("%let keep_historica = CODEJECUCION FECEJECUCION NBRCODOPERACION NBRCODCLIENTE CODESCENARIO NUMSTAGEMES NUMSTAGE NUMPD NUMLGD NUMEAD NUMPE NUMSALDOBALANCE NUMSALDOCONTABLE NUMLINEA NUMBUCKET NUMMADURACION NBRREGLAPRINCIPALSTAGE;");			
		
		
		//nos aseguramos de que se ejecuten en orden de los escenarios!!!
		ArrayList<Escenario> escenarios = md.obtenerEscenarios();
		
		for (Escenario escenario : escenarios) {
			for (GrupoBloquesEscenario grupo : ejecucion.getGrupos_bloquesParamEscenario()) {
				
				codEscenario= grupo.getEscenario_version().getEscenario().getId_escenario();
				
				if (codEscenario.equals(escenario.getId_escenario())) {
				
					lineasSubmit.add("%let codigo_escenario = " + codEscenario + ";");
					lineasSubmit.add("%let escenario = " + grupo.getEscenario_version().getEscenario().getNombre_variable_motor() + ";");
					lineasSubmit.add("%let codigo_escenario_version = " + grupo.getEscenario_version().getId_escenario_version() + ";");
					lineasSubmit.add("%let peso_escenario = " + grupo.getEscenario_version().getPeso() + ";");
					
					codEjecucion = grupo.getId_ejecucion();
					lineasSubmit.add("%let codigo_ejecucion = " + codEjecucion + ";");
					
					lineasSubmit.add("%put Inicio de la ejecucion del escenario &escenario. para la cartera &cartera. ;");
					
					
					//Recuperamos la ejecucion anterior y actual (en caso de variaciones)
					
					//para distinguir variaciones
					if (tipo_ejecucion==1 && flag_variaciones==0) {
						lineasSubmit.add("%let flag_modelo_macro = 1;");
					}else if (tipo_ejecucion==2 && flag_variaciones==0)  {
						lineasSubmit.add("%let flag_modelo_macro = 0;");
					}
					if (flag_variaciones==1) {
						lineasSubmit.add("%let flag_modelo_macro = 0;");
						//actualizar con el mes que llega de variaciones
						fechaAnterior=ejecucion.getCodmesAnt_variacion().toString();
					}
					
					//Si no es variaciones, el cod ejecucion actual es el que esta en ejecucion
					if (flag_variaciones==0) {
						lineasSubmit.add("%let codEjecucionActual = " + grupo.getId_ejecucion() + ";");
					}
					
					// Si es variaciones, recuperamos las ejecuciones asociadas anteriores
					if(tipo_ejecucion==1 && flag_variaciones==1) {
						
						codEjecucionActual = -1;
						codEjecucionAnterior = -1;
						
						//recuperamos la ejecucion fase 1 asociada actual
						for(MapeoCargasModMacEscVer ejec_f1: md.obtenerEjecucionesOficialEscVerFase1(Integer.valueOf(fechaActual), codcartera)) {
							if(ejec_f1.getEscenario_version().getEscenario().getId_escenario().equals(codEscenario)) {
									codEjecucionActual=ejec_f1.getId_ejecucion();
							}
						}
						
						lineasSubmit.add("%let codEjecucionActual = " + codEjecucionActual + ";");
						
						//recuperamos la ejecucion fase 1 asociada del periodo anterior
						for(MapeoCargasModMacEscVer ejec_f1: md.obtenerEjecucionesOficialEscVerFase1(Integer.valueOf(fechaAnterior), codcartera)) {
							if(ejec_f1.getEscenario_version().getEscenario().getId_escenario().equals(codEscenario)) {
									codEjecucionAnterior=ejec_f1.getId_ejecucion();
							}
						}
						lineasSubmit.add("%let codEjecucionAnterior = " + codEjecucionAnterior +";");	
						
						//información historica de resultados
						lineasSubmit.add("%CreaTablaHistorica(&lib_input., &tbl_pe_historica_var.,"
								+ codEjecucionAnterior+"*"+codEjecucionActual  + ",&keep_historica.);");
						
						//LOGGER.info("SAS ejecucion PE: Tablas historicas para variaciones enviadas.");
						
					}
					else if(tipo_ejecucion==2 && flag_variaciones==1) {
						
						codEjecucionActual = -1;
						codEjecucionAnterior = -1;
						codEjecucionesActual = new ArrayList<String>();
						codEjecucionesAnterior = new ArrayList<String>();
						nombreCarterasEjecucion = new ArrayList<String>();
						
						//recuperamos la ejecucion fase 2 asociada actual
						for(MapeoCargasModMacEscVer ejec_f2: md.obtenerEjecucionesOficialEscVerFase2(Integer.valueOf(fechaActual), codcartera)) {
							if(ejec_f2.getEscenario_version().getEscenario().getId_escenario().equals(codEscenario)) {
									codEjecucionActual=ejec_f2.getId_ejecucion();
							}
						}
						lineasSubmit.add("%let codEjecucionActual = " + codEjecucionActual + ";");
						
						if (flag_regulatorio==1) {
							for (Cartera cart : md.obtenerCarterasPorIdEntidad(ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia().
									getCartera().getEntidad().getId_entidad())) {
								codEjecucionActual=-1;
								for(MapeoCargasModMacEscVer ejec_f2: md.obtenerEjecucionesOficialEscVerFase2(Integer.valueOf(fechaActual), cart.getId_cartera())) {
									if(ejec_f2.getEscenario_version().getEscenario().getId_escenario().equals(codEscenario)) {
										codEjecucionActual=ejec_f2.getId_ejecucion();
									}
								}
								codEjecucionesActual.add(codEjecucionActual.toString());
								nombreCarterasEjecucion.add(cart.getNombre_variablemotor());
							}
							lineasSubmit.add("%let lista_ejec_actual = " + String.join("*", codEjecucionesActual) + " ;");
							lineasSubmit.add("%let lista_cartera = " + String.join("*", nombreCarterasEjecucion) + " ;");
						}
						
						
						//recuperamos la ejecucion fase 2 asociada del periodo anterior
						for(MapeoCargasModMacEscVer ejec_f2: md.obtenerEjecucionesOficialEscVerFase2(Integer.valueOf(fechaAnterior), codcartera)) {
							if(ejec_f2.getEscenario_version().getEscenario().getId_escenario().equals(codEscenario)) {
									codEjecucionAnterior=ejec_f2.getId_ejecucion();
							}
						}
						lineasSubmit.add("%let codEjecucionAnterior = " + codEjecucionAnterior +";");	
						
						if (flag_regulatorio==1) {
							for (Cartera cart : md.obtenerCarterasPorIdEntidad(ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia().
									getCartera().getEntidad().getId_entidad())) {
								codEjecucionAnterior=-1;
								for(MapeoCargasModMacEscVer ejec_f2: md.obtenerEjecucionesOficialEscVerFase2(Integer.valueOf(fechaAnterior), cart.getId_cartera())) {
									if(ejec_f2.getEscenario_version().getEscenario().getId_escenario().equals(codEscenario)) {
										codEjecucionAnterior=ejec_f2.getId_ejecucion();
									}
								}
								codEjecucionesAnterior.add(codEjecucionAnterior.toString());
							}
							lineasSubmit.add("%let lista_ejec_anterior = " + String.join("*", codEjecucionesAnterior) + " ;");
						}
						
						//información historica de resultados
						lineasSubmit.add("%CreaTablaHistorica(&lib_input., &tbl_pe_historica_var.,"
								+ codEjecucionAnterior+"*"+codEjecucionActual  + ",&keep_historica.);");
						
						//LOGGER.info("SAS ejecucion PE: Tablas historicas para variaciones enviadas.");
					}
					
					
					// para tipo extrapolación fase 1
					if (tipo_obtencion_resultados == 2 && tipo_ejecucion==1 && flag_variaciones==0)  {
						codEjecucionesExtrapol = new ArrayList<String>();
						
						//buscamos la ejecucion de la cartera a extrapolar
						for (EjecucionAgregadaPE ejecucionExtrapol : ejecucion.getEjecuciones_extrapolacion()) {
							if(ejecucionExtrapol!=null) {
								for (MapeoCargasModMacEscVer mapeoEscExtrap : ejecucionExtrapol.getMapeo_cargasModMac_ejecucion()
										.getMapeosCargasModMacEsc()) {
									if (mapeoEscExtrap.getEscenario_version().getEscenario().getId_escenario().equals(codEscenario)) {
										codEjecucionesExtrapol.add(mapeoEscExtrap.getId_ejecucion().toString());
									}
								}
							}
						}
						
						lineasSubmit.add("%let nro_tablas_extrapolacion= " + codEjecucionesExtrapol.size()  +  ";");
						lineasSubmit.add("%let lista_tablas_extrapolar = " + String.join("*", codEjecucionesExtrapol) + " ;");
						
						//información historica de resultados
						lineasSubmit.add("%CreaTablaHistorica(&lib_input., &tbl_pe_historica_extrapol.,"
								+ String.join("*", codEjecucionesExtrapol) + ",&keep_historica.);");
						
						//LOGGER.info("SAS ejecucion PE: Tablas historicas para variaciones extrapolacion.");
			
					}
					
					//para fase 2
					if(tipo_ejecucion==2 && flag_variaciones==0) {
						codEjecucionesHistCartera= new ArrayList<String>();
						//identificamos la ejecucion de fase 1 asociada
						for(MapeoCargasModMacEscVer ejec_f1: md.obtenerEjecucionesOficialEscVerFase1(ejecucion.getCodmes(), codcartera)) {
							if(ejec_f1.getEscenario_version().getEscenario().getId_escenario().equals(codEscenario) ) {
								codEjecucionesHistCartera.add(ejec_f1.getId_ejecucion().toString());
							}
						}
						
						//tabla fase 1 del mes
						lineasSubmit.add("%CreaTablaHistorica(&lib_input., &tbl_pe_historica_cart.,"
								+ String.join("*", codEjecucionesHistCartera) + ",&keep_historica.);");
						
						
						//identificamos las n ultimas ejecuciones de fase 2 (por defecto 12 incluyendo la actual)
						
						Integer nroMesesDelta = 11;
						lineasSubmit.add("%let nro_meses_delta = " + (nroMesesDelta + 1) + ";");
						
						c.set(Integer.valueOf(fechaActual.substring(0, 4)), Integer.valueOf(fechaActual.substring(4, 6)) - 1, 1);
						codEjecucionesHistCartera2= new ArrayList<String>();
						
						for (int ff=1; ff<=nroMesesDelta; ff++) {
							c.add(Calendar.MONTH, -1);
							for(MapeoCargasModMacEscVer ejec_f2: md.obtenerEjecucionesOficialEscVerFase2(c.get(Calendar.YEAR) * 100 + (c.get(Calendar.MONTH) + 1), codcartera)) {
								if(ejec_f2.getEscenario_version().getEscenario().getId_escenario().equals(codEscenario) ) {
									codEjecucionesHistCartera2.add(ejec_f2.getId_ejecucion().toString());
								}
							}
						}
						
						//información historica de deltas
						lineasSubmit.add("%CreaTablaHistoricaDeltas(&lib_input., &tbl_pe_historica_del.,"
								+ String.join("*", codEjecucionesHistCartera2) +");");
						
						//tabla fase 2 anterior
						if(codEjecucionesHistCartera2.size()>0) {
							lineasSubmit.add("%CreaTablaHistorica(&lib_input., &tbl_pe_historica_f2.,"
								+ String.join("*", codEjecucionesHistCartera2.get(0)) + ",&keep_historica.);");
						}else {
							lineasSubmit.add("%CreaTablaHistorica(&lib_input., &tbl_pe_historica_f2.,"
									+ String.join("*", codEjecucionesHistCartera2) + ",&keep_historica.);");
						}
						
						//LOGGER.info("SAS ejecucion PE: Tablas historicas para fase 2 enviadas.");
						
					}
					
					//para modelo interno fase 1 o para variaciones
					if(tipo_ejecucion==1 || flag_variaciones==1 ) {
						
						codEjecucionesHistEntidad= new ArrayList<String>();
						codEjecucionesHistCartera= new ArrayList<String>();
						
						//tabla historica 
						
						for (String mes_busqueda : mesesCura) {
							for (Cartera cart : md.obtenerCarterasPorIdEntidad(ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia().
									getCartera().getEntidad().getId_entidad())) {
								for(MapeoCargasModMacEscVer ejec_f1: md.obtenerEjecucionesOficialEscVerFase1(Integer.valueOf(mes_busqueda), cart.getId_cartera())) {
									if(ejec_f1.getEscenario_version().getEscenario().getId_escenario().equals(codEscenario)) {
										codEjecucionesHistEntidad.add(ejec_f1.getId_ejecucion().toString());
										
										if(cart.getId_cartera().equals(codcartera)) {
											codEjecucionesHistCartera.add(ejec_f1.getId_ejecucion().toString());
										}
									}
								}
							}
						}
										
						
						lineasSubmit.add("%CreaTablaHistorica(&lib_input., &tbl_pe_historica_ent.,"
								+ String.join("*", codEjecucionesHistEntidad) + "," + "&keep_historica.);");
						lineasSubmit.add("%CreaTablaHistorica(&lib_input., &tbl_pe_historica_cart.,"
								+ String.join("*", codEjecucionesHistCartera) + "," + "&keep_historica.);");
						
						
						//LOGGER.info("SAS ejecucion PE: Tablas historicas para calculo regular enviadas.");
						
						//informacion de la macro, tanto de variaciones como de flujo normal
						listaMacro1 = new ArrayList<String>();
						//información de la macro (calculada o manual)
						if(ejecucion.getTipo_ajusteManual()!=-1 && grupo.getAjustesPdProy()!=null) {					
							for (AjustePdProyec ajustePd : grupo.getAjustesPdProy()) {
								listaMacro1.add(String.valueOf(ajustePd.getPd_final()));
							}
						}
											
						if(flag_variaciones==1) { 
							if (listaMacro1.size()>0) {
								lineasSubmit.add("%let pd_var_macro = " + listaMacro1.get(0) +  ";");
							}else {
								lineasSubmit.add("%let pd_var_macro = 0 ;");
							}
						}
						//información de la macro (calculada)
						else if(ejecucion.getTipo_ajusteManual()==0 && flag_CFSF==0 ) {
							lineasSubmit.add("%let tbl_rd_app = rd_app;");
							lineasSubmit.add("%CreaTablaRD(&lib_input.,&tbl_rd_app.,&cod_ejecucion_modmacro.);");
							lineasSubmit.add("%let lista_pd_promedio_&flag_modelo_macro._&escenario. = "
									+ String.join("*", listaMacro1) + " ;");
						}
						
						//información de la macro (calculada y duplicada)
						else if(ejecucion.getTipo_ajusteManual()==0 && flag_CFSF==1 ) {
							lineasSubmit.add("%let tbl_rd_app = rd_app;");
							lineasSubmit.add("%CreaTablaRD(&lib_input.,&tbl_rd_app.,&cod_ejecucion_modmacro.);");
							
							int tamPD=listaMacro1.size()/2;
							listaMacro2 = new ArrayList<String>();
							for (int i=1; i<=tamPD; i++) {
								listaMacro2.add(listaMacro1.get(i-1));
							}
							lineasSubmit.add("%let lista_pd_promedio_CF_&flag_modelo_macro._&escenario. = "
									+ String.join("*", listaMacro2) + " ;");
							lineasSubmit.add("%let marca_filtro_CF = 1;");
		
							listaMacro2 = new ArrayList<String>();
							for (int i=tamPD+1; i<=listaMacro1.size(); i++) {
								listaMacro2.add(listaMacro1.get(i-1));
							}
							lineasSubmit.add("%let lista_pd_promedio_SF_&flag_modelo_macro._&escenario. = "
									+ String.join("*", listaMacro2) + " ;");
							lineasSubmit.add("%let marca_filtro_SF = 0;");
							
						}
						
						//información de la macro (manual)
						else if(ejecucion.getTipo_ajusteManual()==1) {
								lineasSubmit.add("%let lista_pd_promedio_&flag_modelo_macro._&escenario. = "
										+ String.join("*", listaMacro1) + " ;");
							}
						//información de la macro (duplicada)
						else if(ejecucion.getTipo_ajusteManual()==2) {
							int tamPD=listaMacro1.size()/2;
							listaMacro2 = new ArrayList<String>();
							for (int i=1; i<=tamPD; i++) {
								listaMacro2.add(listaMacro1.get(i-1));
							}
							lineasSubmit.add("%let lista_pd_promedio_CF_&flag_modelo_macro._&escenario. = "
									+ String.join("*", listaMacro2) + " ;");
							lineasSubmit.add("%let marca_filtro_CF = 1;");
		
							listaMacro2 = new ArrayList<String>();
							for (int i=tamPD+1; i<=listaMacro1.size(); i++) {
								listaMacro2.add(listaMacro1.get(i-1));
							}
							lineasSubmit.add("%let lista_pd_promedio_SF_&flag_modelo_macro._&escenario. = "
									+ String.join("*", listaMacro2) + " ;");
							lineasSubmit.add("%let marca_filtro_SF = 0;");
							
						}	
						//LOGGER.info("SAS ejecucion PE: Información del modelo macro enviadas.");
						
						//variables solo aplicables para  mayoristas
						if (flgMayorista==1) {
							lineasSubmit.add("%let lista_clase = no*pp*de*du*pe;");
							lineasSubmit.add("%let lista_clase_porcentaje =	0.3*0.3*0.3*0.5*1;");
							lineasSubmit.add("%let lista_clase_capex = 0.5*0.5*0.5*0.25*0;");
							lineasSubmit.add("%let lista_clase_anios = 4*4*4*2*0;");
							lineasSubmit.add("%let lista_rating	= AAA+*AAA*AAA-*AA*A*BBB+*BBB*BB+*BB*B+*B*CCC*CC*C*DDD*DD*D*EEE*EE*F;");
							lineasSubmit.add("%let lista_rating_valor = 1*2*3*4*5*6*7*8*9*10*11*12*13*14*15*16*17*18*19*20;");
							lineasSubmit.add("%let lista_rating_f = 1*2*3*4*5*6*7;");
							lineasSubmit.add("%let lista_umbral_rati = 0*6*8*10*12*15*18*20;");
						}
							
					}
										
//					// ejecutamos parcialmente cada escenario
//					sasLanguage.Submit("proc sql;");
//					for (int i = 0; i < lineasSubmit.size(); i++) {
//						sasLanguage.Submit("insert into inicializacionJavaPrev values (\'" + lineasSubmit.get(i) + "\');");
//					}
//					sasLanguage.Submit("quit;");
//					
//					lineasSubmitArr = new String[lineasSubmit.size()];
//					lineasSubmitArr = lineasSubmit.toArray(lineasSubmitArr);
//					sasLanguage.SubmitLines(lineasSubmitArr);
//					lineasSubmitArr = null;
//					lineasSubmit = new ArrayList<String>();
					
//					if(codEscenario.equals(1)) {
//						// tabla para el pintado del codigo en formato *.SAS
//						sasLanguage.Submit("%crearFicheroSAS(&ruta_log.,inicializacionJavaPrev);");
//					}
//					
//					// lanzamiento del calculo
//					sasLanguage.Submit("%flujo();");
//					
//					//volcamos el log al ultimo codejecucion enviado
//					logHldr = new StringSeqHolder();
//					sasLanguage.FlushLogLines(Integer.MAX_VALUE,new CarriageControlSeqHolder(),new LineTypeSeqHolder(),logHldr);
//					// display log file
//					logLines = logHldr.value;
//					LOGGER.info("procesando Log ejecucion " + codEjecucion);
//					md.guardarLogEjecucionPE(logLines, codEjecucion);
				
					//salimos del bucle
					break;
				}
			}
		}
		

//		if (SAS_local.equalsIgnoreCase("SI")) {
//			sasLanguage.Submit("%exportarFichero(&ruta_log.,HE_OutputProvisionesCred,&lib_input.);");
//		}
//
//		// desasignamos las librerias
//		sasLanguage.Submit("libname sche1 clear;");
//		sasLanguage.Submit("libname sche2 clear;");
//		sasLanguage.Submit("libname &lib_input. clear;");

		if (SAS_local.equalsIgnoreCase("SI")) {
			//sasLanguage.Submit("%limpiarTablasVariables(&lib_input.);");
		} else {
			//sasLanguage.Submit("%limpiarTablasVariables();");
		}


		if (SAS_local.equalsIgnoreCase("SI")) {
			//md.leerCSV(ruta_bd, "HE_OutputProvisionesCred", "HE_OutputProvisionesCred", true);
		}


//		// cerramos la conexion y actualizamos el estado a "finalizado"
//		LOGGER.info("Submit ejecucion completado, se cierra workspace " + iWorkspace.UniqueIdentifier());
//		cx.close();

	}

	private String transformarReglasPrelacion(Regla_Staging regla_actual,
			HashMap<String, Expresion_Staging> hm_expresiones, HashMap<Integer, Regla_Staging> hm_reglas,
			HashMap<Integer, Stage> hm_stages) {

		String formula_general = "if <<r>> then do %str(;) <<s>> end %str(;) else do %str(;) <<n>> end %str(;)";
		String formula_iterativa = "";
		String formula_interna = "";
		Regla_Staging regla_nodo = null;
		Stage stage_nodo = null;

		String[] formula_string_array = regla_actual.getFormula_input().split("\'");
		String[] formula_space_array = null;

		ArrayList<String> formulaArray = new ArrayList<String>();

		for (int i = 0; i < formula_string_array.length; i++) {
			if (i % 2 == 0) {
				formula_space_array = formula_string_array[i].split(" ");
				for (int j = 0; j < formula_space_array.length; j++) {
					formulaArray.add(formula_space_array[j]);
				}
			} else {
				formulaArray.add("\"" + formula_string_array[i] + "\"");
			}

		}

		String formula_parcial = "";
		for (int i = 0; i < formulaArray.size(); i++) {
			if (hm_expresiones.containsKey(formulaArray.get(i))) {
				formula_parcial = hm_expresiones.get(formulaArray.get(i)).getFormula_input();
			} else {
				formula_parcial = formulaArray.get(i);
				switch (formula_parcial) {
				case "==":
					formula_parcial = "eq";
					break;
				case "!=":
					formula_parcial = "ne";
					break;
				case "<":
					formula_parcial = "lt";
					break;
				case "<=":
					formula_parcial = "le";
					break;
				case ">":
					formula_parcial = "gt";
					break;
				case ">=":
					formula_parcial = "ge";
					break;
				case "!":
					formula_parcial = "not";
					break;
				case "&":
					formula_parcial = "and";
					break;
				case "|":
					formula_parcial = "or";
					break;
				}
				if (formula_parcial.contains("round(")) {
					formula_parcial = formula_parcial.replace(")", ",1)");
				}
				if (formula_parcial.contains("mod(")) {
					formula_parcial = formula_parcial.replace(")", ",1)");
				}
				if (formula_parcial.contains("concat(")) {
					formula_parcial = formula_parcial.replace("concat(", ",cat(");
				}
			}
			formula_interna = formula_interna + " " + formula_parcial;
		}

		formula_general = formula_general.replace("<<r>>", formula_interna);

		if (regla_actual.getFlag_stage_nodo_cumple()) {
			stage_nodo = hm_stages.get(regla_actual.getId_nodo_cumple());
			formula_general = formula_general.replace("<<s>>", "stage_ifrs = " + stage_nodo.getId_stage() + " %str(;)");
		} else {
			regla_nodo = hm_reglas.get(regla_actual.getId_nodo_cumple());
			formula_iterativa = transformarReglasPrelacion(regla_nodo, hm_expresiones, hm_reglas, hm_stages);
			formula_general = formula_general.replace("<<s>>", formula_iterativa);
		}

		if (regla_actual.getFlag_stage_nodo_no_cumple()) {
			stage_nodo = hm_stages.get(regla_actual.getId_nodo_no_cumple());
			formula_general = formula_general.replace("<<n>>", "stage_ifrs = " + stage_nodo.getId_stage() + " %str(;)");
		} else {
			regla_nodo = hm_reglas.get(regla_actual.getId_nodo_no_cumple());
			formula_iterativa = transformarReglasPrelacion(regla_nodo, hm_expresiones, hm_reglas, hm_stages);
			formula_general = formula_general.replace("<<n>>", formula_iterativa);
		}

		return formula_general;
	}

	private String transformarReglasCaePor(Regla_Staging regla_actual, String nombre_regla,
			HashMap<String, Expresion_Staging> hm_expresiones) {

		String formula_general = "if <<r>> then do %str(;) <<s>> end %str(;) else do %str(;) <<n>> end %str(;)";
		String formula_interna = "";

		String[] formula_string_array = regla_actual.getFormula_input().split("\'");
		String[] formula_space_array = null;

		ArrayList<String> formulaArray = new ArrayList<String>();

		for (int i = 0; i < formula_string_array.length; i++) {
			if (i % 2 == 0) {
				formula_space_array = formula_string_array[i].split(" ");
				for (int j = 0; j < formula_space_array.length; j++) {
					formulaArray.add(formula_space_array[j]);
				}
			} else {
				formulaArray.add("\"" + formula_string_array[i] + "\"");
			}

		}

		String formula_parcial = "";
		for (int i = 0; i < formulaArray.size(); i++) {
			if (hm_expresiones.containsKey(formulaArray.get(i))) {
				formula_parcial = hm_expresiones.get(formulaArray.get(i)).getFormula_input();
			} else {
				formula_parcial = formulaArray.get(i);
				switch (formula_parcial) {
				case "==":
					formula_parcial = "eq";
					break;
				case "!=":
					formula_parcial = "ne";
					break;
				case "<":
					formula_parcial = "lt";
					break;
				case "<=":
					formula_parcial = "le";
					break;
				case ">":
					formula_parcial = "gt";
					break;
				case ">=":
					formula_parcial = "ge";
					break;
				case "!":
					formula_parcial = "not";
					break;
				case "&":
					formula_parcial = "and";
					break;
				case "|":
					formula_parcial = "or";
					break;
				}
				if (formula_parcial.contains("round(")) {
					formula_parcial = formula_parcial.replace(")", ",1)");
				}
				if (formula_parcial.contains("mod(")) {
					formula_parcial = formula_parcial.replace(")", ",1)");
				}
				if (formula_parcial.contains("concat(")) {
					formula_parcial = formula_parcial.replace("concat(", ",cat(");
				}
			}
			formula_interna = formula_interna + " " + formula_parcial;
		}

		formula_general = formula_general.replace("<<r>>", formula_interna);
		formula_general = formula_general.replace("<<s>>",
				nombre_regla + "=" + "\"" + regla_actual.getNombre_regla() + "\"" + " %str(;) ");
		formula_general = formula_general.replace("<<n>>", nombre_regla + "=" + "\"" + "\"" + " %str(;) ");

		return formula_general;
	}

	public void ejecutarPrimerPaso(EjecucionPrimerPasoMotor ejecucion) throws Exception {

		Calendar c = Calendar.getInstance();
		ArrayList<String> lineasSubmit = new ArrayList<String>();
		
		String tiempo_actual = String.valueOf(c.get(Calendar.YEAR)) + ".";
		tiempo_actual += String.valueOf((c.get(Calendar.MONTH) + 1)) + ".";
		tiempo_actual += String.valueOf(c.get(Calendar.DAY_OF_MONTH)) + "_";
		tiempo_actual += String.valueOf(c.get(Calendar.HOUR_OF_DAY)) + ".";
		tiempo_actual += String.valueOf(c.get(Calendar.MINUTE)) + ".";
		tiempo_actual += String.valueOf(c.get(Calendar.SECOND));

		String nombreEntidad = ejecucion.getMetodologia().getCartera().getEntidad().getNombre_variablemotor();
		String nombreCartera = ejecucion.getMetodologia().getCartera().getNombre_variablemotor();

		String nombre_carpeta_log = "log_" + tiempo_actual;
		String nombre_carpeta_cartera = "log_" + nombreCartera;
		String nombre_carpeta_entidad = "log_" + nombreEntidad;
		String ruta_bd = "";

		// obtenemos la conexion
//		if (cxfConfig == null || cxf == null) {
//			cxfConfig = new ManualConnectionFactoryConfiguration(new BridgeServer(classID, SAS_host, Integer.valueOf(SAS_port)));
//			cxfManager = new ConnectionFactoryManager();
//			cxf = cxfManager.getFactory(cxfConfig);
//		}
//
//		ConnectionInterface cx = null;
//		try {
//			cx = cxf.getConnection(SAS_userName, SAS_password);
//		} catch (ConnectionFactoryException cfe) {
//			LOGGER.error("Error creando conexión SAS:" + cfe.getMessage());
//		}
//
//		IWorkspace iWorkspace = IWorkspaceHelper.narrow(cx.getObject());
//		LOGGER.info("Workspace SAS ejecucion Primer paso creado: " + iWorkspace.UniqueIdentifier());
//
//		ILanguageService sasLanguage = iWorkspace.LanguageService();
//
		// comienzo de la creacion del codigo SAS inicialización
		ArrayList<BloqueParametrizado> bloquesBD = new ArrayList<BloqueParametrizado>();

//		// indica si se pintara el log completo
//		if (!SAS_log_completo.equalsIgnoreCase("SI")) {
//			sasLanguage.Submit("options nonotes nosource errors=1;");
//		}
//		sasLanguage.Submit("proc printto log=log; run;");
//
//		// tabla en donde se guarda temporalmente las lineas para el codigo
//		// inicializacion.sas
//		sasLanguage.Submit("%let comilla=%str(%\');");
//		sasLanguage.Submit("proc sql; create table inicializacionJavaPrev (linea varchar(2000)); quit;");
//
//		// ruta de los codigos
//		sasLanguage.Submit("%let slash=%str(" + SAS_separador_ruta + ");");
		lineasSubmit.add("%let slash=%str(" + SAS_separador_ruta + ");");
		ruta_bd = ejecucion.getPrimerPaso().getCodigo_primerPaso();
		lineasSubmit.add("%let ruta_codigos=" + ruta_bd + ";");

		// variables generales con el nombre de la entidad y cartera
		lineasSubmit.add("%let entidad = " + nombreEntidad + ";");
		lineasSubmit.add("%let codigo_entidad = " + ejecucion.getMetodologia().getCartera().getEntidad().getId_entidad() + ";");
		lineasSubmit.add("%let cartera = " + nombreCartera + ";");
		Integer codcartera = ejecucion.getMetodologia().getCartera().getId_cartera();
		lineasSubmit.add("%let codigo_cartera = " + codcartera + ";");

		// fecha de ejecución
		String fechaActual = ejecucion.getCodmes().toString();
		String fechaAntAnho = String.valueOf(ejecucion.getCodmes() - 100);
		lineasSubmit.add("%let fecha_ejecucion = " + fechaActual + ";");
		lineasSubmit.add("%let fecha_referencia = " + fechaAntAnho + ";");
		Integer cod_ejecucion=ejecucion.getId_ejecucion();
		lineasSubmit.add("%let codigo_ejecucion = " + cod_ejecucion+ ";");

		// creamos la jerarquia de carpetas para el log
		
		lineasSubmit.add("%let carpeta_sas=&ruta_codigos.&slash.;");
		lineasSubmit.add("%let nombre_codigo_inicial= " + SAS_codigoInicial + ";");
//		
//		sasLanguage.Submit("%let nombre_raiz=" + ruta_bd + ";");
//		sasLanguage.Submit("%let nombre_carpeta_entidad=" + nombre_carpeta_entidad + ";");
//		sasLanguage.Submit("%let string_nombre_carpeta_entidad=%sysfunc(Trim(%Str(&comilla.)&nombre_carpeta_entidad.%Str(&comilla.)));");
//		sasLanguage.Submit("%let string_nombre_raiz=%sysfunc(Trim(%Str(&comilla.)&nombre_raiz.%Str(&comilla.)));");
//		sasLanguage.Submit("data _null_;ruta_out=dcreate(&string_nombre_carpeta_entidad.,&string_nombre_raiz.); run;");
//
//		sasLanguage.Submit("%let nombre_carpeta_cartera=" + nombre_carpeta_cartera + ";");
//		sasLanguage.Submit("%let string_nombre_carpeta_cartera=%sysfunc(Trim(%Str(&comilla.)&nombre_carpeta_cartera.%Str(&comilla.)));");
//		sasLanguage.Submit("%let string_nombre_raiz=%sysfunc(Trim(%Str(&comilla.)&nombre_raiz.&slash.&nombre_carpeta_entidad.%Str(&comilla.)));");
//		sasLanguage.Submit("data _null_;ruta_out=dcreate(&string_nombre_carpeta_cartera.,&string_nombre_raiz.); run;");
//
//
//		sasLanguage.Submit("%let nombre_carpeta_log=" + nombre_carpeta_log + ";");
//		sasLanguage.Submit("%let string_nombre_carpeta_log=%sysfunc(Trim(%Str(&comilla.)&nombre_carpeta_log.%Str(&comilla.)));");
//		sasLanguage.Submit("%let string_nombre_raiz=%sysfunc(Trim(%Str(&comilla.)&nombre_raiz.&slash.&nombre_carpeta_entidad.&slash.&nombre_carpeta_cartera.%Str(&comilla.)));");
//		sasLanguage.Submit("data _null_;ruta_out=dcreate(&string_nombre_carpeta_log.,&string_nombre_raiz.); run;");
//
//		sasLanguage.Submit("%let ruta_log=&nombre_raiz.&slash.&nombre_carpeta_entidad.&slash.&nombre_carpeta_cartera.&slash.&nombre_carpeta_log.;");
//		sasLanguage.Submit("%let string_ruta_log=%sysfunc(Trim(%Str(&comilla.)&ruta_log.%Str(&comilla.)));");
//		sasLanguage.Submit("%put Ruta Log creada: &string_ruta_log.;");
		
		//sasLanguage.Submit("%let archivo_log=%sysfunc(Trim(%Str(&comilla.)&ruta_log.&slash.Log_" + nombreCartera +  "_" + fechaActual + ".log%Str(&comilla.)));");
		//sasLanguage.Submit("filename ifrs9_L &archivo_log.;");
		//sasLanguage.Submit("proc printto log=ifrs9_L; RUN;");

		// libname a la base de datos
		if (SAS_local.equalsIgnoreCase("SI")) {

			if (SAS_local.equalsIgnoreCase("SI")) {
				ruta_bd = ruta_bd + SAS_separador_ruta + nombre_carpeta_entidad + SAS_separador_ruta
						+ nombre_carpeta_cartera + SAS_separador_ruta + nombre_carpeta_log + SAS_separador_ruta;
			}
			lineasSubmit.add("Libname mymdb &string_ruta_log.;");

		} else {
			lineasSubmit.add("%let usuario_oracle=%sysfunc(Trim(%Str(&comilla.)" + SAS_JDBC_userName + "%Str(&comilla.)));");
			lineasSubmit.add("%let contrasena_oracle=%sysfunc(Trim(%Str(&comilla.)" + SAS_JDBC_encriptado + "%Str(&comilla.)));");
			String path_oracle = "(DESCRIPTION = (ADDRESS = (PROTOCOL=" + SAS_JDBC_protocol + ") (HOST=" + SAS_JDBC_host
					+ ") (PORT=" + SAS_JDBC_port + ")) (CONNECT_DATA = (SERVER = " + SAS_JDBC_serverType
					+ ") (SERVICE_NAME = " + SAS_JDBC_serviceName + ")))";
			lineasSubmit.add("%let path_oracle=%sysfunc(Trim(%Str(&comilla.)" + path_oracle + "%Str(&comilla.)));");
			lineasSubmit.add("Libname sche1 oracle user=&usuario_oracle. encriptado=&contrasena_oracle. path=&path_oracle. schema=&usuario_oracle;");
			lineasSubmit.add("Libname sche2 oracle user=&usuario_oracle. encriptado=&contrasena_oracle. path=&path_oracle. schema="
							+ SAS_JDBC_schema + ";");
			lineasSubmit.add("Libname mymdb (sche1 sche2);");
		}
		lineasSubmit.add("%put libreria mymdb creada;");
		lineasSubmit.add("%let lib_input = mymdb;");
		lineasSubmit.add("%let lib_bases = work;");
		lineasSubmit.add("%let lib_salida = mymdb;");

		// ejecutamos parcialmente, para asegurarnos que se creen las carpetas
		// requeridas

//		sasLanguage.Submit("proc sql;");
//		for (int i = 0; i < lineasSubmit.size(); i++) {
//			sasLanguage.Submit("insert into inicializacionJavaPrev values (\'" + lineasSubmit.get(i) + "\');");
//		}
//		sasLanguage.Submit("quit;");
//		
//		lineasSubmitArr = new String[lineasSubmit.size()];
//		lineasSubmitArr = lineasSubmit.toArray(lineasSubmitArr);
//		sasLanguage.SubmitLines(lineasSubmitArr);
//			
		
		lineasSubmit = new ArrayList<String>();
		
		Integer lifeTime=0;

		lifeTime = ejecucion.getMetodologia().getLife_time();
		lineasSubmit.add("%let lifetime = " + lifeTime + ";");
		lineasSubmit.add("%let periodicidad = " + ejecucion.getMetodologia().getPeriodicidad().getValor_periodicidad() + ";");
		lineasSubmit.add("%let maximo_plazo = "	+ ejecucion.getMetodologia().getNro_maxPlazo()	+ ";");
	
		lineasSubmit.add("%let flag_minorista = " + (ejecucion.getMetodologia().getCartera().getTipo_magnitud().getId_tipo_magnitud() == 2 ? 1 : 0) + ";");
		
		Integer flgMayorista = (ejecucion.getMetodologia().getCartera().getTipo_magnitud().getId_tipo_magnitud() == 1 ? 1 : 0);
		lineasSubmit.add("%let flag_mayorista = " + flgMayorista + ";");
		
		Integer flgRevolvente = (ejecucion.getMetodologia().getCartera().getTipo_cartera().getId_tipo_cartera() == 1 ? 1 : 0);
		lineasSubmit.add("%let flag_revolvente = " + flgRevolvente + ";");
		
		lineasSubmit.add("%let flag_hipotecas = " + (ejecucion.getMetodologia().getCartera().getTipo_cartera().getId_tipo_cartera() == 2 ? 1 : 0) + ";");
		
		Integer flgRefinanciado = (ejecucion.getMetodologia().getCartera().getTipo_cartera().getId_tipo_cartera() == 3 ? 1 : 0);
		lineasSubmit.add("%let flag_refinanciado = " + flgRefinanciado + ";");
		
		lineasSubmit.add("%let flag_consumo = " + (ejecucion.getMetodologia().getCartera().getTipo_cartera().getId_tipo_cartera() == 4 ? 1 : 0) + ";");
		
		lineasSubmit.add("%let flag_bolsonPyme = " + (ejecucion.getMetodologia().getCartera().getTipo_cartera().getId_tipo_cartera() == 5 ? 1 : 0) + ";");
		
		lineasSubmit.add("%let flag_pd_6meses = " + (ejecucion.getMetodologia().getCartera().getFlag_pd6meses() ? 1 : 0) + ";");
			
		// nombres de tablas del modelo de datos a usar;
		lineasSubmit.add("%let HE_Ejecucion = HE_Ejecucion;");

		// Bloques
		String keep_bloque = "";
		for (BloqueParametrizado bloque : ejecucion.getBloquesParam()) {
			// bloques con la información de tablas
			if (bloque.getFlag_tabla_input()) {
				lineasSubmit.add("%let " + bloque.getVariable_motor() + "=" + bloque.getTabla_input().getNombre_fichero() + ";");
				lineasSubmit.add("%let " + "where_" + bloque.getVariable_motor() + "="
						+ (bloque.getFiltro_tabla() != null ? "(" + bloque.getFiltro_tabla().replace("'", "\"") + ")" : "")
						+ ";");
				keep_bloque = "";
				for (CampoParametrizado campo : bloque.getCampos_parametrizado()) {
					if (campo.getCampo_input().getNombre_campo() != null) {
						lineasSubmit.add("%let " + campo.getCampo_bloque().getVariable_motor() + "="
								+ campo.getCampo_input().getNombre_campo() + ";");
						keep_bloque = keep_bloque + campo.getCampo_input().getNombre_campo() + " ";
					} else {
						lineasSubmit.add("%let " + campo.getCampo_bloque().getVariable_motor() + "= ;");
					}
				}
				lineasSubmit.add("%let " + "keep_" + bloque.getVariable_motor() + "=" + keep_bloque + ";");

				// guardamos los bloques normalizados
				bloquesBD.add(bloque);


				// bloques con la información manual
			} else {
				for (CampoParametrizado campo : bloque.getCampos_parametrizado()) {
					if (campo.getCampo_bloque().getTipo_campo().getId_tipo_campo() == 1) {

						if (campo.getNumValor() % 1 == 0) {
							lineasSubmit.add("%let " + campo.getCampo_bloque().getVariable_motor() + "="
									+ campo.getNumValor().intValue() + ";");
						} else {
							lineasSubmit.add("%let " + campo.getCampo_bloque().getVariable_motor() + "="
									+ campo.getNumValor() + ";");
						}
					} else if (campo.getCampo_bloque().getTipo_campo().getId_tipo_campo() == 2) {
						lineasSubmit.add("%let " + campo.getCampo_bloque().getVariable_motor() + "="
								+ campo.getTxtValor() + ";");
					}
				}
			}
		}

		// otras variables
		lineasSubmit.add("%let prefijo = var;");
		lineasSubmit.add("%let trimestres = 4;");
		lineasSubmit.add("%let flag_ajustado = 0;");
		lineasSubmit.add("%let lista_tablas_entrada =;");
		lineasSubmit.add("%let descripcion_ajuste = \"NA\";");

		// enlazamos al codigo de ejecucion
		lineasSubmit.add("%let ruta_inicio = %sysfunc(Trim(%Str(&comilla.)&ruta_codigos.&slash.&nombre_codigo_inicial.%Str(&comilla.)));");
		
//		
//		// ejecutamos parcialmente, para poder hacer el include correctamente
//		sasLanguage.Submit("proc sql;");
//		for (int i = 0; i < lineasSubmit.size(); i++) {
//			sasLanguage.Submit("insert into inicializacionJavaPrev values (\'" + lineasSubmit.get(i) + "\');");
//		}
//		sasLanguage.Submit("quit;");
//		
//		lineasSubmitArr = new String[lineasSubmit.size()];
//		lineasSubmitArr = lineasSubmit.toArray(lineasSubmitArr);
//		sasLanguage.SubmitLines(lineasSubmitArr);
//		lineasSubmitArr = null;
//		lineasSubmit = new ArrayList<String>();
//		
//		sasLanguage.Submit("%include &ruta_inicio / lrecl=500;");

		// Para el caso de local, creamos las tablas en SAS, dado que no tenemos el
		// interface to BD
		String nombre_tabla_fichero = "";

		// Creamos las tablas desde la BD a SAS
		ArrayList<String> listaCargas = null;
		for (BloqueParametrizado bloque_tabla : bloquesBD) {
			listaCargas=new ArrayList<String>();
			
			//recuperamos la carga de las ejecucion actual
			listaCargas.add(bloque_tabla.getCarga().getId_cargafichero().toString());
			
			nombre_tabla_fichero = "he_" + bloque_tabla.getTabla_input().getNombre_fichero() + "_V"
					+ bloque_tabla.getTabla_input().getVersion_metadata();
			
			lineasSubmit.add("%CreaTablaNoNormalizada(&lib_input.," + nombre_tabla_fichero + ","
					+ bloque_tabla.getTabla_input().getNombre_fichero() + ","
					+ bloque_tabla.getTabla_input().getId_tablainput() + ","
					+ String.join("*", listaCargas) + ","
					+ "&keep_" + bloque_tabla.getVariable_motor() + ".,"
					+ "&where_" + bloque_tabla.getVariable_motor() + "."
					+ ");");		
		}
		
		ArrayList<Escenario> escenarios = md.obtenerEscenarios();
		ArrayList<String> nombre_escenario = new ArrayList<String>();
		ArrayList<String> cod_escenario = new ArrayList<String>();
		for (Escenario escenario : escenarios) {
			nombre_escenario.add(escenario.getNombre_variable_motor());
			cod_escenario.add(escenario.getId_escenario().toString());
		}

		lineasSubmit.add("%let lista_escenarios = " + String.join("#", nombre_escenario) + ";");
		lineasSubmit.add("%let cod_escenarios = " + String.join("#", cod_escenario) + ";");
		
//		// ejecutamos parcialmente para obtener el archivo sas 
//		sasLanguage.Submit("proc sql;");
//		for (int i = 0; i < lineasSubmit.size(); i++) {
//			sasLanguage.Submit("insert into inicializacionJavaPrev values (\'" + lineasSubmit.get(i) + "\');");
//		}
//		sasLanguage.Submit("quit;");
//		
//		lineasSubmitArr = new String[lineasSubmit.size()];
//		lineasSubmitArr = lineasSubmit.toArray(lineasSubmitArr);
//		sasLanguage.SubmitLines(lineasSubmitArr);
//		lineasSubmitArr = null;
//		lineasSubmit = new ArrayList<String>();
//		
//		// tabla para el pintado del codigo en formato *.SAS
//		sasLanguage.Submit("%crearFicheroSAS(&ruta_log.,inicializacionJavaPrev);");
//			
//		// lanzamiento del codigo
//		sasLanguage.Submit("%flujo();");
//
//		//volcamos el log al ultimo codejecucion enviado
//		logHldr = new StringSeqHolder();
//		sasLanguage.FlushLogLines(Integer.MAX_VALUE,new CarriageControlSeqHolder(),new LineTypeSeqHolder(),logHldr);
//		// display log file
//		logLines = logHldr.value;
//		LOGGER.info("procesando Log ejecucion " + cod_ejecucion);
//		md.guardarLogEjecucionPE(logLines, cod_ejecucion);
//
//
//		// desasignamos las librerias
//		sasLanguage.Submit("libname sche1 clear;");
//		sasLanguage.Submit("libname sche2 clear;");
//		sasLanguage.Submit("libname &lib_input. clear;");
//
////		if (SAS_local.equalsIgnoreCase("SI")) {
////			lineasSubmit.add("%limpiarTablasVariables(&lib_input.);");
////		} else {
////			lineasSubmit.add("%limpiarTablasVariables();");
////		}
//
//		// cerramos la conexion y actualizamos el estado a "finalizado"
//		LOGGER.info("Submit primer paso completado, se cierra workspace " + iWorkspace.UniqueIdentifier());
//		cx.close();
//
	}

	public void ejecutarCalculoDirecto(EjecucionMotor ejecucion) throws Exception {
		md.ejecutarCalculoDirecto(ejecucion);
		
	}
		
}