package sim.persistencia;

import java.util.ArrayList;

public class CarteraProgreso {

	private Integer id_cartera;
	private String nombre_cartera;
	private Integer progreso;
	private ArrayList<ProgresoPuntoProgreso> progresosPuntoProgreso;

	public CarteraProgreso() {
		this.setProgreso(0);
		ProgresoPuntoProgreso progresoPuntoProgreso = null;
		this.progresosPuntoProgreso = new ArrayList<ProgresoPuntoProgreso>();
		progresoPuntoProgreso = new ProgresoPuntoProgreso(0, "Procesar Ficheros");
		this.progresosPuntoProgreso.add(progresoPuntoProgreso);
		progresoPuntoProgreso = new ProgresoPuntoProgreso(0, "Aprobar Ficheros");
		this.progresosPuntoProgreso.add(progresoPuntoProgreso);
		progresoPuntoProgreso = new ProgresoPuntoProgreso(0, "Calcular PE");
		this.progresosPuntoProgreso.add(progresoPuntoProgreso);
		progresoPuntoProgreso = new ProgresoPuntoProgreso(0, "Validar Resultados");
		this.progresosPuntoProgreso.add(progresoPuntoProgreso);
		progresoPuntoProgreso = new ProgresoPuntoProgreso(0, "Aprobar Resultados");
		this.progresosPuntoProgreso.add(progresoPuntoProgreso);
	}

	public Integer getId_cartera() {
		return id_cartera;
	}

	public void setId_cartera(Integer id_cartera) {
		this.id_cartera = id_cartera;
	}

	public String getNombre_cartera() {
		return nombre_cartera;
	}

	public void setNombre_cartera(String nombre_cartera) {
		this.nombre_cartera = nombre_cartera;
	}

	public ArrayList<ProgresoPuntoProgreso> getProgresosPuntoProgreso() {
		return progresosPuntoProgreso;
	}

	public void setProgresosPuntoProgreso(ArrayList<ProgresoPuntoProgreso> progresosPuntoProgreso) {
		this.progresosPuntoProgreso = progresosPuntoProgreso;
	}

	public Integer getProgreso() {
		return progreso;
	}

	public void setProgreso(Integer progreso) {
		this.progreso = progreso;
	}

}
