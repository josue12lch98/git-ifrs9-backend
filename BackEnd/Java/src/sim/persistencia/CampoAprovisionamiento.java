package sim.persistencia;

import java.util.ArrayList;

import sim.persistencia.dic.TipoCampo;
import sim.persistencia.dic.TipoVariable;

public class CampoAprovisionamiento extends Campo {
	// private TipoCampo tipo_campo;
	private Integer id_aprovisionamiento;
	private Integer id_campo_aprovisionamiento;
	private TipoVariable tipo_variable; // Indica discreta o continua
	private Boolean flag_campo_original; // Indica si el campo provino directamente del fichero
	// private Boolean flag_borrar_campo; // Indica si el campo no forma parte de la
	// tabla input
	private Boolean flag_aprovisionar; // Indica si el campo forma parte de la tabla input
	private Boolean flag_llave; // Indica si el campo forma parte de la llave primaria para analizar duplicados
	private Boolean flag_segmentar; // Indica si el campo se utiliza para segmentar (group by) las reglas de calidad
	private Boolean flag_critica; // Indica si el campo es critico para el analisis-reporte de calidad
	private Boolean flag_evidencia; // Indica si el campo se incluirá en la tabla evidencia de calidad
	private String formula_nuevo_campo_input; // Indica la formula del nuevo campo input
	private String formula_nuevo_campo_sql; // Indica la formula del nuevo campo en sql (para ejecutar la query)

	private ArrayList<ReglaCalidad> reglas;// Reglas de calidad que aplican a cada campo

	private Boolean flag_aplica;

	// Constructores
	public CampoAprovisionamiento() {
		super();
		// tipo_campo = new TipoCampo();
		tipo_variable = new TipoVariable();
		this.flag_evidencia = false;
	}

	public CampoAprovisionamiento(Integer id_campo, String nombre_campo, TipoCampo tipo_campo,
			Boolean flag_campo_original) {
		super(id_campo, nombre_campo, tipo_campo);
		this.flag_campo_original = flag_campo_original;
		this.tipo_variable = new TipoVariable();
		this.flag_evidencia = false;
	}

	public CampoAprovisionamiento(Integer id_campo, String nombre_campo, TipoCampo tipo_campo,
			TipoVariable tipo_variable, Boolean flag_campo_original, Boolean flag_aprovisionar, Boolean flag_llave,
			Boolean flag_segmentar, Boolean flag_critica, Boolean flag_evidencia, String formula_nuevo_campo_input,
			String formula_nuevo_campo_sql) {
		super(id_campo, nombre_campo, tipo_campo);
		this.tipo_variable = tipo_variable;
		this.flag_campo_original = flag_campo_original;
		this.flag_aprovisionar = flag_aprovisionar;
		// this.flag_borrar_campo = flag_borrar_campo;
		this.flag_llave = flag_llave;
		this.flag_segmentar = flag_segmentar;
		this.flag_critica = flag_critica;
		this.flag_evidencia = flag_evidencia;
		this.formula_nuevo_campo_input = formula_nuevo_campo_input;
		this.formula_nuevo_campo_sql = formula_nuevo_campo_sql;
	}

	// Getters y Setters
	public TipoVariable getTipo_variable() {
		return tipo_variable;
	}

	public void setTipo_variable(TipoVariable tipo_variable) {
		this.tipo_variable = tipo_variable;
	}

	public Boolean getFlag_campo_original() {
		return flag_campo_original;
	}

	public void setFlag_campo_original(Boolean flag_campo_original) {
		this.flag_campo_original = flag_campo_original;
	}

	public Boolean getFlag_aprovisionar() {
		return flag_aprovisionar;
	}

	public void setFlag_aprovisionar(Boolean flag_aprovisionar) {
		this.flag_aprovisionar = flag_aprovisionar;
	}

	public Boolean getFlag_llave() {
		return flag_llave;
	}

	public void setFlag_llave(Boolean flag_llave) {
		this.flag_llave = flag_llave;
	}

	public Boolean getFlag_segmentar() {
		return flag_segmentar;
	}

	public void setFlag_segmentar(Boolean flag_segmentar) {
		this.flag_segmentar = flag_segmentar;
	}

	public Boolean getFlag_critica() {
		return flag_critica;
	}

	public void setFlag_critica(Boolean flag_critica) {
		this.flag_critica = flag_critica;
	}

	public String getFormula_nuevo_campo_input() {
		return formula_nuevo_campo_input;
	}

	public void setFormula_nuevo_campo_input(String formula_nuevo_campo_input) {
		this.formula_nuevo_campo_input = formula_nuevo_campo_input;
	}

	public String getFormula_nuevo_campo_sql() {
		return formula_nuevo_campo_sql;
	}

	public void setFormula_nuevo_campo_sql(String formula_nuevo_campo_sql) {
		this.formula_nuevo_campo_sql = formula_nuevo_campo_sql;
	}

//	public TipoCampo getTipo_campo() {
//		return tipo_campo;
//	}
//
//	public void setTipo_campo(TipoCampo tipo_campo) {
//		this.tipo_campo = tipo_campo;
//	}

	public Integer getId_campo_aprovisionamiento() {
		return id_campo_aprovisionamiento;
	}

	public Integer getId_aprovisionamiento() {
		return id_aprovisionamiento;
	}

	public void setId_aprovisionamiento(Integer id_aprovisionamiento) {
		this.id_aprovisionamiento = id_aprovisionamiento;
	}

	public ArrayList<ReglaCalidad> getReglas() {
		return reglas;
	}

	public void setReglas(ArrayList<ReglaCalidad> reglas) {
		this.reglas = reglas;
	}

	public void setId_campo_aprovisionamiento(Integer id_campo_aprovisionamiento) {
		this.id_campo_aprovisionamiento = id_campo_aprovisionamiento;
	}

	public Boolean getFlag_aplica() {
		return flag_aplica;
	}

	public void setFlag_aplica(Boolean flag_aplica) {
		this.flag_aplica = flag_aplica;
	}

	public Boolean getFlag_evidencia() {
		return flag_evidencia;
	}

	public void setFlag_evidencia(Boolean flag_evidencia) {
		this.flag_evidencia = flag_evidencia;
	}


}