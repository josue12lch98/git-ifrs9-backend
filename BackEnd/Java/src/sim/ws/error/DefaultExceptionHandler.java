package sim.ws.error;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotAllowedException;
//Errores
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;

import javax.naming.NamingException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Provider
public class DefaultExceptionHandler implements ExceptionMapper<Exception> {

	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(DefaultExceptionHandler.class);

	@Override
	public Response toResponse(Exception e) {
		// StringBuilder response = new StringBuilder("<response>");

		StringBuilder response = new StringBuilder("{");		

		if (e instanceof BadRequestException || e instanceof InternalServerErrorException
				|| e instanceof NotAcceptableException || e instanceof NotFoundException
				|| e instanceof NotAllowedException || e instanceof ForbiddenException) {

			LOGGER.error("Error sin contenido al front: " + e.getMessage());

			return Response.noContent().build();

		} else if (e instanceof NotAuthorizedException || e instanceof NamingException
				|| e instanceof UnsupportedJwtException || e instanceof MalformedJwtException
				|| e instanceof SignatureException || e instanceof IllegalArgumentException) {

			LOGGER.error("Error autenticacion: " + e.getMessage());
			
			response.append("\"status\":\"UNAUTHORIZED\", ");
			response.append("\"message\":\"-\"");
		} else if (e instanceof ExpiredJwtException) {
			
			LOGGER.warn("Error token expirado: " + e.getMessage());
			response.append("\"status\":\"SESSION_EXPIRED\", ");
			response.append("\"message\":\"-\"");
		} else { // Solo mostramos el mensaje en caso el token este correcto (usuario logueado)
			LOGGER.error("Error: " + e.getMessage());
			response.append("\"status\":\"ERROR\", ");
			response.append(
					"\"message\":\"" + e.getMessage().replace("\"", "\\\"").replace("\n", "").replace("\r", "") + "\"");
		}

		// response.append("</response>");
		response.append("}");

		// return
		// Response.serverError().entity(response.toString()).type(MediaType.APPLICATION_XML).build();
		return Response.serverError().entity(response.toString()).type(MediaType.APPLICATION_JSON).build();
	}
}
