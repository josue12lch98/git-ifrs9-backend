package sim.persistencia;

import java.util.ArrayList;


public class EjecucionPE {
	private Integer id_ejecucion;
	private Integer codmes;
	private Escenario_version escenario_version;	
	private Integer estado_ejecucion;	
	private Long datetime_ejecucion;	
	private ArrayList<TablaMapeoEjecucion> tablas_mapeo_ejecucion;
	private Integer fase;
	
	public EjecucionPE() {
	}

	public Integer getCodmes() {
		return codmes;
	}

	public void setCodmes(Integer codmes) {
		this.codmes = codmes;
	}

	public Escenario_version getEscenario_version() {
		return escenario_version;
	}

	public void setEscenario_version(Escenario_version escenario_version) {
		this.escenario_version = escenario_version;
	}
	
	public Long getDatetime_ejecucion() {
		return datetime_ejecucion;
	}

	public void setDatetime_ejecucion(Long datetime_ejecucion) {
		this.datetime_ejecucion = datetime_ejecucion;
	}

	public Integer getEstado_ejecucion() {
		return estado_ejecucion;
	}

	public void setEstado_ejecucion(Integer estado_ejecucion) {
		this.estado_ejecucion = estado_ejecucion;
	}	

	public Integer getId_ejecucion() {
		return id_ejecucion;
	}

	public void setId_ejecucion(Integer id_ejecucion) {
		this.id_ejecucion = id_ejecucion;
	}

	public ArrayList<TablaMapeoEjecucion> getTablas_mapeo_ejecucion() {
		return tablas_mapeo_ejecucion;
	}

	public void setTablas_mapeo_ejecucion(ArrayList<TablaMapeoEjecucion> tablas_mapeo_ejecucion) {
		this.tablas_mapeo_ejecucion = tablas_mapeo_ejecucion;
	}

	public Integer getFase() {
		return fase;
	}

	public void setFase(Integer fase) {
		this.fase = fase;
	}	
}
