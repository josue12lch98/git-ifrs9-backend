package sim.persistencia.bd;

import sim.AES;
import sim.Claves;
import sim.PropertiesCompartido;
import sim.persistencia.*;
import sim.persistencia.dic.*;

import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.mathworks.util.logger.Logger;
import com.sas.iom.SAS.ILanguageService;
import com.sas.iom.SAS.IWorkspace;
import com.sas.iom.SAS.IWorkspaceHelper;
import com.sas.services.connection.BridgeServer;
import com.sas.services.connection.ConnectionFactoryConfiguration;
import com.sas.services.connection.ConnectionFactoryException;
import com.sas.services.connection.ConnectionFactoryInterface;
import com.sas.services.connection.ConnectionFactoryManager;
import com.sas.services.connection.ConnectionInterface;
import com.sas.services.connection.ManualConnectionFactoryConfiguration;
import com.sas.services.connection.Server;

public abstract class ModuloDatos {

	protected static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(ModuloDatos.class);

	// Clase con metodos utiles para queries
	protected static ModuloDatos_Utils utils;

	// Esquema de trabajo
	protected static String esquema;
	protected static final Boolean autocommit = false;
	protected static Integer max_lote;

	// Variables para conexion mediante context en el servidor de aplicaciones
	protected static DataSource ds;
	protected static Boolean conexionContext;

	// Variables para conexion mediante driver (libreria en buildpath del proyecto)
	protected static Driver dbDriver;
	protected static String dsName;
	protected static String driverName;
	protected static String dbUri;
	protected static String userName;

	// Variables para manejar archivos (ficheros)
	protected static String separador;
	protected static Pattern patron_cabecera;
	protected static String ruta_exportacion_ficheros_cred;
	protected static String ruta_exportacion_ficheros_inv;

	// Logs de calidad
	protected static String rutaLogsCalidad;

	// Desarrollo
	protected static boolean developer;

	// Clases Diccionarios que solo seran inicializadas al construirse el modulo de
	// datos, mejor rendimiento
	protected static ArrayList<Pais> dic_paises;
	protected static ArrayList<TipoAmbito> dic_tipo_ambito;
	protected static ArrayList<TipoMagnitud> dic_tipo_magnitud;
	protected static ArrayList<TipoMagnitud> dic_tipo_magnitud_inversiones;
	protected static ArrayList<TipoMagnitud> dic_tipo_magnitud_creditos;
	protected static ArrayList<MotorCalculo> dic_motor_calculo;
	protected static ArrayList<TipoObtencionResultados> dic_tipo_obtencion_resultados;
	protected static ArrayList<TipoFlujo> dic_tipo_flujo;
	protected static ArrayList<TipoMetodologia> dic_tipo_metodologia;
	protected static ArrayList<TipoPeriodicidad> dic_tipo_periodicidad;
	protected static ArrayList<TipoVariable> dic_tipo_variable;
	protected static ArrayList<TipoCampo> dic_tipo_campo;
	protected static ArrayList<TipoBloque> dic_tipo_bloque;
	protected static ArrayList<TipoEjecucion> dic_tipo_ejecucion;
	protected static ArrayList<TipoIndicador> dic_tipo_indicador;
	protected static ArrayList<TipoCartera> dic_tipo_cartera;
	protected static ArrayList<FormatoInput> dic_formato_input;

	public ModuloDatos(Properties properties, ModuloDatos_Utils utils) throws Exception {
		LOGGER.info("Creando ModuloDatos");

		// Se establece la clase con metodos utiles para queries
		ModuloDatos.utils = utils;

		// Se busca la conexion a BBDD
		conectarBBDD(properties);
		developer = properties.getProperty("Developer") != null
				&& properties.getProperty("Developer").equalsIgnoreCase("SI");

		esquema = properties.getProperty("JDBC_schema");

		// Se determina el userName
		userName = properties.getProperty("JDBC_userName");

		// Numero maximo de filas para ejecutar proceso batch
		max_lote = Integer.valueOf(properties.getProperty("Maximo_Batch"));

		// Se determinan las variables para el manejo de archivos
		if (properties.getProperty("Separador_Campos_Archivo") != null
				&& properties.getProperty("Separador_Campos_Archivo").length() > 0) {
			separador = properties.getProperty("Separador_Campos_Archivo");
		} else {
			LOGGER.warn("No se ha configurado un separador de campos de archivo valido. Se utilizara el caracter | ");
			separador = "|";
		}

		patron_cabecera = Pattern.compile("[_a-zA-Z0-9-]+");
		ruta_exportacion_ficheros_cred = properties.getProperty("Ruta_exportacion_ficheros_cred");
		ruta_exportacion_ficheros_inv = properties.getProperty("Ruta_exportacion_ficheros_inv");

		// Ruta de logs de calidad
		rutaLogsCalidad = properties.getProperty("RutaLogsCalidad");

		// Inicializar clases diccionarios
		obtenerPaisesBD();
		obtenerTiposAmbitoBD();
		obtenerTiposMagnitudBD();
		obtenerMotoresCalculoBD();
		obtenerTiposObtencionResultadosBD();
		obtenerTiposFlujoBD();
		obtenerTiposPeriodicidadBD();
		obtenerTiposVariableBD();
		obtenerTiposCampoBD();
		obtenerTiposBloqueBD();
		obtenerTiposEjecucionBD();
		obtenerTiposIndicadorBD();
		obtenerFormatosInputBD();

		LOGGER.info("ModuloDatos creado");
	}

	// Metodos encapsulados
	private void conectarBBDD(Properties properties) throws Exception {
		if (properties.getProperty("Buscar_Conexion_BD") == null) {
			LOGGER.error(
					"No se ha establecido la propiedad Buscar_Conexion_BD, no se ha podido inicializar ModuloDatos");
		} else {
			if (properties.getProperty("Buscar_Conexion_BD").equalsIgnoreCase("DRIVER")) {
				conexionContext = false;
				registrarDataSourceDriver(properties);
			} else if (properties.getProperty("Buscar_Conexion_BD").equalsIgnoreCase("CONTEXT")) {
				conexionContext = true;
				obtenerDataSourceContext(properties);
			} else {
				LOGGER.error("No se ha configurado como DRIVER o CONTEXT la propiedad Buscar_Conexion_BD");
			}
		}
	}

	private void registrarDataSourceDriver(Properties properties) throws Exception {
		try {
			// Se leen las propiedades para el DataSource por DRIVER
			driverName = properties.getProperty("JDBC_driverName");

			if (properties.getProperty("JDBC_syntax").equals("sid")) {
				dbUri = "jdbc:oracle:thin:" + properties.getProperty("JDBC_userName") + "/"
						+ properties.getProperty("JDBC_encriptado") + "@" + properties.getProperty("JDBC_host") + ":"
						+ properties.getProperty("JDBC_port") + ":" + properties.getProperty("JDBC_sid");
				LOGGER.info("Registrando driver por SID"); // dbUri
			} else if (properties.getProperty("JDBC_syntax").equals("serviceName")) {
				dbUri = "jdbc:oracle:thin:" + properties.getProperty("JDBC_userName") + "/"
						+ properties.getProperty("JDBC_encriptado") + "@//" + properties.getProperty("JDBC_host") + ":"
						+ properties.getProperty("JDBC_port") + "/" + properties.getProperty("JDBC_serviceName");
				LOGGER.info("Registrando driver por serviceName"); // dbUri
			}

			// Se instancia el driver
			ModuloDatos.dbDriver = (Driver) Class.forName(driverName).newInstance();

			// Se registra el driver
			DriverManager.registerDriver(ModuloDatos.dbDriver);
		} catch (Exception e) {
			LOGGER.fatal("Error al obtener datos o registrar el driver: " + driverName + ". Error: " + e.getMessage());
		}
	}

	private void obtenerDataSourceContext(Properties properties) throws Exception {
		try {
			// Se leen las propiedades para el DataSource por CONTEXT
			dsName = properties.getProperty("JDBC_userName");

			// Se inicializa el context
			Context ctx = new InitialContext();

			// Se busca el Datasource en el context
			ModuloDatos.ds = (DataSource) ctx.lookup("java:comp/env/jdbc/" + dsName);
			LOGGER.info("DataSource " + dsName + " enlazado satisfactoriamente");
		} catch (Exception e) {
			LOGGER.fatal("Error al intentar obtener el DataSource " + dsName + ": " + e.getMessage());
		}
	}

	protected Connection obtenerConexion() throws Exception {
		Connection conn = null;
		Statement stmt = null;
		// Primero obtener la conexion

		try {
			if (!conexionContext) {
				conn = DriverManager.getConnection(dbUri);

				conn.setAutoCommit(autocommit);
			} else if (conexionContext) {

				conn = ds.getConnection();
				conn.setAutoCommit(autocommit);
			} else {
				LOGGER.warn("Revisar la propiedad Buscar_Conexion_BD");
			}
		} catch (Exception e) {
			LOGGER.fatal("Error obteniendo conexion: " + e.getMessage());
		}
		// Luego alterar la sesion de la conexion para establecer configuracion
		// especifica (por ejemplo el nombre del schema)
		stmt = crearStatement(conn);
		alterarSesion(stmt);
		cerrarStatement(stmt);

		return conn;
	}

	protected void alterarSesion(Statement stmt) throws Exception {
		try {
			stmt.execute("alter session set current_schema=" + esquema);
		} catch (Exception ex) {
			LOGGER.fatal("Error al alterar sesion actual: " + ex.getMessage());
		}
	}

	protected void cerrarConexion(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			LOGGER.error("Error cerrando conexion: " + e.getMessage() + ". Conexion: " + conn);
		}
	}

	protected Statement crearStatement(Connection conn) throws Exception {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			alterarSesion(stmt);
		} catch (Exception e) {
			LOGGER.fatal("Error al crear el statement: " + e.getMessage());
		}
		return stmt;
	}

	protected void cerrarResultSet(ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			LOGGER.error("Error al cerrar el resultset: " + e.getMessage());
		}
	}

	protected void cerrarStatement(Statement stmt) {
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (Exception e) {
			LOGGER.error("Error al cerrar el statement: " + e.getMessage());
		}
	}

	protected void cerrarPreparedStatement(PreparedStatement pstmt) {
		try {
			if (pstmt != null) {
				pstmt.close();
			}
		} catch (Exception e) {
			LOGGER.error("Error al cerrar el prepared statement: " + e.getMessage());
		}
	}

	protected void ejecutarCommit(Connection conn) throws Exception {
		try {
			if (conn != null) {
				conn.commit();
			}
		} catch (Exception e) {
			LOGGER.error("Error al ejecutar commit: " + e.getMessage() + ". Conexion: " + conn);
		}
	}

	protected void ejecutarRollback(Connection conn) throws Exception {
		try {
			if (conn != null) {
				conn.rollback();
			}
		} catch (Exception e) {
			LOGGER.error("Error al ejecutar rollback: " + e.getMessage() + ". Conexion: " + conn);
		}
	}

	private static void cerrarBufferedReader(BufferedReader br) {
		try {
			if (br != null) {
				br.close();
			}
		} catch (Exception ex) {
			LOGGER.error("Error al cerrar BufferedReader: " + ex.getMessage());
		}
	}

	// Se utiliza para separar campos que tienen doble comillas para delimitar su
	// contenido de inicio a fin, a fin de diferenciar el separador
	private static ArrayList<String> separarCampos(String texto, String delimitador) throws Exception {
		ArrayList<String> palabras = new ArrayList<String>();

		// Si el delimitador es "|" o '.' la funcion split funciona con "\\" adelante
		if (delimitador.equals("|") || delimitador.equals(".")) {
			delimitador = "\\" + delimitador;
		}

		String texto_sp[] = texto.split(delimitador, -1);

		for (int i = 0; i < texto_sp.length; i++) {
			palabras.add(texto_sp[i]);
		}

		return palabras;
	}

	protected abstract Integer obtenerSiguienteId(String nombre_tabla) throws Exception;

	protected Integer obtenerValorActualDeCargaFichero() throws Exception {
		int siguiente_id = 0;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "Select max(CODCARGAFICHERO) FROM HE_CARGAFICHERO hc ";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			rs.next();
			siguiente_id = rs.getInt(1);

		} catch (Exception ex) {
			ejecutarRollback(conn);

		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return siguiente_id;
	}

	/* Fin de metodos encapsulados del modulo de datos */

	// Metodos de inicializacion de diccionarios

	protected void obtenerPaisesBD() throws Exception {
		dic_paises = new ArrayList<Pais>();
		Pais pais = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM ME_Pais ORDER BY CodPais";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				pais = new Pais();
				pais.setId_pais(rs.getInt("CodPais"));
				pais.setNombre_pais(rs.getString("NbrPais"));
				dic_paises.add(pais);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los paises: " + ex.getMessage());
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	protected void obtenerTiposAmbitoBD() throws Exception {
		dic_tipo_ambito = new ArrayList<TipoAmbito>();
		TipoAmbito tipo_ambito = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM ME_TipoAmbito ORDER BY CodTipoAmbito";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tipo_ambito = new TipoAmbito();
				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				dic_tipo_ambito.add(tipo_ambito);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los tipos de ámbito: " + ex.getMessage());
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	protected void obtenerTiposMagnitudBD() throws Exception {
		dic_tipo_magnitud = new ArrayList<TipoMagnitud>();
		dic_tipo_magnitud_creditos = new ArrayList<TipoMagnitud>();
		dic_tipo_magnitud_inversiones = new ArrayList<TipoMagnitud>();
		TipoMagnitud tipo_magnitud = null;
		TipoAmbito tipo_ambito = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select TM.CodTipoMagnitud, TM.NbrTipoMagnitud, TA.CodTipoAmbito, TA.NbrTipoAmbito "
				+ " from ME_TipoMagnitud TM left join ME_TipoAmbito TA" + " on TM.CodTipoAmbito = TA.CodTipoAmbito "
				+ " order by TM.CodTipoMagnitud";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tipo_magnitud = new TipoMagnitud();
				tipo_magnitud.setId_tipo_magnitud(rs.getInt("CodTipoMagnitud"));
				tipo_magnitud.setNombre_tipo_magnitud(rs.getString("NbrTipoMagnitud"));

				tipo_ambito = new TipoAmbito();
				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				tipo_magnitud.setTipo_ambito(tipo_ambito);

				dic_tipo_magnitud.add(tipo_magnitud);
			}
			for (TipoMagnitud tipoMagnitud : dic_tipo_magnitud) {
				if (tipoMagnitud.getTipo_ambito().getId_tipo_ambito().equals(1)) {
					dic_tipo_magnitud_creditos.add(tipoMagnitud);
				} else {
					dic_tipo_magnitud_inversiones.add(tipoMagnitud);
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los tipos de magnitud: " + ex.getMessage());
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	protected void obtenerFormatosInputBD() throws Exception {
		dic_formato_input = new ArrayList<FormatoInput>();
		FormatoInput formatoInput = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM ME_FormatoInput ORDER BY CodFormatoInput";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				formatoInput = new FormatoInput();
				formatoInput.setId_formato_input(rs.getInt("CodFormatoInput"));
				formatoInput.setNombre_formato_input(rs.getString("NbrFormatoInput"));
				dic_formato_input.add(formatoInput);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los formatos input: " + ex.getMessage());
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	// --------------------------------------------

	public ArrayList<TipoCartera> obtenerTiposCartera() throws Exception {
		dic_tipo_cartera = new ArrayList<TipoCartera>();
		TipoCartera tipo_cartera = null;
		TipoAmbito tipo_ambito = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select TC.CodTipoCartera, TC.NbrTipoCartera, TC.NbrVariableMotor,"
				+ " TA.CodTipoAmbito, TA.NbrTipoAmbito " + " from ME_TipoCartera TC left join ME_TipoAmbito TA"
				+ " on TC.CodTipoAmbito = TA.CodTipoAmbito " + " order by TC.CodTipoCartera";

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tipo_cartera = new TipoCartera();
				tipo_cartera.setId_tipo_cartera(rs.getInt("CodTipoCartera"));
				tipo_cartera.setNombre_tipo_cartera(rs.getString("NbrTipoCartera"));
				tipo_cartera.setNombre_variablemotor(rs.getString("NbrVariableMotor"));

				tipo_ambito = new TipoAmbito();
				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				tipo_cartera.setTipo_ambito(tipo_ambito);

				dic_tipo_cartera.add(tipo_cartera);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los tipos de cartera: " + ex.getMessage());
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return dic_tipo_cartera;
	}

	public void crearTipoCartera(TipoCartera tipocartera) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearTipoCartera(tipocartera, conn);
		cerrarConexion(conn);
	}

	private void crearTipoCartera(TipoCartera tipocartera, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "INSERT INTO ME_TipoCartera (CodTipoCartera, NbrTipoCartera, NbrVariableMotor, CodTipoAmbito, FecActualizacionTabla)"
				+ " VALUES (?, ?, ?, ?, ?)";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, obtenerSiguienteId("SEQ_ME_TipoCartera"));
			pstmt.setString(2, tipocartera.getNombre_tipo_cartera());
			pstmt.setString(3, tipocartera.getNombre_variablemotor());
			pstmt.setInt(4, tipocartera.getTipo_ambito().getId_tipo_ambito());
			pstmt.setDate(5, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear un tipo cartera: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void editarTipoCartera(TipoCartera tipocartera) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Update ME_TipoCartera set NbrTipoCartera=?, NbrVariableMotor=?, FecActualizacionTabla=? "
				+ " where CodTipoCartera = ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, tipocartera.getNombre_tipo_cartera());
			pstmt.setString(2, tipocartera.getNombre_variablemotor());
			pstmt.setDate(3, new Date(System.currentTimeMillis()));
			pstmt.setInt(4, tipocartera.getId_tipo_cartera());
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar tipo cartera: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void borrarTipoCartera(Integer id_tipocartera) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Delete from ME_TipoCartera where CodTipoCartera= ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_tipocartera);
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar tipo cartera con id " + id_tipocartera + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	protected void obtenerMotoresCalculoBD() throws Exception {
		dic_motor_calculo = new ArrayList<MotorCalculo>();
		MotorCalculo motor_calculo = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM ME_MotorCalculo ORDER BY CodMotor";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				motor_calculo = new MotorCalculo();
				motor_calculo.setId_motor(rs.getInt("CodMotor"));
				motor_calculo.setNombre_motor(rs.getString("NbrMotor"));
				dic_motor_calculo.add(motor_calculo);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los motores de cálculo: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	protected void obtenerTiposObtencionResultadosBD() throws Exception {
		dic_tipo_obtencion_resultados = new ArrayList<TipoObtencionResultados>();
		TipoObtencionResultados tipo_obtencion_resultados = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM ME_TipoObtencionResultados ORDER BY CodTipoObtenResult";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tipo_obtencion_resultados = new TipoObtencionResultados();
				tipo_obtencion_resultados.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
				tipo_obtencion_resultados.setNombre_tipo_obtencion_resultados(rs.getString("NbrTipoObtenResult"));
				dic_tipo_obtencion_resultados.add(tipo_obtencion_resultados);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los tipos de obtención de resultados: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	protected void obtenerTiposFlujoBD() throws Exception {
		dic_tipo_flujo = new ArrayList<TipoFlujo>();
		TipoFlujo tipo_flujo = null;
		TipoAmbito tipo_ambito = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select TF.CodTipoFlujo, TF.NbrTipoFlujo, TA.CodTipoAmbito, TA.NbrTipoAmbito "
				+ " from ME_TipoFlujo TF left join ME_TipoAmbito TA" + " on TF.CodTipoAmbito = TA.CodTipoAmbito "
				+ " order by TF.CodTipoFlujo";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tipo_flujo = new TipoFlujo();
				tipo_flujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipo_flujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));

				tipo_ambito = new TipoAmbito();
				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				tipo_flujo.setTipo_ambito(tipo_ambito);

				dic_tipo_flujo.add(tipo_flujo);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los tipos de flujo: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	public ArrayList<TipoMetodologia> obtenerTiposMetodologia() throws Exception {
		dic_tipo_metodologia = new ArrayList<TipoMetodologia>();
		TipoMetodologia tipo_metodologia = null;
		TipoAmbito tipo_ambito = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select TM.CodTipoMetodologia, TM.NbrTipoMetodologia, TM.NbrVariableMotor,"
				+ " TA.CodTipoAmbito, TA.NbrTipoAmbito " + " from ME_TipoMetodologia TM left join ME_TipoAmbito TA"
				+ " on TM.CodTipoAmbito = TA.CodTipoAmbito " + " order by TM.CodTipoMetodologia";

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tipo_metodologia = new TipoMetodologia();
				tipo_metodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologia"));
				tipo_metodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologia"));
				tipo_metodologia.setNombre_variablemotor(rs.getString("NbrVariableMotor"));

				tipo_ambito = new TipoAmbito();
				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				tipo_metodologia.setTipo_ambito(tipo_ambito);

				dic_tipo_metodologia.add(tipo_metodologia);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los tipos de metodología: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return dic_tipo_metodologia;
	}

	public void crearTipoMetodologia(TipoMetodologia tipometodologia) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearTipoMetodologia(tipometodologia, conn);
		cerrarConexion(conn);
	}

	private void crearTipoMetodologia(TipoMetodologia tipometodologia, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "INSERT INTO ME_TIPOMETODOLOGIA (CodTipoMetodologia, NbrTipoMetodologia, NbrVariableMotor,"
				+ " CodTipoAmbito, FecActualizacionTabla) VALUES (?, ?, ?, ?, ?)";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, obtenerSiguienteId("SEQ_ME_TipoMetodologia"));
			pstmt.setString(2, tipometodologia.getNombre_tipo_metodologia());
			pstmt.setString(3, tipometodologia.getNombre_variablemotor());
			pstmt.setInt(4, tipometodologia.getTipo_ambito().getId_tipo_ambito());
			pstmt.setDate(5, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear un tipo Metodologia: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void editarTipoMetodologia(TipoMetodologia tipometodologia) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Update ME_TipoMetodologia set NbrTipoMetodologia=?, NbrVariableMotor=?, FecActualizacionTabla=? "
				+ " where CodTipoMetodologia = ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, tipometodologia.getNombre_tipo_metodologia());
			pstmt.setString(2, tipometodologia.getNombre_variablemotor());
			pstmt.setDate(3, new Date(System.currentTimeMillis()));
			pstmt.setInt(4, tipometodologia.getId_tipo_metodologia());
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar entidad: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void borrarTipoMetodologia(Integer id_tipometodologia) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Delete from ME_TipoMetodologia where CodTipoMetodologia= ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_tipometodologia);
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error al borrar tipo metodologia con id " + id_tipometodologia + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	protected void obtenerTiposPeriodicidadBD() throws Exception {
		dic_tipo_periodicidad = new ArrayList<TipoPeriodicidad>();
		TipoPeriodicidad tipo_periodicidad = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM ME_TipoPeriodicidad ORDER BY CodTipoPeriodicidad";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tipo_periodicidad = new TipoPeriodicidad();
				tipo_periodicidad.setId_tipo_periodicidad(rs.getInt("CodTipoPeriodicidad"));
				tipo_periodicidad.setNombre_tipo_periodicidad(rs.getString("NbrTipoPeriodicidad"));
				tipo_periodicidad.setValor_periodicidad(rs.getInt("NumValorPeriodicidad"));
				dic_tipo_periodicidad.add(tipo_periodicidad);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los tipos de periodicidad: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	protected void obtenerTiposVariableBD() throws Exception {
		dic_tipo_variable = new ArrayList<TipoVariable>();
		TipoVariable tipo_variable = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM ME_TipoVariable ORDER BY CodTipoVariable";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tipo_variable = new TipoVariable();
				tipo_variable.setId_tipo_variable(rs.getInt("CodTipoVariable"));
				tipo_variable.setNombre_tipo_variable(rs.getString("NbrTipoVariable"));
				dic_tipo_variable.add(tipo_variable);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los tipos de variable: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	protected void obtenerTiposCampoBD() throws Exception {
		dic_tipo_campo = new ArrayList<TipoCampo>();
		TipoCampo tipo_campo = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM ME_TipoCampo ORDER BY CodTipoCampo";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tipo_campo = new TipoCampo();
				tipo_campo.setId_tipo_campo(rs.getInt("CodTipoCampo"));
				tipo_campo.setNombre_tipo_campo(rs.getString("NbrTipoCampo"));
				dic_tipo_campo.add(tipo_campo);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los tipos de campo: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	protected void obtenerTiposBloqueBD() throws Exception {
		dic_tipo_bloque = new ArrayList<TipoBloque>();
		TipoBloque tipo_bloque = null;
		TipoEjecucion tipo_ejecucion = null;
		TipoFlujo tipo_flujo = null;
		TipoAmbito tipo_ambito = null;
		TipoObtencionResultados tipo_obtencion_resultados = null;
		PrimerPaso primerPaso = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "SELECT TB.*, "
				+ "PP.NbrPrimerPaso, PP.NbrVariableMotor, PP.DesPrimerPaso, TE.NbrTipoEjecucion, TE.CodTipoFlujo, "
				+ "TF.NbrTipoFlujo, TA.CodTipoAmbito, TA.NbrTipoAmbito, TOR.NbrTipoObtenResult "
				+ "FROM ME_TipoBloque TB " + "left join HE_PrimerPaso PP on TB.CodPrimerPaso = PP.CodPrimerPaso "
				+ "left join ME_TipoEjecucion TE on TB.CodTipoEjecucion = TE.CodTipoEjecucion "
				+ "left join ME_TipoFlujo TF on TE.CodTipoFlujo = TF.CodTipoFlujo "
				+ "left join ME_TipoAmbito TA on TF.CodTipoAmbito = TA.CodTipoAmbito "
				+ "left join ME_TipoObtencionResultados TOR on TB.CodTipoObtenResult = TOR.CodTipoObtenResult "
				+ "ORDER BY CodTipoBloque";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tipo_bloque = new TipoBloque();

				tipo_bloque.setId_tipo_bloque(rs.getInt("CodTipoBloque"));
				tipo_bloque.setNombre_tipo_bloque(rs.getString("NbrTipoBloque"));
				tipo_bloque.setNombre_variablemotor(rs.getString("NbrVariableMotor"));
				tipo_bloque.setFlag_bucket(rs.getBoolean("FlgBucket"));

				tipo_ejecucion = new TipoEjecucion();
				tipo_ejecucion.setId_tipo_ejecucion(rs.getInt("CodTipoEjecucion"));
				tipo_ejecucion.setNombre_tipo_ejecucion(rs.getString("NbrTipoEjecucion"));

				tipo_flujo = new TipoFlujo();
				tipo_flujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipo_flujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));

				tipo_ambito = new TipoAmbito();
				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				tipo_flujo.setTipo_ambito(tipo_ambito);

				tipo_ejecucion.setTipo_flujo(tipo_flujo);

				tipo_bloque.setTipo_ejecucion(tipo_ejecucion);

				if (rs.getInt("CodPrimerPaso") != 0) { // no es null
					primerPaso = new PrimerPaso();
					primerPaso.setId_primerPaso(rs.getInt("CodPrimerPaso"));
					primerPaso.setNombre_primerPaso(rs.getString("NbrPrimerPaso"));
					primerPaso.setDescripcion_primerPaso(rs.getString("DesPrimerPaso"));
					tipo_bloque.setPrimerPaso(primerPaso);
				}

				tipo_obtencion_resultados = new TipoObtencionResultados();
				tipo_obtencion_resultados.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
				tipo_obtencion_resultados.setNombre_tipo_obtencion_resultados(rs.getString("NbrTipoObtenResult"));
				tipo_bloque.setTipo_obtencion_resultados(tipo_obtencion_resultados);

				dic_tipo_bloque.add(tipo_bloque);

			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los tipos de de bloque: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	public void crearTipoBloque(TipoBloque tipobloque) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "INSERT INTO ME_TIPOBLOQUE (CodTipoBloque, NbrTipoBloque, NbrVariableMotor, FlgBucket,"
				+ " CodPrimerPaso, FecActualizacionTabla) VALUES (?,?,?,?,?,?)";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, obtenerSiguienteId("SEQ_ME_TipoBloque"));
			pstmt.setString(2, tipobloque.getNombre_tipo_bloque());
			pstmt.setString(3, tipobloque.getNombre_variablemotor());
			pstmt.setBoolean(4, tipobloque.getFlag_bucket());
			pstmt.setInt(5, tipobloque.getPrimerPaso().getId_primerPaso());
			pstmt.setDate(6, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar el tipo de bloque con id " + tipobloque.getId_tipo_bloque() + ". Error: "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void borrarTipoBloque(Integer id_tipobloque) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Delete from ME_TipoBloque where CodTipoBloque= ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_tipobloque);
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar Tipo Bloque con id " + id_tipobloque + "// Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	protected void obtenerTiposEjecucionBD() throws Exception {
		dic_tipo_ejecucion = new ArrayList<TipoEjecucion>();
		TipoEjecucion tipo_ejecucion = null;
		TipoFlujo tipo_flujo = null;
		TipoAmbito tipo_ambito = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select TE.*, TF.*, TA.* FROM ME_TipoEjecucion TE "
				+ "left join ME_TipoFlujo TF on TF.CodTipoFlujo = TE.CodTipoFlujo "
				+ "left join ME_TipoAmbito TA on TF.CodTipoAmbito = TA.CodTipoAmbito " + "order by TE.CodTipoEjecucion";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tipo_ejecucion = new TipoEjecucion();
				tipo_ejecucion.setId_tipo_ejecucion(rs.getInt("CodTipoEjecucion"));
				tipo_ejecucion.setNombre_tipo_ejecucion(rs.getString("NbrTipoEjecucion"));

				tipo_flujo = new TipoFlujo();
				tipo_flujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipo_flujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));

				tipo_ambito = new TipoAmbito();
				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				tipo_flujo.setTipo_ambito(tipo_ambito);

				tipo_ejecucion.setTipo_flujo(tipo_flujo);

				dic_tipo_ejecucion.add(tipo_ejecucion);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los tipos de ejecucion: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	protected void obtenerTiposIndicadorBD() throws Exception {
		dic_tipo_indicador = new ArrayList<TipoIndicador>();
		TipoIndicador tipo_indicador = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM ME_TipoIndicador ORDER BY CodTipoIndicador";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tipo_indicador = new TipoIndicador();
				tipo_indicador.setId_tipo_indicador(rs.getInt("CodTipoIndicador"));
				tipo_indicador.setNombre_indicador(rs.getString("NbrIndicador"));
				dic_tipo_indicador.add(tipo_indicador);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los tipos de indicador: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
	}

	// Aprovisionamiento
	public ArrayList<Ruta> obtenerRutas(Integer id_tipo_ambito) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Ruta ruta = null;
		ArrayList<Ruta> rutas = new ArrayList<Ruta>();

		String sql = "SELECT CodRuta, NbrRuta, FlgActiva,FlgMaestro, DesRuta FROM ME_Ruta WHERE CodTipoAmbito = ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, id_tipo_ambito);

			rs = pstmt.executeQuery();
			while (rs.next()) {

				ruta = new Ruta();
				ruta.setId_ruta(rs.getInt("CodRuta"));
				ruta.setRuta(rs.getString("NbrRuta"));
				ruta.setFlag_activa(rs.getBoolean("FlgActiva"));
				ruta.setFlag_maestro(rs.getBoolean("FlgMaestro"));
				ruta.setDescripcion(rs.getString("DesRuta"));
				RutaValida rutavalida = validarRuta(ruta.getRuta());
				ruta.setPermisosEnRuta(rutavalida.getValido());
				rutas.add(ruta);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las rutas de los ficheros para el tipo ambito: " + id_tipo_ambito
					+ ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return rutas;
	}

	public void crearRuta(Ruta ruta) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		RutaValida rutavalida = new RutaValida();
		String sql = "INSERT INTO ME_Ruta(CodRuta,NbrRuta,CodTipoAmbito,FlgActiva,DesRuta,FlgMaestro,FecActualizacionTabla) "
				+ "VALUES (?,?,?,?,?,?,?)";
		try {
			rutavalida = validarRuta(ruta.getRuta());
			if (rutavalida.getValido()) {
				conn = obtenerConexion();
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, obtenerSiguienteId("Seq_ME_Ruta"));
				pstmt.setString(2, ruta.getRuta());
				pstmt.setInt(3, ruta.getTipo_ambito().getId_tipo_ambito());
				pstmt.setBoolean(4, ruta.getFlag_activa());
				pstmt.setString(5, ruta.getDescripcion());
				pstmt.setBoolean(6, ruta.getFlag_maestro());
				pstmt.setDate(7, new Date(System.currentTimeMillis()));
				pstmt.executeUpdate();
				ejecutarCommit(conn);
			} else {
				throw new CustomException(
						"La ruta: " + ruta.getRuta() + " tiene errores/ " + rutavalida.getObservacion());
			}
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar ruta con id " + ruta.getId_ruta() + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public RutaValida editarRuta(Ruta ruta) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		RutaValida rutavalida = new RutaValida();
		String sql = "UPDATE ME_Ruta SET NbrRuta= ?, FlgActiva= ?, DesRuta= ?, FlgMaestro=?, FecActualizacionTabla= ? where CodRuta= ?";
		try {
			rutavalida = validarRuta(ruta.getRuta());
			if (rutavalida.getValido()) {
				conn = obtenerConexion();
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, ruta.getRuta());
				pstmt.setBoolean(2, ruta.getFlag_activa());
				pstmt.setString(3, ruta.getDescripcion());
				pstmt.setBoolean(4, ruta.getFlag_maestro());
				pstmt.setDate(5, new Date(System.currentTimeMillis()));
				pstmt.setInt(6, ruta.getId_ruta());
				pstmt.executeUpdate();
				ejecutarCommit(conn);

			}

			return rutavalida;
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar ruta con id " + ruta.getId_ruta() + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public RutaValida validarRuta(String ruta) throws Exception {
		RutaValida rutavalida = new RutaValida();
		File carpeta = new File(ruta);
		rutavalida.setValido(true);

		try {
			if (carpeta.exists() && carpeta.isDirectory()) {
				if (!carpeta.canRead()) {
					rutavalida.setValido(false);
					rutavalida.setObservacion("No se tiene permiso de lectura en la ruta");
				} else if (!carpeta.canWrite()) {
					rutavalida.setValido(false);
					rutavalida.setObservacion("No se tiene permiso de escritura en la ruta");
				}
			} else {
				rutavalida.setValido(false);
				rutavalida.setObservacion("No existe o no se tiene acceso a la ruta");
			}
		} catch (Exception ex) {
			LOGGER.error("Error al validar ruta, Error: " + ex.getMessage());
			throw ex;
		}
		return rutavalida;
	}

	public void borrarRuta(Integer id_ruta) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Delete from ME_Ruta where CodRuta= ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_ruta);
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar ruta con id " + id_ruta + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	// ------------------------------------------------------------
	// ------------------------------------------------------------
	// ------------------------------------------------------------

	// Se utiliza para obtener solamente en texto las rutas de donde obtener los
	// ficheros posteriormente
	public ArrayList<String> obtenerSoloRutasTexto(Integer id_tipo_ambito, Boolean flag_maestro) throws Exception {
		PreparedStatement pstm = null;
		Connection conn = null;
		ResultSet rs = null;
		ArrayList<String> rutas_texto = new ArrayList<String>();

		String sql = "SELECT NbrRuta FROM ME_Ruta WHERE CodTipoAmbito = ? AND FlgActiva = 1 AND FlgMaestro = ?";
		try {
			conn = obtenerConexion();
			pstm = conn.prepareStatement(sql);
			pstm.setInt(1, id_tipo_ambito);
			pstm.setBoolean(2, flag_maestro);
			rs = pstm.executeQuery();
			while (rs.next()) {
				rutas_texto.add(rs.getString("NbrRuta"));
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las rutas de los ficheros para el tipo ambito: " + id_tipo_ambito
					+ ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstm);
			cerrarConexion(conn);
		}
		return rutas_texto;
	}

	public ArrayList<CargaFichero> obtenerCargasFicheros(ArrayList<String> rutas) throws Exception {
		File carpeta = null;
		ArrayList<CargaFichero> cargasFichero = new ArrayList<CargaFichero>();
		CargaFichero cargaFichero = null;

		for (String ruta : rutas) {
			try {
				carpeta = new File(ruta);
				if (carpeta.exists()) {
					if (carpeta.canRead()) {
						for (File archivo : carpeta.listFiles()) {
							if (archivo.isDirectory()) {
								// No recoger carpetas ni ficheros dentro de estas subcarpetas.
								LOGGER.debug("Se ha detectado una carpeta en la ruta de ficheros inputs: " + archivo
										+ ". Los ficheros dentro de esta carpeta no seran procesados");
							} else {
								cargaFichero = new CargaFichero();
								cargaFichero.getTablaInput().setRuta(ruta);
								cargaFichero.getTablaInput().setNombre_fichero(archivo.getName().split("\\.")[0]);
								cargaFichero.getTablaInput().setNombre_archivo(archivo.getName());
								cargasFichero.add(cargaFichero);
							}
						}
					} else {
						LOGGER.warn("Backend no tiene permiso de lectura sobre la ruta: " + ruta);
					}
				} else {
					LOGGER.warn("La ruta configurada no existe o no se tiene acceso desde el backend: " + ruta);
				}
			} catch (Exception ex) {
				LOGGER.error("Error al listar archivos de aprovisionamiento de la ruta: " + ruta + ". Error: "
						+ ex.getMessage());
				// throw ex;
			}
		}
		return cargasFichero;
	}

	public ArrayList<Aprovisionamiento> obtenerFicherosMetadata(ArrayList<String> rutas) throws Exception {
		File carpeta = null;
		ArrayList<Aprovisionamiento> aprovs = new ArrayList<Aprovisionamiento>();
		Aprovisionamiento aprov = null;

		for (String ruta : rutas) {
			try {
				carpeta = new File(ruta);
				if (carpeta.exists()) {
					if (carpeta.canRead()) {
						for (File archivo : carpeta.listFiles()) {
							if (archivo.isDirectory()) {
								// No recoger carpetas ni ficheros dentro de estas subcarpetas.
								LOGGER.info("Se ha detectado una carpeta en la ruta de ficheros inputs: " + archivo
										+ ". Los ficheros dentro de esta carpeta no seran procesados");
							} else {
								aprov = new Aprovisionamiento();
								aprov.setRuta(ruta);
								aprov.setNombre_fichero(archivo.getName().split("\\.")[0]);
								aprov.setNombre_archivo(archivo.getName());
								aprovs.add(aprov);
							}
						}
					} else {
						LOGGER.warn("No se tiene permiso de lectura sobre la ruta: " + ruta);
					}
				} else {
					LOGGER.warn("La ruta configurada no existe o no tiene acceso: " + ruta);
				}
			} catch (Exception ex) {
				LOGGER.error("Error al listar archivos de aprovisionamiento de la ruta: " + ruta + ". Error: "
						+ ex.getMessage());
				// throw ex;
			}
		}
		return aprovs;
	}

	public void editarFichero(Aprovisionamiento aprov) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		editarFichero(aprov, conn);
		cerrarConexion(conn);
	}

	private void editarFichero(Aprovisionamiento aprov, Connection conn) throws Exception {
		PreparedStatement pstmt_cf = null;
		String sql_cf = "UPDATE HE_CargaFichero SET CodFecha = ?, NbrCarga = ?, FecActualizacionTabla=? WHERE CodCargaFichero = ?";
		try {
			pstmt_cf = conn.prepareStatement(sql_cf);
			pstmt_cf.setDate(3, new Date(System.currentTimeMillis()));
			pstmt_cf.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar el fichero: " + aprov.getId_aprovisionamiento() + " al aprovisionar / "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_cf);
		}
	}

	// Obtiene el fichero especifico en una ruta o rutas específicas
	public Aprovisionamiento obtenerFicheroConCampos(Aprovisionamiento fichero, ArrayList<String> rutas)
			throws Exception {
		// Aprovisionamiento aprovisionamientoTemporal = new Aprovisionamiento();
		try {
			// rutas.add(fichero.getRuta());
			ArrayList<Aprovisionamiento> aprovisionamientos = obtenerFicherosMetadata(rutas);
			for (Aprovisionamiento aprov : aprovisionamientos) {
				if (fichero.getNombre_fichero().equalsIgnoreCase(aprov.getNombre_fichero())) {
					fichero.setNombre_archivo(aprov.getNombre_archivo());
					fichero.setRuta(aprov.getRuta());
					fichero = obtenerCamposAprovisionamientoParaCreacion(fichero);
					break;
				}
			}
			if (fichero.getNombre_archivo() == null) {
				throw new CustomException("No se encontro el fichero con el nombre:" + fichero.getNombre_fichero());
			}

		} catch (Exception ex) {
			LOGGER.error("Error campos del fichero " + fichero.getNombre_fichero() + ". Error: " + ex.getMessage());
			throw ex;
		}
		return fichero;
	}

	public void crearCampoAprovisionamiento(CampoAprovisionamiento campo,
			ArrayList<SegmentoAprovisionamiento> segmentos, Integer id_aprov) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearCampoAprovisionamiento(campo, segmentos, id_aprov, conn);
		cerrarConexion(conn);
	}

	private void crearCampoAprovisionamiento(CampoAprovisionamiento campo,
			ArrayList<SegmentoAprovisionamiento> segmentos, Integer id_aprov, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		// Integer beginIndex = 0;
		// Integer endIndex = 0;
		// String substr_1 = null;

		String sql = "Insert into HE_CAMPOAPROVISIONAMIENTO (CODCAMPOAPROVISIONAMIENTO,"
				+ "CODAPROVISIONAMIENTO,CODTIPOCAMPO,CODTIPOVARIABLE,NBRCAMPO,FLGCAMPOORIGINAL,"
				+ "FLGBORRARCAMPO,FLGLLAVE,FLGSEGMENTAR,FLGCRITICA, FLGEVIDENCIA, DesFormulaNuevoCampoInput,"
				+ "DesFormulaNuevoCampoSql,FECACTUALIZACIONTABLA) " + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		try {
			// Agregamos los campos aprovisionamiento
			pstmt = conn.prepareStatement(sql);

			campo.setId_campo_aprovisionamiento(obtenerSiguienteId("SEQ_HE_CampoAprovisionamiento"));
			pstmt.setInt(1, campo.getId_campo_aprovisionamiento());
			pstmt.setInt(2, id_aprov);
			pstmt.setInt(3, campo.getTipo_campo().getId_tipo_campo());
			pstmt.setInt(4, campo.getTipo_variable().getId_tipo_variable());
			pstmt.setString(5, campo.getNombre_campo());
			// Setear campo original
			pstmt.setBoolean(6, campo.getFlag_campo_original());
			pstmt.setBoolean(7, !campo.getFlag_aprovisionar());
			pstmt.setBoolean(8, campo.getFlag_llave());
			pstmt.setBoolean(9, campo.getFlag_segmentar());
			pstmt.setBoolean(10, campo.getFlag_critica());
			pstmt.setBoolean(11, campo.getFlag_evidencia() == null ? false : campo.getFlag_evidencia());
			pstmt.setString(12, campo.getFormula_nuevo_campo_input());
			pstmt.setString(13, campo.getFormula_nuevo_campo_sql());
			pstmt.setDate(14, new Date(System.currentTimeMillis()));
			pstmt.addBatch();
			pstmt.executeBatch();

			// ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar Campo Fichero:" + campo.getId_campo() + " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			for (SegmentoAprovisionamiento segmento : segmentos) {
				this.crearReglasCalidad(campo, segmento, conn);
			}
		}
	}

	public void crearAprovisionamiento(Aprovisionamiento fichero) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearAprovisionamiento(fichero, conn);
		cerrarConexion(conn);
	}

	private void crearAprovisionamiento(Aprovisionamiento fichero, Connection conn) throws Exception {
		PreparedStatement pstmt_aprovisionamiento = null;
		Integer id_aprovisionamiento = null;
		ArrayList<CampoAprovisionamiento> campos = null;
		SegmentoAprovisionamiento segmentoSinSeg = null;
		ArrayList<SegmentoAprovisionamiento> segmentos = new ArrayList<SegmentoAprovisionamiento>();
		String sql_aprovisionamiento = "Insert into HE_APROVISIONAMIENTO (CODAPROVISIONAMIENTO,"
				+ "NBRFICHERO,FLGOBLIGATORIO,FLGFECHAAUTOMATICA,CODTIPOAMBITO,FECACTUALIZACIONTABLA,FLGCUENTAOPERACION)"
				+ "values (?,?,?,?,?,?,?)";

		try {
			// new Date(System.currentTimeMillis())
			pstmt_aprovisionamiento = conn.prepareStatement(sql_aprovisionamiento);
			id_aprovisionamiento = obtenerSiguienteId("seq_he_aprovisionamiento");
			fichero.setId_aprovisionamiento(id_aprovisionamiento);
			pstmt_aprovisionamiento.setInt(1, fichero.getId_aprovisionamiento());
			pstmt_aprovisionamiento.setString(2, fichero.getNombre_fichero());
			pstmt_aprovisionamiento.setBoolean(3, fichero.getFlag_obligatorio());
			pstmt_aprovisionamiento.setBoolean(4, fichero.getFlag_fechaautomatica());
			pstmt_aprovisionamiento.setInt(5, fichero.getTipo_ambito().getId_tipo_ambito());
			pstmt_aprovisionamiento.setDate(6, new Date(System.currentTimeMillis()));
			pstmt_aprovisionamiento.setBoolean(7, fichero.getFlag_operacion());
			// Establecer los valores para setear los flag en la base de datos
			pstmt_aprovisionamiento.executeUpdate();

			int it_campo = 0;
			for (CampoAprovisionamiento campo_aprovisionamiento : fichero.getCampos_aprovisionamiento()) {
				// seteamos siempre la primera varaible como llave.
				if (it_campo == 0) {
					campo_aprovisionamiento.setFlag_llave(true);
				}
				it_campo++;
				// Creamos el Campo de Aprovisionamiento, al ser la primera vez que se crea se
				// mandan segmentos en null para evitar la creacion de Reglas de Calidad
				crearCampoAprovisionamiento(campo_aprovisionamiento, segmentos, fichero.getId_aprovisionamiento(),
						conn);
			}

			this.CrearMapeoFicheroCarteraPorFichero(fichero.getId_aprovisionamiento(), fichero.getCarteras(), conn);

			ejecutarCommit(conn);
			// creacion de Sin Segmento
			campos = obtenerCampos(fichero, conn);
			segmentoSinSeg = new SegmentoAprovisionamiento();
			segmentoSinSeg.setFlag_sinsegmento(true);
			segmentoSinSeg.setDescripcion("Sin Segmento");
			crearCampoSegmento(segmentoSinSeg, campos, id_aprovisionamiento, conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar el fichero:" + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_aprovisionamiento);
		}
	}

	public void editarAprovisionamiento(Aprovisionamiento aprovisionamiento) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		editarAprovisionamiento(aprovisionamiento, conn);
		cerrarConexion(conn);
	}

	private void editarAprovisionamiento(Aprovisionamiento aprovisionamiento, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "UPDATE He_Aprovisionamiento set NbrFichero=?, FlgObligatorio=?,"
				+ " FlgFechaAutomatica=?, FLGCUENTAOPERACION=?, CodTipoAmbito=?,"
				+ " FecActualizacionTabla =? WHERE CodAprovisionamiento=?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, aprovisionamiento.getNombre_fichero());
			pstmt.setBoolean(2, aprovisionamiento.getFlag_obligatorio());
			pstmt.setBoolean(3, aprovisionamiento.getFlag_fechaautomatica());
			pstmt.setBoolean(4, aprovisionamiento.getFlag_operacion());
			pstmt.setInt(5, aprovisionamiento.getTipo_ambito().getId_tipo_ambito());
			pstmt.setDate(6, new Date(System.currentTimeMillis()));
			pstmt.setInt(7, aprovisionamiento.getId_aprovisionamiento());
			pstmt.executeUpdate();

			this.borrarMapeoFicheroCarteraPorFichero(aprovisionamiento.getId_aprovisionamiento(), conn);
			this.CrearMapeoFicheroCarteraPorFichero(aprovisionamiento.getId_aprovisionamiento(),
					aprovisionamiento.getCarteras(), conn);

			editarCamposAprovisionamiento(aprovisionamiento, conn);
			// for (CampoAprovisionamiento
			// campo:aprovisionamiento.getCampos_aprovisionamiento()) {
			// editarCampoAprovisionamiento(campo,conn);
			// }

//			for (CampoAprovisionamiento campo : aprovisionamiento.getCampos_aprovisionamiento()) {
//				editarCampoAprovisionamiento(campo, conn);
//			}

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar el aprovisionamiento con ID: " + aprovisionamiento.getId_aprovisionamiento()
					+ " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void borrarAprovisionamiento(Integer id_aprovisionamiento) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		borrarAprovisionamiento(id_aprovisionamiento, conn);
		cerrarConexion(conn);
	}

	private void borrarAprovisionamiento(Integer id_aprovisionamiento, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		Aprovisionamiento aprov = new Aprovisionamiento();
		String sql_borrar_fichero = "DELETE FROM HE_APROVISIONAMIENTO WHERE codaprovisionamiento=?";
		ArrayList<CampoAprovisionamiento> campos_aprovisionamiento = null;
		ArrayList<SegmentoAprovisionamiento> segmentos = null;
		ArrayList<TablaInput> tablas = null;
		// ArrayList<Cartera> carteras = new ArrayList<Cartera>();
		ArrayList<CargaFichero> cargasFlagOficial = obtenerCargasFlagOficial(id_aprovisionamiento, conn);
		try {
			if (cargasFlagOficial.size() <= 0) {
				pstmt = conn.prepareStatement(sql_borrar_fichero);
				aprov.setId_aprovisionamiento(id_aprovisionamiento);
				CampoAprovisionamiento campoNull = new CampoAprovisionamiento();
				aprov.getCampos_aprovisionamiento().add(campoNull);
				campos_aprovisionamiento = obtenerCampos(aprov, conn);
				segmentos = obtenerSegmentosPorAprov(aprov.getId_aprovisionamiento());
				tablas = obtenerTablasInputPorAprov(id_aprovisionamiento, conn);
				// carteras = obtenerCarterasDeAprovisionamientoInput(id_aprovisionamiento,
				// conn);
				for (CampoAprovisionamiento campo_aprovisionamiento : campos_aprovisionamiento) {
					this.borrarCampoAprovisionamiento(campo_aprovisionamiento.getId_campo_aprovisionamiento(), conn);
				}
				for (SegmentoAprovisionamiento segmentoAprovisionamiento : segmentos) {
					borrarCampoSegmento(segmentoAprovisionamiento.getId_segmento(), conn);
				}
				this.borrarMapeoFicheroCarteraPorFichero(id_aprovisionamiento, conn);
				for (TablaInput tabla : tablas) {
					this.borrarTablaInput(tabla, conn);
				}
				pstmt.setInt(1, id_aprovisionamiento);
				pstmt.executeUpdate();
				ejecutarCommit(conn);
			} else {
				throw new CustomException("Existe una carga oficial vinculada a este aprovisionamiento");
			}
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar el fichero con id: " + id_aprovisionamiento + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	/*
	 * public void borrarMapeoFicheroCarteraPorFichero(Integer id_aprovisionamiento)
	 * throws Exception { Connection conn = null; conn = obtenerConexion();
	 * borrarMapeoFicheroCarteraPorFichero(id_aprovisionamiento, conn);
	 * cerrarConexion(conn); }
	 */
	private void CrearMapeoFicheroCarteraPorFichero(Integer id_aprovisionamiento, ArrayList<Cartera> carteras,
			Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "Insert into HE_MAPEOFICHEROCARTERA (CODCARTERA,CODAPROVISIONAMIENTO,FECACTUALIZACIONTABLA) values (?,?,?)";

		try {
			// Agregamos el mapeo_cartera_fichero
			pstmt = conn.prepareStatement(sql);
			for (Cartera cartera : carteras) {
				pstmt.setInt(1, cartera.getId_cartera());
				pstmt.setInt(2, id_aprovisionamiento);
				pstmt.setDate(3, new Date(System.currentTimeMillis()));
				pstmt.addBatch();
			}
			pstmt.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar el mapeoFichero con id's: " + id_aprovisionamiento + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	private void borrarMapeoFicheroCarteraPorFichero(Integer id_aprovisionamiento, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "DELETE FROM HE_MAPEOFICHEROCARTERA" + " WHERE codaprovisionamiento=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_aprovisionamiento);
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar el mapeoFichero con id's: " + id_aprovisionamiento + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	private void borrarTablaInput(TablaInput tabla_input, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "DELETE FROM HE_TABLAINPUT" + " WHERE CODTABLAINPUT=?";
		try {
			limpiarMapeoTablaBloquedeIdTablaInput(tabla_input.getId_tablainput(), conn);
			borrarCargasDeTablaInput(tabla_input.getId_tablainput(), conn);
			if (tabla_input.getFlag_normalizada()) {
				for (CampoInput campo_input : tabla_input.getCampos_input()) {
					limpiarRegistroCampoBloquedeIdCampoInput(campo_input.getId_campo(), conn);
					// borrarRegistrosDeCampoInputNormalizado(campo_input, conn);
				}
				borrarTablaNoNormalizada(tabla_input, false, conn);
			} else {
				for (CampoInput campo_input : tabla_input.getCampos_input()) {
					limpiarRegistroCampoBloquedeIdCampoInput(campo_input.getId_campo(), conn);
				}
				borrarTablaNoNormalizada(tabla_input, false, conn);
			}
			borrarCamposInput(tabla_input, conn);
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, tabla_input.getId_tablainput());
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	private void limpiarMapeoTablaBloquedeIdTablaInput(Integer id_tabla_input, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
//		ArrayList<BloqueRegistro> bloquesRegistro = obtenerMapeoTablaporIdTablaInput(id_tabla_input, conn);
		String sql = "UPDATE HE_MapeoTablaBloque set CODTABLAINPUT=null where CODTABLAINPUT=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_tabla_input);
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar los mapeos_tabla_bloque con id_Tabla_Input: " + id_tabla_input + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	private void borrarCargasDeTablaInput(Integer id_tabla_input, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "DELETE FROM HE_CARGAFICHERO" + " WHERE CODTABLAINPUT=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_tabla_input);
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar las cargas de Tabla_Input con id_Tabla_Input: " + id_tabla_input + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	private void limpiarRegistroCampoBloquedeIdCampoInput(Integer id_campo_input, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "UPDATE HE_REGISTROCAMPOBLOQUE set CODCAMPO=null where CODCAMPO=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_campo_input);
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar los REGISTROCAMPOBLOQUE con id_Campo_Input: " + id_campo_input + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	private void borrarCamposInput(TablaInput tabla_input, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "DELETE FROM HE_CAMPOINPUT" + " WHERE CODTABLAINPUT=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, tabla_input.getId_tablainput());
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar los campos de Tabla_Input con id_Tabla_Input: "
					+ tabla_input.getId_tablainput() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void borrarTablaNoNormalizada(TablaInput tablainput) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		borrarTablaNoNormalizada(tablainput, false, conn);
		cerrarConexion(conn);
	}

	public void borrarTablaNoNormalizada(TablaInput tablainput, Boolean sinThrow, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql_borrar_tabla = "DROP TABLE " + userName + ".HE_" + tablainput.getNombre_fichero() + "_v"
				+ tablainput.getVersion_metadata() + " CASCADE CONSTRAINTS";

		try {
			// borramos la tabla No normalizada
			pstmt = conn.prepareStatement(sql_borrar_tabla);
			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);

			if (sinThrow) {
				LOGGER.error("Error al borrar la tabla no normalizada con id_tabla: " + tablainput.getId_tablainput()
						+ " : " + ex.getMessage());
			} else {
				LOGGER.error("Error al borrar la tabla no normalizada con id_tabla: " + tablainput.getId_tablainput()
						+ " : " + ex.getMessage());
				throw ex;
			}

		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public TablaInput obtenerCamposdeCargaFichero(TablaInput fichero) throws Exception {
		BufferedReader br = null;
		Character separador_fichero = null;
		Boolean separador_valido = false;
		String cabecera = null;
		CampoFichero campofichero = null;
		ArrayList<String> campos = null;
		ArrayList<CampoFichero> campos_fichero = null;
		try {
			br = new BufferedReader(new FileReader(fichero.getRuta() + "\\" + fichero.getNombre_archivo()));
			cabecera = br.readLine();

			for (int i = 0; i < separador.length(); i++) {
				separador_fichero = separador.charAt(i);
				if (cabecera.contains(separador_fichero.toString())) {
					fichero.setSeparador(separador_fichero.toString());
					separador_valido = true;
					break;
				}
			}

			// treta para que acepte unicampos
			if (!separador_valido) {
				if (cabecera.length() < 30) {
					fichero.setSeparador("asdfghjkloiuytrewq");
					separador_valido = true;
				}
			}

			if (separador_valido) {
				campos_fichero = new ArrayList<CampoFichero>();
				// Se separan los campos por el separador configurado para el modulo de archivos
				campos = separarCampos(cabecera, fichero.getSeparador());
				for (String c : campos) {
					// Se quitan las doble comillas que delimitaba el inicio y fin del texto del
					// campo
					c = c.replace("\"", "");
					campofichero = new CampoFichero();
					if (patron_cabecera.matcher(c).matches()) {
						campofichero.setNombre_campo(c);
					} else {
						LOGGER.warn("El fichero " + fichero.getNombre_fichero()
								+ " tiene un campo con caracteres invalidos: " + c);
						fichero.setFlag_error_cabeceras(true);
						fichero.setObservacion("Cabecera con caracteres indebidos");
					}
					campos_fichero.add(campofichero);
				}
				fichero.setCampos_fichero(campos_fichero);
			} else {
				fichero.setFlag_error_separador(true);
				fichero.setObservacion("Separador indebido");
				throw new CustomException("El fichero " + fichero.getNombre_fichero()
						+ " no contiene el separador configurado en el sistema: " + separador
						+ " y la cabecera tiene mas de 30 caracteres para poder considerarlo como un unico campo.");
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener campos del fichero: " + fichero.getNombre_fichero() + ". Error: "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarBufferedReader(br);
		}
		return fichero;
	}

	private Aprovisionamiento obtenerCamposAprovisionamientoParaCreacion(Aprovisionamiento fichero) throws Exception {
		BufferedReader br = null;
		Character separador_fichero = null;
		Boolean separador_valido = false;
		String cabecera = null;
		CampoAprovisionamiento campoAprovisionamiento = null;
		ArrayList<String> campos = null;
		ArrayList<CampoAprovisionamiento> campos_aprovisionamiento = null;
		try {
			br = new BufferedReader(new FileReader(fichero.getRuta() + "\\" + fichero.getNombre_archivo()));
			cabecera = br.readLine();

			for (int i = 0; i < separador.length(); i++) {
				separador_fichero = separador.charAt(i);
				if (cabecera.contains(separador_fichero.toString())) {
					fichero.setSeparador(separador_fichero.toString());
					separador_valido = true;
					break;
				}
			}

			// treta para que acepte unicampos
			if (!separador_valido) {
				if (cabecera.length() < 30) {
					fichero.setSeparador("asdfghjkloiuytrewq");
					separador_valido = true;
				}
			}

			if (separador_valido) {
				campos_aprovisionamiento = new ArrayList<CampoAprovisionamiento>();
				// Se separan los campos por el separador configurado para el modulo de archivos
				campos = separarCampos(cabecera, fichero.getSeparador());
				for (String c : campos) {
					// Se quitan las doble comillas que delimitaba el inicio y fin del texto del
					// campo
					c = c.replace("\"", "");
					campoAprovisionamiento = new CampoAprovisionamiento();
					if (patron_cabecera.matcher(c).matches()) {
						campoAprovisionamiento.setNombre_campo(c);
					} else {
						cerrarBufferedReader(br); // adicional?
						throw new CustomException("El fichero " + fichero.getNombre_fichero()
								+ " tiene un campo con caracteres invalidos: " + c);
					}
					campos_aprovisionamiento.add(campoAprovisionamiento);
				}
				fichero.setCampos_aprovisionamiento(campos_aprovisionamiento);
			} else {
				fichero.setFlag_error_separador(true);
				fichero.setObservacion("Separador indebido");
				throw new CustomException("El fichero " + fichero.getNombre_fichero()
						+ " no contiene el separador configurado en el sistema: " + separador
						+ " y la cabecera tiene mas de 30 caracteres para poder considerarlo como un unico campo.");
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener campos del fichero: " + fichero.getNombre_fichero() + ". Error: "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarBufferedReader(br);
		}
		return fichero;
	}

	public TablaInput obtenerVersionMetadata(TablaInput aprov) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql_version = "select T.CodTablaInput, T.NbrMetadata from HE_TablaInput T"
				+ " where T.CodAprovisionamiento=?" + " and T.NumVersionMetadata = (Select max(NumVersionMetadata)"
				+ " from HE_TablaInput where CodAprovisionamiento=?)";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql_version);
			pstmt.setInt(1, aprov.getId_aprovisionamiento());
			pstmt.setInt(2, aprov.getId_aprovisionamiento());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				aprov.setVersion_metadata(rs.getInt("CodTablaInput"));
				aprov.setNombre_metadata(rs.getString("NbrMetadata"));
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las Version de Metadata y Carga del Aprov:  " + aprov.getNombre_fichero()
					+ ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return aprov;
	}

	// Se utiliza para obtener el ultimo aprovisionamiento de un fichero
	public TablaInput obtenerUltimoAprovisionamiento(TablaInput aprov) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt_campos = null;
		ResultSet rs = null;
		ResultSet rs_campos = null;
		String sql = "SELECT CodAprovisionamiento, NbrFichero, FlgCuentaOperacion FROM HE_Aprovisionamiento"
				+ " where NbrFichero=?";

		String sql_campos = " SELECT CA.CodCampoAprovisionamiento, CA.NbrCampo, CA.CodTipoCampo, TC.NbrTipoCampo,"
				+ " CA.CodTipoVariable, TV.NbrTipoVariable, CA.FlgCampoOriginal, CA.FlgBorrarCampo, CA.FlgLlave,"
				+ " CA.FlgEvidencia, CA.FlgSegmentar, CA.FlgCritica, CA.DesFormulaNuevoCampoInput, CA.DesFormulaNuevoCampoSql"
				+ " FROM HE_CampoAprovisionamiento CA "
				+ " LEFT JOIN HE_Aprovisionamiento A on CA.CodAprovisionamiento = A.CodAprovisionamiento"
				+ " LEFT JOIN ME_TipoCampo TC on CA.CodTipoCampo = TC.CodTipoCampo"
				+ " LEFT JOIN ME_TipoVariable TV on CA.CodTipoVariable = TV.CodTipoVariable  WHERE A.NbrFichero=?"
				+ " order by CA.CodCampoAprovisionamiento";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, aprov.getNombre_fichero());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				aprov.setId_aprovisionamiento(rs.getInt("CodAprovisionamiento"));
				aprov.setNombre_fichero(rs.getString("NbrFichero"));
				aprov.setFlag_operacion(rs.getBoolean("FlgCuentaOperacion"));
				// Si se encontraron registros, entonces el fichero si tiene aprovisionamiento
				// anterior
				aprov.setFlag_tiene_aprov(true);
			}
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);

			pstmt_campos = conn.prepareStatement(sql_campos);
			pstmt_campos.setString(1, aprov.getNombre_fichero());
			rs_campos = pstmt_campos.executeQuery();
			while (rs_campos.next()) {
				aprov.agregarCampoAprov(new CampoAprovisionamiento(rs_campos.getInt("CodCampoAprovisionamiento"),
						rs_campos.getString("NbrCampo"),
						new TipoCampo(rs_campos.getInt("CodTipoCampo"), rs_campos.getString("NbrTipoCampo")),
						new TipoVariable(rs_campos.getInt("CodTipoVariable"), rs_campos.getString("NbrTipoVariable")),
						rs_campos.getBoolean("FlgCampoOriginal"), !rs_campos.getBoolean("FlgBorrarCampo"),
						rs_campos.getBoolean("FlgLlave"), rs_campos.getBoolean("FlgSegmentar"),
						rs_campos.getBoolean("FlgCritica"), rs_campos.getBoolean("FlgEvidencia"),
						rs_campos.getString("DesFormulaNuevoCampoInput"),
						rs_campos.getString("DesFormulaNuevoCampoSql")));
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los campos del aprovisionamiento del ultimo fichero: "
					+ aprov.getNombre_fichero() + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarResultSet(rs_campos);
			cerrarPreparedStatement(pstmt_campos);
			cerrarConexion(conn);
		}
		return aprov;
	}

	// -------------- Aprovisionamiento ---------------------

	// Proceso de Aprovisionamiento Para tablas No Normalizadas
	public void crearTablaNoNormalizada(CargaFichero cargaFichero) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearTablaNoNormalizada(cargaFichero, conn);

		cerrarConexion(conn);
	}

	public void crearTablaNoNormalizada(CargaFichero cargaFichero, Connection conn) throws Exception {
		PreparedStatement pstmt = null;

		// nuevo: obtenemos el orden de la tabla a crear, a partir del fichero maestro

		Aprovisionamiento aprov = new Aprovisionamiento();
		aprov.setNombre_fichero(cargaFichero.getTablaInput().getNombre_fichero());

		ArrayList<String> rutas = obtenerSoloRutasTexto(
				cargaFichero.getTablaInput().getTipo_ambito().getId_tipo_ambito(), true);
		aprov = obtenerFicheroConCampos(aprov, rutas);

		String sql_crear_tabla_campos = "";
		ArrayList<String> array_crear_campos = new ArrayList<String>();

		// parte para variables originales
		for (CampoAprovisionamiento campo : aprov.getCampos_aprovisionamiento()) {

			for (CampoAprovisionamiento campo_carga : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
				if (campo.getNombre_campo().equals(campo_carga.getNombre_campo())
						&& campo_carga.getFlag_campo_original()) {
					campo.setTipo_campo(campo_carga.getTipo_campo());
					campo.setFlag_aprovisionar(campo_carga.getFlag_aprovisionar());
					break;
				}
			}

			if (campo.getFlag_aprovisionar()) {
				if (campo.getTipo_campo().getId_tipo_campo().equals(1)) {
					array_crear_campos.add(campo.getNombre_campo() + " INTEGER");
				} else if (campo.getTipo_campo().getId_tipo_campo().equals(2)) {
					array_crear_campos.add(campo.getNombre_campo() + " VARCHAR2(1)"); // 800
				} else if (campo.getTipo_campo().getId_tipo_campo().equals(3)) {
					array_crear_campos.add(campo.getNombre_campo() + " NUMBER(34,12)"); // 800
				}
			}
		}

		// parte para variables calculadas
		for (CampoAprovisionamiento campo_carga : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
			if (!campo_carga.getFlag_campo_original() && campo_carga.getFlag_aprovisionar()) {
				if (campo_carga.getTipo_campo().getId_tipo_campo().equals(1)) {
					array_crear_campos.add(campo_carga.getNombre_campo() + " INTEGER ");
				} else if (campo_carga.getTipo_campo().getId_tipo_campo().equals(2)) {
					array_crear_campos.add(campo_carga.getNombre_campo() + " VARCHAR2(1) "); // 800
				} else if (campo_carga.getTipo_campo().getId_tipo_campo().equals(3)) {
					array_crear_campos.add(campo_carga.getNombre_campo() + " NUMBER(34,12) "); // 800
				}
			}
		}

		sql_crear_tabla_campos = String.join(",", array_crear_campos);

		String sql_crear_tabla = "CREATE TABLE " + userName + ".HE_" + cargaFichero.getTablaInput().getNombre_fichero()
				+ "_v" + cargaFichero.getTablaInput().getVersion_metadata()
				+ " (CodCargaFichero INTEGER NOT NULL, NumOrden INTEGER NOT NULL, FECCALCULORES INTEGER NOT NULL, "
				+ sql_crear_tabla_campos + ", CONSTRAINT " + "PK_" + cargaFichero.getTablaInput().getNombre_fichero()
				+ "_v" + cargaFichero.getTablaInput().getVersion_metadata() + " PRIMARY KEY  (CodCargaFichero, NumOrden) ) ";

		try {

			// Agregamos el mapeo_cartera_fichero
			//LOGGER.info(sql_crear_tabla);
			pstmt = conn.prepareStatement(sql_crear_tabla);
			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear tabla no normalizada: " + cargaFichero.getTablaInput().getNombre_fichero()
					+ ". Error: " + ex.getMessage());
		} finally {
			cerrarPreparedStatement(pstmt);
		}

		try {

			pstmt = conn.prepareStatement(agregarPartitionByCodigoCargaFichero(cargaFichero));
			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear particiones durante el aprovisionamiento: "
					+ cargaFichero.getTablaInput().getNombre_fichero() + ". Error: " + ex.getMessage());
		} finally {
			cerrarPreparedStatement(pstmt);
		}
		
		
		LOGGER.info("se envía ejecutar la query " + agregarIndex(cargaFichero));
		try {
				pstmt = conn.prepareStatement(agregarIndex(cargaFichero));
				pstmt.addBatch();
				pstmt.executeBatch();
				ejecutarCommit(conn);

			} catch (Exception ex) {
				ejecutarRollback(conn);
				LOGGER.error("Error al crear indices durante el aprovisionamiento: "
						+ cargaFichero.getTablaInput().getNombre_fichero() + ". Error: " + ex.getMessage());
			} finally {
				cerrarPreparedStatement(pstmt);
			}

	}

	public void crearTablaNoNormalizadaTemp(CargaFichero cargaFichero, Connection conn) throws Exception {
		PreparedStatement pstmt = null;

		// nuevo: obtenemos el orden de la tabla a crear, a partir del fichero maestro

		Aprovisionamiento aprov = new Aprovisionamiento();
		aprov.setNombre_fichero(cargaFichero.getTablaInput().getNombre_fichero());

		ArrayList<String> rutas = obtenerSoloRutasTexto(
				cargaFichero.getTablaInput().getTipo_ambito().getId_tipo_ambito(), true);
		aprov = obtenerFicheroConCampos(aprov, rutas);

		String sql_crear_tabla_campos = "";
		ArrayList<String> array_crear_campos = new ArrayList<String>();

		// parte para variables originales
		for (CampoAprovisionamiento campo : aprov.getCampos_aprovisionamiento()) {

			for (CampoAprovisionamiento campo_carga : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
				if (campo.getNombre_campo().equals(campo_carga.getNombre_campo())
						&& campo_carga.getFlag_campo_original()) {
					campo.setTipo_campo(campo_carga.getTipo_campo());
					campo.setFlag_aprovisionar(campo_carga.getFlag_aprovisionar());
					break;
				}
			}

			if (campo.getFlag_aprovisionar()) {
				if (campo.getTipo_campo().getId_tipo_campo().equals(1)) {
					array_crear_campos.add(campo.getNombre_campo() + " INTEGER");
				}
			} else if (campo.getTipo_campo().getId_tipo_campo().equals(2)) {
				array_crear_campos.add(campo.getNombre_campo() + " VARCHAR2(1) "); // 800
			} else if (campo.getTipo_campo().getId_tipo_campo().equals(3)) {
				array_crear_campos.add(campo.getNombre_campo() + " NUMBER(34,12) "); // 800
			}
		}

		// parte para variables calculadas
		for (CampoAprovisionamiento campo_carga : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
			if (!campo_carga.getFlag_campo_original() && campo_carga.getFlag_aprovisionar()) {
				if (campo_carga.getTipo_campo().getId_tipo_campo().equals(1)) {
					array_crear_campos.add(campo_carga.getNombre_campo() + " INTEGER");
				} else if (campo_carga.getTipo_campo().getId_tipo_campo().equals(2)) {
					array_crear_campos.add(campo_carga.getNombre_campo() + " VARCHAR2(1) "); // 800
				} else if (campo_carga.getTipo_campo().getId_tipo_campo().equals(3)) {
					array_crear_campos.add(campo_carga.getNombre_campo() + " NUMBER(34,12) "); // 800
				}
			}
		}

		sql_crear_tabla_campos = String.join(",", array_crear_campos);

		String sql_crear_tabla = "CREATE TABLE " + userName + ".HE_" + cargaFichero.getTablaInput().getNombre_fichero()
				+ "_v" + cargaFichero.getTablaInput().getVersion_metadata() + "_temp"
				+ " (CodCargaFichero INTEGER NOT NULL, NumOrden INTEGER NOT NULL, FECCALCULORES INTEGER NOT NULL, "
				+ sql_crear_tabla_campos + " )"/* + agregarPartitionByCodigoCargaFichero(cargaFichero) */;

		try {
			//LOGGER.info(sql_crear_tabla);
			// Agregamos el mapeo_cartera_fichero0
			pstmt = conn.prepareStatement(sql_crear_tabla);
			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear tabla no normalizada: " + cargaFichero.getTablaInput().getNombre_fichero()
					+ ". Error: " + ex.getMessage());
		} finally {
			cerrarPreparedStatement(pstmt);
		}

	}

	public AprovProcesado procesaraprovNuevoNoNormalizado(CargaFichero cargaFichero, Integer id_carga_fichero)
			throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		AprovProcesado procesado = procesaraprovNuevoNoNormalizado(cargaFichero, id_carga_fichero, conn);
		cerrarConexion(conn);
		return procesado;
	}

	public AprovProcesado procesaraprovNuevoNoNormalizado(CargaFichero cargaFichero, Integer id_carga_fichero,
			Connection conn) throws Exception {
		BufferedReader br = null;
		AprovProcesado procesado = new AprovProcesado();
		Integer nro_linea = 0;
		Integer nro_campos = 0;
		String linea_input = null;
		ArrayList<String> campos = null;
		ArrayList<String> cabecera = null;

		ArrayList<Boolean> error = new ArrayList<Boolean>();
		Character separador_fichero = null;

		int index_insert = 0;
		int index_fichero = 0;

		PreparedStatement pstmt_campos = null;
		PreparedStatement pstmt_registro = null;

		String sql_campos = "INSERT INTO HE_CampoInput ( CodCampo, CodTablaInput, CodTipoCampo, NbrCampo, FecActualizacionTabla) values (?,?,?,?,?)";

		String sql_registro_nombres_campos = "";
		String sql_registro_registros_campos = "";

		ArrayList<String> array_nombres_campos = new ArrayList<String>();
		ArrayList<String> array_registros_campos = new ArrayList<String>();

		for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
			array_nombres_campos.add(campo.getNombre_campo());
			array_registros_campos.add("?");
		}
		sql_registro_nombres_campos = String.join(",", array_nombres_campos);
		sql_registro_registros_campos = String.join(",", array_registros_campos);

		String sql_registro = "INSERT INTO " + userName + ".HE_" + cargaFichero.getTablaInput().getNombre_fichero()
				+ "_v" + cargaFichero.getTablaInput().getVersion_metadata()
				+ " (CodCargaFichero, NumOrden,FECCALCULORES, " + sql_registro_nombres_campos + ") values (?,?,?,"
				+ sql_registro_registros_campos + ")";

		try {

			// Antes de insertar los registros, se inserta la metadata (tabla y campos)
			pstmt_campos = conn.prepareStatement(sql_campos);
			for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
				LOGGER.debug("aprov: " + cargaFichero.getTablaInput().getNombre_fichero());
				LOGGER.debug("Campo: " + campo.getNombre_campo());
				campo.setId_campo(obtenerSiguienteId("seq_he_campoinput"));
				campo.setAncho_campo(0);
				pstmt_campos.setInt(1, campo.getId_campo());
				pstmt_campos.setInt(2, cargaFichero.getTablaInput().getId_tablainput());
				pstmt_campos.setInt(3, campo.getTipo_campo().getId_tipo_campo());
				pstmt_campos.setString(4, campo.getNombre_campo());
				pstmt_campos.setDate(5, new Date(System.currentTimeMillis()));
				pstmt_campos.addBatch();
				LOGGER.debug("Procesado a campo input temporal: " + campo.getNombre_campo());
			}
			pstmt_campos.executeBatch();
			cerrarPreparedStatement(pstmt_campos);

			// Se lee completamente el aprov para insertar los registros

			br = new BufferedReader(new FileReader(
					cargaFichero.getTablaInput().getRuta() + "\\" + cargaFichero.getTablaInput().getNombre_archivo()));

			nro_campos = cargaFichero.getTablaInput().getCampos_aprovisionamiento().size();
			LOGGER.info("Procesando el aprov " + cargaFichero.getTablaInput().getNombre_fichero() + " con " + nro_campos
					+ " campos");
			// Se recorren todas las lineas del aprov input

			pstmt_registro = conn.prepareStatement(sql_registro);
			// LOGGER.debug("Dato:" + br.readLine());

			// Estado de error 1: Error / 0: Correcto
			for (int i = 0; i < cargaFichero.getTablaInput().getCampos_aprovisionamiento().size(); i++) {
				error.add(false);
			}

			Double numero_insert = 0.0;
			String numero_insert_str = "";

			while ((linea_input = br.readLine()) != null) {
				// de la primera linea se obtiene el separador
				if (nro_linea == 0) {
					// cargamos un separador dummy, en caso de que no encuentre el separador
					// configurado intentara cargarlo como unicampo
					cargaFichero.getTablaInput().setSeparador("asdfgtrewq");
					for (int i = 0; i < separador.length(); i++) {
						separador_fichero = separador.charAt(i);
						if (linea_input.contains(separador_fichero.toString())) {
							cargaFichero.getTablaInput().setSeparador(separador_fichero.toString());
							break;
						}
					}
					cabecera = separarCampos(linea_input, cargaFichero.getTablaInput().getSeparador());
				}

				// Se comprueba si el numero de campos es consistente en cada linea
				campos = separarCampos(linea_input, cargaFichero.getTablaInput().getSeparador());

				index_insert = 0;
				index_fichero = 0;
				if (nro_campos == campos.size()) {
					// Solo se desea procesar los registros, no la primera linea (posicion 0) que
					// tiene los campos
					if (nro_linea > 0) {
						Calendar fecha_calculo = cargaFichero.getFecha_calculo();
						pstmt_registro.setInt(1, id_carga_fichero);
						pstmt_registro.setInt(2, nro_linea);
						pstmt_registro.setInt(3,
								fecha_calculo.get(Calendar.YEAR) * 10000 + (fecha_calculo.get(Calendar.MONTH) + 1) * 100
										+ fecha_calculo.get(Calendar.DAY_OF_MONTH));

						// recorremos los campos en el orden de la cabecera!!
						for (String nombre_variable : cabecera) {
							// buscamos el objeto asociado
							for (CampoAprovisionamiento campo : cargaFichero.getTablaInput()
									.getCampos_aprovisionamiento()) {
								if (campo.getNombre_campo().equalsIgnoreCase(nombre_variable)) {

									// buscamos la posición del campo (+ 4 porque incluye las variables llave de la
									// tabla)
									index_insert = array_nombres_campos.indexOf(cabecera.get(index_fichero)) + 4;

									// guardamos el ancho del dato
									if (campos.get(index_fichero).length() > campo.getAncho_campo()) {
										campo.setAncho_campo(campos.get(index_fichero).length());
										if (campo.getTipo_campo().getId_tipo_campo() == 2) {
											String sql_modificar_tabla = "ALTER TABLE  " + userName + ".HE_"
													+ cargaFichero.getTablaInput().getNombre_fichero() + "_v"
													+ cargaFichero.getTablaInput().getVersion_metadata() + " MODIFY "
													+ campo.getNombre_campo() + " varchar2("
													+ campos.get(index_fichero).length() + " )";

											PreparedStatement pstmt = null;
											try {
												// Agregamos el mapeo_cartera_fichero0
												pstmt = conn.prepareStatement(sql_modificar_tabla);
												pstmt.addBatch();
												pstmt.executeBatch();
												ejecutarCommit(conn);

											} catch (Exception ex) {
												ejecutarRollback(conn);
												LOGGER.error("Error al alterar tabla no normalizada: "
														+ cargaFichero.getTablaInput().getNombre_fichero() + ". Error: "
														+ ex.getMessage());
											} finally {
												cerrarPreparedStatement(pstmt);
											}
										}
									}

									if (campo.getTipo_campo().getId_tipo_campo().equals(1)) {
										// Se evalua si el campo es nulo o no
										if (!campos.get(index_fichero).isEmpty()) {

											try {
												numero_insert_str = campos.get(index_fichero).replace("\"", "")
														.replace(",", ".");
												// Agregamos la validación NaN porque en Java se interpreta como un
												// numérico, pero debemos guardarlo en base de datos como null
												if (numero_insert_str.trim().equals("NaN")) {
													pstmt_registro.setNull(index_insert, Types.DOUBLE);
												} else {
													numero_insert = Double.valueOf(numero_insert_str);
												}
											} catch (Exception ei) {
												cerrarBufferedReader(br); // adicional?
												cerrarPreparedStatement(pstmt_registro); // adicional?
												throw new CustomException("No se puede ingresar "
														+ campo.getNombre_campo() + " : " + campos.get(index_fichero)
														+ " linea " + nro_linea + " , porque no es numerico.");
											}

											if (numero_insert_str.substring(0,
													(numero_insert_str.indexOf(".") == -1 ? numero_insert_str.length()
															: numero_insert_str.indexOf(".")))
													.length() > 22) {
												cerrarBufferedReader(br); // adicional?
												cerrarPreparedStatement(pstmt_registro); // adicional?
												throw new CustomException("No se puede ingresar "
														+ campo.getNombre_campo() + " : " + campos.get(index_fichero)
														+ " linea " + nro_linea
														+ " , porque excede el tamaño máximo (22 digitos enteros). Ingrese el campo como texto.");
											} else {
												pstmt_registro.setDouble(index_insert, numero_insert);
											}

										} else {
											pstmt_registro.setNull(index_insert, Types.DOUBLE);
										}
									} else {
										// Se evalua si el campo es nulo o no
										if (!campos.get(index_fichero).isEmpty()) {
											pstmt_registro.setString(index_insert,
													campos.get(index_fichero).replace("\"", ""));
										} else {
											pstmt_registro.setNull(index_insert, Types.VARCHAR);
										}
									}
									index_fichero++;
									break;
								}
							}
						}
						pstmt_registro.addBatch();
					}
				} else {
					// LOGGER.warn("El aprov " + cargaFichero.getTablaInput().getNombre_fichero()
					// + " presenta inconsistencia con su numero de campos en la linea: " +
					// nro_linea);
					cargaFichero.getTablaInput().setFlag_error_inconsistencia(true);
					cerrarBufferedReader(br); // adicional?
					cerrarPreparedStatement(pstmt_registro); // adicional?
					throw new CustomException("El aprov " + cargaFichero.getTablaInput().getNombre_fichero()
							+ " presenta inconsistencia con su numero de campos en la linea: " + nro_linea);
				}
				nro_linea++;
				if (nro_linea % max_lote == 0) {
					pstmt_registro.executeBatch();
					LOGGER.info("Se han procesado " + nro_linea + " registros hacia la tabla input temporal");
				}
			}
			pstmt_registro.executeBatch();
			ejecutarCommit(conn);
			LOGGER.info("Se ha procesado el aprov " + cargaFichero.getTablaInput().getNombre_fichero()
					+ " con un total de " + nro_linea + " lineas");

			// actualizamos el ancho del campo
			for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
				actualizarAnchoTablaInput(campo.getAncho_campo(), campo.getId_campo(), conn);
			}

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error en procesaraprovNuevoNoNormalizado al procesar aprov hacia tabla input temporal: "
					+ ex.getMessage());
			cargaFichero.getTablaInput().setFlag_error_inconsistencia(true);
			throw ex;
		} finally {
			procesado.setError_campos(error);
			procesado.setNro_lineas(nro_linea);
			cerrarBufferedReader(br);
			cerrarPreparedStatement(pstmt_campos);
			cerrarPreparedStatement(pstmt_registro);
		}
		return procesado;
	}

	public Integer procesaraprovAntiguoNoNormalizado(CargaFichero cargaFichero, Integer ordenInicio) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		Integer ultimoOrden = procesaraprovAntiguoNoNormalizado(cargaFichero, ordenInicio, conn);
		cerrarConexion(conn);
		return ultimoOrden;
	}

	public Integer procesaraprovAntiguoNoNormalizado(CargaFichero cargaFichero, Integer ordenInicio, Connection conn)
			throws Exception {
		BufferedReader br = null;
		Integer nro_linea = ordenInicio;
		Integer nro_campos = 0;
		String linea_input = null;
		ArrayList<String> campos = null;
		ArrayList<String> cabecera = null;
		Integer index_insert = 0;
		Integer index_fichero = 0;
		Character separador_fichero = null;
		PreparedStatement pstmt_registro = null;

		String sql_registro_nombres_campos = "";
		String sql_registro_registros_campos = "";
		ArrayList<String> array_nombres_campos = new ArrayList<String>();
		ArrayList<String> array_registros_campos = new ArrayList<String>();

		ArrayList<CampoInput> campos_inputs = null;
		campos_inputs = obtenerCamposporTablaInputVersion(cargaFichero.getTablaInput());

		// sobreescribimos el codcampo con el ids del campo input y el ancho anterior
		for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
			for (CampoInput campo_input : campos_inputs) {
				if (campo.getNombre_campo().equals(campo_input.getNombre_campo())) {
					campo.setId_campo(campo_input.getId_campo());
					campo.setAncho_campo(campo_input.getAncho_campo());
					break;
				}
			}
			array_nombres_campos.add(campo.getNombre_campo());
			array_registros_campos.add("?");
		}
		sql_registro_nombres_campos = String.join(",", array_nombres_campos);
		sql_registro_registros_campos = String.join(",", array_registros_campos);

		String sql_registro = "INSERT INTO " + userName + ".HE_" + cargaFichero.getTablaInput().getNombre_fichero()
				+ "_v" + cargaFichero.getTablaInput().getVersion_metadata()
				+ " (CodCargaFichero, NumOrden,FECCALCULORES, " + sql_registro_nombres_campos + ") values (?,?,?,"
				+ sql_registro_registros_campos + ")";
		cargaFichero.setTablaDestino("HE_" + cargaFichero.getTablaInput().getNombre_fichero().toUpperCase() + "_V"
				+ cargaFichero.getTablaInput().getVersion_metadata());
		cargaFichero.setNumeroCampos(campos_inputs.size());
		try {
			br = new BufferedReader(new FileReader(
					cargaFichero.getTablaInput().getRuta() + "\\" + cargaFichero.getTablaInput().getNombre_archivo()));

			nro_campos = cargaFichero.getTablaInput().getCampos_aprovisionamiento().size();
			LOGGER.info("Procesando el aprov " + cargaFichero.getTablaInput().getNombre_fichero() + " con " + nro_campos
					+ " campos");
			// Se recorren todas las lineas del aprov input
			pstmt_registro = conn.prepareStatement(sql_registro);
			// LOGGER.info("Dato:" + br.readLine());

			Double numero_insert = 0.0;
			String numero_insert_str = "";

			while ((linea_input = br.readLine()) != null) {
				// de la primera linea se obtiene el separador
				if (nro_linea == ordenInicio) {

					// cargamos un separador dummy, en caso de que no encuentre el separador
					// configurado intentara cargarlo como unicampo
					cargaFichero.getTablaInput().setSeparador("asdfgtrewq");
					for (int i = 0; i < separador.length(); i++) {
						separador_fichero = separador.charAt(i);
						if (linea_input.contains(separador_fichero.toString())) {
							cargaFichero.getTablaInput().setSeparador(separador_fichero.toString());
							break;
						}
					}
					cabecera = separarCampos(linea_input, cargaFichero.getTablaInput().getSeparador());
				}

				index_insert = 0;
				index_fichero = 0;

				// Se comprueba si el numero de campos es consistente en cada linea
				campos = separarCampos(linea_input, cargaFichero.getTablaInput().getSeparador());
				if (nro_campos == campos.size()) {
					// Solo se desea procesar los registros, no la primera linea (posicion 0) que
					// tiene los campos
					if (nro_linea > ordenInicio) {
						// Se recorre todos los campos de la linea que se ha leido
						Calendar fecha_calculo = cargaFichero.getFecha_calculo();
						pstmt_registro.setInt(1, cargaFichero.getId_cargafichero());
						pstmt_registro.setInt(2, nro_linea);
						pstmt_registro.setInt(3,
								fecha_calculo.get(Calendar.YEAR) * 10000 + (fecha_calculo.get(Calendar.MONTH) + 1) * 100
										+ fecha_calculo.get(Calendar.DAY_OF_MONTH));

						// recorremos los campos en el orden de la cabecera!!
						for (String nombre_variable : cabecera) {
							// buscamos el objeto asociado
							for (CampoAprovisionamiento campo : cargaFichero.getTablaInput()
									.getCampos_aprovisionamiento()) {
								if (campo.getNombre_campo().equalsIgnoreCase(nombre_variable)) {

									// buscamos la posición del campo (+ 4 porque incluye las variables llave de la
									// tabla)
									index_insert = array_nombres_campos.indexOf(cabecera.get(index_fichero)) + 4;
									// guardamos el ancho del dato
									if (campos.get(index_fichero).length() > campo.getAncho_campo()) {
										campo.setAncho_campo(campos.get(index_fichero).length());

										if (campo.getTipo_campo().getId_tipo_campo() == 2) {
											String sql_modificar_tabla = "ALTER TABLE  " + userName + ".HE_"
													+ cargaFichero.getTablaInput().getNombre_fichero() + "_v"
													+ cargaFichero.getTablaInput().getVersion_metadata() + " MODIFY "
													+ campo.getNombre_campo() + " varchar2("
													+ campos.get(index_fichero).length() + " )";

											PreparedStatement pstmt = null;
											try {
												//LOGGER.info(sql_modificar_tabla);
												// Agregamos el mapeo_cartera_fichero0
												pstmt = conn.prepareStatement(sql_modificar_tabla);
												pstmt.addBatch();
												pstmt.executeBatch();
												ejecutarCommit(conn);

											} catch (Exception ex) {
												ejecutarRollback(conn);
												LOGGER.error("Error al alterar tabla no normalizada: "
														+ cargaFichero.getTablaInput().getNombre_fichero() + ". Error: "
														+ ex.getMessage());
											} finally {
												cerrarPreparedStatement(pstmt);
											}
										}
									}

									if (campo.getTipo_campo().getId_tipo_campo().equals(1)) {
										if (!campos.get(index_fichero).isEmpty()) {

											try {
												numero_insert_str = campos.get(index_fichero).replace("\"", "")
														.replace(",", ".");
												// Agregamos la validación NaN porque en Java se interpreta como un
												// numérico, pero debemos guardarlo en base de datos como null
												if (numero_insert_str.trim().equals("NaN")) {
													pstmt_registro.setNull(index_insert, Types.DOUBLE);
												} else {
													numero_insert = Double.valueOf(numero_insert_str);
												}

											} catch (Exception ei) {
												cerrarBufferedReader(br); // adicional?
												cerrarPreparedStatement(pstmt_registro); // adicional?
												throw new CustomException("No se puede ingresar "
														+ campo.getNombre_campo() + " : " + campos.get(index_fichero)
														+ " linea " + nro_linea + " , porque no es numerico.");
											}
											if (numero_insert_str.substring(0,
													(numero_insert_str.indexOf(".") == -1 ? numero_insert_str.length()
															: numero_insert_str.indexOf(".")))
													.length() > 22) {
												cerrarBufferedReader(br); // adicional?
												cerrarPreparedStatement(pstmt_registro); // adicional?
												throw new CustomException("No se puede ingresar "
														+ campo.getNombre_campo() + " : " + campos.get(index_fichero)
														+ " linea " + nro_linea
														+ " , porque excede el tamaño máximo (22 digitos enteros). Ingrese el campo como texto.");
											}

											else {
												pstmt_registro.setDouble(index_insert, numero_insert);
											}

										} else {
											pstmt_registro.setNull(index_insert, Types.DOUBLE);
										}
									} else {
										if (!campos.get(index_fichero).isEmpty()) {
											pstmt_registro.setString(index_insert,
													campos.get(index_fichero).replace("\"", ""));
										} else {
											pstmt_registro.setNull(index_insert, Types.VARCHAR);
										}
									}
									index_fichero++;
									break;
								}

							}
						}
						pstmt_registro.addBatch();
					}
				} else {
					// LOGGER.warn("El aprov " + cargaFichero.getTablaInput().getNombre_fichero()
					// + " presenta inconsistencia con su numero de campos en la linea: " +
					// nro_linea);
					cargaFichero.getTablaInput().setFlag_error_inconsistencia(true);
					cerrarBufferedReader(br); // adicional?
					cerrarPreparedStatement(pstmt_registro); // adicional?
					throw new CustomException("El aprov " + cargaFichero.getTablaInput().getNombre_fichero()
							+ " presenta inconsistencia con su numero de campos en la linea: " + nro_linea);
				}
				nro_linea++;
				if (nro_linea % max_lote == 0) {
					pstmt_registro.executeBatch();
					LOGGER.info("Se han procesado " + nro_linea + " registros hacia la tabla input temporal");
				}
			}
			pstmt_registro.executeBatch();
			ejecutarCommit(conn);
			LOGGER.info("Se ha procesado el aprov " + cargaFichero.getTablaInput().getNombre_fichero()
					+ " con un total de " + nro_linea + " lineas");

			// actualizamos el ancho del campo
			for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
				actualizarAnchoTablaInput(campo.getAncho_campo(), campo.getId_campo(), conn);
			}

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error en procesaraprovAntiguoNoNormalizado al procesar aprov hacia tabla input temporal: "
					+ ex.getMessage());
			cargaFichero.getTablaInput().setFlag_error_inconsistencia(true);
			throw ex;
		} finally {
			cerrarBufferedReader(br);
			cerrarPreparedStatement(pstmt_registro);
		}

		return nro_linea;
	}

	public void crearCamposInput(CargaFichero cargaFichero) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearCamposInput(cargaFichero, conn);
		cerrarConexion(conn);
	}

	public void crearCamposInput(CargaFichero cargaFichero, Connection conn) throws Exception {

		PreparedStatement pstmt_campos = null;

		String sql_campos = "INSERT INTO HE_CampoInput ( CodCampo, CodTablaInput, CodTipoCampo, NbrCampo, FecActualizacionTabla) values (?,?,?,?,?)";

		try {
			// Antes de insertar los registros, se inserta la metadata (tabla y campos)
			// incluyendo calculados
			pstmt_campos = conn.prepareStatement(sql_campos);
			for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
				LOGGER.debug("aprov: " + cargaFichero.getTablaInput().getNombre_fichero());
				LOGGER.debug("Campo: " + campo.getNombre_campo());
				campo.setId_campo(obtenerSiguienteId("seq_he_campoinput"));
				campo.setAncho_campo(0);
				pstmt_campos.setInt(1, campo.getId_campo());
				pstmt_campos.setInt(2, cargaFichero.getTablaInput().getId_tablainput());
				pstmt_campos.setInt(3, campo.getTipo_campo().getId_tipo_campo());
				pstmt_campos.setString(4, campo.getNombre_campo());
				pstmt_campos.setDate(5, new Date(System.currentTimeMillis()));
				pstmt_campos.addBatch();
				LOGGER.debug("Procesado a campo input temporal: " + campo.getNombre_campo());
			}
			pstmt_campos.executeBatch();
			cerrarPreparedStatement(pstmt_campos);
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error en la creación de campos input: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_campos);
		}
	}

	public void crearTablaNoNormalizadaTemporalConCamposOriginales(CargaFichero cargaFichero) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearTablaNoNormalizadaTemporalConCamposOriginales(cargaFichero, conn);
		cerrarConexion(conn);
	}

	public void crearTablaNoNormalizadaTemporalConCamposOriginales(CargaFichero cargaFichero, Connection conn)
			throws Exception {
		PreparedStatement pstmt = null;

		Aprovisionamiento aprov = new Aprovisionamiento();
		aprov.setNombre_fichero(cargaFichero.getTablaInput().getNombre_fichero());

		ArrayList<String> rutas = obtenerSoloRutasTexto(
				cargaFichero.getTablaInput().getTipo_ambito().getId_tipo_ambito(), true);
		aprov = obtenerFicheroConCampos(aprov, rutas);

		String sql_crear_tabla_campos = "";
		ArrayList<String> array_crear_campos = new ArrayList<String>();

		for (CampoAprovisionamiento campo : aprov.getCampos_aprovisionamiento()) {

			for (CampoAprovisionamiento campo_carga : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
				if (campo.getNombre_campo().equals(campo_carga.getNombre_campo())) {
					campo.setTipo_campo(campo_carga.getTipo_campo());
					campo.setFlag_aprovisionar(campo_carga.getFlag_aprovisionar());
					campo.setFlag_campo_original(campo_carga.getFlag_campo_original());
					break;
				}
			}

			if (campo.getFlag_aprovisionar() && campo.getFlag_campo_original()) {
				if (campo.getTipo_campo().getId_tipo_campo().equals(1)) {
					array_crear_campos.add(campo.getNombre_campo() + " INTEGER ");
				} else if (campo.getTipo_campo().getId_tipo_campo().equals(2)) {
					array_crear_campos.add(campo.getNombre_campo() + " VARCHAR2(1) "); // 800
				} else if (campo.getTipo_campo().getId_tipo_campo().equals(3)) {
					array_crear_campos.add(campo.getNombre_campo() + " NUMBER(34,12) "); // 800
				}
			}
		}
		sql_crear_tabla_campos = String.join(",", array_crear_campos);

		String sql_crear_tabla = "CREATE TABLE " + userName + ".HE_" + cargaFichero.getTablaInput().getNombre_fichero()
				+ "_v" + cargaFichero.getTablaInput().getVersion_metadata() + "_tmp"
				+ " (CodCargaFichero INTEGER NOT NULL, NumOrden INTEGER NOT NULL, FECCALCULORES INTEGER NOT NULL,"
				+ sql_crear_tabla_campos + ")"; // + agregarPartitionByCodigoCargaFichero(cargaFichero);

		try {
			// Agregamos el mapeo_cartera_fichero
			pstmt = conn.prepareStatement(sql_crear_tabla);
			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear tabla no normalizada: " + cargaFichero.getTablaInput().getNombre_fichero()
					+ ". Error: " + ex.getMessage());
		} finally {
			cerrarPreparedStatement(pstmt);
		}

	}

	public void alterarTablaNoNormalizadaTemporalPorCampoCalculado(CargaFichero cargaFichero,
			CampoAprovisionamiento campo_calculado) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		alterarTablaNoNormalizadaTemporalPorCampoCalculado(cargaFichero, campo_calculado, conn);
		cerrarConexion(conn);
	}

	// Para campos calculados con dependencia
	public void alterarTablaNoNormalizadaTemporalPorCampoCalculado(CargaFichero cargaFichero,
			CampoAprovisionamiento campo_calculado, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String campo_alter = "";

		if (campo_calculado.getTipo_campo().getId_tipo_campo().equals(1)) {
			campo_alter = campo_calculado.getNombre_campo() + " INTEGER";
		} else if (campo_calculado.getTipo_campo().getId_tipo_campo().equals(2)) {
			campo_alter = campo_calculado.getNombre_campo() + " VARCHAR2(1) "; // 800
		} else if (campo_calculado.getTipo_campo().getId_tipo_campo().equals(3)) {
			campo_alter = campo_calculado.getNombre_campo() + " NUMBER(34,12) "; // 800
		}

		String sql_alterar_tabla = "ALTER TABLE " + userName + ".HE_" + cargaFichero.getTablaInput().getNombre_fichero()
				+ "_v" + cargaFichero.getTablaInput().getVersion_metadata() + "_tmp" + " ADD " + campo_alter;
		try {
			pstmt = conn.prepareStatement(sql_alterar_tabla);
			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al alterar tabla no normalizada por campo calculado: "
					+ cargaFichero.getTablaInput().getNombre_fichero() + ". Error: " + ex.getMessage());
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public Integer procesaraprovTemporal(CargaFichero cargaFichero, Integer ordenInicio) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		Integer nro_linea = procesaraprovTemporal(cargaFichero, ordenInicio, conn);
		cerrarConexion(conn);
		return nro_linea;
	}

	public Integer procesaraprovTemporal(CargaFichero cargaFichero, Integer ordenInicio, Connection conn)
			throws Exception {
		BufferedReader br = null;
		Integer nro_linea = ordenInicio;
		Integer nro_campos = 0;
		String linea_input = null;
		ArrayList<String> campos = null;
		ArrayList<String> cabecera = null;
		Integer index_insert = 0;
		Integer index_fichero = 0;
		Character separador_fichero = null;

		PreparedStatement pstmt_registro = null;

		String sql_registro_nombres_campos = "";
		String sql_registro_registros_campos = "";
		ArrayList<String> array_nombres_campos = new ArrayList<String>();
		ArrayList<String> array_registros_campos = new ArrayList<String>();

		ArrayList<CampoInput> campos_inputs = null;
		campos_inputs = obtenerCamposporTablaInputVersion(cargaFichero.getTablaInput());

		// sobreescribimos el codcampo con el ids del campo input y el ancho anterior
		for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento(true)) {
			for (CampoInput campo_input : campos_inputs) {
				if (campo.getNombre_campo().equals(campo_input.getNombre_campo())) {
					campo.setId_campo(campo_input.getId_campo());
					campo.setAncho_campo(campo_input.getAncho_campo());
					break;
				}
			}
			array_nombres_campos.add(campo.getNombre_campo());
			array_registros_campos.add("?");
		}

		sql_registro_nombres_campos = String.join(",", array_nombres_campos);
		sql_registro_registros_campos = String.join(",", array_registros_campos);

		String sql_registro = "INSERT INTO " + userName + ".HE_" + cargaFichero.getTablaInput().getNombre_fichero()
				+ "_v" + cargaFichero.getTablaInput().getVersion_metadata() + "_tmp"
				+ " (CodCargaFichero, NumOrden,FECCALCULORES, " + sql_registro_nombres_campos + ") values (?,?,?,"
				+ sql_registro_registros_campos + ")";
		try {
			br = new BufferedReader(new FileReader(
					cargaFichero.getTablaInput().getRuta() + "\\" + cargaFichero.getTablaInput().getNombre_archivo()));

			nro_campos = cargaFichero.getTablaInput().getCampos_aprovisionamiento(true).size();
			LOGGER.info("Procesando el aprov " + cargaFichero.getTablaInput().getNombre_fichero() + " con " + nro_campos
					+ " campos");
			// Se recorren todas las lineas del aprov input
			pstmt_registro = conn.prepareStatement(sql_registro);
			// LOGGER.info("Dato:" + br.readLine());

			Double numero_insert = 0.0;
			String numero_insert_str = "";

			while ((linea_input = br.readLine()) != null) {
				// de la primera linea se obtiene el separador
				if (nro_linea == ordenInicio) {

					// cargamos un separador dummy, en caso de que no encuentre el separador
					// configurado intentara cargarlo como unicampo
					cargaFichero.getTablaInput().setSeparador("asdfgtrewq");
					for (int i = 0; i < separador.length(); i++) {
						separador_fichero = separador.charAt(i);
						if (linea_input.contains(separador_fichero.toString())) {
							cargaFichero.getTablaInput().setSeparador(separador_fichero.toString());
							break;
						}
					}
					cabecera = separarCampos(linea_input, cargaFichero.getTablaInput().getSeparador());
				}

				index_insert = 0;
				index_fichero = 0;

				// Se comprueba si el numero de campos es consistente en cada linea
				campos = separarCampos(linea_input, cargaFichero.getTablaInput().getSeparador());
				if (nro_campos == campos.size()) {
					// Solo se desea procesar los registros, no la primera linea (posicion 0) que
					// tiene los campos...
					if (nro_linea > ordenInicio) {
						// Se recorre todos los campos de la linea que se ha leido
						Calendar fecha_calculo = cargaFichero.getFecha_calculo();
						pstmt_registro.setInt(1, cargaFichero.getId_cargafichero());
						pstmt_registro.setInt(2, nro_linea);
						pstmt_registro.setInt(3,
								fecha_calculo.get(Calendar.YEAR) * 10000 + (fecha_calculo.get(Calendar.MONTH) + 1) * 100
										+ fecha_calculo.get(Calendar.DAY_OF_MONTH));

						// recorremos los campos en el orden de la cabecera!!
						for (String nombre_variable : cabecera) {
							// buscamos el objeto asociado
							for (CampoAprovisionamiento campo : cargaFichero.getTablaInput()
									.getCampos_aprovisionamiento(true)) {
								if (campo.getNombre_campo().equalsIgnoreCase(nombre_variable)) {

									// buscamos la posición del campo (+ 4 porque incluye las variables llave de la
									// tabla)
									index_insert = array_nombres_campos.indexOf(cabecera.get(index_fichero)) + 4;

									// guardamos el ancho del dato
									if (campos.get(index_fichero).length() > campo.getAncho_campo()) {
										campo.setAncho_campo(campos.get(index_fichero).length());
									}

									if (campo.getTipo_campo().getId_tipo_campo().equals(1)) {
										if (!campos.get(index_fichero).isEmpty()) {

											try {
												numero_insert_str = campos.get(index_fichero).replace("\"", "")
														.replace(",", ".");
												// Agregamos la validación NaN porque en Java se interpreta como un
												// numérico, pero debemos guardarlo en base de datos como null
												if (numero_insert_str.trim().equals("NaN")) {
													pstmt_registro.setNull(index_insert, Types.DOUBLE);
												} else {
													numero_insert = Double.valueOf(numero_insert_str);
												}
											} catch (Exception ei) {
												cerrarBufferedReader(br); // adicional?
												cerrarPreparedStatement(pstmt_registro); // adicional?
												throw new CustomException("No se puede ingresar "
														+ campo.getNombre_campo() + " : " + campos.get(index_fichero)
														+ " linea " + nro_linea + " , porque no es numerico.");
											}
											if (numero_insert_str.substring(0,
													(numero_insert_str.indexOf(".") == -1 ? numero_insert_str.length()
															: numero_insert_str.indexOf(".")))
													.length() > 22) {
												cerrarBufferedReader(br); // adicional?
												cerrarPreparedStatement(pstmt_registro); // adicional?
												throw new CustomException("No se puede ingresar "
														+ campo.getNombre_campo() + " : " + campos.get(index_fichero)
														+ " linea " + nro_linea
														+ " , porque excede el tama�o m�ximo (22 digitos enteros). Ingrese el campo como texto.");
											} else {
												pstmt_registro.setDouble(index_insert, numero_insert);
											}

										} else {
											pstmt_registro.setNull(index_insert, Types.DOUBLE);
										}
									} else {
										if (!campos.get(index_fichero).isEmpty()) {
											pstmt_registro.setString(index_insert,
													campos.get(index_fichero).replace("\"", ""));
										} else {
											pstmt_registro.setNull(index_insert, Types.VARCHAR);
										}
									}
									index_fichero++;
									break;
								}
							}
						}
						pstmt_registro.addBatch();
					}
				} else {
					// LOGGER.warn("El aprov " + cargaFichero.getTablaInput().getNombre_fichero()
					// + " presenta inconsistencia con su numero de campos en la linea: " +
					// nro_linea);
					cargaFichero.getTablaInput().setFlag_error_inconsistencia(true);
					cerrarBufferedReader(br); // adicional?
					cerrarPreparedStatement(pstmt_registro); // adicional?
					throw new CustomException("El aprov " + cargaFichero.getTablaInput().getNombre_fichero()
							+ " presenta inconsistencia con su numero de campos en la linea: " + nro_linea);
				}
				nro_linea++;
				if (nro_linea % max_lote == 0) {
					pstmt_registro.executeBatch();
					LOGGER.info("Se han procesado " + nro_linea + " registros hacia la tabla input temporal");
				}
			}
			pstmt_registro.executeBatch();
			ejecutarCommit(conn);
			LOGGER.info("Se ha procesado el aprov " + cargaFichero.getTablaInput().getNombre_fichero()
					+ " con un total de " + nro_linea + " lineas");

			// actualizamos el ancho del campo
			for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento(true)) {
				actualizarAnchoTablaInput(campo.getAncho_campo(), campo.getId_campo(), conn);
			}

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error en procesaraprovTemporal al procesar aprov hacia tabla input temporal: " + ex.getMessage());
			cargaFichero.getTablaInput().setFlag_error_inconsistencia(true);
			throw ex;
		} finally {
			cerrarBufferedReader(br);
			cerrarPreparedStatement(pstmt_registro);
		}

		return nro_linea;
	}

	public void insertarAprovConCamposCalculados(CargaFichero cargaFichero, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql_campos_totales = "";
		String sql_campos_originales = "";
		String sql_campos_caludados_formula = "";
		ArrayList<String> array_campos_totales = new ArrayList<String>();
		ArrayList<String> array_campos_originales = new ArrayList<String>();
		ArrayList<String> array_campos_caludados_formula = new ArrayList<String>();

		for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
			if (campo.getFlag_aprovisionar()) {
				array_campos_totales.add(campo.getNombre_campo());
			}
		}
		for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
			if (campo.getFlag_aprovisionar() && campo.getFlag_campo_original()) {
				array_campos_originales.add(campo.getNombre_campo());
			}
		}

		for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
			if (campo.getFlag_aprovisionar() && !campo.getFlag_campo_original()) {
				array_campos_caludados_formula.add(campo.getFormula_nuevo_campo_sql());
			}
		}

		sql_campos_totales = String.join(",", array_campos_totales);
		sql_campos_originales = String.join(",", array_campos_originales);
		sql_campos_caludados_formula = String.join(",", array_campos_caludados_formula);

		String sql_insercionTablaTmp = "insert into " + userName + ".HE_"
				+ cargaFichero.getTablaInput().getNombre_fichero() + "_v"
				+ cargaFichero.getTablaInput().getVersion_metadata() + " (CodCargaFichero,NumOrden,FECCALCULORES,"
				+ sql_campos_totales + ")" + "select CodCargaFichero,NumOrden,FECCALCULORES," + sql_campos_originales
				+ "," + sql_campos_caludados_formula + " from " + userName + ".HE_"
				+ cargaFichero.getTablaInput().getNombre_fichero() + "_v"
				+ cargaFichero.getTablaInput().getVersion_metadata() + "_tmp";

		try {
			// Agregamos el mapeo_cartera_fichero
			pstmt = conn.prepareStatement(sql_insercionTablaTmp);
			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al insertar tabla no normalizada con campos calculados: "
					+ cargaFichero.getTablaInput().getNombre_fichero() + ". Error: " + ex.getMessage());
		} finally {
			cerrarPreparedStatement(pstmt);
		}

	}

	public void actualizarAprovPorCampoCalculado(CargaFichero cargaFichero, CampoAprovisionamiento campo_calculado)
			throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		actualizarAprovPorCampoCalculado(cargaFichero, campo_calculado, conn);
		cerrarConexion(conn);
	}

	// Para campos calculados con dependencia
	public void actualizarAprovPorCampoCalculado(CargaFichero cargaFichero, CampoAprovisionamiento campo_calculado,
			Connection conn) throws Exception {
		PreparedStatement pstmt = null;

		String sql_updateTablaTmp = "update " + userName + ".HE_" + cargaFichero.getTablaInput().getNombre_fichero()
				+ "_v" + cargaFichero.getTablaInput().getVersion_metadata() + "_tmp" + " set "
				+ campo_calculado.getNombre_campo() + " = " + campo_calculado.getFormula_nuevo_campo_sql();

		try {
			pstmt = conn.prepareStatement(sql_updateTablaTmp);
			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar tabla no normalizada por campo calculado: "
					+ cargaFichero.getTablaInput().getNombre_fichero() + " |  Campo_calculado: "
					+ campo_calculado.getNombre_campo() + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}

	}

	public void insertarAprovConCamposCalculadosConDatos(CargaFichero cargaFichero) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		insertarAprovConCamposCalculadosConDatos(cargaFichero, conn);
		cerrarConexion(conn);
	}

	// Para campos calculados con dependencia
	public void insertarAprovConCamposCalculadosConDatos(CargaFichero cargaFichero, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql_campos_totales = "";
		ArrayList<String> array_campos_totales = new ArrayList<String>();

		for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
			if (campo.getFlag_aprovisionar()) {
				array_campos_totales.add(campo.getNombre_campo());
			}
		}

		sql_campos_totales = String.join(",", array_campos_totales);

		String sql_insercionTablaTmp = "insert into " + userName + ".HE_"
				+ cargaFichero.getTablaInput().getNombre_fichero() + "_v"
				+ cargaFichero.getTablaInput().getVersion_metadata() + " (CodCargaFichero,NumOrden,FECCALCULORES,"
				+ sql_campos_totales + ")" + "select CodCargaFichero,NumOrden,FECCALCULORES," + sql_campos_totales
				+ " from " + userName + ".HE_" + cargaFichero.getTablaInput().getNombre_fichero() + "_v"
				+ cargaFichero.getTablaInput().getVersion_metadata() + "_tmp";

		try {
			pstmt = conn.prepareStatement(sql_insercionTablaTmp);
			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al insertar tabla no normalizada incluido campos calculados con datos: "
					+ cargaFichero.getTablaInput().getNombre_fichero() + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}

	}

	public void borrarTablaNoNormalizadaTemporalConCamposOriginales(CargaFichero cargaFichero, Boolean sinThrow)
			throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		borrarTablaNoNormalizadaTemporalConCamposOriginales(cargaFichero, sinThrow);
		cerrarConexion(conn);
	}

	public void borrarTablaNoNormalizadaTemporalConCamposOriginales(CargaFichero cargaFichero, Boolean sinThrow,
			Connection conn) throws Exception {
		PreparedStatement pstmt = null;

		String sql_borrar_tabla = "Drop TABLE " + userName + ".HE_" + cargaFichero.getTablaInput().getNombre_fichero()
				+ "_v" + cargaFichero.getTablaInput().getVersion_metadata() + "_tmp" + " CASCADE CONSTRAINTS";
		try {
			pstmt = conn.prepareStatement(sql_borrar_tabla);
			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);

			if (sinThrow) {
				LOGGER.warn("Error al borrar la tabla no normalizada temporal: "
						+ cargaFichero.getTablaInput().getNombre_fichero() + ". Error: " + ex.getMessage());
			} else {
				LOGGER.error("Error al borrar la tabla no normalizada temporal: "
						+ cargaFichero.getTablaInput().getNombre_fichero() + ". Error: " + ex.getMessage());
				throw ex;
			}

		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void replicarReglasCampo(ArrayList<CampoAprovisionamiento> campos) throws Exception {
		Connection conn = null;
		ArrayList<ReglaCalidad> reglas = new ArrayList<ReglaCalidad>();
		try {
			conn = obtenerConexion();
			reglas = campos.get(0).getReglas();
			for (int i = 0; i < campos.size(); i++) {
				if (i > 0) {
					for (ReglaCalidad regla : reglas) {
						regla.setId_campoaprov(campos.get(i).getId_campo_aprovisionamiento());
					}
					editarReglasCalidad(reglas, conn);
				}
			}
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al replicar reglas / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarConexion(conn);
		}
	}

	public ArrayList<ReglaCalidad> obtenerReglasCalidadPorCampo(CampoAprovisionamiento campo) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ReglaCalidad regla = null;
		TipoIndicador tipoindicador = null;
		ArrayList<ReglaCalidad> reglas = new ArrayList<ReglaCalidad>();

		String sql = "select * from HE_ReglaCalidad where CodCampoAprov=?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, campo.getId_campo_aprovisionamiento());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				regla = new ReglaCalidad();
				tipoindicador = new TipoIndicador();

				regla.setId_segmento(rs.getInt("CodSegmento"));
				regla.setId_campoaprov(rs.getInt("CodCampoAprov"));
				tipoindicador.setId_tipo_indicador(rs.getInt("CodTipoIndicador"));
				regla.setTipo_analisis(rs.getInt("CodTipoAnalisis"));
				regla.setLimite_inferior_rojo(rs.getDouble("NumLimiteInferiorRojo"));
				regla.setLimite_inferior_ambar(rs.getDouble("NumLimiteInferiorAmbar"));
				regla.setLimite_superior_ambar(rs.getDouble("NumLimiteSuperiorAmbar"));
				regla.setLimite_superior_rojo(rs.getDouble("NumLimiteSuperiorRojo"));
				regla.setFlag_aplica(rs.getBoolean("FlgAplica"));

				regla.setTipo_indicador(tipoindicador);
				reglas.add(regla);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las reglas calidad por campo: " + campo.getTipo_campo().getId_tipo_campo()
					+ ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return reglas;
	}

	public ArrayList<ReglaCalidad> obtenerReglasCalidad(ReglaCalidad regla) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ReglaCalidad regla_calidad = null;
		ArrayList<ReglaCalidad> reglas = new ArrayList<ReglaCalidad>();
		TipoIndicador tipo_indicador = null;
		// Dependiendo de lo que se quiera analizar se ingresa el valor de tipocampo y
		// tipoanalisis

		String sql = "SELECT TI.NbrIndicador, RC.*" + " FROM HE_ReglaCalidad RC "
				+ " LEFT JOIN ME_TipoIndicador TI on TI.CodTipoIndicador = RC.CodTipoIndicador"
				+ " LEFT JOIN HE_Segmento S on S.CodSegmento = RC.CodSegmento"
				+ " LEFT JOIN HE_CampoAprovisionamiento CA on CA.CodCampoAprovisionamiento = RC.CodCampoAprov"
				+ " WHERE RC.CodSegmento=? and RC.CodCampoAprov=?" + " and RC.CodTipoAnalisis=?"
				+ " and CA.CodTipoCampo=?";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, regla.getId_segmento());
			pstmt.setInt(2, regla.getId_campoaprov());
			pstmt.setInt(3, regla.getTipo_analisis());
			pstmt.setInt(4, regla.getId_tipocampo());
			rs = pstmt.executeQuery();
			while (rs.next()) {

				regla_calidad = new ReglaCalidad();
				tipo_indicador = new TipoIndicador();

				tipo_indicador.setNombre_indicador(rs.getString("NbrIndicador"));
				regla_calidad.setId_segmento(rs.getInt("CodSegmento"));
				tipo_indicador.setId_tipo_indicador(rs.getInt("CodTipoIndicador"));
				regla_calidad.setId_campoaprov(rs.getInt("CodCampoAprov"));
				regla_calidad.setTipo_analisis(rs.getInt("CodTipoAnalisis"));
				regla_calidad.setLimite_inferior_rojo(rs.getDouble("NumLimiteInferiorRojo"));
				regla_calidad.setLimite_inferior_ambar(rs.getDouble("NumLimiteInferiorAmbar"));
				regla_calidad.setLimite_superior_ambar(rs.getDouble("NumLimiteSuperiorAmbar"));
				regla_calidad.setLimite_superior_rojo(rs.getDouble("NumLimiteSuperiorRojo"));
				regla_calidad.setFlag_aplica(rs.getBoolean("FlgAplica"));

				regla_calidad.setTipo_indicador(tipo_indicador);
				reglas.add(regla_calidad);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las regas del segmento: " + regla.getId_segmento() + " - Con CampoAprov: "
					+ regla.getId_campoaprov() + " - Con Tipo de Analisis" + regla.getTipo_analisis()
					+ " - Con Tipo de Indicador" + regla.getTipo_indicador() + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}

		return reglas;
	}

	private void crearReglasCalidad(CampoAprovisionamiento campo, SegmentoAprovisionamiento segmento, Connection conn)
			throws Exception {
		try {
			if (campo.getTipo_campo().getId_tipo_campo().equals(1)) {

				if (campo.getTipo_variable().getId_tipo_variable().equals(1)) {

					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(1), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(2), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(3), 2), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(4), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(5), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(6), 2), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(7), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(8), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(9), 2), conn);

					if (campo.getFlag_llave()) {
						crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(),
								campo.getId_campo_aprovisionamiento(), new TipoIndicador(14), 1), conn);
						crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(),
								campo.getId_campo_aprovisionamiento(), new TipoIndicador(15), 1), conn);
					}

					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(10), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(11), 2), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(12), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(13), 2), conn);

					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(16), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(17), 2), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(18), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(19), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(20), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(21), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(22), 2), conn);

				} else {

					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(1), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(2), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(3), 2), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(4), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(5), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(6), 2), conn);

					if (campo.getFlag_llave()) {
						crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(),
								campo.getId_campo_aprovisionamiento(), new TipoIndicador(14), 1), conn);
						crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(),
								campo.getId_campo_aprovisionamiento(), new TipoIndicador(15), 1), conn);
					}

					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(10), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(11), 2), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(12), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(13), 2), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(20), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(21), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(22), 2), conn);
				}
			} else {
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(1), 1), conn);
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(2), 1), conn);
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(3), 2), conn);
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(4), 1), conn);
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(5), 1), conn);
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(6), 2), conn);
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(7), 1), conn);
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(8), 1), conn);
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(9), 2), conn);

				if (campo.getFlag_llave()) {
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(14), 1), conn);
					crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
							new TipoIndicador(15), 1), conn);
				}

				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(16), 1), conn);
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(17), 2), conn);
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(18), 1), conn);
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(19), 1), conn);
				crearReglaCalidad(new ReglaCalidad(segmento.getId_segmento(), campo.getId_campo_aprovisionamiento(),
						new TipoIndicador(23), 1), conn);

			}
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear las reglas de los ficheros del campo:" + campo.getId_campo() + " / "
					+ ex.getMessage());
			throw ex;
		}
	}

	private void crearReglaCalidad(ReglaCalidad regla, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "Insert into he_reglacalidad (CodSegmento, CodCampoAprov, CodTipoIndicador, CodTipoAnalisis, NumLimiteInferiorRojo, NumLimiteInferiorAmbar, NumLimiteSuperiorAmbar, NumLimiteSuperiorRojo, FlgAplica,"
				+ " FecActualizacionTabla) values (?,?,?,?,?,?,?,?,?,?)";

		try {

			// Agregamos los campos aprovisionamiento
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, regla.getId_segmento());
			pstmt.setInt(2, regla.getId_campoaprov());
			pstmt.setInt(3, regla.getTipo_indicador().getId_tipo_indicador());
			pstmt.setInt(4, regla.getTipo_analisis());
			pstmt.setInt(5, 0);
			pstmt.setInt(6, 0);
			pstmt.setInt(7, 0);
			pstmt.setInt(8, 0);
			pstmt.setInt(9, 0);
			pstmt.setDate(10, new Date(System.currentTimeMillis()));
			pstmt.addBatch();

			pstmt.executeBatch();

		} catch (Exception ex) {
			LOGGER.error("Error al agregar una regla del campo: " + regla.getId_campoaprov() + " y segmento: "
					+ regla.getId_segmento() + " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void editarReglasCalidad(ArrayList<ReglaCalidad> reglas) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		editarReglasCalidad(reglas, conn);
		cerrarConexion(conn);
	}

	private void editarReglasCalidad(ArrayList<ReglaCalidad> reglas, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		Statement stmt = null;

		try {
			for (ReglaCalidad regla : reglas) {
				editarReglaCalidad(regla, conn);
			}
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar reglas de calidad / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarStatement(stmt);
		}
	}

	public void editarReglaCalidad(ReglaCalidad regla) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		editarReglaCalidad(regla, conn);
		cerrarConexion(conn);
	}

	private void editarReglaCalidad(ReglaCalidad regla, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "UPDATE HE_ReglaCalidad set NumLimiteInferiorRojo=?, NumLimiteInferiorAmbar=?,"
				+ " NumLimiteSuperiorAmbar=?, NumLimiteSuperiorRojo=?, FlgAplica=?, FecActualizacionTabla =?"
				+ " WHERE CodSegmento=? and CodCampoAprov=? and CodTipoIndicador=? and CodTipoAnalisis=?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setDouble(1, regla.getLimite_inferior_rojo());
			pstmt.setDouble(2, regla.getLimite_inferior_ambar());
			pstmt.setDouble(3, regla.getLimite_superior_ambar());
			pstmt.setDouble(4, regla.getLimite_superior_rojo());
			pstmt.setBoolean(5, regla.getFlag_aplica());
			pstmt.setDate(6, new Date(System.currentTimeMillis()));
			pstmt.setInt(7, regla.getId_segmento());
			pstmt.setInt(8, regla.getId_campoaprov());
			pstmt.setInt(9, regla.getTipo_indicador().getId_tipo_indicador());
			pstmt.setInt(10, regla.getTipo_analisis());
			pstmt.executeUpdate();

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar regla de calidad con id campo aprov : " + regla.getId_campoaprov() + " / "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	// Se usa para obtener los segmentos de una tabla durante el aprovisionamiento
	// para aplicar luego reglas de calidad por segmento
	public ArrayList<SegmentoAprovisionamiento> obtenerUltimosSegmentos(CargaFichero cargafichero) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<SegmentoAprovisionamiento> segmentos = new ArrayList<SegmentoAprovisionamiento>();
		String sql = "SELECT S.DesSegmento from HE_Segmento S "
				+ " left join HE_Aprovisionamiento A on S.CodAprovisionamiento = A.CodAprovisionamiento"
				+ " where A.NbrFichero=?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, cargafichero.getNombre_carga());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				segmentos.add(new SegmentoAprovisionamiento(0, rs.getString("DesSegmento")));
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener segmentos de la tabla " + cargafichero.getNombre_carga() + ". Error: "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return segmentos;
	}

	public ArrayList<CargaFichero> obtenerCargasFlagOficial(Integer id_aprovisionamiento, Connection conn)
			throws Exception {
		ArrayList<CargaFichero> cargas_flagOficial = new ArrayList<CargaFichero>();
		CargaFichero carga = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "SELECT CF.CODCARGAFICHERO, CF.CODTABLAINPUT, CF.NBRCARGA, CF.NUMORDENINICIAL, CF.NUMORDENFINAL "
				+ "        FROM he_mapeoejecucion me "
				+ "        LEFT JOIN he_ejecucion E ON e.codejecucion=me.codejecucion "
				+ "        LEFT JOIN  HE_CARGAFICHERO CF ON cf.codcargafichero=me.codcargafichero "
				+ "        LEFT JOIN HE_TABLAINPUT TABI on TABI.CODTABLAINPUT = CF.CODTABLAINPUT "
				+ "        LEFT JOIN HE_APROVISIONAMIENTO A on A.CODAPROVISIONAMIENTO = TABI.CODAPROVISIONAMIENTO "
				+ "        WHERE e.tipestadovalidacion=4 AND A.CODAPROVISIONAMIENTO=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_aprovisionamiento);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				carga = new CargaFichero();

				carga.setId_cargafichero(rs.getInt("CODCARGAFICHERO"));
				carga.setOrden_inicial(rs.getInt("NUMORDENINICIAL"));
				carga.setOrden_final(rs.getInt("NUMORDENFINAL"));
				cargas_flagOficial.add(carga);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las cargas con flag Oficial de la Tabla Input con id : "
					+ id_aprovisionamiento + " / " + ex.getMessage());
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return cargas_flagOficial;
	}

	public ArrayList<SegmentoAprovisionamiento> obtenerSegmentosPorAprov(Integer id_aprov) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		SegmentoAprovisionamiento segmento = null;
		ArrayList<SegmentoAprovisionamiento> segmentos = new ArrayList<SegmentoAprovisionamiento>();
		String sql = "Select * from HE_Segmento where CodAprovisionamiento=? order by CodSegmento";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_aprov);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				segmento = new SegmentoAprovisionamiento();
				segmento.setId_segmento(rs.getInt("CodSegmento"));
				segmento.setId_aprov(rs.getInt("CodAprovisionamiento"));
				segmento.setDescripcion(rs.getString("DesSegmento"));
				segmento.setFlag_sinsegmento(rs.getBoolean("FLGSINSEGMENTO"));
				segmentos.add(segmento);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener segmentos del aprov " + id_aprov + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return segmentos;
	}

	public ArrayList<SegmentoAprovisionamiento> obtenerSegmentosPorCampoAprov(Integer id_campoaprov) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		SegmentoAprovisionamiento segmento = null;
		ArrayList<SegmentoAprovisionamiento> segmentos = new ArrayList<SegmentoAprovisionamiento>();
		String sql = "Select S.CodSegmento, S.CodAprovisionamiento, S.DesSegmento, S.FLGSINSEGMENTO,"
				+ " max(RC.FlgAplica) as FlgAplica from HE_Segmento S"
				+ " left join HE_ReglaCalidad RC on S.CodSegmento = RC.CodSegmento"
				+ " where RC.CodSegmento in (select CodSegmento from HE_Segmento"
				+ " where CodAprovisionamiento in (select codAprovisionamiento"
				+ " from HE_CampoAprovisionamiento where CodCampoAprovisionamiento=?" + " ) and RC.CodCampoAprov=? "
				+ " GROUP BY S.CodSegmento, S.CodAprovisionamiento, S.DesSegmento, S.FLGSINSEGMENTO";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_campoaprov);
			pstmt.setInt(2, id_campoaprov);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				segmento = new SegmentoAprovisionamiento();
				segmento.setId_segmento(rs.getInt("CodSegmento"));
				segmento.setId_aprov(rs.getInt("CodAprovisionamiento"));
				segmento.setDescripcion(rs.getString("DesSegmento"));
				segmento.setFlag_sinsegmento(rs.getBoolean("FLGSINSEGMENTO"));
				segmento.setFlag_aplica(rs.getBoolean("FlgAplica"));
				segmentos.add(segmento);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener segmentos del campo aprov " + id_campoaprov + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return segmentos;
	}

	public void editarCamposSegmento(Aprovisionamiento aprov) throws Exception {
		Connection conn = null;
		try {
			conn = obtenerConexion();
			editarCamposSegmento(aprov, conn);
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar campos segmento: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarConexion(conn);
		}
	}

	protected void editarCamposSegmento(Aprovisionamiento aprov, Connection conn) throws Exception {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Boolean equal = false;
		Integer codsegmento = null;
		String select_campos = "SELECT CodSegmento from HE_Segmento WHERE CodAprovisionamiento=?";

		try {
			pstmt = conn.prepareStatement(select_campos);
			pstmt.setInt(1, aprov.getId_aprovisionamiento());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				equal = false;
				codsegmento = rs.getInt("CodSegmento");
				for (int j = 0; j < aprov.getSegmentos().size(); j++) {
					if (codsegmento.equals(aprov.getSegmentos().get(j).getId_segmento())) {
						equal = true;
						editarCampoSegmento(aprov.getSegmentos().get(j), conn);
						break;
					}
				}

				if (equal == false || aprov.getReconstruir_segmentos_calidad()) {
					borrarCampoSegmento(codsegmento, conn);
				}
			}

			for (int j = 0; j < aprov.getSegmentos().size(); j++) {
				if (aprov.getSegmentos().get(j).getId_segmento() == null || aprov.getReconstruir_segmentos_calidad()) {
					// for (CampoAprovisionamiento campo : aprov.getCampos_aprovisionamiento()) {
					crearCampoSegmento(aprov.getSegmentos().get(j), aprov.getCampos_aprovisionamiento(),
							aprov.getId_aprovisionamiento(), conn);
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Error al editar campos segmento, con id aprov: " + aprov.getId_aprovisionamiento()
					+ ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
	}

	/*
	 * public void crearCampoSegmento(Aprovisionamiento aprov) throws Exception {
	 * Connection conn = null; conn = obtenerConexion(); crearCampoSegmento(aprov,
	 * conn); cerrarConexion(conn); }
	 */

	private void crearCampoSegmento(SegmentoAprovisionamiento segmento, ArrayList<CampoAprovisionamiento> campos,
			Integer id_aprov, Connection conn) throws Exception {
		PreparedStatement pstmt = null;

		String sql = "INSERT INTO HE_Segmento(CodSegmento, CodAprovisionamiento, DesSegmento,FLGSINSEGMENTO, FecActualizacionTabla)"
				+ " VALUES (?,?,?,?,?)";

		try {
			// obtenerCampos(Integer id_aprovisionamiento, Connection conn)
			segmento.setId_segmento(obtenerSiguienteId("SEQ_HE_Segmento"));
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, segmento.getId_segmento());
			pstmt.setInt(2, id_aprov);
			pstmt.setString(3, segmento.getDescripcion());
			pstmt.setBoolean(4, segmento.getFlag_sinsegmento());
			pstmt.setDate(5, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);
			// cerrarPreparedStatement(pstmt);
			for (CampoAprovisionamiento campo : campos) {
				crearReglasCalidad(campo, segmento, conn);
			}
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar Segmento" + segmento.getId_segmento() + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void borrarCampoSegmento(Integer id_segmento) throws Exception {
		Connection conn = null;
		try {
			conn = obtenerConexion();
			borrarCampoSegmento(id_segmento, conn);
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar segmento: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarConexion(conn);
		}
	}

	private void borrarCampoSegmento(Integer id_segmento, Connection conn) throws Exception {
		PreparedStatement pstmt = null;

		ArrayList<String> list_sql = new ArrayList<String>();

		list_sql.add("DELETE FROM HE_ReglaCalidad WHERE CodSegmento = ?");
		list_sql.add("DELETE FROM HE_Segmento WHERE CodSegmento= ?");

		try {
			for (int i = 0; i < list_sql.size(); i++) {
				pstmt = conn.prepareStatement(list_sql.get(i));
				pstmt.setInt(1, id_segmento);
				pstmt.executeUpdate();
				cerrarPreparedStatement(pstmt);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al borrar segmento con id: " + id_segmento + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void editarCampoSegmento(SegmentoAprovisionamiento segmento) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		editarCampoSegmento(segmento, conn);
		cerrarConexion(conn);
	}

	private void editarCampoSegmento(SegmentoAprovisionamiento segmento, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "UPDATE HE_Segmento set DesSegmento=?, FecActualizacionTabla =? WHERE CodSegmento=?";

		try {
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, segmento.getDescripcion());
			pstmt.setDate(2, new Date(System.currentTimeMillis()));
			pstmt.setInt(3, segmento.getId_segmento());
			pstmt.executeUpdate();

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar el segmento con ID: " + segmento.getId_segmento() + " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	// Se utiliza para editar un nuevo campo de la tabla campo aprovisionamiento
	// Metodo que el que lo invoca debera manejar la conexion
	protected CampoAprovisionamiento crearNuevoCampoAprov(Integer id_aprovisionamiento, CampoAprovisionamiento campo,
			Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "INSERT INTO HE_CampoAprovisionamiento ( CodCampoAprovisionamiento, CodAprovisionamiento,"
				+ " CodTipoCampo, CodTipoVariable, NbrCampo, FlgCampoOriginal, DesFormulaNuevoCampoSql, DesFormulaNuevoCampoInput,"
				+ " FecActualizacionTabla) values (?,?,?,?,?,?,?,?,?)";
		try {
			campo.setId_campo(obtenerSiguienteId("seq_he_CampoAprovisionamiento"));
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, campo.getId_campo());
			pstmt.setInt(2, id_aprovisionamiento);
			pstmt.setInt(3, campo.getTipo_campo().getId_tipo_campo());
			pstmt.setInt(4, campo.getTipo_variable().getId_tipo_variable());
			pstmt.setString(5, campo.getNombre_campo());
			pstmt.setBoolean(6, false);
			pstmt.setString(7, campo.getFormula_nuevo_campo_input());
			pstmt.setString(8, campo.getFormula_nuevo_campo_sql());
			pstmt.setDate(9, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
		} catch (Exception ex) {
			LOGGER.error("Error al crear campo_aprovisionamiento: " + campo.getNombre_campo()
					+ "del id aprovisionamiento " + id_aprovisionamiento + ".Error : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
		return campo;
	}

	// Se utiliza para borrar un nuevo campo de la tabla campo aprovisionamiento
	// Metodo que el que lo invoca debera manejar la conexion
	protected void borrarNuevoCampoAprov(Integer id_campo_aprov, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "Delete from HE_CampoAprovisionamiento where CodCampoAprovisionamiento= ?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_campo_aprov);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			LOGGER.error(
					"Error al borrar campo_aprovisionamiento con id " + id_campo_aprov + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void editarNuevosCampos(Aprovisionamiento fichero) throws Exception {
		Connection conn = null;
		try {
			conn = obtenerConexion();

			for (CampoAprovisionamiento campo : fichero.getCampos_aprovisionamiento()) {
				editarCampoAprovisionamiento(campo, conn);
			}
			/*
			 * editarCampoAprovisionamiento(campo, conn);
			 * editarCampoAprovisionamiento(campo, conn); editarNuevoCampoInput(campo,
			 * conn);
			 */
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar nuevo campo aprovisionamiento/input: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarConexion(conn);
		}
	}

	public void editarCamposAprovisionamiento(Aprovisionamiento aprov) throws Exception {
		Connection conn = null;
		try {
			conn = obtenerConexion();
			editarCamposAprovisionamiento(aprov, conn);
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar campos: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarConexion(conn);
		}
	}

	protected void editarCamposAprovisionamiento(Aprovisionamiento aprov, Connection conn) throws Exception {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Boolean equal = false;
		Integer codcampo = null;
		Integer id_tipoCampo = null;
		Integer id_tipoVariable = null;
		Integer id_llave_nueva = 0;
		Boolean hay_segmento = false;
		Boolean update_por_llave = false;

		ArrayList<SegmentoAprovisionamiento> segmentos = null;
		// Boolean original = true;

		String select_campos = "SELECT * from HE_CampoAprovisionamiento" + " WHERE CodAprovisionamiento=?";

		segmentos = obtenerSegmentosPorAprov(aprov.getId_aprovisionamiento());

		// aprov.setSegmentos(segmentos);// ADICIONAL!

		for (CampoAprovisionamiento campo : aprov.getCampos_aprovisionamiento()) {
			if (campo.getFlag_llave()) {
				id_llave_nueva = campo.getId_campo_aprovisionamiento();
			}
			if (campo.getFlag_segmentar()) {
				hay_segmento = true;
			}
			campo.setId_aprovisionamiento(aprov.getId_aprovisionamiento()); // ADICIONAL!!!!
		}

		select_campos += " order by CodCampoAprovisionamiento asc";

		// el unico caso donde ser reinician los segmentos por completo es cuando
		// no hay segmento guardados, pero hay campo segmentador
		// hay segmentos guardador, no hay campo segmentador
		if ((hay_segmento && segmentos.size() == 1) || (!hay_segmento && segmentos.size() > 1)) {
			aprov.setReconstruir_segmentos_calidad(true);
		} else {
			aprov.setReconstruir_segmentos_calidad(false);
		}

		try {
			pstmt = conn.prepareStatement(select_campos);
			pstmt.setInt(1, aprov.getId_aprovisionamiento());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				equal = false;
				update_por_llave = false;
				codcampo = rs.getInt("CodCampoAprovisionamiento");
				id_tipoCampo = rs.getInt("CODTIPOCAMPO");
				id_tipoVariable = rs.getInt("CODTIPOVARIABLE");

				if ((rs.getInt("flgLlave") == 1 && !id_llave_nueva.equals(codcampo))
						|| (rs.getInt("flgLlave") == 0 && id_llave_nueva.equals(codcampo))) {
					update_por_llave = true;
				}

				for (int j = 0; j < aprov.getCampos_aprovisionamiento().size(); j++) {
					if (codcampo.equals(aprov.getCampos_aprovisionamiento().get(j).getId_campo_aprovisionamiento())) {
						equal = true;
						editarCampoAprovisionamiento(aprov.getCampos_aprovisionamiento().get(j), conn);
						if (id_tipoCampo != aprov.getCampos_aprovisionamiento().get(j).getTipo_campo()
								.getId_tipo_campo()
								|| id_tipoVariable != aprov.getCampos_aprovisionamiento().get(j).getTipo_variable()
										.getId_tipo_variable()
								|| update_por_llave) {
							// esta comparación es por si el tipo de variable o dato cambiaron o llave
							editarReglasCalidadDeCampo(aprov.getCampos_aprovisionamiento().get(j), segmentos, conn);

						}
						break;
					}
				}

				if (equal == false) {
					borrarCampoAprovisionamiento(codcampo, conn);
				}
			}

			for (int j = 0; j < aprov.getCampos_aprovisionamiento().size(); j++) {
				if (aprov.getCampos_aprovisionamiento().get(j).getId_campo_aprovisionamiento() == null) {
					crearCampoAprovisionamiento(aprov.getCampos_aprovisionamiento().get(j), segmentos,
							aprov.getId_aprovisionamiento(), conn);
				}
			}

		} catch (Exception ex) {
			LOGGER.error("Error al editar campos aprovisionamiento, con id aprov: " + aprov.getId_aprovisionamiento()
					+ ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
	}

	public void editarReglasCalidadDeCampo(CampoAprovisionamiento campo, ArrayList<SegmentoAprovisionamiento> segmentos,
			Connection conn) throws Exception {
		borrarReglasCalidadDeCampo(campo, conn);
		for (SegmentoAprovisionamiento segmento : segmentos) {
			this.crearReglasCalidad(campo, segmento, conn);
		}
	}

	public void borrarReglasCalidadDeCampo(CampoAprovisionamiento campo, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "delete from he_reglacalidad where codcampoaprov=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, campo.getId_campo_aprovisionamiento());
			pstmt.executeUpdate();
		} catch (Exception ex) {
			LOGGER.error("Error al borrar las reglas de calidad del campo_aprovisionamiento: " + campo.getNombre_campo()
					+ ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void editarCampoAprovisionamiento(CampoAprovisionamiento campo) throws Exception {
		Connection conn = null;
		try {
			conn = obtenerConexion();
			editarCampoAprovisionamiento(campo, conn);
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar campo: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarConexion(conn);
		}
	}

	// Se utiliza para editar un nuevo campo de la tabla campo aprovisionamiento
	// Metodo que el que lo invoca debera manejar la conexion

	protected void editarCampoAprovisionamiento(CampoAprovisionamiento campo, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		// Integer beginIndex = 0;
		// Integer endIndex = 0;
		// String substr_1 = "";

		String sql = "UPDATE HE_CampoAprovisionamiento SET NbrCampo= ?, CodTipoCampo= ?,  CodTipoVariable= ?,"
				+ " FlgCampoOriginal=?, FlgBorrarCampo = ?, FlgLlave= ?, FlgSegmentar= ?, FlgCritica= ?, FlgEvidencia= ?,"
				+ " DesFormulaNuevoCampoInput = ?, DesFormulaNuevoCampoSql = ?, FecActualizacionTabla=?"
				+ " WHERE CodCampoAprovisionamiento = ?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, campo.getNombre_campo());
			pstmt.setInt(2, campo.getTipo_campo().getId_tipo_campo());
			pstmt.setInt(3, campo.getTipo_variable().getId_tipo_variable());
			pstmt.setBoolean(4, campo.getFlag_campo_original());
			pstmt.setBoolean(5, !campo.getFlag_aprovisionar());
			pstmt.setBoolean(6, campo.getFlag_llave());
			pstmt.setBoolean(7, campo.getFlag_segmentar());
			pstmt.setBoolean(8, campo.getFlag_critica());
			pstmt.setBoolean(9, campo.getFlag_evidencia() == null ? false : campo.getFlag_evidencia());
			pstmt.setString(10, campo.getFormula_nuevo_campo_input());
			pstmt.setString(11, campo.getFormula_nuevo_campo_sql());
			pstmt.setDate(12, new Date(System.currentTimeMillis()));
			pstmt.setInt(13, campo.getId_campo_aprovisionamiento());
			pstmt.executeUpdate();
		} catch (Exception ex) {
			LOGGER.error("Error al editar campo_aprovisionamiento: " + campo.getNombre_campo() + ". Error: "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	protected void borrarCampoAprovisionamiento(Integer id_campo, Connection conn) throws Exception {
		PreparedStatement pstmt = null;

		String sql = new String();

		sql = "DELETE from HE_CampoAprovisionamiento where CodCampoAprovisionamiento= ?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_campo);
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);
		} catch (Exception ex) {
			LOGGER.error("Error al eliminar Campo Aprovisionamiento: " + id_campo + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void editarReglaSegmento(Integer id_campo_segmento, ReglaCalidad regla) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "UPDATE HE_ReglaCalidad SET NumLimiteInferiorRojo= ?, NumLimiteInferiorAmbar= ?,"
				+ " NumLimiteSuperiorAmbar= ?, NumLimiteSuperiorRojo= ?, FlgAplica= ?, FecActualizacionTabla=? "
				+ " WHERE CodSegmento= ? AND CodTipoIndicador= ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setDouble(1, regla.getLimite_inferior_rojo());
			pstmt.setDouble(2, regla.getLimite_inferior_ambar());
			pstmt.setDouble(3, regla.getLimite_superior_ambar());
			pstmt.setDouble(4, regla.getLimite_superior_rojo());
			pstmt.setBoolean(5, regla.getFlag_aplica());
			pstmt.setDate(6, new Date(System.currentTimeMillis()));
			pstmt.setInt(7, id_campo_segmento);
			pstmt.setInt(8, regla.getTipo_indicador().getId_tipo_indicador());
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar regla calidad campo segmento: "
					+ regla.getTipo_indicador().getNombre_indicador() + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public ArrayList<CampoAprovisionamiento> obtenerCampos(Aprovisionamiento aprov) throws Exception {
		Connection conn = null;
		ArrayList<CampoAprovisionamiento> campos = new ArrayList<CampoAprovisionamiento>();
		conn = obtenerConexion();
		campos = obtenerCampos(aprov, conn);
		cerrarConexion(conn);
		return campos;
	}

	protected ArrayList<CampoAprovisionamiento> obtenerCampos(Aprovisionamiento aprov, Connection conn)
			throws Exception {

		// En la clase campo se especifica si quiere obtener Campos orginales o creados
		// por el usuario, mediante el Flag
		// Campos original... Si es Null manda todos

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<CampoAprovisionamiento> campos = new ArrayList<CampoAprovisionamiento>();
		CampoAprovisionamiento campo = null;
		TipoCampo tipocampo = null;
		TipoVariable tipovariable = null;

		String sql = "select CA.CodCampoAprovisionamiento, CA.CodTipoCampo, CA.CodTipoVariable, CA.NbrCampo,"
				+ " CA.FlgCampoOriginal,CA.FlgBorrarCampo, CA.FlgLlave, CA.FlgSegmentar, CA.FlgCritica, CA.FlgEvidencia,"
				+ " CA.DesFormulaNuevoCampoInput, CA.DesFormulaNuevoCampoSql,TC.NbrTipoCampo, TV.NbrTipoVariable,"
				+ " max(RC.FlgAplica) as FlgAplica from HE_CampoAprovisionamiento CA"
				+ " left join HE_ReglaCalidad RC on RC.CodCampoAprov = CA.CodCampoAprovisionamiento"
				+ " left join ME_TipoCampo TC on CA.CodTipoCampo = TC.CodTipoCampo"
				+ " left join ME_TipoVariable TV on CA.CodTipoVariable = TV.CodTipoVariable"
				+ " left join HE_Segmento S on RC.CodSegmento = S.CodSegmento" + " where CA.CodAprovisionamiento =?";

		if (obtenerSegmentosPorAprov(aprov.getId_aprovisionamiento()).size() > 1) {
			sql += " and S.FlgSinSegmento = 0";
		}

		/*
		 * if (aprov.getCampos_aprovisionamiento() != null &&
		 * aprov.getCampos_aprovisionamiento().get(0).getFlag_campo_original() != null)
		 * { if (aprov.getCampos_aprovisionamiento().get(0).getFlag_campo_original()) {
		 * sql += " and CA.FlgCampoOriginal = 1"; } else { sql +=
		 * " and CA.FlgCampoOriginal = 0"; } }
		 */

		sql += " group by CA.CodCampoAprovisionamiento, CA.CodTipoCampo, CA.CodTipoVariable, CA.NbrCampo,"
				+ " CA.FlgCampoOriginal, CA.FlgBorrarCampo, CA.FlgLlave, CA.FlgSegmentar, CA.FlgCritica, CA.FlgEvidencia,"
				+ " CA.DesFormulaNuevoCampoInput, CA.DesFormulaNuevoCampoSql, TC.NbrTipoCampo, TV.NbrTipoVariable"
				+ " order by CA.CodCampoAprovisionamiento ASC";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, aprov.getId_aprovisionamiento());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				campo = new CampoAprovisionamiento();
				tipocampo = new TipoCampo();
				tipovariable = new TipoVariable();
				campo.setTipo_campo(tipocampo);
				campo.setTipo_variable(tipovariable);

				campo.setId_campo_aprovisionamiento(rs.getInt("CodCampoAprovisionamiento"));
				tipocampo.setId_tipo_campo(rs.getInt("CodTipoCampo"));
				tipovariable.setId_tipo_variable(rs.getInt("CodTipoVariable"));
				campo.setNombre_campo(rs.getString("NbrCampo"));
				campo.setFlag_campo_original(rs.getBoolean("FlgCampoOriginal"));
				campo.setFlag_aprovisionar(!rs.getBoolean("FlgBorrarCampo"));
				campo.setFlag_llave(rs.getBoolean("FlgLlave"));
				campo.setFlag_segmentar(rs.getBoolean("FlgSegmentar"));
				campo.setFlag_critica(rs.getBoolean("FlgCritica"));
				campo.setFlag_evidencia(rs.getBoolean("FlgEvidencia"));
				campo.setFormula_nuevo_campo_input(rs.getString("DesFormulaNuevoCampoInput"));
				campo.setFormula_nuevo_campo_sql(rs.getString("DesFormulaNuevoCampoSql"));
				tipocampo.setNombre_tipo_campo(rs.getString("NbrTipoCampo"));
				tipovariable.setNombre_tipo_variable(rs.getString("NbrTipoVariable"));
				campo.setFlag_aplica(rs.getBoolean("FlgAplica"));
				campos.add(campo);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los campos del aprovisionamiento: " + aprov.getId_aprovisionamiento()
					+ ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return campos;
	}

	// Obtener solamente la lista de nombres de campos junto al tipo de mpos
	protected ArrayList<String> obtenerNombresTiposCamposFichero(ArrayList<Campo> Campos) throws Exception {
		ArrayList<String> nombres = new ArrayList<String>();
		for (Campo campo : Campos) {
			nombres.add(campo.getNombre_campo() + campo.getTipo_campo().getNombre_tipo_campo());
		}
		return nombres;
	}

	// -------------------------------------------------------------------
	// ----------------------------FECHA--------------------------------
	// -------------------------------------------------------------------

	public ArrayList<Fecha> obtenerFechas() throws Exception {
		Connection conn = null;
		ArrayList<Fecha> ans = new ArrayList<Fecha>();
		conn = obtenerConexion();
		ans = obtenerFechas(conn);
		cerrarConexion(conn);
		return ans;
	}

	public ArrayList<Fecha> obtenerFechas(Connection conn) throws Exception {
		ArrayList<Fecha> Fechas = new ArrayList<Fecha>();
		Fecha fecha = null;
		TipoAmbito tipoAmbito = null;

		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT F.*, TA.NbrTipoAmbito FROM HE_Fecha F "
				+ "left join ME_TipoAmbito TA on F.CodTipoAmbito = TA.CodTipoAmbito ";
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				fecha = new Fecha();
				fecha.setId_fecha(rs.getInt("CodFecha"));
				fecha.setCodmes(rs.getInt("NumCodMes"));
				fecha.setFechaInicio(rs.getDate("FecInicio"));
				fecha.setFechaFin(rs.getDate("FecFin"));
				fecha.setFase(rs.getInt("NumFase"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				fecha.setTipo_ambito(tipoAmbito);

				Fechas.add(fecha);

			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener fechas: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return Fechas;
	}

	public ArrayList<Fecha> obtenerFechasPorAmbito(Connection conn, Integer id_tipo_ambito) throws Exception {
		ArrayList<Fecha> Fechas = new ArrayList<Fecha>();
		Fecha fecha = null;
		TipoAmbito tipoAmbito = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "SELECT F.*, TA.NbrTipoAmbito FROM HE_Fecha F "
				+ "left join ME_TipoAmbito TA on F.CodTipoAmbito = TA.CodTipoAmbito " + "where F.CodTipoAmbito=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_tipo_ambito);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				fecha = new Fecha();
				fecha.setId_fecha(rs.getInt("CodFecha"));
				fecha.setCodmes(rs.getInt("NumCodMes"));
				fecha.setFechaInicio(rs.getDate("FecInicio"));
				fecha.setFechaFin(rs.getDate("FecFin"));
				fecha.setFase(rs.getInt("NumFase"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				fecha.setTipo_ambito(tipoAmbito);

				Fechas.add(fecha);

			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener fechas por ambito: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return Fechas;
	}

	public void crearFecha(Fecha fecha) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearFecha(fecha, conn);
		cerrarConexion(conn);
	}

	private void crearFecha(Fecha fecha, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "INSERT INTO HE_FECHA (CodFecha, CodTipoAmbito, NumCodMes, FecInicio, FecFin, NumFase,"
				+ " FecActualizacionTabla) VALUES (?,?,?,?,?,?,?)";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, obtenerSiguienteId("SEQ_HE_Fecha"));
			pstmt.setInt(2, fecha.getTipo_ambito().getId_tipo_ambito());
			pstmt.setInt(3, fecha.getCodmes());
			pstmt.setDate(4, fecha.getFechaInicio());
			pstmt.setDate(5, fecha.getFechaFin());
			pstmt.setInt(6, fecha.getFase());
			pstmt.setDate(7, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear una entidad: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void editarFecha(Fecha fecha) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Update HE_Fecha set NumCodMes= ?, FecInicio= ?, FecFin= ?, NumFase=?,"
				+ " FecActualizacionTabla=? where CodFecha= ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, fecha.getCodmes());
			pstmt.setDate(2, fecha.getFechaInicio());
			pstmt.setDate(3, fecha.getFechaFin());
			pstmt.setInt(4, fecha.getFase());
			pstmt.setDate(5, new Date(System.currentTimeMillis()));
			pstmt.setInt(6, fecha.getId_fecha());
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar entidad: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	// -------------------------------------------------------------------
	// ----------------------------ENTIDAD--------------------------------
	// -------------------------------------------------------------------

	public void crearEntidad(Entidad entidad) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearEntidad(entidad, conn);
		cerrarConexion(conn);
	}

	private void crearEntidad(Entidad entidad, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "Insert into HE_Entidad(CodEntidad,CodMatriz,NbrEntidad,NbrVariableMotor,CodPais,"
				+ "CodTipoAmbito, FecActualizacionTabla) values (?,?,?,?,?,?,?)";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, obtenerSiguienteId("seq_he_entidad"));
			pstmt.setInt(2, entidad.getId_matriz());
			pstmt.setString(3, entidad.getNombre_entidad());
			pstmt.setString(4, entidad.getNombre_variablemotor());
			pstmt.setInt(5, entidad.getPais().getId_pais());
			pstmt.setInt(6, entidad.getTipo_ambito().getId_tipo_ambito());
			pstmt.setDate(7, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear una entidad: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void borrarEntidad(Integer id_entidad) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = new String();

		// Borramos entidad
		sql = "DELETE from HE_Entidad where CodEntidad = ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_entidad);
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar entidad con id: " + id_entidad + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void editarEntidad(Entidad entidad) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Update HE_Entidad set CodMatriz = ?, NbrEntidad = ?, NbrVariableMotor=?,"
				+ " CodPais= ?, CodTipoAmbito = ?, FecActualizacionTabla = ? where CodEntidad= ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, entidad.getId_matriz());
			pstmt.setString(2, entidad.getNombre_entidad());
			pstmt.setString(3, entidad.getNombre_variablemotor());
			pstmt.setInt(4, entidad.getPais().getId_pais());
			pstmt.setInt(5, entidad.getTipo_ambito().getId_tipo_ambito());
			pstmt.setDate(6, new Date(System.currentTimeMillis()));
			pstmt.setInt(7, entidad.getId_entidad());
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar entidad: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public ArrayList<Entidad> obtenerEntidades() throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		ArrayList<Entidad> entidades = obtenerEntidades(conn);
		cerrarConexion(conn);
		return entidades;
	}

	public ArrayList<Entidad> obtenerEntidades(Connection conn) throws Exception {
		ArrayList<Entidad> entidades = new ArrayList<Entidad>();
		Entidad entidad = null;
		Pais pais = null;
		TipoAmbito tipoAmbito = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select E.CodEntidad, E.NbrEntidad, E.NbrVariableMotor, "
				+ " E.CodPais, P.NbrPais, TA.CodTipoAmbito, TA.NbrTipoAmbito " + " from HE_Entidad E "
				+ " left join ME_Pais P on E.CodPais = P.CodPais "
				+ " left join ME_TipoAmbito TA on E.CodTipoAmbito = TA.CodTipoAmbito " + " order by CodEntidad";
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				entidad = new Entidad();
				pais = new Pais();
				tipoAmbito = new TipoAmbito();

				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				entidad.setNombre_variablemotor(rs.getString("NbrVariableMotor"));
				pais.setId_pais(rs.getInt("CodPais"));
				pais.setNombre_pais(rs.getString("NbrPais"));
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				entidad.setPais(pais);
				entidad.setTipo_ambito(tipoAmbito);
				entidades.add(entidad);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener Entidades: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return entidades;
	}

	// -------------------------------------------------------------------
	// ----------------------------CARTERA--------------------------------
	// -------------------------------------------------------------------

	public void crearCartera(Cartera cartera) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "INSERT INTO HE_Cartera(CodCartera,CodEntidad,NbrCartera,NbrVariableMotor,NumOrden,CodTipoAmbito,"
				+ "CodTipoMagnitud,CodTipoCartera,FlgPd6Meses,FecActualizacionTabla)" + " VALUES (?,?,?,?,?,?,?,?,?,?)";
		try {
			conn = obtenerConexion();

			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, obtenerSiguienteId("seq_he_cartera"));
			pstmt.setInt(2, cartera.getEntidad().getId_entidad());
			pstmt.setString(3, cartera.getNombre_cartera());
			pstmt.setString(4, cartera.getNombre_variablemotor());
			pstmt.setInt(5, cartera.getOrden());
			pstmt.setInt(6, cartera.getTipo_ambito().getId_tipo_ambito());

			if (cartera.getTipo_ambito().getNombre_tipo_ambito().toLowerCase().equals("inversiones")) {
				pstmt.setNull(7, java.sql.Types.INTEGER); // Tipo magnitud en null
				pstmt.setNull(8, java.sql.Types.INTEGER); // Tipo cartera en null
				pstmt.setNull(9, java.sql.Types.INTEGER); // flag de pd 6 meses en null
			} else if (cartera.getTipo_ambito().getNombre_tipo_ambito().toLowerCase().equals("creditos")) {
				pstmt.setInt(7, cartera.getTipo_magnitud().getId_tipo_magnitud());

				if (cartera.getTipo_magnitud().getNombre_tipo_magnitud().toLowerCase().equals("mayorista")) {
					pstmt.setNull(8, java.sql.Types.INTEGER); // Tipo cartera en null
					pstmt.setBoolean(9, cartera.getFlag_pd6meses());
				} else if (cartera.getTipo_magnitud().getNombre_tipo_magnitud().toLowerCase().equals("minorista")) {
					pstmt.setInt(8, cartera.getTipo_cartera().getId_tipo_cartera());
					pstmt.setNull(9, java.sql.Types.INTEGER); // flag de pd 6 meses en null
				}
			}

			pstmt.setDate(10, new Date(System.currentTimeMillis()));

			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar cartera de nombre " + cartera.getNombre_cartera() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public ArrayList<Cartera> obtenerCarteras() throws Exception {
		ArrayList<Cartera> carteras = new ArrayList<Cartera>();
		Cartera cartera = null;
		Entidad entidad = null;
		// Pais pais = null;
		TipoAmbito tipo_ambito = null;
		TipoMagnitud tipo_magnitud = null;
		TipoCartera tipo_cartera = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT C.*,E.NbrEntidad,TA.NbrTipoAmbito,TM.NbrTipoMagnitud, TC.NbrTipoCartera"
				+ " FROM HE_Cartera C " + " LEFT JOIN HE_Entidad E ON C.CodEntidad= E.CodEntidad"
				+ " LEFT JOIN ME_Pais P ON E.CodPais = P.CodPais "
				+ " LEFT JOIN ME_TipoAmbito TA ON C.CodTipoAmbito = TA.CodTipoAmbito"
				+ " LEFT JOIN ME_TipoMagnitud TM ON C.CodTipoMagnitud = TM.CodTipoMagnitud"
				+ " LEFT JOIN ME_TipoCartera TC ON C.CodTipoCartera = TC.CodTipoCartera order by C.CodCartera";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				cartera = new Cartera();
				entidad = new Entidad();
				tipo_ambito = new TipoAmbito();
				tipo_magnitud = new TipoMagnitud();
				tipo_cartera = new TipoCartera();

				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setNombre_variablemotor(rs.getString("NbrVariableMotor"));
				cartera.setOrden(rs.getInt("NumOrden"));
				cartera.setFlag_pd6meses(rs.getBoolean("FlgPd6Meses"));

				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));

				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));

				tipo_magnitud.setId_tipo_magnitud(rs.getInt("CodTipoMagnitud"));
				tipo_magnitud.setNombre_tipo_magnitud(rs.getString("NbrTipoMagnitud"));

				tipo_cartera.setId_tipo_cartera(rs.getInt("CodTipoCartera"));
				tipo_cartera.setNombre_tipo_cartera(rs.getString("NbrTipoCartera"));

				cartera.setTipo_ambito(tipo_ambito);
				cartera.setTipo_magnitud(tipo_magnitud);
				cartera.setTipo_cartera(tipo_cartera);

				// entidad.setPais(pais);
				cartera.setEntidad(entidad);

				carteras.add(cartera);

			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener carteras: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return carteras;
	}

	// obternerCarterasPorIdEntidad
	public ArrayList<Cartera> obtenerCarterasPorIdEntidad(Integer id_entidad) throws Exception {
		ArrayList<Cartera> carteras = new ArrayList<Cartera>();
		Cartera cartera = null;
		TipoAmbito tipoAmbito = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "SELECT C.CodCartera,C.NbrCartera,C.NbrVariableMotor,TA.CodTipoAmbito,TA.NbrTipoAmbito FROM HE_Cartera C "
				+ "left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito " + "WHERE CodEntidad=?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_entidad);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				cartera = new Cartera();
				tipoAmbito = new TipoAmbito();
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setNombre_variablemotor(rs.getString("NbrVariableMotor"));
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);
				carteras.add(cartera);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener carteras por id: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return carteras;
	}

	public Cartera obtenerCartera(Integer id_cartera) throws Exception {
		Cartera cartera = new Cartera();
		Entidad entidad = new Entidad();
		Pais pais = new Pais();
		TipoAmbito tipo_ambito = new TipoAmbito();
		TipoMagnitud tipo_magnitud = new TipoMagnitud();
		TipoCartera tipo_cartera = new TipoCartera();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "SELECT C.CodCartera,C.CodEntidad,C.NbrCartera,C.NbrVariableMotor,C.CodTipoAmbito,"
				+ " C.CodTipoMagnitud, C.CodTipoCartera, C.FlgPd6Meses,E.NbrEntidad,"
				+ " E.CodPais,P.NbrPais,TA.NbrTipoAmbito,TM.NbrTipoMagnitud FROM HE_Cartera C "
				+ " LEFT JOIN HE_Entidad E ON C.CodEntidad = E.CodEntidad "
				+ " JOIN ME_Pais P ON E.CodPais = P.CodPais "
				+ " JOIN ME_TipoAmbito TA ON C.CodTipoAmbito= TA.CodTipoAmbito "
				+ " JOIN ME_TipoMagnitud TM ON C.CodTipoMagnitud = TM.CodTipoMagnitud " + " where C.CodCartera =?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_cartera);
			rs = pstmt.executeQuery();
			while (rs.next()) {

				cartera.setId_cartera(rs.getInt("CodCartera"));
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setNombre_variablemotor(rs.getString("NbrVariableMotor"));
				cartera.setFlag_pd6meses(rs.getBoolean("FlgPd6Meses"));

				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_magnitud.setId_tipo_magnitud(rs.getInt("CodTipoMagnitud"));
				tipo_cartera.setId_tipo_cartera(rs.getInt("CodTipoCartera"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				pais.setId_pais(rs.getInt("CodPais"));
				pais.setNombre_pais(rs.getString("NbrPais"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				tipo_magnitud.setNombre_tipo_magnitud(rs.getString("NbrTipoMagnitud"));
				tipo_cartera.setNombre_tipo_cartera(rs.getString("NbrTipoCartera"));

				cartera.setTipo_ambito(tipo_ambito);
				cartera.setTipo_magnitud(tipo_magnitud);
				cartera.setTipo_cartera(tipo_cartera);
				entidad.setPais(pais);
				cartera.setEntidad(entidad);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener la Cartera con id " + id_cartera + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return cartera;
	}

	public void editarCartera(Cartera cartera) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Update HE_Cartera set CodEntidad= ?, NbrCartera= ?, NbrVariableMotor=?, NumOrden = ?, CodTipoAmbito= ?,"
				+ " CodTipoMagnitud= ?, CodTipoCartera= ?, FlgPd6Meses= ?, FecActualizacionTabla=? "
				+ "where CodCartera = ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, cartera.getEntidad().getId_entidad());
			pstmt.setString(2, cartera.getNombre_cartera());
			pstmt.setString(3, cartera.getNombre_variablemotor());
			pstmt.setInt(4, cartera.getOrden());
			pstmt.setInt(5, cartera.getTipo_ambito().getId_tipo_ambito());

			if (cartera.getTipo_ambito().getNombre_tipo_ambito().toLowerCase().equals("inversiones")) {
				pstmt.setNull(6, java.sql.Types.INTEGER); // Tipo magnitud en null
				pstmt.setNull(7, java.sql.Types.INTEGER); // Tipo cartera en null
				pstmt.setNull(8, java.sql.Types.INTEGER); // flag de pd 6 meses en null
			} else if (cartera.getTipo_ambito().getNombre_tipo_ambito().toLowerCase().equals("creditos")) {
				pstmt.setInt(6, cartera.getTipo_magnitud().getId_tipo_magnitud());

				if (cartera.getTipo_magnitud().getNombre_tipo_magnitud().toLowerCase().equals("mayorista")) {
					pstmt.setNull(7, java.sql.Types.INTEGER); // Tipo cartera en null
					pstmt.setBoolean(8, cartera.getFlag_pd6meses());
				} else if (cartera.getTipo_magnitud().getNombre_tipo_magnitud().toLowerCase().equals("minorista")) {
					pstmt.setInt(7, cartera.getTipo_cartera().getId_tipo_cartera());
					pstmt.setNull(8, java.sql.Types.INTEGER); // flag de pd 6 meses en null
				}
			}

			pstmt.setDate(9, new Date(System.currentTimeMillis()));
			pstmt.setInt(10, cartera.getId_cartera());

			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar entidad con id " + cartera.getId_cartera() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void borrarCartera(Integer id_cartera) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = new String();

		// Borramos cartera
		sql = "DELETE from HE_Cartera where CodCartera= ?";

		try {
			conn = obtenerConexion();

			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_cartera);
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar cartera con id " + id_cartera + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	// -------------------------------------------------------------------
	// -----------------------------STAGE---------------------------------
	// -------------------------------------------------------------------

	public void crearStage(Stage stage) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Insert into ME_Stage(CodStage,NbrStage,NbrVariableMotor,FlgObligatorio,FecActualizacionTabla)"
				+ " values (?,?,?,?,?)";
		try {
			conn = obtenerConexion();

			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, obtenerSiguienteId("seq_me_stage"));
			pstmt.setString(2, stage.getNombre_stage());
			pstmt.setString(3, stage.getNombre_variable_motor());
			pstmt.setBoolean(4, stage.getFlag_obligatorio());
			pstmt.setDate(5, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar stage de nombre " + stage.getNombre_stage() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public ArrayList<Stage> obtenerStages() throws Exception {
		Connection conn = null;
		ArrayList<Stage> ans = new ArrayList<Stage>();
		conn = obtenerConexion();
		ans = obtenerStages(conn);
		cerrarConexion(conn);
		return ans;
	}

	public ArrayList<Stage> obtenerStages(Connection conn) throws Exception {
		ArrayList<Stage> stages = new ArrayList<Stage>();
		Stage stage = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM ME_Stage";
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				stage = new Stage();
				stage.setId_stage(rs.getInt("CodStage"));
				stage.setNombre_stage(rs.getString("NbrStage"));
				stage.setNombre_variable_motor(rs.getString("NbrVariableMotor"));
				stage.setFlag_obligatorio(rs.getBoolean("FlgObligatorio"));
				stages.add(stage);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los stages: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return stages;
	}

	public Stage obtenerStage(Integer id_stage) throws Exception {
		Stage stage = new Stage();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select * from ME_Stage where CodStage=?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_stage);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				stage.setId_stage(rs.getInt("CodStage"));
				stage.setNombre_stage(rs.getString("NbrStage"));
				stage.setNombre_variable_motor(rs.getString("NbrVariableMotor"));
				stage.setFlag_obligatorio(rs.getBoolean("FlgObligatorio"));
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener el Stage con id " + id_stage + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return stage;
	}

	public void editarStage(Stage stage) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Update ME_Stage set NbrStage=?, NbrVariableMotor=?, FlgObligatorio=?, FecActualizacionTabla=? where CodStage=?";
		try {
			conn = obtenerConexion();

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, stage.getNombre_stage());
			pstmt.setString(2, stage.getNombre_variable_motor());
			pstmt.setBoolean(3, stage.getFlag_obligatorio());
			pstmt.setDate(4, new Date(System.currentTimeMillis()));
			pstmt.setInt(5, stage.getId_stage());
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar stage con id " + stage.getId_stage() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void borrarStage(Integer id_stage) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Delete from ME_Stage where CodStage= ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_stage);
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar stage con id " + id_stage + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	// -------------------------------------------------------------------
	// --------------------------VARIABLE---------------------------------
	// -------------------------------------------------------------------

	public void crearVariable(Variable variable) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Insert into HE_VariableCalculadaCartera(CodVariableCalculada,CodCartera,NbrVariable,"
				+ "NbrVariableSas,FecActualizacionTabla) values (?,?,?,?,?)";
		try {
			conn = obtenerConexion();

			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, obtenerSiguienteId("seq_he_varcalcartera"));
			pstmt.setInt(2, variable.getCartera().getId_cartera());
			pstmt.setString(3, variable.getNombre_variable());
			pstmt.setString(4, variable.getVariable_sas());
			pstmt.setDate(5, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error al agregar variable de nombre " + variable.getNombre_variable() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public ArrayList<Variable> obtenerVariables() throws Exception {
		ArrayList<Variable> variables = new ArrayList<Variable>();
		Variable variable = null;
		Cartera cartera = null;
		Entidad entidad = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT VCC.*, C.NbrCartera, E.CodEntidad, E.NbrEntidad "
				+ " FROM HE_VariableCalculadaCartera VCC " + " JOIN HE_Cartera C ON VCC.CodCartera= C.CodCartera "
				+ " JOIN HE_Entidad E ON E.CodEntidad= C.CodEntidad";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				variable = new Variable();
				cartera = new Cartera();
				entidad = new Entidad();
				variable.setId_variable_calculada(rs.getInt("CodVariableCalculada"));
				cartera.setId_cartera(rs.getInt("CodCartera"));
				variable.setNombre_variable(rs.getString("NbrVariable"));
				variable.setVariable_sas(rs.getString("NbrVariableSas"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				cartera.setEntidad(entidad);
				variable.setCartera(cartera);
				variables.add(variable);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las variables : " + ex.getMessage());
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return variables;
	}

	public Variable obtenerVariable(Integer id_variable) throws Exception {
		Variable variable = new Variable();
		Cartera cartera = new Cartera();
		Entidad entidad = new Entidad();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "SELECT VCC.*, C.NbrCartera, E.CodEntidad, E.NbrEntidad "
				+ " FROM HE_VariableCalculadaCartera VCC " + " JOIN HE_Cartera C ON VCC.CodCartera= C.CodCartera "
				+ " JOIN HE_Entidad E ON E.CodEntidad= C.CodEntidad" + " WHERE VCC.CodVariableCalculada=?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_variable);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				variable.setId_variable_calculada(rs.getInt("CodVariableCalculada"));
				cartera.setId_cartera(rs.getInt("CodCartera"));
				variable.setNombre_variable(rs.getString("NbrVariable"));
				variable.setVariable_sas(rs.getString("NbrVariableSas"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				cartera.setEntidad(entidad);
				variable.setCartera(cartera);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener el Variable con id " + id_variable + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return variable;
	}

	public void editarVariable(Variable variable) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Update HE_VariableCalculadaCartera set CodCartera=?, NbrVariable= ?,"
				+ " NbrVariableSas= ?, FecActualizacionTabla=? where CodVariableCalculada = ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, variable.getCartera().getId_cartera());
			pstmt.setString(2, variable.getNombre_variable());
			pstmt.setString(3, variable.getVariable_sas());
			pstmt.setDate(4, new Date(System.currentTimeMillis()));
			pstmt.setInt(5, variable.getId_variable_calculada());
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar variable con id " + variable.getId_variable_calculada() + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void borrarVariable(Integer id_variable) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Delete from HE_VariableCalculadaCartera where CodVariableCalculada=?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_variable);
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar variable con id " + id_variable + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void crearEscenario(Escenario escenario) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "INSERT INTO ME_Escenario(CodEscenario,NbrEscenario,NbrVariableMotor,FecActualizacionTabla)"
				+ " VALUES (?,?,?,?)";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, obtenerSiguienteId("seq_me_escenario"));
			pstmt.setString(2, escenario.getNombre_escenario());
			pstmt.setString(3, escenario.getNombre_variable_motor());
			pstmt.setDate(4, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear un escenario: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void borrarEscenario(Integer id_escenario) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = new String();

		// Borrar escenario
		sql = "DELETE from ME_Escenario where CodEscenario= ?";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_escenario);
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar escenario con id: " + id_escenario + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void editarEscenario(Escenario escenario) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Update ME_Escenario set NbrEscenario=?, NbrVariableMotor=?, FecActualizacionTabla=? where CodEscenario= ?";
		try {
			conn = obtenerConexion();

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, escenario.getNombre_escenario());
			pstmt.setString(2, escenario.getNombre_variable_motor());
			pstmt.setDate(3, new Date(System.currentTimeMillis()));
			pstmt.setInt(4, escenario.getId_escenario());
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar escenario" + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public ArrayList<Escenario> obtenerEscenarios() throws Exception {
		Connection conn = null;
		ArrayList<Escenario> ans = new ArrayList<Escenario>();
		conn = obtenerConexion();
		ans = obtenerEscenarios(conn);
		cerrarConexion(conn);
		return ans;
	}

	public ArrayList<Escenario> obtenerEscenarios(Connection conn) throws Exception {
		ArrayList<Escenario> escenarios = new ArrayList<Escenario>();
		Escenario escenario = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select * from ME_Escenario order by CodEscenario";
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				escenario = new Escenario();
				escenario.setId_escenario(rs.getInt("CodEscenario"));
				escenario.setNombre_variable_motor(rs.getString("NbrVariableMotor"));
				escenario.setNombre_escenario(rs.getString("NbrEscenario"));
				escenario.setFlag_obligatorio(rs.getBoolean("FlgObligatorio"));
				escenarios.add(escenario);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener escenarios: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return escenarios;
	}

	// -------------------------------------------------------------------
	// -----------------------------BLOQUE--------------------------------
	// -------------------------------------------------------------------

	public ArrayList<CampoBloque> obtenerCamposBloquePorIdBloque(Integer id_bloque) throws Exception {
		Connection conn = null;
		ArrayList<CampoBloque> ans = new ArrayList<CampoBloque>();
		conn = obtenerConexion();
		ans = obtenerCamposBloquePorIdBloque(id_bloque, conn);
		cerrarConexion(conn);
		return ans;
	}

	private ArrayList<CampoBloque> obtenerCamposBloquePorIdBloque(Integer id_bloque, Connection conn) throws Exception {

		ArrayList<CampoBloque> campos_bloques = null;
		TipoCampo tipo_campo = null;
		CampoBloque campo_bloque = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		String sql = "select CB.*, TC.NbrTipoCampo from HE_CampoBloque CB "
				+ " JOIN ME_TipoCampo TC on CB.CodTipoCampo = TC.CodTipoCampo where CB.CodBloque=?"
				+ " order by cb.codcampobloque asc";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_bloque);
			rs = pstmt.executeQuery();
			campos_bloques = new ArrayList<CampoBloque>();
			campo_bloque = new CampoBloque();
			tipo_campo = new TipoCampo();
			while (rs.next()) {
				campo_bloque = new CampoBloque();
				tipo_campo = new TipoCampo();
				campo_bloque.setId_campo(rs.getInt("CodCampoBloque"));
				campo_bloque.setId_bloque(rs.getInt("CodBloque"));
				campo_bloque.setNombre_campo(rs.getString("NbrCampo"));
				campo_bloque.setVariable_motor(rs.getString("NbrVariableMotor"));
				tipo_campo.setId_tipo_campo(rs.getInt("CodTipoCampo"));
				tipo_campo.setNombre_tipo_campo(rs.getString("NbrTipoCampo"));
				campo_bloque.setTipo_campo(tipo_campo);
				campos_bloques.add(campo_bloque);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener campos del bloque con id " + id_bloque + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return campos_bloques;
	}

	///////////////////////

	public ArrayList<Cartera> obtenerCarterasExtrapolacionporIdMetodologia(Integer id_metodologia) throws Exception {
		Connection conn = null;
		ArrayList<Cartera> ans = new ArrayList<Cartera>();
		conn = obtenerConexion();
		ans = obtenerCarterasExtrapolacionporIdMetodologia(id_metodologia, conn);
		cerrarConexion(conn);
		return ans;
	}

	private ArrayList<Cartera> obtenerCarterasExtrapolacionporIdMetodologia(Integer id_metodologia, Connection conn)
			throws Exception {
		ArrayList<Cartera> carterasExtrapolacion = new ArrayList<Cartera>();
		Cartera cartera = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select C.CodCartera,C.NbrCartera from HE_MetodologiaExtrapolacion ME "
				+ " left join HE_Cartera C on ME.CodCarteraExtrapol= C.CodCartera where ME.CodMetodologia=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_metodologia);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				cartera = new Cartera();
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				carterasExtrapolacion.add(cartera);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener carteras a extrapolar por id metodologia: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return carterasExtrapolacion;
	}

	///////////////////////////

	public ArrayList<Bloque> obtenerBloques() throws Exception {
		Connection conn = null;
		ArrayList<Bloque> ans = new ArrayList<Bloque>();
		conn = obtenerConexion();
		ans = obtenerBloques(conn);
		cerrarConexion(conn);
		return ans;
	}

	private ArrayList<Bloque> obtenerBloques(Connection conn) throws Exception {
		ArrayList<Bloque> bloques = new ArrayList<Bloque>();
		Bloque bloque = null;
		FormatoInput formatoInput = null;
		PrimerPaso primerPaso = null;
		TipoBloque tipo_bloque = null;
		TipoEjecucion tipo_ejecucion = null;
		TipoFlujo tipo_flujo = null;
		TipoAmbito tipo_ambito = null;
		TipoObtencionResultados tipo_obtencion_resultados = null;

		Statement stmt = null;
		PreparedStatement pstmt_dep = null;
		PreparedStatement pstmt_dep_oficial = null;
		ResultSet rs = null;
		ResultSet rs_dep = null;
		ResultSet rs_dep_oficial = null;

		String sql = "select B.CodBloque,B.NbrBloque,B.NbrVariableMotor,B.FlgTablaInput,"
				+ " FI.CodFormatoInput, FI.NbrFormatoInput, TB.*, PP.*, TE.*, TF.*, " + " TA.NbrTipoAmbito, TOR.* "
				+ " from HE_Bloque B " + " left join ME_FormatoInput FI on B.CodFormatoInput = FI.CodFormatoInput "
				+ " left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque "
				+ " left join HE_PrimerPaso PP on PP.CodPrimerPaso = TB.CodPrimerPaso "
				+ " left join ME_TipoEjecucion TE on TE.CodTipoEjecucion = TB.CodTipoEjecucion "
				+ " left join ME_TipoFlujo TF on TF.CodTipoFlujo = TE.CodTipoFlujo "
				+ " left join ME_TipoAmbito TA on TF.CodTipoAmbito = TA.CodTipoAmbito "
				+ " left join ME_TipoObtencionResultados TOR on TOR.CodTipoObtenResult = TB.CodTipoObtenResult ";

		String sql_dep = "select CodMetodologia from HE_MapeoMetodologiaBloque where CodBloque=?";

		// Asociado a ejecuciones oficiales
		String sql_dep_oficial = "select ME.CodMapeoTabla from HE_MapeoEjecucion ME "
				+ "left join HE_MapeoTablaBloque MTB on ME.CodMapeoTabla = MTB.CodMapeoTabla "
				+ "left join HE_MapeoMetodologiaBloque MMB on MTB.CodMetodologia = MMB.CodMetodologia "
				+ "and MTB.CodBloque = MMB.CodBloque " + "left join HE_Ejecucion E on ME.CodEjecucion = E.CodEjecucion "
				+ "where E.TipEstadoValidacion = 4 and MMB.CodBloque=?";

		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				bloque = new Bloque();

				// Seteamos si existen dependencias
				pstmt_dep = conn.prepareStatement(sql_dep);
				pstmt_dep.setInt(1, rs.getInt("CodBloque"));
				rs_dep = pstmt_dep.executeQuery();

				bloque.setConDependencias(false);
				while (rs_dep.next()) {
					bloque.setConDependencias(true);
				}
				cerrarResultSet(rs_dep);
				cerrarPreparedStatement(pstmt_dep);

				// Seteamos si existen dependencias oficiales
				pstmt_dep_oficial = conn.prepareStatement(sql_dep_oficial);
				pstmt_dep_oficial.setInt(1, rs.getInt("CodBloque"));
				rs_dep_oficial = pstmt_dep_oficial.executeQuery();

				bloque.setConDependenciasOficial(false);
				while (rs_dep_oficial.next()) {
					bloque.setConDependenciasOficial(true);
				}
				cerrarResultSet(rs_dep_oficial);
				cerrarPreparedStatement(pstmt_dep_oficial);

				bloque.setId_bloque(rs.getInt("CodBloque"));
				bloque.setNombre_bloque(rs.getString("NbrBloque"));
				bloque.setVariable_motor(rs.getString("NbrVariableMotor"));
				bloque.setFlag_tabla_input(rs.getBoolean("FlgTablaInput"));

				formatoInput = new FormatoInput();
				formatoInput.setId_formato_input(rs.getInt("CodFormatoInput"));
				formatoInput.setNombre_formato_input(rs.getString("NbrFormatoInput"));
				bloque.setFormato_input(formatoInput);

				tipo_bloque = new TipoBloque();
				tipo_bloque.setId_tipo_bloque(rs.getInt("CodTipoBloque"));
				tipo_bloque.setNombre_tipo_bloque(rs.getString("NbrTipoBloque"));
				tipo_bloque.setFlag_bucket(rs.getBoolean("FlgBucket"));

				if (rs.getInt("CodPrimerPaso") != 0) { // Null
					primerPaso = new PrimerPaso();
					primerPaso.setId_primerPaso(rs.getInt("CodPrimerPaso"));
					primerPaso.setNombre_primerPaso(rs.getString("NbrPrimerPaso"));
					primerPaso.setDescripcion_primerPaso(rs.getString("DesPrimerPaso"));
					tipo_bloque.setPrimerPaso(primerPaso);
				}

				tipo_ejecucion = new TipoEjecucion();
				tipo_ejecucion.setId_tipo_ejecucion(rs.getInt("CodTipoEjecucion"));
				tipo_ejecucion.setNombre_tipo_ejecucion(rs.getString("NbrTipoEjecucion"));

				tipo_flujo = new TipoFlujo();
				tipo_flujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipo_flujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));

				tipo_ambito = new TipoAmbito();
				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				tipo_flujo.setTipo_ambito(tipo_ambito);

				tipo_ejecucion.setTipo_flujo(tipo_flujo);

				tipo_bloque.setTipo_ejecucion(tipo_ejecucion);

				tipo_obtencion_resultados = new TipoObtencionResultados();
				tipo_obtencion_resultados.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
				tipo_obtencion_resultados.setNombre_tipo_obtencion_resultados(rs.getString("NbrTipoObtenResult"));
				tipo_bloque.setTipo_obtencion_resultados(tipo_obtencion_resultados);

				bloque.setTipo_bloque(tipo_bloque);

				bloques.add(bloque);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener bloques: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarResultSet(rs_dep);
			cerrarResultSet(rs_dep_oficial);
			cerrarStatement(stmt);
			cerrarPreparedStatement(pstmt_dep);
			cerrarPreparedStatement(pstmt_dep_oficial);
		}
		return bloques;
	}

	////////////////////////////

	public void crearBloque(Bloque bloque) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearBloque(bloque, conn);
		cerrarConexion(conn);
	}

	private void crearBloque(Bloque bloque, Connection conn) throws Exception {
		PreparedStatement pstmt_campos = null;
		PreparedStatement pstmt_bloque = null;

		String sql_bloque = "INSERT INTO HE_Bloque(CodBloque, NbrBloque, NbrVariableMotor, FlgTablaInput, "
				+ "CodFormatoInput, CodTipoBloque, FecActualizacionTabla) VALUES (?,?,?,?,?,?,?)";
		String sql_campo_bloque = "INSERT INTO HE_CampoBloque(CodCampoBloque,CodBloque,NbrCampo,"
				+ "NbrVariableMotor,CodTipoCampo,FecActualizacionTabla) VALUES (?,?,?,?,?,?)";

		try {
			pstmt_bloque = conn.prepareStatement(sql_bloque);
			Integer siguiente = obtenerSiguienteId("seq_he_bloque");
			pstmt_bloque.setInt(1, siguiente);
			pstmt_bloque.setString(2, bloque.getNombre_bloque());
			pstmt_bloque.setString(3, bloque.getVariable_motor());
			pstmt_bloque.setBoolean(4, bloque.getFlag_tabla_input());

			if (bloque.getFlag_tabla_input()) {
				pstmt_bloque.setInt(5, bloque.getFormato_input().getId_formato_input());
			} else {
				pstmt_bloque.setNull(5, java.sql.Types.INTEGER);
			}

			pstmt_bloque.setInt(6, bloque.getTipo_bloque().getId_tipo_bloque());
			pstmt_bloque.setDate(7, new Date(System.currentTimeMillis()));
			pstmt_bloque.executeUpdate();
			cerrarPreparedStatement(pstmt_bloque);

			pstmt_campos = conn.prepareStatement(sql_campo_bloque);
			for (int j = 0; j < bloque.getCampos_bloque().size(); j++) {
				pstmt_campos.setInt(1, obtenerSiguienteId("seq_he_campobloque"));
				pstmt_campos.setInt(2, siguiente);
				pstmt_campos.setString(3, bloque.getCampos_bloque().get(j).getNombre_campo());
				pstmt_campos.setString(4, bloque.getCampos_bloque().get(j).getVariable_motor());
				pstmt_campos.setInt(5, bloque.getCampos_bloque().get(j).getTipo_campo().getId_tipo_campo());
				pstmt_campos.setDate(6, new Date(System.currentTimeMillis()));
				pstmt_campos.addBatch();
			}
			pstmt_campos.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar el bloque con id " + bloque.getId_bloque() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_bloque);
			cerrarPreparedStatement(pstmt_campos);
		}
	}

	//////////////////////////

	public void editarBloque(Bloque bloque) throws Exception {
		Connection conn;
		conn = obtenerConexion();
		editarBloque(bloque, conn);
		cerrarConexion(conn);
	}

	private void editarBloque(Bloque bloque, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		PreparedStatement pstmt_select = null;
		PreparedStatement up_pstmt = null;
		ResultSet rs = null;
		Boolean equal = false;

		String select_campos = "SELECT CB.CodCampoBloque from HE_CampoBloque CB WHERE CodBloque=?";
		String update_campos = "UPDATE HE_CampoBloque SET NbrCampo=?, NbrVariableMotor=?, CodTipoCampo=?,"
				+ "FecActualizacionTabla=? WHERE CodCampoBloque= ?";
		String update_bloques = "UPDATE HE_Bloque SET NbrBloque= ?, NbrVariableMotor= ?, FlgTablaInput=?, "
				+ " CodFormatoInput=?, CodTipoBloque=?, FecActualizacionTabla=? where CodBloque= ?";

		try {
			pstmt_select = conn.prepareStatement(select_campos);
			pstmt_select.setInt(1, bloque.getId_bloque());
			rs = pstmt_select.executeQuery();

			while (rs.next()) {
				equal = false;

				for (int j = 0; j < bloque.getCampos_bloque().size(); j++) {
					if ((bloque.getCampos_bloque().get(j).getId_campo() != null)
							&& (rs.getInt("CodCampoBloque") == bloque.getCampos_bloque().get(j).getId_campo())) {
						equal = true;
						up_pstmt = conn.prepareStatement(update_campos);
						up_pstmt.setString(1, bloque.getCampos_bloque().get(j).getNombre_campo());
						up_pstmt.setString(2, bloque.getCampos_bloque().get(j).getVariable_motor());
						up_pstmt.setInt(3, bloque.getCampos_bloque().get(j).getTipo_campo().getId_tipo_campo());
						up_pstmt.setDate(4, new Date(System.currentTimeMillis()));
						up_pstmt.setInt(5, rs.getInt("CodCampoBloque"));
						up_pstmt.executeUpdate();
						cerrarPreparedStatement(up_pstmt);
						break;
					}
				}

				if (equal == false) {
					borrarCampoBloque(rs.getInt("CodCampoBloque"), conn);
				}
			}

			for (int j = 0; j < bloque.getCampos_bloque().size(); j++) {
				if (bloque.getCampos_bloque().get(j).getId_campo() == null) {
					bloque.getCampos_bloque().get(j).setId_bloque(bloque.getId_bloque());
					crearCampoBloque(bloque.getCampos_bloque().get(j), conn);
				}
			}

			cerrarResultSet(rs);
			pstmt = conn.prepareStatement(update_bloques);
			pstmt.setString(1, bloque.getNombre_bloque());
			pstmt.setString(2, bloque.getVariable_motor());
			pstmt.setBoolean(3, bloque.getFlag_tabla_input());

			if (bloque.getFlag_tabla_input()) {
				pstmt.setInt(4, bloque.getFormato_input().getId_formato_input());
			} else {
				pstmt.setNull(4, java.sql.Types.INTEGER);
			}

			pstmt.setInt(5, bloque.getTipo_bloque().getId_tipo_bloque());
			pstmt.setDate(6, new Date(System.currentTimeMillis()));
			pstmt.setInt(7, bloque.getId_bloque());
			pstmt.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar el bloque con id " + bloque.getId_bloque() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarPreparedStatement(pstmt_select);
			cerrarPreparedStatement(up_pstmt);
		}
	}

	///////////////////////

	public void borrarBloque(Integer id_bloque) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		borrarBloque(id_bloque, conn);
		cerrarConexion(conn);
	}

	private void borrarBloque(Integer id_bloque, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = new String();

		// Borramos bloque
		sql = "DELETE from HE_Bloque where CodBloque= ?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_bloque);
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar el bloque con id " + id_bloque + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	// ------------------------------------------------------------------- //
	// --------------------------- CAMPO BLOQUE -------------------------- //
	// ------------------------------------------------------------------- //

	private void crearCampoBloque(CampoBloque campo_bloque, Connection conn) throws Exception {
		PreparedStatement pstmt_campo_bloque = null;
		PreparedStatement pstmt_mapeo_tabla = null;
		PreparedStatement pstmt_registro_campo = null;

		String sql_campo_bloque = "INSERT INTO HE_CampoBloque(CodCampoBloque,CodBloque,NbrCampo,"
				+ "NbrVariableMotor,CodTipoCampo,FecActualizacionTabla) VALUES (?,?,?,?,?,?)";
		String sql_mapeo_tabla = "SELECT CodMapeoTabla from HE_MapeoTablaBloque where CodBloque=?";
		String sql_registro_campo = "INSERT INTO HE_RegistroCampoBloque(CodMapeoTabla, CodCampoBloque, FecActualizacionTabla)"
				+ " VALUES (?,?,?)";
		ResultSet rs = null;
		Integer id_campo_bloque = null;
		try {
			id_campo_bloque = obtenerSiguienteId("seq_he_campobloque");
			campo_bloque.setId_campo(id_campo_bloque);
			pstmt_campo_bloque = conn.prepareStatement(sql_campo_bloque);
			pstmt_campo_bloque.setInt(1, campo_bloque.getId_campo());
			pstmt_campo_bloque.setInt(2, campo_bloque.getId_bloque());
			pstmt_campo_bloque.setString(3, campo_bloque.getNombre_campo());
			pstmt_campo_bloque.setString(4, campo_bloque.getVariable_motor());
			pstmt_campo_bloque.setInt(5, campo_bloque.getTipo_campo().getId_tipo_campo());
			pstmt_campo_bloque.setDate(6, new Date(System.currentTimeMillis()));
			pstmt_campo_bloque.executeUpdate();

			pstmt_mapeo_tabla = conn.prepareStatement(sql_mapeo_tabla);
			pstmt_mapeo_tabla.setInt(1, campo_bloque.getId_bloque());

			rs = pstmt_mapeo_tabla.executeQuery();

			pstmt_registro_campo = conn.prepareStatement(sql_registro_campo);
			while (rs.next()) {
				pstmt_registro_campo.setInt(1, rs.getInt("CodMapeoTabla"));
				pstmt_registro_campo.setInt(2, campo_bloque.getId_campo());
				pstmt_registro_campo.setDate(3, new Date(System.currentTimeMillis()));
				pstmt_registro_campo.addBatch();
			}

			pstmt_registro_campo.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error al crear el campo bloque con id " + campo_bloque.getId_campo() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt_campo_bloque);
			cerrarPreparedStatement(pstmt_mapeo_tabla);
			cerrarPreparedStatement(pstmt_registro_campo);
		}
	}

	private void borrarCampoBloque(Integer id_campo_bloque, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = new String();
		sql = "Delete from HE_CampoBloque where CodCampoBloque= ?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_campo_bloque);
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar el campo con id " + id_campo_bloque + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	// ------------------------------------------------------------------- //
	// -----------------------ESTRUCTURA METODOLOGICA--------------------- //
	// ------------------------------------------------------------------- //

	public ArrayList<EstructuraMetodologica> obtenerMetodologias() throws Exception {
		Connection conn = null;
		ArrayList<EstructuraMetodologica> ans = new ArrayList<EstructuraMetodologica>();
		conn = obtenerConexion();
		ans = obtenerMetodologias(conn);
		cerrarConexion(conn);
		return ans;
	}

	private ArrayList<EstructuraMetodologica> obtenerMetodologias(Connection conn) throws Exception {
		ArrayList<EstructuraMetodologica> metodologias = new ArrayList<EstructuraMetodologica>();

		EstructuraMetodologica metodologia = null;
		TipoFlujo tipo_flujo = null;
		TipoPeriodicidad tipo_periodicidad = null;
		MotorCalculo motor = null;
		TipoObtencionResultados metodo_resultados = null;
		TipoMetodologia metodologia_PD = null;
		TipoMetodologia metodologia_LGD = null;
		TipoMetodologia metodologia_LGD_WO = null;
		TipoMetodologia metodologia_LGD_BE = null;
		TipoMetodologia metodologia_EAD = null;
		TipoAmbito tipoAmbito = null;
		Cartera cartera = null;
		Entidad entidad = null;

		Statement stmt = null;
		PreparedStatement pstmt_dep = null;
		PreparedStatement pstmt_dep_oficial = null;
		ResultSet rs = null;
		ResultSet rs_dep = null;
		ResultSet rs_dep_oficial = null;

		String sql = "select M.*, C.NbrCartera, TA.CodTipoAmbito, TA.NbrTipoAmbito, "
				+ " E.CodEntidad, E.NbrEntidad, TF.NbrTipoFlujo, TP.NbrTipoPeriodicidad, "
				+ " MC.NbrMotor, R.NbrTipoObtenResult, TM_PD.NbrTipoMetodologia as Metodologia_PD, "
				+ " TM_LGD.NbrTipoMetodologia as Metodologia_LGD,"
				+ " TM_LGD_WO.NbrTipoMetodologia as Metodologia_LGD_WO,"
				+ " TM_LGD_BE.NbrTipoMetodologia as Metodologia_LGD_BE,"
				+ " TM_EAD.NbrTipoMetodologia as Metodologia_EAD " + " from HE_MetodologiaCartera M "
				+ " left join HE_Cartera C on M.CodCartera= C.CodCartera"
				+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
				+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad"
				+ " left join ME_TipoFlujo TF on M.CodTipoFlujo= TF.CodTipoFlujo "
				+ " left join ME_TipoPeriodicidad TP on M.CodTipoPeriodicidad= TP.CodTipoPeriodicidad "
				+ " left join ME_MotorCalculo MC on M.CodMotor= MC.CodMotor"
				+ " left join ME_TipoObtencionResultados R on M.CodTipoObtenResult = R.CodTipoObtenResult"
				+ " left join ME_TipoMetodologia TM_PD on M.CodTipoMetodologiaPD = TM_PD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TM_LGD on M.CodTipoMetodologiaLGD = TM_LGD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TM_LGD_WO on M.CodTipoMetodologiaLGDWO = TM_LGD_WO.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TM_LGD_BE on M.CodTipoMetodologiaLGDBE = TM_LGD_BE.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TM_EAD on M.CodTipoMetodologiaEAD = TM_EAD.CodTipoMetodologia "
				+ " order by CodMetodologia";

		String sql_dep = "select CodCarteraVersion from HE_CarteraVersion where CodMetodologia=?";

		// Asociado a ejecuciones oficiales
		String sql_dep_oficial = "select ME.CodMapeoTabla from HE_MapeoEjecucion ME "
				+ "left join HE_MapeoTablaBloque MTB on ME.CodMapeoTabla = MTB.CodMapeoTabla "
				+ "left join HE_MapeoMetodologiaBloque MMB on MTB.CodMetodologia = MMB.CodMetodologia "
				+ "and MTB.CodBloque = MMB.CodBloque " + "left join HE_Ejecucion E on ME.CodEjecucion = E.CodEjecucion "
				+ "where E.TipEstadoValidacion = 4 and MMB.CodMetodologia=?";

		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				metodologia = new EstructuraMetodologica();

				// Seteamos si existen dependencias
				pstmt_dep = conn.prepareStatement(sql_dep);
				pstmt_dep.setInt(1, rs.getInt("CodMetodologia"));
				rs_dep = pstmt_dep.executeQuery();

				metodologia.setConDependencias(false);
				while (rs_dep.next()) {
					metodologia.setConDependencias(true);
				}
				cerrarResultSet(rs_dep);
				cerrarPreparedStatement(pstmt_dep);

				// Seteamos si existen dependencias oficial
				pstmt_dep_oficial = conn.prepareStatement(sql_dep_oficial);
				pstmt_dep_oficial.setInt(1, rs.getInt("CodMetodologia"));
				rs_dep_oficial = pstmt_dep_oficial.executeQuery();

				metodologia.setConDependenciasOficial(false);
				while (rs_dep_oficial.next()) {
					metodologia.setConDependenciasOficial(true);
				}
				cerrarResultSet(rs_dep_oficial);
				cerrarPreparedStatement(pstmt_dep_oficial);

				tipoAmbito = new TipoAmbito();
				cartera = new Cartera();
				entidad = new Entidad();
				tipo_flujo = new TipoFlujo();
				tipo_periodicidad = new TipoPeriodicidad();
				motor = new MotorCalculo();
				metodo_resultados = new TipoObtencionResultados();
				metodologia_PD = new TipoMetodologia();
				metodologia_LGD = new TipoMetodologia();
				metodologia_LGD_WO = new TipoMetodologia();
				metodologia_LGD_BE = new TipoMetodologia();
				metodologia_EAD = new TipoMetodologia();

				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
				cartera.setId_cartera(rs.getInt("CodCartera"));
				metodologia.setDescripcion_metodologia(rs.getString("DesMetodologia"));
				metodologia.setNro_buckets(rs.getInt("NumBuckets"));
				metodologia.setLife_time(rs.getInt("NumLifeTime"));
				metodologia.setNro_maxPlazo(rs.getInt("NumMaxPlazo"));
				tipo_flujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipo_periodicidad.setId_tipo_periodicidad(rs.getInt("CodTipoPeriodicidad"));

				metodologia.setFlag_lgdUnica(rs.getBoolean("FlgLgdUnica"));
				metodologia.setCodigo_estructura(rs.getString("NbrCodigoEstructura"));
				metodologia.setNombre_funcPlumbr(rs.getString("NbrFuncPlumbr"));
				metodologia.setPuerto_plumbr(rs.getInt("NumPuertoPlumbr"));

				motor.setId_motor(rs.getInt("CodMotor"));

				metodo_resultados.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));

				metodologia_PD.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaPD"));
				metodologia_LGD.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGD"));
				metodologia_LGD_WO.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGDWO"));
				metodologia_LGD_BE.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGDBE"));
				metodologia_EAD.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaEAD"));

				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));

				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));

				tipo_flujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));
				tipo_periodicidad.setNombre_tipo_periodicidad(rs.getString("NbrTipoPeriodicidad"));
				motor.setNombre_motor(rs.getString("NbrMotor"));
				metodo_resultados.setNombre_tipo_obtencion_resultados(rs.getString("NbrTipoObtenResult"));
				metodologia_PD.setNombre_tipo_metodologia(rs.getString("Metodologia_PD"));
				metodologia_LGD_WO.setNombre_tipo_metodologia(rs.getString("Metodologia_LGD_WO"));
				metodologia_LGD_BE.setNombre_tipo_metodologia(rs.getString("Metodologia_LGD_BE"));
				metodologia_EAD.setNombre_tipo_metodologia(rs.getString("metodologia_EAD"));

				cartera.setEntidad(entidad);
				cartera.setTipo_ambito(tipoAmbito);
				tipo_flujo.setTipo_ambito(tipoAmbito);
				metodologia.setCartera(cartera);
				metodologia.setFlujo(tipo_flujo);
				metodologia.setPeriodicidad(tipo_periodicidad);
				metodologia.setMotor(motor);
				metodologia.setMetodo_resultados(metodo_resultados);
				metodologia.setMetodologia_PD(metodologia_PD);
				metodologia.setMetodologia_LGD(metodologia_LGD);
				metodologia.setMetodologia_LGD_WO(metodologia_LGD_WO);
				metodologia.setMetodologia_LGD_BE(metodologia_LGD_BE);
				metodologia.setMetodologia_EAD(metodologia_EAD);

				metodologias.add(metodologia);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener el metodologias : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarResultSet(rs_dep);
			cerrarResultSet(rs_dep_oficial);
			cerrarStatement(stmt);
			cerrarPreparedStatement(pstmt_dep);
			cerrarPreparedStatement(pstmt_dep_oficial);
		}
		return metodologias;
	}

	public EstructuraMetodologica obtenerMetodologia(Integer id_metodologia) throws Exception {
		Connection conn = null;
		EstructuraMetodologica ans = null;
		conn = obtenerConexion();
		ans = obtenerMetodologia(id_metodologia, conn);
		cerrarConexion(conn);
		return ans;
	}

	private EstructuraMetodologica obtenerMetodologia(Integer id_metodologia, Connection conn) throws Exception {
		EstructuraMetodologica metodologia = null;
		TipoFlujo tipo_flujo;
		TipoPeriodicidad tipo_periodicidad;
		MotorCalculo motor;
		TipoObtencionResultados metodo_resultados;
		TipoMetodologia metodologia_PD;
		TipoMetodologia metodologia_LGD;
		TipoMetodologia metodologia_LGD_WO;
		TipoMetodologia metodologia_LGD_BE;
		TipoMetodologia metodologia_EAD;
		Bloque bloque = null;
		Cartera cartera = null;
		TipoAmbito tipoAmbito = null;
		Entidad entidad = null;
		TipoBloque tipo_bloque = null;
		PrimerPasoConfigurado primerPaso = null;

		ArrayList<Bloque> bloques = null;
		ArrayList<Cartera> carteras_extrapol = null;
		ArrayList<PrimerPasoConfigurado> grupo_primerosPasos;

		Statement stmt_estructura = null;
		Statement stmt_carterasExtrapol = null;
		Statement stmt_primerosPasos = null;
		Statement stmt_bloques = null;
		Statement stmt_bloques1_1 = null;
		Statement stmt_bloques1_2 = null;
		Statement stmt_bloques2 = null;
		Statement stmt_bloquePrincipal = null;
		Statement stmt_bloquesPrimerPaso = null;
		Statement stmt_dep = null;

		ResultSet rs_estructura = null;
		ResultSet rs_carterasExtrapol = null;
		ResultSet rs_primerosPasos = null;
		ResultSet rs_bloques = null;
		ResultSet rs_bloques1_1 = null;
		ResultSet rs_bloques1_2 = null;
		ResultSet rs_bloques2 = null;
		ResultSet rs_bloquePrincipal = null;
		ResultSet rs_bloquesPrimerPaso = null;
		ResultSet rs_dep = null;

		String sql_estructura = "select M.*, C.NbrCartera, TA.CodTipoAmbito, TA.NbrTipoAmbito, "
				+ " E.CodEntidad, E.NbrEntidad, TF.NbrTipoFlujo, "
				+ " TP.NbrTipoPeriodicidad, MC.NbrMotor, R.NbrTipoObtenResult, "
				+ " TM_PD.NbrTipoMetodologia as Metodologia_PD, " + " TM_LGD.NbrTipoMetodologia as Metodologia_LGD,"
				+ " TM_LGD_WO.NbrTipoMetodologia as Metodologia_LGD_WO,"
				+ " TM_LGD_BE.NbrTipoMetodologia as Metodologia_LGD_BE,"
				+ " TM_EAD.NbrTipoMetodologia as Metodologia_EAD " + " from HE_MetodologiaCartera M "
				+ " left join HE_Cartera C on M.CodCartera= C.CodCartera"
				+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
				+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad"
				+ " left join ME_TipoFlujo TF on M.CodTipoFlujo= TF.CodTipoFlujo "
				+ " left join ME_TipoPeriodicidad TP on M.CodTipoPeriodicidad= TP.CodTipoPeriodicidad "
				+ " left join ME_MotorCalculo MC on M.CodMotor= MC.CodMotor"
				+ " left join ME_TipoObtencionResultados R on M.CodTipoObtenResult = R.CodTipoObtenResult"
				+ " left join ME_TipoMetodologia TM_PD on M.CodTipoMetodologiaPD = TM_PD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TM_LGD on M.CodTipoMetodologiaLGD = TM_LGD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TM_LGD_WO on M.CodTipoMetodologiaLGDWO = TM_LGD_WO.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TM_LGD_BE on M.CodTipoMetodologiaLGDBE = TM_LGD_BE.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TM_EAD on M.CodTipoMetodologiaEAD = TM_EAD.CodTipoMetodologia "
				+ " WHERE M.CodMetodologia=" + id_metodologia;

		String sql_bloques = "SELECT B.CodBloque, B.NbrBloque, TB.CodTipoBloque, TB.NbrTipoBloque" + " FROM HE_Bloque B"
				+ " LEFT JOIN HE_MapeoMetodologiaBloque MMB ON B.CodBloque= MMB.CodBloque"
				+ " LEFT JOIN ME_TipoBloque TB ON B.CodTipoBloque = TB.CodTipoBloque" + " WHERE MMB.CodMetodologia = "
				+ id_metodologia + " AND TB.NbrTipoBloque = ";

		String sql_carterasExtrapol = "SELECT C.CodCartera, C.NbrCartera FROM HE_Cartera C"
				+ " JOIN HE_MetodologiaExtrapolacion ME ON C.CodCartera = ME.CodCarteraExtrapol"
				+ " WHERE ME.CodMetodologia = " + id_metodologia;

		String sql_primerosPasos = "select MPPM.CodPrimerPaso, MPPM.NbrCodigoPrimerPaso, PP.NbrPrimerPaso "
				+ "from HE_MapeoPrimerPasoMet MPPM "
				+ "left join HE_PrimerPaso PP on MPPM.CodPrimerPaso = PP.CodPrimerPaso "
				+ "where MPPM.CodMetodologia = " + id_metodologia;

		String sql_bloquesPrimerPaso = "SELECT B.CodBloque, B.NbrBloque, TB.CodTipoBloque, TB.NbrTipoBloque "
				+ " FROM HE_Bloque B" + " LEFT JOIN HE_MapeoMetodologiaBloque MMB ON B.CodBloque= MMB.CodBloque"
				+ " LEFT JOIN ME_TipoBloque TB ON B.CodTipoBloque = TB.CodTipoBloque" + " WHERE MMB.CodMetodologia = "
				+ id_metodologia + " AND TB.CodPrimerPaso = ";

		String sql_dep = "select CodCarteraVersion from HE_CarteraVersion where CodMetodologia = ";

		try {
			stmt_estructura = crearStatement(conn);
			rs_estructura = stmt_estructura.executeQuery(sql_estructura);

			while (rs_estructura.next()) {

				metodologia = new EstructuraMetodologica();

				// Seteamos si existen dependencias
				stmt_dep = crearStatement(conn);
				rs_dep = stmt_dep.executeQuery(sql_dep + rs_estructura.getInt("CodMetodologia"));

				metodologia.setConDependencias(false);
				while (rs_dep.next()) {
					metodologia.setConDependencias(true);
				}
				cerrarResultSet(rs_dep);
				cerrarStatement(stmt_dep);

				metodologia.setId_metodologia(rs_estructura.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs_estructura.getString("NbrMetodologia"));
				metodologia.setDescripcion_metodologia(rs_estructura.getString("DesMetodologia"));
				metodologia.setCodigo_estructura(rs_estructura.getString("NbrCodigoEstructura"));
				metodologia.setNombre_funcPlumbr(rs_estructura.getString("NbrFuncPlumbr"));
				metodologia.setPuerto_plumbr(rs_estructura.getInt("NumPuertoPlumbr"));

				cartera = new Cartera();
				cartera.setId_cartera(rs_estructura.getInt("CodCartera"));
				cartera.setNombre_cartera(rs_estructura.getString("NbrCartera"));

				entidad = new Entidad();
				entidad.setId_entidad(rs_estructura.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs_estructura.getString("NbrEntidad"));
				cartera.setEntidad(entidad);

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs_estructura.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs_estructura.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				metodologia.setCartera(cartera);

				motor = new MotorCalculo();
				motor.setId_motor(rs_estructura.getInt("CodMotor"));
				motor.setNombre_motor(rs_estructura.getString("NbrMotor"));
				metodologia.setMotor(motor);

				tipo_flujo = new TipoFlujo();
				tipo_flujo.setId_tipo_flujo(rs_estructura.getInt("CodTipoFlujo"));
				tipo_flujo.setNombre_tipo_flujo(rs_estructura.getString("NbrTipoFlujo"));
				tipo_flujo.setTipo_ambito(tipoAmbito);
				metodologia.setFlujo(tipo_flujo);

				if (tipo_flujo.getNombre_tipo_flujo().toLowerCase().equals(("Presupuesto").toLowerCase())) {

					// Bloques de tipo general para flujo de presupuesto
					stmt_bloques = crearStatement(conn);
					rs_bloques = stmt_bloques.executeQuery(sql_bloques + "'Presupuesto'");

					bloques = new ArrayList<Bloque>();
					while (rs_bloques.next()) {
						bloque = new Bloque();
						tipo_bloque = new TipoBloque();
						bloque.setId_bloque(rs_bloques.getInt("CodBloque"));
						bloque.setNombre_bloque(rs_bloques.getString("NbrBloque"));
						tipo_bloque.setId_tipo_bloque(rs_bloques.getInt("CodTipoBloque"));
						tipo_bloque.setNombre_tipo_bloque(rs_bloques.getString("NbrTipoBloque"));
						bloque.setTipo_bloque(tipo_bloque);
						bloques.add(bloque);
					}
					metodologia.setBloques_presupuesto(bloques);
					cerrarResultSet(rs_bloques);
					cerrarStatement(stmt_bloques);

				} else if (tipo_flujo.getNombre_tipo_flujo().toLowerCase().equals(("Otros").toLowerCase())) {

					// Bloques de tipo general para flujo 'otros'
					stmt_bloques = crearStatement(conn);
					rs_bloques = stmt_bloques.executeQuery(sql_bloques + "'Otros'");

					bloques = new ArrayList<Bloque>();
					while (rs_bloques.next()) {
						bloque = new Bloque();
						tipo_bloque = new TipoBloque();
						bloque.setId_bloque(rs_bloques.getInt("CodBloque"));
						bloque.setNombre_bloque(rs_bloques.getString("NbrBloque"));
						tipo_bloque.setId_tipo_bloque(rs_bloques.getInt("CodTipoBloque"));
						tipo_bloque.setNombre_tipo_bloque(rs_bloques.getString("NbrTipoBloque"));
						bloque.setTipo_bloque(tipo_bloque);
						bloques.add(bloque);
					}
					metodologia.setBloques_otros(bloques);
					cerrarResultSet(rs_bloques);
					cerrarStatement(stmt_bloques);

				} else if (tipo_flujo.getNombre_tipo_flujo().toLowerCase().equals(("Calculo PE").toLowerCase())) {
					metodo_resultados = new TipoObtencionResultados();
					metodo_resultados.setId_tipo_obtencion_resultados(rs_estructura.getInt("CodTipoObtenResult"));
					metodo_resultados
							.setNombre_tipo_obtencion_resultados(rs_estructura.getString("NbrTipoObtenResult"));
					metodologia.setMetodo_resultados(metodo_resultados);

					if (metodo_resultados.getNombre_tipo_obtencion_resultados().toLowerCase()
							.equals(("Extrapolacion").toLowerCase())) {

						// Bloques de tipo general para flujo de calculo PE y metodo extrapolacion
						stmt_bloques = crearStatement(conn);
						rs_bloques = stmt_bloques.executeQuery(sql_bloques + "'Extrapolacion'");

						bloques = new ArrayList<Bloque>();
						while (rs_bloques.next()) {
							bloque = new Bloque();
							tipo_bloque = new TipoBloque();
							bloque.setId_bloque(rs_bloques.getInt("CodBloque"));
							bloque.setNombre_bloque(rs_bloques.getString("NbrBloque"));
							tipo_bloque.setId_tipo_bloque(rs_bloques.getInt("CodTipoBloque"));
							tipo_bloque.setNombre_tipo_bloque(rs_bloques.getString("NbrTipoBloque"));
							bloque.setTipo_bloque(tipo_bloque);
							bloques.add(bloque);
						}
						metodologia.setBloques_extrapolacion(bloques);
						cerrarResultSet(rs_bloques);
						cerrarStatement(stmt_bloques);

						// Carteras de extrapolacion para flujo de calculo PE y metodo extrapolacion
						stmt_carterasExtrapol = crearStatement(conn);
						rs_carterasExtrapol = stmt_carterasExtrapol.executeQuery(sql_carterasExtrapol);

						carteras_extrapol = new ArrayList<Cartera>();
						while (rs_carterasExtrapol.next()) {
							cartera = new Cartera();
							cartera.setId_cartera(rs_carterasExtrapol.getInt("CodCartera"));
							cartera.setNombre_cartera(rs_carterasExtrapol.getString("NbrCartera"));
							carteras_extrapol.add(cartera);
						}
						metodologia.setCarteras_extrapolacion(carteras_extrapol);
						cerrarResultSet(rs_carterasExtrapol);
						cerrarStatement(stmt_carterasExtrapol);

					} else if (metodo_resultados.getNombre_tipo_obtencion_resultados().toLowerCase()
							.equals(("Resultado directo").toLowerCase())) {

						// Bloques de tipo resultados para flujo de calculo PE y metodo de resultados
						// directos
						stmt_bloques = crearStatement(conn);
						rs_bloques = stmt_bloques.executeQuery(sql_bloques + "'Resultado directo'");

						bloques = new ArrayList<Bloque>();
						while (rs_bloques.next()) {
							bloque = new Bloque();
							tipo_bloque = new TipoBloque();
							bloque.setId_bloque(rs_bloques.getInt("CodBloque"));
							bloque.setNombre_bloque(rs_bloques.getString("NbrBloque"));
							tipo_bloque.setId_tipo_bloque(rs_bloques.getInt("CodTipoBloque"));
							tipo_bloque.setNombre_tipo_bloque(rs_bloques.getString("NbrTipoBloque"));
							bloque.setTipo_bloque(tipo_bloque);
							bloques.add(bloque);
						}
						metodologia.setBloques_resultDirecto(bloques);
						cerrarResultSet(rs_bloques);
						cerrarStatement(stmt_bloques);

					} else if (metodo_resultados.getNombre_tipo_obtencion_resultados().toLowerCase()
							.equals(("Modelo interno").toLowerCase())) {
						metodologia.setNro_buckets(rs_estructura.getInt("NumBuckets"));
						metodologia.setLife_time(rs_estructura.getInt("NumLifeTime"));
						metodologia.setNro_maxPlazo(rs_estructura.getInt("NumMaxPlazo"));

						tipo_periodicidad = new TipoPeriodicidad();
						tipo_periodicidad.setId_tipo_periodicidad(rs_estructura.getInt("CodTipoPeriodicidad"));
						tipo_periodicidad.setNombre_tipo_periodicidad(rs_estructura.getString("NbrTipoPeriodicidad"));
						metodologia.setPeriodicidad(tipo_periodicidad);

						metodologia_PD = new TipoMetodologia();
						metodologia_PD.setId_tipo_metodologia(rs_estructura.getInt("CodTipoMetodologiaPD"));
						metodologia_PD.setNombre_tipo_metodologia(rs_estructura.getString("Metodologia_PD"));
						metodologia.setMetodologia_PD(metodologia_PD);

						metodologia_EAD = new TipoMetodologia();
						metodologia_EAD.setId_tipo_metodologia(rs_estructura.getInt("CodTipoMetodologiaEAD"));
						metodologia_EAD.setNombre_tipo_metodologia(rs_estructura.getString("Metodologia_EAD"));
						metodologia.setMetodologia_EAD(metodologia_EAD);

						metodologia.setFlag_lgdUnica(rs_estructura.getBoolean("FlgLgdUnica"));

						if (metodologia.getFlag_lgdUnica()) {
							metodologia_LGD = new TipoMetodologia();
							metodologia_LGD.setId_tipo_metodologia(rs_estructura.getInt("CodTipoMetodologiaLGD"));
							metodologia_LGD.setNombre_tipo_metodologia(rs_estructura.getString("Metodologia_LGD"));
							metodologia.setMetodologia_LGD(metodologia_LGD);
						} else {
							metodologia_LGD_WO = new TipoMetodologia();
							metodologia_LGD_WO.setId_tipo_metodologia(rs_estructura.getInt("CodTipoMetodologiaLGDWO"));
							metodologia_LGD_WO
									.setNombre_tipo_metodologia(rs_estructura.getString("Metodologia_LGD_WO"));
							metodologia.setMetodologia_LGD_WO(metodologia_LGD_WO);

							metodologia_LGD_BE = new TipoMetodologia();
							metodologia_LGD_BE.setId_tipo_metodologia(rs_estructura.getInt("CodTipoMetodologiaLGDBE"));
							metodologia_LGD_BE
									.setNombre_tipo_metodologia(rs_estructura.getString("Metodologia_LGD_BE"));
							metodologia.setMetodologia_LGD_BE(metodologia_LGD_BE);
						}

						// Bloques de tipo general para flujo de calculo PE y metodo de modelo interno
						stmt_bloques = crearStatement(conn);
						rs_bloques = stmt_bloques.executeQuery(sql_bloques + "'General'"); // Bloques de tipo general
																							// (fase 1 + fase 2)

						bloques = new ArrayList<Bloque>();
						while (rs_bloques.next()) {
							bloque = new Bloque();
							tipo_bloque = new TipoBloque();
							bloque.setId_bloque(rs_bloques.getInt("CodBloque"));
							bloque.setNombre_bloque(rs_bloques.getString("NbrBloque"));
							tipo_bloque.setId_tipo_bloque(rs_bloques.getInt("CodTipoBloque"));
							tipo_bloque.setNombre_tipo_bloque(rs_bloques.getString("NbrTipoBloque"));
							bloque.setTipo_bloque(tipo_bloque);
							bloques.add(bloque);
						}
						cerrarResultSet(rs_bloques);
						cerrarStatement(stmt_bloques);

						// Adicionamos bloques de tipo general extendido
						stmt_bloques1_1 = crearStatement(conn);
						rs_bloques1_1 = stmt_bloques1_1.executeQuery(sql_bloques + "'General extendido'"); // Bloques de
																											// tipo
																											// general
																											// extendido

						while (rs_bloques1_1.next()) {
							bloque = new Bloque();
							tipo_bloque = new TipoBloque();
							bloque.setId_bloque(rs_bloques1_1.getInt("CodBloque"));
							bloque.setNombre_bloque(rs_bloques1_1.getString("NbrBloque"));
							tipo_bloque.setId_tipo_bloque(rs_bloques1_1.getInt("CodTipoBloque"));
							tipo_bloque.setNombre_tipo_bloque(rs_bloques1_1.getString("NbrTipoBloque"));
							bloque.setTipo_bloque(tipo_bloque);
							bloques.add(bloque);
						}
						cerrarResultSet(rs_bloques1_1);
						cerrarStatement(stmt_bloques1_1);

						// Adicionamos bloques de tipo variacion
						stmt_bloques1_2 = crearStatement(conn);
						rs_bloques1_2 = stmt_bloques1_2.executeQuery(sql_bloques + "'Variacion'"); // Bloques de
																									// tipo variacion

						while (rs_bloques1_2.next()) {
							bloque = new Bloque();
							tipo_bloque = new TipoBloque();
							bloque.setId_bloque(rs_bloques1_2.getInt("CodBloque"));
							bloque.setNombre_bloque(rs_bloques1_2.getString("NbrBloque"));
							tipo_bloque.setId_tipo_bloque(rs_bloques1_2.getInt("CodTipoBloque"));
							tipo_bloque.setNombre_tipo_bloque(rs_bloques1_2.getString("NbrTipoBloque"));
							bloque.setTipo_bloque(tipo_bloque);
							bloques.add(bloque);
						}

						metodologia.setBloques_general(bloques);
						cerrarResultSet(rs_bloques1_2);
						cerrarStatement(stmt_bloques1_2);

						// Bloques de tipo escenario para flujo de calculo PE y metodo de modelo interno
						stmt_bloques2 = crearStatement(conn);
						rs_bloques2 = stmt_bloques2.executeQuery(sql_bloques + "'Escenario'");

						bloques = new ArrayList<Bloque>();
						while (rs_bloques2.next()) {
							bloque = new Bloque();
							tipo_bloque = new TipoBloque();
							bloque.setId_bloque(rs_bloques2.getInt("CodBloque"));
							bloque.setNombre_bloque(rs_bloques2.getString("NbrBloque"));
							tipo_bloque.setId_tipo_bloque(rs_bloques2.getInt("CodTipoBloque"));
							tipo_bloque.setNombre_tipo_bloque(rs_bloques2.getString("NbrTipoBloque"));
							bloque.setTipo_bloque(tipo_bloque);
							bloques.add(bloque);
						}
						metodologia.setBloques_escenario(bloques);
						cerrarResultSet(rs_bloques2);
						cerrarStatement(stmt_bloques2);

						// Bloque principal
						stmt_bloquePrincipal = crearStatement(conn);
						rs_bloquePrincipal = stmt_bloquePrincipal.executeQuery(sql_bloques + "'Principal'");

						bloque = new Bloque();
						tipo_bloque = new TipoBloque();
						while (rs_bloquePrincipal.next()) {
							bloque.setId_bloque(rs_bloquePrincipal.getInt("CodBloque"));
							bloque.setNombre_bloque(rs_bloquePrincipal.getString("NbrBloque"));
							tipo_bloque.setId_tipo_bloque(rs_bloquePrincipal.getInt("CodTipoBloque"));
							tipo_bloque.setNombre_tipo_bloque(rs_bloquePrincipal.getString("NbrTipoBloque"));
							bloque.setTipo_bloque(tipo_bloque);

							metodologia.setBloque_principal(bloque);
						}
						cerrarResultSet(rs_bloquePrincipal);
						cerrarStatement(stmt_bloquePrincipal);

						// Primeros pasos asociados a la estructura (flujo de calculo PE y metodo de
						// modelo interno)
						stmt_primerosPasos = crearStatement(conn);
						rs_primerosPasos = stmt_primerosPasos.executeQuery(sql_primerosPasos);

						grupo_primerosPasos = new ArrayList<PrimerPasoConfigurado>();
						while (rs_primerosPasos.next()) {
							primerPaso = new PrimerPasoConfigurado();

							primerPaso.setId_primerPaso(rs_primerosPasos.getInt("CodPrimerPaso"));
							primerPaso.setNombre_primerPaso(rs_primerosPasos.getString("NbrPrimerPaso"));
							primerPaso.setCodigo_primerPaso(rs_primerosPasos.getString("NbrCodigoPrimerPaso"));

							// Bloques de cada primer paso para flujo de calculo PE y metodo de modelo
							// interno
							stmt_bloquesPrimerPaso = crearStatement(conn);
							rs_bloquesPrimerPaso = stmt_bloquesPrimerPaso
									.executeQuery(sql_bloquesPrimerPaso + primerPaso.getId_primerPaso());
							// Id de primer paso especifico

							bloques = new ArrayList<Bloque>();
							while (rs_bloquesPrimerPaso.next()) {
								bloque = new Bloque();
								tipo_bloque = new TipoBloque();
								bloque.setId_bloque(rs_bloquesPrimerPaso.getInt("CodBloque"));
								bloque.setNombre_bloque(rs_bloquesPrimerPaso.getString("NbrBloque"));
								tipo_bloque.setId_tipo_bloque(rs_bloquesPrimerPaso.getInt("CodTipoBloque"));
								tipo_bloque.setNombre_tipo_bloque(rs_bloquesPrimerPaso.getString("NbrTipoBloque"));
								bloque.setTipo_bloque(tipo_bloque);
								bloques.add(bloque);
							}
							primerPaso.setBloques_primerPaso(bloques);

							cerrarResultSet(rs_bloquesPrimerPaso);
							cerrarStatement(stmt_bloquesPrimerPaso);

							// Adicionamos un primer paso configurado
							grupo_primerosPasos.add(primerPaso);
						}

						metodologia.setGrupo_primerosPasos(grupo_primerosPasos);
						cerrarResultSet(rs_primerosPasos);
						cerrarStatement(stmt_primerosPasos);
					}

				}
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener el metodologia con id " + id_metodologia + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_estructura);
			cerrarResultSet(rs_carterasExtrapol);
			cerrarResultSet(rs_primerosPasos);
			cerrarResultSet(rs_bloques);
			cerrarResultSet(rs_bloques1_1);
			cerrarResultSet(rs_bloques1_2);
			cerrarResultSet(rs_bloques2);
			cerrarResultSet(rs_bloquePrincipal);
			cerrarResultSet(rs_bloquesPrimerPaso);
			cerrarResultSet(rs_dep);

			cerrarStatement(stmt_estructura);
			cerrarStatement(stmt_carterasExtrapol);
			cerrarStatement(stmt_primerosPasos);
			cerrarStatement(stmt_bloques);
			cerrarStatement(stmt_bloques1_1);
			cerrarStatement(stmt_bloques1_2);
			cerrarStatement(stmt_bloques2);
			cerrarStatement(stmt_bloquePrincipal);
			cerrarStatement(stmt_bloquesPrimerPaso);
			cerrarStatement(stmt_dep);
		}
		return metodologia;
	}

	// Obtener bloques totales sin primerosPasos
	public ArrayList<Bloque> obtenerBloquesTotalesSinPrimerosPasosPorMetodologia(Integer id_metodologia,
			Connection conn) throws Exception {
		ArrayList<Bloque> bloques = new ArrayList<Bloque>();
		TipoBloque tipo_bloque = null;
		Bloque bloque = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		String sql = "select b.* from HE_MAPEOMETODOLOGIABLOQUE mmb "
				+ "left join HE_BLOQUE b on b.CODBLOQUE=mmb.CODBLOQUE "
				+ "left join ME_TIPOBLOQUE tb on b.CODTIPOBLOQUE = tb.CODTIPOBLOQUE "
				+ "where tb.CODPRIMERPASO is null and mmb.CODMETODOLOGIA=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_metodologia);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				bloque = new Bloque();
				tipo_bloque = new TipoBloque();
				bloque.setId_bloque(rs.getInt("CodBloque"));
				bloque.setNombre_bloque(rs.getString("NbrBloque"));
				tipo_bloque.setId_tipo_bloque(rs.getInt("CodTipoBloque"));
				bloque.setTipo_bloque(tipo_bloque);
				bloques.add(bloque);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los bloques sin campos de la metodologia con id " + id_metodologia + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return bloques;
	}

	public void borrarMetodologia(Integer id_metodologia) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		borrarMetodologia(id_metodologia, conn);
		cerrarConexion(conn);
	}

	private void borrarMetodologia(Integer id_metodologia, Connection conn) throws Exception {

		ResultSet rs_bloque_buckets = null;
		PreparedStatement pstmt_bloque_buckets = null;
		PreparedStatement pstmt_borrar_bloqueBuckets = null;
		PreparedStatement pstmt_borrar_metodologia = null;

		// Seleccionar el bloque de buckets
		String sql_bloque_buckets = "SELECT MMB.CodBloque from HE_MapeoMetodologiaBloque MMB"
				+ " left join HE_Bloque B on MMB.CodBloque=B.CodBloque "
				+ " left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque "
				+ " where TB.FlgBucket=1 and MMB.CodMetodologia=?";

		String sql_borrar_bloqueBuckets = "DELETE from HE_Bloque where CodBloque=?";

		String sql_borrar_metodologia = "DELETE from HE_MetodologiaCartera where CodMetodologia=?";

		try {

			// Borramos el bloque de buckets
			pstmt_bloque_buckets = conn.prepareStatement(sql_bloque_buckets);
			pstmt_bloque_buckets.setInt(1, id_metodologia);
			rs_bloque_buckets = pstmt_bloque_buckets.executeQuery();

			while (rs_bloque_buckets.next()) {
				pstmt_borrar_bloqueBuckets = conn.prepareStatement(sql_borrar_bloqueBuckets);
				pstmt_borrar_bloqueBuckets.setInt(1, rs_bloque_buckets.getInt("CodBloque"));
				pstmt_borrar_bloqueBuckets.executeUpdate();
				cerrarPreparedStatement(pstmt_borrar_bloqueBuckets);
			}

			pstmt_borrar_metodologia = conn.prepareStatement(sql_borrar_metodologia);
			pstmt_borrar_metodologia.setInt(1, id_metodologia);
			pstmt_borrar_metodologia.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar metodologia con id " + id_metodologia + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_bloque_buckets);
			cerrarPreparedStatement(pstmt_bloque_buckets);
			cerrarPreparedStatement(pstmt_borrar_bloqueBuckets);
			cerrarPreparedStatement(pstmt_borrar_metodologia);
			cerrarConexion(conn);
		}
	}

	//////////////////////

	public void crearMetodologia(EstructuraMetodologica metodologia) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearMetodologia(metodologia, conn);
		cerrarConexion(conn);
	}

	private Integer crearMetodologia(EstructuraMetodologica metodologia, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		PreparedStatement pstmt_primPasos = null;
		PreparedStatement pstmt_mapeo_tabla = null;
		PreparedStatement pstmt_registro_campo = null;
		PreparedStatement pstmt_bloq = null;
		PreparedStatement pstmt_cart = null;
		PreparedStatement pstmt_bloq_bucket_modMacro = null;
		PreparedStatement pstmt_mapeo_bucket_modMacro = null;
		// PreparedStatement pstmt_camp_bucket_modMacro = null;
		ArrayList<PreparedStatement> list_pstmt_camp_bucket_modMacro = new ArrayList<PreparedStatement>();
		Integer id_metodologia = null;
		Integer id_bloque_bucket_modMacro = null;
		Integer id_campo_bucket_modMacro = null;
		Integer id_mapeo_tabla = null;

		String sql = "INSERT INTO HE_METODOLOGIACARTERA (CodMetodologia, NbrMetodologia, CodCartera, DesMetodologia,"
				+ " CodMotor, NbrFuncPlumbr, NumPuertoPlumbr, NbrCodigoEstructura, CodTipoFlujo, CodTipoObtenResult, "
				+ " NumBuckets, NumLifeTime, NumMaxPlazo, CodTipoPeriodicidad,"
				+ " CodTipoMetodologiaPD, CodTipoMetodologiaEAD, FlgLgdUnica, CodTipoMetodologiaLGD, CodTipoMetodologiaLGDWO,"
				+ " CodTipoMetodologiaLGDBE, FecActualizacionTabla) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		String sql_carteras = "INSERT INTO HE_METODOLOGIAEXTRAPOLACION (CodMetodologia, CodCarteraExtrapol,"
				+ " FecActualizacionTabla) VALUES (?,?,?)";

		String sql_primerosPasos = "INSERT INTO HE_MAPEOPRIMERPASOMET (CodMetodologia, CodPrimerPaso, NbrCodigoPrimerPaso,"
				+ " FecActualizacionTabla) VALUES (?,?,?,?)";

		String sql_mapeo_tabla = "insert into HE_MapeoTablaBloque(CodMapeoTabla, CodCarteraVersion, CodEscenarioVersion,"
				+ " FlgPrimerPaso, CodMetodologia, CodBloque, CodTablaInput, DesFiltroTabla, FecActualizacionTabla) values (?,?,?,?,?,?,?,?,?)";

		String sql_registro_campo = "insert into HE_RegistroCampoBloque(CodMapeoTabla, CodCampoBloque, CodCampo, TxtValor, "
				+ "NumValor, FecActualizacionTabla)" + " values (?,?,?,?,?,?)";

		String sql_bloques = "INSERT INTO HE_MAPEOMETODOLOGIABLOQUE (CodMetodologia, CodBloque,"
				+ " FecActualizacionTabla) VALUES (?,?,?)";

		String sql_bloque_bucket_modMacro = "INSERT INTO HE_BLOQUE (CodBloque, NbrBloque, NbrVariableMotor, FlgTablaInput,"
				+ " CodFormatoInput, CodTipoBloque, FecActualizacionTabla) VALUES (?,?,?,?,?,?,?)";

		String sql_campo_bucket_modMacro = "INSERT INTO HE_CAMPOBLOQUE (CodCampoBloque, CodBloque, NbrCampo, NbrVariableMotor,"
				+ " CodTipoCampo, FecActualizacionTabla) VALUES  (?, ?, ?, ?, ?, ?)";

		// String sql_mapeo_bucket = "INSERT INTO MAPEO_METODOLOGIA_BLOQUE
		// (id_metodologia, id_bloque) VALUES (?, ?)";

		try {
			pstmt = conn.prepareStatement(sql);
			id_metodologia = obtenerSiguienteId("seq_he_metodologiacartera");
			pstmt.setInt(1, id_metodologia);
			pstmt.setString(2, metodologia.getNombre_metodologia());
			pstmt.setInt(3, metodologia.getCartera().getId_cartera());
			pstmt.setString(4, metodologia.getDescripcion_metodologia());
			pstmt.setInt(5, metodologia.getMotor().getId_motor());

			if (metodologia.getMotor().getNombre_motor().equalsIgnoreCase("R")) {
				pstmt.setString(6, metodologia.getNombre_funcPlumbr());
				pstmt.setInt(7, metodologia.getPuerto_plumbr());
			} else {
				pstmt.setNull(6, java.sql.Types.VARCHAR);
				pstmt.setNull(7, java.sql.Types.INTEGER);
			}

			pstmt.setString(8, metodologia.getCodigo_estructura());
			pstmt.setInt(9, metodologia.getFlujo().getId_tipo_flujo());

			if (metodologia.getFlujo().getNombre_tipo_flujo().toLowerCase().equals(("Presupuesto").toLowerCase())
					|| metodologia.getFlujo().getNombre_tipo_flujo().toLowerCase().equals(("Otros").toLowerCase())) {
				pstmt.setNull(10, java.sql.Types.INTEGER);
				pstmt.setNull(11, java.sql.Types.INTEGER);
				pstmt.setNull(12, java.sql.Types.INTEGER);
				pstmt.setNull(13, java.sql.Types.INTEGER);
				pstmt.setNull(14, java.sql.Types.INTEGER);
				pstmt.setNull(15, java.sql.Types.INTEGER);
				pstmt.setNull(16, java.sql.Types.INTEGER);
				pstmt.setNull(17, java.sql.Types.INTEGER);
				pstmt.setNull(18, java.sql.Types.INTEGER);
				pstmt.setNull(19, java.sql.Types.INTEGER);
				pstmt.setNull(20, java.sql.Types.INTEGER);
				pstmt.setDate(21, new Date(System.currentTimeMillis()));
				pstmt.executeUpdate();
				cerrarPreparedStatement(pstmt);

			} else if (metodologia.getFlujo().getNombre_tipo_flujo().toLowerCase()
					.equals(("Calculo PE").toLowerCase())) {
				pstmt.setInt(10, metodologia.getMetodo_resultados().getId_tipo_obtencion_resultados());

				if (metodologia.getMetodo_resultados().getNombre_tipo_obtencion_resultados().equals("Extrapolacion")
						|| metodologia.getMetodo_resultados().getNombre_tipo_obtencion_resultados()
								.equals("Resultado directo")) {

					pstmt.setNull(11, java.sql.Types.INTEGER);
					pstmt.setNull(12, java.sql.Types.INTEGER);
					pstmt.setNull(13, java.sql.Types.INTEGER);
					pstmt.setNull(14, java.sql.Types.INTEGER);
					pstmt.setNull(15, java.sql.Types.INTEGER);
					pstmt.setNull(16, java.sql.Types.INTEGER);
					pstmt.setNull(17, java.sql.Types.INTEGER);
					pstmt.setNull(18, java.sql.Types.INTEGER);
					pstmt.setNull(19, java.sql.Types.INTEGER);
					pstmt.setNull(20, java.sql.Types.INTEGER);
					pstmt.setDate(21, new Date(System.currentTimeMillis()));
					pstmt.executeUpdate();
					cerrarPreparedStatement(pstmt);

					if (metodologia.getMetodo_resultados().getNombre_tipo_obtencion_resultados()
							.equals("Extrapolacion")) {
						// Ingresamos carteras de extrapolacion
						for (int j = 0; j < metodologia.getCarteras_extrapolacion().size(); j++) {
							pstmt_cart = conn.prepareStatement(sql_carteras);
							pstmt_cart.setInt(1, id_metodologia);
							pstmt_cart.setInt(2, metodologia.getCarteras_extrapolacion().get(j).getId_cartera());
							pstmt_cart.setDate(3, new Date(System.currentTimeMillis()));
							pstmt_cart.executeUpdate();
							cerrarPreparedStatement(pstmt_cart);
						}
					}

				} else if (metodologia.getMetodo_resultados().getNombre_tipo_obtencion_resultados()
						.equals("Modelo interno")) {
					if (metodologia.getFlujo().getTipo_ambito().getNombre_tipo_ambito()
							.equalsIgnoreCase("Inversiones")) {
						pstmt.setNull(11, java.sql.Types.INTEGER);
						pstmt.setNull(12, java.sql.Types.INTEGER);
						pstmt.setNull(13, java.sql.Types.INTEGER);
						pstmt.setNull(14, java.sql.Types.INTEGER);
						pstmt.setNull(15, java.sql.Types.INTEGER);
						pstmt.setNull(16, java.sql.Types.INTEGER);
						pstmt.setNull(17, java.sql.Types.INTEGER);
						pstmt.setNull(18, java.sql.Types.INTEGER);
						pstmt.setNull(19, java.sql.Types.INTEGER);
						pstmt.setNull(20, java.sql.Types.INTEGER);
						pstmt.setDate(21, new Date(System.currentTimeMillis()));
						pstmt.executeUpdate();
						cerrarPreparedStatement(pstmt);
					} else if (metodologia.getFlujo().getTipo_ambito().getNombre_tipo_ambito()
							.equalsIgnoreCase("Creditos")) {

						pstmt.setInt(11, metodologia.getNro_buckets());
						pstmt.setInt(12, metodologia.getLife_time());
						pstmt.setInt(13, metodologia.getNro_maxPlazo());
						pstmt.setInt(14, metodologia.getPeriodicidad().getId_tipo_periodicidad());
						pstmt.setInt(15, metodologia.getMetodologia_PD().getId_tipo_metodologia());
						pstmt.setInt(16, metodologia.getMetodologia_EAD().getId_tipo_metodologia());
						pstmt.setBoolean(17, metodologia.getFlag_lgdUnica());

						if (metodologia.getFlag_lgdUnica()) {
							pstmt.setInt(18, metodologia.getMetodologia_LGD().getId_tipo_metodologia());
							pstmt.setNull(19, java.sql.Types.INTEGER);
							pstmt.setNull(20, java.sql.Types.INTEGER);
						} else {
							pstmt.setNull(18, java.sql.Types.INTEGER);
							pstmt.setInt(19, metodologia.getMetodologia_LGD_WO().getId_tipo_metodologia());
							pstmt.setInt(20, metodologia.getMetodologia_LGD_BE().getId_tipo_metodologia());
						}

						pstmt.setDate(21, new Date(System.currentTimeMillis()));
						pstmt.executeUpdate();
						cerrarPreparedStatement(pstmt);

						// Ingresamos bloques de buckets
						// Creamos el bloque
						id_bloque_bucket_modMacro = obtenerSiguienteId("SEQ_HE_Bloque");
						pstmt_bloq_bucket_modMacro = conn.prepareStatement(sql_bloque_bucket_modMacro);
						pstmt_bloq_bucket_modMacro.setInt(1, id_bloque_bucket_modMacro);
						pstmt_bloq_bucket_modMacro.setString(2,
								"Buckets - Metodologia " + metodologia.getNombre_metodologia());
						pstmt_bloq_bucket_modMacro.setString(3, "-");
						pstmt_bloq_bucket_modMacro.setBoolean(4, false);
						pstmt_bloq_bucket_modMacro.setNull(5, java.sql.Types.INTEGER);
						pstmt_bloq_bucket_modMacro.setInt(6, 8); // Tipo bloque: Bucket
						pstmt_bloq_bucket_modMacro.setDate(7, new Date(System.currentTimeMillis()));
						pstmt_bloq_bucket_modMacro.executeUpdate();
						cerrarPreparedStatement(pstmt_bloq_bucket_modMacro);

						// Llenamos sus campos
						for (int i = 0; i < metodologia.getNro_buckets(); i++) {
							id_campo_bucket_modMacro = obtenerSiguienteId("SEQ_HE_CampoBloque");
							list_pstmt_camp_bucket_modMacro.add(conn.prepareStatement(sql_campo_bucket_modMacro));
							list_pstmt_camp_bucket_modMacro.get(2 * i).setInt(1, id_campo_bucket_modMacro);
							list_pstmt_camp_bucket_modMacro.get(2 * i).setInt(2, id_bloque_bucket_modMacro);
							list_pstmt_camp_bucket_modMacro.get(2 * i).setString(3,
									"Bucket " + (i + 1) + " - Lim. INF");
							list_pstmt_camp_bucket_modMacro.get(2 * i).setString(4, "-");
							list_pstmt_camp_bucket_modMacro.get(2 * i).setInt(5, 1);
							list_pstmt_camp_bucket_modMacro.get(2 * i).setDate(6, new Date(System.currentTimeMillis()));
							list_pstmt_camp_bucket_modMacro.get(2 * i).executeUpdate();
							cerrarPreparedStatement(list_pstmt_camp_bucket_modMacro.get(2 * i));

							id_campo_bucket_modMacro = obtenerSiguienteId("SEQ_HE_CampoBloque");
							list_pstmt_camp_bucket_modMacro.add(conn.prepareStatement(sql_campo_bucket_modMacro));
							list_pstmt_camp_bucket_modMacro.get(2 * i + 1).setInt(1, id_campo_bucket_modMacro);
							list_pstmt_camp_bucket_modMacro.get(2 * i + 1).setInt(2, id_bloque_bucket_modMacro);
							list_pstmt_camp_bucket_modMacro.get(2 * i + 1).setString(3,
									"Bucket " + (i + 1) + " - Lim. SUP");
							list_pstmt_camp_bucket_modMacro.get(2 * i + 1).setString(4, "-");
							list_pstmt_camp_bucket_modMacro.get(2 * i + 1).setInt(5, 1);
							list_pstmt_camp_bucket_modMacro.get(2 * i + 1).setDate(6,
									new Date(System.currentTimeMillis()));
							list_pstmt_camp_bucket_modMacro.get(2 * i + 1).executeUpdate();
							cerrarPreparedStatement(list_pstmt_camp_bucket_modMacro.get(2 * i + 1));
						}

						// Insertamos el bloque al mapeo
						pstmt_mapeo_bucket_modMacro = conn.prepareStatement(sql_bloques);
						pstmt_mapeo_bucket_modMacro.setInt(1, id_metodologia);
						pstmt_mapeo_bucket_modMacro.setInt(2, id_bloque_bucket_modMacro);
						pstmt_mapeo_bucket_modMacro.setDate(3, new Date(System.currentTimeMillis()));
						pstmt_mapeo_bucket_modMacro.executeUpdate();
						cerrarPreparedStatement(pstmt_mapeo_bucket_modMacro);

						// Ingresamos los codigos de cada primer paso asociado
						for (int i = 0; i < metodologia.getGrupo_primerosPasos().size(); i++) {
							pstmt_primPasos = conn.prepareStatement(sql_primerosPasos);
							pstmt_primPasos.setInt(1, id_metodologia);
							pstmt_primPasos.setInt(2, metodologia.getGrupo_primerosPasos().get(i).getId_primerPaso());
							pstmt_primPasos.setString(3,
									metodologia.getGrupo_primerosPasos().get(i).getCodigo_primerPaso());
							pstmt_primPasos.setDate(4, new Date(System.currentTimeMillis()));
							pstmt_primPasos.executeUpdate();
							cerrarPreparedStatement(pstmt_primPasos);
						}
					}
				}
			}

			// Ingresamos todos los bloques (incluido los de primeros pasos y principal)
			for (int i = 0; i < metodologia.obtenerBloquesTotales().size(); i++) {
				pstmt_bloq = conn.prepareStatement(sql_bloques);
				pstmt_bloq.setInt(1, id_metodologia);
				pstmt_bloq.setInt(2, metodologia.obtenerBloquesTotales().get(i).getId_bloque());
				pstmt_bloq.setDate(3, new Date(System.currentTimeMillis()));
				pstmt_bloq.executeUpdate();
				cerrarPreparedStatement(pstmt_bloq);
			}

			// Creamos registros iniciales en mapeo de tabla (unicamente de primeros pasos)
			if (metodologia.getMetodo_resultados() != null
					&& metodologia.getMetodo_resultados().getNombre_tipo_obtencion_resultados().equals("Modelo interno")
					&& metodologia.getFlujo().getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Creditos")) {
				pstmt_mapeo_tabla = conn.prepareStatement(sql_mapeo_tabla);
				pstmt_registro_campo = conn.prepareStatement(sql_registro_campo);

				for (Bloque bloque : metodologia.obtenerBloquesPrimerosPasos()) {
					// Creamos el mapeo-bloque
					id_mapeo_tabla = obtenerSiguienteId("seq_he_mapeotablabloque");
					pstmt_mapeo_tabla.setInt(1, id_mapeo_tabla);
					pstmt_mapeo_tabla.setNull(2, java.sql.Types.INTEGER); // Cartera version en null
					pstmt_mapeo_tabla.setNull(3, java.sql.Types.INTEGER); // Cartera version en null
					pstmt_mapeo_tabla.setInt(4, 1); // Flag primer paso activado
					pstmt_mapeo_tabla.setInt(5, id_metodologia);
					pstmt_mapeo_tabla.setInt(6, bloque.getId_bloque());
					pstmt_mapeo_tabla.setNull(7, java.sql.Types.INTEGER); // CodTabla input en null
					pstmt_mapeo_tabla.setNull(8, java.sql.Types.VARCHAR); // filtro tabla input en null
					pstmt_mapeo_tabla.setDate(9, new Date(System.currentTimeMillis()));
					pstmt_mapeo_tabla.addBatch();

					// Obtenemos campos del bloque
					bloque.setCampos_bloque(this.obtenerCamposBloquePorIdBloque(bloque.getId_bloque()));

					// Creamos el registro-campo
					for (CampoBloque campo : bloque.getCampos_bloque()) {
						pstmt_registro_campo.setInt(1, id_mapeo_tabla);
						pstmt_registro_campo.setInt(2, campo.getId_campo());
						pstmt_registro_campo.setNull(3, java.sql.Types.INTEGER); // Cod tabla input en null
						pstmt_registro_campo.setNull(4, java.sql.Types.VARCHAR); // Txtvalor en null
						pstmt_registro_campo.setNull(5, java.sql.Types.INTEGER); // Numvalor en null
						pstmt_registro_campo.setDate(6, new Date(System.currentTimeMillis()));
						pstmt_registro_campo.addBatch();
					}

				}
				pstmt_mapeo_tabla.executeBatch();
				cerrarPreparedStatement(pstmt_mapeo_tabla);
				pstmt_registro_campo.executeBatch();
				cerrarPreparedStatement(pstmt_registro_campo);
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar la estructura metodologica:" + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarPreparedStatement(pstmt_cart);
			cerrarPreparedStatement(pstmt_primPasos);
			cerrarPreparedStatement(pstmt_bloq);
			cerrarPreparedStatement(pstmt_bloq_bucket_modMacro);
			cerrarPreparedStatement(pstmt_mapeo_bucket_modMacro);
			cerrarPreparedStatement(pstmt_mapeo_tabla);
			cerrarPreparedStatement(pstmt_registro_campo);

			for (int i = 0; i < list_pstmt_camp_bucket_modMacro.size(); i++) {
				cerrarPreparedStatement(list_pstmt_camp_bucket_modMacro.get(i));
			}
		}
		return id_metodologia; // Adicional para replica
	}

	public void editarMetodologia(EstructuraMetodologica metodologia) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		editarMetodologia(metodologia, conn);
		cerrarConexion(conn);
	}

	public void editarMetodologia(EstructuraMetodologica metodologia, Connection conn) throws Exception {
		Statement stmt_select_bloques = null;
		Statement stmt_nro_buckets = null;
		Statement stmt_bloque_buckets = null;
		Statement stmt_campos_buckets_modMacro = null;
		Statement stmt_met_extrapolacion = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt_primPasos = null;
		PreparedStatement pstmt_delete_mapeo_tabla = null;
		PreparedStatement pstmt_mapeo_tabla = null;
		PreparedStatement pstmt_delete_registro_campo = null;
		PreparedStatement pstmt_registro_campo = null;
		PreparedStatement pstmt_delete_met_extrapol = null;
		PreparedStatement pstmt_insert_met_extrapol = null;

		String sql_update_met = "UPDATE HE_MetodologiaCartera SET NbrMetodologia= ?, CodCartera= ?, DesMetodologia=?, "
				+ "CodMotor=?, NbrFuncPlumbr=?, NumPuertoPlumbr=?, NbrCodigoEstructura=?, CodTipoFlujo=?, CodTipoObtenResult=?, "
				+ "NumBuckets=?, NumLifeTime=?, NumMaxPlazo=?, "
				+ "CodTipoPeriodicidad=?, CodTipoMetodologiaPD=?, CodTipoMetodologiaEAD=?, FlgLgdUnica=?, CodTipoMetodologiaLGD=?, "
				+ "CodTipoMetodologiaLGDWO=?, CodTipoMetodologiaLGDBE=?, FecActualizacionTabla=? where CodMetodologia = ?";

		String sql_update_primPasos = "Update HE_MapeoPrimerPasoMet set NbrCodigoPrimerPaso=?, FecActualizacionTabla=? "
				+ "where CodMetodologia=? and CodPrimerPaso=?";

		String sql_mapeo_tabla = "insert into HE_MapeoTablaBloque(CodMapeoTabla, CodCarteraVersion, CodEscenarioVersion,"
				+ " FlgPrimerPaso, CodMetodologia, CodBloque, CodTablaInput, DesFiltroTabla, FecActualizacionTabla) values (?,?,?,?,?,?,?,?,?)";

		String sql_delete_mapeo_tabla = "delete from HE_MapeoTablaBloque where FlgPrimerPaso = 1 and CodMetodologia = ?";

		String sql_registro_campo = "insert into HE_RegistroCampoBloque(CodMapeoTabla, CodCampoBloque, CodCampo, TxtValor, "
				+ "NumValor, FecActualizacionTabla)" + " values (?,?,?,?,?,?)";

		String sql_delete_registro_campo = "delete from HE_RegistroCampoBloque where CodMapeoTabla in ("
				+ "select CodMapeoTabla from HE_MapeoTablaBloque where FlgPrimerPaso = 1 and CodMetodologia = ?)";

		String sql_select_bloques = "select MMB.CodBloque from HE_MapeoMetodologiaBloque MMB "
				+ " left join HE_Bloque B on MMB.CodBloque = B.CodBloque "
				+ " left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque "
				+ " where TB.FlgBucket <> 1 and MMB.CodMetodologia= "; // Excepto bloques de buckets

		String sql_nro_buckets = "SELECT NumBuckets from HE_MetodologiaCartera where CodMetodologia= ";

		String sql_bloque_buckets = "SELECT MMB.CodBloque from HE_MapeoMetodologiaBloque MMB "
				+ " left join HE_Bloque B on MMB.CodBloque = B.CodBloque "
				+ " left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque "
				+ " where TB.FlgBucket = 1 and MMB.CodMetodologia = ";

		String sql_campos_buckets_modMacro = "SELECT CodCampoBloque from HE_CampoBloque where CodBloque= ";

		String sql_met_extrapolacion = "SELECT CodCarteraExtrapol from HE_MetodologiaExtrapolacion where CodMetodologia= ";
		String sql_delete_met_extrapol = "DELETE from HE_MetodologiaExtrapolacion where CodMetodologia= ? and CodCarteraExtrapol= ?";
		String sql_insert_met_extrapol = "INSERT into HE_MetodologiaExtrapolacion (CodMetodologia,CodCarteraExtrapol,"
				+ "FecActualizacionTabla) values (?,?,?)";

		ResultSet rs = null;
		Integer cnt_bucket = null;
		Integer nro_buckets_antes = null;
		Integer nro_buckets_despues = null;
		Integer id_bloque_bucket_modMacro = null;
		Integer id_mapeo_tabla = null;
		Boolean existe = null;
		ArrayList<Boolean> bloque_nuevo = new ArrayList<Boolean>();
		ArrayList<Boolean> cartera_extrapol_nuevo = new ArrayList<Boolean>();
		ArrayList<Integer> id_campos = null;
		CampoBloque bucket_inf = null;
		CampoBloque bucket_sup = null;

		try {
			// Actualizamos los atributos
			pstmt = conn.prepareStatement(sql_update_met);
			pstmt.setString(1, metodologia.getNombre_metodologia());
			pstmt.setInt(2, metodologia.getCartera().getId_cartera());
			pstmt.setString(3, metodologia.getDescripcion_metodologia());
			pstmt.setInt(4, metodologia.getMotor().getId_motor());

			if (metodologia.getMotor().getNombre_motor().equalsIgnoreCase("R")) {
				pstmt.setString(5, metodologia.getNombre_funcPlumbr());
				pstmt.setInt(6, metodologia.getPuerto_plumbr());
			} else {
				pstmt.setNull(5, java.sql.Types.VARCHAR);
				pstmt.setNull(6, java.sql.Types.INTEGER);
			}

			pstmt.setString(7, metodologia.getCodigo_estructura());
			pstmt.setInt(8, metodologia.getFlujo().getId_tipo_flujo());

			if (metodologia.getFlujo().getNombre_tipo_flujo().toLowerCase().equals(("Presupuesto").toLowerCase())
					|| metodologia.getFlujo().getNombre_tipo_flujo().toLowerCase().equals(("Otros").toLowerCase())) {
				pstmt.setNull(9, java.sql.Types.INTEGER);
				pstmt.setNull(10, java.sql.Types.INTEGER);
				pstmt.setNull(11, java.sql.Types.INTEGER);
				pstmt.setNull(12, java.sql.Types.INTEGER);
				pstmt.setNull(13, java.sql.Types.INTEGER);
				pstmt.setNull(14, java.sql.Types.INTEGER);
				pstmt.setNull(15, java.sql.Types.INTEGER);
				pstmt.setNull(16, java.sql.Types.INTEGER);
				pstmt.setNull(17, java.sql.Types.INTEGER);
				pstmt.setNull(18, java.sql.Types.INTEGER);
				pstmt.setNull(19, java.sql.Types.INTEGER);

			} else if (metodologia.getFlujo().getNombre_tipo_flujo().toLowerCase()
					.equals(("Calculo PE").toLowerCase())) {
				pstmt.setInt(9, metodologia.getMetodo_resultados().getId_tipo_obtencion_resultados());

				if (metodologia.getMetodo_resultados().getNombre_tipo_obtencion_resultados().equals("Extrapolacion")
						|| metodologia.getMetodo_resultados().getNombre_tipo_obtencion_resultados()
								.equals("Resultado directo")) {

					pstmt.setNull(10, java.sql.Types.INTEGER);
					pstmt.setNull(11, java.sql.Types.INTEGER);
					pstmt.setNull(12, java.sql.Types.INTEGER);
					pstmt.setNull(13, java.sql.Types.INTEGER);
					pstmt.setNull(14, java.sql.Types.INTEGER);
					pstmt.setNull(15, java.sql.Types.INTEGER);
					pstmt.setNull(16, java.sql.Types.INTEGER);
					pstmt.setNull(17, java.sql.Types.INTEGER);
					pstmt.setNull(18, java.sql.Types.INTEGER);
					pstmt.setNull(19, java.sql.Types.INTEGER);

					if (metodologia.getMetodo_resultados().getNombre_tipo_obtencion_resultados()
							.equals("Extrapolacion")) {
						// Si la metodología es una extrapolación
						// Obtenemos las carteras de la extrapolacion
						stmt_met_extrapolacion = crearStatement(conn);
						sql_met_extrapolacion += metodologia.getId_metodologia();

						rs = stmt_met_extrapolacion.executeQuery(sql_met_extrapolacion);

						for (int i = 0; i < metodologia.getCarteras_extrapolacion().size(); i++) {
							cartera_extrapol_nuevo.add(true);
						}

						while (rs.next()) {
							existe = false;
							for (int i = 0; i < metodologia.getCarteras_extrapolacion().size(); i++) {
								if (rs.getInt("CodCarteraExtrapol") == metodologia.getCarteras_extrapolacion().get(i)
										.getId_cartera()) {
									cartera_extrapol_nuevo.set(i, false);
									existe = true;
									break;
								}
							}
							if (!existe) {
								// Borrar registro
								pstmt_delete_met_extrapol = conn.prepareStatement(sql_delete_met_extrapol);
								pstmt_delete_met_extrapol.setInt(1, metodologia.getId_metodologia());
								pstmt_delete_met_extrapol.setInt(2, rs.getInt("CodCarteraExtrapol"));

								pstmt_delete_met_extrapol.executeUpdate();
							}
						}

						pstmt_insert_met_extrapol = conn.prepareStatement(sql_insert_met_extrapol);
						for (int i = 0; i < metodologia.getCarteras_extrapolacion().size(); i++) {
							if (cartera_extrapol_nuevo.get(i)) {
								// Insertar registro
								pstmt_insert_met_extrapol.setInt(1, metodologia.getId_metodologia());
								pstmt_insert_met_extrapol.setInt(2,
										metodologia.getCarteras_extrapolacion().get(i).getId_cartera());
								pstmt_insert_met_extrapol.setDate(3, new Date(System.currentTimeMillis()));
								pstmt_insert_met_extrapol.addBatch();
							}
						}
						pstmt_insert_met_extrapol.executeBatch();
					}

				} else if (metodologia.getMetodo_resultados().getNombre_tipo_obtencion_resultados()
						.equals("Modelo interno")) { // Si la metodologia es un modelo interno

					if (metodologia.getFlujo().getTipo_ambito().getNombre_tipo_ambito()
							.equalsIgnoreCase("Inversiones")) {
						pstmt.setNull(10, java.sql.Types.INTEGER);
						pstmt.setNull(11, java.sql.Types.INTEGER);
						pstmt.setNull(12, java.sql.Types.INTEGER);
						pstmt.setNull(13, java.sql.Types.INTEGER);
						pstmt.setNull(14, java.sql.Types.INTEGER);
						pstmt.setNull(15, java.sql.Types.INTEGER);
						pstmt.setNull(16, java.sql.Types.INTEGER);
						pstmt.setNull(17, java.sql.Types.INTEGER);
						pstmt.setNull(18, java.sql.Types.INTEGER);
						pstmt.setNull(19, java.sql.Types.INTEGER);

					} else if (metodologia.getFlujo().getTipo_ambito().getNombre_tipo_ambito()
							.equalsIgnoreCase("Creditos")) {

						// Obtenemos el nro_bucktes
						stmt_nro_buckets = crearStatement(conn);
						sql_nro_buckets += metodologia.getId_metodologia();

						// LOGGER.info("Ejecutara SQL: " + sql_nro_buckets);
						rs = stmt_nro_buckets.executeQuery(sql_nro_buckets);
						while (rs.next()) {
							nro_buckets_antes = rs.getInt("NumBuckets");
						}
						cerrarResultSet(rs);
						cerrarStatement(stmt_nro_buckets);

						// Obtenemos el bloque de buckets
						stmt_bloque_buckets = crearStatement(conn);
						sql_bloque_buckets += metodologia.getId_metodologia();

						rs = stmt_bloque_buckets.executeQuery(sql_bloque_buckets);
						while (rs.next()) {
							id_bloque_bucket_modMacro = rs.getInt("CodBloque");
						}
						cerrarResultSet(rs);
						cerrarStatement(stmt_nro_buckets);

						// Obtenemos los id's de los campos de los buckets
						stmt_campos_buckets_modMacro = crearStatement(conn);
						sql_campos_buckets_modMacro = sql_campos_buckets_modMacro + id_bloque_bucket_modMacro
								+ " order by CodCampoBloque";

						rs = stmt_campos_buckets_modMacro.executeQuery(sql_campos_buckets_modMacro);
						id_campos = new ArrayList<Integer>();
						while (rs.next()) {
							id_campos.add(rs.getInt("CodCampoBloque"));
						}
						cerrarResultSet(rs);
						cerrarStatement(stmt_campos_buckets_modMacro);

						nro_buckets_despues = metodologia.getNro_buckets();
						if (nro_buckets_despues > nro_buckets_antes) {
							// En caso se requieran mas buckets, los creamos
							cnt_bucket = nro_buckets_antes;

							for (int i = 0; i < nro_buckets_despues - nro_buckets_antes; i++) {
								cnt_bucket++;

								bucket_inf = new CampoBloque();
								bucket_inf.setId_bloque(id_bloque_bucket_modMacro);
								bucket_inf.setNombre_campo("Bucket " + (cnt_bucket) + " - Lim. INF");
								bucket_inf.setVariable_motor("-");
								bucket_inf.setTipo_campo(dic_tipo_campo.get(0));
								crearCampoBloque(bucket_inf, conn);

								bucket_sup = new CampoBloque();
								bucket_sup.setId_bloque(id_bloque_bucket_modMacro);
								bucket_sup.setNombre_campo("Bucket " + (cnt_bucket) + " - Lim. SUP");
								bucket_sup.setVariable_motor("-");
								bucket_sup.setTipo_campo(dic_tipo_campo.get(0));
								crearCampoBloque(bucket_sup, conn);
							}

						} else if (nro_buckets_despues < nro_buckets_antes) {
							// En caso se requieran menos buckets, eliminamos el excedente
							for (int i = 0; i < 2 * (nro_buckets_antes - nro_buckets_despues); i++) {
								borrarCampoBloque(id_campos.get(id_campos.size() - i - 1), conn);
							}
						}

						pstmt.setInt(10, metodologia.getNro_buckets());
						pstmt.setInt(11, metodologia.getLife_time());

						pstmt.setInt(12, metodologia.getNro_maxPlazo());
						pstmt.setInt(13, metodologia.getPeriodicidad().getId_tipo_periodicidad());
						pstmt.setInt(14, metodologia.getMetodologia_PD().getId_tipo_metodologia());
						pstmt.setInt(15, metodologia.getMetodologia_EAD().getId_tipo_metodologia());
						pstmt.setBoolean(16, metodologia.getFlag_lgdUnica());

						if (metodologia.getFlag_lgdUnica()) {
							pstmt.setInt(17, metodologia.getMetodologia_LGD().getId_tipo_metodologia());
							pstmt.setNull(18, java.sql.Types.INTEGER);
							pstmt.setNull(19, java.sql.Types.INTEGER);
						} else {
							pstmt.setNull(17, java.sql.Types.INTEGER);
							pstmt.setInt(18, metodologia.getMetodologia_LGD_WO().getId_tipo_metodologia());
							pstmt.setInt(19, metodologia.getMetodologia_LGD_BE().getId_tipo_metodologia());
						}

						// Actualizamos los codigos de cada primer paso asociado
						for (int i = 0; i < metodologia.getGrupo_primerosPasos().size(); i++) {
							pstmt_primPasos = conn.prepareStatement(sql_update_primPasos);
							pstmt_primPasos.setString(1,
									metodologia.getGrupo_primerosPasos().get(i).getCodigo_primerPaso());
							pstmt_primPasos.setDate(2, new Date(System.currentTimeMillis()));
							pstmt_primPasos.setInt(3, metodologia.getId_metodologia());
							pstmt_primPasos.setInt(4, metodologia.getGrupo_primerosPasos().get(i).getId_primerPaso());
							pstmt_primPasos.executeUpdate();
							cerrarPreparedStatement(pstmt_primPasos);
						}

					}
				}
			}

			pstmt.setDate(20, new Date(System.currentTimeMillis()));
			pstmt.setInt(21, metodologia.getId_metodologia());

			pstmt.executeUpdate();

			// Actualizamos los bloques (todos excepto el de buckets)
			sql_select_bloques += metodologia.getId_metodologia();
			stmt_select_bloques = crearStatement(conn);

			rs = stmt_select_bloques.executeQuery(sql_select_bloques);

			for (int i = 0; i < metodologia.obtenerBloquesTotales().size(); i++) {
				bloque_nuevo.add(true);
			}

			while (rs.next()) {
				existe = false;
				for (int i = 0; i < metodologia.obtenerBloquesTotales().size(); i++) {
					if (rs.getInt("CodBloque") == metodologia.obtenerBloquesTotales().get(i).getId_bloque()) {
						bloque_nuevo.set(i, false);
						existe = true;
						break;
					}
				}
				if (!existe) {
					borrarBloqueDeMetodologia(rs.getInt("CodBloque"), metodologia.getId_metodologia(), conn);
				}
			}

			for (int i = 0; i < metodologia.obtenerBloquesTotales().size(); i++) {
				if (bloque_nuevo.get(i)) {
					agregarBloqueAMetodologia(metodologia.obtenerBloquesTotales().get(i).getId_bloque(),
							metodologia.getId_metodologia(), conn);
				}
			}

			// Solo para primeros pasos
			if (metodologia.getFlujo().getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Creditos")) {
				if ((metodologia.getFlujo().getNombre_tipo_flujo().toLowerCase().equals(("Calculo PE").toLowerCase()))
						&& (metodologia.getMetodo_resultados().getNombre_tipo_obtencion_resultados()
								.equals("Modelo interno"))) {

					// Borramos registros iniciales de mapeo de tabla y registros campo (unicamente
					// de primeros pasos)
					pstmt_delete_registro_campo = conn.prepareStatement(sql_delete_registro_campo);
					pstmt_delete_registro_campo.setInt(1, metodologia.getId_metodologia());

					// LOGGER.info("Ejecutara SQL: " + sql_delete_registro_campo);
					pstmt_delete_registro_campo.executeUpdate();
					cerrarPreparedStatement(pstmt_delete_registro_campo);

					pstmt_delete_mapeo_tabla = conn.prepareStatement(sql_delete_mapeo_tabla);
					pstmt_delete_mapeo_tabla.setInt(1, metodologia.getId_metodologia());

					// LOGGER.info("Ejecutara SQL: " + sql_delete_mapeo_tabla);
					pstmt_delete_mapeo_tabla.executeUpdate();
					cerrarPreparedStatement(pstmt_delete_mapeo_tabla);

					// Creamos registros iniciales en mapeo de tabla y registros campo (unicamente
					// de primeros pasos)
					pstmt_mapeo_tabla = conn.prepareStatement(sql_mapeo_tabla);
					pstmt_registro_campo = conn.prepareStatement(sql_registro_campo);
					for (Bloque bloque : metodologia.obtenerBloquesPrimerosPasos()) {
						// Creamos el mapeo-bloque
						id_mapeo_tabla = obtenerSiguienteId("seq_he_mapeotablabloque");
						pstmt_mapeo_tabla.setInt(1, id_mapeo_tabla);
						pstmt_mapeo_tabla.setNull(2, java.sql.Types.INTEGER); // Cartera version en null
						pstmt_mapeo_tabla.setNull(3, java.sql.Types.INTEGER); // Escenario version en null
						pstmt_mapeo_tabla.setBoolean(4, true); // Flag primer paso activado
						pstmt_mapeo_tabla.setInt(5, metodologia.getId_metodologia());
						pstmt_mapeo_tabla.setInt(6, bloque.getId_bloque());
						pstmt_mapeo_tabla.setNull(7, java.sql.Types.INTEGER); // CodTabla input en null
						pstmt_mapeo_tabla.setNull(8, java.sql.Types.VARCHAR); // filtro tabla input en null
						pstmt_mapeo_tabla.setDate(9, new Date(System.currentTimeMillis()));
						pstmt_mapeo_tabla.addBatch();

						// Obtenemos campos del bloque
						bloque.setCampos_bloque(this.obtenerCamposBloquePorIdBloque(bloque.getId_bloque()));

						// Creamos el registro-campo
						for (CampoBloque campo : bloque.getCampos_bloque()) {
							pstmt_registro_campo.setInt(1, id_mapeo_tabla);
							pstmt_registro_campo.setInt(2, campo.getId_campo());
							pstmt_registro_campo.setNull(3, java.sql.Types.INTEGER); // Cod tabla input en null
							pstmt_registro_campo.setNull(4, java.sql.Types.VARCHAR); // Txtvalor en null
							pstmt_registro_campo.setNull(5, java.sql.Types.INTEGER); // Numvalor en null
							pstmt_registro_campo.setDate(6, new Date(System.currentTimeMillis()));
							pstmt_registro_campo.addBatch();
						}

					}
					// LOGGER.info("Ejecutara SQL: " + sql_mapeo_tabla);
					pstmt_mapeo_tabla.executeBatch();
					cerrarPreparedStatement(pstmt_mapeo_tabla);

					// LOGGER.info("Ejecutara SQL: " + sql_registro_campo);
					pstmt_registro_campo.executeBatch();
					cerrarPreparedStatement(pstmt_registro_campo);
				}
			}

			// LOGGER.info("Ejecutara commit para actualizar metodologia: " +
			// metodologia.getId_metodologia());
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar la metodologia con id " + metodologia.getId_metodologia() + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt_select_bloques);
			cerrarStatement(stmt_nro_buckets);
			cerrarStatement(stmt_bloque_buckets);
			cerrarStatement(stmt_campos_buckets_modMacro);
			cerrarStatement(stmt_met_extrapolacion);
			cerrarPreparedStatement(pstmt);
			cerrarPreparedStatement(pstmt_primPasos);
			cerrarPreparedStatement(pstmt_delete_mapeo_tabla);
			cerrarPreparedStatement(pstmt_mapeo_tabla);
			cerrarPreparedStatement(pstmt_delete_registro_campo);
			cerrarPreparedStatement(pstmt_registro_campo);
			cerrarPreparedStatement(pstmt_delete_met_extrapol);
			cerrarPreparedStatement(pstmt_insert_met_extrapol);
		}
	}

	////////////////////////

	private void agregarBloqueAMetodologia(Integer id_bloque, Integer id_metodologia, Connection conn)
			throws Exception {
		PreparedStatement pstmt_tipo_bloque = null;
		PreparedStatement pstmt_campos_bloque = null;
		PreparedStatement pstmt_car_versiones = null;
		PreparedStatement pstmt_esc_versiones = null;
		PreparedStatement pstmt_mapeo_met = null;
		PreparedStatement pstmt_mapeo_tabla = null;
		PreparedStatement pstmt_registro_campo = null;
		String sql_tipo_bloque = "SELECT TB.NbrTipoBloque from HE_Bloque B "
				+ "left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque where B.CodBloque=?";
		String sql_campos_bloque = "SELECT CodCampoBloque from HE_Campobloque where CodBloque=?";
		String sql_mapeo_met = "INSERT into HE_MapeoMetodologiaBloque (CodMetodologia, CodBloque, FecActualizacionTabla)"
				+ " values (?, ?, ?)";
		String sql_car_versiones = "SELECT CodCarteraVersion from HE_CarteraVersion where CodMetodologia=?";
		String sql_esc_versiones = "SELECT CodEscenarioVersion from HE_EscenarioVersion EV "
				+ "join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
				+ "where JEV.CodCarteraVersion=?";
		String sql_mapeo_tabla = "INSERT into HE_MapeoTablaBloque (CodMapeoTabla,CodCarteraVersion,CodEscenarioVersion,"
				+ "CodBloque,CodMetodologia,FecActualizacionTabla) values (?, ?, ?, ?, ?, ?)";
		String sql_registro_campo = "INSERT into HE_RegistroCampoBloque (CodMapeoTabla, CodCampoBloque, FecActualizacionTabla)"
				+ " values (?, ?, ?)";
		ResultSet rs = null;
		ResultSet rs2 = null;
		// Integer id_tipo_bloque = null;
		String nombre_tipo_bloque = null;
		Integer id_mapeo_tabla = null;
		ArrayList<Integer> id_campos = new ArrayList<Integer>();
		try {
			// Agregamos el bloque al mapeo de la metodologia
			pstmt_mapeo_met = conn.prepareStatement(sql_mapeo_met);
			pstmt_mapeo_met.setInt(1, id_metodologia);
			pstmt_mapeo_met.setInt(2, id_bloque);
			pstmt_mapeo_met.setDate(3, new Date(System.currentTimeMillis()));
			pstmt_mapeo_met.executeUpdate();

			// Obtenemos el tipo bloque
			pstmt_tipo_bloque = conn.prepareStatement(sql_tipo_bloque);
			pstmt_tipo_bloque.setInt(1, id_bloque);
			rs = pstmt_tipo_bloque.executeQuery();
			while (rs.next()) {
				// id_tipo_bloque = rs.getInt("CodTipoBloque");
				nombre_tipo_bloque = rs.getString("NbrTipoBloque");
			}
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt_tipo_bloque);

			// Obtenemos los campos del bloque
			pstmt_campos_bloque = conn.prepareStatement(sql_campos_bloque);
			pstmt_campos_bloque.setInt(1, id_bloque);
			rs = pstmt_campos_bloque.executeQuery();
			while (rs.next()) {
				id_campos.add(rs.getInt("CodCampoBloque"));
			}
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt_campos_bloque);

			if (!nombre_tipo_bloque.equalsIgnoreCase("Escenario")) {
				// Si no es un bloque del tipo escenario

				// Obtenemos las carteras_version
				pstmt_car_versiones = conn.prepareStatement(sql_car_versiones);
				pstmt_car_versiones.setInt(1, id_metodologia);
				rs = pstmt_car_versiones.executeQuery();

				// Agregamos el mapeo_tabla y el registro_campo
				pstmt_mapeo_tabla = conn.prepareStatement(sql_mapeo_tabla);
				pstmt_registro_campo = conn.prepareStatement(sql_registro_campo);
				while (rs.next()) {
					id_mapeo_tabla = obtenerSiguienteId("seq_he_mapeotablabloque");
					pstmt_mapeo_tabla.setInt(1, id_mapeo_tabla);
					pstmt_mapeo_tabla.setInt(2, rs.getInt("CodCarteraVersion"));
					pstmt_mapeo_tabla.setNull(3, java.sql.Types.INTEGER);
					pstmt_mapeo_tabla.setInt(4, id_bloque);
					pstmt_mapeo_tabla.setInt(5, id_metodologia);
					pstmt_mapeo_tabla.setDate(6, new Date(System.currentTimeMillis()));
					pstmt_mapeo_tabla.addBatch();

					for (int i = 0; i < id_campos.size(); i++) {
						pstmt_registro_campo.setInt(1, id_mapeo_tabla);
						pstmt_registro_campo.setInt(2, id_campos.get(i));
						pstmt_registro_campo.setDate(3, new Date(System.currentTimeMillis()));
						pstmt_registro_campo.addBatch();
					}
				}

				pstmt_mapeo_tabla.executeBatch();
				pstmt_registro_campo.executeBatch();
				cerrarResultSet(rs);
				cerrarPreparedStatement(pstmt_car_versiones);
			} else {
				// Si es un bloque del tipo escenario

				// Obtenemos las carteras_version
				pstmt_car_versiones = conn.prepareStatement(sql_car_versiones);
				pstmt_car_versiones.setInt(1, id_metodologia);
				rs = pstmt_car_versiones.executeQuery();

				// Agregamos el mapeo_tabla y el registro_campo
				pstmt_mapeo_tabla = conn.prepareStatement(sql_mapeo_tabla);
				pstmt_registro_campo = conn.prepareStatement(sql_registro_campo);
				while (rs.next()) { // Recorremos versiones de carteras
					pstmt_esc_versiones = conn.prepareStatement(sql_esc_versiones);
					// TODO: revisar si es setInt
					// pstmt_esc_versiones.setInt(1, rs.getInt("CodCarteraVersion"));
					pstmt_esc_versiones.setString(1, rs.getString("CodCarteraVersion"));
					rs2 = pstmt_esc_versiones.executeQuery();
					while (rs2.next()) { // Recorremos versiones de escenarios
						id_mapeo_tabla = obtenerSiguienteId("seq_he_mapeotablabloque");
						pstmt_mapeo_tabla.setInt(1, id_mapeo_tabla);
						pstmt_mapeo_tabla.setInt(2, rs.getInt("CodCarteraVersion"));
						pstmt_mapeo_tabla.setNull(3, rs2.getInt("CodEscenarioVersion"));
						pstmt_mapeo_tabla.setInt(4, id_bloque);
						pstmt_mapeo_tabla.setInt(5, id_metodologia);
						pstmt_mapeo_tabla.setDate(6, new Date(System.currentTimeMillis()));
						pstmt_mapeo_tabla.addBatch();

						for (int i = 0; i < id_campos.size(); i++) {
							pstmt_registro_campo.setInt(1, id_mapeo_tabla);
							pstmt_registro_campo.setInt(2, id_campos.get(i));
							pstmt_registro_campo.addBatch();
						}
					}
					cerrarResultSet(rs2);
					cerrarPreparedStatement(pstmt_esc_versiones);
					// TODO: Revisar cierre parcial de prepared statement antes de volver a
					// generarse
				}
				pstmt_mapeo_tabla.executeBatch();
				pstmt_registro_campo.executeBatch();
				cerrarResultSet(rs);
			}
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar el bloque con id " + id_bloque + " a la metodologia con id " + id_metodologia
					+ " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_tipo_bloque);
			cerrarPreparedStatement(pstmt_campos_bloque);
			cerrarPreparedStatement(pstmt_car_versiones);
			cerrarPreparedStatement(pstmt_esc_versiones);
			cerrarPreparedStatement(pstmt_mapeo_met);
			cerrarPreparedStatement(pstmt_mapeo_tabla);
			cerrarPreparedStatement(pstmt_registro_campo);
			cerrarResultSet(rs);
			cerrarResultSet(rs2);
		}
	}

	///////////////////////

	private void borrarBloqueDeMetodologia(Integer id_bloque, Integer id_metodologia, Connection conn)
			throws Exception {
		PreparedStatement pstmt = null;
		ArrayList<String> list_sql = new ArrayList<String>();
		list_sql.add("DELETE from HE_MapeoTablaBloque where CodBloque= ? and CodMetodologia= ?");
		list_sql.add("DELETE from HE_MapeoMetodologiaBloque where CodBloque= ? and CodMetodologia= ?");
		try {
			for (int i = 0; i < list_sql.size(); i++) {
				pstmt = conn.prepareStatement(list_sql.get(i));
				pstmt.setInt(1, id_bloque);
				pstmt.setInt(2, id_metodologia);
				pstmt.executeUpdate();
				cerrarPreparedStatement(pstmt);
			}
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar el bloque con id: " + id_bloque + " de la metodología con id "
					+ id_metodologia + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	// -------------------------------------------------------------------- //
	// ------------------------ Version Parametria ------------------------ //
	// -------------------------------------------------------------------- //

	/////////////////////// Cartera version con info staging ///////////////////////

	public ArrayList<CarteraVersion_staging> obtenerCarterasVersionStaging() throws Exception {
		Connection conn = null;
		ArrayList<CarteraVersion_staging> ans = new ArrayList<CarteraVersion_staging>();
		conn = obtenerConexion();
		ans = obtenerCarterasVersionStaging(conn);
		cerrarConexion(conn);
		return ans;
	}

	private ArrayList<CarteraVersion_staging> obtenerCarterasVersionStaging(Connection conn) throws Exception {
		ArrayList<CarteraVersion_staging> carterasVersion = new ArrayList<CarteraVersion_staging>();
		ArrayList<Regla_Staging> reglasStaging = new ArrayList<Regla_Staging>();
		ArrayList<Expresion_Staging> expresionesStaging = new ArrayList<Expresion_Staging>();
		CarteraVersion_staging carteraVersion = null;
		Entidad entidad = null;
		TablaInput tablaInputExp = null;
		TipoFlujo tipoFlujo = null;
		TipoAmbito tipoAmbito = null;
		TipoObtencionResultados tipoObtencionResultados = null;
		Statement stmt = null;
		Statement stmt_dep = null;
		Statement stmt_dep_oficial = null;
		ResultSet rs = null;
		ResultSet rs_dep = null;
		ResultSet rs_dep_oficial = null;

		String sql = "select e.CodEntidad, e.NbrEntidad, c.CodCartera, c.NbrCartera, ta.CodTipoAmbito, ta.NbrTipoAmbito, "
				+ "mc.CodMetodologia, mc.NbrMetodologia, tor.CodTipoObtenResult, tor.NbrTipoObtenResult, tf.CodTipoFlujo, tf.NbrTipoFlujo, \r\n"
				+ "cv.CodCarteraVersion, cv.NbrVersion, cv.DesVersion,cv.FlgStageInterno, cv.FlgStageExterno, cv.FlgStageSensibilidad, \r\n"
				+ "cv.CodTablaInputExp, tabi.codaprovisionamiento,cv.NumMesesSensibilidad,(select count(1) from HE_ExpresionStaging ES where cv.CodCarteraVersion= ES.CodCarteraVersion) as ExpS,\r\n"
				+ "(select count(1) from HE_ReglaStaging RS where cv.CodCarteraVersion = RS.CodCarteraVersion) as RegS \r\n"
				+ "from HE_CarteraVersion cv \r\n"
				+ "left join he_tablainput tabi on tabi.codtablainput=cv.CodTablaInputExp\r\n"
				+ "left join HE_MetodologiaCartera mc on cv.CodMetodologia = mc.CodMetodologia\r\n"
				+ "left join ME_TipoFlujo tf on mc.CodTipoFlujo = tf.CodTipoFlujo\r\n"
				+ "left join ME_TipoObtencionResultados tor on mc.CodTipoObtenResult = tor.CodTipoObtenResult\r\n"
				+ "left join HE_Cartera c on mc.CodCartera = c.CodCartera \r\n"
				+ "left join ME_TipoAmbito ta on c.CodTipoAmbito = ta.CodTipoAmbito "
				+ "left join HE_Entidad e on c.CodEntidad = e.CodEntidad";

		String sql_dep = "select ME.CodEjecucion from HE_MapeoEjecucion ME "
				+ "left join HE_MapeoTablaBloque MTB on ME.CodMapeoTabla = MTB.CodMapeoTabla "
				+ "where MTB.CodCarteraVersion = ";

		String sql_dep_oficial = "select ME.CodEjecucion from HE_MapeoEjecucion ME "
				+ "left join HE_MapeoTablaBloque MTB on ME.CodMapeoTabla = MTB.CodMapeoTabla "
				+ "left join HE_Ejecucion E on ME.CodEjecucion = E.CodEjecucion "
				+ "where E.TipEstadoValidacion = 4 and MTB.CodCarteraVersion = "; // Asociado a ejecuciones oficiales

		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				carteraVersion = new CarteraVersion_staging();

				// Seteamos si existen dependencias
				stmt_dep = crearStatement(conn);
				rs_dep = stmt_dep.executeQuery(sql_dep + rs.getInt("CodCarteraVersion"));

				carteraVersion.setConDependencias(false);
				while (rs_dep.next()) {
					carteraVersion.setConDependencias(true);
				}
				cerrarResultSet(rs_dep);
				cerrarStatement(stmt_dep);

				// Seteamos si existen dependencias oficiales
				stmt_dep_oficial = crearStatement(conn);
				rs_dep_oficial = stmt_dep_oficial.executeQuery(sql_dep_oficial + rs.getInt("CodCarteraVersion"));

				carteraVersion.setConDependenciasOficial(false);
				while (rs_dep_oficial.next()) {
					carteraVersion.setConDependenciasOficial(true);
				}
				cerrarResultSet(rs_dep_oficial);
				cerrarStatement(stmt_dep_oficial);

				entidad = new Entidad();
				tablaInputExp = new TablaInput();
				tipoFlujo = new TipoFlujo();
				tipoAmbito = new TipoAmbito();
				tipoObtencionResultados = new TipoObtencionResultados();
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));

				carteraVersion.getMetodologia().getCartera().setEntidad(entidad);
				carteraVersion.getMetodologia().getCartera().setId_cartera(rs.getInt("CodCartera"));
				carteraVersion.getMetodologia().getCartera().setNombre_cartera(rs.getString("NbrCartera"));
				carteraVersion.getMetodologia().setId_metodologia(rs.getInt("CodMetodologia"));
				carteraVersion.getMetodologia().setNombre_metodologia(rs.getString("NbrMetodologia"));
				tipoObtencionResultados.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
				tipoObtencionResultados.setNombre_tipo_obtencion_resultados(rs.getString("NbrTipoObtenResult"));
				carteraVersion.getMetodologia().setMetodo_resultados(tipoObtencionResultados);
				tipoFlujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipoFlujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));
				carteraVersion.getMetodologia().setFlujo(tipoFlujo);
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				carteraVersion.getMetodologia().getCartera().setTipo_ambito(tipoAmbito);
				tablaInputExp.setId_tablainput(rs.getInt("CodTablaInputExp"));
				tablaInputExp.setId_aprovisionamiento(rs.getInt("codaprovisionamiento"));
				carteraVersion.setTablaInputExpresiones(tablaInputExp);
				carteraVersion.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				carteraVersion.setNombre_cartera_version(rs.getString("NbrVersion"));
				carteraVersion.setDescripcion(rs.getString("DesVersion"));
				carteraVersion.setStage_interno(rs.getBoolean("FlgStageInterno"));
				carteraVersion.setStage_externo(rs.getBoolean("FlgStageExterno"));
				carteraVersion.setStage_sensibilidad(rs.getBoolean("FlgStageSensibilidad"));
				carteraVersion.setMeses_sensibilidad(rs.getInt("NumMesesSensibilidad"));
				carteraVersion.setFlag_expresion((rs.getInt("exps") > 0 ? true : false));
				carteraVersion.setFlag_regla((rs.getInt("regs") > 0 ? true : false));
				reglasStaging = this.obtenerReglasStagingPorCarteraVersion(carteraVersion.getId_cartera_version(),
						conn);
				carteraVersion.setReglasStaging(reglasStaging);
				expresionesStaging = this
						.obtenerExpresionesStagingPorCarteraVersion(carteraVersion.getId_cartera_version(), conn);
				carteraVersion.setExpresionesStaging(expresionesStaging);
				carterasVersion.add(carteraVersion);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener versiones de carteras: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarResultSet(rs_dep);
			cerrarResultSet(rs_dep_oficial);
			cerrarStatement(stmt);
			cerrarStatement(stmt_dep);
			cerrarStatement(stmt_dep_oficial);
		}
		return carterasVersion;
	}

	//////////////////////

	public CarteraVersion_staging obtenerCarteraVersionStaging(Integer id_cartera_version) throws Exception {
		Connection conn = null;
		CarteraVersion_staging ans = new CarteraVersion_staging();
		conn = obtenerConexion();
		ans = obtenerCarteraVersionStaging(id_cartera_version, conn);
		cerrarConexion(conn);
		return ans;
	}

	private CarteraVersion_staging obtenerCarteraVersionStaging(Integer id_cartera_version, Connection conn)
			throws Exception {
		CarteraVersion_staging cartera_version = null;
		Entidad entidad = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select e.CodEntidad,e.NbrEntidad, cv.CodCarteraVersion,mc.CodMetodologia,cv.NbrVersion,cv.DesVersion,"
				+ " cv.FlgStageInterno, cv.FlgStageExterno,cv.FlgStageSensibilidad, cv.NumMesesSensibilidad, "
				+ " mc.NbrMetodologia, c.CodCartera, c.NbrCartera" + " from HE_CarteraVersion cv "
				+ " left join HE_MetodologiaCartera mc on cv.CodMetodologia = mc.CodMetodologia"
				+ " left join HE_Cartera c on mc.CodCartera = c.CodCartera "
				+ " left join HE_Entidad e on c.CodEntidad = e.CodEntidad where cv.CodCarteraVersion= "
				+ id_cartera_version;

		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				entidad = new Entidad();
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				cartera_version = new CarteraVersion_staging();
				cartera_version.getMetodologia().getCartera().setEntidad(entidad);
				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.getMetodologia().setId_metodologia(rs.getInt("CodMetodologia"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));
				cartera_version.setDescripcion(rs.getString("DesVersion"));
				cartera_version.setStage_interno(rs.getBoolean("FlgStageInterno"));
				cartera_version.setStage_externo(rs.getBoolean("FlgStageExterno"));
				cartera_version.setStage_sensibilidad(rs.getBoolean("FlgStageSensibilidad"));
				cartera_version.setMeses_sensibilidad(rs.getInt("NumMesesSensibilidad"));
				cartera_version.getMetodologia().setNombre_metodologia(rs.getString("NbrMetodologia"));
				cartera_version.getMetodologia().getCartera().setId_cartera(rs.getInt("CodCartera"));
				cartera_version.getMetodologia().getCartera().setNombre_cartera(rs.getString("NbrCartera"));
			}
		} catch (Exception ex) {
			LOGGER.error(
					"Error al obtener la version de cartera con id " + id_cartera_version + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return cartera_version;
	}

	//////////////////////////

	public ArrayList<CarteraVersion_staging> obtenerCarterasVersionStagingPorIdCartera(Integer id_cartera)
			throws Exception {
		Connection conn = null;
		ArrayList<CarteraVersion_staging> ans = new ArrayList<CarteraVersion_staging>();
		conn = obtenerConexion();
		ans = obtenerCarterasVersionStagingPorIdCartera(id_cartera, conn);
		cerrarConexion(conn);
		return ans;
	}

	private ArrayList<CarteraVersion_staging> obtenerCarterasVersionStagingPorIdCartera(Integer id_cartera,
			Connection conn) throws Exception {
		CarteraVersion_staging carteraVersion = null;
		ArrayList<CarteraVersion_staging> carterasVersion = new ArrayList<CarteraVersion_staging>();
		Entidad entidad = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select e.CodEntidad, e.NbrEntidad, cv.CodCarteraVersion,mc.CodMetodologia, cv.NbrVersion,"
				+ " cv.DesVersion, cv.FlgStageInterno, cv.FlgStageExterno,cv.FlgStageSensibilidad, cv.NumMesesSensibilidad, "
				+ " mc.NbrMetodologia, c.CodCartera,c.NbrCartera from HE_CarteraVersion cv "
				+ " left join HE_MetodologiaCartera mc on cv.CodMetodologia = mc.CodMetodologia "
				+ " left join HE_Cartera c on mc.CodCartera = c.CodCartera "
				+ " left join HE_Entidad e on c.CodEntidad = e.CodEntidad where c.CodCartera = " + id_cartera;

		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				entidad = new Entidad();
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				carteraVersion = new CarteraVersion_staging();
				carteraVersion.getMetodologia().getCartera().setEntidad(entidad);
				carteraVersion.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				carteraVersion.getMetodologia().setId_metodologia(rs.getInt("CodMetodologia"));
				carteraVersion.setNombre_cartera_version(rs.getString("NbrVersion"));
				carteraVersion.setDescripcion(rs.getString("DesVersion"));
				carteraVersion.setStage_interno(rs.getBoolean("FlgStageInterno"));
				carteraVersion.setStage_externo(rs.getBoolean("FlgStageExterno"));
				carteraVersion.setStage_sensibilidad(rs.getBoolean("FlgStageSensibilidad"));
				carteraVersion.setMeses_sensibilidad(rs.getInt("NumMesesSensibilidad"));
				carteraVersion.getMetodologia().setNombre_metodologia(rs.getString("NbrMetodologia"));
				carteraVersion.getMetodologia().getCartera().setId_cartera(rs.getInt("CodCartera"));
				carteraVersion.getMetodologia().getCartera().setNombre_cartera(rs.getString("NbrCartera"));
				carterasVersion.add(carteraVersion);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener versiones de la cartera con id " + id_cartera + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return carterasVersion;
	}

	/////////////////////////////

	public void crearCarteraVersion(CarteraVersion_staging escenariosCartera) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearCarteraVersion(escenariosCartera, conn);
		cerrarConexion(conn);
	}

	private Integer crearCarteraVersion(CarteraVersion_staging carteraVersion, Connection conn) throws Exception {
		PreparedStatement pstmt_cartera_version = null;
		PreparedStatement pstmt_mapeo_tabla = null;
		PreparedStatement pstmt_registro_campo = null;
		Integer id_cartera_version = null;
		Integer id_mapeo_tabla = null;
		String sql_cartera_version = "insert into HE_CarteraVersion(CodCarteraVersion, CodMetodologia, NbrVersion, "
				+ "DesVersion, FecActualizacionTabla) values (?,?,?,?,?)";
		String sql_mapeo_tabla = "insert into HE_MapeoTablaBloque(CodMapeoTabla, CodCarteraVersion, CodEscenarioVersion,"
				+ " FlgPrimerPaso, CodBloque, CodTablaInput, DesFiltroTabla, CodMetodologia, FecActualizacionTabla) values (?,?,?,?,?,?,?,?,?)";
		String sql_registro_campo = "insert into HE_RegistroCampoBloque(CodMapeoTabla, CodCampoBloque, FecActualizacionTabla)"
				+ " values (?,?,?)";

		try {
			// new Date(System.currentTimeMillis())
			pstmt_cartera_version = conn.prepareStatement(sql_cartera_version);
			id_cartera_version = obtenerSiguienteId("seq_he_carteraversion");
			pstmt_cartera_version.setInt(1, id_cartera_version);
			pstmt_cartera_version.setInt(2, carteraVersion.getMetodologia().getId_metodologia());
			pstmt_cartera_version.setString(3, carteraVersion.getNombre_cartera_version());
			pstmt_cartera_version.setString(4, carteraVersion.getDescripcion());
			pstmt_cartera_version.setDate(5, new Date(System.currentTimeMillis()));
			// Establecer los valores para setear los flag en la base de datos
			// pstmt_cartera_version.setBoolean(5, version_Cartera.getStage_interno());
			// pstmt_cartera_version.setBoolean(6, version_Cartera.getStage_externo());
			// pstmt_cartera_version.setBoolean(7, version_Cartera.getStage_sensibilidad());
			// pstmt_cartera_version.setInt(8, version_Cartera.getMeses_sensibilidad());

			pstmt_cartera_version.executeUpdate();

			// Agregamos los bloques
			// escenariosCartera.getMetodologia().setBLoquesTotales(
			// obtenerBloquesporIdMetodologia(escenariosCartera.getMetodologia().getId_metodologia(),
			// conn));

			pstmt_mapeo_tabla = conn.prepareStatement(sql_mapeo_tabla);
			pstmt_registro_campo = conn.prepareStatement(sql_registro_campo);
			for (JuegoEscenarios_version juegoEscenarios : carteraVersion.getJuegos_versionesEscenario()) {
				this.crearJuegoEscenariosVersion(juegoEscenarios, id_cartera_version, conn);
			}

			// carteraVersion.setJuegos_versionesEscenario(
			// obtenerJuegosConEscenariosPorCarteraVersion(id_cartera_version, conn));

			for (Bloque bloque : this.obtenerBloquesTotalesSinPrimerosPasosPorMetodologia(
					carteraVersion.getMetodologia().getId_metodologia(), conn)) {
				bloque.setCampos_bloque(this.obtenerCamposBloquePorIdBloque(bloque.getId_bloque(), conn));
				// Llenamos el mapeo y los registros si el bloque no es tipo escenario
				if (bloque.getTipo_bloque().getId_tipo_bloque() != 2 && bloque.getTipo_bloque().getId_tipo_bloque() != 4
						&& bloque.getTipo_bloque().getId_tipo_bloque() != 16
						&& bloque.getTipo_bloque().getId_tipo_bloque() != 19) {
					// Creamos el mapeo-bloque
					id_mapeo_tabla = obtenerSiguienteId("seq_he_mapeotablabloque");
					pstmt_mapeo_tabla.setInt(1, id_mapeo_tabla);
					pstmt_mapeo_tabla.setInt(2, id_cartera_version);
					pstmt_mapeo_tabla.setNull(3, java.sql.Types.INTEGER);
					pstmt_mapeo_tabla.setInt(4, 0); // Flag primer paso desactivado
					pstmt_mapeo_tabla.setInt(5, bloque.getId_bloque());
					pstmt_mapeo_tabla.setNull(6, java.sql.Types.INTEGER);
					pstmt_mapeo_tabla.setNull(7, java.sql.Types.VARCHAR);
					pstmt_mapeo_tabla.setInt(8, carteraVersion.getMetodologia().getId_metodologia());
					pstmt_mapeo_tabla.setDate(9, new Date(System.currentTimeMillis()));
					pstmt_mapeo_tabla.addBatch();
					// Creamos el registro-campo
					for (CampoBloque campo : bloque.getCampos_bloque()) {
						pstmt_registro_campo.setInt(1, id_mapeo_tabla);
						pstmt_registro_campo.setInt(2, campo.getId_campo());
						pstmt_registro_campo.setDate(3, new Date(System.currentTimeMillis()));
						pstmt_registro_campo.addBatch();
					}
				} else if (bloque.getTipo_bloque().getId_tipo_bloque().equals(2)
						|| bloque.getTipo_bloque().getId_tipo_bloque().equals(4)
						|| bloque.getTipo_bloque().getId_tipo_bloque().equals(16)
						|| bloque.getTipo_bloque().getId_tipo_bloque().equals(19)) { // Bloques a nivel escenario
					for (JuegoEscenarios_version juegoEscenarios : carteraVersion.getJuegos_versionesEscenario()) {
						for (Escenario_version escenario_version : juegoEscenarios.getEscenarios_version()) {
							// Creamos el mapeo-bloque
							id_mapeo_tabla = obtenerSiguienteId("seq_he_mapeotablabloque");
							pstmt_mapeo_tabla.setInt(1, id_mapeo_tabla);
							pstmt_mapeo_tabla.setInt(2, id_cartera_version);
							pstmt_mapeo_tabla.setInt(3, escenario_version.getId_escenario_version());
							pstmt_mapeo_tabla.setInt(4, 0); // Flag primer paso desactivado
							pstmt_mapeo_tabla.setInt(5, bloque.getId_bloque());
							pstmt_mapeo_tabla.setNull(6, java.sql.Types.INTEGER);
							pstmt_mapeo_tabla.setNull(7, java.sql.Types.VARCHAR);
							pstmt_mapeo_tabla.setInt(8, carteraVersion.getMetodologia().getId_metodologia());
							pstmt_mapeo_tabla.setDate(9, new Date(System.currentTimeMillis()));
							pstmt_mapeo_tabla.addBatch();
							// Creamos el registro-campo
							for (CampoBloque campo : bloque.getCampos_bloque()) {
								pstmt_registro_campo.setInt(1, id_mapeo_tabla);
								pstmt_registro_campo.setInt(2, campo.getId_campo());
								pstmt_registro_campo.setDate(3, new Date(System.currentTimeMillis()));
								pstmt_registro_campo.addBatch();
							}
						}
					}
				}
			}
			pstmt_mapeo_tabla.executeBatch();
			pstmt_registro_campo.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar la cartera version :" + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_cartera_version);
			cerrarPreparedStatement(pstmt_mapeo_tabla);
			cerrarPreparedStatement(pstmt_registro_campo);
		}
		return id_cartera_version;
	}

	public void replicarCarteraVersion(CarteraVersion_staging carteraVersionAReplicar, Boolean cambioCodsCmpBloque)
			throws Exception {
		Connection conn = null;
		conn = obtenerConexion();

		Integer id_carteraVer_replicado = null;
		BloqueParametrizado bloqueReplicado = null;
		ArrayList<BloqueParametrizado> bloquesReplicados = null;

		// Para replica de estructura con versiones
		BloqueParametrizado bloqueBucketsCambiado = null;
		try {
			// Creamos cartera replica
			id_carteraVer_replicado = this.crearCarteraVersion(carteraVersionAReplicar, conn); // id nuevo

			// METODO QUE RETORNE NUEVO ID CARTERA VERSION?? REEMPLAZAR?
			// ACTUALIZAR MAPEO BUCKETS
			// ACTUALIZAR MAPEOSSS BLOQUES Y IDS JUEGOS/ESCENARIOS VER
			// ACTUALIZAR ID CARTERA VERSION DE REGLAS; EXPRESIONES

			// Replicamos buckets (solo creditos y unicamente modelo interno)
			if (carteraVersionAReplicar.getMetodologia().getCartera().getTipo_ambito().getId_tipo_ambito().equals(1)
					&& carteraVersionAReplicar.getMetodologia().getMetodo_resultados()
							.getNombre_tipo_obtencion_resultados().equalsIgnoreCase("Modelo interno")) {
				bloqueReplicado = this.obtenerBloqueBuckets(carteraVersionAReplicar.getId_cartera_version());

				// Reemplazamos id mapeo tabla
				bloqueReplicado
						.setId_mapeo_tabla(this.obtenerBloqueBuckets(id_carteraVer_replicado).getId_mapeo_tabla());

				// Reemplazamos ids de campo de bloque (en caso haya cambio de metodologia) -
				// Replica de estructura con versiones
				if (cambioCodsCmpBloque.equals(true)) {
					bloqueBucketsCambiado = this.obtenerBloqueBuckets(id_carteraVer_replicado);

					for (CampoParametrizado cmpBcktRef : bloqueBucketsCambiado.getCampos_parametrizado()) {
						for (CampoParametrizado cmpBcktRemp : bloqueReplicado.getCampos_parametrizado()) {
							if (cmpBcktRemp.getCampo_bloque().getNombre_campo()
									.equals(cmpBcktRef.getCampo_bloque().getNombre_campo())) {
								cmpBcktRemp.setCampo_bloque(cmpBcktRef.getCampo_bloque());
							}
						}
					}
				}

				this.editarBloqueParametrizado(bloqueReplicado);
			}

			// Replicamos variables (bloques a nivel cartera version y escenario version)
			bloquesReplicados = this
					.obtenerBloquesParametrizadoPorCarteraVersion(carteraVersionAReplicar.getId_cartera_version());

			// Reemplazamos id's de mapeo tabla
			for (BloqueParametrizado bloq : this
					.obtenerBloquesParametrizadoPorCarteraVersion(id_carteraVer_replicado)) {
				for (BloqueParametrizado bloqRep : bloquesReplicados) {
					if (bloqRep.getEscenario_version() != null && bloq.getEscenario_version() != null) {

						// En caso sean bloques de escenario version
						if (bloqRep.getId_bloque().equals(bloq.getId_bloque())
								&& bloqRep.getEscenario_version().getEscenario().getId_escenario()
										.equals(bloq.getEscenario_version().getEscenario().getId_escenario())) {
							bloqRep.setId_mapeo_tabla(bloq.getId_mapeo_tabla());
						}
					} else if (bloqRep.getCartera_version() != null && bloq.getCartera_version() != null) {

						// En caso sean bloques de cartera version
						if (bloqRep.getId_bloque().equals(bloq.getId_bloque())) {
							bloqRep.setId_mapeo_tabla(bloq.getId_mapeo_tabla());
						}
					}

				}
			}
			this.editarBloquesParametrizados(bloquesReplicados);

			// Replicamos staging, expresiones, tablas expresion (solo creditos)
			if (carteraVersionAReplicar.getMetodologia().getCartera().getTipo_ambito().getId_tipo_ambito().equals(1)) {
				// Reemplazamos ids
				carteraVersionAReplicar.setId_cartera_version(id_carteraVer_replicado);

				for (Expresion_Staging exp : carteraVersionAReplicar.getExpresionesStaging()) {
					exp.setId_cartera_version(id_carteraVer_replicado);
				}

				for (Regla_Staging reg : carteraVersionAReplicar.getReglasStaging()) {
					reg.setId_cartera_version(id_carteraVer_replicado);
				}

				this.editarStagingCarteraVersion(carteraVersionAReplicar);
			}
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al replicar la cartera version :" + ex.getMessage());
			throw ex;
		} finally {
			cerrarConexion(conn);
		}
	}

	/////////////////////

	public void editarCarteraVersion(CarteraVersion_staging cartera_version) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		editarCarteraVersion(cartera_version, conn);
		cerrarConexion(conn);
	}

	private void editarCarteraVersion(CarteraVersion_staging cartera_version, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		Boolean equal = false;
		String sqlUpdateCarteraVersion = "update HE_CarteraVersion set NbrVersion= ?, DesVersion= ?, FlgStageInterno= ?,"
				+ " FlgStageExterno= ?, FlgStageSensibilidad= ?, NumMesesSensibilidad= ?, FecActualizacionTabla = ?"
				+ " where CodCarteraVersion=?";
		String select_juegoEscenariosVersion = "select CodJuegoEscenarios from "
				+ " HE_JuegoEscenariosVersion where CodCarteraVersion= ";
		try {
			select_juegoEscenariosVersion += cartera_version.getId_cartera_version();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(select_juegoEscenariosVersion);
			while (rs.next()) {
				equal = false;
				for (int j = 0; j < cartera_version.getJuegos_versionesEscenario().size(); j++) {
					if (rs.getInt("CodJuegoEscenarios") == cartera_version.getJuegos_versionesEscenario().get(j)
							.getId_juego_escenarios()) {
						equal = true;
						this.editarJuegoEscenariosVersion(cartera_version.getJuegos_versionesEscenario().get(j), conn);
						break;
					}
				}
				if (equal == false) {
					this.borrarJuegoEscenariosVersion(rs.getInt("CodJuegoEscenarios"), conn);
				}
			}

			for (int j = 0; j < cartera_version.getJuegos_versionesEscenario().size(); j++) {
				if (cartera_version.getJuegos_versionesEscenario().get(j).getId_juego_escenarios() == null) {
					this.crearJuegoEscenariosVersion(cartera_version.getJuegos_versionesEscenario().get(j),
							cartera_version.getId_cartera_version(), conn);
				}
			}
			cerrarResultSet(rs);
			pstmt = conn.prepareStatement(sqlUpdateCarteraVersion);
			pstmt.setString(1, cartera_version.getNombre_cartera_version());
			pstmt.setString(2, cartera_version.getDescripcion());
			pstmt.setBoolean(3, cartera_version.getStage_interno());
			pstmt.setBoolean(4, cartera_version.getStage_externo());
			pstmt.setBoolean(5, cartera_version.getStage_sensibilidad());
			pstmt.setInt(6, cartera_version.getMeses_sensibilidad());
			pstmt.setDate(7, new Date(System.currentTimeMillis()));
			pstmt.setInt(8, cartera_version.getId_cartera_version());
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar la versión de cartera con id " + cartera_version.getId_cartera_version()
					+ " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarPreparedStatement(pstmt);
		}
	}
	//////////////////////////

	public void borrarCarteraVersion(Integer id_version_cartera) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		borrarCarteraVersion(id_version_cartera, conn);
		cerrarConexion(conn);
	}

	private void borrarCarteraVersion(Integer id_cartera_version, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "Delete from HE_CarteraVersion where CodCarteraVersion= ?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_cartera_version);
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar la cartera_version con id " + id_cartera_version + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	// ----------Juego de escenarios version ------------------

	public void crearJuegoEscenariosVersion(JuegoEscenarios_version juegoEscenarios, Integer id_cartera_version)
			throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearJuegoEscenariosVersion(juegoEscenarios, id_cartera_version, conn);
		cerrarConexion(conn);
	}

	private void crearJuegoEscenariosVersion(JuegoEscenarios_version juegoEscenarios, Integer id_cartera_version,
			Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		Integer id_juego_esc_version = null;
		String sql = "insert into HE_JuegoEscenariosVersion(CodJuegoEscenarios,CodCarteraVersion,NbrJuegoEscenarios,"
				+ "DesJuegoEscenarios,FecActualizacionTabla) values (?,?,?,?,?)";
		try {
			id_juego_esc_version = obtenerSiguienteId("seq_he_JuegoEscenariosversion");
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_juego_esc_version);
			pstmt.setInt(2, id_cartera_version);
			pstmt.setString(3, juegoEscenarios.getNombre_juego_escenarios());
			pstmt.setString(4, juegoEscenarios.getDescripcion_juego_escenarios());
			pstmt.setDate(5, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);

			for (Escenario_version escenario_version : juegoEscenarios.getEscenarios_version()) {
				this.crearEscenarioVersion(escenario_version, id_juego_esc_version, id_cartera_version, conn);
			}
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear el juego de versiones escenario : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	// ----------Escenarios version ------------------

	//////////////////// Versión Escenario ///////////////////////

	public ArrayList<JuegoEscenarios_version> obtenerJuegosConEscenariosPorCarteraVersion(Integer id_cartera_version)
			throws Exception {
		Connection conn = null;
		ArrayList<JuegoEscenarios_version> ans = new ArrayList<JuegoEscenarios_version>();
		conn = obtenerConexion();
		ans = obtenerJuegosConEscenariosPorCarteraVersion(id_cartera_version, conn);
		cerrarConexion(conn);
		return ans;
	}

	public ArrayList<JuegoEscenarios_version> obtenerJuegosConEscenariosPorCarteraVersion(Integer id_cartera_version,
			Connection conn) throws Exception {
		ArrayList<JuegoEscenarios_version> juegosEscenariosVersion = null;
		JuegoEscenarios_version juegoEscenariosVersion = null;
		ArrayList<Escenario_version> escenarios_version = null;

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		String sql = "select JEV.CodJuegoEscenarios, JEV.NbrJuegoEscenarios, JEV.DesJuegoEscenarios "
				+ " from HE_JuegoEscenariosVersion JEV " + " where JEV.CodCarteraVersion=?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_cartera_version);
			rs = pstmt.executeQuery();
			juegosEscenariosVersion = new ArrayList<JuegoEscenarios_version>();

			while (rs.next()) {
				juegoEscenariosVersion = new JuegoEscenarios_version();
				juegoEscenariosVersion.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenariosVersion.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenariosVersion.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));
				escenarios_version = this
						.obtenerEscenariosVersionPorIdJuegoEscenarios(juegoEscenariosVersion.getId_juego_escenarios());
				juegoEscenariosVersion.setEscenarios_version(escenarios_version);

				juegosEscenariosVersion.add(juegoEscenariosVersion);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener juegos con escenarios para la cartera version con id " + id_cartera_version
					+ " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return juegosEscenariosVersion;
	}

	public void editarJuegoEscenariosVersion(JuegoEscenarios_version juegoEscenarios) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		editarJuegoEscenariosVersion(juegoEscenarios, conn);
		cerrarConexion(conn);
	}

	private void editarJuegoEscenariosVersion(JuegoEscenarios_version juegoEscenarios, Connection conn)
			throws Exception {
		PreparedStatement pstmt = null;
		String sql = "update HE_JuegoEscenariosVersion set NbrJuegoEscenarios=?, DesJuegoEscenarios=?, FecActualizacionTabla=?"
				+ " where CodJuegoEscenarios= ?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, juegoEscenarios.getNombre_juego_escenarios());
			pstmt.setString(2, juegoEscenarios.getDescripcion_juego_escenarios());
			pstmt.setDate(3, new Date(System.currentTimeMillis()));
			pstmt.setInt(4, juegoEscenarios.getId_juego_escenarios());
			pstmt.executeUpdate();

			for (Escenario_version escenario_version : juegoEscenarios.getEscenarios_version()) {
				this.editarEscenarioVersion(escenario_version, conn);
			}

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar el juego de los escenarios version con id "
					+ juegoEscenarios.getId_juego_escenarios() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void borrarJuegoEscenariosVersion(Integer id_juego_escenarios) throws Exception {
		Connection conn;
		conn = obtenerConexion();
		borrarJuegoEscenariosVersion(id_juego_escenarios, conn);
		cerrarConexion(conn);
	}

	private void borrarJuegoEscenariosVersion(Integer id_juego_escenarios, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "delete from HE_JuegoEscenariosVersion where CodJuegoEscenarios= ?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_juego_escenarios);
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar el juego de versiones de escenario con id " + id_juego_escenarios + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	//////////////////// Escenario version por id_carteraVersion //////////

	public ArrayList<Escenario_version> obtenerEscenariosVersionPorCarteraVersion(Integer id_cartera_version)
			throws Exception {
		Connection conn = null;
		ArrayList<Escenario_version> ans = new ArrayList<Escenario_version>();
		conn = obtenerConexion();
		ans = obtenerEscenariosVersionPorCarteraVersion(id_cartera_version, conn);
		cerrarConexion(conn);
		return ans;
	}

	private ArrayList<Escenario_version> obtenerEscenariosVersionPorCarteraVersion(Integer id_cartera_version,
			Connection conn) throws Exception {
		ArrayList<Escenario_version> escenarios_version = new ArrayList<Escenario_version>();
		Escenario_version escenario_version = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select EV.CodEscenarioVersion, EV.CodEscenario, EV.NumPeso, JEV.CodCarteraVersion, ES.NbrEscenario"
				+ " from HE_EscenarioVersion EV, ME_Escenario ES, HE_JuegoEscenariosVersion JEV "
				+ " where EV.CodEscenario=es.CodEscenario and EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
				+ " and JEV.CodCarteraVersion=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_cartera_version);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				escenario_version = new Escenario_version();
				escenario_version.setId_escenario_version(rs.getInt("CodEscenarioVersion"));
				escenario_version.getEscenario().setId_escenario(rs.getInt("CodEscenario"));
				escenario_version.setPeso(rs.getDouble("NumPeso"));
				escenario_version.getEscenario().setNombre_escenario(rs.getString("NbrEscenario"));
				escenarios_version.add(escenario_version);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los escenarios version por la cartera version con id " + id_cartera_version
					+ " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return escenarios_version;
	}

	public void crearEscenarioVersion(Escenario_version escenario_version, Integer id_juego_escenarios,
			Integer id_cartera_version) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearEscenarioVersion(escenario_version, id_juego_escenarios, id_cartera_version, conn);
		cerrarConexion(conn);
	}

	private void crearEscenarioVersion(Escenario_version escenario_version, Integer id_juego_escenarios,
			Integer id_cartera_version, Connection conn) throws Exception {
		PreparedStatement pstmt_escenario_version = null;
		String sql_esc_version = "insert into HE_EscenarioVersion(CodEscenarioVersion,CodJuegoEscenarios,CodEscenario,NumPeso"
				+ ",FecActualizacionTabla) values (?,?,?,?,?)";
		Integer id_escenario_version = null;

		try {
			pstmt_escenario_version = conn.prepareStatement(sql_esc_version);
			id_escenario_version = obtenerSiguienteId("seq_he_escenarioversion");
			escenario_version.setId_escenario_version(id_escenario_version);
			pstmt_escenario_version.setInt(1, escenario_version.getId_escenario_version());
			pstmt_escenario_version.setInt(2, id_juego_escenarios);
			pstmt_escenario_version.setInt(3, escenario_version.getEscenario().getId_escenario());
			pstmt_escenario_version.setDouble(4, escenario_version.getPeso());
			pstmt_escenario_version.setDate(5, new Date(System.currentTimeMillis()));
			pstmt_escenario_version.executeUpdate();

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar la versión de escenario : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_escenario_version);
		}
	}

	///////////////////////// Versiones Escenario

	public void editarEscenarioVersion(Escenario_version escenario_version) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		editarEscenarioVersion(escenario_version, conn);
		cerrarConexion(conn);
	}

	private void editarEscenarioVersion(Escenario_version escenario_version, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "update HE_EscenarioVersion set NumPeso= ?, FecActualizacionTabla=? where CodEscenarioversion= ?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setDouble(1, escenario_version.getPeso());
			pstmt.setDate(2, new Date(System.currentTimeMillis()));
			pstmt.setInt(3, escenario_version.getId_escenario_version());
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar el escenario version con id " + escenario_version.getId_escenario_version()
					+ " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	/////////////////////

	public void borrarEscenarioVersion(Integer id_escenario_version) throws Exception {
		Connection conn;
		conn = obtenerConexion();
		borrarEscenarioVersion(id_escenario_version, conn);
		cerrarConexion(conn);
	}

	private void borrarEscenarioVersion(Integer id_escenario_version, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "Delete from HE_EscenarioVersion where CodEscenarioVersion = ?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_escenario_version);
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error al borrar el escenario version con id " + id_escenario_version + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	// Obtener diccionarios y sus elementos

	// Obtener pais
	public String obtenerPaisPorID(Integer id_pais) throws Exception {
		String nombre = null;
		for (int i = 0; i < dic_paises.size(); i++) {
			if (dic_paises.get(i).getId_pais().equals(id_pais)) {
				nombre = dic_paises.get(i).getNombre_pais();
				break;
			}
		}
		return nombre;
	}

	public ArrayList<Pais> obtenerPaises() throws Exception {
		return dic_paises;
	}

	// Obtener tipo_ambito
	public String obtenerTipoAmbitoPorID(Integer id_tipo_ambito) throws Exception {
		String nombre = null;
		for (int i = 0; i < dic_tipo_ambito.size(); i++) {
			if (dic_tipo_ambito.get(i).getId_tipo_ambito().equals(id_tipo_ambito)) {
				nombre = dic_tipo_ambito.get(i).getNombre_tipo_ambito();
				break;
			}
		}
		return nombre;
	}

	public ArrayList<TipoAmbito> obtenerTiposAmbito() throws Exception {
		return dic_tipo_ambito;
	}

	// Obtener formatos input
	public ArrayList<FormatoInput> obtenerFormatosInput() throws Exception {
		return dic_formato_input;
	}

	// Obtener tipo_magnitud
	public String obtenerTipoMagnitudPorID(Integer id_tipo_magnitud) throws Exception {
		String nombre = null;
		for (int i = 0; i < dic_tipo_magnitud.size(); i++) {
			if (dic_tipo_magnitud.get(i).getId_tipo_magnitud().equals(id_tipo_magnitud)) {
				nombre = dic_tipo_magnitud.get(i).getNombre_tipo_magnitud();
				break;
			}
		}
		return nombre;
	}

	public ArrayList<TipoMagnitud> obtenerTiposMagnitud(Integer id_tipo_ambito) throws Exception {
		if (id_tipo_ambito.equals(1)) {
			return dic_tipo_magnitud_creditos;
		} else {
			return dic_tipo_magnitud_inversiones;
		}
	}

	/*
	 * public ArrayList<TipoCartera> obtenerTiposCartera() throws Exception { return
	 * dic_tipo_cartera; }
	 */

	// Obtener motor_calculo
	public String obtenerMotorCalculoPorID(Integer id_motor) throws Exception {
		String nombre = null;
		for (int i = 0; i < dic_motor_calculo.size(); i++) {
			if (dic_motor_calculo.get(i).getId_motor().equals(id_motor)) {
				nombre = dic_motor_calculo.get(i).getNombre_motor();
				break;
			}
		}
		return nombre;
	}

	public ArrayList<MotorCalculo> obtenerMotoresCalculo() throws Exception {
		return dic_motor_calculo;
	}

	// Obtener tipo_obtencion_resultados
	public String obtenerTipoObtencionResultadosPorID(Integer id_tipo_obtencion_resultados) throws Exception {
		String nombre = null;
		for (int i = 0; i < dic_tipo_obtencion_resultados.size(); i++) {
			if (dic_tipo_obtencion_resultados.get(i).getId_tipo_obtencion_resultados()
					.equals(id_tipo_obtencion_resultados)) {
				nombre = dic_tipo_obtencion_resultados.get(i).getNombre_tipo_obtencion_resultados();
				break;
			}
		}
		return nombre;
	}

	public ArrayList<TipoObtencionResultados> obtenerTiposObtencionResultados() throws Exception {
		return dic_tipo_obtencion_resultados;
	}

	// Obtener tipo_metodologia
	public String obtenerTipoMetodologiaPorID(Integer id_tipo_metodologia) throws Exception {
		String nombre = null;
		for (int i = 0; i < dic_tipo_metodologia.size(); i++) {
			if (dic_tipo_metodologia.get(i).getId_tipo_metodologia().equals(id_tipo_metodologia)) {
				nombre = dic_tipo_metodologia.get(i).getNombre_tipo_metodologia();
				break;
			}
		}
		return nombre;
	}

	/*
	 * public ArrayList<TipoMetodologia> obtenerTiposMetodologia() throws Exception
	 * { return dic_tipo_metodologia; }
	 */

	// Obtener tipo_periodicidad
	public String obtenerTipoPeriodicidadPorID(Integer id_tipo_periodicidad) throws Exception {
		String nombre = null;
		for (int i = 0; i < dic_tipo_periodicidad.size(); i++) {
			if (dic_tipo_metodologia.get(i).getId_tipo_metodologia().equals(id_tipo_periodicidad)) {
				nombre = dic_tipo_periodicidad.get(i).getNombre_tipo_periodicidad();
				break;
			}
		}
		return nombre;
	}

	public ArrayList<TipoPeriodicidad> obtenerTiposPeriodicidad() throws Exception {
		return dic_tipo_periodicidad;
	}

	// Obtener tipo_variable
	public String obtenerTipoVariablePorID(Integer id_tipo_variable) throws Exception {
		String nombre = null;
		for (int i = 0; i < dic_tipo_variable.size(); i++) {
			if (dic_tipo_variable.get(i).getId_tipo_variable().equals(id_tipo_variable)) {
				nombre = dic_tipo_variable.get(i).getNombre_tipo_variable();
				break;
			}
		}
		return nombre;
	}

	public ArrayList<TipoVariable> obtenerTiposVariable() throws Exception {
		return dic_tipo_variable;
	}

	// Obtener tipo_campo
	public String obtenerTipoCampoPorID(Integer id_tipo_campo) throws Exception {
		String nombre = null;
		for (int i = 0; i < dic_tipo_campo.size(); i++) {
			if (dic_tipo_campo.get(i).getId_tipo_campo().equals(id_tipo_campo)) {
				nombre = dic_tipo_campo.get(i).getNombre_tipo_campo();
				break;
			}
		}
		return nombre;
	}

	public ArrayList<TipoCampo> obtenerTiposCampo() throws Exception {
		return dic_tipo_campo;
	}

	// Obtener Tipo Flujo
	public ArrayList<TipoFlujo> obtenerTiposFlujo() throws Exception {
		return dic_tipo_flujo;
	}

	// Obtener Tipo Ejecucion
	public ArrayList<TipoEjecucion> obtenerTiposEjecucion() throws Exception {
		return dic_tipo_ejecucion;
	}

	// Obtener Tipo Bloque
	public ArrayList<TipoBloque> obtenerTiposBloque() throws Exception {
		return dic_tipo_bloque;
	}

	// Obtener tipo_indicador
	public ArrayList<TipoIndicador> obtenerTiposIndicador() throws Exception {
		return dic_tipo_indicador;
	}

	// ----------------- Parametrización Buckets y Variables-----------
	// ----------------- Made by Yisus -------------------------------
	public ArrayList<BloqueParametrizado> obtenerBloquesParametrizadoPorCarteraVersion(Integer id_cartera_version)
			throws Exception {
		Connection conn = null;
		ArrayList<BloqueParametrizado> bloquesParametrizado = new ArrayList<BloqueParametrizado>();
		conn = obtenerConexion();
		bloquesParametrizado = obtenerBloquesParametrizadoPorCarteraVersion(id_cartera_version, conn);
		cerrarConexion(conn);
		return bloquesParametrizado;
	}

	public ArrayList<BloqueParametrizado> obtenerBloquesParametrizadoPorCarteraVersion(Integer id_cartera_version,
			Connection conn) throws Exception {
		ArrayList<BloqueParametrizado> bloquesParametrizado = new ArrayList<BloqueParametrizado>();
		BloqueParametrizado bloqueParametrizado = null;
		ArrayList<CampoParametrizado> campos_parametrizado = null;
		Cartera_version cartera_version = null;
		Escenario_version escenario_version = null;
		Escenario escenario = null;
		FormatoInput formatoInput = null;
		TipoBloque tipoBloque = null;
		TipoEjecucion tipoEjecucion = null;
		TipoFlujo tipoFlujo = null;
		TipoObtencionResultados tipoObtencionResultados = null;
		TablaInput tablaInput = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select MTB.CodMapeoTabla, MTB.Desfiltrotabla, "
				+ "CV.CodCarteraVersion, CV.NbrVersion, CV.DesVersion, "
				+ "TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata, A.NbrFichero, "
				+ "EV.CodEscenarioVersion, EV.NumPeso, E.CodEscenario, E.NbrEscenario, "
				+ "B.CodBloque, B.NbrBloque, B.NbrVariableMotor, B.FlgTablaInput, "
				+ "FI.CodFormatoInput, FI.NbrFormatoInput, "
				+ "TB.CodTipoBloque, TB.NbrTipoBloque, TE.CodTipoEjecucion, TE.NbrTipoEjecucion, "
				+ "TF.CodTipoFlujo, TF.NbrTipoFlujo, TOR.CodTipoObtenResult, TOR.NbrTipoObtenResult "
				+ "from HE_MapeoTablaBloque MTB "
				+ "left join HE_TablaInput TI on MTB.CodTablaInput = TI.CodTablaInput "
				+ "left join HE_Aprovisionamiento A on TI.CodAprovisionamiento = A.CodAprovisionamiento "
				+ "left join HE_CarteraVersion CV on MTB.CodCarteraVersion = CV.CodCarteraVersion "
				+ "left join HE_EscenarioVersion EV on MTB.CodEscenarioVersion = EV.CodEscenarioVersion "
				+ "left join ME_Escenario E on EV.CodEscenario = E.CodEscenario "
				+ "left join HE_MapeoMetodologiaBloque MMB on MTB.CodMetodologia=MMB.CodMetodologia and MTB.CodBloque=MMB.CodBloque "
				+ "left join He_Bloque B on B.CodBloque=MMB.CodBloque "
				+ "left join ME_FormatoInput FI on B.CodFormatoInput = FI.CodFormatoInput "
				+ "left join ME_TipoBloque TB on TB.CodTipoBloque=B.CodTipoBloque "
				+ "left join ME_TipoObtencionResultados TOR on TB.CodTipoObtenResult = TOR.CodTipoObtenResult "
				+ "left join ME_TipoEjecucion TE on TE.CodTipoEjecucion = TB.CodTipoEjecucion "
				+ "left join ME_TipoFlujo TF on TF.CodTipoFlujo = TE.CodTipoFlujo "
				+ "where B.CodTipoBloque<>8 and TB.FlgBucket=0 and MTB.FlgPrimerPaso = 0 and MTB.CodCarteraVersion="
				+ id_cartera_version;
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				bloqueParametrizado = new BloqueParametrizado();
				bloqueParametrizado.setId_mapeo_tabla(rs.getInt("CodMapeoTabla"));

				// Si es un bloque a nivel de escenario version
				if (rs.getInt("CodEscenarioVersion") != 0) {
					escenario_version = new Escenario_version();
					escenario_version.setId_escenario_version(rs.getInt("CodEscenarioVersion"));
					escenario_version.setPeso(rs.getDouble("NumPeso"));

					escenario = new Escenario();
					escenario.setId_escenario(rs.getInt("CodEscenario"));
					escenario.setNombre_escenario(rs.getString("NbrEscenario"));
					escenario_version.setEscenario(escenario);

					bloqueParametrizado.setEscenario_version(escenario_version);

				} else {
					cartera_version = new Cartera_version();
					cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
					cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));
					cartera_version.setDescripcion(rs.getString("DesVersion"));

					bloqueParametrizado.setCartera_version(cartera_version);
				}

				tablaInput = new TablaInput();
				tablaInput.setId_tablainput(rs.getInt("CodTablaInput"));
				tablaInput.setNombre_metadata(rs.getString("NbrMetadata"));
				tablaInput.setVersion_metadata(rs.getInt("NumVersionMetadata"));
				tablaInput.setNombre_fichero(rs.getString("NbrFichero"));
				bloqueParametrizado.setTabla_input(tablaInput);

				bloqueParametrizado.setId_bloque(rs.getInt("CodBloque"));
				bloqueParametrizado.setNombre_bloque(rs.getString("NbrBloque"));
				bloqueParametrizado.setVariable_motor(rs.getString("NbrVariableMotor"));
				bloqueParametrizado.setFiltro_tabla(rs.getString("Desfiltrotabla"));
				bloqueParametrizado.setFlag_tabla_input(rs.getBoolean("FlgTablaInput"));

				formatoInput = new FormatoInput();
				formatoInput.setId_formato_input(rs.getInt("CodFormatoInput"));
				formatoInput.setNombre_formato_input(rs.getString("NbrFormatoInput"));
				bloqueParametrizado.setFormato_input(formatoInput);

				tipoBloque = new TipoBloque();
				tipoBloque.setId_tipo_bloque(rs.getInt("CodTipoBloque"));
				tipoBloque.setNombre_tipo_bloque(rs.getString("NbrTipoBloque"));

				tipoEjecucion = new TipoEjecucion();
				tipoEjecucion.setId_tipo_ejecucion(rs.getInt("CodTipoEjecucion"));
				tipoEjecucion.setNombre_tipo_ejecucion(rs.getString("NbrTipoEjecucion"));

				tipoFlujo = new TipoFlujo();
				tipoFlujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipoFlujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));
				tipoEjecucion.setTipo_flujo(tipoFlujo);
				tipoBloque.setTipo_ejecucion(tipoEjecucion);

				tipoObtencionResultados = new TipoObtencionResultados();
				tipoObtencionResultados.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
				tipoObtencionResultados.setNombre_tipo_obtencion_resultados(rs.getString("NbrTipoObtenResult"));
				tipoBloque.setTipo_obtencion_resultados(tipoObtencionResultados);
				bloqueParametrizado.setTipo_bloque(tipoBloque);

				campos_parametrizado = this.obtenerCamposParametrizadoPorMapeo(bloqueParametrizado.getId_mapeo_tabla(),
						bloqueParametrizado.getFlag_tabla_input(), conn);
				bloqueParametrizado.setCampos_parametrizado(campos_parametrizado);

				bloquesParametrizado.add(bloqueParametrizado);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los bloques parametrizados de la cartera version con id: "
					+ id_cartera_version + " " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return bloquesParametrizado;
	}

	public BloqueParametrizado obtenerBloqueBuckets(Integer id_cartera_version) throws Exception {
		BloqueParametrizado bloqueBuckets = new BloqueParametrizado();
		ArrayList<CampoParametrizado> camposParam = null;
		Cartera_version cartera_version = null;
		TipoBloque tipoBloque = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select MTB.CodMapeoTabla, " + "CV.CodCarteraVersion, CV.NbrVersion, CV.DesVersion, "
				+ "B.CodBloque, B.NbrBloque, B.NbrVariableMotor, B.FlgTablaInput, "
				+ "TB.CodTipoBloque, TB.NbrTipoBloque " + "from HE_MapeoTablaBloque MTB "
				+ "left join HE_CarteraVersion CV on MTB.CodCarteraVersion = CV.CodCarteraVersion "
				+ "left join HE_MapeoMetodologiaBloque MMB on MTB.CodMetodologia=MMB.CodMetodologia and MTB.CodBloque=MMB.CodBloque "
				+ "left join He_Bloque B on B.CodBloque=MMB.CodBloque "
				+ "left join ME_TipoBloque TB on TB.CodTipoBloque=B.CodTipoBloque "
				+ "where B.CodTipoBloque=8 and TB.FlgBucket=1 and MTB.FlgPrimerPaso = 0 and MTB.CodCarteraVersion="
				+ id_cartera_version;
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				bloqueBuckets.setId_mapeo_tabla(rs.getInt("CodMapeoTabla"));

				cartera_version = new Cartera_version();
				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));
				cartera_version.setDescripcion(rs.getString("DesVersion"));
				bloqueBuckets.setCartera_version(cartera_version);

				bloqueBuckets.setId_bloque(rs.getInt("CodBloque"));
				bloqueBuckets.setNombre_bloque(rs.getString("NbrBloque"));
				bloqueBuckets.setVariable_motor(rs.getString("NbrVariableMotor"));
				bloqueBuckets.setFlag_tabla_input(rs.getBoolean("FlgTablaInput"));

				tipoBloque = new TipoBloque();
				tipoBloque.setId_tipo_bloque(rs.getInt("CodTipoBloque"));
				tipoBloque.setNombre_tipo_bloque(rs.getString("NbrTipoBloque"));
				bloqueBuckets.setTipo_bloque(tipoBloque);

				camposParam = this.obtenerCamposParametrizadoPorMapeo(bloqueBuckets.getId_mapeo_tabla(), false, conn);

				bloqueBuckets.setCampos_parametrizado(camposParam);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener el Bloque de Buckets: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return bloqueBuckets;
	}

	public ArrayList<CampoParametrizado> obtenerCamposParametrizadoPorMapeo(Integer id_mapeo_tabla,
			Boolean flag_tabla_input) throws Exception {
		Connection conn = null;
		ArrayList<CampoParametrizado> camposParam = new ArrayList<CampoParametrizado>();
		conn = obtenerConexion();
		camposParam = obtenerCamposParametrizadoPorMapeo(id_mapeo_tabla, flag_tabla_input, conn);
		cerrarConexion(conn);
		return camposParam;
	}

	public ArrayList<CampoParametrizado> obtenerCamposParametrizadoPorMapeo(Integer id_mapeo_tabla,
			Boolean flag_tabla_input, Connection conn) throws Exception {
		ArrayList<CampoParametrizado> camposParam = new ArrayList<CampoParametrizado>();
		TipoCampo tipoCampo = null;
		CampoParametrizado campoParam = null;
		CampoBloque campoBloque = null;
		CampoInput campoInput = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select RCB.TxtValor, RCB.NumValor, " + "CB.CodCampoBloque, CB.NbrCampo as NbrCampoBloque, "
				+ "TC.CodTipoCampo, TC.NbrTipoCampo, " + "CI.CodCampo, CI.NbrCampo as NbrCampoInput "
				+ "from HE_RegistroCampoBloque RCB "
				+ "left join HE_CampoBloque CB on RCB.CodCampoBloque = CB.CodCampoBloque "
				+ "left join ME_TipoCampo TC on CB.CodTipoCampo = TC.CodTipoCampo "
				+ "left join HE_CampoInput CI on RCB.CodCampo = CI.CodCampo " + "where RCB.CodMapeoTabla = "
				+ id_mapeo_tabla + " order by rcb.codcampobloque asc";
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				campoParam = new CampoParametrizado();
				campoBloque = new CampoBloque();
				campoBloque.setId_campo(rs.getInt("CodCampoBloque"));
				campoBloque.setNombre_campo(rs.getString("NbrCampoBloque"));

				tipoCampo = new TipoCampo();
				tipoCampo.setId_tipo_campo(rs.getInt("CodTipoCampo"));
				tipoCampo.setNombre_tipo_campo(rs.getString("NbrTipoCampo"));
				campoBloque.setTipo_campo(tipoCampo);

				campoParam.setCampo_bloque(campoBloque);
				if (flag_tabla_input) {

					campoInput = new CampoInput();
					campoInput.setId_campo(rs.getInt("CodCampo"));
					campoInput.setNombre_campo(rs.getString("NbrCampoInput"));

					campoParam.setCampo_input(campoInput);

				} else {
					campoParam.setNumValor(rs.getDouble("NumValor"));
					campoParam.setTxtValor(rs.getString("TxtValor"));
				}
				camposParam.add(campoParam);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener los campos parametrizados del mapeo_tabla con id: " + id_mapeo_tabla
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return camposParam;
	}

	public BloqueParametrizado editarBloqueParametrizado(BloqueParametrizado bloqueParam) throws Exception {
		ArrayList<CampoParametrizado> camposParam = null;
		if (bloqueParam.getTipo_bloque().getId_tipo_bloque() != 8) { //
			this.editarMapeoTablaInput(bloqueParam);
		}
		camposParam = bloqueParam.getCampos_parametrizado();
		for (CampoParametrizado campoParam : camposParam) {
			this.editarCampoParametrizado(campoParam, bloqueParam.getFlag_tabla_input(),
					bloqueParam.getId_mapeo_tabla());
		}
		return bloqueParam;
	}

	public void editarBloquesParametrizados(ArrayList<BloqueParametrizado> bloquesParam) throws Exception {
		ArrayList<CampoParametrizado> camposParam = null;
		for (BloqueParametrizado bloqueParam : bloquesParam) {
			if (bloqueParam.getTipo_bloque().getId_tipo_bloque() != 8) {
				this.editarMapeoTablaInput(bloqueParam);
			}
			camposParam = bloqueParam.getCampos_parametrizado();
			for (CampoParametrizado campoParam : camposParam) {
				this.editarCampoParametrizado(campoParam, bloqueParam.getFlag_tabla_input(),
						bloqueParam.getId_mapeo_tabla());
			}
		}
	}

	public void editarMapeoTablaInput(BloqueParametrizado bloqueParam) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "update HE_MapeoTablaBloque set CodTablaInput= ?, DesFiltroTabla=?, FecActualizacionTabla=?"
				+ " where CodMapeoTabla= ?"; // FlgPrimerPaso = 0 -> Ahora sirve para primer paso tambien

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);

			if (bloqueParam.getFlag_tabla_input()) {
				if (bloqueParam.getTabla_input().getId_tablainput() != null
						&& !bloqueParam.getTabla_input().getId_tablainput().equals(0)) { // Si es cero es null!
					pstmt.setInt(1, bloqueParam.getTabla_input().getId_tablainput());
				} else {
					pstmt.setNull(1, java.sql.Types.INTEGER); // CodTablaInput en null
				}
				if (bloqueParam.getFiltro_tabla() != null) {
					pstmt.setString(2, bloqueParam.getFiltro_tabla());
				} else {
					pstmt.setNull(2, java.sql.Types.VARCHAR);
				}
			} else {
				pstmt.setNull(1, java.sql.Types.INTEGER); // CodTablaInput en null
				pstmt.setNull(2, java.sql.Types.VARCHAR); // Filtro tabla en null
			}
			pstmt.setDate(3, new Date(System.currentTimeMillis()));
			pstmt.setInt(4, bloqueParam.getId_mapeo_tabla());
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar la tabla input del mapeo con id " + bloqueParam.getId_mapeo_tabla()
					+ " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void editarCampoParametrizado(CampoParametrizado campoParam, Boolean flag_tabla_input,
			Integer id_mapeo_tabla) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "update HE_RegistroCampoBloque set CodCampo = ?, NumValor = ?, TxtValor = ?, "
				+ "FecActualizacionTabla = ? where CodCampoBloque = ? and CodMapeoTabla = ?";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);

			if (flag_tabla_input) {
				if (campoParam.getCampo_input().getId_campo() != null
						&& campoParam.getCampo_input().getId_campo() != 0) {
					pstmt.setInt(1, campoParam.getCampo_input().getId_campo());
				} else {
					pstmt.setNull(1, java.sql.Types.INTEGER); // NumValor en null
				}
				pstmt.setNull(2, java.sql.Types.INTEGER); // NumValor en null
				pstmt.setNull(3, java.sql.Types.VARCHAR); // txtValor en null
			} else {
				pstmt.setNull(1, java.sql.Types.INTEGER); // CodCampoInput en null
				if (campoParam.getCampo_bloque().getTipo_campo().getId_tipo_campo().equals(1)) { // Numerico
					if (campoParam.getNumValor() != null) {
						pstmt.setDouble(2, campoParam.getNumValor());
					} else {
						pstmt.setNull(2, java.sql.Types.INTEGER);
					}
					pstmt.setNull(3, java.sql.Types.VARCHAR); // txtValor en null

				} else if (campoParam.getCampo_bloque().getTipo_campo().getId_tipo_campo().equals(2)) { // Texto
					pstmt.setNull(2, java.sql.Types.INTEGER); // NumValor en null
					if (campoParam.getTxtValor() != null) {
						pstmt.setString(3, campoParam.getTxtValor());
					} else {
						pstmt.setNull(3, java.sql.Types.VARCHAR); // txtValor en null
					}
				}
			}

			pstmt.setDate(4, new Date(System.currentTimeMillis()));
			pstmt.setInt(5, campoParam.getCampo_bloque().getId_campo());
			pstmt.setInt(6, id_mapeo_tabla);
			pstmt.executeUpdate();

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar parametria del campo con id "
					+ campoParam.getCampo_bloque().getId_bloque() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	// --------- Parametrización de Registros en Variables----------------

	public CargaFichero crearCargaTablaInput(CargaFichero cargaFichero) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		cargaFichero = crearCargaTablaInput(cargaFichero, conn);
		cerrarConexion(conn);
		return cargaFichero;
	}

	private CargaFichero crearCargaTablaInput(CargaFichero cargaFichero, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		Integer ultimoOrden = null;
		AprovProcesado procesado = new AprovProcesado();
		Boolean flag_tiene_campos_calculados = false;
		String sql = "Insert into HE_TablaInput (CodTablaInput, CodAprovisionamiento, NumVersionMetadata, NbrMetadata, FlgNormalizada, FecActualizacionTabla)"
				+ " values (?,?,?,?,?,?)";
		try {
			// Agregamos el mapeo_cartera_fichero
			pstmt = conn.prepareStatement(sql);
			cargaFichero.getTablaInput().setId_tablainput(obtenerSiguienteId("SEQ_HE_TablaInput"));
			pstmt.setInt(1, cargaFichero.getTablaInput().getId_tablainput());
			pstmt.setInt(2, cargaFichero.getTablaInput().getId_aprovisionamiento());
			pstmt.setInt(3, cargaFichero.getTablaInput().getVersion_metadata());
			pstmt.setString(4, cargaFichero.getTablaInput().getNombre_metadata());
			pstmt.setBoolean(5, cargaFichero.getTablaInput().getFlag_normalizada());
			pstmt.setDate(6, new Date(System.currentTimeMillis()));

			pstmt.addBatch();
			pstmt.executeBatch();

			ejecutarCommit(conn);

			// ------- Lo quitamos del finally, para capturar los errores tambien
			// ----------- //

			crearTablaNoNormalizada(cargaFichero, conn);
			cargaFichero.setId_cargafichero(obtenerSiguienteId("seq_he_cargafichero"));

			// se identifica si hay campos calculados
			for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
				if (campo.getFlag_aprovisionar() && !campo.getFlag_campo_original()) {
					flag_tiene_campos_calculados = true;
				}
			}
			if (flag_tiene_campos_calculados) {
				crearCamposInput(cargaFichero, conn);
				crearTablaNoNormalizadaTemporalConCamposOriginales(cargaFichero);
				ultimoOrden = procesaraprovTemporal(cargaFichero, 0, conn);
				// insertarAprovConCamposCalculados(cargaFichero, conn);

				// ------- PARA CAMPOS CALC CON DEPENDENCIA -------
				// Recorremos los campos calculados ()
				for (CampoAprovisionamiento campoCalc : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
					if (!campoCalc.getFlag_campo_original()) {
						alterarTablaNoNormalizadaTemporalPorCampoCalculado(cargaFichero, campoCalc, conn);
						actualizarAprovPorCampoCalculado(cargaFichero, campoCalc, conn);
					}
				}
				insertarAprovConCamposCalculadosConDatos(cargaFichero, conn);
				// ----------------------------------------------

				borrarTablaNoNormalizadaTemporalConCamposOriginales(cargaFichero, false, conn);
			} else {
				procesado = procesaraprovNuevoNoNormalizado(cargaFichero, cargaFichero.getId_cargafichero(), conn);
				ultimoOrden = procesado.getNro_lineas();
			}
			cargaFichero.setError_campo(procesado.getError_campos());
			cargaFichero.setOrden_inicial(1);
			cargaFichero.setOrden_final(ultimoOrden - 1);
			cargaFichero = crearCargaFichero(cargaFichero, conn);

			return cargaFichero;
			// -----------------------------------------------------------------------------------------

		} catch (Exception ex) {
			ejecutarRollback(conn);

			// Borramos tabla temporal en caso falle (en caso exista)
			borrarTablaNoNormalizadaTemporalConCamposOriginales(cargaFichero, true, conn);

			// Borramos tabla principal en caso falle
			borrarTablaNoNormalizada(cargaFichero.getTablaInput(), true, conn);

			// Borramos la tabla de he_tablainput en caso falle (no se puede con el
			// rollback,
			// no se pueden crear campos input sin el commit de tabla input...)
			borrarTablaInputCargaFallida(cargaFichero.getTablaInput().getId_tablainput(), conn);

			LOGGER.error("Error al crear tabla input con aprov: "
					+ cargaFichero.getTablaInput().getId_aprovisionamiento() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			// ---- Quitamos logica del finally ------//
		}
	}

	public void borrarTablaInputCargaFallida(Integer id_tabla_input, Connection conn) {
		PreparedStatement pstmt = null;
		String sql = new String();

		sql = "DELETE from HE_TablaInput where CodTablaInput = ?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_tabla_input);
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);
			ejecutarCommit(conn);

		} catch (Exception ex) {
			// ejecutarRollback(conn);
			LOGGER.warn("Error al borrar la tabla input con id: " + id_tabla_input + " : " + ex.getMessage());
			// throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public CargaFichero insertarRegistrosCargaFichero(CargaFichero cargaFichero) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		cargaFichero = insertarRegistrosCargaFichero(cargaFichero, conn);
		cerrarConexion(conn);
		return cargaFichero;
	}

	private CargaFichero insertarRegistrosCargaFichero(CargaFichero cargaFichero, Connection conn) throws Exception {

		CargaFichero ultimaCarga = null;
		Integer ultimoOrden = null;
		Boolean flag_tiene_campos_calculados = false;
		long tiempoInicio = System.currentTimeMillis();
		LOGGER.info("Inicio de carga de ficheros: " + tiempoInicio);
		cargaFichero.getTablaInput().setFlag_normalizada(!cargaFichero.getTablaInput().getFlag_operacion());
		try {
			ultimaCarga = obtenerultimaCargaporTablaInput(cargaFichero.getTablaInput());

			if (ultimaCarga != null) {
				cargaFichero.setVersion_carga(ultimaCarga.getVersion_carga() + 1);
			} else {
				cargaFichero.setVersion_carga(1);
			}
			cargaFichero.setId_cargafichero(obtenerSiguienteId("seq_he_cargafichero"));

			for (CampoAprovisionamiento campo : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
				if (campo.getFlag_aprovisionar() && !campo.getFlag_campo_original()) {
					flag_tiene_campos_calculados = true;
				}
			}
			if (flag_tiene_campos_calculados) {
				crearTablaNoNormalizadaTemporalConCamposOriginales(cargaFichero, conn);
				// ultimoOrden = procesaraprovTemporal(cargaFichero,
				// ultimaCarga.getOrden_final(), conn);
				ultimoOrden = procesaraprovTemporal(cargaFichero, 0, conn);
				// insertarAprovConCamposCalculados(cargaFichero, conn);

				// ------- PARA CAMPOS CALC CON DEPENDENCIA -------
				// Recorremos los campos calculados ()
				for (CampoAprovisionamiento campoCalc : cargaFichero.getTablaInput().getCampos_aprovisionamiento()) {
					if (!campoCalc.getFlag_campo_original()) {
						alterarTablaNoNormalizadaTemporalPorCampoCalculado(cargaFichero, campoCalc, conn);
						actualizarAprovPorCampoCalculado(cargaFichero, campoCalc, conn);
					}
				}
				insertarAprovConCamposCalculadosConDatos(cargaFichero, conn);
				// ----------------------------------------------

				borrarTablaNoNormalizadaTemporalConCamposOriginales(cargaFichero, false, conn);
			} else {
				ultimoOrden = procesaraprovAntiguoNoNormalizado(cargaFichero, 0, conn);
			}
			// cargaFichero.setOrden_inicial(ultimaCarga.getOrden_final() + 1);
			cargaFichero.setOrden_inicial(0 + 1);
			cargaFichero.setOrden_final(ultimoOrden - 1);
			long tiempoFinal = System.currentTimeMillis();
			cargaFichero.setTiempoDeCarga((int) (tiempoFinal - tiempoInicio));
			cargaFichero = crearCargaFichero(cargaFichero, conn);
		} catch (Exception ex) {
			// Borramos tabla temporal en caso falle (en caso exista)
			borrarTablaNoNormalizadaTemporalConCamposOriginales(cargaFichero, true, conn);

			LOGGER.error("Error al insertar registros en tabla input con aprov: "
					+ cargaFichero.getTablaInput().getId_aprovisionamiento() + " : " + ex.getMessage());

			throw ex;
		} finally {

		}
		return cargaFichero;
	}

	public CargaFichero obtenerultimaCargaporTablaInput(TablaInput tablainput) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		CargaFichero ultimaCarga = obtenerultimaCargaporTablaInput(tablainput, conn);
		cerrarConexion(conn);
		return ultimaCarga;
	}

	public CargaFichero obtenerultimaCargaporTablaInput(TablaInput tablainput, Connection conn) throws Exception {
		ArrayList<CargaFichero> ultimas_cargas = new ArrayList<CargaFichero>();
		CargaFichero carga = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select CODCARGAFICHERO,NUMVERSIONCARGA,NUMORDENINICIAL,NUMORDENFINAL from HE_CARGAFICHERO where  CODTABLAINPUT = "
				+ tablainput.getId_tablainput()
				+ " and NUMVERSIONCARGA=(SELECT MAX(NUMVERSIONCARGA) from HE_CARGAFICHERO where CODTABLAINPUT = "
				+ tablainput.getId_tablainput() + ")";
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				carga = new CargaFichero();

				carga.setId_cargafichero(rs.getInt("CODCARGAFICHERO"));
				carga.setVersion_carga(rs.getInt("NUMVERSIONCARGA"));
				carga.setOrden_inicial(rs.getInt("NUMORDENINICIAL"));
				carga.setOrden_final(rs.getInt("NUMORDENFINAL"));
				ultimas_cargas.add(carga);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener la ultima Carga de Tabla Input con id : " + tablainput.getId_tablainput()
					+ " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		if (ultimas_cargas.size() > 0) {
			return ultimas_cargas.get(0);
		} else {
			LOGGER.warn("No se encontraron cargas para esa metadata, se ultima carga nula");
			return null;
		}

	}

	public CargaFichero crearCargaFichero(CargaFichero cargaFichero) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		CargaFichero cargaFicheroRet = crearCargaFichero(cargaFichero, conn);
		cerrarConexion(conn);
		return cargaFicheroRet;
	}

	private CargaFichero crearCargaFichero(CargaFichero cargaFichero, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String id_usuario_desenc = null;
		String sql = "Insert into HE_CargaFichero (CodCargaFichero, CodTablaInput, CodFecha,"
				+ " NbrCarga, NumVersionCarga, FecActualizacionTabla,NUMORDENINICIAL,NUMORDENFINAL,NbrRutaAprovisionamiento,FLGOFICIAL,TIPESTADOCARGA,FECCALCULORES, FECCARGA,CODUSUARIO,TIEMPODECARGA"
				+ ",TABLADESTINO,NUMEROCAMPOS) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			// Agregamos el mapeo_cartera_fichero
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, cargaFichero.getId_cargafichero());
			pstmt.setInt(2, cargaFichero.getTablaInput().getId_tablainput());
			pstmt.setInt(3, cargaFichero.getFecha().getId_fecha());
			pstmt.setString(4, cargaFichero.getNombre_carga());
			pstmt.setInt(5, cargaFichero.getVersion_carga());
			pstmt.setDate(6, new Date(System.currentTimeMillis()));
			pstmt.setInt(7, cargaFichero.getOrden_inicial());
			pstmt.setInt(8, cargaFichero.getOrden_final());
			pstmt.setString(9, cargaFichero.getTablaInput().getRuta());
			pstmt.setBoolean(10, cargaFichero.getFlag_oficial());
//			pstmt.setInt(11, cargaFichero.getTipoEstadoCarga());
			pstmt.setInt(11, 0);
			Calendar fecha_calculo = cargaFichero.getFecha_calculo();
			pstmt.setInt(12, fecha_calculo.get(Calendar.YEAR) * 10000 + (fecha_calculo.get(Calendar.MONTH) + 1) * 100
					+ fecha_calculo.get(Calendar.DAY_OF_MONTH));

			int time = (int) (new Date(System.currentTimeMillis()).getTime() / 1000);

			pstmt.setInt(13, time);

			if (developer) {
				pstmt.setString(14, cargaFichero.getCodUsuario());

			} else {
				id_usuario_desenc = AES.decrypt(cargaFichero.getCodUsuario(), Claves.claveEncLogueo);
				pstmt.setString(14, id_usuario_desenc);

			}

			pstmt.setInt(15, cargaFichero.getTiempoDeCarga() == null ? 0 : cargaFichero.getTiempoDeCarga());
			pstmt.setString(16, cargaFichero.getTablaDestino());
			pstmt.setInt(17, cargaFichero.getNumeroCampos() == null ? 0 : cargaFichero.getNumeroCampos());

			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear cargafichero: " + cargaFichero.getNombre_carga() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
		return cargaFichero;
	}

	// Editar Carga Fichero
	public void editarCargaFichero(CargaFichero cargaFichero) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		editarCargaFichero(cargaFichero, conn);
		cerrarConexion(conn);
	}

	private void editarCargaFichero(CargaFichero cargaFichero, Connection conn) throws Exception {

		PreparedStatement pstmt = null;
		String sql = "UPDATE he_cargafichero SET " + "	descomentariocalidad = ?, " + " flgoficial = ?, "
				+ " fecactualizaciontabla = ?, " + " tipestadocarga = ? " + "WHERE codcargafichero = ?";

		try {
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, cargaFichero.getComentario_calidad());
			pstmt.setBoolean(2, cargaFichero.getFlag_oficial());
			pstmt.setDate(3, new Date(System.currentTimeMillis()));
			pstmt.setInt(4, cargaFichero.getTipoEstadoCarga());
			pstmt.setInt(5, cargaFichero.getId_cargafichero());
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar la carga fichero con nombre: " + cargaFichero.getNombre_carga()
					+ " al aprovisionar / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public ArrayList<CargaFichero> obtenerCargasFicheroTotales() throws Exception {
		ArrayList<CargaFichero> cargasFichero = null;
		Connection conn = null;
		conn = obtenerConexion();
		cargasFichero = obtenerCargasFicheroTotales(conn);
		cerrarConexion(conn);
		return cargasFichero;
	}

	public ArrayList<CargaFichero> obtenerCargasFicheroTotales(Connection conn) throws Exception {
		ArrayList<CargaFichero> cargasFichero = new ArrayList<CargaFichero>();
		CargaFichero cargaFichero = null;

		Fecha fecha = null;
		TipoAmbito tipoAmbito = null;
		TablaInput tablaInput = null;
		ArrayList<CampoInput> campos_input = new ArrayList<CampoInput>();

		Statement stmt = null;
		ResultSet rs = null;
		String sql = "Select Cf.* , F.NUMFASE, ofrc.FLGAMBAR, ofrc.FLGROJO,"
				+ "case when Cf.NUMVERSIONCARGA=(Select Max(Cf_V2.Numversioncarga) " + " 	From He_Cargafichero Cf_V2 "
				+ " 	Where Cf_V2.Codtablainput=Cf.Codtablainput) " + " then 1 else 0 end as flag_ultimaCarga,"
				+ " F.Numcodmes, TA.CodTipoAmbito, TA.NbrTipoAmbito, " + " A.Nbrfichero, A.Flgcuentaoperacion, "
				+ " A.Codaprovisionamiento, " + " Tabi.Flgnormalizada, " + " Tabi.Codtablainput, "
				+ " Tabi.Numversionmetadata, Tabi.NbrMetadata" + " From He_Cargafichero Cf "
				+ " Left Join He_Fecha F On F.Codfecha= Cf.Codfecha "
				+ " Left Join Me_TipoAmbito TA On F.CodTipoAmbito = TA.CodTipoAmbito "
				+ " Left Join He_Tablainput Tabi On Tabi.Codtablainput=Cf.Codtablainput"
				+ " Left Join He_Aprovisionamiento A On A.Codaprovisionamiento=Tabi.Codaprovisionamiento "
				+ "left join (select max(CODCARGAFICHERO) as CODCARGAFICHERO,  sum(FLGAMBAR) as FLGAMBAR,"
				+ "sum(FLGROJO) as FLGROJO  from he_outputficheroreglacalidad ) ofrc  on Cf.CODCARGAFICHERO = ofrc.CODCARGAFICHERO "
				+ "order by cf.codcargafichero desc";
		try {
			stmt = crearStatement(conn);
			//LOGGER.info(sql);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				cargaFichero = new CargaFichero();
				cargaFichero.setId_cargafichero(rs.getInt("CODCARGAFICHERO"));
				cargaFichero.setNombre_carga(rs.getString("NBRCARGA"));

				cargaFichero.setFlag_oficial(rs.getBoolean("FLGOFICIAL"));
				cargaFichero.setOrden_inicial(rs.getInt("NUMORDENINICIAL"));
				cargaFichero.setOrden_final(rs.getInt("NUMORDENFINAL"));
				cargaFichero.setNombre_carga(rs.getString("NBRCARGA"));
				cargaFichero.setVersion_carga(rs.getInt("NUMVERSIONCARGA"));
				cargaFichero.setFlag_ultimaCarga(rs.getBoolean("flag_ultimaCarga"));
				cargaFichero.setTipoEstadoCarga(rs.getInt("TIPESTADOCARGA"));
				fecha = new Fecha();
				fecha.setCodmes(rs.getInt("numcodmes"));
				fecha.setId_fecha(rs.getInt("CODFECHA"));
				fecha.setFase(rs.getInt("NUMFASE"));
				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));

				cargaFichero.setTiempoDeCarga(rs.getInt("TIEMPODECARGA"));
				cargaFichero.setContadorAlertasAmarillas(rs.getInt("FLGAMBAR"));
				cargaFichero.setContadorAlertasRojas(rs.getInt("FLGROJO"));
				fecha.setTipo_ambito(tipoAmbito);
				cargaFichero.setFecha(fecha);
				cargaFichero.setFecha_carga(rs.getInt("FECCARGA"));
				cargaFichero.setCodUsuario(rs.getString("CODUSUARIO") == null ? " "
						: (rs.getString("CODUSUARIO").isEmpty() ? " " : rs.getString("CODUSUARIO")));
				cargaFichero.setNumeroCampos(rs.getInt("NUMEROCAMPOS"));
				cargaFichero.setTablaDestino(rs.getString("TABLADESTINO"));
				cargaFichero.setComentario_calidad(rs.getString("DesComentarioCalidad"));

				tablaInput = new TablaInput();
				tablaInput.setId_aprovisionamiento(rs.getInt("codaprovisionamiento"));
				tablaInput.setId_tablainput(rs.getInt("codtablainput"));
				tablaInput.setNombre_fichero(rs.getString("nbrfichero"));
				tablaInput.setVersion_metadata(rs.getInt("numversionmetadata"));
				tablaInput.setNombre_metadata(rs.getString("NbrMetadata"));
				tablaInput.setRuta(rs.getString("NBRRUTAAPROVISIONAMIENTO"));
				tablaInput.setFlag_normalizada(rs.getBoolean("Flgnormalizada"));
				tablaInput.setFlag_operacion(rs.getBoolean("FLGCUENTAOPERACION"));

				// campos_input = this.obtenerCamposporTablaInputVersion(tablaInput, conn);
				// tablaInput.setCampos_input(campos_input);
				cargaFichero.setTablaInput(tablaInput);

				// cargaFichero = actualizarEstadoCargaCalidadFichero(cargaFichero, conn);

				cargasFichero.add(cargaFichero);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener el total de cargas Fichero: " + "/ " + ex.getMessage());
			throw ex;
		} finally {
			// cargasFichero = actualizarEstadoCargaCalidad(cargasFichero, conn);
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return cargasFichero;
	}

	public void aprobarCargaFichero(CargaFichero cargaFichero) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		cargaFichero.setTipoEstadoCarga(2);
		editarCargaFichero(cargaFichero, conn);
		cerrarConexion(conn);
	}

	public void rechazarCargaFichero(CargaFichero cargaFichero) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		boolean esBorrable = validarFicheroEsBorrable(cargaFichero);
		if (esBorrable) {
			try {
				cargaFichero.setTipoEstadoCarga(3);
				editarCargaFichero(cargaFichero, conn);
				if (cargaFichero.getTablaInput().getFlag_normalizada()) {
				} else {
					eliminarRegitrosDeCargaNoNormalizada(cargaFichero, conn);
				}

				borrarRegistrosDeFicheroAprovisionado(cargaFichero, conn);


			} catch (Exception ex) {
				ejecutarRollback(conn);
				LOGGER.error("Error al borrar los registros de la carga: " + cargaFichero.getNombre_carga()
						+ " de la tabla " + cargaFichero.getTablaInput().getNombre_fichero() + " / " + ex.getMessage());
				throw ex;
			} finally {
				cerrarConexion(conn);
			}

		} else {
			throw new Exception("No se puede borrar el fichero porque está siendo utilizado");
		}
	}

	public void eliminarRegitrosDeCargaNoNormalizada(CargaFichero cargaFichero, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql_borrarRegistros = "DELETE FROM " + userName + ".HE_"
				+ cargaFichero.getTablaInput().getNombre_fichero() + "_V"
				+ cargaFichero.getTablaInput().getVersion_metadata() + " WHERE NumOrden >= ? AND NumOrden <= ?";

		try {
			pstmt = conn.prepareStatement(sql_borrarRegistros);
			pstmt.setInt(1, cargaFichero.getOrden_inicial());
			pstmt.setInt(2, cargaFichero.getOrden_final());
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar los registros de la carga: " + cargaFichero.getNombre_carga()
					+ " de la tabla " + cargaFichero.getTablaInput().getNombre_fichero() + " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public ArrayList<CargaFichero> actualizarEstadoCargaCalidad(ArrayList<CargaFichero> cargasFichero)
			throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		cargasFichero = actualizarEstadoCargaCalidad(cargasFichero, conn);
		cerrarConexion(conn);
		return cargasFichero;
	}

	public ArrayList<CargaFichero> actualizarEstadoCargaCalidad(ArrayList<CargaFichero> cargasFichero, Connection conn)
			throws Exception {
		ArrayList<CargaFichero> cargasFicheroActualizado = new ArrayList<CargaFichero>();
		CargaFichero cargaFichero = null;

		try {
			for (int i = 0; i < cargasFichero.size(); i++) {
				cargaFichero = actualizarEstadoCargaCalidadFichero(cargasFichero.get(i), conn);
				cargasFicheroActualizado.add(cargaFichero);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al actualizar el estado de las cargas:  " + ex.getMessage());
			throw ex;
		} finally {

		}

		return cargasFicheroActualizado;
	}

	public CargaFichero actualizarEstadoCargaCalidadFichero(CargaFichero cargaFichero, Connection conn)
			throws Exception {
		Fecha fecha = null;
		TipoAmbito tipoAmbito = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "Select cf.CODCARGAFICHERO, cf.TIPESTADOCARGA, ofrc.FLGAMBAR, ofrc.FLGROJO, oft.TOTALREGISTROS, "
				+ " cf.CodFecha, cf.NumCodMes, cf.NumFase, cf.CodTipoAmbito, cf.NbrTipoAmbito "
				+ " from  (select He_Cargafichero.CODCARGAFICHERO, He_Cargafichero.TIPESTADOCARGA, "
				+ " HE_Fecha.CodFecha, HE_Fecha.NumCodMes, HE_Fecha.NumFase, "
				+ " ME_TipoAmbito.CodTipoAmbito, ME_TipoAmbito.NbrTipoAmbito " + " from He_Cargafichero "
				+ " left join HE_Fecha on He_Cargafichero.CodFecha = HE_Fecha.CodFecha "
				+ " left join ME_TipoAmbito on HE_Fecha.CodTipoAmbito = ME_TipoAmbito.CodTipoAmbito "
				+ " where He_Cargafichero.CODCARGAFICHERO =" + cargaFichero.getId_cargafichero() + ") cf left join "
				+ " (select max(CODCARGAFICHERO) as CODCARGAFICHERO, " + " sum(FLGAMBAR) as FLGAMBAR, "
				+ " sum(FLGROJO) as FLGROJO " + " from he_outputficheroreglacalidad " + "	where codcargafichero = "
				+ cargaFichero.getId_cargafichero() + ") ofrc " + " on cf.CODCARGAFICHERO = ofrc.CODCARGAFICHERO "
				+ " left join (select CODCARGAFICHERO, NumValor as TOTALREGISTROS from he_outputficherototal where TipResultadoFinal=1 and CODCARGAFICHERO = "
				+ +cargaFichero.getId_cargafichero() + ") oft on cf.CODCARGAFICHERO = oft.CODCARGAFICHERO ";
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				cargaFichero.setId_cargafichero(rs.getInt("CODCARGAFICHERO"));
				cargaFichero.setTipoEstadoCarga(rs.getInt("TIPESTADOCARGA"));
				cargaFichero.setContadorAlertasAmarillas(rs.getInt("FLGAMBAR"));
				cargaFichero.setContadorAlertasRojas(rs.getInt("FLGROJO"));
				cargaFichero.setContadorRegistros(rs.getInt("TOTALREGISTROS"));

				fecha = new Fecha();
				fecha.setId_fecha(rs.getInt("CodFecha"));
				fecha.setCodmes(rs.getInt("NumCodMes"));
				fecha.setFase(rs.getInt("NumFase"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				fecha.setTipo_ambito(tipoAmbito);

				cargaFichero.setFecha(fecha);
			}
		} catch (Exception ex) {
			LOGGER.error(
					"Error al actualizar el estado de la carga: " + cargaFichero.getNombre_carga() + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return cargaFichero;
	}

	public ArrayList<AjusteFilaInput> obtenerRegistrosdeCargaFichero(CargaFichero cargaFichero) throws Exception {
		ArrayList<AjusteFilaInput> registrosdeCargaFichero = null;
		Connection conn = null;
		conn = obtenerConexion();
		registrosdeCargaFichero = obtenerRegistrosdeCargaFichero(cargaFichero, conn);
		cerrarConexion(conn);
		return registrosdeCargaFichero;
	}

	public ArrayList<AjusteFilaInput> obtenerRegistrosdeCargaFichero(CargaFichero cargaFichero, Connection conn)
			throws Exception {
		ArrayList<AjusteFilaInput> registrosdeCargaFichero = new ArrayList<AjusteFilaInput>();
		AjusteFilaInput registrodeCargaFichero = null;
		AjusteCampoInput campodeRegistro = null;
		Statement stmt = null;
		ResultSet rs = null;
		Integer numeroDeRegistros = null;
		String sql = "select * " + "from " + userName + ".he_" + cargaFichero.getTablaInput().getNombre_fichero() + "_v"
				+ cargaFichero.getTablaInput().getVersion_metadata() + " where CODCARGAFICHERO="
				+ cargaFichero.getId_cargafichero() + cargaFichero.getFiltroAjuste();
		String sqlCount = "select count(*) as count " + "from " + userName + ".he_"
				+ cargaFichero.getTablaInput().getNombre_fichero() + "_v"
				+ cargaFichero.getTablaInput().getVersion_metadata() + " where CODCARGAFICHERO="
				+ cargaFichero.getId_cargafichero() + cargaFichero.getFiltroAjuste();
		numeroDeRegistros = obtenerCantidadRegistrosCargaFicheroConFiltro(sqlCount, conn);
		if (numeroDeRegistros > 100) {
			throw new CustomException(
					"La busqueda cuenta con un total de:" + numeroDeRegistros + ", mayor al límite 100");
		}
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				registrodeCargaFichero = new AjusteFilaInput();
				registrodeCargaFichero.setId_cargaFichero(cargaFichero.getId_cargafichero());
				registrodeCargaFichero.setOrden(rs.getInt("NUMORDEN"));
				registrodeCargaFichero.setTabla("he_" + cargaFichero.getTablaInput().getNombre_fichero() + "_v"
						+ cargaFichero.getTablaInput().getVersion_metadata());
				for (CampoInput campoInput : cargaFichero.getTablaInput().getCampos_input()) {
					campodeRegistro = new AjusteCampoInput();
					campodeRegistro.setNombre_campo(campoInput.getNombre_campo());
					if (campoInput.getTipo_campo().getId_tipo_campo().equals(1)) {
						campodeRegistro.setValor_numero(rs.getDouble(campodeRegistro.getNombre_campo()));
						campodeRegistro.setFlag_numero(true);
					} else {
						campodeRegistro.setValor_texto(rs.getString(campodeRegistro.getNombre_campo()));
						campodeRegistro.setFlag_texto(true);
					}
					registrodeCargaFichero.getAjustesCampoInput().add(campodeRegistro);
				}
				registrosdeCargaFichero.add(registrodeCargaFichero);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los registros de Carga Fichero con nombre: " + cargaFichero.getNombre_carga()
					+ "/ " + ex.getMessage());

			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return registrosdeCargaFichero;
	}

	public Integer obtenerCantidadRegistrosCargaFicheroConFiltro(String sqlCount, Connection conn) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		Integer count = null;
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sqlCount);
			while (rs.next()) {
				count = rs.getInt("count");
			}
		} catch (Exception ex) {
			if (ex.getMessage().contains("missing expression")) {
				throw new Exception("Falta ingresar valor en el filtro, no se ha ingresado ningún valor numérico. ");

			} else if ((ex.getMessage().contains("invalid identifier"))) {
				throw new Exception("No se ha ingresado un valor numérico. ");
			} else {
				LOGGER.error("Error al hacer el conteo de la Query: " + sqlCount + "/ " + ex.getMessage());
				throw ex;
			}
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return count;
	}

	public void editarRegistrosdeCargaFichero(ArrayList<AjusteFilaInput> ajustesFilaInput) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		for (AjusteFilaInput ajusteFilaInput : ajustesFilaInput) {
			editarRegistrodeCargaFichero(ajusteFilaInput, conn);
		}
		cerrarConexion(conn);
	}

	public void editarRegistrodeCargaFichero(AjusteFilaInput ajusteFilaInput) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		editarRegistrodeCargaFichero(ajusteFilaInput, conn);
		cerrarConexion(conn);
	}

	private void editarRegistrodeCargaFichero(AjusteFilaInput ajusteFilaInput, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql_edicion_campos = "";

		for (int index = 0; index < ajusteFilaInput.getAjustesCampoInput().size(); index++) {
			if (index < ajusteFilaInput.getAjustesCampoInput().size() - 1) {
				sql_edicion_campos += ajusteFilaInput.getAjustesCampoInput().get(index).getNombre_campo() + "= ?,";
			} else {
				sql_edicion_campos += ajusteFilaInput.getAjustesCampoInput().get(index).getNombre_campo() + "= ?";
			}
		}

		String sql_editarRegistro = "UPDATE " + userName + "." + ajusteFilaInput.getTabla() + " set "
				+ sql_edicion_campos + " WHERE NUMORDEN=? and CODCARGAFICHERO=?";

		try {
			pstmt = conn.prepareStatement(sql_editarRegistro);

			for (int index = 0; index < ajusteFilaInput.getAjustesCampoInput().size(); index++) {
				if (ajusteFilaInput.getAjustesCampoInput().get(index).getFlag_numero()) {
					pstmt.setDouble(index + 1, ajusteFilaInput.getAjustesCampoInput().get(index).getValor_numero());
				} else {
					pstmt.setString(index + 1, ajusteFilaInput.getAjustesCampoInput().get(index).getValor_texto());
				}
			}
			pstmt.setInt(ajusteFilaInput.getAjustesCampoInput().size() + 1, ajusteFilaInput.getOrden());
			pstmt.setInt(ajusteFilaInput.getAjustesCampoInput().size() + 2, ajusteFilaInput.getId_cargaFichero());

			pstmt.executeUpdate();

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar la fila con orden : " + ajusteFilaInput.getOrden() + " de la tabla "
					+ ajusteFilaInput.getTabla() + " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void crearRegistrodeCargaFichero(AjusteFilaInput ajusteFilaInput) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearRegistrodeCargaFichero(ajusteFilaInput, conn);
		cerrarConexion(conn);
	}

	private void crearRegistrodeCargaFichero(AjusteFilaInput ajusteFilaInput, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql_registro_nombres_campos = "";
		String sql_registro_registros_campos = "";
		for (int index = 0; index < ajusteFilaInput.getAjustesCampoInput().size(); index++) {
			if (index < ajusteFilaInput.getAjustesCampoInput().size() - 1) {
				sql_registro_nombres_campos += ajusteFilaInput.getAjustesCampoInput().get(index).getNombre_campo()
						+ ",";
				sql_registro_registros_campos += "?,";
			} else {
				sql_registro_nombres_campos += ajusteFilaInput.getAjustesCampoInput().get(index).getNombre_campo();
				sql_registro_registros_campos += "?";
			}
		}
		String sql_crearRegistro = "INSERT INTO " + userName + "." + ajusteFilaInput.getTabla()
				+ " (NumOrden,CODCARGAFICHERO,FECCALCULORES, " + sql_registro_nombres_campos + ") values (?,?,?,"
				+ sql_registro_registros_campos + ")";
		try {
			pstmt = conn.prepareStatement(sql_crearRegistro);
			pstmt.setInt(1, ajusteFilaInput.getOrden());
			pstmt.setInt(2, ajusteFilaInput.getId_cargaFichero());
			pstmt.setInt(3, 0);
			for (int index = 0; index < ajusteFilaInput.getAjustesCampoInput().size(); index++) {
				if (ajusteFilaInput.getAjustesCampoInput().get(index).getFlag_numero()) {
					pstmt.setDouble(index + 4, ajusteFilaInput.getAjustesCampoInput().get(index).getValor_numero());
				} else {
					pstmt.setString(index + 4, ajusteFilaInput.getAjustesCampoInput().get(index).getValor_texto());
				}
			}
			pstmt.executeUpdate();
			ejecutarCommit(conn);
			actualizarOrdenFinalDeCarga(ajusteFilaInput.getId_cargaFichero(), ajusteFilaInput.getOrden(), conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al insertar registro en fila con orden : " + ajusteFilaInput.getOrden()
					+ " de la tabla " + ajusteFilaInput.getTabla() + " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	private void actualizarOrdenFinalDeCarga(Integer id_cargaFichero, Integer orden, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "UPDATE he_cargafichero SET numordenfinal = ? WHERE codcargafichero = ?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, orden);
			pstmt.setInt(2, id_cargaFichero);
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar la actualización de carga con id: " + id_cargaFichero + " con el orden: "
					+ orden + " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public void borrarRegistrosdeCargaFichero(ArrayList<AjusteFilaInput> ajustesFilaInput) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		for (AjusteFilaInput ajusteFilaInput : ajustesFilaInput) {
			borrarRegistrodeCargaFichero(ajusteFilaInput, conn);
		}
		cerrarConexion(conn);
	}

	public void borrarRegistrodeCargaFichero(AjusteFilaInput ajusteFilaInput) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		borrarRegistrodeCargaFichero(ajusteFilaInput, conn);
		cerrarConexion(conn);
	}

	private void borrarRegistrodeCargaFichero(AjusteFilaInput ajusteFilaInput, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql_borrarRegistro = "DELETE FROM " + userName + "." + ajusteFilaInput.getTabla()
				+ " WHERE NumOrden = ? and CODCARGAFICHERO=?";

		try {
			pstmt = conn.prepareStatement(sql_borrarRegistro);
			pstmt.setInt(1, ajusteFilaInput.getOrden());
			pstmt.setInt(2, ajusteFilaInput.getId_cargaFichero());
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar la fila con orden : " + ajusteFilaInput.getOrden() + " de la tabla "
					+ ajusteFilaInput.getTabla() + " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public ArrayList<TablaInput> obtenerTablasInput() throws Exception {
		Connection conn = null;
		ArrayList<TablaInput> tablas_input = new ArrayList<TablaInput>();
		conn = obtenerConexion();
		tablas_input = obtenerTablasInput(conn);
		cerrarConexion(conn);
		return tablas_input;
	}

	public ArrayList<TablaInput> obtenerTablasInput(Connection conn) throws Exception {
		ArrayList<TablaInput> tablas_input = new ArrayList<TablaInput>();
		ArrayList<CampoInput> campos_input = null;
		TablaInput tabla = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select distinct CodTablaInput,NumVersionMetadata,NbrMetadata,FlgAjustado from HE_TablaInput";
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tabla = new TablaInput();

				tabla.setId_tablainput(rs.getInt("CodTablaInput"));
				tabla.setVersion_metadata(rs.getInt("NumVersionMetadata"));
				tabla.setNombre_metadata(rs.getString("NbrMetaData"));
				tabla.setFlag_ajustado(rs.getBoolean("FlgAjustado"));

				campos_input = this.obtenerCamposporTablaInputVersion(tabla, conn);
				tabla.setCampos_input(campos_input);
				tablas_input.add(tabla);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las tablas input: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return tablas_input;
	}

	public ArrayList<TablaInput> obtenerTablasInputPorIdCartera(Integer id_cartera) throws Exception {
		Connection conn = null;
		ArrayList<TablaInput> tablas_input = new ArrayList<TablaInput>();
		conn = obtenerConexion();
		tablas_input = obtenerTablasInputPorIdCartera(conn, id_cartera);
		cerrarConexion(conn);
		return tablas_input;
	}

	public ArrayList<TablaInput> obtenerTablasInputPorIdCartera(Connection conn, Integer id_cartera) throws Exception {
		ArrayList<TablaInput> tablas_input = new ArrayList<TablaInput>();
		ArrayList<CampoInput> campos_input = null;
		TablaInput tabla = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select distinct tabi.CodTablaInput,tabi.CODAPROVISIONAMIENTO,tabi.NumVersionMetadata,tabi.NbrMetadata,tabi.FlgAjustado, "
				+ "a.nbrfichero from HE_TablaInput tabi "
				+ "left join he_aprovisionamiento a on a.codaprovisionamiento= tabi.codaprovisionamiento "
				+ "left join he_mapeoficherocartera mfc on  mfc.codaprovisionamiento =  a.codaprovisionamiento "
				+ "where mfc.codcartera=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_cartera);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				tabla = new TablaInput();

				tabla.setId_tablainput(rs.getInt("CodTablaInput"));
				tabla.setVersion_metadata(rs.getInt("NumVersionMetadata"));
				tabla.setNombre_metadata(rs.getString("NbrMetaData"));
				tabla.setFlag_ajustado(rs.getBoolean("FlgAjustado"));
				tabla.setNombre_fichero(rs.getString("nbrfichero"));
				tabla.setId_aprovisionamiento(rs.getInt("CODAPROVISIONAMIENTO"));

				campos_input = this.obtenerCamposporTablaInputVersion(tabla, conn);
				tabla.setCampos_input(campos_input);
				tablas_input.add(tabla);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las tablas input por cartera: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return tablas_input;
	}

	public ArrayList<ResultadoVariableMacro> obtenerResultadosVariablesMacrosPorEstructura(Integer id_metodologia)
			throws Exception {
		Connection conn = null;
		ArrayList<ResultadoVariableMacro> resultados = new ArrayList<ResultadoVariableMacro>();
		conn = obtenerConexion();
		resultados = obtenerResultadosVariablesMacrosPorEstructura(conn, id_metodologia);
		cerrarConexion(conn);
		return resultados;
	}

	public ArrayList<ResultadoVariableMacro> obtenerResultadosVariablesMacrosPorEstructura(Connection conn,
			Integer id_metodologia) throws Exception {
		ArrayList<ResultadoVariableMacro> resultados = new ArrayList<ResultadoVariableMacro>();
		ResultadoVariableMacro resultado = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "Select * From He_Outputmodelomacro2 Where Codmetodologia=? And "
				+ "Fecejecucion=(Select Max(Fecejecucion) From He_Outputmodelomacro2 Where Codmetodologia=?)";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_metodologia);
			pstmt.setInt(2, id_metodologia);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				resultado = new ResultadoVariableMacro();
				resultado.setId_metodologia(rs.getInt("CODMETODOLOGIA"));
				resultado.setFecha_ejecucion(rs.getInt("FECEJECUCION"));
				resultado.setNumPDBase(rs.getInt("NUMPDBASE"));
				resultado.setNumPDOptimista(rs.getInt("NUMPDOPTIMISTA"));
				resultado.setNumPDAdverso(rs.getInt("NUMPDADVERSO"));
				resultado.setFecha_proyeccion(rs.getInt("FECPROYECCION"));
				resultados.add(resultado);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los resultados de variables macro de primeros pasos: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return resultados;
	}

	public ArrayList<TablaInput> obtenerTablasInputPorAprov(Integer id_aprov) throws Exception {
		Connection conn = null;
		ArrayList<TablaInput> tablas_input = new ArrayList<TablaInput>();
		conn = obtenerConexion();
		tablas_input = obtenerTablasInputPorAprov(id_aprov, conn);
		cerrarConexion(conn);
		return tablas_input;
	}

	public ArrayList<TablaInput> obtenerTablasInputPorAprov(Integer id_aprov, Connection conn) throws Exception {
		ArrayList<TablaInput> tablas_input = new ArrayList<TablaInput>();
		ArrayList<CampoInput> campos_input = null;
		TablaInput tabla = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "Select Tabi.Codtablainput,Tabi.Numversionmetadata,Tabi.Nbrmetadata,Tabi.Flgajustado,Tabi.Flgnormalizada, Aprov.Nbrfichero "
				+ "From He_Tablainput Tabi "
				+ "Left Join He_Aprovisionamiento Aprov On Aprov.Codaprovisionamiento=Tabi.Codaprovisionamiento "
				+ "where Aprov.Codaprovisionamiento=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_aprov);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				tabla = new TablaInput();

				tabla.setId_tablainput(rs.getInt("CodTablaInput"));
				tabla.setVersion_metadata(rs.getInt("NumVersionMetadata"));
				tabla.setNombre_metadata(rs.getString("NbrMetaData"));
				tabla.setFlag_ajustado(rs.getBoolean("FlgAjustado"));
				tabla.setFlag_normalizada(rs.getBoolean("FLGNORMALIZADA"));
				tabla.setNombre_fichero(rs.getString("nbrfichero"));
				campos_input = this.obtenerCamposporTablaInputVersion(tabla, conn);
				tabla.setCampos_input(campos_input);
				tablas_input.add(tabla);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las tablas input de aprov: " + id_aprov + " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return tablas_input;
	}

	public ArrayList<TablaInput> obtenerTablasInputUltVerMetadataPorAprov(Integer id_aprov) throws Exception {
		Connection conn = null;
		ArrayList<TablaInput> tablas_input = new ArrayList<TablaInput>();
		conn = obtenerConexion();
		tablas_input = obtenerTablasInputUltVerMetadataPorAprov(id_aprov, conn);
		cerrarConexion(conn);
		return tablas_input;
	}

	public ArrayList<TablaInput> obtenerTablasInputUltVerMetadataPorAprov(Integer id_aprov, Connection conn)
			throws Exception {
		ArrayList<TablaInput> tablas_input = new ArrayList<TablaInput>();
		ArrayList<CampoInput> campos_input = null;
		TablaInput tabla = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "SELECT CodTablaInput,NumVersionMetadata,NbrMetadata,FlgAjustado from HE_TablaInput where CodAprovisionamiento=? "
				+ " and numversionmetadata=(SELECT MAX(NumVersionMetadata) from HE_TablaInput where CodAprovisionamiento=?)";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_aprov);
			pstmt.setInt(2, id_aprov);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				tabla = new TablaInput();

				tabla.setId_tablainput(rs.getInt("CodTablaInput"));
				tabla.setVersion_metadata(rs.getInt("NumVersionMetadata"));
				tabla.setNombre_metadata(rs.getString("NbrMetaData"));
				tabla.setFlag_ajustado(rs.getBoolean("FlgAjustado"));

				campos_input = this.obtenerCamposporTablaInputVersion(tabla, conn);
				tabla.setCampos_input(campos_input);
				tablas_input.add(tabla);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las tablas input de aprov: " + id_aprov + " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return tablas_input;
	}

	public ArrayList<CampoInput> obtenerCamposporTablaInputVersion(TablaInput tablainput) throws Exception {
		Connection conn = null;
		ArrayList<CampoInput> campos_input = new ArrayList<CampoInput>();
		conn = obtenerConexion();
		campos_input = obtenerCamposporTablaInputVersion(tablainput, conn);
		cerrarConexion(conn);
		return campos_input;
	}

	public ArrayList<CampoInput> obtenerCamposporTablaInputVersion(TablaInput tablainput, Connection conn)
			throws Exception {
		ArrayList<CampoInput> campos_input = new ArrayList<CampoInput>();
		CampoInput campo = null;
		TipoCampo tipoCampo = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select CI.* from HE_CampoInput CI join HE_TablaInput TI on CI.CodTablaInput = TI.CodTablaInput"
				+ " where CI.CodTablaInput=? and TI.NumVersionMetadata=? " + " order by CodCampo";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, tablainput.getId_tablainput());
			pstmt.setInt(2, tablainput.getVersion_metadata());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				campo = new CampoInput();
				tipoCampo = new TipoCampo();

				campo.setId_campo(rs.getInt("CodCampo"));
				campo.setId_tabla(rs.getInt("CodTablaInput"));
				tipoCampo.setId_tipo_campo(rs.getInt("CodTipoCampo"));
				campo.setNombre_campo(rs.getString("NbrCampo"));
				campo.setAncho_campo(rs.getInt("NumAncho"));

				campo.setTipo_campo(tipoCampo);
				campos_input.add(campo);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los campos input de la tabla con id: " + tablainput.getId_tablainput()
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return campos_input;
	}

	// --------------------- Parametrización Staging ----------------------
	// --------------------- Made by Yisus ----------------------
	public void editarStagingCarteraVersion(CarteraVersion_staging cartera_version) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		editarStagingCarteraVersion(cartera_version, conn);
		cerrarConexion(conn);
	}

	public void editarStagingCarteraVersion(CarteraVersion_staging cartera_version, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sqlUpdateCarteraVersion = "update HE_CarteraVersion set FlgStageInterno= ?, FlgStageExterno= ?,"
				+ " FlgStageSensibilidad= ?, NumMesesSensibilidad= ?, FecActualizacionTabla=?, CODTABLAINPUTEXP=? where CodCarteraVersion=?";
		try {
			pstmt = conn.prepareStatement(sqlUpdateCarteraVersion);
			pstmt.setBoolean(1, cartera_version.getStage_interno());
			pstmt.setBoolean(2, cartera_version.getStage_externo());
			pstmt.setBoolean(3, cartera_version.getStage_sensibilidad());
			pstmt.setInt(4, cartera_version.getMeses_sensibilidad());
			pstmt.setDate(5, new Date(System.currentTimeMillis()));
			if (cartera_version.getTablaInputExpresiones() == null
					|| cartera_version.getTablaInputExpresiones().getId_tablainput() == 0) {
				pstmt.setNull(6, Types.INTEGER);
			} else {
				pstmt.setInt(6, cartera_version.getTablaInputExpresiones().getId_tablainput());
			}
			pstmt.setInt(7, cartera_version.getId_cartera_version());
			pstmt.executeUpdate();
			this.editarExpresionesStaging(conn, cartera_version.getExpresionesStaging());
			this.editarReglasStaging(conn, cartera_version.getReglasStaging());
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al editar la cartera version con id " + cartera_version.getId_cartera_version() + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}

	}

	// --------------------- Parametrización Expresiones--------------------
	public void crearExpresionStaging(Expresion_Staging expresion) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearExpresionStaging(conn, expresion);
		cerrarConexion(conn);
	}

	public void crearExpresionStaging(Connection conn, Expresion_Staging expresion) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "INSERT INTO HE_ExpresionStaging(CodExpresion, CodCarteraVersion, NbrExpresion, NbrFormulaInput,"
				+ " NbrFormulaSql,FlgEditar, FecActualizacionTabla) VALUES (?,?,?,?,?,?,?)";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, expresion.getId_expresion());
			pstmt.setInt(2, expresion.getId_cartera_version());
			pstmt.setString(3, expresion.getNombre_expresion());
			pstmt.setString(4, expresion.getFormula_input());
			pstmt.setString(5, expresion.getFormula_sql());
			pstmt.setBoolean(6, expresion.getFlag_editar());
			pstmt.setDate(7, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar una expresion_Staging:" + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public ArrayList<Expresion_Staging> obtenerExpresionesStagingPorCarteraVersion(Integer id_cartera_version)
			throws Exception {
		Connection conn = null;
		ArrayList<Expresion_Staging> ans = new ArrayList<Expresion_Staging>();
		conn = obtenerConexion();
		ans = obtenerExpresionesStagingPorCarteraVersion(id_cartera_version, conn);
		cerrarConexion(conn);
		return ans;
	}

	public ArrayList<Expresion_Staging> obtenerExpresionesStagingPorCarteraVersion(Integer id_cartera_version,
			Connection conn) throws Exception {
		ArrayList<Expresion_Staging> expresiones = new ArrayList<Expresion_Staging>();
		Expresion_Staging expresion = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select * from HE_ExpresionStaging where CodCarteraVersion=? " + " order by CodExpresion";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_cartera_version);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				expresion = new Expresion_Staging();
				expresion.setId_expresion(rs.getInt("CodExpresion"));
				expresion.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				expresion.setNombre_expresion(rs.getString("NbrExpresion"));
				expresion.setFormula_input(rs.getString("NbrFormulaInput"));
				expresion.setFormula_sql(rs.getString("NbrFormulaSql"));
				expresion.setFlag_editar(rs.getBoolean("FLGEDITAR"));
				expresiones.add(expresion);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las expresiones de staging: " + id_cartera_version + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return expresiones;
	}

	public void editarExpresionesStaging(ArrayList<Expresion_Staging> expresiones) throws Exception {
		if (expresiones.size() > 0) {
			Connection conn = null;
			conn = obtenerConexion();
			editarExpresionesStaging(conn, expresiones);
			cerrarConexion(conn);
		}
	}

	public void editarExpresionesStaging(Connection conn, ArrayList<Expresion_Staging> expresiones) throws Exception {
		if (expresiones.size() > 0) {
			Integer id_versionCartera = expresiones.get(0).getId_cartera_version();
			// We first will delete all the expresiones that exist for the current
			// Cartera_Version
			this.borrarExpresionesStagingporIdVersionCartera(conn, id_versionCartera);
			for (Expresion_Staging expresion : expresiones) {
				this.crearExpresionStaging(conn, expresion);
			}
		}
	}

	public void borrarExpresionesStagingporIdVersionCartera(Integer id_versionCartera) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		borrarExpresionesStagingporIdVersionCartera(conn, id_versionCartera);
		cerrarConexion(conn);
	}

	public void borrarExpresionesStagingporIdVersionCartera(Connection conn, Integer id_versionCartera)
			throws Exception {
		PreparedStatement pstmt = null;
		String sql = "DELETE FROM HE_ExpresionStaging WHERE CodCarteraVersion = ?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_versionCartera);
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar las expresiones de la carteraVersion con id: " + id_versionCartera + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	// --------------------- Parametrización Reglas----------------------
	public void crearReglaStaging(Regla_Staging regla) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		crearReglaStaging(conn, regla);
		cerrarConexion(conn);
	}

	public void crearReglaStaging(Connection conn, Regla_Staging regla) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "insert into HE_ReglaStaging(CodRegla, CodCarteraVersion, NbrRegla, NbrFormulaInput, NbrFormulaSql,"
				+ " CodNodoCumple, CodNodoNoCumple, FlgStageNodoCumple, FlgStageNodoNoCumple, NumOrden, FecActualizacionTabla)"
				+ " values(?,?,?,?,?,?,?,?,?,?,?)";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, regla.getId_regla());
			pstmt.setInt(2, regla.getId_cartera_version());
			pstmt.setString(3, regla.getNombre_regla());
			pstmt.setString(4, regla.getFormula_input());
			pstmt.setString(5, regla.getFormula_sql());
			pstmt.setInt(6, regla.getId_nodo_cumple());
			pstmt.setInt(7, regla.getId_nodo_no_cumple());
			pstmt.setBoolean(8, regla.getFlag_stage_nodo_cumple());
			pstmt.setBoolean(9, regla.getFlag_stage_nodo_no_cumple());
			pstmt.setInt(10, regla.getOrden());
			pstmt.setDate(11, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar una regla_Staging:" + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	public ArrayList<Regla_Staging> obtenerReglasStagingPorCarteraVersion(Integer id_cartera_version) throws Exception {
		Connection conn = null;
		ArrayList<Regla_Staging> ans = new ArrayList<Regla_Staging>();
		conn = obtenerConexion();
		ans = obtenerReglasStagingPorCarteraVersion(id_cartera_version, conn);
		cerrarConexion(conn);
		return ans;
	}

	public ArrayList<Regla_Staging> obtenerReglasStagingPorCarteraVersion(Integer id_cartera_version, Connection conn)
			throws Exception {
		ArrayList<Regla_Staging> reglas = new ArrayList<Regla_Staging>();
		Regla_Staging regla = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select * from HE_ReglaStaging where CodCarteraVersion=?" + " order by NumOrden";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_cartera_version);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				regla = new Regla_Staging();
				regla.setId_regla(rs.getInt("CodRegla"));
				regla.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				regla.setNombre_regla(rs.getString("NbrRegla"));
				regla.setFormula_input(rs.getString("NbrFormulaInput"));
				regla.setFormula_sql(rs.getString("NbrFormulaSql"));
				regla.setId_nodo_cumple(rs.getInt("CodNodoCumple"));
				regla.setId_nodo_no_cumple(rs.getInt("CodNodoNoCumple"));
				regla.setFlag_stage_nodo_cumple(rs.getBoolean("FlgStageNodoCumple"));
				regla.setFlag_stage_nodo_no_cumple(rs.getBoolean("FlgStageNodoNoCumple"));
				regla.setOrden(rs.getInt("NumOrden"));
				reglas.add(regla);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las reglas de staging de la carteraVersion con id: " + id_cartera_version
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return reglas;
	}

	public void editarReglasStaging(ArrayList<Regla_Staging> reglas) throws Exception {
		if (reglas.size() > 0) {
			Connection conn = null;
			conn = obtenerConexion();
			editarReglasStaging(conn, reglas);
			cerrarConexion(conn);
		}
	}

	public void editarReglasStaging(Connection conn, ArrayList<Regla_Staging> reglas) throws Exception {
		if (reglas.size() > 0) {
			Integer id_cartera_version = reglas.get(0).getId_cartera_version();
			// We first will delete all the reglas that exist for the current
			// Cartera_Version
			try {
				this.borrarReglasStagingPorCarteraVersion(id_cartera_version, conn);
				for (Regla_Staging regla : reglas) {
					this.crearReglaStaging(conn, regla);
				}
			} catch (Exception ex) {
				LOGGER.error("Error con la edición de las reglas de la carteraVersion id: " + id_cartera_version + " : "
						+ ex.getMessage());
				throw ex;
			} finally {
			}
		}
	}

	public void borrarReglasStagingPorCarteraVersion(Integer id_cartera_version) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();
		borrarReglasStagingPorCarteraVersion(id_cartera_version, conn);
		cerrarConexion(conn);
	}

	public void borrarReglasStagingPorCarteraVersion(Integer id_cartera_version, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql = "DELETE FROM HE_ReglaStaging WHERE CodCarteraVersion = ?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_cartera_version);
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar las reglas de la carteraVersion con id: " + id_cartera_version + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

	// -------------------------------------------------------------------
	// ----------------------------TAREAS-----------------------------
	// -------------------------------------------------------------------

	public void crearTarea(Tarea tarea) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Insert into HE_Tarea(CodTarea, NbrTarea, NbrTareaCorto, FecInicio, FecLimite,"
				+ " FlgFinalizado, CodTipoAmbito, FecActualizacionTabla) values (?,?,?,?,?,?,?,?)";
		try {
			conn = obtenerConexion();

			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, obtenerSiguienteId("seq_he_tarea"));
			pstmt.setString(2, tarea.getNombre_tarea());
			pstmt.setString(3, tarea.getNombre_corto());
			pstmt.setDate(4, tarea.getFecha_inicio());
			pstmt.setDate(5, tarea.getFecha_limite());
			pstmt.setBoolean(6, false); // Crea la tarea sin estado finalizado por defecto
			pstmt.setInt(7, tarea.getTipo_ambito().getId_tipo_ambito());
			pstmt.setDate(8, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear una tarea: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void borrarTarea(Integer id_tarea) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "DELETE from HE_Tarea where CodTarea=?";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_tarea);
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar tarea con id: " + id_tarea + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void editarTarea(Tarea tarea) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Update HE_Tarea set NbrTarea = ?, NbrTareaCorto= ?, FecInicio = ?,"
				+ " FecLimite = ?, FlgFinalizado= ?, FecActualizacionTabla=? where CodTarea= ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, tarea.getNombre_tarea());
			pstmt.setString(2, tarea.getNombre_corto());
			pstmt.setDate(3, tarea.getFecha_inicio());
			pstmt.setDate(4, tarea.getFecha_limite());
			pstmt.setBoolean(5, tarea.getFlag_finalizado());
			pstmt.setDate(6, new Date(System.currentTimeMillis()));
			pstmt.setInt(7, tarea.getId_tarea());

			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar tarea: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public ArrayList<Tarea> obtenerTareas() throws Exception {
		ArrayList<Tarea> tareas = new ArrayList<Tarea>();
		Tarea tarea = null;
		TipoAmbito tipo_ambito = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select T.*, TA.NbrTipoAmbito from HE_Tarea T "
				+ "left join ME_TipoAmbito TA on T.CodTipoAmbito = TA.CodTipoAmbito " + "order by CodTarea";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				tarea = new Tarea();
				tarea.setId_tarea(rs.getInt("CodTarea"));
				tarea.setNombre_tarea(rs.getString("NbrTarea"));
				tarea.setNombre_corto(rs.getString("NbrTareaCorto"));
				tarea.setFecha_inicio(rs.getDate("FecInicio"));
				tarea.setFecha_limite(rs.getDate("FecLimite"));
				tarea.setFlag_finalizado(rs.getBoolean("FlgFinalizado"));

				tipo_ambito = new TipoAmbito();
				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				tarea.setTipo_ambito(tipo_ambito);

				tareas.add(tarea);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener Tareas: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return tareas;
	}

	// -------------------------------------------------------------------
	// -------------------------PERFIL Y ROL------------------------------
	// -------------------------------------------------------------------

	public void crearPerfil(Perfil perfil) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		Integer id_perfil = null;
		String sql = "INSERT INTO ME_Perfil(CodPerfil, NbrPerfil, NbrPerfilAd, FlgObligatorio, CodTipoAMbito, FecActualizacionTabla)"
				+ " VALUES (?,?,?,?,?,?)";
		String sql_ins = "INSERT INTO ME_PerfilPermiso(CodPerfil, CodPermiso, FecActualizacionTabla) VALUES (?,?,?)";
		try {
			conn = obtenerConexion();

			pstmt = conn.prepareStatement(sql);
			id_perfil = obtenerSiguienteId("seq_me_perfil");
			perfil.setId_perfil(id_perfil);
			pstmt.setInt(1, id_perfil);
			pstmt.setString(2, perfil.getNombre_perfil());
			pstmt.setString(3, perfil.getNombre_perfil_ad());
			pstmt.setBoolean(4, perfil.getFlag_obligatorio());
			pstmt.setInt(5, perfil.getTipo_ambito().getId_tipo_ambito());
			pstmt.setDate(6, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);

			for (int i = 0; i < perfil.getPermisos().size(); i++) {
				pstmt = conn.prepareStatement(sql_ins);
				pstmt.setInt(1, perfil.getId_perfil());
				pstmt.setInt(2, perfil.getPermisos().get(i).getId_permiso());
				pstmt.setDate(3, new Date(System.currentTimeMillis()));
				pstmt.executeUpdate();
				cerrarPreparedStatement(pstmt);
			}
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear un perfil: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void borrarPerfil(Integer id_perfil) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;

		String sql = new String();

		sql = "DELETE from ME_Perfil where CodPerfil=?";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_perfil);
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar perfil con id: " + id_perfil + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void editarPerfil(Perfil perfil) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;

		String sql = "Update ME_Perfil set NbrPerfil= ?,NbrPerfilAd=?, FlgObligatorio= ?, CodTipoAmbito= ?, FecActualizacionTabla= ?"
				+ " where CodPerfil= ?";
		String sql_del = "DELETE from ME_PerfilPermiso where CodPerfil=?";
		String sql_ins = "INSERT INTO ME_PerfilPermiso(CodPerfil,CodPermiso,FecActualizacionTabla) VALUES (?,?,?)";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, perfil.getNombre_perfil());
			pstmt.setString(2, perfil.getNombre_perfil_ad());
			pstmt.setBoolean(3, perfil.getFlag_obligatorio());
			pstmt.setInt(4, perfil.getTipo_ambito().getId_tipo_ambito());
			pstmt.setDate(5, new Date(System.currentTimeMillis()));
			pstmt.setInt(6, perfil.getId_perfil());
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);

			pstmt = conn.prepareStatement(sql_del);
			pstmt.setInt(1, perfil.getId_perfil());
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);

			for (int i = 0; i < perfil.getPermisos().size(); i++) {
				pstmt = conn.prepareStatement(sql_ins);
				pstmt.setInt(1, perfil.getId_perfil());
				pstmt.setInt(2, perfil.getPermisos().get(i).getId_permiso());
				pstmt.setDate(3, new Date(System.currentTimeMillis()));
				pstmt.executeUpdate();
				cerrarPreparedStatement(pstmt);
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar perfil: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public ArrayList<Perfil> obtenerPerfiles() throws Exception {
		ArrayList<Perfil> perfiles = new ArrayList<Perfil>();
		Perfil perfil = null;
		TipoAmbito tipo_ambito = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select P.*, TA.NbrTipoAmbito from ME_Perfil P "
				+ "left join ME_TipoAmbito TA on P.CodTipoAmbito = TA.CodTipoAmbito " + "order by P.CodPerfil";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if (!rs.getString("NBRPERFIL").equalsIgnoreCase("Desarrollador")) {
					perfil = new Perfil();
					perfil.setId_perfil(rs.getInt("CodPerfil"));
					perfil.setNombre_perfil(rs.getString("NbrPerfil"));
					perfil.setNombre_perfil_ad(rs.getString("NbrPerfilAd"));

					tipo_ambito = new TipoAmbito();
					tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
					tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
					perfil.setTipo_ambito(tipo_ambito);

					perfiles.add(perfil);
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener Perfiles: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return perfiles;
	}

	public ArrayList<Permiso> obtenerPermisos() throws Exception {
		ArrayList<Permiso> permisos = new ArrayList<Permiso>();
		Permiso permiso = null;
		TipoAmbito tipoAmbito = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select P.*, TA.NbrTipoAmbito from ME_Permiso P "
				+ "left join ME_TipoAmbito TA on P.CodTipoAmbito = TA.CodTipoAmbito " + "order by CodPermiso";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				permiso = new Permiso();
				permiso.setId_permiso(rs.getInt("CodPermiso"));
				permiso.setNombre_permiso(rs.getString("NbrPermiso"));
				permiso.setNivel(rs.getInt("NUMNIVEL"));
				permiso.setNombre_permiso_padre(rs.getString("NBRPERMISOPADRE"));
				permiso.setFlag_boton(rs.getBoolean("FLGBOTON"));
				permiso.setFlag_ultimaJerarquia(rs.getBoolean("FLGULTIMAJERARQUIA"));
				permiso.setRuta(rs.getString("NBRRUTA"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));

				permiso.setTipo_ambito(tipoAmbito);

				permisos.add(permiso);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener Permisos: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return permisos;
	}

	public ArrayList<Perfil> obtenerPerfilesPermisos() throws Exception {
		Connection conn = null;
		ArrayList<Perfil> ans = new ArrayList<Perfil>();
		conn = obtenerConexion();
		ans = obtenerPerfilesPermisos(conn);
		cerrarConexion(conn);
		return ans;
	}

	public ArrayList<Perfil> obtenerPerfilesPermisos(Connection conn) throws Exception {
		ArrayList<Perfil> perfiles = new ArrayList<Perfil>();
		Perfil perfil = null;
		TipoAmbito tipo_ambito = null;

		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select P.*, TA.NbrTipoAmbito from me_perfil P "
				+ "left join ME_TipoAmbito TA on P.CodTipoAmbito = TA.CodTipoAmbito";

		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if (!rs.getString("NBRPERFIL").equalsIgnoreCase("Desarrollador")) {
					perfil = new Perfil();
					perfil.setId_perfil(rs.getInt("CODPERFIL"));
					perfil.setNombre_perfil(rs.getString("NBRPERFIL"));
					perfil.setNombre_perfil_ad(rs.getString("NBRPERFILAD"));
					perfil.setFlag_obligatorio(rs.getBoolean("FLGOBLIGATORIO"));

					tipo_ambito = new TipoAmbito();
					tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
					tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
					perfil.setTipo_ambito(tipo_ambito);

					perfil.setPermisos(obtenerPermisosPorIdPerfil(perfil.getId_perfil(), conn));
					perfiles.add(perfil);
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener perfiles y sus permisos: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return perfiles;
	}

	public Perfil obtenerPerfilConPermisosPorNombreAD(String nombreAD) throws Exception {
		Perfil perfil = null;
		TipoAmbito tipo_ambito = null;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		String sql = "select P.*, TA.NbrTipoAmbito from ME_Perfil P "
				+ "left join ME_TipoAmbito TA on P.CodTipoAmbito = TA.CodTipoAmbito where P.NbrPerfilAd=?";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, nombreAD);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				perfil = new Perfil();
				perfil.setId_perfil(rs.getInt("CODPERFIL"));
				perfil.setNombre_perfil(rs.getString("NBRPERFIL"));
				perfil.setNombre_perfil_ad(rs.getString("NBRPERFILAD"));
				perfil.setFlag_obligatorio(rs.getBoolean("FLGOBLIGATORIO"));

				tipo_ambito = new TipoAmbito();
				tipo_ambito.setId_tipo_ambito(rs.getInt("CODTIPOAMBITO"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NBRTIPOAMBITO"));
				perfil.setTipo_ambito(tipo_ambito);

				perfil.setPermisos(obtenerPermisosPorIdPerfil(perfil.getId_perfil(), conn));
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener el perfil (con permisos): " + nombreAD + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return perfil;
	}

	// -------------------------------------------------------------------
	// ---------------------CORREO Y NOTIFICACIONES-----------------------
	// -------------------------------------------------------------------

	public void crearCorreo(Correo correo) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		Integer id_correo = null;
		String sql = "Insert Into He_Correo(Codcorreo,CodTipoAmbito,Nbrcorreo,Fecactualizaciontabla)"
				+ " VALUES (?,?,?,?)";
		String sql_ins = "Insert Into HE_MAPEOCORREONOTIF(CODCORREO,CODTIPOOBJETO,CODTIPOACCION) VALUES (?,?,?)";
		try {
			conn = obtenerConexion();

			pstmt = conn.prepareStatement(sql);
			id_correo = obtenerSiguienteId("SEQ_He_Correo");
			correo.setId_correo(id_correo);
			pstmt.setInt(1, id_correo);
			pstmt.setInt(2, correo.getTipo_ambito().getId_tipo_ambito());
			pstmt.setString(3, correo.getNombre_correo());
			pstmt.setDate(4, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);

			for (int i = 0; i < correo.getNotificaciones().size(); i++) {
				pstmt = conn.prepareStatement(sql_ins);
				pstmt.setInt(1, correo.getId_correo());
				pstmt.setInt(2, correo.getNotificaciones().get(i).getId_tipoObjeto());
				pstmt.setInt(3, correo.getNotificaciones().get(i).getId_tipoAccion());
				pstmt.executeUpdate();
				cerrarPreparedStatement(pstmt);
			}
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear el correo: " + correo.getNombre_correo() + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void borrarCorreo(Integer id_correo) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = new String();
		sql = "DELETE from HE_CORREO where CODCORREO=?";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_correo);
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar el correo con id: " + id_correo + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void editarCorreo(Correo correo) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;

		String sql = "Update HE_CORREO set NBRCORREO= ?, FecActualizacionTabla= ?" + " where CODCORREO= ?";
		String sql_del = "DELETE from HE_MAPEOCORREONOTIF where CODCORREO=?";
		String sql_ins = "INSERT INTO HE_MAPEOCORREONOTIF(CODCORREO,CODTIPOOBJETO,CODTIPOACCION) VALUES (?,?,?)";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, correo.getNombre_correo());
			pstmt.setDate(2, new Date(System.currentTimeMillis()));
			pstmt.setInt(3, correo.getId_correo());
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);

			pstmt = conn.prepareStatement(sql_del);
			pstmt.setInt(1, correo.getId_correo());
			pstmt.executeUpdate();
			cerrarPreparedStatement(pstmt);

			for (int i = 0; i < correo.getNotificaciones().size(); i++) {
				pstmt = conn.prepareStatement(sql_ins);
				pstmt.setInt(1, correo.getId_correo());
				pstmt.setInt(2, correo.getNotificaciones().get(i).getId_tipoObjeto());
				pstmt.setInt(3, correo.getNotificaciones().get(i).getId_tipoAccion());
				pstmt.executeUpdate();
				cerrarPreparedStatement(pstmt);
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar correo: " + correo.getNombre_correo() + " /" + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public ArrayList<Correo> obtenerCorreosConNotificaciones(Integer id_tipoAmbito) throws Exception {
		Connection conn = null;
		ArrayList<Correo> ans = new ArrayList<Correo>();
		conn = obtenerConexion();
		ans = obtenerCorreosConNotificaciones(conn, id_tipoAmbito);
		cerrarConexion(conn);
		return ans;
	}

	public ArrayList<Correo> obtenerCorreosConNotificaciones(Connection conn, Integer id_tipoAmbito) throws Exception {
		ArrayList<Correo> correos = new ArrayList<Correo>();
		Correo correo = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "Select * from HE_CORREO where CodTipoAmbito=?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_tipoAmbito);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				correo = new Correo();
				correo.setId_correo(rs.getInt("CODCORREO"));
				correo.setNombre_correo(rs.getString("NBRCORREO"));
				correo.setNotificaciones(obtenerNotificaionesporIdCorreo(correo.getId_correo(), conn));
				correos.add(correo);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los correos y sus notificaciones: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return correos;
	}

	public ArrayList<Notificacion> obtenerNotificaionesporIdCorreo(Integer id_correo, Connection conn)
			throws Exception {
		ArrayList<Notificacion> notificaciones = new ArrayList<Notificacion>();
		Notificacion notificacion = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select * from HE_MAPEOCORREONOTIF where codcorreo=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_correo);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				notificacion = new Notificacion();
				notificacion.setId_correo(rs.getInt("CODCORREO"));
				notificacion.setId_tipoAccion(rs.getInt("CODTIPOACCION"));
				notificacion.setId_tipoObjeto(rs.getInt("CODTIPOOBJETO"));
				notificaciones.add(notificacion);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las notificaiones del correo con id: " + id_correo + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return notificaciones;
	}

	public ArrayList<String> obtenerListaDestinatariosDeEvento(Evento evento) throws Exception {
		ArrayList<String> correos = new ArrayList<String>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "SELECT c.* from he_mapeocorreonotif MCN "
				+ " inner join he_correo C on mcn.codcorreo = c.codcorreo and mcn.codtipoaccion=? "
				+ " and mcn.codtipoobjeto=? " + " and c.codtipoambito=? ";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, evento.getTipo_accion().getId_tipo_accion());
			pstmt.setInt(2, evento.getTipo_objeto().getId_tipo_objeto());
			pstmt.setInt(3, evento.getPerfil_usuario().getTipo_ambito().getId_tipo_ambito());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				correos.add(rs.getString("NBRCORREO"));
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener lista de correos destinatarios de evento: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return correos;
	}

	public ArrayList<Permiso> obtenerPermisosPorIdPerfil(Integer id_perfil) throws Exception {
		Connection conn = null;
		ArrayList<Permiso> ans = new ArrayList<Permiso>();
		conn = obtenerConexion();
		ans = obtenerPermisosPorIdPerfil(id_perfil, conn);
		cerrarConexion(conn);
		return ans;
	}

	public ArrayList<Permiso> obtenerPermisosPorIdPerfil(Integer id_perfil, Connection conn) throws Exception {
		ArrayList<Permiso> permisos = new ArrayList<Permiso>();
		Permiso permiso = null;
		TipoAmbito tipoAmbito = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select R.*, TA.CodTipoAmbito, TA.NbrTipoAmbito from ME_PerfilPermiso PR"
				+ " left join ME_Permiso R on PR.CodPermiso= R.CodPermiso "
				+ "	left join ME_TipoAmbito TA on R.CodTipoAmbito = TA.CodTipoAmbito where CodPerfil=? "
				+ " order by R.CodPermiso ASC";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_perfil);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				permiso = new Permiso();

				permiso.setId_permiso(rs.getInt("CodPermiso"));
				permiso.setNombre_permiso(rs.getString("NbrPermiso"));
				permiso.setNivel(rs.getInt("NUMNIVEL"));
				permiso.setNombre_permiso_padre(rs.getString("NBRPERMISOPADRE"));
				permiso.setFlag_boton(rs.getBoolean("FLGBOTON"));
				permiso.setFlag_ultimaJerarquia(rs.getBoolean("FLGULTIMAJERARQUIA"));
				permiso.setRuta(rs.getString("NBRRUTA"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				permiso.setTipo_ambito(tipoAmbito);

				permisos.add(permiso);

			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener permisos por id Perfil: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return permisos;
	}

	public ArrayList<Aprovisionamiento> obtenerAprovisionamientoInput() throws Exception {
		Connection conn = null;
		ArrayList<Aprovisionamiento> ans = new ArrayList<Aprovisionamiento>();
		conn = obtenerConexion();
		ans = obtenerAprovisionamientoInput(conn);
		cerrarConexion(conn);
		return ans;
	}

	public ArrayList<Aprovisionamiento> obtenerAprovisionamientosObligatoriosDeCartera(Integer id_cartera,
			Connection conn) throws Exception {
		ArrayList<Aprovisionamiento> AprovInputs = new ArrayList<Aprovisionamiento>();
		Aprovisionamiento aprovInput = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select a.codaprovisionamiento, a.nbrfichero from HE_Aprovisionamiento a "
				+ "left join he_mapeoficherocartera mfc on mfc.codaprovisionamiento=a.codaprovisionamiento "
				+ "where a.flgobligatorio=1 and mfc.codcartera=?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_cartera);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				aprovInput = new Aprovisionamiento();
				aprovInput.setId_aprovisionamiento(rs.getInt("CodAprovisionamiento"));
				aprovInput.setNombre_fichero(rs.getString("NbrFichero"));
				AprovInputs.add(aprovInput);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener aprovisionamientos: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return AprovInputs;
	}

	public ArrayList<Aprovisionamiento> obtenerAprovisionamientoInput(Connection conn) throws Exception {
		ArrayList<Aprovisionamiento> AprovInputs = new ArrayList<Aprovisionamiento>();
		Aprovisionamiento aprovInput = null;
		TipoAmbito tipoambito = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select * from HE_Aprovisionamiento order by codaprovisionamiento desc";

		try {

			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				aprovInput = new Aprovisionamiento();
				tipoambito = new TipoAmbito();
				aprovInput.setId_aprovisionamiento(rs.getInt("CodAprovisionamiento"));
				aprovInput.setNombre_fichero(rs.getString("NbrFichero"));
				aprovInput.setFlag_obligatorio(rs.getBoolean("FLGOBLIGATORIO"));
				aprovInput.setFlag_fechaautomatica(rs.getBoolean("FLGFECHAAUTOMATICA"));
				aprovInput.setFlag_operacion(rs.getBoolean("FLGCUENTAOPERACION"));
				tipoambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				aprovInput.setTipo_ambito(tipoambito);
				aprovInput.setCarteras(
						obtenerCarterasDeAprovisionamientoInput(aprovInput.getId_aprovisionamiento(), conn));
				AprovInputs.add(aprovInput);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener aprovisionamientos: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return AprovInputs;
	}

	public ArrayList<Aprovisionamiento> obtenerAprovCtaOperacion() throws Exception {
		Connection conn = null;
		ArrayList<Aprovisionamiento> aprovs = new ArrayList<Aprovisionamiento>();
		Aprovisionamiento aprov = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select CodAprovisionamiento, NbrFichero from HE_Aprovisionamiento"
				+ " where FlgCuentaOperacion = 1";

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				aprov = new Aprovisionamiento();
				aprov.setId_aprovisionamiento(rs.getInt("CodAprovisionamiento"));
				aprov.setNombre_fichero(rs.getString("NbrFichero"));

				aprovs.add(aprov);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener aprovisionamientos cuenta operacion: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return aprovs;
	}

	public ArrayList<Cartera> obtenerCarterasDeAprovisionamientoInput(Integer id_aprovisionamiento) throws Exception {
		Connection conn = null;
		ArrayList<Cartera> carteras = new ArrayList<Cartera>();
		conn = obtenerConexion();
		carteras = obtenerCarterasDeAprovisionamientoInput(id_aprovisionamiento, conn);
		cerrarConexion(conn);
		return carteras;
	}

	public ArrayList<Cartera> obtenerCarterasDeAprovisionamientoInput(Integer id_aprovisionamiento, Connection conn)
			throws Exception {
		ArrayList<Cartera> carteras = new ArrayList<Cartera>();
		Cartera cartera = null;
		Entidad entidad = null;
		Statement stmt = null;
		TipoAmbito tipo_ambito = null;
		TipoMagnitud tipo_magnitud = null;
		TipoCartera tipo_cartera = null;

		ResultSet rs = null;
		String sql = "SELECT C.*,E.NbrEntidad,TA.NbrTipoAmbito,TM.NbrTipoMagnitud FROM HE_Cartera C"
				+ " LEFT JOIN HE_Entidad E ON C.CodEntidad= E.CodEntidad"
				+ " left JOIN ME_Pais P ON E.CodPais = P.CodPais"
				+ " left JOIN ME_TipoAmbito TA ON C.CodTipoAmbito = TA.CodTipoAmbito"
				+ "	left JOIN HE_MapeoFicheroCartera MFC ON c.codcartera = MFC.codcartera"
				+ " left JOIN ME_TipoMagnitud TM ON C.CodTipoMagnitud = TM.CodTipoMagnitud "
				+ "	left JOIN he_aprovisionamiento A ON mfc.codaprovisionamiento = a.codaprovisionamiento "
				+ " where a.codaprovisionamiento=" + id_aprovisionamiento + " order by C.CodCartera";

		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				cartera = new Cartera();
				entidad = new Entidad();
				tipo_ambito = new TipoAmbito();
				tipo_magnitud = new TipoMagnitud();
				tipo_cartera = new TipoCartera();

				cartera.setId_cartera(rs.getInt("CodCartera"));
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_magnitud.setId_tipo_magnitud(rs.getInt("CodTipoMagnitud"));
				tipo_cartera.setId_tipo_cartera(rs.getInt("CodTipoCartera"));
				cartera.setFlag_pd6meses(rs.getBoolean("FlgPd6Meses"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				tipo_magnitud.setNombre_tipo_magnitud(rs.getString("NbrTipoMagnitud"));

				cartera.setTipo_ambito(tipo_ambito);
				cartera.setTipo_magnitud(tipo_magnitud);
				cartera.setTipo_cartera(tipo_cartera);

				// entidad.setPais(pais);
				cartera.setEntidad(entidad);

				carteras.add(cartera);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las carteras de aprovisionamientos: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return carteras;
	}

	// -------------------------------------------------------------------
	// -----------------------------EVENTO---------------------------------
	// -------------------------------------------------------------------

	public Integer crearEvento(Evento evento) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		Integer id_evento = null;
		String sql = "Insert into HE_Evento(CodEvento,FecEvento,CodTipoObjeto,CodTipoAccion,"
				+ " NbrObjeto,NbrObjetoJson,NbrActualizadoJson,NbrAgregadoJson,NbrBorradoJson, "
				+ " CodUsuario,NbrUsuario,CodPerfil,FecActualizacionTabla) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			conn = obtenerConexion();

			pstmt = conn.prepareStatement(sql);
			id_evento = obtenerSiguienteId("seq_he_evento");
			pstmt.setInt(1, id_evento);
			pstmt.setLong(2, evento.getFecha_evento());
			pstmt.setInt(3, evento.getTipo_objeto().getId_tipo_objeto());
			pstmt.setInt(4, evento.getTipo_accion().getId_tipo_accion());
			pstmt.setString(5, evento.getNombre_objeto());
			pstmt.setString(6, evento.getObjeto_json());
			pstmt.setString(7, evento.getActualizado_json());
			pstmt.setString(8, evento.getAgregado_json());
			pstmt.setString(9, evento.getBorrado_json());
			pstmt.setString(10, evento.getId_usuario());
			pstmt.setString(11, evento.getNombre_usuario());
			pstmt.setInt(12, evento.getPerfil_usuario().getId_perfil());
			pstmt.setDate(13, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar un evento : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return id_evento;
	}

	public ArrayList<Evento> obtenerEventos(Integer id_ambito) throws Exception {
		ArrayList<Evento> eventos = new ArrayList<Evento>();
		Evento evento = null;
		TipoObjeto tipoObjeto = null;
		TipoAccion tipoAccion = null;
		Perfil perfil = null;
		TipoAmbito tipo_ambito = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select E.CodEvento, E.FecEvento, E.NbrObjeto, E.CodUsuario, E.NbrUsuario, "
				+ "E.CodTipoObjeto, E.CodTipoAccion, E.CodPerfil,"
				+ "O.NbrTipoObjeto, TA.NbrTipoAccion, P.NbrPerfil, P.NbrPerfilAd,"
				+ " P.FlgObligatorio, TAM.CodTipoAmbito, TAM.NbrTipoAmbito " + " from HE_Evento E"
				+ " left join ME_Perfil P on E.CodPerfil = P.CodPerfil "
				+ " left join ME_TipoAmbito TAM on P.CodTipoAmbito = TAM.CodTipoAmbito "
				+ " left join ME_TipoObjeto O on E.CodTipoObjeto = O.CodTipoObjeto"
				+ " left join ME_TipoAccion TA on E.CodTipoAccion = TA.CodTipoAccion" + " where P.CodTipoAmbito = "
				+ id_ambito + " order by FecEvento desc";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				evento = new Evento();
				tipoObjeto = new TipoObjeto();
				tipoAccion = new TipoAccion();
				perfil = new Perfil();
				tipo_ambito = new TipoAmbito();

				evento.setId_evento(rs.getInt("CodEvento"));
				evento.setFecha_evento(rs.getLong("FecEvento"));
				evento.setNombre_objeto(rs.getString("NbrObjeto"));

				// evento.setObjeto_json(rs.getString("NbrObjetoJson"));
				// evento.setActualizado_json(rs.getString("NbrActualizadoJson"));
				// evento.setAgregado_json(rs.getString("NbrAgregadoJson"));
				// evento.setBorrado_json(rs.getString("NbrBorradoJson"));
				evento.setObjeto_json("");
				evento.setActualizado_json("");
				evento.setAgregado_json("");
				evento.setBorrado_json("");

				evento.setId_usuario(rs.getString("CodUsuario"));
				evento.setNombre_usuario(rs.getString("NbrUsuario"));

				tipoObjeto.setId_tipo_objeto(rs.getInt("CodTipoObjeto"));
				tipoAccion.setId_tipo_accion(rs.getInt("CodTipoAccion"));
				tipoObjeto.setNombre_tipo_objeto(rs.getString("NbrTipoObjeto"));
				tipoAccion.setNombre_tipo_accion(rs.getString("NbrTipoAccion"));

				perfil.setId_perfil(rs.getInt("CodPerfil"));
				perfil.setNombre_perfil(rs.getString("NbrPerfil"));
				perfil.setNombre_perfil_ad(rs.getString("NbrPerfilAd"));
				perfil.setFlag_obligatorio(rs.getBoolean("FlgObligatorio"));

				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				perfil.setTipo_ambito(tipo_ambito);

				evento.setTipo_objeto(tipoObjeto);
				evento.setTipo_accion(tipoAccion);
				evento.setPerfil_usuario(perfil);
				eventos.add(evento);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener eventos: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return eventos;
	}

	public EventoDetalle obtenerEventoDetalle(Integer id_evento) throws Exception {

		EventoDetalle evento = new EventoDetalle();

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select *  from HE_Evento E" + " where E.CodEvento = " + id_evento;
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {

				evento.setId_evento(id_evento);
				evento.setObjeto_json(rs.getString("NbrObjetoJson"));
				evento.setActualizado_json(rs.getString("NbrActualizadoJson"));
				evento.setAgregado_json(rs.getString("NbrAgregadoJson"));
				evento.setBorrado_json(rs.getString("NbrBorradoJson"));

			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener el detalle del evento " + id_evento + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return evento;
	}

	// -------------------------------------------------------------------
	// -----------------------------ALERTA---------------------------------
	// -------------------------------------------------------------------

	public ArrayList<Alerta> obtenerAlertas() throws Exception {
		ArrayList<Alerta> alertas = new ArrayList<Alerta>();
		Alerta alerta = null;
		TipoAlerta tipoAlerta = null;
		TipoMensajeAlerta tipoMensajeAlerta = null;
		TipoObjeto tipoObjeto = null;
		TipoObjeto tipoObjetoCausa = null;
		TipoAccion tipoAccion = null;
		TipoAmbito tipoAmbito = null;
		Connection conn = null;
		Statement stmt_tipoAlerta = null;
		Statement stmt_alerta = null;
		ResultSet rs_tipoAlerta = null;
		ResultSet rs_alerta = null;
		String sql_tipoAlerta = "";
		String sql_alerta = "";

		sql_tipoAlerta = "select TA.*, TMA.NbrTipoMensajeAlerta, "
				+ " TO1.NbrTipoObjeto, TO2.NbrTipoObjeto as NbrTipoObjetoCausa, TAC.NbrTipoAccion, "
				+ " TAM.CodTipoAmbito, TAM.NbrTipoAmbito from HE_TipoAlerta TA "
				+ " left join ME_TipoMensajeAlerta TMA on TA.CodTipoMensajeAlerta = TMA.CodTipoMensajeAlerta"
				+ " left join ME_TipoObjeto TO1 on TA.CodTipoObjeto= TO1.CodTipoObjeto"
				+ " left join ME_TipoObjeto TO2 on TA.CodTipoObjetoCausa = TO2.CodTipoObjeto"
				+ " left join ME_TipoAccion TAC on TA.CodTipoAccion = TAC.CodTipoAccion"
				+ " left join ME_TipoAmbito TAM on TA.CodTipoAmbito = TAM.CodTipoAmbito";

		try {
			conn = obtenerConexion();
			stmt_tipoAlerta = crearStatement(conn);
			rs_tipoAlerta = stmt_tipoAlerta.executeQuery(sql_tipoAlerta);
			while (rs_tipoAlerta.next()) {
				tipoAlerta = new TipoAlerta();
				tipoMensajeAlerta = new TipoMensajeAlerta();
				tipoObjeto = new TipoObjeto();
				tipoObjetoCausa = new TipoObjeto();
				tipoAccion = new TipoAccion();

				tipoAlerta.setId_tipo_alerta(rs_tipoAlerta.getInt("CodTipoAlerta"));
				tipoAlerta.setNombre_tipo_alerta(rs_tipoAlerta.getString("NbrTipoAlerta"));
				tipoMensajeAlerta.setId_tipo_mensaje_alerta(rs_tipoAlerta.getInt("CodTipoMensajeAlerta"));
				tipoObjeto.setId_tipo_objeto(rs_tipoAlerta.getInt("CodTipoObjeto"));
				tipoObjetoCausa.setId_tipo_objeto(rs_tipoAlerta.getInt("CodTipoObjetoCausa"));
				tipoAccion.setId_tipo_accion(rs_tipoAlerta.getInt("CodTipoAccion"));
				tipoAlerta.setSeccion_alerta(rs_tipoAlerta.getString("NbrSeccionAlerta"));
				tipoAlerta.setSql_alerta(rs_tipoAlerta.getString("NbrSqlAlerta"));
				tipoMensajeAlerta.setNombre_tipo_mensaje_alerta(rs_tipoAlerta.getString("NbrTipoMensajeAlerta"));
				tipoObjeto.setNombre_tipo_objeto(rs_tipoAlerta.getString("NbrTipoObjeto"));
				tipoObjetoCausa.setNombre_tipo_objeto(rs_tipoAlerta.getString("NbrTipoObjetoCausa"));
				tipoAccion.setNombre_tipo_accion(rs_tipoAlerta.getString("NbrTipoAccion"));

				tipoAlerta.setTipo_mensaje_alerta(tipoMensajeAlerta);
				tipoAlerta.setTipo_objeto(tipoObjeto);
				tipoAlerta.setTipo_objeto_causa(tipoObjetoCausa);
				tipoAlerta.setTipo_accion(tipoAccion);

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs_tipoAlerta.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs_tipoAlerta.getString("NbrTipoAmbito"));
				tipoAlerta.setTipo_ambito(tipoAmbito);

				sql_alerta = tipoAlerta.getSql_alerta();
				stmt_alerta = crearStatement(conn);
				rs_alerta = stmt_alerta.executeQuery(sql_alerta);
				while (rs_alerta.next()) {
					alerta = new Alerta();

					alerta.setTipo_alerta(tipoAlerta);
					alerta.setNombre_objeto(rs_alerta.getString("NbrObjeto"));

					alertas.add(alerta);
				}
				cerrarResultSet(rs_alerta);
				cerrarStatement(stmt_alerta);

			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener Alertas: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_tipoAlerta);
			cerrarResultSet(rs_alerta);
			cerrarStatement(stmt_tipoAlerta);
			cerrarStatement(stmt_alerta);
			cerrarConexion(conn);
		}
		return alertas;
	}

	// ------------------------------------------------------------------------
	// ------------------------PROGRESO ENTIDAD--------------------------------
	// ------------------------------------------------------------------------
	public ArrayList<CarteraProgreso> obtenerCarterasProgresoNulas(Connection conn, Integer id_entidad,
			Integer id_tipo_ambito) throws Exception {
		ArrayList<CarteraProgreso> carteras_progreso = new ArrayList<CarteraProgreso>();
		CarteraProgreso carteraProgreso = null;
		Statement stmt = null;

		ResultSet rs = null;
		String sql = "Select C.Codcartera, C.Nbrcartera From He_Cartera C " + "Where C.Codtipoambito = "
				+ id_tipo_ambito + " And C.Codentidad=" + id_entidad;
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				carteraProgreso = new CarteraProgreso();
				carteraProgreso.setId_cartera(rs.getInt("CodCartera"));
				carteraProgreso.setNombre_cartera(rs.getString("NbrCartera"));
				carteras_progreso.add(carteraProgreso);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al formar las carteras progreso de la entidad con id: " + id_entidad + " / "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return carteras_progreso;
	}

	public ArrayList<EntidadProgreso> obtenerEntidadesProgreso(FiltroProgreso filtroProgreso) throws Exception {
		Connection conn = null;
		conn = obtenerConexion();

		ArrayList<EntidadProgreso> entidades_progresos = new ArrayList<EntidadProgreso>();
		ArrayList<Entidad> entidades = obtenerEntidades();
		EntidadProgreso entidadProgreso = null;
		for (Entidad entidad : entidades) {
			if (entidad.getTipo_ambito().getId_tipo_ambito().equals(filtroProgreso.getId_tipo_ambito())) {
				entidadProgreso = new EntidadProgreso();
				entidadProgreso.setNombre_entidad(entidad.getNombre_entidad());
				entidadProgreso.setId_entidad(entidad.getId_entidad());
				entidadProgreso.setCarteras_progreso(obtenerCarterasProgresoNulas(conn, entidadProgreso.getId_entidad(),
						filtroProgreso.getId_tipo_ambito()));
				entidades_progresos.add(entidadProgreso);
			}
		}
		entidades_progresos = obtenerEntidadesProgreso(entidades_progresos, filtroProgreso);
		cerrarConexion(conn);
		return entidades_progresos;
	}

	public ArrayList<EntidadProgreso> obtenerEntidadesProgreso(ArrayList<EntidadProgreso> entidades_progresos,
			FiltroProgreso filtroProgreso) throws Exception {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Statement stmt_total = null;
		ResultSet rs_total = null;

		for (EntidadProgreso entidad_progreso : entidades_progresos) {

			for (CarteraProgreso cartera_progreso : entidad_progreso.getCarteras_progreso()) {

				// Queryes para obtener los totales necesitados
				// Procesar y Aprobar Ficheros TOTALES
				String sql_total_ficheros_obligatorios = "select a.codaprovisionamiento, a.nbrfichero from HE_Aprovisionamiento a "
						+ "left join he_mapeoficherocartera mfc on mfc.codaprovisionamiento=a.codaprovisionamiento "
						+ "where a.flgobligatorio=1 and mfc.codcartera=" + cartera_progreso.getId_cartera();

				// Queryes para obtener lo que se tiene
				// Procesar Fichero
				String sql_cargasDeCartera = "select a.codaprovisionamiento, a.nbrfichero from he_cargafichero cf "
						+ "left join he_tablainput tabi on tabi.codtablainput=cf.codtablainput "
						+ "left join he_aprovisionamiento a on a.codaprovisionamiento= tabi.codaprovisionamiento "
						+ "left join he_mapeoficherocartera mfc on a.codaprovisionamiento=mfc.codaprovisionamiento "
						+ "where cf.codfecha =" + filtroProgreso.getFecha().getId_fecha() + " and mfc.codcartera="
						+ cartera_progreso.getId_cartera();

				// Aprobar Fichero sql.
				String sql_cargasAprobadasDeCartera = "select a.codaprovisionamiento, a.nbrfichero from he_cargafichero cf "
						+ "left join he_tablainput tabi on tabi.codtablainput=cf.codtablainput "
						+ "left join he_aprovisionamiento a on a.codaprovisionamiento= tabi.codaprovisionamiento "
						+ "left join he_mapeoficherocartera mfc on a.codaprovisionamiento=mfc.codaprovisionamiento "
						+ "where cf.tipestadocarga=2 and cf.codfecha =" + filtroProgreso.getFecha().getId_fecha()
						+ " and mfc.codcartera=" + cartera_progreso.getId_cartera();

				// Ejecutar PE
				String sql_ejecutadas = "select e.codejecucion,e.codmes from he_ejecucion e "
						+ "left join he_escenarioversion ev on ev.codescenarioversion= e.codescenarioversion "
						+ "left join he_juegoescenariosversion jv on jv.codjuegoescenarios= ev.codjuegoescenarios "
						+ "left join he_carteraversion cv on cv.codcarteraversion= jv.codcarteraversion "
						+ "left join he_metodologiacartera m on m.codmetodologia = cv.codmetodologia "
						+ "left join he_cartera c on c.codcartera= m.codcartera "
						+ "where e.tipestadoejecucion in (2,4) and e.tipestadovalidacion=0 " + "and c.codcartera="
						+ cartera_progreso.getId_cartera() + " and e.codmes=" + filtroProgreso.getFecha().getCodmes()
						+ " and e.codtipoejecucion in "
						+ ((filtroProgreso.getFecha().getFase().equals(1)) ? "(1,7)" : "(2,8)"); // Incluye inversiones
				String sql_validadas = "select e.codejecucion,e.codmes from he_ejecucion e "
						+ "left join he_escenarioversion ev on ev.codescenarioversion= e.codescenarioversion "
						+ "left join he_juegoescenariosversion jv on jv.codjuegoescenarios= ev.codjuegoescenarios "
						+ "left join he_carteraversion cv on cv.codcarteraversion= jv.codcarteraversion "
						+ "left join he_metodologiacartera m on m.codmetodologia = cv.codmetodologia "
						+ "left join he_cartera c on c.codcartera= m.codcartera "
						+ "where e.tipestadoejecucion in (2,4) and e.tipestadovalidacion=2 " + "and c.codcartera="
						+ cartera_progreso.getId_cartera() + " and e.codmes=" + filtroProgreso.getFecha().getCodmes()
						+ " and e.codtipoejecucion in "
						+ ((filtroProgreso.getFecha().getFase().equals(1)) ? "(1,7)" : "(2,8)"); // Incluye inversiones
				String sql_aprobadas = "select e.codejecucion,e.codmes from he_ejecucion e "
						+ "left join he_escenarioversion ev on ev.codescenarioversion= e.codescenarioversion "
						+ "left join he_juegoescenariosversion jv on jv.codjuegoescenarios= ev.codjuegoescenarios "
						+ "left join he_carteraversion cv on cv.codcarteraversion= jv.codcarteraversion "
						+ "left join he_metodologiacartera m on m.codmetodologia = cv.codmetodologia "
						+ "left join he_cartera c on c.codcartera= m.codcartera "
						+ "where e.tipestadoejecucion in (2,4) and e.tipestadovalidacion=4 " + "and c.codcartera="
						+ cartera_progreso.getId_cartera() + " and e.codmes=" + filtroProgreso.getFecha().getCodmes()
						+ " and e.codtipoejecucion in "
						+ ((filtroProgreso.getFecha().getFase().equals(1)) ? "(1,7)" : "(2,8)"); // Incluye inversiones

				try {
					conn = obtenerConexion();

					// =========================
					// ==========Procesar y Aprobar Ficheros
					ArrayList<Aprovisionamiento> aprovs_obligatorios = new ArrayList<Aprovisionamiento>();
					Aprovisionamiento aprov_obligatorio = null;
					ArrayList<Aprovisionamiento> cargas_totales_procesadas_aprov = new ArrayList<Aprovisionamiento>();
					Aprovisionamiento carga_total_procesada_aprov = null;
					ArrayList<Aprovisionamiento> cargas_totales_aprobadas_aprov = new ArrayList<Aprovisionamiento>();
					Aprovisionamiento carga_total_aprobada_aprov = null;

					// -- Obtencion de ficheros obligatorios
					stmt_total = crearStatement(conn);
					rs_total = stmt_total.executeQuery(sql_total_ficheros_obligatorios);

					while (rs_total.next()) {
						aprov_obligatorio = new Aprovisionamiento();
						aprov_obligatorio.setId_aprovisionamiento(rs_total.getInt("CODAPROVISIONAMIENTO"));
						aprov_obligatorio.setNombre_fichero(rs_total.getString("NBRFICHERO"));
						aprovs_obligatorios.add(aprov_obligatorio);
					}

					cerrarStatement(stmt_total);
					cerrarResultSet(rs_total);

					if (aprovs_obligatorios.size() > 0) {
						// -- Obtencion de cargas Procesadas
						stmt = crearStatement(conn);
						rs = stmt.executeQuery(sql_cargasDeCartera);
						while (rs.next()) {
							carga_total_procesada_aprov = new Aprovisionamiento();
							carga_total_procesada_aprov.setId_aprovisionamiento(rs.getInt("CODAPROVISIONAMIENTO"));
							carga_total_procesada_aprov.setNombre_fichero(rs.getString("NBRFICHERO"));
							cargas_totales_procesadas_aprov.add(carga_total_procesada_aprov); // aprov_obligatorio
						}
						cerrarStatement(stmt);
						cerrarResultSet(rs);
						// -- Obtencion de cargas Aprobadas
						stmt = crearStatement(conn);
						rs = stmt.executeQuery(sql_cargasAprobadasDeCartera);
						while (rs.next()) {
							carga_total_aprobada_aprov = new Aprovisionamiento();
							carga_total_aprobada_aprov.setId_aprovisionamiento(rs.getInt("CODAPROVISIONAMIENTO"));
							carga_total_aprobada_aprov.setNombre_fichero(rs.getString("NBRFICHERO"));
							cargas_totales_aprobadas_aprov.add(carga_total_aprobada_aprov);
						}
						cerrarStatement(stmt);
						cerrarResultSet(rs);

						// Comparacion de Totales contra cargados y aprobados
						ArrayList<Aprovisionamiento> cargas_realizadas_procesadas_aprov = new ArrayList<Aprovisionamiento>();
						ArrayList<Aprovisionamiento> cargas_realizadas_aprobadas_aprov = new ArrayList<Aprovisionamiento>();
						for (Aprovisionamiento aprov_oblig : aprovs_obligatorios) {
							Boolean aprov_obligatorio_procesado_encontrado = false;
							Boolean aprov_obligatorio_aprobado_encontrado = false;
							// ------ Procesamiento de Ficheros
							for (Aprovisionamiento aprov_procesado : cargas_totales_procesadas_aprov) {
								if (aprov_procesado.getId_aprovisionamiento()
										.equals(aprov_oblig.getId_aprovisionamiento())) {
									aprov_obligatorio_procesado_encontrado = true;
								}
							}
							if (aprov_obligatorio_procesado_encontrado) {
								cargas_realizadas_procesadas_aprov.add(aprov_oblig);
							}
							// ------ Aprobación de Ficheros
							for (Aprovisionamiento aprov_aprobado : cargas_totales_aprobadas_aprov) {
								if (aprov_aprobado.getId_aprovisionamiento()
										.equals(aprov_oblig.getId_aprovisionamiento())) {
									aprov_obligatorio_aprobado_encontrado = true;
								}
							}
							if (aprov_obligatorio_aprobado_encontrado) {
								cargas_realizadas_aprobadas_aprov.add(aprov_oblig);
							}
						}
						// ------ Procesamiento de Ficheros
						Integer progreso_procesadas = 100 * cargas_realizadas_procesadas_aprov.size()
								/ aprovs_obligatorios.size();
						cartera_progreso.getProgresosPuntoProgreso().get(0).setPorcentage_progreso(progreso_procesadas);
						if (progreso_procesadas.equals(100)) {
							cartera_progreso.getProgresosPuntoProgreso().get(0)
									.setDetalle_punto_progreso("Progreso de Carga completa para la fecha");
						} else if (progreso_procesadas.equals(0)) {
							cartera_progreso.getProgresosPuntoProgreso().get(0)
									.setDetalle_punto_progreso("No se tiene ninguna Carga provisionada para la fecha");
						} else {
							cartera_progreso.getProgresosPuntoProgreso().get(0).setDetalle_punto_progreso(
									"Se tiene un progreso parcial de la carga de ficheros para la fecha");
						}
						// ------ Aprobación de Ficheros
						Integer progreso_aprobadas = 100 * cargas_realizadas_aprobadas_aprov.size()
								/ aprovs_obligatorios.size();
						cartera_progreso.getProgresosPuntoProgreso().get(1).setPorcentage_progreso(progreso_aprobadas);
						if (progreso_aprobadas.equals(100)) {
							cartera_progreso.getProgresosPuntoProgreso().get(1)
									.setDetalle_punto_progreso("Progreso de aprobación completa para la fecha");
						} else if (progreso_aprobadas.equals(0)) {
							cartera_progreso.getProgresosPuntoProgreso().get(1)
									.setDetalle_punto_progreso("No se tiene ninguna Carga aprobada para la fecha");
						} else {
							cartera_progreso.getProgresosPuntoProgreso().get(1).setDetalle_punto_progreso(
									"Se tiene un progreso parcial de la aprobación de ficheros para la fecha");
						}
					} else {
						// ------ Procesamiento de Ficheros
						cartera_progreso.getProgresosPuntoProgreso().get(0).setPorcentage_progreso(0);
						cartera_progreso.getProgresosPuntoProgreso().get(0).setDetalle_punto_progreso(
								"No existen Ficheros Obligatorios Configurados para esta Cartera");
						// ------ Aprobación de Ficheros
						cartera_progreso.getProgresosPuntoProgreso().get(1).setPorcentage_progreso(0);
						cartera_progreso.getProgresosPuntoProgreso().get(1).setDetalle_punto_progreso(
								"No existen Ficheros Obligatorios Configurados para esta Cartera");
					}

					// =========================
					// ========== Ejecutar, Validar, Aprobar resultados

					// ------ Ejecutar
					stmt = crearStatement(conn);
					rs = stmt.executeQuery(sql_ejecutadas);
					ArrayList<EjecucionPE> eject_ejecutadas = new ArrayList<EjecucionPE>();
					EjecucionPE ejecucionPE = null;
					while (rs.next()) {
						ejecucionPE = new EjecucionPE();
						ejecucionPE.setId_ejecucion(rs.getInt("CodEjecucion"));
						ejecucionPE.setCodmes(rs.getInt("CodMes"));
						eject_ejecutadas.add(ejecucionPE);
					}
					cerrarStatement(stmt);
					cerrarResultSet(rs);
					if (eject_ejecutadas.size() > 0) {
						cartera_progreso.getProgresosPuntoProgreso().get(2).setPorcentage_progreso(100);
						cartera_progreso.getProgresosPuntoProgreso().get(2).setDetalle_punto_progreso(
								"se encontraron un total de: " + eject_ejecutadas.size() + " ejecuciones de PE");
					} else {
						cartera_progreso.getProgresosPuntoProgreso().get(2).setPorcentage_progreso(0);
						cartera_progreso.getProgresosPuntoProgreso().get(2)
								.setDetalle_punto_progreso("No se encontró ninguna ejecución para la cartera");
					}

					// ------ Validar
					stmt = crearStatement(conn);
					rs = stmt.executeQuery(sql_validadas);
					ArrayList<EjecucionPE> eject_validadas = new ArrayList<EjecucionPE>();
					while (rs.next()) {
						ejecucionPE = new EjecucionPE();
						ejecucionPE.setId_ejecucion(rs.getInt("CodEjecucion"));
						ejecucionPE.setCodmes(rs.getInt("CodMes"));
						eject_validadas.add(ejecucionPE);
					}
					cerrarStatement(stmt);
					cerrarResultSet(rs);
					if (eject_validadas.size() > 0) {
						cartera_progreso.getProgresosPuntoProgreso().get(3).setPorcentage_progreso(100);
						cartera_progreso.getProgresosPuntoProgreso().get(3)
								.setDetalle_punto_progreso("se encontraron un total de: " + eject_validadas.size()
										+ " ejecuciones de PE validadas");
					} else {
						cartera_progreso.getProgresosPuntoProgreso().get(3).setPorcentage_progreso(0);
						cartera_progreso.getProgresosPuntoProgreso().get(3)
								.setDetalle_punto_progreso("No se encontró ninguna ejecución para la cartera");
					}
					// ------ Aprobado
					stmt = crearStatement(conn);
					rs = stmt.executeQuery(sql_aprobadas);
					ArrayList<EjecucionPE> eject_aprobadas = new ArrayList<EjecucionPE>();
					while (rs.next()) {
						ejecucionPE = new EjecucionPE();
						ejecucionPE.setId_ejecucion(rs.getInt("CodEjecucion"));
						ejecucionPE.setCodmes(rs.getInt("CodMes"));
						eject_aprobadas.add(ejecucionPE);
					}
					cerrarStatement(stmt);
					cerrarResultSet(rs);
					if (eject_aprobadas.size() > 0) {
						cartera_progreso.getProgresosPuntoProgreso().get(4).setPorcentage_progreso(100);
						cartera_progreso.getProgresosPuntoProgreso().get(4)
								.setDetalle_punto_progreso("se encontraron un total de: " + eject_aprobadas.size()
										+ " ejecuciones de PE aprobadas");
					} else {
						cartera_progreso.getProgresosPuntoProgreso().get(4).setPorcentage_progreso(0);
						cartera_progreso.getProgresosPuntoProgreso().get(4)
								.setDetalle_punto_progreso("No se encontró ninguna ejecución para la cartera");
					}

					// --- Lógica final :
					if (cartera_progreso.getProgresosPuntoProgreso().get(0).getPorcentage_progreso().equals(100)) {
						if (cartera_progreso.getProgresosPuntoProgreso().get(1).getPorcentage_progreso().equals(100)) {

							/*
							 * if
							 * (cartera_progreso.getProgresosPuntoProgreso().get(2).getPorcentage_progreso()
							 * .equals(100)) { if
							 * (cartera_progreso.getProgresosPuntoProgreso().get(3).getPorcentage_progreso()
							 * .equals(100)) { if
							 * (cartera_progreso.getProgresosPuntoProgreso().get(4).getPorcentage_progreso()
							 * .equals(100)) { cartera_progreso.setProgreso(5); } else {
							 * cartera_progreso.setProgreso(4); } } else { cartera_progreso.setProgreso(3);
							 * } } else { cartera_progreso.setProgreso(2); }
							 */

							// Esta parte debe ser inverso
							if (cartera_progreso.getProgresosPuntoProgreso().get(4).getPorcentage_progreso()
									.equals(100)) {
								cartera_progreso.setProgreso(5);
							} else if (cartera_progreso.getProgresosPuntoProgreso().get(3).getPorcentage_progreso()
									.equals(100)) {
								cartera_progreso.setProgreso(4);
							} else if (cartera_progreso.getProgresosPuntoProgreso().get(2).getPorcentage_progreso()
									.equals(100)) {
								cartera_progreso.setProgreso(3);
							} else {
								cartera_progreso.setProgreso(2);
							}
						} else {
							cartera_progreso.setProgreso(1);
						}
					} else {
						cartera_progreso.setProgreso(0);
					}
				} catch (Exception ex) {
					LOGGER.error("Error al obtener el progreso de entidades: " + ex.getMessage());
					throw ex;
				} finally {
					cerrarResultSet(rs);
					cerrarStatement(stmt);
					cerrarConexion(conn);
				}
			}
		}
		return entidades_progresos;
	}

	// -------------------------------------------------------------------
	// ----------------------------EJECUCIONES PE----------------------------
	// -------------------------------------------------------------------
	/*
	 * public ArrayList<EjecucionPE> obtenerEjecucionesPE_fase1() throws Exception {
	 * return this.obtenerEjecucionesPE(1); }
	 * 
	 * public ArrayList<EjecucionPE> obtenerEjecucionesPE_fase2() throws Exception {
	 * return this.obtenerEjecucionesPE(2); }
	 * 
	 * public ArrayList<EjecucionPE> obtenerEjecucionesPE(Integer fase) throws
	 * Exception { ArrayList<EjecucionPE> ejecucionesPE = new
	 * ArrayList<EjecucionPE>(); EjecucionPE ejecucionPE = null; Escenario_version
	 * escenario_version = null; JuegoEscenarios_version juegoEscenarios_version =
	 * null; Escenario escenario = null; Cartera_version cartera_version = null;
	 * EstructuraMetodologica metodologia = null; Cartera cartera = null; Entidad
	 * entidad = null;
	 * 
	 * Connection conn = null; Statement stmt = null; ResultSet rs = null; String
	 * sql =
	 * "select EJ.CodEjecucion, EJ.CodMes, EJ.TipEstadoEjecucion, EJ.FecEjecucion, EV.CodEscenarioVersion,"
	 * +
	 * " JEV.NbrJuegoEscenarios, JEV.CodJuegoEscenarios, JEV.DesJuegoEscenarios, E.CodEscenario, E.NbrEscenario, E.NbrVariableMotor as NbrVariableMotorEsc"
	 * +
	 * " CV.CodCarteraVersion, CV.NbrVersion, MC.CodMetodologia, MC.NbrMetodologia, C.CodCartera, C.NbrCartera,"
	 * + " EN.CodEntidad, EN.NbrEntidad from HE_Ejecucion EJ" +
	 * " left join HE_EscenarioVersion EV on EJ.CodEscenarioVersion = EV.CodEscenarioVersion"
	 * + " left join ME_Escenario E on EV.CodEscenario = E.CodEscenario" +
	 * " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios"
	 * +
	 * " left join HE_CarteraVersion CV on JEV.CodCarteraVersion= CV.CodCarteraVersion"
	 * +
	 * " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia"
	 * + " left join HE_Cartera C on MC.CodCartera= C.CodCartera" +
	 * " left join HE_Entidad EN on C.CodEntidad = EN.CodEntidad" +
	 * " where EJ.CodTipoEjecucion = " + fase + " order by EJ.CodMes desc"; try {
	 * conn = obtenerConexion(); stmt = crearStatement(conn); rs =
	 * stmt.executeQuery(sql); while (rs.next()) { ejecucionPE = new EjecucionPE();
	 * juegoEscenarios_version = new JuegoEscenarios_version(); escenario_version =
	 * new Escenario_version(); escenario = new Escenario(); cartera_version = new
	 * Cartera_version(); metodologia = new EstructuraMetodologica(); cartera = new
	 * Cartera(); entidad = new Entidad();
	 * 
	 * ejecucionPE.setId_ejecucion(rs.getInt("CodEjecucion"));
	 * ejecucionPE.setCodmes(rs.getInt("CodMes"));
	 * ejecucionPE.setEstado_ejecucion(rs.getInt("TipEstadoEjecucion"));
	 * ejecucionPE.setDatetime_ejecucion(rs.getLong("FecEjecucion"));
	 * escenario_version.setId_escenario_version(rs.getInt("CodEscenarioVersion"));
	 * juegoEscenarios_version.setNombre_juego_escenarios(rs.getString(
	 * "NbrJuegoEscenarios"));
	 * juegoEscenarios_version.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"
	 * )); juegoEscenarios_version.setDescripcion_juego_escenarios(rs.getString(
	 * "DesJuegoEscenarios")); escenario.setId_escenario(rs.getInt("CodEscenario"));
	 * escenario.setNombre_escenario(rs.getString("NbrEscenario"));
	 * escenario.setNombre_variable_motor(rs.getString("NbrVariableMotorEsc"));
	 * cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
	 * cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));
	 * metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
	 * metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
	 * cartera.setId_cartera(rs.getInt("CodCartera"));
	 * cartera.setNombre_cartera(rs.getString("NbrCartera"));
	 * entidad.setId_entidad(rs.getInt("CodEntidad"));
	 * entidad.setNombre_entidad(rs.getString("NbrEntidad"));
	 * 
	 * escenario_version.setEscenario(escenario); cartera.setEntidad(entidad);
	 * metodologia.setCartera(cartera); cartera_version.setMetodologia(metodologia);
	 * juegoEscenarios_version.setCartera_version(cartera_version);
	 * escenario_version.setJuego_escenarios_version(juegoEscenarios_version);
	 * ejecucionPE.setEscenario_version(escenario_version);
	 * ejecucionesPE.add(ejecucionPE); } } catch (Exception ex) {
	 * LOGGER.error("Error al obtener ejecuciones PE de fase " + fase + " : " +
	 * ex.getMessage()); throw ex; } finally { cerrarResultSet(rs);
	 * cerrarStatement(stmt); cerrarConexion(conn); } return ejecucionesPE; }
	 * 
	 * public ArrayList<TablaMapeoEjecucion>
	 * obtenerTablasMapeoEjecucionPorIdEjecucion(Integer id_ejecucion) throws
	 * Exception { ArrayList<TablaMapeoEjecucion> tablas_mapeo_ejecucion = null;
	 * TablaMapeoEjecucion tabla_mapeo_ejecucion = null; TablaInput tabla_input =
	 * null; CargaFichero carga = null; Fecha fecha = null; ArrayList<CargaFichero>
	 * opciones_carga = null; CargaFichero opcion_carga = null; ArrayList<Integer>
	 * opciones_codmes = null; Integer opcion_codmes = null;
	 * 
	 * Connection conn = null; Statement stmt = null; ResultSet rs = null;
	 * 
	 * String sql = "select ME.CodMapeoTabla," +
	 * " TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata," +
	 * " A.CodAprovisionamiento, A.NbrFichero," +
	 * " CF.CodCargaFichero, CF.NbrCarga, CF.DesComentarioCalidad, CF.NumVersionCarga,"
	 * + " F.CodFecha, F.NumCodMes" + " from HE_MapeoEjecucion ME" +
	 * " left join HE_MapeoTablaBloque MTB on ME.CodMapeoTabla = MTB.CodMapeoTabla"
	 * + " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput" +
	 * " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
	 * + " left join HE_CargaFichero CF on CF.CodCargaFichero = ME.CodCargaFichero"
	 * + " left join HE_Fecha F on CF.CodFecha = F.CodFecha" +
	 * " where ME.CodEjecucion = " + id_ejecucion;
	 * 
	 * Statement stmt_opcionesCarga = null; ResultSet rs_opcionesCarga = null;
	 * String sql_opcionesCarga = null;
	 * 
	 * Statement stmt_opcionesCodmes = null; ResultSet rs_opcionesCodmes = null;
	 * String sql_opcionesCodmes = null;
	 * 
	 * try { conn = obtenerConexion(); stmt = crearStatement(conn); rs =
	 * stmt.executeQuery(sql); tablas_mapeo_ejecucion = new
	 * ArrayList<TablaMapeoEjecucion>();
	 * 
	 * while (rs.next()) { tabla_mapeo_ejecucion = new TablaMapeoEjecucion();
	 * tabla_mapeo_ejecucion.setId_mapeo_tabla(rs.getInt("CodMapeoTabla"));
	 * 
	 * tabla_input = new TablaInput();
	 * tabla_input.setId_tablainput(rs.getInt("CodTablaInput"));
	 * tabla_input.setVersion_metadata(rs.getInt("NumVersionMetadata"));
	 * tabla_input.setNombre_metadata(rs.getString("NbrMetadata"));
	 * tabla_input.setId_aprovisionamiento(rs.getInt("CodAprovisionamiento"));
	 * tabla_input.setNombre_fichero(rs.getString("NbrFichero"));
	 * tabla_mapeo_ejecucion.setTabla_input(tabla_input);
	 * 
	 * carga = new CargaFichero();
	 * carga.setId_cargafichero(rs.getInt("CodCargaFichero"));
	 * carga.setVersion_carga(rs.getInt("NumVersionCarga"));
	 * carga.setNombre_carga(rs.getString("NbrCarga"));
	 * carga.setComentario_calidad(rs.getString("DesComentarioCalidad"));
	 * 
	 * fecha = new Fecha(); fecha.setId_fecha(rs.getInt("CodFecha"));
	 * fecha.setCodmes(rs.getInt("NumCodMes")); carga.setFecha(fecha);
	 * tabla_mapeo_ejecucion.setCarga(carga);
	 * 
	 * // Opciones de carga stmt_opcionesCarga = crearStatement(conn);
	 * sql_opcionesCarga = "select CF.CodCargaFichero, CF.NumVersionCarga," +
	 * " CF.NbrCarga, CF.DesComentarioCalidad," + " F.CodFecha, F.NumCodMes" +
	 * " from HE_CargaFichero CF" + " join HE_Fecha F on CF.CodFecha = F.CodFecha" +
	 * " where CF.CodTablaInput = " + tabla_input.getId_tablainput();
	 * rs_opcionesCarga = stmt_opcionesCarga.executeQuery(sql_opcionesCarga);
	 * 
	 * opciones_carga = new ArrayList<CargaFichero>(); while
	 * (rs_opcionesCarga.next()) { opcion_carga = new CargaFichero();
	 * opcion_carga.setId_cargafichero(rs_opcionesCarga.getInt("CodCargaFichero"));
	 * opcion_carga.setVersion_carga(rs_opcionesCarga.getInt("NumVersionCarga"));
	 * opcion_carga.setNombre_carga(rs_opcionesCarga.getString("NbrCarga"));
	 * opcion_carga.setComentario_calidad(rs_opcionesCarga.getString(
	 * "DesComentarioCalidad"));
	 * 
	 * fecha = new Fecha(); fecha.setId_fecha(rs_opcionesCarga.getInt("CodFecha"));
	 * fecha.setCodmes(rs_opcionesCarga.getInt("NumCodMes"));
	 * opcion_carga.setFecha(fecha);
	 * 
	 * opciones_carga.add(opcion_carga); }
	 * tabla_mapeo_ejecucion.setOpciones_carga(opciones_carga);
	 * 
	 * // Opciones de codmes stmt_opcionesCodmes = crearStatement(conn);
	 * sql_opcionesCodmes = "select distinct F.NumCodMes" + " from HE_Fecha F" +
	 * " left join HE_CargaFichero CF on F.CodFecha = CF.CodFecha" +
	 * " where CF.CodTablaInput = " + tabla_input.getId_tablainput() +
	 * " order by F.NumCodMes asc"; rs_opcionesCodmes =
	 * stmt_opcionesCodmes.executeQuery(sql_opcionesCodmes);
	 * 
	 * opciones_codmes = new ArrayList<Integer>(); while (rs_opcionesCodmes.next())
	 * { opcion_codmes = rs_opcionesCodmes.getInt("NumCodMes");
	 * opciones_codmes.add(opcion_codmes); }
	 * tabla_mapeo_ejecucion.setOpciones_codmes(opciones_codmes);
	 * 
	 * tablas_mapeo_ejecucion.add(tabla_mapeo_ejecucion); }
	 * 
	 * } catch (Exception ex) {
	 * LOGGER.error("Error al obtener tablas de mapeo para la ejecucion con id " +
	 * id_ejecucion + " : " + ex.getMessage()); throw ex; } finally {
	 * cerrarStatement(stmt); } return tablas_mapeo_ejecucion; }
	 * 
	 * public ArrayList<TablaMapeoEjecucion>
	 * obtenerTablasMapeoEjecucionPorIdEscenarioVersion(Integer
	 * id_escenario_version) throws Exception { ArrayList<TablaMapeoEjecucion>
	 * tablas_mapeo_ejecucion = null; TablaMapeoEjecucion tabla_mapeo_ejecucion =
	 * null; TablaInput tabla_input = null; ArrayList<CargaFichero> opciones_carga =
	 * null; CargaFichero opcion_carga = null; ArrayList<Integer> opciones_codmes =
	 * null; Integer opcion_codmes = null; Fecha fecha = null;
	 * 
	 * Connection conn = null; Statement stmt = null; ResultSet rs = null;
	 * 
	 * String sql = "select MTB.CodMapeoTabla," +
	 * " TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata, " +
	 * " A.CodAprovisionamiento, A.NbrFichero" + " from HE_MapeoTablaBloque MTB " +
	 * " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput" +
	 * " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
	 * + " where MTB.CodEscenarioVersion= " + id_escenario_version +
	 * " or MTB.CodEscenarioVersion is null and CodCarteraVersion in" +
	 * " (select CodCarteraVersion from HE_EscenarioVersion where CodEscenarioVersion = "
	 * + id_escenario_version + ")";
	 * 
	 * Statement stmt_opcionesCarga = null; ResultSet rs_opcionesCarga = null;
	 * String sql_opcionesCarga = null;
	 * 
	 * Statement stmt_opcionesCodmes = null; ResultSet rs_opcionesCodmes = null;
	 * String sql_opcionesCodmes = null;
	 * 
	 * try { conn = obtenerConexion(); stmt = crearStatement(conn); rs =
	 * stmt.executeQuery(sql); tablas_mapeo_ejecucion = new
	 * ArrayList<TablaMapeoEjecucion>();
	 * 
	 * while (rs.next()) { tabla_mapeo_ejecucion = new TablaMapeoEjecucion();
	 * tabla_input = new TablaInput();
	 * 
	 * tabla_mapeo_ejecucion.setId_mapeo_tabla(rs.getInt("CodMapeoTabla"));
	 * tabla_input.setId_tablainput(rs.getInt("CodTablaInput"));
	 * tabla_input.setVersion_metadata(rs.getInt("NumVersionMetadata"));
	 * tabla_input.setNombre_metadata(rs.getString("NbrMetadata"));
	 * tabla_input.setId_aprovisionamiento(rs.getInt("CodAprovisionamiento"));
	 * tabla_input.setNombre_fichero(rs.getString("NbrFichero"));
	 * tabla_mapeo_ejecucion.setTabla_input(tabla_input);
	 * 
	 * // Carga aun no seleccionada
	 * 
	 * // Opciones de carga stmt_opcionesCarga = crearStatement(conn);
	 * sql_opcionesCarga = "select CF.CodCargaFichero, CF.NumVersionCarga," +
	 * " CF.NbrCarga, CF.DesComentarioCalidad," + " F.CodFecha, F.NumCodMes" +
	 * " from HE_CargaFichero CF" + " join HE_Fecha F on CF.CodFecha = F.CodFecha" +
	 * " where CF.CodTablaInput = " + tabla_input.getId_tablainput();
	 * rs_opcionesCarga = stmt_opcionesCarga.executeQuery(sql_opcionesCarga);
	 * 
	 * opciones_carga = new ArrayList<CargaFichero>(); while
	 * (rs_opcionesCarga.next()) { opcion_carga = new CargaFichero();
	 * opcion_carga.setId_cargafichero(rs_opcionesCarga.getInt("CodCargaFichero"));
	 * opcion_carga.setVersion_carga(rs_opcionesCarga.getInt("NumVersionCarga"));
	 * opcion_carga.setNombre_carga(rs_opcionesCarga.getString("NbrCarga"));
	 * opcion_carga.setComentario_calidad(rs_opcionesCarga.getString(
	 * "DesComentarioCalidad"));
	 * 
	 * fecha = new Fecha(); fecha.setId_fecha(rs_opcionesCarga.getInt("CodFecha"));
	 * fecha.setCodmes(rs_opcionesCarga.getInt("NumCodMes"));
	 * opcion_carga.setFecha(fecha);
	 * 
	 * opciones_carga.add(opcion_carga); }
	 * tabla_mapeo_ejecucion.setOpciones_carga(opciones_carga);
	 * 
	 * // Opciones de codmes stmt_opcionesCodmes = crearStatement(conn);
	 * sql_opcionesCodmes = "select distinct F.NumCodMes" + " from HE_Fecha F" +
	 * " left join HE_CargaFichero CF on F.CodFecha = CF.CodFecha" +
	 * " where CF.CodTablaInput = " + tabla_input.getId_tablainput() +
	 * " order by F.NumCodMes asc"; rs_opcionesCodmes =
	 * stmt_opcionesCodmes.executeQuery(sql_opcionesCodmes);
	 * 
	 * opciones_codmes = new ArrayList<Integer>(); while (rs_opcionesCodmes.next())
	 * { opcion_codmes = rs_opcionesCodmes.getInt("NumCodMes");
	 * opciones_codmes.add(opcion_codmes); }
	 * tabla_mapeo_ejecucion.setOpciones_codmes(opciones_codmes);
	 * 
	 * tablas_mapeo_ejecucion.add(tabla_mapeo_ejecucion); }
	 * 
	 * } catch (Exception ex) { LOGGER.
	 * error("Error al obtener tablas de mapeo para el escenario version con id " +
	 * id_escenario_version + " : " + ex.getMessage()); throw ex; } finally {
	 * cerrarStatement(stmt); } return tablas_mapeo_ejecucion; }
	 */

	public ArrayList<EstructuraMetodologica> obtenerEstructurasCalcPePorIdCartera(Integer id_cartera) throws Exception {
		ArrayList<EstructuraMetodologica> estructuras = null;
		EstructuraMetodologica estructura = null;
		TipoFlujo flujo = null;
		TipoObtencionResultados tipoObtencionResultados = null;
		Cartera cartera = null;
		Entidad entidad = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "Select Mc.Codmetodologia, Mc.Nbrmetodologia, Mc.Desmetodologia,Mc.Codtipoflujo, Mc.Codtipoobtenresult, "
				+ " C.Codcartera, C.Nbrcartera, E.Codentidad, E.Nbrentidad, Tf.NbrTipoFlujo "
				+ " From He_Metodologiacartera Mc " + " Left Join ME_TipoFlujo Tf on Mc.CodTipoFlujo = Tf.CodTipoFlujo "
				+ " Left Join He_Cartera C On Mc.Codcartera= C.Codcartera "
				+ " Left Join He_Entidad E On C.Codentidad= E.Codentidad "
				+ " Where Tf.NbrTipoFlujo not in ('Otros', 'Presupuesto') and Mc.Codcartera=" + id_cartera;
		// No consideramos estructuras de flujo: otros o presupuestos

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			estructuras = new ArrayList<EstructuraMetodologica>();

			while (rs.next()) {
				estructura = new EstructuraMetodologica();
				estructura.setId_metodologia(rs.getInt("CodMetodologia"));
				estructura.setNombre_metodologia(rs.getString("NbrMetodologia"));
				estructura.setDescripcion_metodologia(rs.getString("DesMetodologia"));
				flujo = new TipoFlujo();
				flujo.setId_tipo_flujo(rs.getInt("Codtipoflujo"));
				flujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));
				estructura.setFlujo(flujo);
				tipoObtencionResultados = new TipoObtencionResultados();
				tipoObtencionResultados.setId_tipo_obtencion_resultados(rs.getInt("Codtipoobtenresult"));
				estructura.setMetodo_resultados(tipoObtencionResultados);
				estructura.setCarteras_extrapolacion(
						obtenerCarterasExtrapolacionporIdMetodologia(estructura.getId_metodologia(), conn));
				cartera = new Cartera();
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));

				entidad = new Entidad();
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				cartera.setEntidad(entidad);

				estructura.setCartera(cartera);

				estructuras.add(estructura);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener estructuras de calc. PE para la cartera con id " + id_cartera + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return estructuras;
	}

	public ArrayList<EstructuraMetodologica> obtenerEstructurasPorIdCartera(Integer id_cartera) throws Exception {
		ArrayList<EstructuraMetodologica> estructuras = null;
		EstructuraMetodologica estructura = null;
		TipoFlujo flujo = null;
		TipoObtencionResultados tipoObtencionResultados = null;
		Cartera cartera = null;
		Entidad entidad = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "Select Mc.Codmetodologia, Mc.Nbrmetodologia, Mc.Desmetodologia,Mc.Codtipoflujo, Mc.Codtipoobtenresult, "
				+ " C.Codcartera, C.Nbrcartera, E.Codentidad, E.Nbrentidad, Tf.NbrTipoFlujo "
				+ " From He_Metodologiacartera Mc " + " Left Join ME_TipoFlujo Tf on Mc.CodTipoFlujo = Tf.CodTipoFlujo "
				+ " Left Join He_Cartera C On Mc.Codcartera= C.Codcartera "
				+ " Left Join He_Entidad E On C.Codentidad= E.Codentidad " + " Where Mc.Codcartera=" + id_cartera;
		// No consideramos estructuras de flujo: otros o presupuestos

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			estructuras = new ArrayList<EstructuraMetodologica>();

			while (rs.next()) {
				estructura = new EstructuraMetodologica();
				estructura.setId_metodologia(rs.getInt("CodMetodologia"));
				estructura.setNombre_metodologia(rs.getString("NbrMetodologia"));
				estructura.setDescripcion_metodologia(rs.getString("DesMetodologia"));
				flujo = new TipoFlujo();
				flujo.setId_tipo_flujo(rs.getInt("Codtipoflujo"));
				flujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));
				estructura.setFlujo(flujo);
				tipoObtencionResultados = new TipoObtencionResultados();
				tipoObtencionResultados.setId_tipo_obtencion_resultados(rs.getInt("Codtipoobtenresult"));
				estructura.setMetodo_resultados(tipoObtencionResultados);
				estructura.setCarteras_extrapolacion(
						obtenerCarterasExtrapolacionporIdMetodologia(estructura.getId_metodologia(), conn));
				cartera = new Cartera();
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));

				entidad = new Entidad();
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				cartera.setEntidad(entidad);

				estructura.setCartera(cartera);

				estructuras.add(estructura);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener estructuras para la cartera con id " + id_cartera + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return estructuras;
	}

	public ArrayList<Cartera_version> obtenerCarterasVersionPorIdMetodologia(Integer id_metodologia) throws Exception {
		ArrayList<Cartera_version> carterasVersion = null;
		Cartera_version carteraVersion = null;
		EstructuraMetodologica estructura = null;
		TipoAmbito tipoAmbito = null;
		Cartera cartera = null;
		Entidad entidad = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "select CV.CodCarteraVersion, CV.NbrVersion, CV.Desversion,"
				+ " MC.CodMetodologia, MC.NbrMetodologia, MC.DesMetodologia, "
				+ " C.CodCartera, C.NbrCartera, TA.CodTipoAmbito, TA.NbrTipoAmbito, " + " E.CodEntidad, E.NbrEntidad"
				+ " from HE_CarteraVersion CV "
				+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia= MC.CodMetodologia"
				+ " left join HE_Cartera C on MC.CodCartera= C.CodCartera "
				+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
				+ " left join HE_Entidad E on C.CodEntidad= E.CodEntidad" + " where CV.CodMetodologia= "
				+ id_metodologia;

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			carterasVersion = new ArrayList<Cartera_version>();

			while (rs.next()) {
				carteraVersion = new Cartera_version();
				estructura = new EstructuraMetodologica();
				tipoAmbito = new TipoAmbito();
				cartera = new Cartera();
				entidad = new Entidad();
				carteraVersion.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				carteraVersion.setNombre_cartera_version(rs.getString("NbrVersion"));
				carteraVersion.setDescripcion(rs.getString("Desversion"));
				estructura.setId_metodologia(rs.getInt("CodMetodologia"));
				estructura.setNombre_metodologia(rs.getString("NbrMetodologia"));
				estructura.setDescripcion_metodologia(rs.getString("DesMetodologia"));
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));

				cartera.setEntidad(entidad);
				cartera.setTipo_ambito(tipoAmbito);
				estructura.setCartera(cartera);
				carteraVersion.setMetodologia(estructura);
				carterasVersion.add(carteraVersion);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener carteras version para la estructura con id " + id_metodologia + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return carterasVersion;
	}

	public ArrayList<JuegoEscenarios_version> obtenerJuegosEscenariosVersionPorIdCarteraVersion(
			Integer id_cartera_version) throws Exception {
		ArrayList<JuegoEscenarios_version> juegosEscenariosVersion = null;
		JuegoEscenarios_version juegoEscenariosVersion = null;
		Cartera_version carteraVersion = null;
		EstructuraMetodologica estructura = null;
		Cartera cartera = null;
		Entidad entidad = null;
		TipoFlujo flujo = null;
		TipoAmbito tipoAmbito = null;
		TipoObtencionResultados tipoObtencionResultados = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "select JEV.CodJuegoEscenarios, JEV.NbrJuegoEscenarios, JEV.DesJuegoEscenarios, "
				+ " CV.CodCarteraVersion, CV.NbrVersion, CV.DesVersion, MC.CodMetodologia, MC.NbrMetodologia,"
				+ " MC.DesMetodologia, MC.CodTipoFlujo, MC.CodTipoObtenResult, "
				+ " C.CodCartera, C.NbrCartera, TA.CodTipoAmbito, TA.NbrTipoAmbito, " + " E.CodEntidad, E.NbrEntidad"
				+ " from HE_JuegoEscenariosVersion JEV "
				+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion"
				+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
				+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
				+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad" + " where JEV.CodCarteraVersion = "
				+ id_cartera_version;

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			juegosEscenariosVersion = new ArrayList<JuegoEscenarios_version>();

			while (rs.next()) {
				juegoEscenariosVersion = new JuegoEscenarios_version();
				carteraVersion = new Cartera_version();
				estructura = new EstructuraMetodologica();
				cartera = new Cartera();
				entidad = new Entidad();

				juegoEscenariosVersion.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenariosVersion.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenariosVersion.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));

				carteraVersion.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				carteraVersion.setNombre_cartera_version(rs.getString("NbrVersion"));
				carteraVersion.setDescripcion(rs.getString("DesVersion"));

				estructura.setId_metodologia(rs.getInt("CodMetodologia"));
				estructura.setNombre_metodologia(rs.getString("NbrMetodologia"));
				estructura.setDescripcion_metodologia(rs.getString("DesMetodologia"));

				flujo = new TipoFlujo();
				flujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				estructura.setFlujo(flujo);

				tipoObtencionResultados = new TipoObtencionResultados();
				tipoObtencionResultados.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
				estructura.setMetodo_resultados(tipoObtencionResultados);
				estructura.setCarteras_extrapolacion(
						obtenerCarterasExtrapolacionporIdMetodologia(estructura.getId_metodologia(), conn));
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				cartera.setEntidad(entidad);

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				estructura.setCartera(cartera);
				carteraVersion.setMetodologia(estructura);
				juegoEscenariosVersion.setCartera_version(carteraVersion);
				juegosEscenariosVersion.add(juegoEscenariosVersion);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener juegos de escenarios version para la cartera version con id "
					+ id_cartera_version + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return juegosEscenariosVersion;
	}

	public ArrayList<EjecucionPrimerPaso> obtenerEjecucionesModMacPorMetodologia(Integer id_metodologia)
			throws Exception {
		ArrayList<EjecucionPrimerPaso> ejecucionesModMac = new ArrayList<EjecucionPrimerPaso>();
		EjecucionPrimerPaso ejecucionPrimerPaso = null;
		PrimerPaso primerPaso = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select EJ.CodEjecucion, EJ.CodMes, EJ.FecEjecucion, "
				+ " PP.CodPrimerPaso, PP.NbrPrimerPaso, PP.NbrVariableMotor, "
				+ " PP.DesPrimerPaso, PP.NumPeriodMeses, MPPM.NbrCodigoPrimerPaso " + " from HE_Ejecucion EJ"
				+ " left join HE_PrimerPaso PP on EJ.CodPrimerPaso = PP.CodPrimerPaso"
				+ " left join HE_MapeoPrimerPasoMet MPPM "
				+ " on EJ.CodPrimerPaso = MPPM.CodPrimerPaso and EJ.CodMetodologia = MPPM.CodMetodologia "
				+ " where EJ.TipEstadoEjecucion = 2 and EJ.CodTipoEjecucion = 4 and PP.CodPrimerPaso = 1"
				// Ejecutado y tipo 'primer paso' y mod macro
				+ " and EJ.CodMetodologia = " + id_metodologia + " order by EJ.CodMes desc";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ejecucionPrimerPaso = new EjecucionPrimerPaso();
				ejecucionPrimerPaso.setId_ejecucion(rs.getInt("CodEjecucion"));
				ejecucionPrimerPaso.setCodmes(rs.getInt("CodMes"));
				ejecucionPrimerPaso.setDatetime_ejecucion(rs.getLong("FecEjecucion"));

				primerPaso = new PrimerPaso();
				primerPaso.setId_primerPaso(rs.getInt("CodPrimerPaso"));
				primerPaso.setNombre_primerPaso(rs.getString("NbrPrimerPaso"));
				primerPaso.setNombre_variablemotor(rs.getString("NbrVariableMotor"));
				primerPaso.setDescripcion_primerPaso(rs.getString("DesPrimerPaso"));
				primerPaso.setCodigo_primerPaso(rs.getString("NbrCodigoPrimerPaso"));
				ejecucionPrimerPaso.setPrimerPaso(primerPaso);

				ejecucionesModMac.add(ejecucionPrimerPaso);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener ejecuciones de modelo macro por metodologia: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return ejecucionesModMac;
	}

	public ArrayList<EjecucionPrimerPaso> obtenerEjecucionesRepricingPorMetodologia(Integer id_metodologia)
			throws Exception {
		ArrayList<EjecucionPrimerPaso> ejecucionesModMac = new ArrayList<EjecucionPrimerPaso>();
		EjecucionPrimerPaso ejecucionPrimerPaso = null;
		PrimerPaso primerPaso = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select EJ.CodEjecucion, EJ.CodMes, EJ.FecEjecucion, "
				+ " PP.CodPrimerPaso, PP.NbrPrimerPaso, PP.NbrVariableMotor, "
				+ " PP.DesPrimerPaso, PP.NumPeriodMeses, MPPM.NbrCodigoPrimerPaso " + " from HE_Ejecucion EJ"
				+ " left join HE_PrimerPaso PP on EJ.CodPrimerPaso = PP.CodPrimerPaso"
				+ " left join HE_MapeoPrimerPasoMet MPPM "
				+ " on EJ.CodPrimerPaso = MPPM.CodPrimerPaso and EJ.CodMetodologia = MPPM.CodMetodologia "
				+ " where EJ.TipEstadoEjecucion = 2 and EJ.CodTipoEjecucion = 4 and PP.CodPrimerPaso = 2"
				// Ejecutado y tipo 'primer paso' y repricing
				+ " and EJ.CodMetodologia = " + id_metodologia + " order by EJ.CodMes desc";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ejecucionPrimerPaso = new EjecucionPrimerPaso();
				ejecucionPrimerPaso.setId_ejecucion(rs.getInt("CodEjecucion"));
				ejecucionPrimerPaso.setCodmes(rs.getInt("CodMes"));
				ejecucionPrimerPaso.setDatetime_ejecucion(rs.getLong("FecEjecucion"));

				primerPaso = new PrimerPaso();
				primerPaso.setId_primerPaso(rs.getInt("CodPrimerPaso"));
				primerPaso.setNombre_primerPaso(rs.getString("NbrPrimerPaso"));
				primerPaso.setNombre_variablemotor(rs.getString("NbrVariableMotor"));
				primerPaso.setDescripcion_primerPaso(rs.getString("DesPrimerPaso"));
				primerPaso.setCodigo_primerPaso(rs.getString("NbrCodigoPrimerPaso"));
				ejecucionPrimerPaso.setPrimerPaso(primerPaso);

				ejecucionesModMac.add(ejecucionPrimerPaso);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener ejecuciones de repricing por metodologia: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return ejecucionesModMac;
	}

	public Integer obtenerCodmesActual_fase1(Integer id_tipo_ambito) throws Exception {
		Connection conn = null;
		Integer ans = null;
		conn = obtenerConexion();
		ans = obtenerCodmesActual(conn, 1, id_tipo_ambito, true);
		cerrarConexion(conn);
		return ans;
	}

	public Integer obtenerCodmesActual_fase2(Integer id_tipo_ambito) throws Exception {
		Connection conn = null;
		Integer ans = null;
		conn = obtenerConexion();
		ans = obtenerCodmesActual(conn, 2, id_tipo_ambito, true);
		cerrarConexion(conn);
		return ans;
	}

	public Integer obtenerCodmesActualPorFase(Integer fase, Integer id_tipo_ambito) throws Exception {
		Connection conn = null;
		Integer ans = null;
		conn = obtenerConexion();
		ans = obtenerCodmesActual(conn, fase, id_tipo_ambito, true);
		cerrarConexion(conn);
		return ans;
	}

	public Integer obtenerCodmesActual(Connection conn, Integer fase, Integer id_tipo_ambito, Boolean conError)
			throws Exception {
		Integer codmesActual = null;
		ArrayList<Fecha> fechas = null;
		Date fechaActual = null;

		try {
			fechas = obtenerFechasPorAmbito(conn, id_tipo_ambito);

			fechaActual = new Date(System.currentTimeMillis());

			for (Fecha fecha : fechas) {
				// Si la fecha actual se encuentra en el rango
				if ((!fecha.getFechaInicio().after(fechaActual) && !fecha.getFechaFin().before(fechaActual))
						&& fecha.getFase().equals(fase)) {
					codmesActual = fecha.getCodmes();
					break;
				}
			}

			if (conError) {
				// En caso no se tenga codmes actual lanzamos error
				if (codmesActual == null) {
					throw new CustomException("No ha configurado la fase " + fase + " para la fecha actual");
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener el codmes actual por fase : " + ex.getMessage());
			throw ex;
		} finally {
		}

		return codmesActual;
	}

	public JuegoEscenarios_version obtenerJuegoVigentePorCartera(Integer id_cartera) throws Exception {
		return this.obtenerJuegoVigentePorCarteraPorFase(id_cartera, 1);
	}

	public JuegoEscenarios_version obtenerJuegoVigentePorCarteraFase2(Integer id_cartera) throws Exception {
		return this.obtenerJuegoVigentePorCarteraPorFase(id_cartera, 2);
	}

	public JuegoEscenarios_version obtenerJuegoVigentePorCarteraPorFase(Integer id_cartera, Integer fase)
			throws Exception {
		JuegoEscenarios_version juegoEscenariosVersion = null;
		Cartera_version carteraVersion = null;
		EstructuraMetodologica estructura = null;
		TipoObtencionResultados metodo_resultados;
		TipoFlujo tipo_flujo;
		Cartera cartera = null;
		TipoAmbito tipoAmbito = null;
		Entidad entidad = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "select JEV.CodJuegoEscenarios, JEV.NbrJuegoEscenarios, JEV.DesJuegoEscenarios,"
				+ " CV.CodCarteraVersion, CV.NbrVersion, CV.DesVersion, MC.CodMetodologia, MC.NbrMetodologia,"
				+ " MC.DesMetodologia, TF.CodTipoFlujo, TF.NbrTipoFlujo, R.CodTipoObtenResult, R.NbrTipoObtenResult, "
				+ " C.CodCartera, C.NbrCartera, TA.CodTipoAmbito, TA.NbrTipoAmbito, " + " E.CodEntidad, E.NbrEntidad"
				+ " from HE_Ejecucion EJ "
				+ " left join HE_EscenarioVersion EV on EJ.CodEscenarioVersion = EV.CodEscenarioVersion"
				+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios"
				+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion"
				+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ " left join ME_TipoFlujo TF on MC.CodTipoFlujo= TF.CodTipoFlujo "
				+ " left join ME_TipoObtencionResultados R on MC.CodTipoObtenResult = R.CodTipoObtenResult"
				+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
				+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
				+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad" + " where EJ.CodTipoEjecucion in "
				+ (fase.equals(1) ? "(1,7)" : "(2,8)")
				+ " and EJ.TipEstadoEjecucion in (2,4) and EJ.TipEstadoAjustes in (0,3)"
				+ " and EJ.TipEstadoValidacion = 4" // Estado: ejecutado, ajustadoyaprobado o sin ajustes y oficial
				+ " and C.CodCartera = " + id_cartera + " order by EJ.CodMes desc";

		// Para obtener el de mayor codmes
		sql = "select TB2.* from (select ROWNUM as ROWNUMTB, TB1.* from (" + sql + ") TB1) TB2 where TB2.ROWNUMTB = 1";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				juegoEscenariosVersion = new JuegoEscenarios_version();
				carteraVersion = new Cartera_version();
				estructura = new EstructuraMetodologica();
				cartera = new Cartera();
				entidad = new Entidad();

				juegoEscenariosVersion.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenariosVersion.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenariosVersion.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));

				carteraVersion.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				carteraVersion.setNombre_cartera_version(rs.getString("NbrVersion"));
				carteraVersion.setDescripcion(rs.getString("DesVersion"));

				estructura.setId_metodologia(rs.getInt("CodMetodologia"));
				estructura.setNombre_metodologia(rs.getString("NbrMetodologia"));
				estructura.setDescripcion_metodologia(rs.getString("DesMetodologia"));

				tipo_flujo = new TipoFlujo();
				tipo_flujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipo_flujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));
				estructura.setFlujo(tipo_flujo);

				metodo_resultados = new TipoObtencionResultados();
				metodo_resultados.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
				metodo_resultados.setNombre_tipo_obtencion_resultados(rs.getString("NbrTipoObtenResult"));
				estructura.setMetodo_resultados(metodo_resultados);

				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				cartera.setEntidad(entidad);

				estructura.setCartera(cartera);

				// Seteamos carteras a extrapolar tambien
				estructura.setCarteras_extrapolacion(
						this.obtenerCarterasExtrapolacionporIdMetodologia(estructura.getId_metodologia(), conn));

				carteraVersion.setMetodologia(estructura);
				juegoEscenariosVersion.setCartera_version(carteraVersion);

			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener el juego de escenarios vigente la cartera con id " + id_cartera + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarStatement(stmt);
			cerrarResultSet(rs);
			cerrarConexion(conn);
		}
		return juegoEscenariosVersion;
	}

	public JuegoEscenarios_version obtenerJuegoFlagOficialPorCarterayMes(Integer id_cartera, Integer codmes,
			Connection conn) throws Exception {
		JuegoEscenarios_version juegoEscenariosVersion = null;
		Cartera_version carteraVersion = null;
		EstructuraMetodologica estructura = null;
		Cartera cartera = null;
		Entidad entidad = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select JEV.CodJuegoEscenarios, JEV.NbrJuegoEscenarios, JEV.DesJuegoEscenarios,"
				+ " CV.CodCarteraVersion, CV.NbrVersion, CV.DesVersion, MC.CodMetodologia, MC.NbrMetodologia,"
				+ " MC.DesMetodologia, C.CodCartera, C.NbrCartera, E.CodEntidad, E.NbrEntidad"
				+ " from HE_Ejecucion EJ "
				+ " left join HE_EscenarioVersion EV on EJ.CodEscenarioVersion = EV.CodEscenarioVersion"
				+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios"
				+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion"
				+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera"
				+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad" + " where EJ.CodTipoEjecucion in (1,7)"
				+ " and EJ.TipEstadoEjecucion in (2,4) and EJ.TipEstadoAjustes in (0,3)"
				+ " and EJ.TipEstadoValidacion = 4" + " and C.CodCartera = " + id_cartera + " and EJ.codmes = " + codmes
				+ " and ROWNUM = 1" + " order by EJ.CodMes desc";

		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				juegoEscenariosVersion = new JuegoEscenarios_version();
				carteraVersion = new Cartera_version();
				estructura = new EstructuraMetodologica();
				cartera = new Cartera();
				entidad = new Entidad();
				juegoEscenariosVersion.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenariosVersion.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenariosVersion.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));
				carteraVersion.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				carteraVersion.setNombre_cartera_version(rs.getString("NbrVersion"));
				carteraVersion.setDescripcion(rs.getString("DesVersion"));
				estructura.setId_metodologia(rs.getInt("CodMetodologia"));
				estructura.setNombre_metodologia(rs.getString("NbrMetodologia"));
				estructura.setDescripcion_metodologia(rs.getString("DesMetodologia"));
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				cartera.setEntidad(entidad);
				estructura.setCartera(cartera);
				carteraVersion.setMetodologia(estructura);
				juegoEscenariosVersion.setCartera_version(carteraVersion);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener el juego de escenarios vigente la cartera con id " + id_cartera + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarStatement(stmt);
			cerrarResultSet(rs);
		}
		return juegoEscenariosVersion;
	}

	public EjecucionAgregadaPE obtenerEjecucionFase1OficialPorCarteraCodmes(Integer id_cartera, Integer codmes)
			throws Exception {
		EjecucionAgregadaPE ejecucionAgregada = null;
		JuegoEscenarios_version juegoEscenarios_version = null;
		Cartera_version cartera_version = null;
		EstructuraMetodologica metodologia = null;
		MotorCalculo motor = null;
		TipoFlujo tipoFlujo = null;
		TipoObtencionResultados tipoObtenResult = null;
		TipoMetodologia tipoMetodologia = null;
		TipoPeriodicidad tipoPeriodicidad = null;
		Cartera cartera = null;
		TipoAmbito tipoAmbito = null;
		TipoMagnitud tipoMagnitud = null;
		TipoCartera tipoCartera = null;
		Entidad entidad = null;

		// EjecucionPrimerPaso ejecucionModMacro = null;
		// PrimerPaso primerPaso = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;

		// Statement stmt_ejecModMacro = null;
		// ResultSet rs_ejecModMacro = null;

		// String sql_ejecModMacro = null;

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);

			sql = "select E.CodMes, JEV.CodJuegoEscenarios, " + " max(E.FecEjecucion) as FecEjecucion,"
					+ " max(JEV.NbrJuegoEscenarios) as NbrJuegoEscenarios, "
					+ " max(JEV.DesJuegoEscenarios) as DesJuegoEscenarios, "
					+ " max(CV.CodCarteraVersion) as CodCarteraVersion," + " max(CV.NbrVersion) as NbrVersion, "
					+ " max(MC.CodMetodologia) as CodMetodologia," + " max(MC.NbrMetodologia) as NbrMetodologia, "
					+ " max(MC.NbrCodigoEstructura) as NbrCodigoEstructura, max(MC.NumBuckets) as NumBuckets,"
					+ " max(MC.NumLifeTime) as NumLifeTime, max(MC.NumMaxPlazo) as NumMaxPlazo, max(MC.FlgLgdUnica) as FlgLgdUnica,"
					+ " max(MOC.CodMotor) as CodMotor, max(MOC.NbrMotor) as NbrMotor, "
					+ " max(TF.CodTipoFlujo) as CodTipoFlujo, max(TF.NbrTipoFlujo) as NbrTipoFlujo,"
					+ " max(TOR.CodTipoObtenResult) as CodTipoObtenResult, max(TOR.NbrTipoObtenResult) as NbrTipoObtenResult,"
					+ " max(TP.CodTipoPeriodicidad) as CodTipoPeriodicidad, max(TP.NbrTipoPeriodicidad) as NbrTipoPeriodicidad, max(TP.NumValorPeriodicidad) as NumValorPeriodicidad,"
					+ " max(TMPD.CodTipoMetodologia) as CodTipoMetodologiaPD, max(TMPD.NbrTipoMetodologia) as NbrTipoMetodologiaPD, max(TMPD.NbrVariableMotor) as NbrVariableMotorPD,"
					+ " max(TMLGD.CodTipoMetodologia) as CodTipoMetodologiaLGD, max(TMLGD.NbrTipoMetodologia) as NbrTipoMetodologiaLGD, max(TMLGD.NbrVariableMotor) as NbrVariableMotorLGD,"
					+ " max(TMLGDWO.CodTipoMetodologia) as CodTipoMetodologiaLGDWO, max(TMLGDWO.NbrTipoMetodologia) as NbrTipoMetodologiaLGDWO, max(TMLGDWO.NbrVariableMotor) as NbrVariableMotorLGDWO,"
					+ " max(TMLGDBE.CodTipoMetodologia) as CodTipoMetodologiaLGDBE, max(TMLGDBE.NbrTipoMetodologia) as NbrTipoMetodologiaLGDBE, max(TMLGDBE.NbrVariableMotor) as NbrVariableMotorLGDBE,"
					+ " max(TMEAD.CodTipoMetodologia) as CodTipoMetodologiaEAD, max(TMEAD.NbrTipoMetodologia) as NbrTipoMetodologiaEAD, max(TMEAD.NbrVariableMotor) as NbrVariableMotorEAD,"
					+ " max(C.CodCartera) as CodCartera," + " max(C.NbrCartera) as NbrCartera,"
					+ " max(C.NbrVariableMotor) as NbrVariableMotorCartera, max(C.NumOrden) as NumOrden, "
					+ " max(C.FlgPd6Meses) as FlgPd6Meses, max(TA.CodTipoAmbito) as CodTipoAmbito, max(TA.NbrTipoAmbito) as NbrTipoAmbito, "
					+ " max(TM.CodTipoMagnitud) as CodTipoMagnitud, max(TM.NbrTipoMagnitud) as NbrTipoMagnitud,"
					+ " max(TC.CodTipoCartera) as CodTipoCartera, max(TC.NbrTipoCartera) as NbrTipoCartera, max(TC.NbrVariableMotor) as NbrVariableMotorTipoCartera,"
					+ " max(E.CodEntidad) as CodEntidad, max(E.NbrEntidad) as NbrEntidad,  max(E.NbrVariableMotor) as NbrVariableMotorEntidad "
					+ " from HE_Ejecucion E "
					+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion"
					+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
					+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
					+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
					+ " left join ME_MotorCalculo MOC on MC.CodMotor = MOC.CodMotor"
					+ " left join ME_TipoFlujo TF on MC.CodTipoFlujo = TF.CodTipoFlujo "
					+ " left join ME_TipoObtencionResultados TOR on MC.CodTipoObtenResult = TOR.CodTipoObtenResult "
					+ " left join ME_TipoPeriodicidad TP on MC.CodTipoPeriodicidad = TP.CodTipoPeriodicidad "
					+ " left join ME_TipoMetodologia TMPD on MC.CodTipoMetodologiaPD = TMPD.CodTipoMetodologia "
					+ " left join ME_TipoMetodologia TMEAD on MC.CodTipoMetodologiaEAD = TMEAD.CodTipoMetodologia "
					+ " left join ME_TipoMetodologia TMLGD on MC.CodTipoMetodologiaLGD = TMLGD.CodTipoMetodologia "
					+ " left join ME_TipoMetodologia TMLGDWO on MC.CodTipoMetodologiaLGDWO = TMLGDWO.CodTipoMetodologia "
					+ " left join ME_TipoMetodologia TMLGDBE on MC.CodTipoMetodologiaLGDBE = TMLGDBE.CodTipoMetodologia "
					+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
					+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
					+ " left join ME_TipoMagnitud TM on C.CodTipoMagnitud = TM.CodTipoMagnitud "
					+ " left join ME_TipoCartera TC on C.CodTipoCartera = TC.CodTipoCartera "
					+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad " + " where E.CodTipoEjecucion in (1,7)"
					// Siempre fase 1 (incluido inversiones)
					+ " and E.TipEstadoValidacion = 4 " // Oficial
					+ " and E.CodMes = " + codmes + " and C.CodCartera = " + id_cartera
					+ " group by E.codmes, JEV.CodJuegoEscenarios" + " order by E.codmes desc";

			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				ejecucionAgregada = new EjecucionAgregadaPE();
				juegoEscenarios_version = new JuegoEscenarios_version();
				cartera_version = new Cartera_version();
				metodologia = new EstructuraMetodologica();
				cartera = new Cartera();
				entidad = new Entidad();

				ejecucionAgregada.setCodmes(rs.getInt("CodMes"));
				ejecucionAgregada.setDatetime_ejecucion(rs.getLong("FecEjecucion"));

				juegoEscenarios_version.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenarios_version.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenarios_version.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));

				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));

				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
				metodologia.setCodigo_estructura(rs.getString("NbrCodigoEstructura"));

				motor = new MotorCalculo();
				motor.setId_motor(rs.getInt("CodMotor"));
				motor.setNombre_motor(rs.getString("NbrMotor"));
				metodologia.setMotor(motor);

				tipoFlujo = new TipoFlujo();
				tipoFlujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipoFlujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));
				metodologia.setFlujo(tipoFlujo);

				if (tipoFlujo.getNombre_tipo_flujo().toLowerCase().equals(("Calculo PE").toLowerCase())) {
					tipoObtenResult = new TipoObtencionResultados();
					tipoObtenResult.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
					tipoObtenResult.setNombre_tipo_obtencion_resultados(rs.getString("NbrTipoObtenResult"));
					metodologia.setMetodo_resultados(tipoObtenResult);

					if (tipoObtenResult.getNombre_tipo_obtencion_resultados().toLowerCase()
							.equals(("Modelo interno").toLowerCase())) {
						metodologia.setNro_buckets(rs.getInt("NumBuckets"));
						metodologia.setLife_time(rs.getInt("NumLifeTime"));
						metodologia.setNro_maxPlazo(rs.getInt("NumMaxPlazo"));

						tipoPeriodicidad = new TipoPeriodicidad();
						tipoPeriodicidad.setId_tipo_periodicidad(rs.getInt("CodTipoPeriodicidad"));
						tipoPeriodicidad.setNombre_tipo_periodicidad(rs.getString("NbrTipoPeriodicidad"));
						tipoPeriodicidad.setValor_periodicidad(rs.getInt("NumValorPeriodicidad"));
						metodologia.setPeriodicidad(tipoPeriodicidad);

						tipoMetodologia = new TipoMetodologia();
						tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaPD"));
						tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaPD"));
						metodologia.setMetodologia_PD(tipoMetodologia);

						tipoMetodologia = new TipoMetodologia();
						tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaEAD"));
						tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaEAD"));
						metodologia.setMetodologia_EAD(tipoMetodologia);

						metodologia.setFlag_lgdUnica(rs.getBoolean("FlgLgdUnica"));

						if (metodologia.getFlag_lgdUnica()) {
							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGD"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGD"));
							metodologia.setMetodologia_LGD(tipoMetodologia);
						} else {
							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGDWO"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGDWO"));
							metodologia.setMetodologia_LGD_WO(tipoMetodologia);

							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGDBE"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGDBE"));
							metodologia.setMetodologia_LGD_BE(tipoMetodologia);
						}
					} else if (tipoObtenResult.getNombre_tipo_obtencion_resultados().toLowerCase()
							.equals(("Extrapolacion").toLowerCase())) {
						metodologia.setCarteras_extrapolacion(this
								.obtenerCarterasExtrapolacionporIdMetodologia(metodologia.getId_metodologia(), conn));
					}
				}

				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setNombre_variablemotor(rs.getString("NbrVariableMotorCartera"));
				cartera.setOrden(rs.getInt("NumOrden"));
				cartera.setFlag_pd6meses(rs.getBoolean("FlgPd6Meses"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				tipoMagnitud = new TipoMagnitud();
				tipoMagnitud.setId_tipo_magnitud(rs.getInt("CodTipoMagnitud"));
				tipoMagnitud.setNombre_tipo_magnitud(rs.getString("NbrTipoMagnitud"));
				cartera.setTipo_magnitud(tipoMagnitud);

				tipoCartera = new TipoCartera();
				tipoCartera.setId_tipo_cartera(rs.getInt("CodTipoCartera"));
				tipoCartera.setNombre_tipo_cartera(rs.getString("NbrTipoCartera"));
				tipoCartera.setNombre_variablemotor(rs.getString("NbrVariableMotorTipoCartera"));
				cartera.setTipo_cartera(tipoCartera);

				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				entidad.setNombre_variablemotor(rs.getString("NbrVariableMotorEntidad"));
				cartera.setEntidad(entidad);

				metodologia.setCartera(cartera);

				cartera_version.setMetodologia(metodologia);

				juegoEscenarios_version.setCartera_version(cartera_version);

				ejecucionAgregada.setJuego_escenarios_version(juegoEscenarios_version);

				// Ingresamos la ejecucion de modelo macro configurada
				/*
				 * sql_ejecModMacro =
				 * "select E_MM.CodEjecucion as CodEjecucionModMacro, E_MM.CodMes as CodMesModMacro, "
				 * + "PP.CodPrimerPaso, PP.NbrPrimerPaso, PP.DesPrimerPaso " +
				 * "from HE_MapeoModMacro MMM " +
				 * "left join HE_Ejecucion E_MM on MMM.CodEjecucionModMacro = E_MM.CodEjecucion "
				 * + "left join HE_PrimerPaso PP on E_MM.CodPrimerPaso = PP.CodPrimerPaso " +
				 * "left join HE_Ejecucion E_PE on MMM.CodEjecucionPE = E_PE.CodEjecucion " +
				 * "left join HE_EscenarioVersion EV on E_PE.CodEscenarioVersion = EV.CodEscenarioVersion "
				 * + "where ROWNUM=1 and E_PE.CodTipoEjecucion = 1 " + " and E_PE.CodMes = " +
				 * ejecucionAgregada.getCodmes() + " and EV.CodJuegoEscenarios = " +
				 * ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios();
				 * 
				 * stmt_ejecModMacro = crearStatement(conn); rs_ejecModMacro =
				 * stmt_ejecModMacro.executeQuery(sql_ejecModMacro);
				 * 
				 * ejecucionModMacro = new EjecucionPrimerPaso(); primerPaso = new PrimerPaso();
				 * while (rs_ejecModMacro.next()) {
				 * ejecucionModMacro.setId_ejecucion(rs_ejecModMacro.getInt(
				 * "CodEjecucionModMacro"));
				 * ejecucionModMacro.setCodmes(rs_ejecModMacro.getInt("CodMesModMacro"));
				 * 
				 * primerPaso.setId_primerPaso(rs_ejecModMacro.getInt("CodPrimerPaso"));
				 * primerPaso.setNombre_primerPaso(rs_ejecModMacro.getString("NbrPrimerPaso"));
				 * primerPaso.setDescripcion_primerPaso(rs_ejecModMacro.getString(
				 * "DesPrimerPaso")); ejecucionModMacro.setPrimerPaso(primerPaso);
				 * 
				 * ejecucionModMacro.setMetodologia(
				 * ejecucionAgregada.getJuego_escenarios_version().getCartera_version().
				 * getMetodologia()); }
				 * ejecucionAgregada.setEjecucion_modMacro(ejecucionModMacro);
				 */
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener la ejecucion de fase 1 oficial : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			// cerrarResultSet(rs_ejecModMacro);
			// cerrarStatement(stmt_ejecModMacro);
			cerrarConexion(conn);
		}
		return ejecucionAgregada;
	}

	public ArrayList<EjecucionPE> obtenerEjecucionesBaseFase1OficialPorCartera(Integer id_cartera) throws Exception {
		ArrayList<EjecucionPE> ejecuciones = null;
		EjecucionPE ejecucion = null;
		Escenario_version escenario_version = null;
		Escenario escenario = null;
		JuegoEscenarios_version juegoEscenarios_version = null;
		Cartera_version cartera_version = null;
		EstructuraMetodologica metodologia = null;
		Cartera cartera = null;
		TipoAmbito tipoAmbito = null;
		Entidad entidad = null;

		Boolean existeCodmesActual = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);

			sql = "select E.CodEjecucion, E.CodMes, JEV.CodJuegoEscenarios, E.FecEjecucion, EV.CodEscenarioVersion, EV.NumPeso, "
					+ " ESC.CodEscenario, ESC.NbrEscenario, ESC.FlgObligatorio, JEV.NbrJuegoEscenarios, JEV.DesJuegoEscenarios, "
					+ " CV.CodCarteraVersion, CV.NbrVersion, MC.CodMetodologia, MC.NbrMetodologia, C.CodCartera, C.NbrCartera,"
					+ " C.NumOrden, TA.CodTipoAmbito, TA.NbrTipoAmbito, E.CodEntidad, E.NbrEntidad "
					+ " from HE_Ejecucion E "
					+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
					+ " left join ME_Escenario ESC on EV.CodEscenario = ESC.CodEscenario "
					+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
					+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
					+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
					+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
					+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
					+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad " + " where E.CodTipoEjecucion in (1,7)" // Siempre
																														// fase
																														// 1
																														// includo
																														// inv
					+ " and E.TipEstadoValidacion = 4 and ESC.FlgObligatorio = 1" // Oficial y base
					+ " and E.CodMes <= " + this.obtenerCodmesActual(conn, 1, 1, true) // Mismo codmes actual que la
																						// fase2
					// (el metodo solo sirve en creditos ...)
					+ " and C.CodCartera = " + id_cartera + " order by E.codmes desc";

			rs = stmt.executeQuery(sql);

			ejecuciones = new ArrayList<EjecucionPE>();
			while (rs.next()) {
				ejecucion = new EjecucionPE();
				escenario_version = new Escenario_version();
				escenario = new Escenario();
				juegoEscenarios_version = new JuegoEscenarios_version();
				cartera_version = new Cartera_version();
				metodologia = new EstructuraMetodologica();
				cartera = new Cartera();
				entidad = new Entidad();

				ejecucion.setId_ejecucion(rs.getInt("CodEjecucion"));
				ejecucion.setFase(1); // Siempre 1
				ejecucion.setCodmes(rs.getInt("CodMes"));
				ejecucion.setDatetime_ejecucion(rs.getLong("FecEjecucion"));

				escenario.setId_escenario(rs.getInt("CodEscenario"));
				escenario.setNombre_escenario(rs.getString("NbrEscenario"));
				escenario.setFlag_obligatorio(rs.getBoolean("FlgObligatorio"));

				escenario_version.setId_escenario_version(rs.getInt("CodEscenarioVersion"));
				escenario_version.setPeso(rs.getDouble("NumPeso"));

				juegoEscenarios_version.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenarios_version.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenarios_version.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));

				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));

				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));

				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setOrden(rs.getInt("NumOrden"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				cartera.setEntidad(entidad);

				metodologia.setCartera(cartera);

				cartera_version.setMetodologia(metodologia);

				juegoEscenarios_version.setCartera_version(cartera_version);

				escenario_version.setJuego_escenarios_version(juegoEscenarios_version);

				escenario_version.setEscenario(escenario);

				ejecucion.setEscenario_version(escenario_version);

				ejecuciones.add(ejecucion);
			}

			// Aseguramos que incluya el codmes actual
			existeCodmesActual = false;

			for (EjecucionPE ejec : ejecuciones) {
				if (ejec.getCodmes().equals(this.obtenerCodmesActual(conn, 1, 1, false))) { // metodo solo para
																							// creditos...
					existeCodmesActual = true;
					break;
				}
			}

			if (!existeCodmesActual) {
				throw new CustomException("No existe una ejecucion oficial actual de la cartera seleccionada");
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener la ejecuciones base de fase 1 oficiales por cartera : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return ejecuciones;
	}

	public ArrayList<EjecucionPE> obtenerEjecucionesBaseOficialPorCarteraCodmesFase(Integer id_cartera, Integer codmes,
			Integer fase) throws Exception {
		ArrayList<EjecucionPE> ejecuciones = null;
		EjecucionPE ejecucion = null;
		Escenario_version escenario_version = null;
		Escenario escenario = null;
		JuegoEscenarios_version juegoEscenarios_version = null;
		Cartera_version cartera_version = null;
		EstructuraMetodologica metodologia = null;
		Cartera cartera = null;
		TipoAmbito tipoAmbito = null;
		Entidad entidad = null;

		Boolean existeCodmesSel = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);

			sql = "select E.CodEjecucion, E.CodMes, E.FecEjecucion, E.CodTipoEjecucion, JEV.CodJuegoEscenarios, "
					+ " EV.CodEscenarioVersion, EV.NumPeso, "
					+ " ESC.CodEscenario, ESC.NbrEscenario, ESC.FlgObligatorio, JEV.NbrJuegoEscenarios, JEV.DesJuegoEscenarios, "
					+ " CV.CodCarteraVersion, CV.NbrVersion, MC.CodMetodologia, MC.NbrMetodologia, C.CodCartera, C.NbrCartera,"
					+ " C.NumOrden, TA.CodTipoAmbito, TA.NbrTipoAmbito, EN.CodEntidad, EN.NbrEntidad "
					+ " from HE_Ejecucion E "
					+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
					+ " left join ME_Escenario ESC on EV.CodEscenario = ESC.CodEscenario "
					+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
					+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
					+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
					+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
					+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
					+ " left join HE_Entidad EN on C.CodEntidad = EN.CodEntidad " + " where E.CodTipoEjecucion = "
					+ fase + " and E.TipEstadoValidacion = 4 and ESC.FlgObligatorio = 1" // Oficial y base
					+ " and E.CodMes <= " + codmes + " and C.CodCartera = " + id_cartera + " order by E.codmes desc";

			rs = stmt.executeQuery(sql);

			ejecuciones = new ArrayList<EjecucionPE>();
			while (rs.next()) {
				ejecucion = new EjecucionPE();
				escenario_version = new Escenario_version();
				escenario = new Escenario();
				juegoEscenarios_version = new JuegoEscenarios_version();
				cartera_version = new Cartera_version();
				metodologia = new EstructuraMetodologica();
				cartera = new Cartera();
				entidad = new Entidad();

				ejecucion.setId_ejecucion(rs.getInt("CodEjecucion"));
				ejecucion.setFase(rs.getInt("CodTipoEjecucion"));
				ejecucion.setCodmes(rs.getInt("CodMes"));
				ejecucion.setDatetime_ejecucion(rs.getLong("FecEjecucion"));

				escenario.setId_escenario(rs.getInt("CodEscenario"));
				escenario.setNombre_escenario(rs.getString("NbrEscenario"));
				escenario.setFlag_obligatorio(rs.getBoolean("FlgObligatorio"));

				escenario_version.setId_escenario_version(rs.getInt("CodEscenarioVersion"));
				escenario_version.setPeso(rs.getDouble("NumPeso"));

				juegoEscenarios_version.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenarios_version.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenarios_version.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));

				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));

				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));

				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setOrden(rs.getInt("NumOrden"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				cartera.setEntidad(entidad);

				metodologia.setCartera(cartera);

				cartera_version.setMetodologia(metodologia);

				juegoEscenarios_version.setCartera_version(cartera_version);

				escenario_version.setJuego_escenarios_version(juegoEscenarios_version);

				escenario_version.setEscenario(escenario);

				ejecucion.setEscenario_version(escenario_version);

				ejecuciones.add(ejecucion);
			}

			// Aseguramos que incluya el codmes seleccionado
			existeCodmesSel = false;

			for (EjecucionPE ejec : ejecuciones) {
				if (ejec.getCodmes().equals(codmes)) {
					existeCodmesSel = true;
					break;
				}
			}

			if (!existeCodmesSel) {
				throw new CustomException("No existe una ejecucion oficial para la cartera, fecha y fase seleccionada");
			} else if (ejecuciones.size() == 1) {
				throw new CustomException(
						"No existen ejecuciones oficiales anteriores (comparacion) para la cartera, fecha y fase seleccionada");
			} else {
				// Llenamos tablas mapeo de bloques de variacion para la ejecucion actual
				for (EjecucionPE ejec : ejecuciones) {
					if (ejec.getCodmes().equals(codmes)) {
						ejec.setTablas_mapeo_ejecucion(
								this.obtenerTablasMapeoVariacPorIdCarVerEjecAct(ejec.getEscenario_version()
										.getJuego_escenarios_version().getCartera_version().getId_cartera_version()));
						break;
					}
				}
			}

		} catch (Exception ex) {
			LOGGER.error(
					"Error al obtener la ejecuciones base oficiales por cartera, codmes y fase : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return ejecuciones;
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoVariacPorIdCarVerEjecAct(Integer id_carVer_ejecAct)
			throws Exception {
		ArrayList<TablaMapeoEjecucion> tablasMapeo = new ArrayList<TablaMapeoEjecucion>();
		Connection conn = null;

		String sql = "select MTB.CodMapeoTabla," + " TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata, "
				+ " A.CodAprovisionamiento, A.NbrFichero," + " B.CodBloque, B.NbrBloque, B.NbrVariableMotor "
				+ " from HE_MapeoTablaBloque MTB "
				+ " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput"
				+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
				+ " left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
				+ " left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque "
				+ " where MTB.CodEscenarioVersion is null and B.FlgTablaInput = 1 " + " and TB.CodTipoBloque = 23" // De
																													// tipo
																													// variacion
				+ " and MTB.CodCarteraVersion = " + id_carVer_ejecAct;

		conn = obtenerConexion();
		tablasMapeo = obtenerTablasMapeo(sql, 1, true, conn, false, true);
		cerrarConexion(conn);

		return tablasMapeo;
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoVariacPorIdEjecVar(Integer id_ejecucion_variac)
			throws Exception {
		ArrayList<TablaMapeoEjecucion> tablasMapeo = new ArrayList<TablaMapeoEjecucion>();
		Connection conn = null;

		String sql = "select ME.CodMapeoTabla," + " TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata,"
				+ " A.CodAprovisionamiento, A.NbrFichero,"
				+ " CF.CodCargaFichero, CF.NbrCarga, CF.NumOrdenInicial, CF.NumOrdenFinal, CF.DesComentarioCalidad, CF.NumVersionCarga,"
				+ " F.CodFecha, F.NumCodMes," + " B.CodBloque, B.NbrBloque, B.NbrVariableMotor "
				+ " from HE_MapeoEjecucion ME"
				+ " left join HE_MapeoTablaBloque MTB on ME.CodMapeoTabla = MTB.CodMapeoTabla"
				+ " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput"
				+ " left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
				+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
				+ " left join HE_CargaFichero CF on CF.CodCargaFichero = ME.CodCargaFichero"
				+ " left join HE_Fecha F on CF.CodFecha = F.CodFecha" + " where ME.CodEjecucion = "
				+ id_ejecucion_variac;

		conn = obtenerConexion();
		tablasMapeo = obtenerTablasMapeo(sql, 1, true, conn, true, true);
		cerrarConexion(conn);

		return tablasMapeo;
	}

	public ArrayList<Escenario_version> obtenerEscenariosVersionPorIdJuegoEscenarios(Integer id_juego_escenarios)
			throws Exception {
		ArrayList<Escenario_version> escenariosVersion = null;
		Escenario_version escenarioVersion = null;
		Escenario escenario = null;
		JuegoEscenarios_version juegoEscenariosVersion = null;
		Cartera_version carteraVersion = null;
		EstructuraMetodologica estructura = null;
		Cartera cartera = null;
		Entidad entidad = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "select EV.CodEscenarioVersion, EV.NumPeso, E.CodEscenario, E.NbrEscenario, JEV.CodJuegoEscenarios,"
				+ " JEV.NbrJuegoEscenarios, JEV.DesJuegoEscenarios, CV.CodCarteraVersion, CV.NbrVersion, CV.DesVersion,"
				+ " MC.CodMetodologia, MC.NbrMetodologia, MC.DesMetodologia,"
				+ " C.CodCartera, C.NbrCartera, EN.CodEntidad, EN.NbrEntidad" + " from HE_EscenarioVersion EV "
				+ " left join ME_Escenario E on EV.CodEscenario= E.CodEscenario"
				+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios"
				+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion"
				+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia"
				+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera"
				+ " left join HE_Entidad EN on C.CodEntidad = EN.CodEntidad " + " where JEV.CodJuegoEscenarios= "
				+ id_juego_escenarios;

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			escenariosVersion = new ArrayList<Escenario_version>();

			while (rs.next()) {
				escenarioVersion = new Escenario_version();
				escenario = new Escenario();
				juegoEscenariosVersion = new JuegoEscenarios_version();
				carteraVersion = new Cartera_version();
				estructura = new EstructuraMetodologica();
				cartera = new Cartera();
				entidad = new Entidad();

				escenarioVersion.setId_escenario_version(rs.getInt("CodEscenarioVersion"));
				escenarioVersion.setPeso(rs.getDouble("NumPeso"));
				escenario.setId_escenario(rs.getInt("CodEscenario"));
				escenario.setNombre_escenario(rs.getString("NbrEscenario"));
				juegoEscenariosVersion.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenariosVersion.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenariosVersion.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));
				carteraVersion.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				carteraVersion.setNombre_cartera_version(rs.getString("NbrVersion"));
				carteraVersion.setDescripcion(rs.getString("DesVersion"));
				estructura.setId_metodologia(rs.getInt("CodMetodologia"));
				estructura.setNombre_metodologia(rs.getString("NbrMetodologia"));
				estructura.setDescripcion_metodologia(rs.getString("DesMetodologia"));
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));

				escenarioVersion.setEscenario(escenario);
				cartera.setEntidad(entidad);
				estructura.setCartera(cartera);
				carteraVersion.setMetodologia(estructura);
				juegoEscenariosVersion.setCartera_version(carteraVersion);
				escenarioVersion.setJuego_escenarios_version(juegoEscenariosVersion);
				escenariosVersion.add(escenarioVersion);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener escenarios version para el juego de escenarios version con id "
					+ id_juego_escenarios + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return escenariosVersion;
	}

	/*
	 * public void crearEjecucionPE_fase1(EjecucionPE ejecucionPE) throws Exception
	 * { this.crearEjecucionPE(ejecucionPE, 1); }
	 * 
	 * public void crearEjecucionPE_fase2(EjecucionPE ejecucionPE) throws Exception
	 * { this.crearEjecucionPE(ejecucionPE, 2); }
	 * 
	 * public void crearEjecucionPE(EjecucionPE ejecucionPE, Integer fase) throws
	 * Exception { Connection conn = null; PreparedStatement pstmt_ejecucion = null;
	 * PreparedStatement pstmt_mapeo_ejecucion = null; Integer id_ejecucion = null;
	 * 
	 * String sql_ejecucion =
	 * "Insert into HE_Ejecucion(CodEjecucion, CodMes, CodEscenarioVersion, CodTipoEjecucion,"
	 * +
	 * " TipEstadoEjecucion, TipEstadoValidacion, TipEstadoAjustes, DesComentarioEjecucion, FecEjecucion,"
	 * +
	 * " CodMesInicial, CodMesFinal, CodJuegoEscenariosInicial, CodJuegoEscenariosFinal, FecActualizacionTabla) "
	 * + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	 * 
	 * String sql_mapeo_ejecucion =
	 * "Insert into HE_MapeoEjecucion(CodEjecucion, CodMapeoTabla, CodCargaFichero,"
	 * + " FecActualizacionTabla) values (?,?,?,?)"; try { conn = obtenerConexion();
	 * id_ejecucion = obtenerSiguienteId("SEQ_HE_Ejecucion");
	 * 
	 * pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
	 * pstmt_ejecucion.setInt(1, id_ejecucion); pstmt_ejecucion.setInt(2,
	 * ejecucionPE.getCodmes()); pstmt_ejecucion.setLong(3,
	 * ejecucionPE.getEscenario_version().getId_escenario_version());
	 * pstmt_ejecucion.setInt(4, fase); // Tipo de ejecucion: Calculo PE - fase 1 o
	 * 2 pstmt_ejecucion.setInt(5, ejecucionPE.getEstado_ejecucion());
	 * pstmt_ejecucion.setInt(6, 0); // Estado validacion: Validacion pendiente
	 * pstmt_ejecucion.setInt(7, 0); // Estado ajuste: Sin ajuste
	 * pstmt_ejecucion.setString(8, ""); // Sin comentarios
	 * pstmt_ejecucion.setLong(9, ejecucionPE.getDatetime_ejecucion());
	 * pstmt_ejecucion.setNull(10, java.sql.Types.INTEGER);
	 * pstmt_ejecucion.setNull(11, java.sql.Types.INTEGER);
	 * pstmt_ejecucion.setNull(12, java.sql.Types.INTEGER);
	 * pstmt_ejecucion.setNull(13, java.sql.Types.INTEGER);
	 * pstmt_ejecucion.setDate(14, new Date(System.currentTimeMillis()));
	 * 
	 * pstmt_ejecucion.executeUpdate();
	 * 
	 * for (TablaMapeoEjecucion tabla_mapeo_ejecucion :
	 * ejecucionPE.getTablas_mapeo_ejecucion()) { pstmt_mapeo_ejecucion =
	 * conn.prepareStatement(sql_mapeo_ejecucion); pstmt_mapeo_ejecucion.setInt(1,
	 * id_ejecucion); pstmt_mapeo_ejecucion.setInt(2,
	 * tabla_mapeo_ejecucion.getId_mapeo_tabla()); pstmt_mapeo_ejecucion.setInt(3,
	 * tabla_mapeo_ejecucion.getCarga().getId_cargafichero());
	 * pstmt_mapeo_ejecucion.setDate(4, new Date(System.currentTimeMillis()));
	 * 
	 * pstmt_mapeo_ejecucion.executeUpdate(); }
	 * 
	 * ejecutarCommit(conn);
	 * 
	 * } catch (Exception ex) { ejecutarRollback(conn);
	 * LOGGER.error("Error al agregar una ejecucion PE de fase " + fase + " : " +
	 * ex.getMessage()); throw ex; } finally {
	 * cerrarPreparedStatement(pstmt_ejecucion);
	 * cerrarPreparedStatement(pstmt_mapeo_ejecucion); cerrarConexion(conn); } }
	 * 
	 * public void replicarEjecucionesPE_fase1(ReplicaEjecucionesPE
	 * replicaEjecucionesPE) throws Exception {
	 * this.replicarEjecucionesPE(replicaEjecucionesPE, 1); }
	 * 
	 * public void replicarEjecucionesPE_fase2(ReplicaEjecucionesPE
	 * replicaEjecucionesPE) throws Exception {
	 * this.replicarEjecucionesPE(replicaEjecucionesPE, 2); }
	 * 
	 * public void replicarEjecucionesPE(ReplicaEjecucionesPE replicaEjecucionesPE,
	 * Integer fase) throws Exception { for (EjecucionPE ejecucionPE :
	 * replicaEjecucionesPE.getEjecuciones_replicar()) {
	 * ejecucionPE.setCodmes(replicaEjecucionesPE.getCodmes_replica());
	 * ejecucionPE.setTablas_mapeo_ejecucion(
	 * this.obtenerTablasMapeoEjecucionPorIdEjecucion(ejecucionPE.getId_ejecucion())
	 * ); this.crearEjecucionPE(ejecucionPE, fase); } }
	 * 
	 * public void editarEjecucionPE_fase1(EjecucionPE ejecucionPE) throws Exception
	 * { this.editarEjecucionPE(ejecucionPE, 1); }
	 * 
	 * public void editarEjecucionPE_fase2(EjecucionPE ejecucionPE) throws Exception
	 * { this.editarEjecucionPE(ejecucionPE, 2); }
	 * 
	 * public void editarEjecucionPE(EjecucionPE ejecucionPE, Integer fase) throws
	 * Exception { Connection conn = null; PreparedStatement pstmt_ejecucion = null;
	 * PreparedStatement pstmt_mapeo_ejecucion = null; String sql_ejecucion =
	 * "Update HE_Ejecucion set CodMes= ?, CodEscenarioVersion= ?, CodTipoEjecucion= ?, "
	 * +
	 * "TipEstadoEjecucion= ?, FecEjecucion= ?, FecActualizacionTabla= ? where CodEjecucion = ?"
	 * ;
	 * 
	 * String sql_mapeo_ejecucion =
	 * "Update HE_MapeoEjecucion set CodCargaFichero= ?, FecActualizacionTabla= ?" +
	 * " where CodEjecucion= ? and CodMapeoTabla= ?";
	 * 
	 * try { conn = obtenerConexion(); pstmt_ejecucion =
	 * conn.prepareStatement(sql_ejecucion);
	 * 
	 * pstmt_ejecucion.setInt(1, ejecucionPE.getCodmes()); pstmt_ejecucion.setInt(2,
	 * ejecucionPE.getEscenario_version().getId_escenario_version());
	 * pstmt_ejecucion.setInt(3, fase); // Tipo de ejecucion: Calculo PE - fase 1 o
	 * 2 pstmt_ejecucion.setInt(4, ejecucionPE.getEstado_ejecucion());
	 * pstmt_ejecucion.setLong(5, ejecucionPE.getDatetime_ejecucion());
	 * pstmt_ejecucion.setDate(6, new Date(System.currentTimeMillis()));
	 * pstmt_ejecucion.setInt(7, ejecucionPE.getId_ejecucion());
	 * 
	 * pstmt_ejecucion.executeUpdate();
	 * 
	 * if (ejecucionPE.getTablas_mapeo_ejecucion() != null) { for
	 * (TablaMapeoEjecucion tabla_mapeo_ejecucion :
	 * ejecucionPE.getTablas_mapeo_ejecucion()) { pstmt_mapeo_ejecucion =
	 * conn.prepareStatement(sql_mapeo_ejecucion); pstmt_mapeo_ejecucion.setInt(1,
	 * tabla_mapeo_ejecucion.getCarga().getId_cargafichero());
	 * pstmt_mapeo_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
	 * pstmt_mapeo_ejecucion.setInt(3, ejecucionPE.getId_ejecucion());
	 * pstmt_mapeo_ejecucion.setInt(4, tabla_mapeo_ejecucion.getId_mapeo_tabla());
	 * 
	 * pstmt_mapeo_ejecucion.executeUpdate(); } }
	 * 
	 * ejecutarCommit(conn);
	 * 
	 * } catch (Exception ex) { ejecutarRollback(conn);
	 * LOGGER.error("Error al actualizar ejecucion PE de fase " + fase + " : " +
	 * ex.getMessage()); throw ex;
	 * 
	 * } finally { cerrarPreparedStatement(pstmt_ejecucion);
	 * cerrarPreparedStatement(pstmt_mapeo_ejecucion); cerrarConexion(conn); } }
	 * 
	 * public void borrarEjecucionPE_fase1(Integer id_ejecucion) throws Exception {
	 * this.borrarEjecucionPE(id_ejecucion, 1); }
	 * 
	 * public void borrarEjecucionPE_fase2(Integer id_ejecucion) throws Exception {
	 * this.borrarEjecucionPE(id_ejecucion, 2); }
	 * 
	 * public void borrarEjecucionPE(Integer id_ejecucion, Integer fase) throws
	 * Exception { Connection conn = null; PreparedStatement pstmt_ejecucion = null;
	 * PreparedStatement pstmt_mapeo_ejecucion = null; String sql_ejecucion =
	 * "DELETE from HE_Ejecucion where CodEjecucion=? and CodTipoEjecucion= " +
	 * fase;
	 * 
	 * try { conn = obtenerConexion(); pstmt_ejecucion =
	 * conn.prepareStatement(sql_ejecucion); pstmt_ejecucion.setInt(1,
	 * id_ejecucion); pstmt_ejecucion.executeUpdate();
	 * 
	 * ejecutarCommit(conn);
	 * 
	 * } catch (Exception ex) { ejecutarRollback(conn);
	 * LOGGER.error("Error al borrar una ejecucion PE - fase " + fase + " con id: "
	 * + id_ejecucion + " : " + ex.getMessage()); throw ex; } finally {
	 * cerrarPreparedStatement(pstmt_ejecucion);
	 * cerrarPreparedStatement(pstmt_mapeo_ejecucion); cerrarConexion(conn); } }
	 */

	// -------------------------------------------------------------------
	// ----------------------EJECUCIONES VARIACIONES----------------------
	// -------------------------------------------------------------------

	public ArrayList<EjecucionVariacion> obtenerEjecucionesVariacion() throws Exception {
		ArrayList<EjecucionVariacion> ejecucionesVariacion = new ArrayList<EjecucionVariacion>();
		EjecucionVariacion ejecucionVariacion = null;
		EjecucionPE ejecucionPE = null;
		Escenario_version escenarioVersion = null;
		Escenario escenario = null;
		JuegoEscenarios_version juegoEscenarios = null;
		Cartera_version carteraVersion = null;
		EstructuraMetodologica metodologia = null;
		Cartera cartera = null;
		Entidad entidad = null;
		TipoObtencionResultados tor = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "select EJ.CodEjecucion, EJ.TipEstadoEjecucion, EJ.NumEjecucionAvance, EJ.FecEjecucion, EJ.FlgRegulatorio, "
				+ " EJ_F1.CodEjecucion as CodEjecucionF1, EJ_F1.CodMes as CodMesF1, EJ_F1.CodTipoEjecucion as FaseF1,"
				+ " EV_F1.CodEscenarioVersion as CodEscenarioVersionF1,"
				+ " E_F1.CodEscenario as CodEscenarioF1, E_F1.NbrEscenario as NbrEscenarioF1, E_F1.FlgObligatorio as FlgObligatorioF1,"
				+ " JEV_F1.CodJuegoEscenarios as CodJuegoEscenariosF1, JEV_F1.NbrJuegoEscenarios as NbrJuegoEscenariosF1, "
				+ " CV_F1.CodCarteraVersion, CV_F1.NbrVersion, MC_F1.CodMetodologia, "
				+ " MC_F1.NbrMetodologia, MC_F1.CodTipoObtenResult, C_F1.CodCartera, C_F1.NbrCartera, E_F1.CodEntidad, E_F1.NbrEntidad, "
				+ " EJ_F1MM.CodEjecucion as CodEjecucionF1MM, EJ_F1MM.CodMes as CodMesF1MM, EJ_F1MM.CodTipoEjecucion as FaseF1MM, "
				+ "	EV_F1MM.CodEscenarioVersion as CodEscenarioVersionF1MM,"
				+ " E_F1MM.CodEscenario as CodEscenarioF1MM, E_F1MM.NbrEscenario as NbrEscenarioF1MM, E_F1MM.FlgObligatorio as FlgObligatorioF1MM, "
				+ " JEV_F1MM.CodJuegoEscenarios as CodJuegoEscenariosF1MM, JEV_F1MM.NbrJuegoEscenarios as NbrJuegoEscenariosF1MM, "
				+ " CV_F1MM.CodCarteraVersion as CodCarteraVersionF1MM, MC_F1MM.CodMetodologia as CodMetodologiaF1MM, MC_F1MM.CodTipoObtenResult as CodTipoObtenResultF1MM "
				+ " from HE_Ejecucion EJ" + " left join HE_Ejecucion EJ_F1 on EJ.CodEjecucionVar = EJ_F1.CodEjecucion "
				+ "	left join HE_EscenarioVersion EV_F1 on EJ_F1.CodEscenarioVersion = EV_F1.CodEscenarioVersion "
				+ " left join ME_Escenario E_F1 on EV_F1.CodEscenario = E_F1.CodEscenario "
				+ " left join HE_JuegoEscenariosVersion JEV_F1 on EV_F1.CodJuegoEscenarios = JEV_F1.CodJuegoEscenarios "
				+ " left join HE_CarteraVersion CV_F1 on JEV_F1.CodCarteraVersion = CV_F1.CodCarteraVersion "
				+ " left join HE_MetodologiaCartera MC_F1 on CV_F1.CodMetodologia = MC_F1.CodMetodologia "
				+ " left join HE_Cartera C_F1 on MC_F1.CodCartera = C_F1.CodCartera "
				+ " left join HE_Entidad E_F1 on C_F1.CodEntidad = E_F1.CodEntidad "
				+ " left join HE_Ejecucion EJ_F1MM on EJ.CodEjecucionModMac = EJ_F1MM.CodEjecucion "
				+ " left join HE_EscenarioVersion EV_F1MM on EJ_F1MM.CodEscenarioVersion = EV_F1MM.CodEscenarioVersion "
				+ " left join ME_Escenario E_F1MM on EV_F1MM.CodEscenario = E_F1MM.CodEscenario "
				+ " left join HE_JuegoEscenariosVersion JEV_F1MM on EV_F1MM.CodJuegoEscenarios = JEV_F1MM.CodJuegoEscenarios "
				+ " left join HE_CarteraVersion CV_F1MM on JEV_F1MM.CodCarteraVersion = CV_F1MM.CodCarteraVersion "
				+ " left join HE_MetodologiaCartera MC_F1MM on CV_F1MM.CodMetodologia = MC_F1MM.CodMetodologia "
				+ " where EJ.CodTipoEjecucion = 3 order by EJ.CodMes desc";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ejecucionVariacion = new EjecucionVariacion();
				ejecucionVariacion.setId_ejecucion(rs.getInt("CodEjecucion"));
				ejecucionVariacion.setEstado_ejecucion(rs.getInt("TipEstadoEjecucion"));
				ejecucionVariacion.setPorcentaje_avance(rs.getInt("NumEjecucionAvance"));
				ejecucionVariacion.setDatetime_ejecucion(rs.getLong("FecEjecucion"));
				ejecucionVariacion.setFlag_regulatorio(rs.getBoolean("FlgRegulatorio"));

				// Seteamos ejecucion de fase 1 asociada (de escenario base)
				ejecucionPE = new EjecucionPE();
				ejecucionPE.setId_ejecucion(rs.getInt("CodEjecucionF1"));
				ejecucionPE.setCodmes(rs.getInt("CodMesF1"));
				ejecucionPE.setFase(rs.getInt("FaseF1"));

				entidad = new Entidad();
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));

				cartera = new Cartera();
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setEntidad(entidad);

				metodologia = new EstructuraMetodologica();
				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));

				tor = new TipoObtencionResultados();
				tor.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
				metodologia.setMetodo_resultados(tor);
				metodologia.setCartera(cartera);

				carteraVersion = new Cartera_version();
				carteraVersion.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				carteraVersion.setNombre_cartera_version(rs.getString("NbrVersion"));
				carteraVersion.setMetodologia(metodologia);

				juegoEscenarios = new JuegoEscenarios_version();
				juegoEscenarios.setId_juego_escenarios(rs.getInt("CodJuegoEscenariosF1"));
				juegoEscenarios.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenariosF1"));
				juegoEscenarios.setCartera_version(carteraVersion);

				escenarioVersion = new Escenario_version();
				escenarioVersion.setId_escenario_version(rs.getInt("CodEscenarioVersionF1"));
				escenarioVersion.setJuego_escenarios_version(juegoEscenarios);

				escenario = new Escenario();
				escenario.setId_escenario(rs.getInt("CodEscenarioF1"));
				escenario.setNombre_escenario(rs.getString("NbrEscenarioF1"));
				escenario.setFlag_obligatorio(rs.getBoolean("FlgObligatorioF1"));
				escenarioVersion.setEscenario(escenario);
				ejecucionPE.setEscenario_version(escenarioVersion);
				ejecucionVariacion.setEjecucion(ejecucionPE);

				// Seteamos ejecucion de fase 1 de modelo macro asociada (de escenario base)
				ejecucionPE = new EjecucionPE();
				ejecucionPE.setId_ejecucion(rs.getInt("CodEjecucionF1MM"));
				ejecucionPE.setCodmes(rs.getInt("CodMesF1MM"));
				ejecucionPE.setFase(rs.getInt("FaseF1MM"));

				metodologia = new EstructuraMetodologica();
				metodologia.setId_metodologia(rs.getInt("CodMetodologiaF1MM"));

				tor = new TipoObtencionResultados();
				tor.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResultF1MM"));
				metodologia.setMetodo_resultados(tor);
				metodologia.setCartera(cartera);

				carteraVersion = new Cartera_version();
				carteraVersion.setId_cartera_version(rs.getInt("CodCarteraVersionF1MM"));
				carteraVersion.setMetodologia(metodologia);

				escenarioVersion = new Escenario_version();
				escenarioVersion.setId_escenario_version(rs.getInt("CodEscenarioVersionF1MM"));

				juegoEscenarios = new JuegoEscenarios_version();
				juegoEscenarios.setId_juego_escenarios(rs.getInt("CodJuegoEscenariosF1MM"));
				juegoEscenarios.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenariosF1MM"));
				juegoEscenarios.setCartera_version(carteraVersion);

				escenarioVersion.setJuego_escenarios_version(juegoEscenarios);

				escenario = new Escenario();
				escenario.setId_escenario(rs.getInt("CodEscenarioF1MM"));
				escenario.setNombre_escenario(rs.getString("NbrEscenarioF1MM"));
				escenario.setFlag_obligatorio(rs.getBoolean("FlgObligatorioF1MM"));
				escenarioVersion.setEscenario(escenario);
				ejecucionPE.setEscenario_version(escenarioVersion);
				ejecucionVariacion.setEjecucionAnt_modMac(ejecucionPE);

				ejecucionesVariacion.add(ejecucionVariacion);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener ejecuciones de variacion: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return ejecucionesVariacion;
	}

	public void crearEjecucionVariacion(EjecucionVariacion ejecucionVariacion) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;

		Integer id_ejecucion = null;

		String sql_ejecucion = "Insert into HE_Ejecucion(CodEjecucion, CodMes, CodEscenarioVersion, CodTipoEjecucion,"
				+ " TipEstadoEjecucion, TipEstadoValidacion, TipEstadoAjustes, DesComentarioEjecucion, FecEjecucion,"
				+ " CodMetodologia, CodPrimerPaso, CodEjecucionVar, FlgRegulatorio, CodEjecucionModMac, CodEjecucionRepricing,"
				+ " CodCarteraVerOtros, FecActualizacionTabla)" + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		String sql_mapeo_ejecucion = "Insert into HE_MapeoEjecucion(CodEjecucion, CodMapeoTabla, CodCargaFichero,"
				+ " FecActualizacionTabla) values (?,?,?,?)";

		try {
			conn = obtenerConexion();
			id_ejecucion = obtenerSiguienteId("SEQ_HE_Ejecucion");

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
			pstmt_ejecucion.setInt(1, id_ejecucion);
			pstmt_ejecucion.setNull(2, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(3, java.sql.Types.INTEGER);
			pstmt_ejecucion.setInt(4, 3); // Tipo de ejecucion: Variacion
			pstmt_ejecucion.setInt(5, ejecucionVariacion.getEstado_ejecucion());
			pstmt_ejecucion.setNull(6, -1); // No se validan ejecuciones de variaciones
			pstmt_ejecucion.setNull(7, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(8, java.sql.Types.VARCHAR);
			pstmt_ejecucion.setLong(9, ejecucionVariacion.getDatetime_ejecucion());
			pstmt_ejecucion.setNull(10, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(11, java.sql.Types.INTEGER);
			pstmt_ejecucion.setInt(12, ejecucionVariacion.getEjecucion().getId_ejecucion());
			pstmt_ejecucion.setBoolean(13, ejecucionVariacion.getFlag_regulatorio());
			pstmt_ejecucion.setInt(14, ejecucionVariacion.getEjecucionAnt_modMac().getId_ejecucion());
			pstmt_ejecucion.setNull(15, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(16, java.sql.Types.INTEGER);
			pstmt_ejecucion.setDate(17, new Date(System.currentTimeMillis()));
			pstmt_ejecucion.executeUpdate();

			for (TablaMapeoEjecucion tabla_mapeo_ejecucion : ejecucionVariacion.getEjecucion()
					.getTablas_mapeo_ejecucion()) {
				try (PreparedStatement pstmt_mapeo_ejecucion = conn.prepareStatement(sql_mapeo_ejecucion)) {
					pstmt_mapeo_ejecucion.setInt(1, id_ejecucion);
					pstmt_mapeo_ejecucion.setInt(2, tabla_mapeo_ejecucion.getId_mapeo_tabla());

					// La carga es opcional en ejecuciones de variacion
					if (tabla_mapeo_ejecucion.getCarga() == null) {
						pstmt_mapeo_ejecucion.setNull(3, java.sql.Types.INTEGER);
					} else {
						pstmt_mapeo_ejecucion.setInt(3, tabla_mapeo_ejecucion.getCarga().getId_cargafichero());
					}
					pstmt_mapeo_ejecucion.setDate(4, new Date(System.currentTimeMillis()));

					pstmt_mapeo_ejecucion.executeUpdate();
				}
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar una ejecucion de variacion" + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);

			cerrarConexion(conn);
		}
	}

	public void editarEjecucionVariacion(EjecucionVariacion ejecucionVariacion) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;

		String sql_ejecucion = "Update HE_Ejecucion set TipEstadoEjecucion=?, FecEjecucion= ?, CodEjecucionVar= ?, "
				+ " FlgRegulatorio = ?, CodEjecucionModMac= ?, FecActualizacionTabla=? " + " where CodEjecucion= ?";

		String sql_mapeo_ejecucion = "Update HE_MapeoEjecucion set CodCargaFichero= ?, FecActualizacionTabla= ?"
				+ " where CodEjecucion= ? and CodMapeoTabla= ?";

		// Borramos los resultados asociados
		this.borrarResultadosVariacion(ejecucionVariacion.getId_ejecucion());

		try {
			conn = obtenerConexion();
			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
			pstmt_ejecucion.setInt(1, ejecucionVariacion.getEstado_ejecucion());
			pstmt_ejecucion.setLong(2, ejecucionVariacion.getDatetime_ejecucion());
			pstmt_ejecucion.setInt(3, ejecucionVariacion.getEjecucion().getId_ejecucion());
			pstmt_ejecucion.setBoolean(4, ejecucionVariacion.getFlag_regulatorio());
			pstmt_ejecucion.setInt(5, ejecucionVariacion.getEjecucionAnt_modMac().getId_ejecucion());
			pstmt_ejecucion.setDate(6, new Date(System.currentTimeMillis()));
			pstmt_ejecucion.setInt(7, ejecucionVariacion.getId_ejecucion());

			pstmt_ejecucion.executeUpdate();

			if (ejecucionVariacion.getEjecucion().getTablas_mapeo_ejecucion() != null) {
				for (TablaMapeoEjecucion tabla_mapeo_ejecucion : ejecucionVariacion.getEjecucion()
						.getTablas_mapeo_ejecucion()) {
					try (PreparedStatement pstmt_mapeo_ejecucion = conn.prepareStatement(sql_mapeo_ejecucion)) {

						// La carga es opcional en ejecuciones de variacion
						if (tabla_mapeo_ejecucion.getCarga() == null
								|| tabla_mapeo_ejecucion.getCarga().getId_cargafichero() == 0) {
							pstmt_mapeo_ejecucion.setNull(1, java.sql.Types.INTEGER);
						} else {
							pstmt_mapeo_ejecucion.setInt(1, tabla_mapeo_ejecucion.getCarga().getId_cargafichero());
						}

						pstmt_mapeo_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
						pstmt_mapeo_ejecucion.setInt(3, ejecucionVariacion.getId_ejecucion());
						pstmt_mapeo_ejecucion.setInt(4, tabla_mapeo_ejecucion.getId_mapeo_tabla());

						pstmt_mapeo_ejecucion.executeUpdate();
					}
				}
				ejecutarCommit(conn);

			}
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar ejecucion de variacion: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	public void borrarEjecucionVariacion(Integer id_ejecucion) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;

		String sql_ejecucion = "DELETE from HE_Ejecucion where CodEjecucion=? and CodTipoEjecucion= 3";
		try {
			conn = obtenerConexion();

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
			pstmt_ejecucion.setInt(1, id_ejecucion);
			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error al borrar una ejecucion de variacion con id: " + id_ejecucion + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	public void borrarEjecucionVariacionPorEjecAct(Integer codmes, Integer id_juego_escenarios, Integer fase)
			throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;

		Integer codEjecBase = null;
		Statement stmt_consultaCodEjecBase = null;
		ResultSet rs_consultaCodEjecBase = null;

		String sql_ejecucion = "DELETE from HE_Ejecucion where CodEjecucion=? and CodTipoEjecucion= 3";

		String sql_consultaCodEjecBase = "select E.CodEjecucion from HE_Ejecucion E "
				+ "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
				+ "left join ME_Escenario ES on EV.CodEscenario = ES.CodEscenario "
				+ "where ES.FlgObligatorio = 1 and EV.CodJuegoEscenarios = " + id_juego_escenarios + " and E.Codmes = "
				+ codmes + " and E.CodTipoEjecucion = " + fase;
		try {
			conn = obtenerConexion();

			// Consultamos el codigo de ejecucion base de la ejec. actual
			stmt_consultaCodEjecBase = crearStatement(conn);
			rs_consultaCodEjecBase = stmt_consultaCodEjecBase.executeQuery(sql_consultaCodEjecBase);

			while (rs_consultaCodEjecBase.next()) {
				codEjecBase = rs_consultaCodEjecBase.getInt("CodEjecucion");
			}

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
			pstmt_ejecucion.setInt(1, codEjecBase);
			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar una ejecucion de variacion con id: " + codEjecBase + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarStatement(stmt_consultaCodEjecBase);
			cerrarResultSet(rs_consultaCodEjecBase);
			cerrarConexion(conn);
		}
	}

	// -------------------------------------------------------------------
	// ---------------------EJECUCIONES VALIDACION------------------------
	// -------------------------------------------------------------------
	public ArrayList<EjecucionValidacion> obtenerEjecucionesValidacion() throws Exception {
		ArrayList<EjecucionValidacion> ejecucionesValidacion = new ArrayList<EjecucionValidacion>();
		EjecucionValidacion ejecucionValidacion = null;
		JuegoEscenarios_version juegoEscenarios_version = null;
		Cartera_version cartera_version = null;
		EstructuraMetodologica metodologia = null;
		Cartera cartera = null;
		TipoAmbito tipo_ambito = null;
		Entidad entidad = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select E.CodMes, E.CodTipoEjecucion," + " max(E.TipEstadoValidacion) as EstadoValidacion,"
		// + " xmlagg(xmlelement(e, esc.nbrescenario || ': \n' || e.deslogmotor,
		// '\n').extract('//text()')).getclobval() AS deslogmotor,"
				+ " max(E.DesComentarioEjecucion) as ComentarioEjecucion, "
				+ " max(E.TipEstadoAjustes) as EstadoAjusteMayor, "
				+ " min(E.TipEstadoAjustes) as EstadoAjusteMenor, JEV.CodJuegoEscenarios, "
				+ " max(JEV.NbrJuegoEscenarios) as NbrJuegoEscenarios, "
				+ " max(JEV.DesJuegoEscenarios) as DesJuegoEscenarios, "
				+ " max(CV.CodCarteraVersion) as CodCarteraVersion," + " max(CV.NbrVersion) as NbrVersion, "
				+ " max(MC.CodMetodologia) as CodMetodologia," + " max(MC.NbrMetodologia) as NbrMetodologia, "
				+ " max(C.CodCartera) as CodCartera," + " max(C.NbrCartera) as NbrCartera,"
				+ " max(TA.CodTipoAmbito) as CodTipoAmbito, max(TA.NbrTipoAmbito) as NbrTipoAmbito, "
				+ " max(E.CodEntidad) as CodEntidad, " + " max(E.NbrEntidad) as NbrEntidad " + " from HE_Ejecucion E "
				+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion"
				+ " LEFT JOIN me_escenario esc ON ev.codescenario = esc.codescenario "
				+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
				+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
				+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
				+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
				+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad "
				+ " where E.CodTipoEjecucion in (1,2,7,8) and E.TipEstadoEjecucion in (2,4) "
				+ " and E.TipEstadoValidacion in (0,1,2,3,4,5) "
				+ " group by E.codmes, JEV.CodJuegoEscenarios, E.CodTipoEjecucion order by E.codmes desc";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ejecucionValidacion = new EjecucionValidacion();
				juegoEscenarios_version = new JuegoEscenarios_version();
				cartera_version = new Cartera_version();
				metodologia = new EstructuraMetodologica();
				cartera = new Cartera();
				tipo_ambito = new TipoAmbito();
				entidad = new Entidad();

				tipo_ambito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipo_ambito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));

				ejecucionValidacion.setCodmes(rs.getInt("CodMes"));

				if (tipo_ambito.getId_tipo_ambito().equals(1)) { // Creditos
					ejecucionValidacion.setFase(rs.getInt("CodTipoEjecucion"));
				} else if (tipo_ambito.getId_tipo_ambito().equals(2)) { // Inversiones
					ejecucionValidacion.setFase(rs.getInt("CodTipoEjecucion") - 6);
				}

				ejecucionValidacion.setEstado_validacion(rs.getInt("EstadoValidacion"));
				// ejecucionValidacion.setLog_motor(rs.getString("DesLogMotor"));
				/*
				 * Reader in = rs.getCharacterStream("DesLogMotor"); StringBuffer buffer = new
				 * StringBuffer(); int ch; while ((ch = in.read()) != -1) { buffer.append("" +
				 * (char) ch); } ejecucionValidacion.setLog_motor(buffer.toString());
				 */
				ejecucionValidacion.setComentario_ejecucion(rs.getString("ComentarioEjecucion"));

				if (rs.getInt("EstadoAjusteMayor") == 0) { // En caso algun estado de ajustes no es 0 ('Sin ajuste')
					ejecucionValidacion.setEstadoAgregado_ajustes(0); // Sin ajustes solicitados
				} else if (rs.getInt("EstadoAjusteMayor") == 2 && rs.getInt("EstadoAjusteMenor") == 2) { // Todos en
																											// 'ajustes
																											// por
																											// aprobar'
					ejecucionValidacion.setEstadoAgregado_ajustes(1); // Ajustes listos
				} else if (rs.getInt("EstadoAjusteMayor") == 3 && rs.getInt("EstadoAjusteMenor") == 3) { // Todos
																											// aprobados
					ejecucionValidacion.setEstadoAgregado_ajustes(3); // Ajustes aprobados
				} else {
					ejecucionValidacion.setEstadoAgregado_ajustes(2); // Ajustes incompletos
				}

				juegoEscenarios_version.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenarios_version.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenarios_version.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));
				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));
				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));

				cartera.setEntidad(entidad);
				cartera.setTipo_ambito(tipo_ambito);
				metodologia.setCartera(cartera);
				cartera_version.setMetodologia(metodologia);
				juegoEscenarios_version.setCartera_version(cartera_version);
				ejecucionValidacion.setJuego_escenarios_version(juegoEscenarios_version);
				ejecucionesValidacion.add(ejecucionValidacion);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener ejecuciones a validar: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return ejecucionesValidacion;
	}

	public ArrayList<LogEjecEscenario> obtenerLogsEjecucionAgregada(Integer codmes, Integer id_juego_escenarios,
			Integer fase, Integer id_tipo_ambito) throws Exception {

		ArrayList<LogEjecEscenario> logsEjecEscenario = new ArrayList<LogEjecEscenario>();
		LogEjecEscenario logEjecEscenario = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select E.DesLogMotor from HE_Ejecucion E "
				+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
				+ " left join ME_Escenario ESC on EV.CodEscenario = ESC.CodEscenario " + " where E.codmes = " + codmes
				+ " and EV.CodJuegoEscenarios = " + id_juego_escenarios + " and E.CodTipoEjecucion = "
				+ (fase + (id_tipo_ambito.equals(2) ? 6 : 0)) + " and ESC.CodEscenario = ";
		// Tipos de ejecucion f1 y f2 para inversiones son 7 y 8
		try {
			conn = obtenerConexion();

			for (Escenario esc : this.obtenerEscenarios(conn)) {
				stmt = crearStatement(conn);
				rs = stmt.executeQuery(sql + esc.getId_escenario());

				logEjecEscenario = new LogEjecEscenario();
				logEjecEscenario.setEscenario(esc);

				while (rs.next()) {
					logEjecEscenario.setLog_escenario(rs.getString("DesLogMotor"));
				}
				logsEjecEscenario.add(logEjecEscenario);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener logs por escenario de una ejecucion agregada : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return logsEjecEscenario;
	}

	public LogEjecEscenario obtenerLogEjecucionPorIdEjec(Integer id_ejecucion) throws Exception {

		LogEjecEscenario logEjec = new LogEjecEscenario();

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select DesLogMotor from HE_Ejecucion where CodEjecucion = " + id_ejecucion;

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				logEjec.setLog_escenario(rs.getString("DesLogMotor"));
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener log por id ejecucion : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return logEjec;
	}

	public void editarEjecucionValidacion(EjecucionValidacion ejecucionValidacion) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		PreparedStatement pstmt_ajustes = null;

		String sql_ejecucion = "Update (select HE_Ejecucion.* from HE_Ejecucion left join HE_EscenarioVersion "
				+ " on HE_Ejecucion.CodEscenarioVersion = HE_EscenarioVersion.CodEscenarioVersion"
				+ " where HE_EscenarioVersion.CodJuegoEscenarios = ? "
				+ " and HE_Ejecucion.CodMes= ? and HE_Ejecucion.CodTipoEjecucion= ?)"
				+ " set DesComentarioEjecucion= ?, TipEstadoValidacion= ?, FecActualizacionTabla = ? ";

		String sql_ajustes = "Update (select HE_Ejecucion.* from HE_Ejecucion left join HE_EscenarioVersion "
				+ " on HE_Ejecucion.CodEscenarioVersion = HE_EscenarioVersion.CodEscenarioVersion"
				+ " where HE_EscenarioVersion.CodJuegoEscenarios = ? "
				+ " and HE_Ejecucion.CodMes= ? and HE_Ejecucion.CodTipoEjecucion= ?"
				+ " and HE_Ejecucion.TipEstadoAjustes in (1,2,3))"
				+ " set TipEstadoAjustes= ?, FecActualizacionTabla = ? ";

		/*
		 * String sql_ajustes = "Update HE_Ejecucion E, HE_EscenarioVersion EV " +
		 * "set E.TipEstadoAjustes= ?, E.FecActualizacionTabla = ? " +
		 * "where E.CodEscenarioVersion = EV.CodEscenarioVersion " +
		 * "and EV.CodJuegoEscenarios = ? and E.CodMes= ? and E.CodTipoEjecucion= ? " +
		 * "and E.TipEstadoAjustes in (1,2,3)";
		 */

		/*
		 * (select EV.CodJuegoEscenarios from HE_EscenarioVersion EV where " +
		 * "E.CodEscenarioVersion= EV.CodEscenarioVersion) = ?
		 */
		try {
			conn = obtenerConexion();
			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);

			pstmt_ejecucion.setInt(1, ejecucionValidacion.getJuego_escenarios_version().getId_juego_escenarios());
			pstmt_ejecucion.setInt(2, ejecucionValidacion.getCodmes());

			if (ejecucionValidacion.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getId_tipo_ambito().equals(1)) { // Creditos
				pstmt_ejecucion.setInt(3, ejecucionValidacion.getFase());
			} else if (ejecucionValidacion.getJuego_escenarios_version().getCartera_version().getMetodologia()
					.getCartera().getTipo_ambito().getId_tipo_ambito().equals(2)) { // Inversiones
				pstmt_ejecucion.setInt(3, ejecucionValidacion.getFase() + 6);
			}

			pstmt_ejecucion.setString(4, ejecucionValidacion.getComentario_ejecucion());
			pstmt_ejecucion.setInt(5, ejecucionValidacion.getEstado_validacion());
			pstmt_ejecucion.setDate(6, new Date(System.currentTimeMillis()));
			pstmt_ejecucion.executeUpdate();

			// Cambiamos el estado de ajustes a 'ajustes aprobados' si el estado de
			// validacion pasa a 'oficial' o 'no oficial'
			// (solo si estaba como pendiente de ajustar o ajustado sin aprobacion)
			if (ejecucionValidacion.getEstado_validacion().equals(4)
					|| ejecucionValidacion.getEstado_validacion().equals(5)) {
				pstmt_ajustes = conn.prepareStatement(sql_ajustes);

				pstmt_ajustes.setInt(1, ejecucionValidacion.getJuego_escenarios_version().getId_juego_escenarios());
				pstmt_ajustes.setInt(2, ejecucionValidacion.getCodmes());

				if (ejecucionValidacion.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
						.getTipo_ambito().getId_tipo_ambito().equals(1)) { // Creditos
					pstmt_ajustes.setInt(3, ejecucionValidacion.getFase());
				} else if (ejecucionValidacion.getJuego_escenarios_version().getCartera_version().getMetodologia()
						.getCartera().getTipo_ambito().getId_tipo_ambito().equals(2)) { // Inversiones
					pstmt_ajustes.setInt(3, ejecucionValidacion.getFase() + 6);
				}

				pstmt_ajustes.setInt(4, 3); // Estado_ajuste: 'ajustes aprobados'
				pstmt_ajustes.setDate(5, new Date(System.currentTimeMillis()));
				pstmt_ajustes.executeUpdate();
			}

			ejecutarCommit(conn);

			// En caso se cambien ejecuciones a estado de simulacion (rechazo de
			// validacion/oficializacion),
			// debemos eliminar las dependencias de fase 2 y variaciones (en caso exista,
			// solo para creditos)
			if (ejecucionValidacion.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getId_tipo_ambito().equals(1)) {
				if (ejecucionValidacion.getEstado_validacion().equals(-1)) {
					if (ejecucionValidacion.getFase().equals(1)) { // Borramos fase 2 y variaciones solo si es fase1
						this.borrarEjecucionAgregadaPE(ejecucionValidacion.getCodmes(),
								ejecucionValidacion.getJuego_escenarios_version().getId_juego_escenarios(), 2);

						this.borrarEjecucionVariacionPorEjecAct(ejecucionValidacion.getCodmes(),
								ejecucionValidacion.getJuego_escenarios_version().getId_juego_escenarios(),
								ejecucionValidacion.getFase());

					} else if (ejecucionValidacion.getFase().equals(2)) { // En el caso de fase2, directamente lo
																			// eliminamos
																			// No se puede regresar a simulaciones!
						this.borrarEjecucionAgregadaPE(ejecucionValidacion.getCodmes(),
								ejecucionValidacion.getJuego_escenarios_version().getId_juego_escenarios(),
								ejecucionValidacion.getFase());
					}
				}
			}

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar ejecucion de validacion: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarPreparedStatement(pstmt_ajustes);
			cerrarConexion(conn);
		}
	}

	public void solicitarAjustesEjecucionValidacion(EjecucionValidacion ejecucionValidacion) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;

		String sql_ejecucion = "Update HE_Ejecucion E set E.TipEstadoAjustes= ?, E.FecActualizacionTabla=? "
				+ " where (select EV.CodJuegoEscenarios from HE_EscenarioVersion EV where "
				+ " E.CodEscenarioVersion= EV.CodEscenarioVersion) = ? " + " and E.CodMes= ? and E.CodTipoEjecucion= ?";

		try {
			conn = obtenerConexion();
			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);

			pstmt_ejecucion.setInt(1, 1); // Pendiente de ajuste
			pstmt_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
			pstmt_ejecucion.setInt(3, ejecucionValidacion.getJuego_escenarios_version().getId_juego_escenarios());
			pstmt_ejecucion.setInt(4, ejecucionValidacion.getCodmes());

			if (ejecucionValidacion.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getId_tipo_ambito().equals(1)) { // Creditos
				pstmt_ejecucion.setInt(5, ejecucionValidacion.getFase());
			} else if (ejecucionValidacion.getJuego_escenarios_version().getCartera_version().getMetodologia()
					.getCartera().getTipo_ambito().getId_tipo_ambito().equals(2)) { // Inversiones
				pstmt_ejecucion.setInt(5, ejecucionValidacion.getFase() + 6);
			}

			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al solicitar ajuste: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	// -------------------------------------------------------------------
	// --------------------EJECUCIONES AJUSTES OUTPUT---------------------
	// -------------------------------------------------------------------
	public ArrayList<EjecucionAjustes> obtenerEjecucionesAjustes() throws Exception {
		ArrayList<EjecucionAjustes> ejecucionesAjustes = new ArrayList<EjecucionAjustes>();
		EjecucionAjustes ejecucionAjustes = null;
		Escenario_version escenario_version = null;
		JuegoEscenarios_version juegoEscenarios_version = null;
		Escenario escenario = null;
		Cartera_version cartera_version = null;
		EstructuraMetodologica metodologia = null;
		Cartera cartera = null;
		Entidad entidad = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select EJ.CodEjecucion, EJ.CodMes, EJ.TipEstadoAjustes, EJ.CodTipoEjecucion, "
				+ "EV.CodEscenarioVersion, ES.CodEscenario, ES.NbrEscenario, ES.FlgObligatorio, "
				+ "JEV.CodJuegoEscenarios, JEV.NbrJuegoEscenarios, "
				+ "JEV.DesJuegoEscenarios, CV.CodCarteraVersion, CV.NbrVersion, MC.CodMetodologia, "
				+ "MC.NbrMetodologia, C.CodCartera, C.NbrCartera, E.CodEntidad, E.NbrEntidad " + "from HE_Ejecucion EJ "
				+ "left join HE_EscenarioVersion EV on EJ.CodEscenarioVersion = EV.CodEscenarioVersion "
				+ "left join ME_Escenario ES on EV.CodEscenario = ES.CodEscenario "
				+ "left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
				+ "left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
				+ "left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ "left join HE_Cartera C on MC.CodCartera = C.CodCartera "
				+ "left join HE_Entidad E on C.CodEntidad = E.CodEntidad "
				+ "where EJ.CodTipoEjecucion in (1,2,7,8) and EJ.TipEstadoAjustes in (1,2,3) "
				+ "order by EJ.codmes desc";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ejecucionAjustes = new EjecucionAjustes();
				escenario_version = new Escenario_version();
				escenario = new Escenario();
				juegoEscenarios_version = new JuegoEscenarios_version();
				cartera_version = new Cartera_version();
				metodologia = new EstructuraMetodologica();
				cartera = new Cartera();
				entidad = new Entidad();

				ejecucionAjustes.setId_ejecucion(rs.getInt("CodEjecucion"));
				ejecucionAjustes.setCodmes(rs.getInt("CodMes"));
				ejecucionAjustes.setEstado_ajustes(rs.getInt("TipEstadoAjustes"));
				ejecucionAjustes.setFase(rs.getInt("CodTipoEjecucion"));
				escenario_version.setId_escenario_version(rs.getInt("CodEscenarioVersion"));
				escenario.setId_escenario(rs.getInt("CodEscenario"));
				escenario.setNombre_escenario(rs.getString("NbrEscenario"));
				escenario.setFlag_obligatorio(rs.getBoolean("FlgObligatorio"));
				juegoEscenarios_version.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenarios_version.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenarios_version.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));
				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));
				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));

				escenario_version.setEscenario(escenario);
				cartera.setEntidad(entidad);
				metodologia.setCartera(cartera);
				cartera_version.setMetodologia(metodologia);
				juegoEscenarios_version.setCartera_version(cartera_version);
				escenario_version.setJuego_escenarios_version(juegoEscenarios_version);
				ejecucionAjustes.setEscenario_version(escenario_version);
				ejecucionesAjustes.add(ejecucionAjustes);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener ejecuciones a ajustar: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return ejecucionesAjustes;
	}

	public InfoValidacionAjustes obtenerInfoValidacionAjustes(Integer id_ejecucion) throws Exception {
		ArrayList<AjusteRegistro> ajustesRegistro = new ArrayList<AjusteRegistro>();
		AjusteRegistro ajusteRegistro = null;
		InfoValidacionAjustes infoValidacionAjustes = new InfoValidacionAjustes();

		Connection conn = null;
		Statement stmt_ejecucion = null;
		Statement stmt_ajustes = null;
		ResultSet rs_ejecucion = null;
		ResultSet rs_ajustes = null;
		String sql_ejecucion = "select DesComentarioEjecucion from HE_Ejecucion " + "where CodEjecucion = "
				+ id_ejecucion;
		String sql_ajustes = "select NbrCodOperacion, NbrCodCliente, NumPeOriginal, NumPe, "
				+ "NumStageOriginal, NumStage, NumPdOriginal, NumPd, DesAjuste " + "from HE_OutputProvisionesCred "
				+ "where FlgAjustado = 1 and CodEjecucion = " + id_ejecucion;
		try {
			conn = obtenerConexion();

			stmt_ejecucion = crearStatement(conn);
			rs_ejecucion = stmt_ejecucion.executeQuery(sql_ejecucion);
			while (rs_ejecucion.next()) {
				infoValidacionAjustes.setComentarios_validacion(rs_ejecucion.getString("DesComentarioEjecucion"));
			}

			stmt_ajustes = crearStatement(conn);
			rs_ajustes = stmt_ajustes.executeQuery(sql_ajustes);
			while (rs_ajustes.next()) {
				ajusteRegistro = new AjusteRegistro();

				ajusteRegistro.setCod_operacion(rs_ajustes.getString("NbrCodOperacion"));
				ajusteRegistro.setCod_cliente(rs_ajustes.getString("NbrCodCliente"));
				ajusteRegistro.setValorPE_original(rs_ajustes.getDouble("NumPeOriginal"));
				ajusteRegistro.setValorPE_ajustado(rs_ajustes.getDouble("NumPe"));
				ajusteRegistro.setValorStage_original(rs_ajustes.getInt("NumStageOriginal"));
				ajusteRegistro.setValorStage_ajustado(rs_ajustes.getInt("NumStage"));
				ajusteRegistro.setValorPD_original(rs_ajustes.getDouble("NumPdOriginal"));
				ajusteRegistro.setValorPD_ajustado(rs_ajustes.getDouble("NumPd"));
				ajusteRegistro.setDescripcion_ajuste(rs_ajustes.getString("DesAjuste"));

				ajustesRegistro.add(ajusteRegistro);
			}

			infoValidacionAjustes.setAjustes_registro(ajustesRegistro);

		} catch (Exception ex) {
			LOGGER.error("Error al obtener informacion de validacion y ajustes: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_ajustes);
			cerrarResultSet(rs_ejecucion);
			cerrarStatement(stmt_ajustes);
			cerrarStatement(stmt_ejecucion);
			cerrarConexion(conn);
		}
		return infoValidacionAjustes;
	}

	public ArrayList<CampoOutputCredito> obtenerCamposOutputCredito() throws Exception {
		ArrayList<CampoOutputCredito> camposOutputCredito = new ArrayList<CampoOutputCredito>();
		CampoOutputCredito campoOutputCredito = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select column_name, data_type from all_tab_columns where table_name = 'HE_OUTPUTPROVISIONESCRED'";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if (!rs.getString("column_name").toLowerCase().equals(("CodEjecucion").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("FecEjecucion").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("CodEscenarioVersion").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("CodTipoEjecucion").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("CodJuegoEscenarios").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("CodCarteraVersion").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("CodEscenario").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("CodCartera").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("CodEntidad").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("FlgAjustado").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("FlgJuegoOficial").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("DesAjuste").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumStageOriginal").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumPeOriginal").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumPdOriginal").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumLgd").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumEad").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumSaldoBalance").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumSaldoContable").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumPd12").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumPdMarg").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumLgdBe").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumLgdWo").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumBucket").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NbrRating").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NbrCodMoneda").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumMaduracion").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumDiasAtraso").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumDeudaDirecta").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumDeudaSoles").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NumDeudaTotal").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NbrSegRegulatorio").toLowerCase())
						&& !rs.getString("column_name").toLowerCase().equals(("NbrClacRegulatoria").toLowerCase())) {

					campoOutputCredito = new CampoOutputCredito();
					campoOutputCredito.setNombre_campo(rs.getString("column_name"));

					if (rs.getString("data_type").equals("NUMBER")) {
						campoOutputCredito.setTipo_dato(1);
					} else if (rs.getString("data_type").equals("VARCHAR2")) {
						campoOutputCredito.setTipo_dato(2);
					}

					camposOutputCredito.add(campoOutputCredito);
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener campos de la tabla output de credito: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return camposOutputCredito;
	}

	public ArrayList<RegistroOutputCredito> obtenerRegistrosOutputCredito(
			ConsultaRegistrosOutput consultaRegistrosOutput) throws Exception {
		ArrayList<RegistroOutputCredito> registrosOutputCredito = new ArrayList<RegistroOutputCredito>();
		RegistroOutputCredito registroOutputCredito = null;
		CampoOutputCredito campoOutputCredito = null;
		ArrayList<CampoValor> camposValor = null;
		CampoValor campoValor = null;
		Boolean primerFiltro = true;

		ArrayList<CampoOutputCredito> camposVisualizar = null;
		ArrayList<CampoOutputCredito> camposFiltrar = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "select ";

		// Iteramos por cada campo output credito (seleccion de campos)

		// Solo los que tengan el flag visualizar
		camposVisualizar = consultaRegistrosOutput.getCampos_output_credito().stream()
				.filter(campo -> campo.getVisualizar() == true).collect(Collectors.toCollection(ArrayList::new));

		Iterator<CampoOutputCredito> iteradorCampoOutput = camposVisualizar.iterator();
		while (iteradorCampoOutput.hasNext()) {
			campoOutputCredito = iteradorCampoOutput.next();

			sql += campoOutputCredito.getNombre_campo();

			if (iteradorCampoOutput.hasNext()) {
				sql += ", ";
			} else if (!iteradorCampoOutput.hasNext()) {
				sql += " ";
			}
		}

		sql += "from HE_OutputProvisionesCred where CodEjecucion = " + consultaRegistrosOutput.getId_ejecucion();

		// Iteramos por cada campo output credito (condiciones de filtrado)
		// Solo los que tengan el flag visualizar
		camposFiltrar = consultaRegistrosOutput.getCampos_output_credito().stream()
				.filter(campo -> campo.getFiltrar() == true).collect(Collectors.toCollection(ArrayList::new));

		iteradorCampoOutput = camposFiltrar.iterator();

		while (iteradorCampoOutput.hasNext()) {
			campoOutputCredito = iteradorCampoOutput.next();

			// Solo al primer filtro
			if (primerFiltro) {
				sql += " and ";
				primerFiltro = false;
			}

			sql += campoOutputCredito.getNombre_campo();

			if (campoOutputCredito.getValor_filtro().split(",").length > 1) {
				sql += " in (";
				sql += campoOutputCredito.getValor_filtro();
				sql += ") ";
			} else {
				sql += " = ";
				sql += campoOutputCredito.getValor_filtro();
			}

			if (iteradorCampoOutput.hasNext()) {
				sql += "and ";
			} else if (!iteradorCampoOutput.hasNext()) {
				sql += " ";
			}
		}

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				registroOutputCredito = new RegistroOutputCredito();

				// Iteramos por cada campo output credito
				iteradorCampoOutput = consultaRegistrosOutput.getCampos_output_credito().iterator();

				camposValor = new ArrayList<CampoValor>();
				while (iteradorCampoOutput.hasNext()) {
					campoOutputCredito = iteradorCampoOutput.next();

					if (campoOutputCredito.getVisualizar()) {
						campoValor = new CampoValor();
						campoValor.setNombre(campoOutputCredito.getNombre_campo());
						campoValor.setTipo_dato(campoOutputCredito.getTipo_dato());

						if (campoValor.getTipo_dato().equals(1)) {
							campoValor.setValor_numero(rs.getDouble(campoValor.getNombre()));
						} else if (campoValor.getTipo_dato().equals(2)) {
							campoValor.setValor_texto(rs.getString(campoValor.getNombre()));
						}
						camposValor.add(campoValor);

						// Codigo cliente y operacion
						if (campoValor.getNombre().toLowerCase().equals(("NbrCodOperacion").toLowerCase())) {
							registroOutputCredito.setCod_operacion(campoValor.getValor_texto());
						}

						if (campoValor.getNombre().toLowerCase().equals(("NbrCodCliente").toLowerCase())) {
							registroOutputCredito.setCod_cliente(campoValor.getValor_texto());
						}
					}
				}

				registroOutputCredito.setCampos_valores(camposValor);

				registrosOutputCredito.add(registroOutputCredito);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener registros output de credito: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return registrosOutputCredito;
	}

	public void ajustarRegistrosOutputCredito(GrupoRegistrosOutput grupoRegistroOutput) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;

		Integer numUnico = null;
		Double valorPE_original = null;
		Integer valorStage_original = null;
		Double valorPD_original = null;

		String sql_ejecucion = "Update HE_Ejecucion set TipEstadoAjustes = ? " + "where CodEjecucion = ?";

		String sql_update_registro = "Update HE_OutputProvisionesCred set FlgAjustado = ?, NumPe = ?, NumPeOriginal = ?, "
				+ "NumStage = ?, NumStageOriginal = ?, NumPd = ?, NumPdOriginal = ?, DesAjuste = ? "
				+ "where CodEjecucion = ? and NbrCodOperacion = ? and NbrCodCliente = ?";

		String sql_insert_registro = "Insert into HE_OutputProvisionesCred (CodEjecucion, FecEjecucion, CodEscenarioVersion, CodTipoEjecucion, "
				+ "CodJuegoEscenarios, CodCarteraVersion, CodEscenario, CodCartera, CodEntidad, DesAjuste, NumPe, "
				+ "NumStage, NumPd, NumCodOperacion, NumCodCliente) "
				+ "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		try {
			conn = obtenerConexion();
			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);

			pstmt_ejecucion.setInt(1, 2); // Estado: 'Ajustado sin aprobar'
			pstmt_ejecucion.setInt(2, grupoRegistroOutput.getId_ejecucion());
			pstmt_ejecucion.executeUpdate();

			for (RegistroOutputCredito registroOutputCredito : grupoRegistroOutput.getRegistros_output()) {
				if (registroOutputCredito.getCod_operacion().contains("OPFICTICIO")
						&& registroOutputCredito.getCod_cliente().contains("CLIFICTICIO")) {

					try (PreparedStatement pstmt_insert_registro = conn.prepareStatement(sql_insert_registro)) {

						pstmt_insert_registro.setInt(1, grupoRegistroOutput.getId_ejecucion());
						pstmt_insert_registro.setInt(2, grupoRegistroOutput.getCodmes());
						pstmt_insert_registro.setInt(3,
								grupoRegistroOutput.getEscenario_version().getId_escenario_version());
						pstmt_insert_registro.setInt(4, grupoRegistroOutput.getFase());
						pstmt_insert_registro.setInt(5, grupoRegistroOutput.getEscenario_version()
								.getJuego_escenarios_version().getId_juego_escenarios());
						pstmt_insert_registro.setInt(6, grupoRegistroOutput.getEscenario_version()
								.getJuego_escenarios_version().getCartera_version().getId_cartera_version());
						pstmt_insert_registro.setInt(7,
								grupoRegistroOutput.getEscenario_version().getEscenario().getId_escenario());
						pstmt_insert_registro.setInt(8,
								grupoRegistroOutput.getEscenario_version().getJuego_escenarios_version()
										.getCartera_version().getMetodologia().getCartera().getId_cartera());
						pstmt_insert_registro.setInt(9,
								grupoRegistroOutput.getEscenario_version().getJuego_escenarios_version()
										.getCartera_version().getMetodologia().getCartera().getEntidad()
										.getId_entidad());

						pstmt_insert_registro.setString(10, registroOutputCredito.getDescripcion_ajuste());
						pstmt_insert_registro.setDouble(11, registroOutputCredito.getValorPE_ajustado());
						pstmt_insert_registro.setInt(12, registroOutputCredito.getStage_ajustado().getId_stage());
						pstmt_insert_registro.setDouble(13, registroOutputCredito.getValorPD_ajustado());
						pstmt_insert_registro.setString(14,
								registroOutputCredito.getCod_operacion() + numUnico.toString());
						pstmt_insert_registro.setString(15,
								registroOutputCredito.getCod_cliente() + numUnico.toString());
						pstmt_insert_registro.executeUpdate();

					}
				} else {
					try (PreparedStatement pstmt_update_registro = conn.prepareStatement(sql_update_registro)) {

						// Hallamos los valores originales
						for (CampoValor campoValor : registroOutputCredito.getCampos_valores()) {
							if (campoValor.getNombre().toLowerCase().equals(("NumPe").toLowerCase())) {
								valorPE_original = campoValor.getValor_numero();
							}

							if (campoValor.getNombre().toLowerCase().equals(("NumStage").toLowerCase())) {
								valorStage_original = campoValor.getValor_numero().intValue();
							}

							if (campoValor.getNombre().toLowerCase().equals(("NumPd").toLowerCase())) {
								valorPD_original = campoValor.getValor_numero();
							}
						}

						numUnico = obtenerSiguienteId("SEQ_HE_OutputProvisionesCred");

						pstmt_update_registro.setBoolean(1, true);
						pstmt_update_registro.setDouble(2, registroOutputCredito.getValorPE_ajustado());
						pstmt_update_registro.setDouble(3, valorPE_original);
						pstmt_update_registro.setInt(4, registroOutputCredito.getStage_ajustado().getId_stage());
						pstmt_update_registro.setInt(5, valorStage_original);
						pstmt_update_registro.setDouble(6, registroOutputCredito.getValorPD_ajustado());
						pstmt_update_registro.setDouble(7, valorPD_original);
						pstmt_update_registro.setString(8, registroOutputCredito.getDescripcion_ajuste());
						pstmt_update_registro.setInt(9, grupoRegistroOutput.getId_ejecucion());
						pstmt_update_registro.setString(10, registroOutputCredito.getCod_operacion());
						pstmt_update_registro.setString(11, registroOutputCredito.getCod_cliente());
						pstmt_update_registro.executeUpdate();
					}
				}
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar registro en ajustes de ejecucion: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);

			cerrarConexion(conn);
		}
	}

	public OutputModeloMacro obtenerOutputModeloMacro(OutputModeloMacro output) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM HE_OutputModeloMacro WHERE FecEjecucion=? "
				+ " and CodCarteraVersion=? and CodEscenarioVersion=?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, output.getFecEjecucion());
			pstmt.setInt(2, output.getCodCarteraVersion());
			pstmt.setInt(3, output.getCodEscenarioVersion());
			rs = pstmt.executeQuery(sql);
			while (rs.next()) {
				output.setNbrFactor(rs.getString("NbrFactor"));
				output.setNumNivel1(rs.getInt("NumNivel1"));
				output.setNumNivel2(rs.getInt("NumNivel2"));
				output.setNumNivel3(rs.getInt("NumNivel3"));
				output.setNumNivel4(rs.getInt("NumNivel4"));
				output.setNumNivel5(rs.getInt("NumNivel5"));
				output.setNumNivel6(rs.getInt("NumNivel6"));
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los numero de los niveles output macro de la cartera: "
					+ output.getCodCarteraVersion() + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
		return output;
	}

	// -------------------------------------------------------------------
	// ---------------------EJECUCIONES PE AGREGADAS----------------------
	// -------------------------------------------------------------------
	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesAgregadaPE_fase1(Integer id_tipo_ambito) throws Exception {
		return this.obtenerEjecucionesAgregadaPE(1, id_tipo_ambito);
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesAgregadaPE_fase2(Integer id_tipo_ambito) throws Exception {
		return this.obtenerEjecucionesAgregadaPE(2, id_tipo_ambito);
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesAgregadaPE(Integer fase, Integer id_tipo_ambito)
			throws Exception {
		ArrayList<EjecucionAgregadaPE> ejecucionesAgregada = new ArrayList<EjecucionAgregadaPE>();
		EjecucionAgregadaPE ejecucionAgregada = null;
		JuegoEscenarios_version juegoEscenarios_version = null;
		Cartera_version cartera_version = null;
		EstructuraMetodologica metodologia = null;
		MotorCalculo motor = null;
		TipoFlujo tipoFlujo = null;
		TipoObtencionResultados tipoObtenResult = null;
		TipoMetodologia tipoMetodologia = null;
		TipoPeriodicidad tipoPeriodicidad = null;
		Cartera cartera = null;
		TipoAmbito tipoAmbito = null;
		TipoMagnitud tipoMagnitud = null;
		TipoCartera tipoCartera = null;
		Entidad entidad = null;

		EjecucionPrimerPaso ejecucionModMacro = null;
		PrimerPaso primerPaso = null;
		Boolean sinResultado = null;

		EjecucionPrimerPaso ejecucionRepricing = null;
		PrimerPaso primerPasoRep = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;

		Statement stmt_ejecModMacro = null;
		ResultSet rs_ejecModMacro = null;

		String sql_ejecModMacro = null;

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);

			if (fase.equals(1) && this.obtenerCodmesActual(conn, fase, id_tipo_ambito, false) == null) { // Fase 1 (no
																											// incluye
																											// actual)
				sql = "select E.CodMes, JEV.CodJuegoEscenarios, "
						+ " min(E.TipEstadoEjecucion) as EstadoEjecucionMin, avg(E.NumEjecucionAvance) as NumEjecucionAvanceProm, "
						+ " max(E.FecEjecucion) as FecEjecucion,"
						+ " min(E.TipEstadoValidacion) as EstadoValidacionMin, "
						+ " max(JEV.NbrJuegoEscenarios) as NbrJuegoEscenarios, "
						+ " max(JEV.DesJuegoEscenarios) as DesJuegoEscenarios, "
						+ " max(CV.CodCarteraVersion) as CodCarteraVersion," + " max(CV.NbrVersion) as NbrVersion, "
						+ " max(MC.CodMetodologia) as CodMetodologia," + " max(MC.NbrMetodologia) as NbrMetodologia, "
						+ " max(MC.NbrCodigoEstructura) as NbrCodigoEstructura, max(MC.NbrFuncPlumbr) as NbrFuncPlumbr, "
						+ " max(MC.NumPuertoPlumbr) as NumPuertoPlumbr, max(MC.NumBuckets) as NumBuckets,"
						+ " max(MC.NumLifeTime) as NumLifeTime, max(MC.NumMaxPlazo) as NumMaxPlazo, max(MC.FlgLgdUnica) as FlgLgdUnica,"
						+ " max(MOC.CodMotor) as CodMotor, max(MOC.NbrMotor) as NbrMotor, "
						+ " max(TF.CodTipoFlujo) as CodTipoFlujo, max(TF.NbrTipoFlujo) as NbrTipoFlujo,"
						+ " max(TOR.CodTipoObtenResult) as CodTipoObtenResult, max(TOR.NbrTipoObtenResult) as NbrTipoObtenResult,"
						+ " max(TP.CodTipoPeriodicidad) as CodTipoPeriodicidad, max(TP.NbrTipoPeriodicidad) as NbrTipoPeriodicidad, max(TP.NumValorPeriodicidad) as NumValorPeriodicidad,"
						+ " max(TMPD.CodTipoMetodologia) as CodTipoMetodologiaPD, max(TMPD.NbrTipoMetodologia) as NbrTipoMetodologiaPD, max(TMPD.NbrVariableMotor) as NbrVariableMotorPD,"
						+ " max(TMLGD.CodTipoMetodologia) as CodTipoMetodologiaLGD, max(TMLGD.NbrTipoMetodologia) as NbrTipoMetodologiaLGD, max(TMLGD.NbrVariableMotor) as NbrVariableMotorLGD,"
						+ " max(TMLGDWO.CodTipoMetodologia) as CodTipoMetodologiaLGDWO, max(TMLGDWO.NbrTipoMetodologia) as NbrTipoMetodologiaLGDWO, max(TMLGDWO.NbrVariableMotor) as NbrVariableMotorLGDWO,"
						+ " max(TMLGDBE.CodTipoMetodologia) as CodTipoMetodologiaLGDBE, max(TMLGDBE.NbrTipoMetodologia) as NbrTipoMetodologiaLGDBE, max(TMLGDBE.NbrVariableMotor) as NbrVariableMotorLGDBE,"
						+ " max(TMEAD.CodTipoMetodologia) as CodTipoMetodologiaEAD, max(TMEAD.NbrTipoMetodologia) as NbrTipoMetodologiaEAD, max(TMEAD.NbrVariableMotor) as NbrVariableMotorEAD,"
						+ " max(C.CodCartera) as CodCartera," + " max(C.NbrCartera) as NbrCartera,"
						+ " max(C.NbrVariableMotor) as NbrVariableMotorCartera, max(C.NumOrden) as NumOrden, "
						+ " max(C.FlgPd6Meses) as FlgPd6Meses, max(TA.CodTipoAmbito) as CodTipoAmbito, max(TA.NbrTipoAmbito) as NbrTipoAmbito, "
						+ " max(TM.CodTipoMagnitud) as CodTipoMagnitud, max(TM.NbrTipoMagnitud) as NbrTipoMagnitud,"
						+ " max(TC.CodTipoCartera) as CodTipoCartera, max(TC.NbrTipoCartera) as NbrTipoCartera, max(TC.NbrVariableMotor) as NbrVariableMotorTipoCartera,"
						+ " max(E.CodEntidad) as CodEntidad, max(E.NbrEntidad) as NbrEntidad, max(E.NbrVariableMotor) as NbrVariableMotorEntidad, "
						+ " max(E_REP.CodEjecucion) as CodEjecucionRepricing, max(E_REP.CodMes) as CodMesRepricing, "
						+ " max(PP_REP.CodPrimerPaso) as CodPrimerPasoRepricing,  max(PP_REP.NbrPrimerPaso) as NbrPrimerPasoRepricing, "
						+ " max(PP_REP.DesPrimerPaso) as DesPrimerPasoRepricing " + " from HE_Ejecucion E "
						+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion"
						+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
						+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
						+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
						+ " left join ME_MotorCalculo MOC on MC.CodMotor = MOC.CodMotor"
						+ " left join ME_TipoFlujo TF on MC.CodTipoFlujo = TF.CodTipoFlujo "
						+ " left join ME_TipoObtencionResultados TOR on MC.CodTipoObtenResult = TOR.CodTipoObtenResult "
						+ " left join ME_TipoPeriodicidad TP on MC.CodTipoPeriodicidad = TP.CodTipoPeriodicidad "
						+ " left join ME_TipoMetodologia TMPD on MC.CodTipoMetodologiaPD = TMPD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMEAD on MC.CodTipoMetodologiaEAD = TMEAD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGD on MC.CodTipoMetodologiaLGD = TMLGD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGDWO on MC.CodTipoMetodologiaLGDWO = TMLGDWO.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGDBE on MC.CodTipoMetodologiaLGDBE = TMLGDBE.CodTipoMetodologia "
						+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
						+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
						+ " left join ME_TipoMagnitud TM on C.CodTipoMagnitud = TM.CodTipoMagnitud "
						+ " left join ME_TipoCartera TC on C.CodTipoCartera = TC.CodTipoCartera "
						+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad "
						+ " left join HE_Ejecucion E_REP on E.CodEjecucionRepricing = E_REP.CodEjecucion "
						+ " left join HE_PrimerPaso PP_REP on E_REP.CodPrimerPaso = PP_REP.CodPrimerPaso "
						+ " where E.CodTipoEjecucion in (1,7)" + " and E.TipEstadoValidacion = 4 " // Oficial
						+ " group by E.codmes, JEV.CodJuegoEscenarios" + " order by E.codmes desc";

			} else if (fase.equals(1) && this.obtenerCodmesActual(conn, fase, id_tipo_ambito, false) != null) { // Fase
																												// 1
																												// (incluye
																												// actual)
				sql = "select E.CodMes, JEV.CodJuegoEscenarios, "
						+ " min(E.TipEstadoEjecucion) as EstadoEjecucionMin, avg(E.NumEjecucionAvance) as NumEjecucionAvanceProm, "
						+ " max(E.FecEjecucion) as FecEjecucion,"
						+ " min(E.TipEstadoValidacion) as EstadoValidacionMin, "
						+ " max(JEV.NbrJuegoEscenarios) as NbrJuegoEscenarios, "
						+ " max(JEV.DesJuegoEscenarios) as DesJuegoEscenarios, "
						+ " max(CV.CodCarteraVersion) as CodCarteraVersion," + " max(CV.NbrVersion) as NbrVersion, "
						+ " max(MC.CodMetodologia) as CodMetodologia," + " max(MC.NbrMetodologia) as NbrMetodologia, "
						+ " max(MC.NbrCodigoEstructura) as NbrCodigoEstructura, max(MC.NbrFuncPlumbr) as NbrFuncPlumbr, "
						+ " max(MC.NumPuertoPlumbr) as NumPuertoPlumbr, max(MC.NumBuckets) as NumBuckets,"
						+ " max(MC.NumLifeTime) as NumLifeTime, max(MC.NumMaxPlazo) as NumMaxPlazo, max(MC.FlgLgdUnica) as FlgLgdUnica,"
						+ " max(MOC.CodMotor) as CodMotor, max(MOC.NbrMotor) as NbrMotor, "
						+ " max(TF.CodTipoFlujo) as CodTipoFlujo, max(TF.NbrTipoFlujo) as NbrTipoFlujo,"
						+ " max(TOR.CodTipoObtenResult) as CodTipoObtenResult, max(TOR.NbrTipoObtenResult) as NbrTipoObtenResult,"
						+ " max(TP.CodTipoPeriodicidad) as CodTipoPeriodicidad, max(TP.NbrTipoPeriodicidad) as NbrTipoPeriodicidad, max(TP.NumValorPeriodicidad) as NumValorPeriodicidad,"
						+ " max(TMPD.CodTipoMetodologia) as CodTipoMetodologiaPD, max(TMPD.NbrTipoMetodologia) as NbrTipoMetodologiaPD, max(TMPD.NbrVariableMotor) as NbrVariableMotorPD,"
						+ " max(TMLGD.CodTipoMetodologia) as CodTipoMetodologiaLGD, max(TMLGD.NbrTipoMetodologia) as NbrTipoMetodologiaLGD, max(TMLGD.NbrVariableMotor) as NbrVariableMotorLGD,"
						+ " max(TMLGDWO.CodTipoMetodologia) as CodTipoMetodologiaLGDWO, max(TMLGDWO.NbrTipoMetodologia) as NbrTipoMetodologiaLGDWO, max(TMLGDWO.NbrVariableMotor) as NbrVariableMotorLGDWO,"
						+ " max(TMLGDBE.CodTipoMetodologia) as CodTipoMetodologiaLGDBE, max(TMLGDBE.NbrTipoMetodologia) as NbrTipoMetodologiaLGDBE, max(TMLGDBE.NbrVariableMotor) as NbrVariableMotorLGDBE,"
						+ " max(TMEAD.CodTipoMetodologia) as CodTipoMetodologiaEAD, max(TMEAD.NbrTipoMetodologia) as NbrTipoMetodologiaEAD, max(TMEAD.NbrVariableMotor) as NbrVariableMotorEAD,"
						+ " max(C.CodCartera) as CodCartera," + " max(C.NbrCartera) as NbrCartera,"
						+ " max(C.NbrVariableMotor) as NbrVariableMotorCartera, max(C.NumOrden) as NumOrden,"
						+ " max(C.FlgPd6Meses) as FlgPd6Meses, max(TA.CodTipoAmbito) as CodTipoAmbito, max(TA.NbrTipoAmbito) as NbrTipoAmbito, "
						+ " max(TM.CodTipoMagnitud) as CodTipoMagnitud, max(TM.NbrTipoMagnitud) as NbrTipoMagnitud,"
						+ " max(TC.CodTipoCartera) as CodTipoCartera, max(TC.NbrTipoCartera) as NbrTipoCartera, max(TC.NbrVariableMotor) as NbrVariableMotorTipoCartera,"
						+ " max(E.CodEntidad) as CodEntidad, max(E.NbrEntidad) as NbrEntidad, max(E.NbrVariableMotor) as NbrVariableMotorEntidad, "
						+ " max(E_REP.CodEjecucion) as CodEjecucionRepricing, max(E_REP.CodMes) as CodMesRepricing, "
						+ " max(PP_REP.CodPrimerPaso) as CodPrimerPasoRepricing,  max(PP_REP.NbrPrimerPaso) as NbrPrimerPasoRepricing, "
						+ " max(PP_REP.DesPrimerPaso) as DesPrimerPasoRepricing " + " from HE_Ejecucion E "
						+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion"
						+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
						+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
						+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
						+ " left join ME_MotorCalculo MOC on MC.CodMotor = MOC.CodMotor"
						+ " left join ME_TipoFlujo TF on MC.CodTipoFlujo = TF.CodTipoFlujo "
						+ " left join ME_TipoObtencionResultados TOR on MC.CodTipoObtenResult = TOR.CodTipoObtenResult "
						+ " left join ME_TipoPeriodicidad TP on MC.CodTipoPeriodicidad = TP.CodTipoPeriodicidad "
						+ " left join ME_TipoMetodologia TMPD on MC.CodTipoMetodologiaPD = TMPD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMEAD on MC.CodTipoMetodologiaEAD = TMEAD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGD on MC.CodTipoMetodologiaLGD = TMLGD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGDWO on MC.CodTipoMetodologiaLGDWO = TMLGDWO.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGDBE on MC.CodTipoMetodologiaLGDBE = TMLGDBE.CodTipoMetodologia "
						+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
						+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
						+ " left join ME_TipoMagnitud TM on C.CodTipoMagnitud = TM.CodTipoMagnitud "
						+ " left join ME_TipoCartera TC on C.CodTipoCartera = TC.CodTipoCartera "
						+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad "
						+ " left join HE_Ejecucion E_REP on E.CodEjecucionRepricing = E_REP.CodEjecucion "
						+ " left join HE_PrimerPaso PP_REP on E_REP.CodPrimerPaso = PP_REP.CodPrimerPaso "
						+ " where E.CodTipoEjecucion in (1,7)" + " and E.TipEstadoValidacion = 4 " // Oficial
						+ " or (E.codmes = " + this.obtenerCodmesActual(conn, 1, id_tipo_ambito, true)
						+ " and E.CodTipoEjecucion in (1,7) " + " and JEV.CodJuegoEscenarios in ("
						+ "select JEV1.CodJuegoEscenarios from HE_Ejecucion EJE1 "
						+ " left join HE_EscenarioVersion EV1 on EJE1.CodEscenarioVersion = EV1.CodEscenarioVersion "
						+ " left join HE_JuegoEscenariosVersion JEV1 on EV1.CodJuegoEscenarios = JEV1.CodJuegoEscenarios "
						+ " left join HE_CarteraVersion CV1 on JEV1.CodCarteraVersion = CV1.CodCarteraVersion "
						+ " left join HE_MetodologiaCartera MC1 on CV1.CodMetodologia = MC1.CodMetodologia "
						+ " left join HE_Cartera C1 on MC1.CodCartera = C1.CodCartera "
						+ " where EJE1.CodTipoEjecucion in (1,7) "
						+ " and EJE1.TipEstadoEjecucion in (2,4) and EJE1.TipEstadoAjustes in (0,3) "
						+ " and EJE1.TipEstadoValidacion = 4 and C1.CodCartera = C.CodCartera " + " and not exists("
						+ " select * from HE_Ejecucion EJE2 "
						+ " left join HE_EscenarioVersion EV2 on EJE2.CodEscenarioVersion = EV2.CodEscenarioVersion "
						+ " left join HE_JuegoEscenariosVersion JEV2 on EV2.CodJuegoEscenarios = JEV2.CodJuegoEscenarios "
						+ " left join HE_CarteraVersion CV2 on JEV2.CodCarteraVersion = CV2.CodCarteraVersion "
						+ " left join HE_MetodologiaCartera MC2 on CV2.CodMetodologia = MC2.CodMetodologia "
						+ " left join HE_Cartera C2 on MC2.CodCartera = C2.CodCartera "
						+ " where EJE2.CodMes > EJE1.CodMes " + " and EJE2.CodTipoEjecucion = EJE1.CodTipoEjecucion "
						+ " and EJE2.TipEstadoEjecucion in (2,4) and EJE2.TipEstadoAjustes in (0,3) "
						+ " and EJE2.TipEstadoValidacion = 4 and C2.CodCartera = C.CodCartera))) "
						+ " group by E.codmes, JEV.CodJuegoEscenarios" + " order by E.codmes desc";

				/*
				 * +
				 * " (select HE_JuegoEscenariosVersion.CodJuegoEscenarios from HE_Ejecucion EJE1 "
				 * +
				 * " left join HE_EscenarioVersion on EJE1.CodEscenarioVersion = HE_EscenarioVersion.CodEscenarioVersion"
				 * +
				 * " left join HE_JuegoEscenariosVersion on HE_EscenarioVersion.CodJuegoEscenarios = HE_JuegoEscenariosVersion.CodJuegoEscenarios"
				 * +
				 * " left join HE_CarteraVersion on HE_JuegoEscenariosVersion.CodCarteraVersion = HE_CarteraVersion.CodCarteraVersion"
				 * +
				 * " left join HE_MetodologiaCartera on HE_CarteraVersion.CodMetodologia = HE_MetodologiaCartera.CodMetodologia "
				 * +
				 * " left join HE_Cartera on HE_MetodologiaCartera.CodCartera = HE_Cartera.CodCartera"
				 * + " where EJE1.CodTipoEjecucion in (1,7)" +
				 * " and EJE1.TipEstadoEjecucion = 2 and EJE1.TipEstadoAjustes in (0,3) and EJE1.TipEstadoValidacion = 4"
				 * + " and HE_Cartera.CodCartera = C.CodCartera " +
				 * " and not exists(select * from HE_Ejecucion EJE2 where EJE2.CodMes > EJE1.CodMes"
				 * + " and EJE2.TipEstadoEjecucion = 2 and EJE2.TipEstadoAjustes in (0,3)" +
				 * " and EJE2.TipEstadoValidacion = 4 and EJE2.CodTipoEjecucion = EJE1.CodTipoEjecucion)))"
				 */

			} else if (fase.equals(2) && id_tipo_ambito.equals(1)) { // Fase 2 creditos (siempre dependiente)
				sql = "select E.CodMes, JEV.CodJuegoEscenarios, "
						+ " min(E.TipEstadoEjecucion) as EstadoEjecucionMin, avg(E.NumEjecucionAvance) as NumEjecucionAvanceProm, "
						+ " max(E.FecEjecucion) as FecEjecucion,"
						+ " min(E.TipEstadoValidacion) as EstadoValidacionMin, "
						+ " max(JEV.NbrJuegoEscenarios) as NbrJuegoEscenarios, "
						+ " max(JEV.DesJuegoEscenarios) as DesJuegoEscenarios, "
						+ " max(CV.CodCarteraVersion) as CodCarteraVersion," + " max(CV.NbrVersion) as NbrVersion, "
						+ " max(MC.CodMetodologia) as CodMetodologia," + " max(MC.NbrMetodologia) as NbrMetodologia, "
						+ " max(MC.NbrCodigoEstructura) as NbrCodigoEstructura, max(MC.NbrFuncPlumbr) as NbrFuncPlumbr, "
						+ " max(MC.NumPuertoPlumbr) as NumPuertoPlumbr, max(MC.NumBuckets) as NumBuckets,"
						+ " max(MC.NumLifeTime) as NumLifeTime, max(MC.NumMaxPlazo) as NumMaxPlazo, max(MC.FlgLgdUnica) as FlgLgdUnica,"
						+ " max(MOC.CodMotor) as CodMotor, max(MOC.NbrMotor) as NbrMotor, "
						+ " max(TF.CodTipoFlujo) as CodTipoFlujo, max(TF.NbrTipoFlujo) as NbrTipoFlujo,"
						+ " max(TOR.CodTipoObtenResult) as CodTipoObtenResult, max(TOR.NbrTipoObtenResult) as NbrTipoObtenResult,"
						+ " max(TP.CodTipoPeriodicidad) as CodTipoPeriodicidad, max(TP.NbrTipoPeriodicidad) as NbrTipoPeriodicidad, max(TP.NumValorPeriodicidad) as NumValorPeriodicidad,"
						+ " max(TMPD.CodTipoMetodologia) as CodTipoMetodologiaPD, max(TMPD.NbrTipoMetodologia) as NbrTipoMetodologiaPD, max(TMPD.NbrVariableMotor) as NbrVariableMotorPD,"
						+ " max(TMLGD.CodTipoMetodologia) as CodTipoMetodologiaLGD, max(TMLGD.NbrTipoMetodologia) as NbrTipoMetodologiaLGD, max(TMLGD.NbrVariableMotor) as NbrVariableMotorLGD,"
						+ " max(TMLGDWO.CodTipoMetodologia) as CodTipoMetodologiaLGDWO, max(TMLGDWO.NbrTipoMetodologia) as NbrTipoMetodologiaLGDWO, max(TMLGDWO.NbrVariableMotor) as NbrVariableMotorLGDWO,"
						+ " max(TMLGDBE.CodTipoMetodologia) as CodTipoMetodologiaLGDBE, max(TMLGDBE.NbrTipoMetodologia) as NbrTipoMetodologiaLGDBE, max(TMLGDBE.NbrVariableMotor) as NbrVariableMotorLGDBE,"
						+ " max(TMEAD.CodTipoMetodologia) as CodTipoMetodologiaEAD, max(TMEAD.NbrTipoMetodologia) as NbrTipoMetodologiaEAD, max(TMEAD.NbrVariableMotor) as NbrVariableMotorEAD,"
						+ " max(C.CodCartera) as CodCartera," + " max(C.NbrCartera) as NbrCartera,"
						+ " max(C.NbrVariableMotor) as NbrVariableMotorCartera, max(C.NumOrden) as NumOrden, "
						+ " max(C.FlgPd6Meses) as FlgPd6Meses, max(TA.CodTipoAmbito) as CodTipoAmbito, max(TA.NbrTipoAmbito) as NbrTipoAmbito, "
						+ " max(TM.CodTipoMagnitud) as CodTipoMagnitud, max(TM.NbrTipoMagnitud) as NbrTipoMagnitud,"
						+ " max(TC.CodTipoCartera) as CodTipoCartera, max(TC.NbrTipoCartera) as NbrTipoCartera, max(TC.NbrVariableMotor) as NbrVariableMotorTipoCartera,"
						+ " max(E.CodEntidad) as CodEntidad, max(E.NbrEntidad) as NbrEntidad, max(E.NbrVariableMotor) as NbrVariableMotorEntidad, "
						+ " max(E_REP.CodEjecucion) as CodEjecucionRepricing, max(E_REP.CodMes) as CodMesRepricing, "
						+ " max(PP_REP.CodPrimerPaso) as CodPrimerPasoRepricing,  max(PP_REP.NbrPrimerPaso) as NbrPrimerPasoRepricing, "
						+ " max(PP_REP.DesPrimerPaso) as DesPrimerPasoRepricing " + " from HE_Ejecucion E "
						+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion"
						+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
						+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
						+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
						+ " left join ME_MotorCalculo MOC on MC.CodMotor = MOC.CodMotor"
						+ " left join ME_TipoFlujo TF on MC.CodTipoFlujo = TF.CodTipoFlujo "
						+ " left join ME_TipoObtencionResultados TOR on MC.CodTipoObtenResult = TOR.CodTipoObtenResult "
						+ " left join ME_TipoPeriodicidad TP on MC.CodTipoPeriodicidad = TP.CodTipoPeriodicidad "
						+ " left join ME_TipoMetodologia TMPD on MC.CodTipoMetodologiaPD = TMPD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMEAD on MC.CodTipoMetodologiaEAD = TMEAD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGD on MC.CodTipoMetodologiaLGD = TMLGD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGDWO on MC.CodTipoMetodologiaLGDWO = TMLGDWO.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGDBE on MC.CodTipoMetodologiaLGDBE = TMLGDBE.CodTipoMetodologia "
						+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
						+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
						+ " left join ME_TipoMagnitud TM on C.CodTipoMagnitud = TM.CodTipoMagnitud "
						+ " left join ME_TipoCartera TC on C.CodTipoCartera = TC.CodTipoCartera "
						+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad "
						+ " left join HE_Ejecucion E_REP on E.CodEjecucionRepricing = E_REP.CodEjecucion "
						+ " left join HE_PrimerPaso PP_REP on E_REP.CodPrimerPaso = PP_REP.CodPrimerPaso "
						+ " where E.CodTipoEjecucion in (2,8)" // Mostramos todos
						+ " group by E.codmes, JEV.CodJuegoEscenarios" + " order by E.codmes desc";

			} else if (fase.equals(2) && id_tipo_ambito.equals(2)
					&& this.obtenerCodmesActual(conn, fase, id_tipo_ambito, false) == null) { // Fase 2 inversiones
																								// (independiente, sin
																								// actual)
				sql = "select E.CodMes, JEV.CodJuegoEscenarios, "
						+ " min(E.TipEstadoEjecucion) as EstadoEjecucionMin, avg(E.NumEjecucionAvance) as NumEjecucionAvanceProm, "
						+ " max(E.FecEjecucion) as FecEjecucion,"
						+ " min(E.TipEstadoValidacion) as EstadoValidacionMin, "
						+ " max(JEV.NbrJuegoEscenarios) as NbrJuegoEscenarios, "
						+ " max(JEV.DesJuegoEscenarios) as DesJuegoEscenarios, "
						+ " max(CV.CodCarteraVersion) as CodCarteraVersion," + " max(CV.NbrVersion) as NbrVersion, "
						+ " max(MC.CodMetodologia) as CodMetodologia," + " max(MC.NbrMetodologia) as NbrMetodologia, "
						+ " max(MC.NbrCodigoEstructura) as NbrCodigoEstructura, max(MC.NbrFuncPlumbr) as NbrFuncPlumbr, "
						+ " max(MC.NumPuertoPlumbr) as NumPuertoPlumbr, max(MC.NumBuckets) as NumBuckets,"
						+ " max(MC.NumLifeTime) as NumLifeTime, max(MC.NumMaxPlazo) as NumMaxPlazo, max(MC.FlgLgdUnica) as FlgLgdUnica,"
						+ " max(MOC.CodMotor) as CodMotor, max(MOC.NbrMotor) as NbrMotor, "
						+ " max(TF.CodTipoFlujo) as CodTipoFlujo, max(TF.NbrTipoFlujo) as NbrTipoFlujo,"
						+ " max(TOR.CodTipoObtenResult) as CodTipoObtenResult, max(TOR.NbrTipoObtenResult) as NbrTipoObtenResult,"
						+ " max(TP.CodTipoPeriodicidad) as CodTipoPeriodicidad, max(TP.NbrTipoPeriodicidad) as NbrTipoPeriodicidad, max(TP.NumValorPeriodicidad) as NumValorPeriodicidad,"
						+ " max(TMPD.CodTipoMetodologia) as CodTipoMetodologiaPD, max(TMPD.NbrTipoMetodologia) as NbrTipoMetodologiaPD, max(TMPD.NbrVariableMotor) as NbrVariableMotorPD,"
						+ " max(TMLGD.CodTipoMetodologia) as CodTipoMetodologiaLGD, max(TMLGD.NbrTipoMetodologia) as NbrTipoMetodologiaLGD, max(TMLGD.NbrVariableMotor) as NbrVariableMotorLGD,"
						+ " max(TMLGDWO.CodTipoMetodologia) as CodTipoMetodologiaLGDWO, max(TMLGDWO.NbrTipoMetodologia) as NbrTipoMetodologiaLGDWO, max(TMLGDWO.NbrVariableMotor) as NbrVariableMotorLGDWO,"
						+ " max(TMLGDBE.CodTipoMetodologia) as CodTipoMetodologiaLGDBE, max(TMLGDBE.NbrTipoMetodologia) as NbrTipoMetodologiaLGDBE, max(TMLGDBE.NbrVariableMotor) as NbrVariableMotorLGDBE,"
						+ " max(TMEAD.CodTipoMetodologia) as CodTipoMetodologiaEAD, max(TMEAD.NbrTipoMetodologia) as NbrTipoMetodologiaEAD, max(TMEAD.NbrVariableMotor) as NbrVariableMotorEAD,"
						+ " max(C.CodCartera) as CodCartera," + " max(C.NbrCartera) as NbrCartera,"
						+ " max(C.NbrVariableMotor) as NbrVariableMotorCartera, max(C.NumOrden) as NumOrden, "
						+ " max(C.FlgPd6Meses) as FlgPd6Meses, max(TA.CodTipoAmbito) as CodTipoAmbito, max(TA.NbrTipoAmbito) as NbrTipoAmbito, "
						+ " max(TM.CodTipoMagnitud) as CodTipoMagnitud, max(TM.NbrTipoMagnitud) as NbrTipoMagnitud,"
						+ " max(TC.CodTipoCartera) as CodTipoCartera, max(TC.NbrTipoCartera) as NbrTipoCartera, max(TC.NbrVariableMotor) as NbrVariableMotorTipoCartera,"
						+ " max(E.CodEntidad) as CodEntidad, max(E.NbrEntidad) as NbrEntidad, max(E.NbrVariableMotor) as NbrVariableMotorEntidad, "
						+ " max(E_REP.CodEjecucion) as CodEjecucionRepricing, max(E_REP.CodMes) as CodMesRepricing, "
						+ " max(PP_REP.CodPrimerPaso) as CodPrimerPasoRepricing,  max(PP_REP.NbrPrimerPaso) as NbrPrimerPasoRepricing, "
						+ " max(PP_REP.DesPrimerPaso) as DesPrimerPasoRepricing " + " from HE_Ejecucion E "
						+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion"
						+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
						+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
						+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
						+ " left join ME_MotorCalculo MOC on MC.CodMotor = MOC.CodMotor"
						+ " left join ME_TipoFlujo TF on MC.CodTipoFlujo = TF.CodTipoFlujo "
						+ " left join ME_TipoObtencionResultados TOR on MC.CodTipoObtenResult = TOR.CodTipoObtenResult "
						+ " left join ME_TipoPeriodicidad TP on MC.CodTipoPeriodicidad = TP.CodTipoPeriodicidad "
						+ " left join ME_TipoMetodologia TMPD on MC.CodTipoMetodologiaPD = TMPD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMEAD on MC.CodTipoMetodologiaEAD = TMEAD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGD on MC.CodTipoMetodologiaLGD = TMLGD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGDWO on MC.CodTipoMetodologiaLGDWO = TMLGDWO.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGDBE on MC.CodTipoMetodologiaLGDBE = TMLGDBE.CodTipoMetodologia "
						+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
						+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
						+ " left join ME_TipoMagnitud TM on C.CodTipoMagnitud = TM.CodTipoMagnitud "
						+ " left join ME_TipoCartera TC on C.CodTipoCartera = TC.CodTipoCartera "
						+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad "
						+ " left join HE_Ejecucion E_REP on E.CodEjecucionRepricing = E_REP.CodEjecucion "
						+ " left join HE_PrimerPaso PP_REP on E_REP.CodPrimerPaso = PP_REP.CodPrimerPaso "
						+ " where E.CodTipoEjecucion = 8" + " and E.TipEstadoValidacion = 4 " // Oficial
						+ " group by E.codmes, JEV.CodJuegoEscenarios" + " order by E.codmes desc";
			} else if (fase.equals(2) && id_tipo_ambito.equals(2)
					&& this.obtenerCodmesActual(conn, fase, id_tipo_ambito, false) != null) { // Fase 2 inversiones
																								// (independiente, con
																								// actual)
				sql = "select E.CodMes, JEV.CodJuegoEscenarios, "
						+ " min(E.TipEstadoEjecucion) as EstadoEjecucionMin, avg(E.NumEjecucionAvance) as NumEjecucionAvanceProm, "
						+ " max(E.FecEjecucion) as FecEjecucion,"
						+ " min(E.TipEstadoValidacion) as EstadoValidacionMin, "
						+ " max(JEV.NbrJuegoEscenarios) as NbrJuegoEscenarios, "
						+ " max(JEV.DesJuegoEscenarios) as DesJuegoEscenarios, "
						+ " max(CV.CodCarteraVersion) as CodCarteraVersion," + " max(CV.NbrVersion) as NbrVersion, "
						+ " max(MC.CodMetodologia) as CodMetodologia," + " max(MC.NbrMetodologia) as NbrMetodologia, "
						+ " max(MC.NbrCodigoEstructura) as NbrCodigoEstructura, max(MC.NbrFuncPlumbr) as NbrFuncPlumbr, "
						+ " max(MC.NumPuertoPlumbr) as NumPuertoPlumbr, max(MC.NumBuckets) as NumBuckets,"
						+ " max(MC.NumLifeTime) as NumLifeTime, max(MC.NumMaxPlazo) as NumMaxPlazo, max(MC.FlgLgdUnica) as FlgLgdUnica,"
						+ " max(MOC.CodMotor) as CodMotor, max(MOC.NbrMotor) as NbrMotor, "
						+ " max(TF.CodTipoFlujo) as CodTipoFlujo, max(TF.NbrTipoFlujo) as NbrTipoFlujo,"
						+ " max(TOR.CodTipoObtenResult) as CodTipoObtenResult, max(TOR.NbrTipoObtenResult) as NbrTipoObtenResult,"
						+ " max(TP.CodTipoPeriodicidad) as CodTipoPeriodicidad, max(TP.NbrTipoPeriodicidad) as NbrTipoPeriodicidad, max(TP.NumValorPeriodicidad) as NumValorPeriodicidad,"
						+ " max(TMPD.CodTipoMetodologia) as CodTipoMetodologiaPD, max(TMPD.NbrTipoMetodologia) as NbrTipoMetodologiaPD, max(TMPD.NbrVariableMotor) as NbrVariableMotorPD,"
						+ " max(TMLGD.CodTipoMetodologia) as CodTipoMetodologiaLGD, max(TMLGD.NbrTipoMetodologia) as NbrTipoMetodologiaLGD, max(TMLGD.NbrVariableMotor) as NbrVariableMotorLGD,"
						+ " max(TMLGDWO.CodTipoMetodologia) as CodTipoMetodologiaLGDWO, max(TMLGDWO.NbrTipoMetodologia) as NbrTipoMetodologiaLGDWO, max(TMLGDWO.NbrVariableMotor) as NbrVariableMotorLGDWO,"
						+ " max(TMLGDBE.CodTipoMetodologia) as CodTipoMetodologiaLGDBE, max(TMLGDBE.NbrTipoMetodologia) as NbrTipoMetodologiaLGDBE, max(TMLGDBE.NbrVariableMotor) as NbrVariableMotorLGDBE,"
						+ " max(TMEAD.CodTipoMetodologia) as CodTipoMetodologiaEAD, max(TMEAD.NbrTipoMetodologia) as NbrTipoMetodologiaEAD, max(TMEAD.NbrVariableMotor) as NbrVariableMotorEAD,"
						+ " max(C.CodCartera) as CodCartera," + " max(C.NbrCartera) as NbrCartera,"
						+ " max(C.NbrVariableMotor) as NbrVariableMotorCartera, max(C.NumOrden) as NumOrden,"
						+ " max(C.FlgPd6Meses) as FlgPd6Meses, max(TA.CodTipoAmbito) as CodTipoAmbito, max(TA.NbrTipoAmbito) as NbrTipoAmbito, "
						+ " max(TM.CodTipoMagnitud) as CodTipoMagnitud, max(TM.NbrTipoMagnitud) as NbrTipoMagnitud,"
						+ " max(TC.CodTipoCartera) as CodTipoCartera, max(TC.NbrTipoCartera) as NbrTipoCartera, max(TC.NbrVariableMotor) as NbrVariableMotorTipoCartera,"
						+ " max(E.CodEntidad) as CodEntidad, max(E.NbrEntidad) as NbrEntidad, max(E.NbrVariableMotor) as NbrVariableMotorEntidad, "
						+ " max(E_REP.CodEjecucion) as CodEjecucionRepricing, max(E_REP.CodMes) as CodMesRepricing, "
						+ " max(PP_REP.CodPrimerPaso) as CodPrimerPasoRepricing,  max(PP_REP.NbrPrimerPaso) as NbrPrimerPasoRepricing, "
						+ " max(PP_REP.DesPrimerPaso) as DesPrimerPasoRepricing " + " from HE_Ejecucion E "
						+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion"
						+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
						+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
						+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
						+ " left join ME_MotorCalculo MOC on MC.CodMotor = MOC.CodMotor"
						+ " left join ME_TipoFlujo TF on MC.CodTipoFlujo = TF.CodTipoFlujo "
						+ " left join ME_TipoObtencionResultados TOR on MC.CodTipoObtenResult = TOR.CodTipoObtenResult "
						+ " left join ME_TipoPeriodicidad TP on MC.CodTipoPeriodicidad = TP.CodTipoPeriodicidad "
						+ " left join ME_TipoMetodologia TMPD on MC.CodTipoMetodologiaPD = TMPD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMEAD on MC.CodTipoMetodologiaEAD = TMEAD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGD on MC.CodTipoMetodologiaLGD = TMLGD.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGDWO on MC.CodTipoMetodologiaLGDWO = TMLGDWO.CodTipoMetodologia "
						+ " left join ME_TipoMetodologia TMLGDBE on MC.CodTipoMetodologiaLGDBE = TMLGDBE.CodTipoMetodologia "
						+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
						+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
						+ " left join ME_TipoMagnitud TM on C.CodTipoMagnitud = TM.CodTipoMagnitud "
						+ " left join ME_TipoCartera TC on C.CodTipoCartera = TC.CodTipoCartera "
						+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad "
						+ " left join HE_Ejecucion E_REP on E.CodEjecucionRepricing = E_REP.CodEjecucion "
						+ " left join HE_PrimerPaso PP_REP on E_REP.CodPrimerPaso = PP_REP.CodPrimerPaso "
						+ " where E.CodTipoEjecucion = 8" + " and E.TipEstadoValidacion = 4 " // Oficial
						+ " or (E.codmes = " + this.obtenerCodmesActual(conn, 2, id_tipo_ambito, true)
						+ " and E.CodTipoEjecucion = 8 " + " and JEV.CodJuegoEscenarios in ("
						+ "select JEV1.CodJuegoEscenarios from HE_Ejecucion EJE1 "
						+ " left join HE_EscenarioVersion EV1 on EJE1.CodEscenarioVersion = EV1.CodEscenarioVersion "
						+ " left join HE_JuegoEscenariosVersion JEV1 on EV1.CodJuegoEscenarios = JEV1.CodJuegoEscenarios "
						+ " left join HE_CarteraVersion CV1 on JEV1.CodCarteraVersion = CV1.CodCarteraVersion "
						+ " left join HE_MetodologiaCartera MC1 on CV1.CodMetodologia = MC1.CodMetodologia "
						+ " left join HE_Cartera C1 on MC1.CodCartera = C1.CodCartera "
						+ " where EJE1.CodTipoEjecucion = 8 "
						+ " and EJE1.TipEstadoEjecucion in (2,4) and EJE1.TipEstadoAjustes in (0,3) "
						+ " and EJE1.TipEstadoValidacion = 4 and C1.CodCartera = C.CodCartera " + " and not exists("
						+ " select * from HE_Ejecucion EJE2 "
						+ " left join HE_EscenarioVersion EV2 on EJE2.CodEscenarioVersion = EV2.CodEscenarioVersion "
						+ " left join HE_JuegoEscenariosVersion JEV2 on EV2.CodJuegoEscenarios = JEV2.CodJuegoEscenarios "
						+ " left join HE_CarteraVersion CV2 on JEV2.CodCarteraVersion = CV2.CodCarteraVersion "
						+ " left join HE_MetodologiaCartera MC2 on CV2.CodMetodologia = MC2.CodMetodologia "
						+ " left join HE_Cartera C2 on MC2.CodCartera = C2.CodCartera "
						+ " where EJE2.CodMes > EJE1.CodMes " + " and EJE2.CodTipoEjecucion = EJE1.CodTipoEjecucion "
						+ " and EJE2.TipEstadoEjecucion in (2,4) and EJE2.TipEstadoAjustes in (0,3) "
						+ " and EJE2.TipEstadoValidacion = 4 and C2.CodCartera = C.CodCartera))) "
						+ " group by E.codmes, JEV.CodJuegoEscenarios" + " order by E.codmes desc";
			}

			/*
			 * Muestra todos en caso no exista oficial para ese mes
			 * 
			 * + " or (select max(TipEstadoValidacion) from HE_Ejecucion " // o si no existe
			 * oficial para esa fecha + " where CodMes = E.CodMes and CodTipoEjecucion = " +
			 * fase + ") < 4 "
			 */

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ejecucionAgregada = new EjecucionAgregadaPE();
				juegoEscenarios_version = new JuegoEscenarios_version();
				cartera_version = new Cartera_version();
				metodologia = new EstructuraMetodologica();
				cartera = new Cartera();
				entidad = new Entidad();

				ejecucionAgregada.setCodmes(rs.getInt("CodMes"));
				ejecucionAgregada.setEstado_ejecucion(rs.getInt("EstadoEjecucionMin"));
				ejecucionAgregada.setPorcentaje_avance(rs.getInt("NumEjecucionAvanceProm")); // Se utiliza en estado: en
																								// ejecucion
				ejecucionAgregada.setDatetime_ejecucion(rs.getLong("FecEjecucion"));
				ejecucionAgregada.setEstado_validacion(rs.getInt("EstadoValidacionMin")); // Se requiere para analizar
																							// consistencia

				juegoEscenarios_version.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenarios_version.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenarios_version.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));
				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));

				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
				metodologia.setCodigo_estructura(rs.getString("NbrCodigoEstructura"));
				metodologia.setNombre_funcPlumbr(rs.getString("NbrFuncPlumbr"));
				metodologia.setPuerto_plumbr(rs.getInt("NumPuertoPlumbr"));

				motor = new MotorCalculo();
				motor.setId_motor(rs.getInt("CodMotor"));
				motor.setNombre_motor(rs.getString("NbrMotor"));
				metodologia.setMotor(motor);

				tipoFlujo = new TipoFlujo();
				tipoFlujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipoFlujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));
				metodologia.setFlujo(tipoFlujo);

				tipoObtenResult = new TipoObtencionResultados();
				tipoObtenResult.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
				tipoObtenResult.setNombre_tipo_obtencion_resultados(rs.getString("NbrTipoObtenResult"));
				metodologia.setMetodo_resultados(tipoObtenResult);

				if (tipoFlujo.getNombre_tipo_flujo().toLowerCase().equals(("Calculo PE").toLowerCase())) {
					if (tipoObtenResult.getNombre_tipo_obtencion_resultados().toLowerCase()
							.equals(("Modelo interno").toLowerCase())) {
						metodologia.setNro_buckets(rs.getInt("NumBuckets"));
						metodologia.setLife_time(rs.getInt("NumLifeTime"));
						metodologia.setNro_maxPlazo(rs.getInt("NumMaxPlazo"));

						tipoPeriodicidad = new TipoPeriodicidad();
						tipoPeriodicidad.setId_tipo_periodicidad(rs.getInt("CodTipoPeriodicidad"));
						tipoPeriodicidad.setNombre_tipo_periodicidad(rs.getString("NbrTipoPeriodicidad"));
						tipoPeriodicidad.setValor_periodicidad(rs.getInt("NumValorPeriodicidad"));
						metodologia.setPeriodicidad(tipoPeriodicidad);

						tipoMetodologia = new TipoMetodologia();
						tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaPD"));
						tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaPD"));
						metodologia.setMetodologia_PD(tipoMetodologia);

						tipoMetodologia = new TipoMetodologia();
						tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaEAD"));
						tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaEAD"));
						metodologia.setMetodologia_EAD(tipoMetodologia);

						metodologia.setFlag_lgdUnica(rs.getBoolean("FlgLgdUnica"));

						if (metodologia.getFlag_lgdUnica()) {
							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGD"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGD"));
							metodologia.setMetodologia_LGD(tipoMetodologia);
						} else {
							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGDWO"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGDWO"));
							metodologia.setMetodologia_LGD_WO(tipoMetodologia);

							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGDBE"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGDBE"));
							metodologia.setMetodologia_LGD_BE(tipoMetodologia);
						}
					} else if (tipoObtenResult.getNombre_tipo_obtencion_resultados().toLowerCase()
							.equals(("Extrapolacion").toLowerCase())) {
						metodologia.setCarteras_extrapolacion(this
								.obtenerCarterasExtrapolacionporIdMetodologia(metodologia.getId_metodologia(), conn));
					}
				}

				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setNombre_variablemotor(rs.getString("NbrVariableMotorCartera"));
				cartera.setOrden(rs.getInt("NumOrden"));
				cartera.setFlag_pd6meses(rs.getBoolean("FlgPd6Meses"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				tipoMagnitud = new TipoMagnitud();
				tipoMagnitud.setId_tipo_magnitud(rs.getInt("CodTipoMagnitud"));
				tipoMagnitud.setNombre_tipo_magnitud(rs.getString("NbrTipoMagnitud"));
				cartera.setTipo_magnitud(tipoMagnitud);

				tipoCartera = new TipoCartera();
				tipoCartera.setId_tipo_cartera(rs.getInt("CodTipoCartera"));
				tipoCartera.setNombre_tipo_cartera(rs.getString("NbrTipoCartera"));
				tipoCartera.setNombre_variablemotor(rs.getString("NbrVariableMotorTipoCartera"));
				cartera.setTipo_cartera(tipoCartera);

				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				entidad.setNombre_variablemotor(rs.getString("NbrVariableMotorEntidad"));
				cartera.setEntidad(entidad);

				metodologia.setCartera(cartera);

				cartera_version.setMetodologia(metodologia);

				juegoEscenarios_version.setCartera_version(cartera_version);

				ejecucionAgregada.setJuego_escenarios_version(juegoEscenarios_version);

				// Repricing
				if (rs.getInt("CodEjecucionRepricing") != 0) {
					ejecucionRepricing = new EjecucionPrimerPaso();
					primerPasoRep = new PrimerPaso();

					ejecucionRepricing.setId_ejecucion(rs_ejecModMacro.getInt("CodEjecucionRepricing"));
					ejecucionRepricing.setCodmes(rs_ejecModMacro.getInt("CodMesRepricing"));

					primerPasoRep.setId_primerPaso(rs_ejecModMacro.getInt("CodPrimerPasoRepricing"));
					primerPasoRep.setNombre_primerPaso(rs_ejecModMacro.getString("NbrPrimerPasoRepricing"));
					primerPasoRep.setDescripcion_primerPaso(rs_ejecModMacro.getString("DesPrimerPasoRepricing"));
					ejecucionRepricing.setPrimerPaso(primerPasoRep);

					ejecucionRepricing.setMetodologia(
							ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia());

					ejecucionAgregada.setEjecucion_repricing(ejecucionRepricing);
				}

				// Ingresamos la ejecucion de modelo macro configurada
				sql_ejecModMacro = "select MMM.TipAjusteManual, E_MM.CodEjecucion as CodEjecucionModMacro, "
						+ "E_MM.CodMes as CodMesModMacro, PP.CodPrimerPaso, PP.NbrPrimerPaso, PP.DesPrimerPaso "
						+ "from HE_MapeoModMacro MMM "
						+ "left join HE_Ejecucion E_MM on MMM.CodEjecucionModMacro = E_MM.CodEjecucion "
						+ "left join HE_PrimerPaso PP on E_MM.CodPrimerPaso = PP.CodPrimerPaso "
						+ "left join HE_Ejecucion E_PE on MMM.CodEjecucionPE = E_PE.CodEjecucion "
						+ "left join HE_EscenarioVersion EV on E_PE.CodEscenarioVersion = EV.CodEscenarioVersion "
						+ "where ROWNUM=1 and E_PE.CodTipoEjecucion = " + fase + " and E_PE.CodMes = "
						+ ejecucionAgregada.getCodmes() + " and EV.CodJuegoEscenarios = "
						+ ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios();

				stmt_ejecModMacro = crearStatement(conn);
				rs_ejecModMacro = stmt_ejecModMacro.executeQuery(sql_ejecModMacro);

				ejecucionModMacro = new EjecucionPrimerPaso();
				primerPaso = new PrimerPaso();

				sinResultado = true;
				while (rs_ejecModMacro.next()) {
					sinResultado = false;

					ejecucionAgregada.setTipo_ajusteManual(rs_ejecModMacro.getInt("TipAjusteManual"));

					if (ejecucionAgregada.getTipo_ajusteManual().equals(0)) {
						ejecucionModMacro.setId_ejecucion(rs_ejecModMacro.getInt("CodEjecucionModMacro"));
						ejecucionModMacro.setCodmes(rs_ejecModMacro.getInt("CodMesModMacro"));

						primerPaso.setId_primerPaso(rs_ejecModMacro.getInt("CodPrimerPaso"));
						primerPaso.setNombre_primerPaso(rs_ejecModMacro.getString("NbrPrimerPaso"));
						primerPaso.setDescripcion_primerPaso(rs_ejecModMacro.getString("DesPrimerPaso"));
						ejecucionModMacro.setPrimerPaso(primerPaso);

						ejecucionModMacro.setMetodologia(
								ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia());
					}
				}
				cerrarResultSet(rs_ejecModMacro);
				cerrarStatement(stmt_ejecModMacro);

				if (sinResultado) {
					ejecucionAgregada.setTipo_ajusteManual(-1); // No requiere mod macro
				} else {
					if (ejecucionAgregada.getTipo_ajusteManual().equals(0)) {
						ejecucionAgregada.setEjecucion_modMacro(ejecucionModMacro);
					}
				}

				ejecucionesAgregada.add(ejecucionAgregada);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener ejecuciones agregadas: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarResultSet(rs_ejecModMacro);
			cerrarStatement(stmt);
			cerrarStatement(stmt_ejecModMacro);
			cerrarConexion(conn);
		}
		return ejecucionesAgregada;
	}

	public EjecucionAgregadaPE obtenerEjecucionesAgregadaPEPorFaseCodmesJuego(Integer fase, Integer codmes,
			Integer id_juego_escenarios) throws Exception {
		EjecucionAgregadaPE ejecucionAgregada = null;
		JuegoEscenarios_version juegoEscenarios_version = null;
		Cartera_version cartera_version = null;
		EstructuraMetodologica metodologia = null;
		MotorCalculo motor = null;
		TipoFlujo tipoFlujo = null;
		TipoObtencionResultados tipoObtenResult = null;
		TipoMetodologia tipoMetodologia = null;
		TipoPeriodicidad tipoPeriodicidad = null;
		Cartera cartera = null;
		TipoAmbito tipoAmbito = null;
		TipoMagnitud tipoMagnitud = null;
		TipoCartera tipoCartera = null;
		Entidad entidad = null;

		EjecucionPrimerPaso ejecucionModMacro = null;
		PrimerPaso primerPaso = null;
		Boolean sinResultado = null;

		EjecucionPrimerPaso ejecucionRepricing = null;
		PrimerPaso primerPasoRep = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		Statement stmt_ejecModMacro = null;
		ResultSet rs_ejecModMacro = null;

		String sql_ejecModMacro = null;

		String sql = "select E.CodMes, JEV.CodJuegoEscenarios, "
				+ " min(E.TipEstadoEjecucion) as EstadoEjecucionMin, avg(E.NumEjecucionAvance) as NumEjecucionAvanceProm, "
				+ " max(E.FecEjecucion) as FecEjecucion," + " max(JEV.NbrJuegoEscenarios) as NbrJuegoEscenarios, "
				+ " max(JEV.DesJuegoEscenarios) as DesJuegoEscenarios, "
				+ " max(CV.CodCarteraVersion) as CodCarteraVersion," + " max(CV.NbrVersion) as NbrVersion, "
				+ " max(MC.CodMetodologia) as CodMetodologia," + " max(MC.NbrMetodologia) as NbrMetodologia, "
				+ " max(MC.NbrCodigoEstructura) as NbrCodigoEstructura, max(MC.NbrFuncPlumbr) as NbrFuncPlumbr, "
				+ " max(MC.NumPuertoPlumbr) as NumPuertoPlumbr, max(MC.NumBuckets) as NumBuckets,"
				+ " max(MC.NumLifeTime) as NumLifeTime, max(MC.NumMaxPlazo) as NumMaxPlazo, max(MC.FlgLgdUnica) as FlgLgdUnica,"
				+ " max(MOC.CodMotor) as CodMotor, max(MOC.NbrMotor) as NbrMotor, "
				+ " max(TF.CodTipoFlujo) as CodTipoFlujo, max(TF.NbrTipoFlujo) as NbrTipoFlujo,"
				+ " max(TOR.CodTipoObtenResult) as CodTipoObtenResult, max(TOR.NbrTipoObtenResult) as NbrTipoObtenResult,"
				+ " max(TP.CodTipoPeriodicidad) as CodTipoPeriodicidad, max(TP.NbrTipoPeriodicidad) as NbrTipoPeriodicidad, max(TP.NumValorPeriodicidad) as NumValorPeriodicidad,"
				+ " max(TMPD.CodTipoMetodologia) as CodTipoMetodologiaPD, max(TMPD.NbrTipoMetodologia) as NbrTipoMetodologiaPD, max(TMPD.NbrVariableMotor) as NbrVariableMotorPD,"
				+ " max(TMLGD.CodTipoMetodologia) as CodTipoMetodologiaLGD, max(TMLGD.NbrTipoMetodologia) as NbrTipoMetodologiaLGD, max(TMLGD.NbrVariableMotor) as NbrVariableMotorLGD,"
				+ " max(TMLGDWO.CodTipoMetodologia) as CodTipoMetodologiaLGDWO, max(TMLGDWO.NbrTipoMetodologia) as NbrTipoMetodologiaLGDWO, max(TMLGDWO.NbrVariableMotor) as NbrVariableMotorLGDWO,"
				+ " max(TMLGDBE.CodTipoMetodologia) as CodTipoMetodologiaLGDBE, max(TMLGDBE.NbrTipoMetodologia) as NbrTipoMetodologiaLGDBE, max(TMLGDBE.NbrVariableMotor) as NbrVariableMotorLGDBE,"
				+ " max(TMEAD.CodTipoMetodologia) as CodTipoMetodologiaEAD, max(TMEAD.NbrTipoMetodologia) as NbrTipoMetodologiaEAD, max(TMEAD.NbrVariableMotor) as NbrVariableMotorEAD,"
				+ " max(C.CodCartera) as CodCartera," + " max(C.NbrCartera) as NbrCartera,"
				+ " max(C.NbrVariableMotor) as NbrVariableMotorCartera, max(C.NumOrden) as NumOrden, "
				+ " max(C.FlgPd6Meses) as FlgPd6Meses, max(TA.CodTipoAmbito) as CodTipoAmbito, max(TA.NbrTipoAmbito) as NbrTipoAmbito, "
				+ " max(TM.CodTipoMagnitud) as CodTipoMagnitud, max(TM.NbrTipoMagnitud) as NbrTipoMagnitud,"
				+ " max(TC.CodTipoCartera) as CodTipoCartera, max(TC.NbrTipoCartera) as NbrTipoCartera, max(TC.NbrVariableMotor) as NbrVariableMotorTipoCartera,"
				+ " max(E.CodEntidad) as CodEntidad, max(E.NbrEntidad) as NbrEntidad, max(E.NbrVariableMotor) as NbrVariableMotorEntidad, "
				+ " max(E_REP.CodEjecucion) as CodEjecucionRepricing, max(E_REP.CodMes) as CodMesRepricing, "
				+ " max(PP_REP.CodPrimerPaso) as CodPrimerPasoRepricing,  max(PP_REP.NbrPrimerPaso) as NbrPrimerPasoRepricing, "
				+ " max(PP_REP.DesPrimerPaso) as DesPrimerPasoRepricing " + " from HE_Ejecucion E "
				+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion"
				+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
				+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
				+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ " left join ME_MotorCalculo MOC on MC.CodMotor = MOC.CodMotor"
				+ " left join ME_TipoFlujo TF on MC.CodTipoFlujo = TF.CodTipoFlujo "
				+ " left join ME_TipoObtencionResultados TOR on MC.CodTipoObtenResult = TOR.CodTipoObtenResult "
				+ " left join ME_TipoPeriodicidad TP on MC.CodTipoPeriodicidad = TP.CodTipoPeriodicidad "
				+ " left join ME_TipoMetodologia TMPD on MC.CodTipoMetodologiaPD = TMPD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TMEAD on MC.CodTipoMetodologiaEAD = TMEAD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TMLGD on MC.CodTipoMetodologiaLGD = TMLGD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TMLGDWO on MC.CodTipoMetodologiaLGDWO = TMLGDWO.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TMLGDBE on MC.CodTipoMetodologiaLGDBE = TMLGDBE.CodTipoMetodologia "
				+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
				+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
				+ " left join ME_TipoMagnitud TM on C.CodTipoMagnitud = TM.CodTipoMagnitud "
				+ " left join ME_TipoCartera TC on C.CodTipoCartera = TC.CodTipoCartera "
				+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad "
				+ " left join HE_Ejecucion E_REP on E.CodEjecucionRepricing = E_REP.CodEjecucion "
				+ " left join HE_PrimerPaso PP_REP on E_REP.CodPrimerPaso = PP_REP.CodPrimerPaso "
				+ " where E.CodTipoEjecucion in " + ((fase.equals(1)) ? "(1,7)" : "(2,8)") // Incluye inversiones
				+ " and E.codmes = " + codmes + "and JEV.CodJuegoEscenarios = " + id_juego_escenarios
				+ " group by E.codmes, JEV.CodJuegoEscenarios" + " order by E.codmes desc";

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				ejecucionAgregada = new EjecucionAgregadaPE();
				juegoEscenarios_version = new JuegoEscenarios_version();
				cartera_version = new Cartera_version();
				metodologia = new EstructuraMetodologica();
				cartera = new Cartera();
				entidad = new Entidad();

				ejecucionAgregada.setCodmes(rs.getInt("CodMes"));
				ejecucionAgregada.setEstado_ejecucion(rs.getInt("EstadoEjecucionMin"));
				ejecucionAgregada.setPorcentaje_avance(rs.getInt("NumEjecucionAvanceProm")); // Se utiliza en estado: en
																								// ejecucion
				ejecucionAgregada.setDatetime_ejecucion(rs.getLong("FecEjecucion"));

				juegoEscenarios_version.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenarios_version.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenarios_version.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));
				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));

				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
				metodologia.setCodigo_estructura(rs.getString("NbrCodigoEstructura"));
				metodologia.setNombre_funcPlumbr(rs.getString("NbrFuncPlumbr"));
				metodologia.setPuerto_plumbr(rs.getInt("NumPuertoPlumbr"));

				motor = new MotorCalculo();
				motor.setId_motor(rs.getInt("CodMotor"));
				motor.setNombre_motor(rs.getString("NbrMotor"));
				metodologia.setMotor(motor);

				tipoFlujo = new TipoFlujo();
				tipoFlujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipoFlujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));
				metodologia.setFlujo(tipoFlujo);

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				if (tipoAmbito.getId_tipo_ambito().equals(1)) { // Creditos
					if (tipoFlujo.getNombre_tipo_flujo().toLowerCase().equals(("Calculo PE").toLowerCase())) {
						tipoObtenResult = new TipoObtencionResultados();
						tipoObtenResult.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
						tipoObtenResult.setNombre_tipo_obtencion_resultados(rs.getString("NbrTipoObtenResult"));
						metodologia.setMetodo_resultados(tipoObtenResult);

						if (tipoObtenResult.getNombre_tipo_obtencion_resultados().toLowerCase()
								.equals(("Modelo interno").toLowerCase())) {
							metodologia.setNro_buckets(rs.getInt("NumBuckets"));
							metodologia.setLife_time(rs.getInt("NumLifeTime"));
							metodologia.setNro_maxPlazo(rs.getInt("NumMaxPlazo"));

							tipoPeriodicidad = new TipoPeriodicidad();
							tipoPeriodicidad.setId_tipo_periodicidad(rs.getInt("CodTipoPeriodicidad"));
							tipoPeriodicidad.setNombre_tipo_periodicidad(rs.getString("NbrTipoPeriodicidad"));
							tipoPeriodicidad.setValor_periodicidad(rs.getInt("NumValorPeriodicidad"));
							metodologia.setPeriodicidad(tipoPeriodicidad);

							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaPD"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaPD"));
							metodologia.setMetodologia_PD(tipoMetodologia);

							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaEAD"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaEAD"));
							metodologia.setMetodologia_EAD(tipoMetodologia);

							metodologia.setFlag_lgdUnica(rs.getBoolean("FlgLgdUnica"));

							if (metodologia.getFlag_lgdUnica()) {
								tipoMetodologia = new TipoMetodologia();
								tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGD"));
								tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGD"));
								metodologia.setMetodologia_LGD(tipoMetodologia);
							} else {
								tipoMetodologia = new TipoMetodologia();
								tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGDWO"));
								tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGDWO"));
								metodologia.setMetodologia_LGD_WO(tipoMetodologia);

								tipoMetodologia = new TipoMetodologia();
								tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGDBE"));
								tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGDBE"));
								metodologia.setMetodologia_LGD_BE(tipoMetodologia);
							}
						} else if (tipoObtenResult.getNombre_tipo_obtencion_resultados().toLowerCase()
								.equals(("Extrapolacion").toLowerCase())) {
							metodologia.setCarteras_extrapolacion(this.obtenerCarterasExtrapolacionporIdMetodologia(
									metodologia.getId_metodologia(), conn));
						}
					}
				} else if (tipoAmbito.getId_tipo_ambito().equals(2)) { // Inversiones
					// -
				}

				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setNombre_variablemotor(rs.getString("NbrVariableMotorCartera"));
				cartera.setOrden(rs.getInt("NumOrden"));
				cartera.setFlag_pd6meses(rs.getBoolean("FlgPd6Meses"));

				if (tipoAmbito.getId_tipo_ambito().equals(1)) { // Solo para creditos
					tipoMagnitud = new TipoMagnitud();
					tipoMagnitud.setId_tipo_magnitud(rs.getInt("CodTipoMagnitud"));
					tipoMagnitud.setNombre_tipo_magnitud(rs.getString("NbrTipoMagnitud"));
					cartera.setTipo_magnitud(tipoMagnitud);

					tipoCartera = new TipoCartera();
					tipoCartera.setId_tipo_cartera(rs.getInt("CodTipoCartera"));
					tipoCartera.setNombre_tipo_cartera(rs.getString("NbrTipoCartera"));
					tipoCartera.setNombre_variablemotor(rs.getString("NbrVariableMotorTipoCartera"));
					cartera.setTipo_cartera(tipoCartera);
				}

				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				entidad.setNombre_variablemotor(rs.getString("NbrVariableMotorEntidad"));
				cartera.setEntidad(entidad);

				metodologia.setCartera(cartera);

				cartera_version.setMetodologia(metodologia);

				juegoEscenarios_version.setCartera_version(cartera_version);

				ejecucionAgregada.setJuego_escenarios_version(juegoEscenarios_version);

				// Repricing
				if (rs.getInt("CodEjecucionRepricing") != 0) {
					ejecucionRepricing = new EjecucionPrimerPaso();
					primerPasoRep = new PrimerPaso();

					ejecucionRepricing.setId_ejecucion(rs_ejecModMacro.getInt("CodEjecucionRepricing"));
					ejecucionRepricing.setCodmes(rs_ejecModMacro.getInt("CodMesRepricing"));

					primerPasoRep.setId_primerPaso(rs_ejecModMacro.getInt("CodPrimerPasoRepricing"));
					primerPasoRep.setNombre_primerPaso(rs_ejecModMacro.getString("NbrPrimerPasoRepricing"));
					primerPasoRep.setDescripcion_primerPaso(rs_ejecModMacro.getString("DesPrimerPasoRepricing"));
					ejecucionRepricing.setPrimerPaso(primerPasoRep);

					ejecucionRepricing.setMetodologia(
							ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia());

					ejecucionAgregada.setEjecucion_repricing(ejecucionRepricing);
				}

				// Ingresamos la ejecucion de modelo macro configurada
				sql_ejecModMacro = "select MMM.TipAjusteManual, E_MM.CodEjecucion as CodEjecucionModMacro, "
						+ "E_MM.CodMes as CodMesModMacro, PP.CodPrimerPaso, PP.NbrPrimerPaso, PP.DesPrimerPaso "
						+ "from HE_MapeoModMacro MMM "
						+ "left join HE_Ejecucion E_MM on MMM.CodEjecucionModMacro = E_MM.CodEjecucion "
						+ "left join HE_PrimerPaso PP on E_MM.CodPrimerPaso = PP.CodPrimerPaso "
						+ "left join HE_Ejecucion E_PE on MMM.CodEjecucionPE = E_PE.CodEjecucion "
						+ "left join HE_EscenarioVersion EV on E_PE.CodEscenarioVersion = EV.CodEscenarioVersion "
						+ "where ROWNUM=1 and E_PE.CodTipoEjecucion in " + ((fase.equals(1)) ? "(1,7)" : "(2,8)") // Incluye
																													// inversiones
						+ " and E_PE.CodMes = " + ejecucionAgregada.getCodmes() + " and EV.CodJuegoEscenarios = "
						+ ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios();

				stmt_ejecModMacro = crearStatement(conn);
				rs_ejecModMacro = stmt_ejecModMacro.executeQuery(sql_ejecModMacro);

				ejecucionModMacro = new EjecucionPrimerPaso();
				primerPaso = new PrimerPaso();

				sinResultado = true;
				while (rs_ejecModMacro.next()) {
					sinResultado = false;

					ejecucionAgregada.setTipo_ajusteManual(rs_ejecModMacro.getInt("TipAjusteManual"));

					if (ejecucionAgregada.getTipo_ajusteManual().equals(0)) {
						ejecucionModMacro.setId_ejecucion(rs_ejecModMacro.getInt("CodEjecucionModMacro"));
						ejecucionModMacro.setCodmes(rs_ejecModMacro.getInt("CodMesModMacro"));

						primerPaso.setId_primerPaso(rs_ejecModMacro.getInt("CodPrimerPaso"));
						primerPaso.setNombre_primerPaso(rs_ejecModMacro.getString("NbrPrimerPaso"));
						primerPaso.setDescripcion_primerPaso(rs_ejecModMacro.getString("DesPrimerPaso"));
						ejecucionModMacro.setPrimerPaso(primerPaso);

						ejecucionModMacro.setMetodologia(
								ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia());
					}
				}
				cerrarResultSet(rs_ejecModMacro);
				cerrarStatement(stmt_ejecModMacro);

				if (sinResultado) {
					ejecucionAgregada.setTipo_ajusteManual(-1); // No requiere mod macro
				} else {
					if (ejecucionAgregada.getTipo_ajusteManual().equals(0)) {
						ejecucionAgregada.setEjecucion_modMacro(ejecucionModMacro);
					}
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener una ejecucion agregada: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarResultSet(rs_ejecModMacro);
			cerrarStatement(stmt);
			cerrarStatement(stmt_ejecModMacro);
			cerrarConexion(conn);
		}
		return ejecucionAgregada;
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucion_fase1(Integer codmes, Integer id_juego_escenarios,
			Integer tipo_ajusteManual, Integer id_ejecucion_modMac) throws Exception {
		return this.obtenerMapeoCargasModMacEjecucion(codmes, id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMac, 1, false);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucion_fase2(Integer codmes, Integer id_juego_escenarios)
			throws Exception {
		return this.obtenerMapeoCargasModMacEjecucion(codmes, id_juego_escenarios, null, null, 2, false);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacSimulacion(Integer codmes, Integer fase,
			Integer id_juego_escenarios, Integer tipo_ajusteManual, Integer id_ejecucion_modMac) throws Exception {
		return this.obtenerMapeoCargasModMacEjecucion(codmes, id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMac, fase, true);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucion(Integer codmes, Integer id_juego_escenarios,
			Integer tipo_ajusteManual, Integer id_ejecucion_modMac, Integer fase, Boolean esSimulacion)
			throws Exception {
		MapeoCargasModMacEjec mapeo_cargas_modMac = new MapeoCargasModMacEjec();
		ArrayList<MapeoCargasModMacEscVer> mapeos_cargasModMac_esc = new ArrayList<MapeoCargasModMacEscVer>();
		MapeoCargasModMacEscVer mapeo_cargasModMac_esc = null;
		Escenario_version escenario_version = null;
		Escenario escenario = null;
		Boolean existe = null;

		Connection conn = null;
		Statement stmt_ejecuciones = null;
		ResultSet rs_ejecuciones = null;

		String sql_mapeo_escenarioVersion = null;

		String sql_ejecuciones = "select E.CodEjecucion, EV.CodEscenarioVersion, EV.NumPeso, "
				+ "ES.CodEscenario, ES.NbrEscenario, ES.FlgObligatorio, ES.NbrVariableMotor as escMotor "
				+ "from HE_Ejecucion E "
				+ "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
				+ "left join ME_Escenario ES on EV.CodEscenario = ES.CodEscenario " + "where E.CodMes = " + codmes
				+ " and E.CodTipoEjecucion in " + ((fase.equals(1)) ? "(1,7)" : "(2,8)") // Incluye inversiones
				+ " and EV.CodJuegoEscenarios = " + id_juego_escenarios;
		//LOGGER.info(sql_ejecuciones);
		String sql_mapeo_carteraVersion = "select ME.CodMapeoTabla,"
				+ " TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata," + " A.CodAprovisionamiento, A.NbrFichero,"
				+ " CF.CodCargaFichero, CF.NbrCarga, CF.NumOrdenInicial, CF.NumOrdenFinal, CF.DesComentarioCalidad, CF.NumVersionCarga,"
				+ " F.CodFecha, F.NumCodMes, " + " B.CodBloque, B.NbrBloque, B.NbrVariableMotor "
				+ " from HE_MapeoEjecucion ME"
				+ " left join HE_MapeoTablaBloque MTB on ME.CodMapeoTabla = MTB.CodMapeoTabla"
				+ " left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
				+ " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput"
				+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
				+ " left join HE_CargaFichero CF on CF.CodCargaFichero = ME.CodCargaFichero"
				+ " left join HE_Fecha F on CF.CodFecha = F.CodFecha" + " where ME.CodEjecucion = "
				+ "(select HE_Ejecucion.CodEjecucion from HE_Ejecucion left join HE_EscenarioVersion "
				+ " on HE_Ejecucion.CodEscenarioVersion = HE_EscenarioVersion.CodEscenarioVersion "
				+ " where HE_Ejecucion.CodMes = " + codmes + " and HE_EscenarioVersion.CodJuegoEscenarios = "
				+ id_juego_escenarios + " and CF.tipestadocarga <> 3  and HE_Ejecucion.CodTipoEjecucion in "
				+ ((fase.equals(1)) ? "(1,7)" : "(2,8)")
				// Incluye inversiones
				+ " and ROWNUM = 1)" // Limitamos a una ejecucion cualquiera
				+ " and MTB.CodEscenarioVersion is null and MTB.CodCarteraVersion = "
				+ " (select CodCarteraVersion from HE_JuegoEscenariosVersion where CodJuegoEscenarios = "
				+ id_juego_escenarios + ")";
		//LOGGER.info(sql_mapeo_carteraVersion);
		try {
			conn = obtenerConexion();

			// Completamos tablas mapeo a nivel de cartera version
			mapeo_cargas_modMac.setTablas_mapeo_carteraVersion(
					obtenerTablasMapeo(sql_mapeo_carteraVersion, fase, esSimulacion, conn, true, true));

			stmt_ejecuciones = crearStatement(conn);
			rs_ejecuciones = stmt_ejecuciones.executeQuery(sql_ejecuciones);

			while (rs_ejecuciones.next()) {
				mapeo_cargasModMac_esc = new MapeoCargasModMacEscVer();
				mapeo_cargasModMac_esc.setId_ejecucion(rs_ejecuciones.getInt("CodEjecucion"));

				escenario_version = new Escenario_version();
				escenario_version.setId_escenario_version(rs_ejecuciones.getInt("CodEscenarioVersion"));
				escenario_version.setPeso(rs_ejecuciones.getDouble("NumPeso"));

				escenario = new Escenario();
				escenario.setId_escenario(rs_ejecuciones.getInt("CodEscenario"));
				escenario.setNombre_escenario(rs_ejecuciones.getString("NbrEscenario"));
				escenario.setNombre_variable_motor(rs_ejecuciones.getString("escMotor"));
				escenario.setFlag_obligatorio(rs_ejecuciones.getBoolean("FlgObligatorio"));
				escenario_version.setEscenario(escenario);
				mapeo_cargasModMac_esc.setEscenario_version(escenario_version);

				// Solo para simulaciones
				if (esSimulacion) {
					mapeo_cargasModMac_esc.setFlag_ejecutar(true);
				}

				// Solo existen bloques por escenario en fase 1
				// if (fase.equals(1)) {
				sql_mapeo_escenarioVersion = "select ME.CodMapeoTabla,"
						+ " TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata,"
						+ " A.CodAprovisionamiento, A.NbrFichero,"
						+ " CF.CodCargaFichero, CF.NbrCarga, CF.NumOrdenInicial, CF.NumOrdenFinal, CF.DesComentarioCalidad, CF.NumVersionCarga,"
						+ " F.CodFecha, F.NumCodMes, " + " B.CodBloque, B.NbrBloque, B.NbrVariableMotor "
						+ " from HE_MapeoEjecucion ME"
						+ " left join HE_MapeoTablaBloque MTB on ME.CodMapeoTabla = MTB.CodMapeoTabla"
						+ " left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
						+ " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput"
						+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
						+ " left join HE_CargaFichero CF on CF.CodCargaFichero = ME.CodCargaFichero"
						+ " left join HE_Fecha F on CF.CodFecha = F.CodFecha" + " where ME.CodEjecucion = "
						+ mapeo_cargasModMac_esc.getId_ejecucion()
						+ " and CF.tipestadocarga <> 3 and MTB.CodEscenarioVersion = "
						+ mapeo_cargasModMac_esc.getEscenario_version().getId_escenario_version();

				// Completamos tablas mapeo a nivel de escenario version
				mapeo_cargasModMac_esc.setTablas_mapeo_escenarioVersion(
						obtenerTablasMapeo(sql_mapeo_escenarioVersion, fase, esSimulacion, conn, true, true));
				// }
				//LOGGER.info(sql_mapeo_carteraVersion);
				// Completamos mapeo de modelo macro (nivel escenario version), solo fase1 (y
				// requiere mod macro)
				if (fase.equals(1) && !tipo_ajusteManual.equals(-1)) {
					mapeo_cargasModMac_esc.setAjustesPdProy(
							this.obtenerAjustesPdProyPorEscenarioCreado(conn, mapeo_cargasModMac_esc.getId_ejecucion(),
									mapeo_cargasModMac_esc.getEscenario_version().getEscenario().getId_escenario()));
				}

				mapeos_cargasModMac_esc.add(mapeo_cargasModMac_esc);
			}

			// Agregamos mapeos de escenarios no ejecutados (sin cargas...)
			if (esSimulacion) {
				for (MapeoCargasModMacEscVer mapeoEscenarioSinCarga : this
						.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios(id_juego_escenarios, tipo_ajusteManual,
								id_ejecucion_modMac, fase, true)
						.getMapeosCargasModMacEsc()) {
					existe = false;
					for (MapeoCargasModMacEscVer mapeoEscenario : mapeos_cargasModMac_esc) {
						if (mapeoEscenarioSinCarga.getEscenario_version().getEscenario().getId_escenario()
								.equals(mapeoEscenario.getEscenario_version().getEscenario().getId_escenario())) {
							existe = true;
							break;
						}
					}

					if (!existe) {
						mapeos_cargasModMac_esc.add(mapeoEscenarioSinCarga);
					}
				}
			}

			mapeo_cargas_modMac.setMapeosCargasModMacEsc(mapeos_cargasModMac_esc);

		} catch (Exception ex) {
			LOGGER.error("Error al obtener mapeo de cargas y modelo macro de la ejecucion : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_ejecuciones);
			cerrarStatement(stmt_ejecuciones);
		}
		return mapeo_cargas_modMac;
	}

	private ArrayList<TablaMapeoEjecucion> obtenerTablasMapeo(String sql, Integer fase, Boolean esSimulacion,
			Connection conn, Boolean cargasSeleccionadas, Boolean conInfoBloque) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		Statement stmt_opcionesCarga = null;
		ResultSet rs_opcionesCarga = null;
		String sql_opcionesCarga = null;

		Statement stmt_opcionesCodmes = null;
		ResultSet rs_opcionesCodmes = null;
		String sql_opcionesCodmes = null;

		ArrayList<TablaMapeoEjecucion> tablas_mapeo = null;
		TablaMapeoEjecucion tabla_mapeo = null;
		TablaInput tabla_input = null;
		Bloque bloque = null;
		CargaFichero carga = null;
		Fecha fecha = null;
		ArrayList<CargaFichero> opciones_carga = null;
		CargaFichero opcion_carga = null;
		ArrayList<Integer> opciones_codmes = null;
		Integer opcion_codmes = null;

		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			tablas_mapeo = new ArrayList<TablaMapeoEjecucion>();
			while (rs.next()) {
				tabla_mapeo = new TablaMapeoEjecucion();
				tabla_mapeo.setId_mapeo_tabla(rs.getInt("CodMapeoTabla"));

				tabla_input = new TablaInput();
				tabla_input.setId_tablainput(rs.getInt("CodTablaInput"));
				tabla_input.setVersion_metadata(rs.getInt("NumVersionMetadata"));
				tabla_input.setNombre_metadata(rs.getString("NbrMetadata"));
				tabla_input.setId_aprovisionamiento(rs.getInt("CodAprovisionamiento"));
				tabla_input.setNombre_fichero(rs.getString("NbrFichero"));
				tabla_mapeo.setTabla_input(tabla_input);

				if (cargasSeleccionadas) {
					carga = new CargaFichero();
					carga.setId_cargafichero(rs.getInt("CodCargaFichero"));
					carga.setVersion_carga(rs.getInt("NumVersionCarga"));
					carga.setNombre_carga(rs.getString("NbrCarga"));
					carga.setOrden_inicial(rs.getInt("NumOrdenInicial"));
					carga.setOrden_final(rs.getInt("NumOrdenFinal"));
					carga.setComentario_calidad(rs.getString("DesComentarioCalidad"));

					fecha = new Fecha();
					fecha.setId_fecha(rs.getInt("CodFecha"));
					fecha.setCodmes(rs.getInt("NumCodMes"));
					carga.setFecha(fecha);
					tabla_mapeo.setCarga(carga);
				}

				// Agregamos info general de bloques
				if (conInfoBloque) {
					bloque = new Bloque();
					bloque.setId_bloque(rs.getInt("CodBloque"));
					bloque.setNombre_bloque(rs.getString("NbrBloque"));
					bloque.setVariable_motor(rs.getString("NbrVariableMotor"));
					tabla_mapeo.setBloque(bloque);
				}

				// Opciones de carga
				stmt_opcionesCarga = crearStatement(conn);

				if (esSimulacion) {
					sql_opcionesCarga = "select CF.CodCargaFichero, CF.NumVersionCarga,"
							+ " CF.NbrCarga, CF.DesComentarioCalidad, F.CodFecha, F.NumCodMes"
							+ " from HE_CargaFichero CF" + " left join HE_Fecha F on CF.CodFecha = F.CodFecha "
							+ " left join HE_TablaInput TI on CF.CodTablaInput = TI.CodTablaInput "
							+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento = A.CodAprovisionamiento "
							+ " where TI.CodTablaInput = " + tabla_input.getId_tablainput() + " and (F.NumFase = "
							+ fase + " or A.CodTipoAmbito = 2) and CF.TipEstadoCarga<>3"; // Para inversiones no
							//LOGGER.info(sql_opcionesCarga);								  // interesa fases de carga

				} else {
					sql_opcionesCarga = "select CF.CodCargaFichero, CF.NumVersionCarga,"
							+ " CF.NbrCarga, CF.DesComentarioCalidad, F.CodFecha, F.NumCodMes"
							+ " from HE_CargaFichero CF" + " left join HE_Fecha F on CF.CodFecha = F.CodFecha"
							+ " left join HE_TablaInput TI on CF.CodTablaInput = TI.CodTablaInput "
							+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento = A.CodAprovisionamiento "
							+ " where TI.CodTablaInput = " + tabla_input.getId_tablainput()
							+ " and CF.TipEstadoCarga = 2 " + " and (F.NumFase in "
							+ ((fase.equals(1)) ? "(1)" : "(1,2)") + " or A.CodTipoAmbito = 2)";
					// En fase 2 se pueden mapear cargas de ambas fases..
					// Para inversiones no interesa fases de carga
					//LOGGER.info(sql_opcionesCarga);		
				}

				rs_opcionesCarga = stmt_opcionesCarga.executeQuery(sql_opcionesCarga);

				opciones_carga = new ArrayList<CargaFichero>();
				while (rs_opcionesCarga.next()) {
					opcion_carga = new CargaFichero();
					opcion_carga.setId_cargafichero(rs_opcionesCarga.getInt("CodCargaFichero"));
					opcion_carga.setVersion_carga(rs_opcionesCarga.getInt("NumVersionCarga"));
					opcion_carga.setNombre_carga(rs_opcionesCarga.getString("NbrCarga"));
					opcion_carga.setComentario_calidad(rs_opcionesCarga.getString("DesComentarioCalidad"));

					fecha = new Fecha();
					fecha.setId_fecha(rs_opcionesCarga.getInt("CodFecha"));
					fecha.setCodmes(rs_opcionesCarga.getInt("NumCodMes"));
					opcion_carga.setFecha(fecha);

					opciones_carga.add(opcion_carga);
				}
				tabla_mapeo.setOpciones_carga(opciones_carga);
				cerrarResultSet(rs_opcionesCarga);
				cerrarStatement(stmt_opcionesCarga);

				// Opciones de codmes
				stmt_opcionesCodmes = crearStatement(conn);
				sql_opcionesCodmes = "select distinct F.NumCodMes" + " from HE_Fecha F"
						+ " left join HE_CargaFichero CF on F.CodFecha = CF.CodFecha" + " where CF.CodTablaInput = "
						+ tabla_input.getId_tablainput() + " order by F.NumCodMes asc";
				rs_opcionesCodmes = stmt_opcionesCodmes.executeQuery(sql_opcionesCodmes);

				opciones_codmes = new ArrayList<Integer>();
				while (rs_opcionesCodmes.next()) {
					opcion_codmes = rs_opcionesCodmes.getInt("NumCodMes");
					opciones_codmes.add(opcion_codmes);
				}
				tabla_mapeo.setOpciones_codmes(opciones_codmes);
				cerrarResultSet(rs_opcionesCodmes);
				cerrarStatement(stmt_opcionesCodmes);

				tablas_mapeo.add(tabla_mapeo);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener tablas de mapeo : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarResultSet(rs_opcionesCarga);
			cerrarResultSet(rs_opcionesCodmes);
			cerrarStatement(stmt);
			cerrarStatement(stmt_opcionesCarga);
			cerrarStatement(stmt_opcionesCodmes);
		}
		return tablas_mapeo;
	}

	// Para ajustes ya realizados (ejecucion PE creada)
	private ArrayList<AjustePdProyec> obtenerAjustesPdProyPorEscenarioCreado(Connection conn, Integer id_ejecucionPE,
			Integer id_escenarioModMac) throws Exception { // Integer id_ejecucionModMac

		ArrayList<AjustePdProyec> ajustesPdProy = new ArrayList<AjustePdProyec>();
		AjustePdProyec ajustePdProy = null;

		Statement stmt = null;
		ResultSet rs = null;

		String sql = "select MMM.FecProyeccion, MMM.NumValorPdFinal, MMM.FlgAjustado, OMM.NumValorPd "
				+ "from HE_MapeoModMacro MMM "
				+ "left join HE_OutputModeloMacro OMM on MMM.CodEjecucionModMacro = OMM.CodEjecucion "
				+ "and MMM.CodEscenarioModMacro = OMM.CodEscenario and MMM.FecProyeccion = OMM.FecProyeccion "
				+ "where MMM.CodEjecucionPE = " + id_ejecucionPE
				// + " and MMM.CodEjecucionModMacro = "+ id_ejecucionModMac
				+ " and MMM.CodEscenarioModMacro = " + id_escenarioModMac + " order by MMM.FecProyeccion";

		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				ajustePdProy = new AjustePdProyec();
				ajustePdProy.setFecha_proyeccion(rs.getInt("FecProyeccion"));
				ajustePdProy.setPd_calculado(rs.getDouble("NumValorPd"));
				ajustePdProy.setFlag_ajustado(rs.getBoolean("FlgAjustado"));
				ajustePdProy.setPd_final(rs.getDouble("NumValorPdFinal"));

				ajustesPdProy.add(ajustePdProy);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener los ajustes de pd del modelo macro por escenario: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return ajustesPdProy;
	}

	// Para ajustes por realizar (nueva ejecucion PE)
	private ArrayList<AjustePdProyec> obtenerAjustesPdProyPorEscenario(Connection conn, Integer id_juego_escenarios,
			Integer tipo_ajusteManual, Integer id_ejecucionModMac, Integer id_escenarioModMac) throws Exception {

		ArrayList<AjustePdProyec> ajustesPdProy = new ArrayList<AjustePdProyec>();
		AjustePdProyec ajustePdProy = null;
		Integer numLifetime = null;

		Statement stmt = null;
		ResultSet rs = null;

		Statement stmt_lifetime = null;
		ResultSet rs_lifetime = null;

		String sql = "select FecProyeccion, NumValorPd from HE_OutputModeloMacro " + " where CodEjecucion = "
				+ id_ejecucionModMac + " and CodEscenario = " + id_escenarioModMac;

		String sql_lifetime = "select MC.NumLifeTime from HE_JuegoEscenariosVersion JEV "
				+ "left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
				+ "left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ "where JEV.CodJuegoEscenarios = " + id_juego_escenarios;

		try {
			if (tipo_ajusteManual.equals(1)) { // Ajuste manual
				stmt_lifetime = crearStatement(conn);
				rs_lifetime = stmt_lifetime.executeQuery(sql_lifetime);

				while (rs_lifetime.next()) {
					numLifetime = rs_lifetime.getInt("NumLifeTime");
				}

				for (int i = 1; i <= numLifetime; i++) {
					ajustePdProy = new AjustePdProyec();
					ajustePdProy.setFecha_proyeccion(i);
					ajustePdProy.setPd_calculado(null); // no exite resultado
					ajustePdProy.setFlag_ajustado(true); // ajustado por default
					ajustePdProy.setPd_final(0.0); // cero por default

					ajustesPdProy.add(ajustePdProy);
				}
			} else if (tipo_ajusteManual.equals(2)) { // Ajuste manual doble
				stmt_lifetime = crearStatement(conn);
				rs_lifetime = stmt_lifetime.executeQuery(sql_lifetime);

				while (rs_lifetime.next()) {
					numLifetime = rs_lifetime.getInt("NumLifeTime");
				}

				for (int i = 1; i <= numLifetime; i++) {
					ajustePdProy = new AjustePdProyec();
					ajustePdProy.setFecha_proyeccion(i + 1000);
					ajustePdProy.setPd_calculado(null); // no exite resultado
					ajustePdProy.setFlag_ajustado(true); // ajustado por default
					ajustePdProy.setPd_final(0.0); // cero por default

					ajustesPdProy.add(ajustePdProy);
				}

				for (int i = 1; i <= numLifetime; i++) {
					ajustePdProy = new AjustePdProyec();
					ajustePdProy.setFecha_proyeccion(i + 2000);
					ajustePdProy.setPd_calculado(null); // no exite resultado
					ajustePdProy.setFlag_ajustado(true); // ajustado por default
					ajustePdProy.setPd_final(0.0); // cero por default

					ajustesPdProy.add(ajustePdProy);
				}

			} else if (tipo_ajusteManual.equals(0)) {
				stmt = crearStatement(conn);
				rs = stmt.executeQuery(sql);

				while (rs.next()) {
					ajustePdProy = new AjustePdProyec();
					ajustePdProy.setFecha_proyeccion(rs.getInt("FecProyeccion"));
					ajustePdProy.setPd_calculado(rs.getDouble("NumValorPd"));

					// Por default sin ajuste y valor final igual que el calculado
					ajustePdProy.setFlag_ajustado(false);
					ajustePdProy.setPd_final(rs.getDouble("NumValorPd"));

					ajustesPdProy.add(ajustePdProy);
				}
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener los ajustes de pd del modelo macro por escenario: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarResultSet(rs_lifetime);
			cerrarStatement(stmt);
			cerrarStatement(stmt_lifetime);
		}
		return ajustesPdProy;
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase1(
			Integer id_juego_escenarios, Integer tipo_ajusteManual, Integer id_ejecucion_modMacro) throws Exception {
		return this.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios(id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMacro, 1, false);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase2(
			Integer id_juego_escenarios, Integer tipo_ajusteManual, Integer id_ejecucion_modMacro) throws Exception {
		return this.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios(id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMacro, 2, false);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenariosSimulacion(
			Integer id_juego_escenarios, Integer tipo_ajusteManual, Integer id_ejecucion_modMacro, Integer fase)
			throws Exception {
		return this.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios(id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMacro, fase, true);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios(Integer id_juego_escenarios,
			Integer tipo_ajusteManual, Integer id_ejecucion_modMacro, Integer fase, Boolean esSimulacion)
			throws Exception {
		MapeoCargasModMacEjec mapeo_cargas_modMac = new MapeoCargasModMacEjec();
		ArrayList<MapeoCargasModMacEscVer> mapeos_cargasModMac_esc = new ArrayList<MapeoCargasModMacEscVer>();
		MapeoCargasModMacEscVer mapeo_cargasModMac_esc = null;
		Escenario_version escenario_version = null;
		Escenario escenario = null;

		Connection conn = null;
		Statement stmt_ejecuciones = null;
		ResultSet rs_ejecuciones = null;

		String sql_mapeo_escenarioVersion = null;

		String sql_ejecuciones = "select EV.CodEscenarioVersion, "
				+ "ES.CodEscenario, ES.NbrEscenario, ES.FlgObligatorio " + "from HE_EscenarioVersion EV "
				+ "left join ME_Escenario ES on EV.CodEscenario = ES.CodEscenario " + "where EV.CodJuegoEscenarios = "
				+ id_juego_escenarios;

		String sql_mapeo_carteraVersion = "select MTB.CodMapeoTabla,"
				+ " TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata,"
				+ " A.CodAprovisionamiento, A.NbrFichero, " + " B.CodBloque, B.NbrBloque, B.NbrVariableMotor "
				+ " from HE_MapeoTablaBloque MTB" + " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput"
				+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
				+ " left join HE_Bloque B on MTB.CodBloque= B.CodBloque"
				+ " left join ME_TipoBloque TB on B.CodTipoBloque= TB.CodTipoBloque"
				+ " where MTB.CodEscenarioVersion is null and B.FlgTablaInput = 1 and TB.FlgBucket = 0"
				+ " and MTB.CodCarteraVersion = "
				+ " (select CodCarteraVersion from HE_JuegoEscenariosVersion where CodJuegoEscenarios = "
				+ id_juego_escenarios + ")" + " and (TB.CodTipoEjecucion in " + ((fase.equals(1)) ? "(1,7)" : "(2,8)")
				// Incluye inversiones
				+ " or TB.CodTipoBloque in (11, 21, 22, 3))";
		// Si es bloque principal o general extendido (de cred e inv) o extrapolacion
		// (solo cred),
		// debe aparecer en fase2 tambien...

		try {
			conn = obtenerConexion();

			// Completamos tablas mapeo a nivel de cartera version
			mapeo_cargas_modMac.setTablas_mapeo_carteraVersion(
					obtenerTablasMapeo(sql_mapeo_carteraVersion, fase, esSimulacion, conn, false, true));

			stmt_ejecuciones = crearStatement(conn);
			rs_ejecuciones = stmt_ejecuciones.executeQuery(sql_ejecuciones);

			while (rs_ejecuciones.next()) {
				mapeo_cargasModMac_esc = new MapeoCargasModMacEscVer();

				escenario_version = new Escenario_version();
				escenario_version.setId_escenario_version(rs_ejecuciones.getInt("CodEscenarioVersion"));

				escenario = new Escenario();
				escenario.setId_escenario(rs_ejecuciones.getInt("CodEscenario"));
				escenario.setNombre_escenario(rs_ejecuciones.getString("NbrEscenario"));
				escenario.setFlag_obligatorio(rs_ejecuciones.getBoolean("FlgObligatorio"));
				escenario_version.setEscenario(escenario);
				mapeo_cargasModMac_esc.setEscenario_version(escenario_version);

				// Solo para simulaciones
				if (mapeo_cargasModMac_esc.getEscenario_version().getEscenario().getFlag_obligatorio()) { // Obligamos a
																											// ejecutar
																											// el base
					mapeo_cargasModMac_esc.setFlag_ejecutar(true);
				} else { // False por default
					mapeo_cargasModMac_esc.setFlag_ejecutar(false);
				}

				// Solo existen bloques por escenario en fase 1
				// if (fase.equals(1)) {
				sql_mapeo_escenarioVersion = "select MTB.CodMapeoTabla,"
						+ " TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata,"
						+ " A.CodAprovisionamiento, A.NbrFichero, " + " B.CodBloque, B.NbrBloque, B.NbrVariableMotor "
						+ " from HE_MapeoTablaBloque MTB"
						+ " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput"
						+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
						+ " left join HE_Bloque B on MTB.CodBloque= B.CodBloque"
						+ " left join ME_TipoBloque TB on B.CodTipoBloque= TB.CodTipoBloque"
						+ " where B.FlgTablaInput = 1 and TB.FlgBucket = 0 and TB.CodTipoEjecucion in "
						+ ((fase.equals(1)) ? "(1,7)" : "(2,8)") + " and MTB.CodEscenarioVersion = "
						+ mapeo_cargasModMac_esc.getEscenario_version().getId_escenario_version();

				// Completamos tablas mapeo a nivel de escenario version
				mapeo_cargasModMac_esc.setTablas_mapeo_escenarioVersion(
						obtenerTablasMapeo(sql_mapeo_escenarioVersion, fase, esSimulacion, conn, false, true));
				// }

				// Completamos opciones de mapeo de modelo macro (nivel escenario version)
				if (fase.equals(1) && !tipo_ajusteManual.equals(-1)) {
					mapeo_cargasModMac_esc.setAjustesPdProy(this.obtenerAjustesPdProyPorEscenario(conn,
							id_juego_escenarios, tipo_ajusteManual, id_ejecucion_modMacro,
							mapeo_cargasModMac_esc.getEscenario_version().getEscenario().getId_escenario()));
				}

				mapeos_cargasModMac_esc.add(mapeo_cargasModMac_esc);
			}

			mapeo_cargas_modMac.setMapeosCargasModMacEsc(mapeos_cargasModMac_esc);

		} catch (Exception ex) {
			LOGGER.error("Error al obtener mapeo de cargas y modelo macro de la ejecucion : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_ejecuciones);
			cerrarStatement(stmt_ejecuciones);
		}
		return mapeo_cargas_modMac;
	}

	public void crearEjecucionAgregadaPE_fase1(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		this.crearEjecucionAgregadaPE(ejecucionAgregada, 1, false, false);
	}

	public void crearEjecucionAgregadaPE_fase2(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		this.crearEjecucionAgregadaPE(ejecucionAgregada, 2, false, false);
	}

	public void crearSimulacionAgregadaPE(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		this.crearEjecucionAgregadaPE(ejecucionAgregada,
				(ejecucionAgregada.getFase() == null ? 1 : ejecucionAgregada.getFase()), true, false);
	}

	public void crearEjecucionAgregadaPE(EjecucionAgregadaPE ejecucionAgregada, Integer fase, Boolean esSimulacion,
			Boolean esOficialF1Pasada) throws Exception {
		ArrayList<TablaMapeoEjecucion> tablas_mapeo = null;
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		PreparedStatement pstmt_mapeo_ejecucion = null;
		PreparedStatement pstmt_mapeo_modMacro = null;
		Integer id_ejecucion = null;
		EstructuraMetodologica metodologia = null;
		// Solo para deltas (fase2)
		PreparedStatement pstmt_mapeo_deltas = null;
		Statement stmt_ejecucionFase1 = null;
		Statement stmt_ejecucionFase2Ant = null;
		ResultSet rs_ejecucionFase1 = null;
		ResultSet rs_ejecucionFase2Ant = null;
		ArrayList<Stage> stages = null;
		Integer id_ejecucion_fase1 = null;
		Integer id_ejecucion_fase2Ant = null;
		Boolean sinFase2Ant = null;

		String sql_ejecucion = "Insert into HE_Ejecucion(CodEjecucion, CodMes, CodEscenarioVersion, CodTipoEjecucion,"
				+ " TipEstadoEjecucion, TipEstadoValidacion, TipEstadoAjustes, DesComentarioEjecucion, FecEjecucion,"
				+ " CodMetodologia, CodPrimerPaso, CodEjecucionVar, FlgRegulatorio, CodEjecucionModMac, CodEjecucionRepricing,"
				+ " CodCarteraVerOtros, FecActualizacionTabla) " + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		String sql_mapeo_ejecucion = "Insert into HE_MapeoEjecucion(CodEjecucion, CodMapeoTabla, CodCargaFichero,"
				+ " FecActualizacionTabla) values (?,?,?,?)";

		String sql_mapeo_modMacro = "Insert into HE_MapeoModMacro(CodEjecucionPE, TipAjusteManual, FecProyeccion, "
				+ "CodEjecucionModMacro, CodEscenarioModMacro, NumValorPdFinal, FlgAjustado, FecActualizacionTabla) values "
				+ "(?,?,?,?,?,?,?,?)";

		// Solo para deltas (fase2)
		String sql_ejecucionFase1 = "select E.CodEjecucion from HE_Ejecucion E "
				+ "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
				+ "where E.CodTipoEjecucion = 1 and E.TipEstadoEjecucion in (2,4) and E.TipEstadoValidacion = 4 "
				+ " and E.TipEstadoAjustes in (0,3) "
				// Fase1 - Ejecutado - Oficial - Ajuste aprobado / sin ajuste
				+ "and E.CodMes = " + ejecucionAgregada.getCodmes() + " and EV.CodJuegoEscenarios = "
				+ ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios();

		String sql_ejecucionFase2Ant = "select E.CodEjecucion from HE_Ejecucion E "
				+ "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
				+ "left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
				+ "left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
				+ "left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ "where E.CodTipoEjecucion = 2 and E.TipEstadoEjecucion in (2,4) and E.TipEstadoValidacion = 4 "
				+ " and E.TipEstadoAjustes in (0,3) "
				// Fase2 - Ejecutado - Oficial - Ajuste aprobado / sin ajuste
				+ "and E.CodMes = " + ejecucionAgregada.getCodmes() + " and MC.CodCartera = "
				+ ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
						.getId_cartera();

		String sql_mapeo_deltas = "	Insert into HE_MapeoDelta (CodEjecucionFase2, CodStage, CodEjecucionFase1, "
				+ "CodEjecucionFase2Ant, NumValorDeltaMes, NumValorDeltaProm, NumValorDeltaPromFinal, "
				+ "FlgDeltaAjustado,CODCARTERA ,FecActualizacionTabla) values " + "(?,?,?,?,?,?,?,?,?,?)";
		try {
			// Revisa si la metodología es extrapolación, de ser así revisa la existencia de
			// ejecuciones oficiales de las carteras extrapoladas para el mes

			conn = obtenerConexion();

			metodologia = ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia();
			if (metodologia.getFlujo().getId_tipo_flujo().equals(1)
					&& metodologia.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(2)) {
				ArrayList<JuegoEscenarios_version> juegosExtrapolacion = obtenerJuegosExtrapolacion(metodologia,
						ejecucionAgregada.getCodmes(), fase, conn);
				for (JuegoEscenarios_version juegoExtrapolacion : juegosExtrapolacion) {
					if (juegoExtrapolacion == null) {
						throw new CustomException(
								"Las carteras de extrapolación no cuentan con un juego oficial para la fecha:"
										+ ejecucionAgregada.getCodmes());
					}
				}
			}
			for (MapeoCargasModMacEscVer mapeoEscenario : ejecucionAgregada.getMapeo_cargasModMac_ejecucion()
					.getMapeosCargasModMacEsc()) {

				if (mapeoEscenario.getFlag_ejecutar() || !esSimulacion) { // Si no es simulacion siempre se ejecuta
					id_ejecucion = obtenerSiguienteId("SEQ_HE_Ejecucion");

					pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
					pstmt_ejecucion.setInt(1, id_ejecucion);
					pstmt_ejecucion.setInt(2, ejecucionAgregada.getCodmes());
					pstmt_ejecucion.setLong(3, mapeoEscenario.getEscenario_version().getId_escenario_version());

					if (ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia()
							.getCartera().getTipo_ambito().getId_tipo_ambito().equals(2)) { // Inversiones
						pstmt_ejecucion.setInt(4, 6 + fase); // Tipo de ejecucion: Calculo PE Inv - fase 1 o 2
					} else if (ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia()
							.getCartera().getTipo_ambito().getId_tipo_ambito().equals(1)) { // Creditos
						pstmt_ejecucion.setInt(4, fase); // Tipo de ejecucion: Calculo PE - fase 1 o 2
					}

					pstmt_ejecucion.setInt(5, ejecucionAgregada.getEstado_ejecucion());

					if (esSimulacion || esOficialF1Pasada) { // Simulacion o oficial pasada fase 1 (lo regresamos a
																// simulacion)
						pstmt_ejecucion.setInt(6, -1); // Estado validacion: No requiere validacion
					} else { // EjecucionPE
						pstmt_ejecucion.setInt(6, 0); // Estado validacion: Validacion pendiente
					}

					pstmt_ejecucion.setInt(7, 0); // Estado ajuste: Sin ajuste
					pstmt_ejecucion.setString(8, ""); // Sin comentarios
					pstmt_ejecucion.setLong(9, ejecucionAgregada.getDatetime_ejecucion());
					pstmt_ejecucion.setNull(10, java.sql.Types.INTEGER);
					pstmt_ejecucion.setNull(11, java.sql.Types.INTEGER);
					pstmt_ejecucion.setNull(12, java.sql.Types.INTEGER);
					pstmt_ejecucion.setNull(13, java.sql.Types.INTEGER);
					pstmt_ejecucion.setNull(14, java.sql.Types.INTEGER);

					if (ejecucionAgregada.getEjecucion_repricing() != null) {
						pstmt_ejecucion.setInt(15, ejecucionAgregada.getEjecucion_repricing().getId_ejecucion());
					} else {
						pstmt_ejecucion.setNull(15, java.sql.Types.INTEGER);
					}

					pstmt_ejecucion.setNull(16, java.sql.Types.INTEGER);
					pstmt_ejecucion.setDate(17, new Date(System.currentTimeMillis()));

					pstmt_ejecucion.executeUpdate();
					cerrarPreparedStatement(pstmt_ejecucion);

					// Tablas mapeo de cartera version
					tablas_mapeo = new ArrayList<TablaMapeoEjecucion>();
					tablas_mapeo = ejecucionAgregada.getMapeo_cargasModMac_ejecucion().getTablas_mapeo_carteraVersion();

					// Juntamos tablas de escenario version
					for (TablaMapeoEjecucion tablaMapeo : mapeoEscenario.getTablas_mapeo_escenarioVersion()) {
						tablas_mapeo.add(tablaMapeo);
					}

					for (TablaMapeoEjecucion tabla_mapeo_ejecucion : tablas_mapeo) {
						pstmt_mapeo_ejecucion = conn.prepareStatement(sql_mapeo_ejecucion);
						pstmt_mapeo_ejecucion.setInt(1, id_ejecucion);
						pstmt_mapeo_ejecucion.setInt(2, tabla_mapeo_ejecucion.getId_mapeo_tabla());
						pstmt_mapeo_ejecucion.setInt(3, tabla_mapeo_ejecucion.getCarga().getId_cargafichero());
						pstmt_mapeo_ejecucion.setDate(4, new Date(System.currentTimeMillis()));

						pstmt_mapeo_ejecucion.executeUpdate();
						cerrarPreparedStatement(pstmt_mapeo_ejecucion);
					}

					// Ingresamos mapeo para modelo macro (solo para fase 1)
					if (fase.equals(1) && !ejecucionAgregada.getTipo_ajusteManual().equals(-1)) {
						for (AjustePdProyec ajuste : mapeoEscenario.getAjustesPdProy()) {
							pstmt_mapeo_modMacro = conn.prepareStatement(sql_mapeo_modMacro);
							pstmt_mapeo_modMacro.setInt(1, id_ejecucion);
							pstmt_mapeo_modMacro.setInt(2, ejecucionAgregada.getTipo_ajusteManual());
							pstmt_mapeo_modMacro.setInt(3, ajuste.getFecha_proyeccion());

							if (ejecucionAgregada.getTipo_ajusteManual().equals(0)) {
								pstmt_mapeo_modMacro.setInt(4,
										ejecucionAgregada.getEjecucion_modMacro().getId_ejecucion());
							} else {
								pstmt_mapeo_modMacro.setNull(4, java.sql.Types.INTEGER); // no asociado a resultado mod
																							// macro
							}

							pstmt_mapeo_modMacro.setInt(5,
									mapeoEscenario.getEscenario_version().getEscenario().getId_escenario());
							pstmt_mapeo_modMacro.setDouble(6, ajuste.getPd_final());
							pstmt_mapeo_modMacro.setBoolean(7, ajuste.getFlag_ajustado());
							pstmt_mapeo_modMacro.setDate(8, new Date(System.currentTimeMillis()));

							pstmt_mapeo_modMacro.executeUpdate();
							cerrarPreparedStatement(pstmt_mapeo_modMacro);
						}
					}

					// Creamos mapeos de deltas solo para fase 2 y creditos (y solo para escenario
					// base)
					if (fase.equals(2) && mapeoEscenario.getEscenario_version().getEscenario().getFlag_obligatorio()
							&& ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia()
									.getCartera().getTipo_ambito().getNombre_tipo_ambito()
									.equalsIgnoreCase("Creditos")) {

						// Obtenemos id de la ejecucion de fase 1 correspondiente
						stmt_ejecucionFase1 = crearStatement(conn);
						rs_ejecucionFase1 = stmt_ejecucionFase1.executeQuery(sql_ejecucionFase1);

						while (rs_ejecucionFase1.next()) {
							id_ejecucion_fase1 = rs_ejecucionFase1.getInt("CodEjecucion");
						}
						cerrarResultSet(rs_ejecucionFase1);
						cerrarStatement(stmt_ejecucionFase1);

						// Obtenemos id de la ejecucion de fase 2 (oficial) anterior
						stmt_ejecucionFase2Ant = crearStatement(conn);
						rs_ejecucionFase2Ant = stmt_ejecucionFase2Ant.executeQuery(sql_ejecucionFase2Ant);

						sinFase2Ant = true;
						while (rs_ejecucionFase2Ant.next()) {
							id_ejecucion_fase2Ant = rs_ejecucionFase2Ant.getInt("CodEjecucion");
							sinFase2Ant = false;
						}
						cerrarResultSet(rs_ejecucionFase2Ant);
						cerrarStatement(stmt_ejecucionFase2Ant);

						stages = this.obtenerStages(conn);

						for (Stage stage : stages) {
							pstmt_mapeo_deltas = conn.prepareStatement(sql_mapeo_deltas);
							pstmt_mapeo_deltas.setInt(1, id_ejecucion);
							pstmt_mapeo_deltas.setInt(2, stage.getId_stage());
							pstmt_mapeo_deltas.setInt(3, id_ejecucion_fase1);

							if (sinFase2Ant) {
								pstmt_mapeo_deltas.setNull(4, java.sql.Types.INTEGER);
							} else {
								pstmt_mapeo_deltas.setInt(4, id_ejecucion_fase2Ant);
							}

							pstmt_mapeo_deltas.setNull(5, java.sql.Types.DOUBLE); // No se calcula el delta mensual aun

							pstmt_mapeo_deltas.setDouble(6, 1.0); // No se calcula el delta promedio aun (1.0 por
																	// default)
							pstmt_mapeo_deltas.setDouble(7, 1.0); // No se ajusta el delta final aun (1.0 por default)

							pstmt_mapeo_deltas.setBoolean(8, false); // Aun no se ajusta
							pstmt_mapeo_deltas.setInt(9, ejecucionAgregada.getJuego_escenarios_version()
									.getCartera_version().getMetodologia().getCartera().getId_cartera());
							pstmt_mapeo_deltas.setDate(10, new Date(System.currentTimeMillis()));

							pstmt_mapeo_deltas.executeUpdate();
							cerrarPreparedStatement(pstmt_mapeo_deltas);
						}
					}
				}
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar una ejecucion agregada PE de fase " + fase + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_ejecucionFase1);
			cerrarResultSet(rs_ejecucionFase2Ant);
			cerrarStatement(stmt_ejecucionFase1);
			cerrarStatement(stmt_ejecucionFase2Ant);
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarPreparedStatement(pstmt_mapeo_ejecucion);
			cerrarPreparedStatement(pstmt_mapeo_modMacro);
			cerrarPreparedStatement(pstmt_mapeo_deltas);
			cerrarConexion(conn);
		}
	}

	public ArrayList<JuegoEscenarios_version> obtenerJuegosExtrapolacion(EstructuraMetodologica metodologia,
			Integer codmes, Integer fase, Connection conn) throws Exception {
		JuegoEscenarios_version juegoEscenarios_version = null;
		ArrayList<JuegoEscenarios_version> juegosEscenarios_version = new ArrayList<JuegoEscenarios_version>();
		try {
			for (Cartera carteraExtrapolacion : metodologia.getCarteras_extrapolacion()) {
				juegoEscenarios_version = obtenerJuegoFlagOficialPorCarterayMes(carteraExtrapolacion.getId_cartera(),
						codmes, conn);
				juegosEscenarios_version.add(juegoEscenarios_version);
			}
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al obtener el Juego de Escenarios Version de la cartera con id:"
					+ metodologia.getCartera().getId_cartera() + " : " + ex.getMessage());
			throw ex;
		}
		return juegosEscenarios_version;
	}

	public ReplicaEjecucionesAgregadasPE replicarEjecucionesAgregadasPE_fase1(
			ReplicaEjecucionesAgregadasPE replicaEjecucionesAgregadas) throws Exception {
		return this.replicarEjecucionesAgregadasPE(replicaEjecucionesAgregadas, 1);
	}

	public ReplicaEjecucionesAgregadasPE replicarEjecucionesAgregadasPE_fase2(
			ReplicaEjecucionesAgregadasPE replicaEjecucionesAgregadas) throws Exception {
		return this.replicarEjecucionesAgregadasPE(replicaEjecucionesAgregadas, 2);
	}

	public ReplicaEjecucionesAgregadasPE replicarEjecucionesAgregadasPE(
			ReplicaEjecucionesAgregadasPE replicaEjecucionesAgregadas, Integer fase) throws Exception {
		Integer id_ejec_modMac = null;
		for (EjecucionAgregadaPE ejecucionAgregada : replicaEjecucionesAgregadas.getEjecuciones_replicar()) {
			ejecucionAgregada.setEstado_ejecucion(0); // Seteamos el estado: no ejecutado

			if (ejecucionAgregada.getEjecucion_modMacro() == null) {
				id_ejec_modMac = -1;
			} else {
				id_ejec_modMac = ejecucionAgregada.getEjecucion_modMacro().getId_ejecucion();
			}

			ejecucionAgregada.setMapeo_cargasModMac_ejecucion(
					this.obtenerMapeoCargasModMacEjecucion(ejecucionAgregada.getCodmes(),
							ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios(),
							ejecucionAgregada.getTipo_ajusteManual(), id_ejec_modMac, fase, true)); // Solo se puede
																									// replicar en
																									// simulaciones
			ejecucionAgregada.setCodmes(replicaEjecucionesAgregadas.getCodmes_replica());
			this.crearEjecucionAgregadaPE(ejecucionAgregada, fase, true, false); // Solo se puede replicar en
																					// simulaciones
		}

		// Returnamos replica de ejecuciones con mapeos completados
		return replicaEjecucionesAgregadas;
	}

	public ReplicaEjecucionesAgregadasPE replicarSimulacionesAgregadasPE(
			ReplicaEjecucionesAgregadasPE replicaEjecucionesAgregadas) throws Exception {
		Integer id_ejec_modMac = null;
		for (EjecucionAgregadaPE ejecucionAgregada : replicaEjecucionesAgregadas.getEjecuciones_replicar()) {
			ejecucionAgregada.setEstado_ejecucion(0); // Seteamos el estado: no ejecutado

			if (ejecucionAgregada.getEjecucion_modMacro() == null) {
				id_ejec_modMac = -1;
			} else {
				id_ejec_modMac = ejecucionAgregada.getEjecucion_modMacro().getId_ejecucion();
			}

			ejecucionAgregada.setMapeo_cargasModMac_ejecucion(this.obtenerMapeoCargasModMacEjecucion(
					ejecucionAgregada.getCodmes(),
					ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios(),
					ejecucionAgregada.getTipo_ajusteManual(), id_ejec_modMac, ejecucionAgregada.getFase(), true));

			ejecucionAgregada.setCodmes(replicaEjecucionesAgregadas.getCodmes_replica());
			ejecucionAgregada.setFase(replicaEjecucionesAgregadas.getFase_replica());
			this.crearEjecucionAgregadaPE(ejecucionAgregada, ejecucionAgregada.getFase(), true, false);
		}

		// Returnamos replica de simulaciones con mapeos completados
		return replicaEjecucionesAgregadas;
	}

	public void editarEjecucionAgregadaPE_fase1(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		this.editarEjecucionAgregadaPE(ejecucionAgregada, 1);
	}

	public void editarEjecucionAgregadaPE_fase2(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		this.editarEjecucionAgregadaPE(ejecucionAgregada, 2);
	}

	public void editarEjecucionAgregadaPE(EjecucionAgregadaPE ejecucionAgregada, Integer fase) throws Exception {
		/*
		 * ArrayList<TablaMapeoEjecucion> tablas_mapeo = null; Connection conn = null;
		 * PreparedStatement pstmt_ejecucion = null; PreparedStatement
		 * pstmt_mapeo_ejecucion = null; String sql_ejecucion =
		 * "Update HE_Ejecucion set CodMes= ?, CodEscenarioVersion= ?, CodTipoEjecucion= ?, "
		 * +
		 * "TipEstadoEjecucion= ?, FecEjecucion= ?, FecActualizacionTabla= ? where CodEjecucion = ?"
		 * ;
		 * 
		 * String sql_mapeo_ejecucion =
		 * "Update HE_MapeoEjecucion set CodCargaFichero= ?, FecActualizacionTabla= ?" +
		 * " where CodEjecucion= ? and CodMapeoTabla= ?";
		 * 
		 * try { conn = obtenerConexion();
		 * 
		 * for (MapeoCargasModMacEscVer mapeoEscenario :
		 * ejecucionAgregada.getMapeo_cargasModMac_ejecucion()
		 * .getMapeosCargasModMacEsc()) { pstmt_ejecucion =
		 * conn.prepareStatement(sql_ejecucion);
		 * 
		 * pstmt_ejecucion.setInt(1, ejecucionAgregada.getCodmes());
		 * pstmt_ejecucion.setInt(2,
		 * mapeoEscenario.getEscenario_version().getId_escenario_version());
		 * pstmt_ejecucion.setInt(3, fase); // Tipo de ejecucion: Calculo PE - fase 1 o
		 * 2 pstmt_ejecucion.setInt(4, ejecucionAgregada.getEstado_ejecucion());
		 * pstmt_ejecucion.setLong(5, ejecucionAgregada.getDatetime_ejecucion());
		 * pstmt_ejecucion.setDate(6, new Date(System.currentTimeMillis()));
		 * pstmt_ejecucion.setInt(7, mapeoEscenario.getId_ejecucion());
		 * 
		 * pstmt_ejecucion.executeUpdate();
		 * 
		 * // Juntamos tablas mapeo de cartera version y escenario version tablas_mapeo
		 * = new ArrayList<TablaMapeoEjecucion>(); tablas_mapeo =
		 * ejecucionAgregada.getMapeo_cargasModMac_ejecucion().
		 * getTablas_mapeo_carteraVersion(); for (TablaMapeoEjecucion tablaMapeo :
		 * mapeoEscenario.getTablas_mapeo_escenarioVersion()) {
		 * tablas_mapeo.add(tablaMapeo); }
		 * 
		 * if (tablas_mapeo != null) { for (TablaMapeoEjecucion tabla_mapeo_ejecucion :
		 * tablas_mapeo) { pstmt_mapeo_ejecucion =
		 * conn.prepareStatement(sql_mapeo_ejecucion); pstmt_mapeo_ejecucion.setInt(1,
		 * tabla_mapeo_ejecucion.getCarga().getId_cargafichero());
		 * pstmt_mapeo_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
		 * pstmt_mapeo_ejecucion.setInt(3, mapeoEscenario.getId_ejecucion());
		 * pstmt_mapeo_ejecucion.setInt(4, tabla_mapeo_ejecucion.getId_mapeo_tabla());
		 * 
		 * pstmt_mapeo_ejecucion.executeUpdate(); } } }
		 * 
		 * ejecutarCommit(conn);
		 * 
		 * } catch (Exception ex) { ejecutarRollback(conn);
		 * LOGGER.error("Error al actualizar ejecucion agregada PE de fase " + fase +
		 * " : " + ex.getMessage()); throw ex;
		 * 
		 * } finally { cerrarPreparedStatement(pstmt_ejecucion);
		 * cerrarPreparedStatement(pstmt_mapeo_ejecucion); cerrarConexion(conn); }
		 */

		// Borramos todas las ejecuciones anteriores (aca se borran dependencias de
		// variaciones tambien)
		this.borrarEjecucionAgregadaPE(ejecucionAgregada.getCodmes(),
				ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios(), fase);

		// En caso sea fase 1 y oficial, debemos borrar la ejecucion de fase 2 asociada
		// (solo creditos)
		if (fase.equals(1) && ejecucionAgregada.getEstado_validacion().equals(4)
				&& ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
						.getTipo_ambito().getId_tipo_ambito().equals(1)) {
			this.borrarEjecucionAgregadaPE(ejecucionAgregada.getCodmes(),
					ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios(), 2);
		}

		// Creamos todas las ejecuciones nuevamente
		if (fase.equals(1) && ejecucionAgregada.getEstado_validacion().equals(4)) { // Si es fase 1 y oficial debe
																					// regresarlo a simulacion
			this.crearEjecucionAgregadaPE(ejecucionAgregada, fase, false, true);
		} else {
			this.crearEjecucionAgregadaPE(ejecucionAgregada, fase, false, false);
		}
	}

	public void editarSimulacionAgregadaPE(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		// Borramos todas las ejecuciones anteriores
		this.borrarEjecucionAgregadaPE(ejecucionAgregada.getCodmes(),
				ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios(),
				ejecucionAgregada.getFase() == null ? 1 : ejecucionAgregada.getFase());

		// Creamos todas las ejecuciones nuevamente
		this.crearSimulacionAgregadaPE(ejecucionAgregada);
	}

	public void borrarEjecucionAgregadaPE_fase1(Integer codmes, Integer id_juego_escenarios) throws Exception {
		this.borrarEjecucionAgregadaPE(codmes, id_juego_escenarios, 1);
	}

	public void borrarEjecucionAgregadaPE_fase2(Integer codmes, Integer id_juego_escenarios) throws Exception {
		this.borrarEjecucionAgregadaPE(codmes, id_juego_escenarios, 2);
	}

	public void borrarSimulacionBifAgregadaPE(Integer codmes, Integer fase, Integer id_juego_escenarios)
			throws Exception {
		this.borrarEjecucionAgregadaPE(codmes, id_juego_escenarios, fase);
	}

	public void borrarEjecucionAgregadaPE(Integer codmes, Integer id_juego_escenarios, Integer fase) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;

		Integer id_tipo_ambito = null;
		Statement stmt_consultaAmbito = null;
		ResultSet rs_consultaAmbito = null;

		String sql_ejecucion = "DELETE from HE_Ejecucion where CodEjecucion in "
				+ "(select HE_Ejecucion.CodEjecucion from HE_Ejecucion left join HE_EscenarioVersion "
				+ "on HE_Ejecucion.CodEscenarioVersion = HE_EscenarioVersion.CodEscenarioVersion "
				+ "where HE_EscenarioVersion.CodJuegoEscenarios = ? " + "and HE_Ejecucion.CodMes = ? "
				+ "and HE_Ejecucion.CodTipoEjecucion = ?)";

		String sql_consultaAmbito = "select TA.CodTipoAmbito from HE_JuegoEscenariosVersion JEV "
				+ "left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
				+ "left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ "left join HE_Cartera C on MC.CodCartera = C.CodCartera "
				+ "left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
				+ "where JEV.CodJuegoEscenarios = " + id_juego_escenarios;

		try {
			conn = obtenerConexion();

			// Consultamos ambito
			stmt_consultaAmbito = crearStatement(conn);
			rs_consultaAmbito = stmt_consultaAmbito.executeQuery(sql_consultaAmbito);

			while (rs_consultaAmbito.next()) {
				id_tipo_ambito = rs_consultaAmbito.getInt("CodTipoAmbito");
			}

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
			pstmt_ejecucion.setInt(1, id_juego_escenarios);
			pstmt_ejecucion.setInt(2, codmes);

			if (id_tipo_ambito.equals(2)) { // Inversiones
				pstmt_ejecucion.setInt(3, 6 + fase);
			} else if (id_tipo_ambito.equals(1)) { // Creditos
				pstmt_ejecucion.setInt(3, fase);
			}

			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar una ejecucion agregada PE - fase " + fase + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_consultaAmbito);
			cerrarStatement(stmt_consultaAmbito);
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	public void borrarResultadosPE(Connection conn, Integer codmes, Integer id_juego_escenarios, Integer fase)
			throws Exception {
		PreparedStatement pstmt_resultados = null;
		// PreparedStatement pstmt_resultados_otros1 = null;
		// PreparedStatement pstmt_resultados_otros2 = null;
		// PreparedStatement pstmt_resultados_otros3 = null;
		// PreparedStatement pstmt_resultados_otros4 = null;
		// PreparedStatement pstmt_resultados_otros5 = null;
		PreparedStatement pstmt_updateFecEjec = null;

		Integer id_tipo_ambito = null;
		Statement stmt_consultaAmbito = null;
		ResultSet rs_consultaAmbito = null;

		ArrayList<String> codigos_ejec = null;
		Statement stmt_consultaCodsEjec = null;
		ResultSet rs_consultaCodsEjec = null;

		// String sql_resultados_otros1 = null;
		// String sql_resultados_otros2 = null;
		// String sql_resultados_otros3 = null;
		// String sql_resultados_otros4 = null;
		// String sql_resultados_otros5 = null;

		String sql_resultados = null;

		String sql_consultaAmbito = "select TA.CodTipoAmbito from HE_JuegoEscenariosVersion JEV "
				+ "left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
				+ "left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ "left join HE_Cartera C on MC.CodCartera = C.CodCartera "
				+ "left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
				+ "where JEV.CodJuegoEscenarios = " + id_juego_escenarios;

		// Actualizamos timestamp de ejecucion (para detectar actualizacion en Qlik)
		String sql_updateFecEjec = "Update HE_Ejecucion set FecEjecucion = ? " + "where CodEjecucion in "
				+ "(select HE_Ejecucion.CodEjecucion from HE_Ejecucion left join HE_EscenarioVersion "
				+ "on HE_Ejecucion.CodEscenarioVersion = HE_EscenarioVersion.CodEscenarioVersion "
				+ "where HE_EscenarioVersion.CodJuegoEscenarios = ? and HE_Ejecucion.CodMes = ? "
				+ " and HE_Ejecucion.CodTipoEjecucion = ?)";

		try {
			// Consultamos ambito
			stmt_consultaAmbito = crearStatement(conn);
			rs_consultaAmbito = stmt_consultaAmbito.executeQuery(sql_consultaAmbito);

			while (rs_consultaAmbito.next()) {
				id_tipo_ambito = rs_consultaAmbito.getInt("CodTipoAmbito");
			}

			stmt_consultaCodsEjec = crearStatement(conn);

			String sql_consultaCodsEjec = "select E.CodEjecucion from HE_Ejecucion E "
					+ "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
					+ "where EV.CodJuegoEscenarios = " + id_juego_escenarios + " and E.Codmes = " + codmes
					+ " and E.CodTipoEjecucion = " + (id_tipo_ambito.equals(2) ? fase + 6 : fase);

			rs_consultaCodsEjec = stmt_consultaCodsEjec.executeQuery(sql_consultaCodsEjec);

			codigos_ejec = new ArrayList<String>();
			while (rs_consultaCodsEjec.next()) {
				codigos_ejec.add(Integer.toString(rs_consultaCodsEjec.getInt("CodEjecucion")));
			}

			if (id_tipo_ambito.equals(2)) { // Inversiones
				sql_resultados = "DELETE from HE_OutputProvisionesInv " + "where codEjecucion in ("
						+ String.join(",", codigos_ejec) + ")";
			} else if (id_tipo_ambito.equals(1)) { // Creditos
				sql_resultados = "DELETE from HE_OutputProvisionesCred " + "where codEjecucion in ("
						+ String.join(",", codigos_ejec) + ")";
			}

			pstmt_resultados = conn.prepareStatement(sql_resultados);

			pstmt_resultados.executeUpdate();

			/*
			 * sql_resultados_otros1 = "DELETE from HE_OutputCastigos " +
			 * "where CodEjecucion in (" + String.join(",", codigos_ejec) + ")";
			 * pstmt_resultados_otros1 = conn.prepareStatement(sql_resultados_otros1);
			 * pstmt_resultados_otros1.executeUpdate();
			 * 
			 * sql_resultados_otros2 = "DELETE from HE_OutputVentaCartera " +
			 * "where CodEjecucion in (" + String.join(",", codigos_ejec) + ")";
			 * pstmt_resultados_otros2 = conn.prepareStatement(sql_resultados_otros2);
			 * pstmt_resultados_otros2.executeUpdate();
			 * 
			 * sql_resultados_otros3 = "DELETE from HE_OutputTipoCambio " +
			 * "where CodEjecucion in (" + String.join(",", codigos_ejec) + ")";
			 * pstmt_resultados_otros3 = conn.prepareStatement(sql_resultados_otros3);
			 * pstmt_resultados_otros3.executeUpdate();
			 * 
			 * sql_resultados_otros4 = "DELETE from HE_OutputGarantia " +
			 * "where CodEjecucion in (" + String.join(",", codigos_ejec) + ")";
			 * pstmt_resultados_otros4 = conn.prepareStatement(sql_resultados_otros4);
			 * pstmt_resultados_otros4.executeUpdate();
			 * 
			 * 
			 * sql_resultados_otros5 = "DELETE from HE_OutputFacMad " +
			 * "where CodEjecucion in (" + String.join(",", codigos_ejec) + ")";
			 * pstmt_resultados_otros5 = conn.prepareStatement(sql_resultados_otros5);
			 * pstmt_resultados_otros5.executeUpdate();
			 */

			// Actualizamos fecEjecucion (timestamp)
			pstmt_updateFecEjec = conn.prepareStatement(sql_updateFecEjec);
			pstmt_updateFecEjec.setLong(1, System.currentTimeMillis());
			pstmt_updateFecEjec.setInt(2, id_juego_escenarios);
			pstmt_updateFecEjec.setInt(3, codmes);

			if (id_tipo_ambito.equals(2)) { // Inversiones
				pstmt_updateFecEjec.setInt(4, 6 + fase);
			} else if (id_tipo_ambito.equals(1)) { // Creditos
				pstmt_updateFecEjec.setInt(4, fase);
			}

			pstmt_updateFecEjec.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar resultados PE : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_consultaAmbito);
			cerrarResultSet(rs_consultaCodsEjec);
			cerrarStatement(stmt_consultaAmbito);
			cerrarStatement(stmt_consultaCodsEjec);
			cerrarPreparedStatement(pstmt_resultados);
			// cerrarPreparedStatement(pstmt_resultados_otros1);
			// cerrarPreparedStatement(pstmt_resultados_otros2);
			// cerrarPreparedStatement(pstmt_resultados_otros3);
			// cerrarPreparedStatement(pstmt_resultados_otros4);
			// cerrarPreparedStatement(pstmt_resultados_otros5);
			cerrarPreparedStatement(pstmt_updateFecEjec);
		}
	}

	public void borrarResultadosVariacion(Integer id_ejecucion) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_resultados = null;
		PreparedStatement pstmt_resultados2 = null;
		PreparedStatement pstmt_resultados3 = null;
		PreparedStatement pstmt_resultados_otros1 = null;
		PreparedStatement pstmt_resultados_otros2 = null;
		PreparedStatement pstmt_resultados_otros3 = null;
		PreparedStatement pstmt_resultados_otros4 = null;
		PreparedStatement pstmt_updateFecEjec = null;

		String sql_resultados_otros1 = null;
		String sql_resultados_otros2 = null;
		String sql_resultados_otros3 = null;
		String sql_resultados_otros4 = null;

		String sql_resultados = "DELETE from HE_OutputVariacionesCred where CodEjecucion = ?";
		String sql_resultados2 = "DELETE from HE_OutputProvisionesCred where CodEjecucion = ?";
		String sql_resultados3 = "DELETE from HE_OutputRegulatoriosCred where CodEjecucion = ?";

		// Actualizamos timestamp de ejecucion (para detectar actualizacion en Qlik)
		String sql_updateFecEjec = "Update HE_Ejecucion set FecEjecucion = ? " + "where CodEjecucion = ?";

		try {
			conn = obtenerConexion();
			pstmt_resultados = conn.prepareStatement(sql_resultados);
			pstmt_resultados.setInt(1, id_ejecucion);
			pstmt_resultados.executeUpdate();

			pstmt_resultados2 = conn.prepareStatement(sql_resultados2);
			pstmt_resultados2.setInt(1, id_ejecucion);
			pstmt_resultados2.executeUpdate();

			pstmt_resultados3 = conn.prepareStatement(sql_resultados3);
			pstmt_resultados3.setInt(1, id_ejecucion);
			pstmt_resultados3.executeUpdate();

			// Tablas adicionales (4) unicamente para creditos
			sql_resultados_otros1 = "DELETE from HE_OutputCastigos " + "where CodEjecucion = " + id_ejecucion;
			pstmt_resultados_otros1 = conn.prepareStatement(sql_resultados_otros1);
			pstmt_resultados_otros1.executeUpdate();

			sql_resultados_otros2 = "DELETE from HE_OutputVentaCartera " + "where CodEjecucion = " + id_ejecucion;
			pstmt_resultados_otros2 = conn.prepareStatement(sql_resultados_otros2);
			pstmt_resultados_otros2.executeUpdate();

			sql_resultados_otros3 = "DELETE from HE_OutputTipoCambio " + "where CodEjecucion = " + id_ejecucion;
			pstmt_resultados_otros3 = conn.prepareStatement(sql_resultados_otros3);
			pstmt_resultados_otros3.executeUpdate();

			sql_resultados_otros4 = "DELETE from HE_OutputGarantia " + "where CodEjecucion = " + id_ejecucion;
			pstmt_resultados_otros4 = conn.prepareStatement(sql_resultados_otros4);
			pstmt_resultados_otros4.executeUpdate();

			// Actualizamos fecEjecucion (timestamp)
			pstmt_updateFecEjec = conn.prepareStatement(sql_updateFecEjec);
			pstmt_updateFecEjec.setLong(1, System.currentTimeMillis());
			pstmt_updateFecEjec.setInt(2, id_ejecucion);
			pstmt_updateFecEjec.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar resultados de variaciones : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_resultados);
			cerrarPreparedStatement(pstmt_resultados2);
			cerrarPreparedStatement(pstmt_resultados3);
			cerrarPreparedStatement(pstmt_resultados_otros1);
			cerrarPreparedStatement(pstmt_resultados_otros2);
			cerrarPreparedStatement(pstmt_resultados_otros3);
			cerrarPreparedStatement(pstmt_resultados_otros4);
			cerrarPreparedStatement(pstmt_updateFecEjec);
			cerrarConexion(conn);
		}
	}

	public void borrarResultadosPrimerPaso(Connection conn, Integer id_ejecucion) throws Exception {
		PreparedStatement pstmt_resultados1 = null;
		PreparedStatement pstmt_resultados2 = null;
		PreparedStatement pstmt_resultados3 = null;
		PreparedStatement pstmt_resultados4 = null;
		PreparedStatement pstmt_resultados5 = null;

		String sql_resultados1 = "DELETE from HE_OutputModeloMacro where CodEjecucion = ?";

		String sql_resultados2 = "DELETE from HE_OutputRdApp where CodEjecucion = ?";

		String sql_resultados3 = "DELETE from HE_OutputRd where CodEjecucion = ?";

		String sql_resultados4 = "DELETE from HE_OutputFacMad where CodEjecucion = ?";

		String sql_resultados5 = "DELETE from HE_OutputModMacroRef where CodEjecucion = ?";

		try {
			pstmt_resultados1 = conn.prepareStatement(sql_resultados1);
			pstmt_resultados1.setInt(1, id_ejecucion);
			pstmt_resultados1.executeUpdate();

			pstmt_resultados2 = conn.prepareStatement(sql_resultados2);
			pstmt_resultados2.setInt(1, id_ejecucion);
			pstmt_resultados2.executeUpdate();

			pstmt_resultados3 = conn.prepareStatement(sql_resultados3);
			pstmt_resultados3.setInt(1, id_ejecucion);
			pstmt_resultados3.executeUpdate();

			pstmt_resultados4 = conn.prepareStatement(sql_resultados4);
			pstmt_resultados4.setInt(1, id_ejecucion);
			pstmt_resultados4.executeUpdate();

			pstmt_resultados5 = conn.prepareStatement(sql_resultados5);
			pstmt_resultados5.setInt(1, id_ejecucion);
			pstmt_resultados5.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar resultados de primer paso : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_resultados1);
			cerrarPreparedStatement(pstmt_resultados2);
			cerrarPreparedStatement(pstmt_resultados3);
			cerrarPreparedStatement(pstmt_resultados4);
			cerrarPreparedStatement(pstmt_resultados5);
		}
	}

	public EjecucionMotor convertirEjecucionPEAMotor(EjecucionAgregadaPE ejecucionAgregada, Integer fase,
			Boolean esSimulacion, Boolean noBorrarResultado, Boolean esEjectActVar, Integer id_ejec_variacion)
			throws Exception {

		ArrayList<BloqueParametrizado> bloquesParam = null;
		BloqueParametrizado bloqueParam = null;
		ArrayList<GrupoBloquesEscenario> gruposBloquesEscenario = null;
		GrupoBloquesEscenario grupoBloquesEscenario = null;
		TablaInput tablaInput = null;
		TipoEjecucion tipoEjecucion = null;
		Integer id_ejec_modMac = null;

		Connection conn = null;
		Statement stmt_mapeos_carteraVersion = null;
		Statement stmt_mapeos_escenarioVersion = null;
		Statement stmt_infoStaging = null;
		ResultSet rs_carteraVersion = null;
		ResultSet rs_escenarioVersion = null;
		ResultSet rs_infoStaging = null;

		String sql_mapeo_carteraVersion = "select MTB.CodMapeoTabla from HE_MapeoTablaBloque MTB "
				+ "left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
				+ "left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque "
				+ "left join ME_TipoEjecucion TE on TB.CodTipoEjecucion = TE.CodTipoEjecucion "
				+ " where (TE.CodTipoEjecucion in "
				+ (fase.equals(1) ? (esEjectActVar ? "(1,3)" : "(1,7)") : (esEjectActVar ? "(2,3)" : "(2,8)"))
				// Incluimos el tipo de bloque de variaciones (el flag es unicamente para
				// creditos)
				// Incluye inversiones
				+ " or TB.CodTipoBloque in (11, 21, 22, 3)) "
				// Si es bloque principal o general extendido (de cred e inv) o extrapolacion
				// (solo cred), debe aparecer en fase2 tambien...
				+ " and MTB.CodEscenarioVersion is null and MTB.FlgPrimerPaso = 0" + " and MTB.CodCarteraVersion = "
				+ " (select CodCarteraVersion from HE_JuegoEscenariosVersion where CodJuegoEscenarios = "
				+ ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios() + ")";

		String sql_mapeo_escenarioVersion = "select MTB.CodMapeoTabla from HE_MapeoTablaBloque MTB "
				+ " left join HE_Bloque B on MTB.CodBloque= B.CodBloque"
				+ " left join ME_TipoBloque TB on B.CodTipoBloque= TB.CodTipoBloque"
				+ " where MTB.FlgPrimerPaso = 0 and TB.CodTipoEjecucion in " + ((fase.equals(1)) ? "(1,7)" : "(2,8)")
				+ " and MTB.CodEscenarioVersion = ";

		String sql_infoStaging = "select CV.FlgStageInterno, CV.FlgStageExterno, CV.FlgStageSensibilidad,"
				+ " CV.NumMesesSensibilidad, TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata "
				+ " from HE_CarteraVersion CV" + " left join HE_TablaInput TI on CV.CodTablaInputExp = TI.CodTablaInput"
				+ " where CodCarteraVersion = "
				+ ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getId_cartera_version();

		try {
			//LOGGER.info("método convertirEjecucionPEAMotor");
			conn = obtenerConexion();

			// De manera previa, borramos todos los resultados (en caso existan ejecuciones)
			if (!noBorrarResultado) {
				//LOGGER.info("borrarResultadosPE");
				this.borrarResultadosPE(conn, ejecucionAgregada.getCodmes(),
						ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios(), fase);
			}
			//LOGGER.info("ejecucionMotor");
			EjecucionMotor ejecucionMotor = new EjecucionMotor();

			// Completamos mapeos de ejecucion agregada
			if (fase.equals(1)) {

				if (ejecucionAgregada.getEjecucion_modMacro() == null) {
					id_ejec_modMac = -1;
				} else {
					id_ejec_modMac = ejecucionAgregada.getEjecucion_modMacro().getId_ejecucion();
				}

				ejecucionAgregada.setMapeo_cargasModMac_ejecucion(
						this.obtenerMapeoCargasModMacEjecucion(ejecucionAgregada.getCodmes(),
								ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios(),
								ejecucionAgregada.getTipo_ajusteManual(), id_ejec_modMac, fase, esSimulacion));

			} else if (fase.equals(2)) {
				ejecucionAgregada.setMapeo_cargasModMac_ejecucion(
						this.obtenerMapeoCargasModMacEjecucion(ejecucionAgregada.getCodmes(),
								ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios(), -1, -1, fase,
								esSimulacion));
			}

			// Parseo de los identificadores
			ejecucionMotor.setCodmes(ejecucionAgregada.getCodmes());

			// Seteamos tipo de ejecucion
			for (TipoEjecucion tipo : dic_tipo_ejecucion) {
				if (ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
						.getTipo_ambito().getId_tipo_ambito().equals(2)) { // Inversiones
					if (tipo.getId_tipo_ejecucion().equals(6 + fase)) {
						tipoEjecucion = tipo;
					}
				} else if (ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia()
						.getCartera().getTipo_ambito().getId_tipo_ambito().equals(1)) { // Creditos
					if (tipo.getId_tipo_ejecucion().equals(fase)) {
						tipoEjecucion = tipo;
					}
				}
			}

			ejecucionMotor.setTipo_ejecucion(tipoEjecucion);

			ejecucionMotor.setJuego_escenarios_version(ejecucionAgregada.getJuego_escenarios_version());

			// Modelo macro y repricing (solo fase 1)
			if (fase.equals(1)) {
				ejecucionMotor.setEjecucion_modMacro(ejecucionAgregada.getEjecucion_modMacro());
				ejecucionMotor.setTipo_ajusteManual(ejecucionAgregada.getTipo_ajusteManual());

				// Asignamos ejecucion de repricing
				ejecucionMotor.setEjecucion_repricing(this.obtenerEjecucionRepricing(ejecucionAgregada
						.getJuego_escenarios_version().getCartera_version().getMetodologia().getId_metodologia(),
						ejecucionAgregada.getCodmes()));
			}

			// Obtenemos lista de mapeos a nivel de cartera version, incluidos los de
			// buckets y overrides
			stmt_mapeos_carteraVersion = crearStatement(conn);
			rs_carteraVersion = stmt_mapeos_carteraVersion.executeQuery(sql_mapeo_carteraVersion);
			LOGGER.info("Inicio  Bloques parametrizados (con carga) de la cartera versionr");
			// Bloques parametrizados (con carga) de la cartera version
			bloquesParam = new ArrayList<BloqueParametrizado>();
			while (rs_carteraVersion.next()) {
				//LOGGER.info("rs_carteraVersion.next() loop while");
				bloqueParam = new BloqueParametrizado();
				bloqueParam = obtenerBloquesParametrizadoPorMapeo(rs_carteraVersion.getInt("CodMapeoTabla"), conn);

				// bloqueParam.setCartera_version(ejecucionAgregada.getJuego_escenarios_version().getCartera_version());
				bloquesParam.add(bloqueParam);
			}
			ejecucionMotor.setBloquesParam_carteraVersion(bloquesParam);
			LOGGER.info("Fin Bloques parametrizados (con carga) de la cartera version");
			// Completamos cargas de bloques parametrizados con los mapeos de la ejecucion
			for (TablaMapeoEjecucion tablaMapeo : ejecucionAgregada.getMapeo_cargasModMac_ejecucion()
					.getTablas_mapeo_carteraVersion()) {
				for (BloqueParametrizado bloq : ejecucionMotor.getBloquesParam_carteraVersion()) {
					if (bloq.getId_mapeo_tabla().equals(tablaMapeo.getId_mapeo_tabla())) {
						bloq.setCarga(tablaMapeo.getCarga());
					}
				}
			}

			// Completamos cargas de bloques de tipo variacion
			if (esEjectActVar) {
				for (TablaMapeoEjecucion tablaMapeo : this.obtenerTablasMapeoVariacPorIdEjecVar(id_ejec_variacion)) {
					for (BloqueParametrizado bloq : ejecucionMotor.getBloquesParam_carteraVersion()) {
						if (bloq.getId_mapeo_tabla().equals(tablaMapeo.getId_mapeo_tabla())) {
							bloq.setCarga(tablaMapeo.getCarga());
						}
					}
				}
			}

			// Obtenemos lista de mapeos a nivel de escerio version

			// Bloques parametrizados (con carga) de los escenarios version
			LOGGER.info("Inicio  Bloques parametrizados (con carga) de los escenarios version ");
			gruposBloquesEscenario = new ArrayList<GrupoBloquesEscenario>();
			for (MapeoCargasModMacEscVer mapeoCargasEscenario : ejecucionAgregada.getMapeo_cargasModMac_ejecucion()
					.getMapeosCargasModMacEsc()) {
				grupoBloquesEscenario = new GrupoBloquesEscenario();
				grupoBloquesEscenario.setEscenario_version(mapeoCargasEscenario.getEscenario_version());
				grupoBloquesEscenario.setId_ejecucion(mapeoCargasEscenario.getId_ejecucion());

				// Modelo macro (solo fase 1)
				if (fase.equals(1)) {
					grupoBloquesEscenario.setAjustesPdProy(mapeoCargasEscenario.getAjustesPdProy());
				}

				if (esSimulacion) {
					grupoBloquesEscenario.setFlag_ejecutar(mapeoCargasEscenario.getFlag_ejecutar());
				} else {
					grupoBloquesEscenario.setFlag_ejecutar(true);
				}

				stmt_mapeos_escenarioVersion = crearStatement(conn);
				rs_escenarioVersion = stmt_mapeos_escenarioVersion.executeQuery(sql_mapeo_escenarioVersion
						+ mapeoCargasEscenario.getEscenario_version().getId_escenario_version());

				bloquesParam = new ArrayList<BloqueParametrizado>();
				while (rs_escenarioVersion.next()) {
					bloqueParam = new BloqueParametrizado();
					bloqueParam = obtenerBloquesParametrizadoPorMapeo(rs_escenarioVersion.getInt("CodMapeoTabla"),
							conn);

					// bloqueParam.setEscenario_version(mapeoCargasEscenario.getEscenario_version());
					bloquesParam.add(bloqueParam);
				}
				grupoBloquesEscenario.setBloquesParam_escenarioVersion(bloquesParam);
				cerrarResultSet(rs_escenarioVersion);
				cerrarStatement(stmt_mapeos_escenarioVersion);

				// Completamos cargas de bloques parametrizados con los mapeos de la ejecucion
				for (TablaMapeoEjecucion tablaMapeo : mapeoCargasEscenario.getTablas_mapeo_escenarioVersion()) {
					for (BloqueParametrizado bloq : grupoBloquesEscenario.getBloquesParam_escenarioVersion()) {
						if (bloq.getId_mapeo_tabla().equals(tablaMapeo.getId_mapeo_tabla())) {
							bloq.setCarga(tablaMapeo.getCarga());
						}
					}
				}

				gruposBloquesEscenario.add(grupoBloquesEscenario);
			}
			ejecucionMotor.setGrupos_bloquesParamEscenario(gruposBloquesEscenario);

			// Completamos cargas historicas del bloque principal (solo creditos)
			if (ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Creditos")) {
				ejecucionMotor.setEjecuciones_hist(this.obtenerEjecucionesHistBloqPrincip(ejecucionMotor.getCodmes(),
						/* ejecucionMotor.getJuego_escenarios_version().getId_juego_escenarios() */
						ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia()
								.getId_metodologia(),
						fase));
			}

			/*
			 * for (BloqueParametrizado bloq :
			 * ejecucionMotor.getBloquesParam_carteraVersion()) { if
			 * (bloq.getTipo_bloque().getId_tipo_bloque() == 11) { //Tipo principal (solo
			 * existe uno)
			 * ejecucionMotor.setCargasBloqPrincipal_hist(this.obtenerCargasHistBloqPrincip(
			 * ejecucionMotor.getCodmes(),
			 * ejecucionMotor.getJuego_escenarios_version().getId_juego_escenarios(),
			 * fase)); break; } }
			 */

			// Llenamos informacion de staging (solo fase 1 y creditos)
			if (fase.equals(1) && ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia()
					.getCartera().getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Creditos")) {

				stmt_infoStaging = crearStatement(conn);
				rs_infoStaging = stmt_infoStaging.executeQuery(sql_infoStaging);

				while (rs_infoStaging.next()) {
					ejecucionMotor.setStage_externo(rs_infoStaging.getBoolean("FlgStageExterno"));
					ejecucionMotor.setStage_interno(rs_infoStaging.getBoolean("FlgStageInterno"));
					ejecucionMotor.setStage_sensibilidad(rs_infoStaging.getBoolean("FlgStageSensibilidad"));
					ejecucionMotor.setMeses_sensibilidad(rs_infoStaging.getInt("NumMesesSensibilidad"));

					tablaInput = new TablaInput();
					tablaInput.setId_tablainput(rs_infoStaging.getInt("CodTablaInput"));
					tablaInput.setVersion_metadata(rs_infoStaging.getInt("NumVersionMetadata"));
					tablaInput.setNombre_metadata(rs_infoStaging.getString("NbrMetadata"));

					ejecucionMotor.setTablaInputExpresiones(tablaInput);
				}

				for (Regla_Staging regla : this.obtenerReglasStagingPorCarteraVersion(
						ejecucionMotor.getJuego_escenarios_version().getCartera_version().getId_cartera_version(),
						conn)) {
					ejecucionMotor.agregarReglaStaging(regla.getId_regla(), regla);
				}

				for (Expresion_Staging expresion : this.obtenerExpresionesStagingPorCarteraVersion(
						ejecucionMotor.getJuego_escenarios_version().getCartera_version().getId_cartera_version(),
						conn)) {
					ejecucionMotor.agregarExpresionStaging(expresion.getNombre_expresion(), expresion);
				}

				for (Stage stage : this.obtenerStages(conn)) {
					ejecucionMotor.agregarStageStaging(stage.getId_stage(), stage);
				}
			}
			//LOGGER.info(" FIN de try  método convertirEjecucionPEAMotor");
			return ejecucionMotor;

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al completar la informacion para la ejecucion motor : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_carteraVersion);
			cerrarResultSet(rs_escenarioVersion);
			cerrarResultSet(rs_infoStaging);
			cerrarStatement(stmt_mapeos_carteraVersion);
			cerrarStatement(stmt_mapeos_escenarioVersion);
			cerrarStatement(stmt_infoStaging);
			cerrarConexion(conn);
			//LOGGER.info(" FIN método convertirEjecucionPEAMotor");
		}

	}

	public ArrayList<EjecucionHistorica> obtenerEjecucionesHistBloqPrincip(Integer codmes, /*
																							 * Integer
																							 * id_juego_escenarios,
																							 */
			Integer id_metodologia, Integer fase) throws Exception {
		ArrayList<EjecucionHistorica> ejecucionesHist = new ArrayList<EjecucionHistorica>();
		EjecucionHistorica ejecucionHist = null;
		CargaFichero carga = null;
		Fecha fecha = null;
		TablaInput tablaInput = null;

		ArrayList<MapeoCargasModMacEscVer> mapeos_cargasModMac_esc = null;
		MapeoCargasModMacEscVer mapeo_cargasModMac_esc = null;
		Escenario_version escenario_version = null;
		Escenario escenario = null;

		Connection conn = null;
		Statement stmt_ejecuciones = null;
		Statement stmt_ejecuciones_hist = null;
		ResultSet rs_ejecuciones = null;
		ResultSet rs_ejecuciones_hist = null;
		Statement stmt_cargas = null;
		ResultSet rs_cargas = null;

		String sql_cargas = null;
		String sql_ejecuciones_hist = null;

		String sql_ejecuciones = null;

		/*
		 * if (fase.equals(1)) { sql_ejecuciones =
		 * "select max(E.CodEjecucion) as CodEjecucion, max(CodMes) as CodMes from HE_Ejecucion E "
		 * +
		 * "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
		 * + "left join ME_Escenario ES on EV.CodEscenario = ES.CodEscenario " +
		 * "where E.TipEstadoEjecucion = 2 and E.CodMes < " + codmes // ejecutado y
		 * anterior al codmes + " and E.CodTipoEjecucion = 1" +
		 * " and EV.CodJuegoEscenarios = " + id_juego_escenarios +
		 * " group by E.CodMes, EV.CodJuegoEscenarios" + " order by E.codmes desc"; }
		 * else if (fase.equals(2)) { // Historico de fase 1 igual (incluido el actual)
		 * sql_ejecuciones =
		 * "select max(E.CodEjecucion) as CodEjecucion, max(CodMes) as CodMes from HE_Ejecucion E "
		 * +
		 * "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
		 * + "left join ME_Escenario ES on EV.CodEscenario = ES.CodEscenario " +
		 * "where E.TipEstadoEjecucion = 2 and E.CodMes <= " + codmes // Se incluye el
		 * actual + " and E.CodTipoEjecucion = 1" + " and EV.CodJuegoEscenarios = " +
		 * id_juego_escenarios + " group by E.CodMes, EV.CodJuegoEscenarios" +
		 * " order by E.codmes desc"; }
		 */

		// Update: Ahora buscamos todas las ejecuciones f1 oficiales que tengan misma
		// metodologia
		if (fase.equals(1)) {
			sql_ejecuciones = "select max(E.CodEjecucion) as CodEjecucion, max(CodMes) as CodMes, "
					+ "max(EV.CodJuegoEscenarios) as CodJuegoEscenarios from HE_Ejecucion E "
					+ "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
					+ "left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
					+ "left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
					+ "left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
					+ "left join ME_Escenario ES on EV.CodEscenario = ES.CodEscenario "
					+ "where E.TipEstadoEjecucion = 2 and E.CodMes < " + codmes // ejecutado y anterior al codmes
					+ " and E.CodTipoEjecucion = 1" + " and MC.CodMetodologia = " + id_metodologia
					+ " group by E.CodMes, EV.CodJuegoEscenarios" + " order by E.codmes desc";
		} else if (fase.equals(2)) { // Historico de fase 1 igual (incluido el actual)
			sql_ejecuciones = "select max(E.CodEjecucion) as CodEjecucion, max(CodMes) as CodMes, "
					+ "max(EV.CodJuegoEscenarios) as CodJuegoEscenarios from HE_Ejecucion E "
					+ "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
					+ "left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
					+ "left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
					+ "left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
					+ "left join ME_Escenario ES on EV.CodEscenario = ES.CodEscenario "
					+ "where E.TipEstadoEjecucion = 2 and E.CodMes <= " + codmes // Se incluye el actual
					+ " and E.CodTipoEjecucion = 1" + " and MC.CodMetodologia = " + id_metodologia
					+ " group by E.CodMes, EV.CodJuegoEscenarios" + " order by E.codmes desc";
		}

		try {
			conn = obtenerConexion();

			stmt_ejecuciones = crearStatement(conn);
			rs_ejecuciones = stmt_ejecuciones.executeQuery(sql_ejecuciones);

			while (rs_ejecuciones.next()) {
				ejecucionHist = new EjecucionHistorica();

				sql_ejecuciones_hist = "select E.CodEjecucion, EV.CodEscenarioVersion, ES.CodEscenario, ES.NbrEscenario, ES.FlgObligatorio, ES.NbrVariableMotor as escMotor "
						+ "from HE_Ejecucion E "
						+ "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
						+ "left join ME_Escenario ES on EV.CodEscenario = ES.CodEscenario " + "where E.CodMes = "
						+ rs_ejecuciones.getInt("CodMes") + " and E.CodTipoEjecucion = 1"
						+ " and EV.CodJuegoEscenarios = "
						+ rs_ejecuciones.getInt("CodJuegoEscenarios"); /* id_juego_escenarios; */

				stmt_ejecuciones_hist = crearStatement(conn);
				rs_ejecuciones_hist = stmt_ejecuciones_hist.executeQuery(sql_ejecuciones_hist);

				mapeos_cargasModMac_esc = new ArrayList<MapeoCargasModMacEscVer>();
				while (rs_ejecuciones_hist.next()) {
					mapeo_cargasModMac_esc = new MapeoCargasModMacEscVer();
					mapeo_cargasModMac_esc.setId_ejecucion(rs_ejecuciones_hist.getInt("CodEjecucion"));

					escenario_version = new Escenario_version();
					escenario_version.setId_escenario_version(rs_ejecuciones_hist.getInt("CodEscenarioVersion"));

					escenario = new Escenario();
					escenario.setId_escenario(rs_ejecuciones_hist.getInt("CodEscenario"));
					escenario.setNombre_escenario(rs_ejecuciones_hist.getString("NbrEscenario"));
					escenario.setNombre_variable_motor(rs_ejecuciones_hist.getString("escMotor"));
					escenario.setFlag_obligatorio(rs_ejecuciones_hist.getBoolean("FlgObligatorio"));
					escenario_version.setEscenario(escenario);
					mapeo_cargasModMac_esc.setEscenario_version(escenario_version);

					mapeos_cargasModMac_esc.add(mapeo_cargasModMac_esc);
				}
				ejecucionHist.setEjecuciones_escVer(mapeos_cargasModMac_esc);
				cerrarResultSet(rs_ejecuciones_hist);
				cerrarStatement(stmt_ejecuciones_hist);

				sql_cargas = "select TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata,"
						+ " A.CodAprovisionamiento, A.NbrFichero,"
						+ " CF.CodCargaFichero, CF.NbrCarga, CF.NumOrdenInicial, CF.NumOrdenFinal, CF.DesComentarioCalidad, "
						+ " CF.NumVersionCarga, F.CodFecha, F.NumCodMes, F.NumFase" + " from HE_MapeoEjecucion ME"
						+ " left join HE_MapeoTablaBloque MTB on ME.CodMapeoTabla = MTB.CodMapeoTabla "
						+ " left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
						+ " left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque "
						+ " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput"
						+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
						+ " left join HE_CargaFichero CF on CF.CodCargaFichero = ME.CodCargaFichero"
						+ " left join HE_Fecha F on CF.CodFecha = F.CodFecha" + " where ME.CodEjecucion = "
						+ "(select HE_Ejecucion.CodEjecucion from HE_Ejecucion " + " left join HE_EscenarioVersion "
						+ " on HE_Ejecucion.CodEscenarioVersion = HE_EscenarioVersion.CodEscenarioVersion "
						+ " where HE_Ejecucion.CodMes = " + rs_ejecuciones.getInt("CodMes")
						+ " and HE_EscenarioVersion.CodJuegoEscenarios = "
						+ rs_ejecuciones.getInt("CodJuegoEscenarios") /* id_juego_escenarios */
						+ " and HE_Ejecucion.CodTipoEjecucion = 1" + " and ROWNUM = 1)" // Limitamos
						// a
						// una
						// ejecucion
						// cualquiera
						+ " and MTB.CodEscenarioVersion is null and MTB.CodCarteraVersion = "
						+ " (select CodCarteraVersion from HE_JuegoEscenariosVersion where CodJuegoEscenarios = "
						+ rs_ejecuciones.getInt("CodJuegoEscenarios") /* id_juego_escenarios */ + ")"
						+ " and TB.CodTipoBloque = 11"; // Tipo principal

				stmt_cargas = crearStatement(conn);
				rs_cargas = stmt_cargas.executeQuery(sql_cargas);

				while (rs_cargas.next()) {
					carga = new CargaFichero();
					carga.setId_cargafichero(rs_cargas.getInt("CodCargaFichero"));
					carga.setVersion_carga(rs_cargas.getInt("NumVersionCarga"));
					carga.setNombre_carga(rs_cargas.getString("NbrCarga"));
					carga.setOrden_inicial(rs_cargas.getInt("NumOrdenInicial"));
					carga.setOrden_final(rs_cargas.getInt("NumOrdenFinal"));
					carga.setComentario_calidad(rs_cargas.getString("DesComentarioCalidad"));

					fecha = new Fecha();
					fecha.setId_fecha(rs_cargas.getInt("CodFecha"));
					fecha.setCodmes(rs_cargas.getInt("NumCodMes"));
					fecha.setFase(rs_cargas.getInt("NumFase"));
					carga.setFecha(fecha);

					tablaInput = new TablaInput();
					tablaInput.setId_tablainput(rs_cargas.getInt("CodTablaInput"));
					tablaInput.setVersion_metadata(rs_cargas.getInt("NumVersionMetadata"));
					tablaInput.setNombre_metadata(rs_cargas.getString("NbrMetadata"));
					tablaInput.setId_aprovisionamiento(rs_cargas.getInt("CodAprovisionamiento"));
					tablaInput.setNombre_fichero(rs_cargas.getString("NbrFichero"));
					carga.setTablaInput(tablaInput);
				}
				ejecucionHist.setCargaBloqPrincipal_hist(carga);
				cerrarResultSet(rs_cargas);
				cerrarStatement(stmt_cargas);

				ejecucionesHist.add(ejecucionHist);
			}

		} catch (Exception ex) {
			LOGGER.error(
					"Error al obtener las ejecuciones historicas con carga de bloque principal : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_ejecuciones);
			cerrarResultSet(rs_ejecuciones_hist);
			cerrarResultSet(rs_cargas);
			cerrarStatement(stmt_ejecuciones);
			cerrarStatement(stmt_ejecuciones_hist);
			cerrarStatement(stmt_cargas);
		}
		return ejecucionesHist;
	}

	public ArrayList<MapeoCargasModMacEscVer> obtenerEjecucionesOficialEscVerFase1(Integer codmes, Integer id_cartera)
			throws Exception {
		ArrayList<MapeoCargasModMacEscVer> ejecucionesEscVer = new ArrayList<MapeoCargasModMacEscVer>();
		MapeoCargasModMacEscVer ejecEscVer = null;
		Escenario_version escenario_version = null;
		Escenario escenario = null;

		Connection conn = null;
		Statement stmt_ejecuciones = null;
		ResultSet rs_ejecuciones = null;

		String sql_ejecuciones = "select E.CodEjecucion, EV.CodEscenarioVersion, ES.CodEscenario, ES.NbrEscenario, "
				+ "ES.FlgObligatorio, ES.NbrVariableMotor as escMotor " + "from HE_Ejecucion E "
				+ "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
				+ "left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
				+ "left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
				+ "left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ "left join ME_Escenario ES on EV.CodEscenario = ES.CodEscenario " + "where E.CodMes = " + codmes
				+ " and E.CodTipoEjecucion = 1 and E.TipEstadoEjecucion in (2,4) and E.TipEstadoValidacion = 4 "
				+ " and MC.CodCartera = " + id_cartera;

		try {
			conn = obtenerConexion();
			stmt_ejecuciones = crearStatement(conn);
			rs_ejecuciones = stmt_ejecuciones.executeQuery(sql_ejecuciones);

			while (rs_ejecuciones.next()) {
				ejecEscVer = new MapeoCargasModMacEscVer();
				ejecEscVer.setId_ejecucion(rs_ejecuciones.getInt("CodEjecucion"));

				escenario_version = new Escenario_version();
				escenario_version.setId_escenario_version(rs_ejecuciones.getInt("CodEscenarioVersion"));

				escenario = new Escenario();
				escenario.setId_escenario(rs_ejecuciones.getInt("CodEscenario"));
				escenario.setNombre_escenario(rs_ejecuciones.getString("NbrEscenario"));
				escenario.setNombre_variable_motor(rs_ejecuciones.getString("escMotor"));
				escenario.setFlag_obligatorio(rs_ejecuciones.getBoolean("FlgObligatorio"));
				escenario_version.setEscenario(escenario);
				ejecEscVer.setEscenario_version(escenario_version);

				ejecucionesEscVer.add(ejecEscVer);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener las ejecuciones por escenario version correspondiente de fase 1 : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_ejecuciones);
			cerrarStatement(stmt_ejecuciones);
			cerrarConexion(conn);
		}
		return ejecucionesEscVer;
	}

	public ArrayList<MapeoCargasModMacEscVer> obtenerEjecucionesOficialEscVerFase2(Integer codmes, Integer id_cartera)
			throws Exception {
		ArrayList<MapeoCargasModMacEscVer> ejecucionesEscVer = new ArrayList<MapeoCargasModMacEscVer>();
		MapeoCargasModMacEscVer ejecEscVer = null;
		Escenario_version escenario_version = null;
		Escenario escenario = null;

		Connection conn = null;
		Statement stmt_ejecuciones = null;
		ResultSet rs_ejecuciones = null;

		String sql_ejecuciones = "select E.CodEjecucion, EV.CodEscenarioVersion, ES.CodEscenario, ES.NbrEscenario, "
				+ "ES.FlgObligatorio, ES.NbrVariableMotor as escMotor " + "from HE_Ejecucion E "
				+ "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
				+ "left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
				+ "left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
				+ "left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ "left join ME_Escenario ES on EV.CodEscenario = ES.CodEscenario " + "where E.CodMes = " + codmes
				+ " and E.CodTipoEjecucion = 2 and E.TipEstadoEjecucion in (2,4) and E.TipEstadoValidacion = 4 "
				+ " and MC.CodCartera = " + id_cartera;

		try {
			conn = obtenerConexion();
			stmt_ejecuciones = crearStatement(conn);
			rs_ejecuciones = stmt_ejecuciones.executeQuery(sql_ejecuciones);

			while (rs_ejecuciones.next()) {
				ejecEscVer = new MapeoCargasModMacEscVer();
				ejecEscVer.setId_ejecucion(rs_ejecuciones.getInt("CodEjecucion"));

				escenario_version = new Escenario_version();
				escenario_version.setId_escenario_version(rs_ejecuciones.getInt("CodEscenarioVersion"));

				escenario = new Escenario();
				escenario.setId_escenario(rs_ejecuciones.getInt("CodEscenario"));
				escenario.setNombre_escenario(rs_ejecuciones.getString("NbrEscenario"));
				escenario.setNombre_variable_motor(rs_ejecuciones.getString("escMotor"));
				escenario.setFlag_obligatorio(rs_ejecuciones.getBoolean("FlgObligatorio"));
				escenario_version.setEscenario(escenario);
				ejecEscVer.setEscenario_version(escenario_version);

				ejecucionesEscVer.add(ejecEscVer);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener las ejecuciones por escenario version correspondiente de fase 2 : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_ejecuciones);
			cerrarStatement(stmt_ejecuciones);
			cerrarConexion(conn);
		}
		return ejecucionesEscVer;
	}

	public EjecucionPrimerPaso obtenerEjecucionRepricing(Integer id_metodologia, Integer codmes) throws Exception {
		EjecucionPrimerPaso ejecucionPrimerPaso = new EjecucionPrimerPaso();
		PrimerPaso primerPaso = null;
		EstructuraMetodologica metodologia = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select EJ.CodEjecucion, EJ.CodMes, EJ.TipEstadoEjecucion, EJ.NumEjecucionAvance, EJ.FecEjecucion, "
				+ " PP.CodPrimerPaso, PP.NbrPrimerPaso, PP.NbrVariableMotor as NbrVariableMotorPrimerPaso, "
				+ " PP.DesPrimerPaso, PP.NumPeriodMeses, MPPM.NbrCodigoPrimerPaso, "
				+ " MC.CodMetodologia, MC.NbrMetodologia, MC.NbrCodigoEstructura" + " from HE_Ejecucion EJ"
				+ " left join HE_PrimerPaso PP on EJ.CodPrimerPaso = PP.CodPrimerPaso"
				+ " left join HE_MetodologiaCartera MC on EJ.CodMetodologia = MC.CodMetodologia"
				+ " left join HE_MapeoPrimerPasoMet MPPM "
				+ " on PP.CodPrimerPaso = MPPM.CodPrimerPaso and MC.CodMetodologia = MPPM.CodMetodologia "
				+ " where EJ.CodTipoEjecucion = 4 and PP.CodPrimerPaso = 2" // Tipo: Primer Paso y repricing
				+ " and EJ.codmes = " + codmes + " and MC.CodMetodologia = " + id_metodologia
				+ " order by EJ.CodMes desc";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ejecucionPrimerPaso = new EjecucionPrimerPaso();
				ejecucionPrimerPaso.setId_ejecucion(rs.getInt("CodEjecucion"));
				ejecucionPrimerPaso.setCodmes(rs.getInt("CodMes"));
				ejecucionPrimerPaso.setEstado_ejecucion(rs.getInt("TipEstadoEjecucion"));
				ejecucionPrimerPaso.setPorcentaje_avance(rs.getInt("NumEjecucionAvance"));
				ejecucionPrimerPaso.setDatetime_ejecucion(rs.getLong("FecEjecucion"));

				primerPaso = new PrimerPaso();
				primerPaso.setId_primerPaso(rs.getInt("CodPrimerPaso"));
				primerPaso.setNombre_primerPaso(rs.getString("NbrPrimerPaso"));
				primerPaso.setNombre_variablemotor(rs.getString("NbrVariableMotorPrimerPaso"));
				primerPaso.setDescripcion_primerPaso(rs.getString("DesPrimerPaso"));
				primerPaso.setCodigo_primerPaso(rs.getString("NbrCodigoPrimerPaso"));
				ejecucionPrimerPaso.setPrimerPaso(primerPaso);

				metodologia = new EstructuraMetodologica();
				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
				metodologia.setCodigo_estructura(rs.getString("NbrCodigoEstructura"));

				ejecucionPrimerPaso.setMetodologia(metodologia);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener ejecucion de repricing : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return ejecucionPrimerPaso;
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesOficialesExtrapolacion(ArrayList<Cartera> carterasExtrapol,
			Integer codmes) throws Exception {
		ArrayList<EjecucionAgregadaPE> ejecucionesExtrapol = new ArrayList<EjecucionAgregadaPE>();

		for (Cartera carteraExt : carterasExtrapol) {
			ejecucionesExtrapol
					.add(this.obtenerEjecucionAgregadaOficialPorCarteraMes(carteraExt.getId_cartera(), codmes, 1));
		}
		return ejecucionesExtrapol;
	}

	public EjecucionAgregadaPE obtenerEjecucionAgregadaOficialPorCarteraMes(Integer id_cartera, Integer codmes,
			Integer fase) throws Exception {
		EjecucionAgregadaPE ejecucionAgregada = null;
		JuegoEscenarios_version juegoEscenarios_version = null;
		Cartera_version cartera_version = null;
		EstructuraMetodologica metodologia = null;
		Boolean sinResultado = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;

		Statement stmt_tipAjusteModMac = null;
		ResultSet rs_tipAjusteModMac = null;
		String sql_tipAjusteModMac = null;

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);

			sql = "select E.CodMes, JEV.CodJuegoEscenarios, " + " min(E.TipEstadoEjecucion) as EstadoEjecucionMin,"
					+ " max(E.FecEjecucion) as FecEjecucion," + " max(JEV.NbrJuegoEscenarios) as NbrJuegoEscenarios, "
					+ " max(JEV.DesJuegoEscenarios) as DesJuegoEscenarios, "
					+ " max(CV.CodCarteraVersion) as CodCarteraVersion," + " max(CV.NbrVersion) as NbrVersion, "
					+ " max(MC.CodMetodologia) as CodMetodologia," + " max(MC.NbrMetodologia) as NbrMetodologia, "
					+ " max(MC.NbrCodigoEstructura) as NbrCodigoEstructura, max(MC.NumBuckets) as NumBuckets,"
					+ " max(MC.NumLifeTime) as NumLifeTime, max(MC.NumMaxPlazo) as NumMaxPlazo, max(MC.FlgLgdUnica) as FlgLgdUnica "
					+ " from HE_Ejecucion E "
					+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion"
					+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
					+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
					+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
					+ " where E.CodTipoEjecucion = " + fase + " and E.TipEstadoValidacion = 4 " // Oficial
					+ " and E.codmes = " + codmes + " and MC.CodCartera = " + id_cartera + " and ROWNUM = 1 "
					+ " group by E.codmes, JEV.CodJuegoEscenarios" + " order by E.codmes desc";

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ejecucionAgregada = new EjecucionAgregadaPE();
				juegoEscenarios_version = new JuegoEscenarios_version();
				cartera_version = new Cartera_version();
				metodologia = new EstructuraMetodologica();

				ejecucionAgregada.setCodmes(rs.getInt("CodMes"));
				ejecucionAgregada.setEstado_ejecucion(rs.getInt("EstadoEjecucionMin"));
				ejecucionAgregada.setDatetime_ejecucion(rs.getLong("FecEjecucion"));

				juegoEscenarios_version.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenarios_version.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenarios_version.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));
				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));

				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
				metodologia.setCodigo_estructura(rs.getString("NbrCodigoEstructura"));

				cartera_version.setMetodologia(metodologia);

				juegoEscenarios_version.setCartera_version(cartera_version);

				ejecucionAgregada.setJuego_escenarios_version(juegoEscenarios_version);

				// Obtenemos tipo de ajuste manual para modelo macro
				sql_tipAjusteModMac = "select MMM.TipAjusteManual from HE_MapeoModMacro MMM "
						+ "left join HE_Ejecucion E_PE on MMM.CodEjecucionPE = E_PE.CodEjecucion "
						+ "left join HE_EscenarioVersion EV on E_PE.CodEscenarioVersion = EV.CodEscenarioVersion "
						+ "where ROWNUM=1 and E_PE.CodTipoEjecucion = " + fase + " and E_PE.CodMes = "
						+ ejecucionAgregada.getCodmes() + " and EV.CodJuegoEscenarios = "
						+ ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios();

				stmt_tipAjusteModMac = crearStatement(conn);
				rs_tipAjusteModMac = stmt_tipAjusteModMac.executeQuery(sql_tipAjusteModMac);

				sinResultado = true;
				while (rs_tipAjusteModMac.next()) {
					sinResultado = false;
					ejecucionAgregada.setTipo_ajusteManual(rs_tipAjusteModMac.getInt("TipAjusteManual"));
				}
				cerrarResultSet(rs_tipAjusteModMac);
				cerrarStatement(stmt_tipAjusteModMac);

				if (sinResultado) {
					ejecucionAgregada.setTipo_ajusteManual(-1); // No requiere mod macro
				}

				// Obtenemos los mapeos de la ejecucion agregada
				ejecucionAgregada.setMapeo_cargasModMac_ejecucion(this.obtenerMapeoCargasModMacEjecucion(codmes,
						ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios(),
						ejecucionAgregada.getTipo_ajusteManual(), -1, fase, false));
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener una ejecucion agregada oficial por cartera y codmes: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarResultSet(rs_tipAjusteModMac);
			cerrarStatement(stmt);
			cerrarStatement(stmt_tipAjusteModMac);
			cerrarConexion(conn);
		}
		return ejecucionAgregada;
	}

	public EjecucionPrimerPasoMotor convertirEjecucionPrimerPasoAMotor(EjecucionPrimerPaso ejecucion) throws Exception {
		ArrayList<BloqueParametrizado> bloquesParam = null;
		BloqueParametrizado bloqueParam = null;

		Connection conn = null;
		Statement stmt_mapeos = null;
		ResultSet rs_mapeos = null;

		String sql_mapeo = "select MTB.CodMapeoTabla from HE_MapeoTablaBloque MTB"
				+ " left join HE_Bloque B on MTB.CodBloque = B.CodBloque"
				+ " left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque"
				+ " where MTB.CodCarteraVersion is null and MTB.CodEscenarioVersion is null and MTB.FlgPrimerPaso = 1 "
				+ " and TB.CodPrimerPaso = " + ejecucion.getPrimerPaso().getId_primerPaso()
				+ " and MTB.CodMetodologia = " + ejecucion.getMetodologia().getId_metodologia();
		// No esta versionado por cartera/escenario version y tiene flag de primer paso

		try {
			conn = obtenerConexion();

			// De manera previa, borramos todos los resultados (en caso existan ejecuciones)
			this.borrarResultadosPrimerPaso(conn, ejecucion.getId_ejecucion());

			EjecucionPrimerPasoMotor ejecucionMotor = new EjecucionPrimerPasoMotor();

			// Completamos mapeos de ejecucion
			ejecucion
					.setTablas_mapeo_ejecucion(this.obtenerTablasMapeoEjecucionPrimerPaso(ejecucion.getId_ejecucion()));

			// Parseo de los identificadores
			ejecucionMotor.setCodmes(ejecucion.getCodmes());
			ejecucionMotor.setId_ejecucion(ejecucion.getId_ejecucion());
			ejecucionMotor.setMetodologia(ejecucion.getMetodologia());
			ejecucionMotor.setPrimerPaso(ejecucion.getPrimerPaso());

			// Obtenemos lista de mapeos
			stmt_mapeos = crearStatement(conn);
			rs_mapeos = stmt_mapeos.executeQuery(sql_mapeo);

			// Bloques parametrizados (con carga) de la cartera version
			bloquesParam = new ArrayList<BloqueParametrizado>();
			while (rs_mapeos.next()) {
				bloqueParam = new BloqueParametrizado();
				bloqueParam = obtenerBloquesParametrizadoPorMapeo(rs_mapeos.getInt("CodMapeoTabla"), conn);
				bloquesParam.add(bloqueParam);
			}
			ejecucionMotor.setBloquesParam(bloquesParam);

			// Completamos cargas de bloques parametrizados con los mapeos de la ejecucion
			for (TablaMapeoEjecucion tablaMapeo : ejecucion.getTablas_mapeo_ejecucion()) {
				for (BloqueParametrizado bloq : ejecucionMotor.getBloquesParam()) {
					if (bloq.getId_mapeo_tabla().equals(tablaMapeo.getId_mapeo_tabla())) {
						bloq.setCarga(tablaMapeo.getCarga());
					}
				}
			}

			return ejecucionMotor;

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error al completar la informacion para la ejecucion motor de primer paso : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_mapeos);
			cerrarStatement(stmt_mapeos);
			cerrarConexion(conn);
		}
	}

	public EjecucionMotorOtro convertirEjecucionOtroAMotor(EjecucionOtro ejecucion) throws Exception {
		ArrayList<BloqueParametrizado> bloquesParam = null;
		BloqueParametrizado bloqueParam = null;

		Connection conn = null;
		Statement stmt_mapeos = null;
		ResultSet rs_mapeos = null;

		String sql_mapeo = "select MTB.CodMapeoTabla from HE_MapeoTablaBloque MTB"
				+ " left join HE_Bloque B on MTB.CodBloque = B.CodBloque"
				+ " left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque"
				+ " where MTB.CodEscenarioVersion is null " + " and MTB.CodCarteraVersion = "
				+ ejecucion.getCartera_version().getId_cartera_version();

		try {
			conn = obtenerConexion();

			EjecucionMotorOtro ejecucionMotor = new EjecucionMotorOtro();

			// Completamos mapeos de ejecucion
			ejecucion.setTablas_mapeo_ejecucion(this.obtenerTablasMapeoEjecucionOtros(ejecucion.getId_ejecucion()));

			// Parseo de los identificadores
			ejecucionMotor.setCodmes(ejecucion.getCodmes());
			ejecucionMotor.setId_ejecucion(ejecucion.getId_ejecucion());
			ejecucionMotor.setCartera_version(ejecucion.getCartera_version());

			// Obtenemos lista de mapeos
			stmt_mapeos = crearStatement(conn);
			rs_mapeos = stmt_mapeos.executeQuery(sql_mapeo);

			// Bloques parametrizados (con carga) de la cartera version
			bloquesParam = new ArrayList<BloqueParametrizado>();
			while (rs_mapeos.next()) {
				bloqueParam = new BloqueParametrizado();
				bloqueParam = obtenerBloquesParametrizadoPorMapeo(rs_mapeos.getInt("CodMapeoTabla"), conn);
				bloquesParam.add(bloqueParam);
			}
			ejecucionMotor.setBloquesParam(bloquesParam);

			// Completamos cargas de bloques parametrizados con los mapeos de la ejecucion
			for (TablaMapeoEjecucion tablaMapeo : ejecucion.getTablas_mapeo_ejecucion()) {
				for (BloqueParametrizado bloq : ejecucionMotor.getBloquesParam()) {
					if (bloq.getId_mapeo_tabla().equals(tablaMapeo.getId_mapeo_tabla())) {
						bloq.setCarga(tablaMapeo.getCarga());
					}
				}
			}

			return ejecucionMotor;

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al completar la informacion para la ejecucion motor para calculo de tipo otros : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_mapeos);
			cerrarStatement(stmt_mapeos);
			cerrarConexion(conn);
		}
	}

	public ArrayList<AjusteGasto> obtenerAjustesGasto() throws Exception {
		ArrayList<AjusteGasto> ajustesGasto = new ArrayList<AjusteGasto>();
		AjusteGasto ajusteGasto = new AjusteGasto();
		Cartera cartera = null;
		Entidad entidad = null;
		Fecha fecha = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT ag.*, C.nbrcartera, F.numcodmes, F.numfase, e.codentidad\r\n"
				+ "FROM he_ajustegasto ag\r\n" + "LEFT JOIN he_cartera C ON C.codcartera = ag.codcartera\r\n"
				+ "LEFT JOIN he_entidad E ON E.codentidad = C.codentidad\r\n"
				+ "LEFT JOIN he_fecha F ON F.codfecha = ag.codfecha";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ajusteGasto = new AjusteGasto();
				cartera = new Cartera();
				entidad = new Entidad();
				fecha = new Fecha();
				fecha.setId_fecha(rs.getInt("CODFECHA"));
				fecha.setCodmes(rs.getInt("NUMCODMES"));
				fecha.setFase(rs.getInt("NUMFASE"));
				ajusteGasto.setFecha(fecha);
				cartera.setNombre_cartera(rs.getString("Nbrcartera"));
				cartera.setId_cartera(rs.getInt("CODCARTERA"));
				entidad.setId_entidad(rs.getInt("codentidad"));
				cartera.setEntidad(entidad);
				ajusteGasto.setCartera(cartera);
				ajusteGasto.setId_ajusteGasto(rs.getInt("CODAJUSTEGASTO"));
				ajusteGasto.setTipo_ajuste(rs.getString("TIPESTADOAJUSTE"));
				ajusteGasto.setDetalle_ajuste(rs.getString("DESDETALLEAJUSTE"));
				ajusteGasto.setProv_NIF9(rs.getDouble("NUMPROVNIF9"));
				ajusteGasto.setProv_local(rs.getDouble("NUMPROVLOCAL"));
				ajusteGasto.setComentario(rs.getString("DESCOMENTARIO"));
				ajustesGasto.add(ajusteGasto);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los ajustes Gasto: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return ajustesGasto;
	}

	public void crearAjusteGasto(AjusteGasto ajusteGasto) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "INSERT INTO ME_AjusteGasto(CodAjusteGasto,CodCartera,CodFecha,TipEstadoAjuste,DesDetalleAjuste,NumProvNIF9,NumProvLocal,DesComentario,FecActualizacionTabla) "
				+ "VALUES (?,?,?,?,?,?,?,?,?)";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			ajusteGasto.setId_ajusteGasto(obtenerSiguienteId(""));
			pstmt.setInt(1, ajusteGasto.getId_ajusteGasto());
			pstmt.setInt(2, ajusteGasto.getCartera().getId_cartera());
			pstmt.setInt(3, ajusteGasto.getFecha().getId_fecha());
			pstmt.setString(4, ajusteGasto.getTipo_ajuste());
			pstmt.setString(5, ajusteGasto.getDetalle_ajuste());
			pstmt.setDouble(6, ajusteGasto.getProv_NIF9());
			pstmt.setDouble(7, ajusteGasto.getProv_local());
			pstmt.setString(8, ajusteGasto.getComentario());
			pstmt.setDate(9, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar el ajuste gasto: " + ajusteGasto.getDetalle_ajuste() + ". Error: "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void editarAjusteGasto(AjusteGasto ajusteGasto) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "UPDATE ME_AjusteGasto Set CodCartera =?, CodFecha=?, TipEstadoAjuste=?, "
				+ "DesDetalleAjuste=?, NumProvNIF9=?, NumProvLocal=?, DesComentario=?, FecActualizacionTabla=? where CodAjusteGasto=?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, ajusteGasto.getCartera().getId_cartera());
			pstmt.setInt(2, ajusteGasto.getFecha().getId_fecha());
			pstmt.setString(3, ajusteGasto.getTipo_ajuste());
			pstmt.setString(4, ajusteGasto.getDetalle_ajuste());
			pstmt.setDouble(5, ajusteGasto.getProv_NIF9());
			pstmt.setDouble(6, ajusteGasto.getProv_local());
			pstmt.setString(7, ajusteGasto.getComentario());
			pstmt.setDate(8, new Date(System.currentTimeMillis()));
			pstmt.setInt(9, ajusteGasto.getId_ajusteGasto());
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar el ajuste gasto: " + ajusteGasto.getDetalle_ajuste() + ". Error: "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void borrarAjusteGasto(Integer id_ajusteGasto) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Delete from HE_AJUSTEGASTO where CODAJUSTEGASTO= ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_ajusteGasto);
			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar ruta con id: " + id_ajusteGasto + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public BloqueParametrizado obtenerBloquesParametrizadoPorMapeo(Integer id_mapeo_tabla, Connection conn)
			throws Exception {
		PreparedStatement pstmt_bloqueParam = null;
		PreparedStatement pstmt_camposParam = null;
		ResultSet rs_bloqueParam = null;
		ResultSet rs_camposParam = null;

		BloqueParametrizado bloqueParam = null;
		TablaInput tablaInput = null;
		FormatoInput formatoInput = null;
		TipoBloque tipoBloque = null;
		TipoEjecucion tipoEjecucion = null;
		TipoFlujo tipoFlujo = null;
		PrimerPaso primerPaso = null;
		TipoObtencionResultados tipoObtenResult = null;

		ArrayList<CampoParametrizado> camposParam = null;
		CampoParametrizado campoParam = null;
		CampoBloque campoBloque = null;
		TipoCampo tipoCampo = null;
		CampoInput campoInput = null;

		String sql_bloqueParam = "select MTB.CodMapeoTabla, MTB.DesFiltroTabla, "
				+ "B.CodBloque, B.NbrBloque, B.NbrVariableMotor, B.FlgTablaInput, "
				+ "FI.CodFormatoInput, FI.NbrFormatoInput, "
				+ "TB.CodTipoBloque, TB.NbrTipoBloque, TB.NbrVariableMotor, TB.FlgBucket, "
				+ "TE.CodTipoEjecucion, TE.NbrTipoEjecucion, " + "TF.CodTipoFlujo, TF.NbrTipoFlujo, "
				+ "PP.CodPrimerPaso, PP.NbrPrimerPaso, PP.NbrVariableMotor, PP.DesPrimerPaso, PP.NumPeriodMeses, "
				+ "TOR.CodTipoObtenResult, TOR.NbrTipoObtenResult, "
				+ "TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata, TI.FlgNormalizada, A.NbrFichero "
				+ "from HE_MapeoTablaBloque MTB "
				+ "left join HE_TablaInput TI on MTB.CodTablaInput = TI.CodTablaInput "
				+ "left join HE_Aprovisionamiento A on TI.CodAprovisionamiento = A.CodAprovisionamiento "
				+ "left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
				+ "left join ME_FormatoInput FI on B.CodFormatoInput = FI.CodFormatoInput "
				+ "left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque "
				+ "left join ME_TipoEjecucion TE on TB.CodTipoEjecucion = TE.CodTipoEjecucion "
				+ "left join ME_TipoFlujo TF on TE.CodTipoFlujo = TF.CodTipoFlujo "
				+ "left join ME_TipoObtencionResultados TOR on TB.CodTipoObtenResult = TOR.CodTipoObtenResult "
				+ "left join HE_PrimerPaso PP on TB.CodPrimerPaso = PP.CodPrimerPaso " + "where MTB.CodMapeoTabla=?";

		String sql_camposParam = "select RCB.TxtValor, RCB.NumValor, "
				+ "CB.CodCampoBloque, CB.NbrCampo as NbrCampoBloque, CB.NbrVariableMotor, "
				+ "TC.CodTipoCampo, TC.NbrTipoCampo, " + "CI.CodCampo, CI.NbrCampo as NbrCampoInput "
				+ "from HE_RegistroCampoBloque RCB "
				+ "left join HE_CampoBloque CB on RCB.CodCampoBloque = CB.CodCampoBloque "
				+ "left join ME_TipoCampo TC on CB.CodTipoCampo = TC.CodTipoCampo "
				+ "left join HE_CampoInput CI on RCB.CodCampo = CI.CodCampo " + "where RCB.CodMapeoTabla=?"
				+ " order by CB.CodCampoBloque ";

		try {
			pstmt_bloqueParam = conn.prepareStatement(sql_bloqueParam);
			pstmt_bloqueParam.setInt(1, id_mapeo_tabla);
			rs_bloqueParam = pstmt_bloqueParam.executeQuery();
			while (rs_bloqueParam.next()) {
				bloqueParam = new BloqueParametrizado();
				bloqueParam.setId_mapeo_tabla(rs_bloqueParam.getInt("CodMapeoTabla"));
				bloqueParam.setFiltro_tabla(rs_bloqueParam.getString("DesFiltroTabla"));

				bloqueParam.setId_bloque(rs_bloqueParam.getInt("CodBloque"));
				bloqueParam.setNombre_bloque(rs_bloqueParam.getString("NbrBloque"));
				bloqueParam.setVariable_motor(rs_bloqueParam.getString("NbrVariableMotor"));
				bloqueParam.setFlag_tabla_input(rs_bloqueParam.getBoolean("FlgTablaInput"));

				formatoInput = new FormatoInput();
				formatoInput.setId_formato_input(rs_bloqueParam.getInt("CodFormatoInput"));
				formatoInput.setNombre_formato_input(rs_bloqueParam.getString("NbrFormatoInput"));
				bloqueParam.setFormato_input(formatoInput);

				tipoBloque = new TipoBloque();
				tipoBloque.setId_tipo_bloque(rs_bloqueParam.getInt("CodTipoBloque"));
				tipoBloque.setNombre_tipo_bloque(rs_bloqueParam.getString("NbrTipoBloque"));
				tipoBloque.setNombre_variablemotor(rs_bloqueParam.getString("NbrVariableMotor"));
				tipoBloque.setFlag_bucket(rs_bloqueParam.getBoolean("FlgBucket"));

				tipoEjecucion = new TipoEjecucion();
				tipoEjecucion.setId_tipo_ejecucion(rs_bloqueParam.getInt("CodTipoEjecucion"));
				tipoEjecucion.setNombre_tipo_ejecucion(rs_bloqueParam.getString("NbrTipoEjecucion"));

				tipoFlujo = new TipoFlujo();
				tipoFlujo.setId_tipo_flujo(rs_bloqueParam.getInt("CodTipoFlujo"));
				tipoFlujo.setNombre_tipo_flujo(rs_bloqueParam.getString("NbrTipoFlujo"));
				tipoEjecucion.setTipo_flujo(tipoFlujo);
				tipoBloque.setTipo_ejecucion(tipoEjecucion);

				tipoObtenResult = new TipoObtencionResultados();
				tipoObtenResult.setId_tipo_obtencion_resultados(rs_bloqueParam.getInt("CodTipoObtenResult"));
				tipoObtenResult.setNombre_tipo_obtencion_resultados(rs_bloqueParam.getString("NbrTipoObtenResult"));
				tipoBloque.setTipo_obtencion_resultados(tipoObtenResult);

				primerPaso = new PrimerPaso();
				primerPaso.setId_primerPaso(rs_bloqueParam.getInt("CodPrimerPaso"));
				primerPaso.setNombre_primerPaso(rs_bloqueParam.getString("NbrPrimerPaso"));
				primerPaso.setNombre_variablemotor(rs_bloqueParam.getString("NbrVariableMotor"));
				primerPaso.setDescripcion_primerPaso(rs_bloqueParam.getString("DesPrimerPaso"));
				primerPaso.setPeriodicidad_meses(rs_bloqueParam.getInt("NumPeriodMeses"));
				tipoBloque.setPrimerPaso(primerPaso);

				bloqueParam.setTipo_bloque(tipoBloque);

				if (bloqueParam.getFlag_tabla_input()) {
					tablaInput = new TablaInput();
					tablaInput.setId_tablainput(rs_bloqueParam.getInt("CodTablaInput"));
					tablaInput.setNombre_metadata(rs_bloqueParam.getString("NbrMetadata"));
					tablaInput.setVersion_metadata(rs_bloqueParam.getInt("NumVersionMetadata"));
					tablaInput.setNombre_fichero(rs_bloqueParam.getString("NbrFichero"));
					tablaInput.setFlag_normalizada(rs_bloqueParam.getBoolean("FlgNormalizada"));
					bloqueParam.setTabla_input(tablaInput);
				}

				pstmt_camposParam = conn.prepareStatement(sql_camposParam);
				pstmt_camposParam.setInt(1, id_mapeo_tabla);
				rs_camposParam = pstmt_camposParam.executeQuery();

				camposParam = new ArrayList<CampoParametrizado>();
				while (rs_camposParam.next()) {

					campoParam = new CampoParametrizado();
					campoBloque = new CampoBloque();
					campoBloque.setId_campo(rs_camposParam.getInt("CodCampoBloque"));
					campoBloque.setNombre_campo(rs_camposParam.getString("NbrCampoBloque"));
					campoBloque.setVariable_motor(rs_camposParam.getString("NbrVariableMotor"));

					tipoCampo = new TipoCampo();
					tipoCampo.setId_tipo_campo(rs_camposParam.getInt("CodTipoCampo"));
					tipoCampo.setNombre_tipo_campo(rs_camposParam.getString("NbrTipoCampo"));
					campoBloque.setTipo_campo(tipoCampo);

					campoParam.setCampo_bloque(campoBloque);

					if (bloqueParam.getFlag_tabla_input()) {
						campoInput = new CampoInput();
						campoInput.setId_campo(rs_camposParam.getInt("CodCampo"));
						campoInput.setNombre_campo(rs_camposParam.getString("NbrCampoInput"));

						campoParam.setCampo_input(campoInput);

					} else {
						campoParam.setNumValor(rs_camposParam.getDouble("NumValor"));
						campoParam.setTxtValor(rs_camposParam.getString("TxtValor"));
					}
					camposParam.add(campoParam);
				}
				bloqueParam.setCampos_parametrizado(camposParam);
				cerrarResultSet(rs_camposParam);
				cerrarPreparedStatement(pstmt_camposParam);
			}

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al obtener el bloque parametrizado para el mapeo con id " + id_mapeo_tabla + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_bloqueParam);
			cerrarResultSet(rs_camposParam);
			cerrarPreparedStatement(pstmt_bloqueParam);
			cerrarPreparedStatement(pstmt_camposParam);
		}

		return bloqueParam;
	}

	// -------------------------------------------------------------------
	// ---------------------SIMULACIONES PE AGREGADAS----------------------
	// -------------------------------------------------------------------

	public ArrayList<EjecucionAgregadaPE> obtenerSimulacionesAgregadaPE(Boolean incluyeF2) throws Exception {
		ArrayList<EjecucionAgregadaPE> ejecucionesAgregada = new ArrayList<EjecucionAgregadaPE>();
		EjecucionAgregadaPE ejecucionAgregada = null;
		JuegoEscenarios_version juegoEscenarios_version = null;
		Cartera_version cartera_version = null;
		EstructuraMetodologica metodologia = null;
		MotorCalculo motor = null;
		TipoFlujo tipoFlujo = null;
		TipoObtencionResultados tipoObtenResult = null;
		TipoMetodologia tipoMetodologia = null;
		TipoPeriodicidad tipoPeriodicidad = null;
		Cartera cartera = null;
		TipoAmbito tipoAmbito = null;
		TipoMagnitud tipoMagnitud = null;
		TipoCartera tipoCartera = null;
		Entidad entidad = null;

		EjecucionPrimerPaso ejecucionModMacro = null;
		PrimerPaso primerPaso = null;
		Boolean sinResultado = null;

		EjecucionPrimerPaso ejecucionRepricing = null;
		PrimerPaso primerPasoRep = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		Statement stmt_ejecModMacro = null;
		ResultSet rs_ejecModMacro = null;

		String sql = " select E.CodMes, JEV.CodJuegoEscenarios, " + (incluyeF2 ? " E.CodTipoEjecucion, " : "")
				+ " min(E.TipEstadoValidacion) as EstadoValidacionMin,"
				+ " min(E.TipEstadoEjecucion) as EstadoEjecucionMin, avg(E.NumEjecucionAvance) as NumEjecucionAvanceProm, "
				// + " xmlagg(xmlelement(e, esc.nbrescenario || ': \n' || e.deslogmotor,
				// '\n').extract('//text()')).getclobval() AS deslogmotor,"
				+ " max(E.FecEjecucion) as FecEjecucion," + " max(JEV.NbrJuegoEscenarios) as NbrJuegoEscenarios, "
				+ " max(JEV.DesJuegoEscenarios) as DesJuegoEscenarios, "
				+ " max(CV.CodCarteraVersion) as CodCarteraVersion," + " max(CV.NbrVersion) as NbrVersion, "
				+ " max(MC.CodMetodologia) as CodMetodologia," + " max(MC.NbrMetodologia) as NbrMetodologia, "
				+ " max(MC.NbrCodigoEstructura) as NbrCodigoEstructura, max(MC.NbrFuncPlumbr) as NbrFuncPlumbr, "
				+ " max(MC.NumPuertoPlumbr) as NumPuertoPlumbr, max(MC.NumBuckets) as NumBuckets,"
				+ " max(MC.NumLifeTime) as NumLifeTime, max(MC.NumMaxPlazo) as NumMaxPlazo, max(MC.FlgLgdUnica) as FlgLgdUnica,"
				+ " max(MOC.CodMotor) as CodMotor, max(MOC.NbrMotor) as NbrMotor, "
				+ " max(TF.CodTipoFlujo) as CodTipoFlujo, max(TF.NbrTipoFlujo) as NbrTipoFlujo,"
				+ " max(TOR.CodTipoObtenResult) as CodTipoObtenResult, max(TOR.NbrTipoObtenResult) as NbrTipoObtenResult,"
				+ " max(TP.CodTipoPeriodicidad) as CodTipoPeriodicidad, max(TP.NbrTipoPeriodicidad) as NbrTipoPeriodicidad, max(TP.NumValorPeriodicidad) as NumValorPeriodicidad,"
				+ " max(TMPD.CodTipoMetodologia) as CodTipoMetodologiaPD, max(TMPD.NbrTipoMetodologia) as NbrTipoMetodologiaPD, max(TMPD.NbrVariableMotor) as NbrVariableMotorPD,"
				+ " max(TMLGD.CodTipoMetodologia) as CodTipoMetodologiaLGD, max(TMLGD.NbrTipoMetodologia) as NbrTipoMetodologiaLGD, max(TMLGD.NbrVariableMotor) as NbrVariableMotorLGD,"
				+ " max(TMLGDWO.CodTipoMetodologia) as CodTipoMetodologiaLGDWO, max(TMLGDWO.NbrTipoMetodologia) as NbrTipoMetodologiaLGDWO, max(TMLGDWO.NbrVariableMotor) as NbrVariableMotorLGDWO,"
				+ " max(TMLGDBE.CodTipoMetodologia) as CodTipoMetodologiaLGDBE, max(TMLGDBE.NbrTipoMetodologia) as NbrTipoMetodologiaLGDBE, max(TMLGDBE.NbrVariableMotor) as NbrVariableMotorLGDBE,"
				+ " max(TMEAD.CodTipoMetodologia) as CodTipoMetodologiaEAD, max(TMEAD.NbrTipoMetodologia) as NbrTipoMetodologiaEAD, max(TMEAD.NbrVariableMotor) as NbrVariableMotorEAD,"
				+ " max(C.CodCartera) as CodCartera," + " max(C.NbrCartera) as NbrCartera,"
				+ " max(C.NbrVariableMotor) as NbrVariableMotorCartera, max(C.NumOrden) as NumOrden,"
				+ " max(C.FlgPd6Meses) as FlgPd6Meses, max(TA.CodTipoAmbito) as CodTipoAmbito, max(TA.NbrTipoAmbito) as NbrTipoAmbito, "
				+ " max(TM.CodTipoMagnitud) as CodTipoMagnitud, max(TM.NbrTipoMagnitud) as NbrTipoMagnitud,"
				+ " max(TC.CodTipoCartera) as CodTipoCartera, max(TC.NbrTipoCartera) as NbrTipoCartera, max(TC.NbrVariableMotor) as NbrVariableMotorTipoCartera,"
				+ " max(E.CodEntidad) as CodEntidad, max(E.NbrEntidad) as NbrEntidad, max(E.NbrVariableMotor) as NbrVariableMotorEntidad, "
				+ " max(E_REP.CodEjecucion) as CodEjecucionRepricing, max(E_REP.CodMes) as CodMesRepricing, "
				+ " max(PP_REP.CodPrimerPaso) as CodPrimerPasoRepricing,  max(PP_REP.NbrPrimerPaso) as NbrPrimerPasoRepricing, "
				+ " max(PP_REP.DesPrimerPaso) as DesPrimerPasoRepricing " + " from HE_Ejecucion E "
				+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion"
				+ " LEFT JOIN me_escenario esc ON ev.codescenario = esc.codescenario"
				+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
				+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
				+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ " left join ME_MotorCalculo MOC on MC.CodMotor = MOC.CodMotor"
				+ " left join ME_TipoFlujo TF on MC.CodTipoFlujo = TF.CodTipoFlujo "
				+ " left join ME_TipoObtencionResultados TOR on MC.CodTipoObtenResult = TOR.CodTipoObtenResult "
				+ " left join ME_TipoPeriodicidad TP on MC.CodTipoPeriodicidad = TP.CodTipoPeriodicidad "
				+ " left join ME_TipoMetodologia TMPD on MC.CodTipoMetodologiaPD = TMPD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TMEAD on MC.CodTipoMetodologiaEAD = TMEAD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TMLGD on MC.CodTipoMetodologiaLGD = TMLGD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TMLGDWO on MC.CodTipoMetodologiaLGDWO = TMLGDWO.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TMLGDBE on MC.CodTipoMetodologiaLGDBE = TMLGDBE.CodTipoMetodologia "
				+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
				+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
				+ " left join ME_TipoMagnitud TM on C.CodTipoMagnitud = TM.CodTipoMagnitud "
				+ " left join ME_TipoCartera TC on C.CodTipoCartera = TC.CodTipoCartera "
				+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad "
				+ " left join HE_Ejecucion E_REP on E.CodEjecucionRepricing = E_REP.CodEjecucion "
				+ " left join HE_PrimerPaso PP_REP on E_REP.CodPrimerPaso = PP_REP.CodPrimerPaso "
				+ " where E.CodTipoEjecucion in " + (incluyeF2 ? "(1,2,7,8)" : "(1,7)") // Siempre fase 1 (en caso flag
																						// en falso)
				+ " and E.TipEstadoValidacion = -1 " // No requieren proceso de validacion
				+ "group by E.codmes, JEV.CodJuegoEscenarios" + (incluyeF2 ? ", E.CodTipoEjecucion" : "")
				+ " order by E.codmes desc";

		String sql_ejecModMacro = null;

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ejecucionAgregada = new EjecucionAgregadaPE();
				juegoEscenarios_version = new JuegoEscenarios_version();
				cartera_version = new Cartera_version();
				metodologia = new EstructuraMetodologica();
				cartera = new Cartera();
				entidad = new Entidad();

				ejecucionAgregada.setCodmes(rs.getInt("CodMes"));
				if (incluyeF2) {
					ejecucionAgregada.setFase(rs.getInt("CodTipoEjecucion") % 6); // Incluye inversiones
				}

				ejecucionAgregada.setEstado_ejecucion(rs.getInt("EstadoEjecucionMin"));
				ejecucionAgregada.setPorcentaje_avance(rs.getInt("NumEjecucionAvanceProm"));
				// ejecucionAgregada.setLog_motor(rs.getString("DesLogMotor"));

				// Seteamos flag de validacion solicitada para la simulacion
				if (rs.getInt("EstadoValidacionMin") == -1) {
					ejecucionAgregada.setValidacionSolicitada(false);
				} else {
					ejecucionAgregada.setValidacionSolicitada(true);
				}

				ejecucionAgregada.setDatetime_ejecucion(rs.getLong("FecEjecucion"));

				juegoEscenarios_version.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenarios_version.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenarios_version.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));
				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));

				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
				metodologia.setCodigo_estructura(rs.getString("NbrCodigoEstructura"));
				metodologia.setNombre_funcPlumbr(rs.getString("NbrFuncPlumbr"));
				metodologia.setPuerto_plumbr(rs.getInt("NumPuertoPlumbr"));

				motor = new MotorCalculo();
				motor.setId_motor(rs.getInt("CodMotor"));
				motor.setNombre_motor(rs.getString("NbrMotor"));
				metodologia.setMotor(motor);

				tipoFlujo = new TipoFlujo();
				tipoFlujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipoFlujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));
				metodologia.setFlujo(tipoFlujo);

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				tipoObtenResult = new TipoObtencionResultados();
				tipoObtenResult.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
				tipoObtenResult.setNombre_tipo_obtencion_resultados(rs.getString("NbrTipoObtenResult"));
				metodologia.setMetodo_resultados(tipoObtenResult);

				if (tipoAmbito.getId_tipo_ambito().equals(1)) { // Creditos
					if (tipoFlujo.getNombre_tipo_flujo().toLowerCase().equals(("Calculo PE").toLowerCase())) {
						if (tipoObtenResult.getNombre_tipo_obtencion_resultados().toLowerCase()
								.equals(("Modelo interno").toLowerCase())) {
							metodologia.setNro_buckets(rs.getInt("NumBuckets"));
							metodologia.setLife_time(rs.getInt("NumLifeTime"));
							metodologia.setNro_maxPlazo(rs.getInt("NumMaxPlazo"));

							tipoPeriodicidad = new TipoPeriodicidad();
							tipoPeriodicidad.setId_tipo_periodicidad(rs.getInt("CodTipoPeriodicidad"));
							tipoPeriodicidad.setNombre_tipo_periodicidad(rs.getString("NbrTipoPeriodicidad"));
							tipoPeriodicidad.setValor_periodicidad(rs.getInt("NumValorPeriodicidad"));
							metodologia.setPeriodicidad(tipoPeriodicidad);

							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaPD"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaPD"));
							metodologia.setMetodologia_PD(tipoMetodologia);

							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaEAD"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaEAD"));
							metodologia.setMetodologia_EAD(tipoMetodologia);

							metodologia.setFlag_lgdUnica(rs.getBoolean("FlgLgdUnica"));

							if (metodologia.getFlag_lgdUnica()) {
								tipoMetodologia = new TipoMetodologia();
								tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGD"));
								tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGD"));
								metodologia.setMetodologia_LGD(tipoMetodologia);
							} else {
								tipoMetodologia = new TipoMetodologia();
								tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGDWO"));
								tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGDWO"));
								metodologia.setMetodologia_LGD_WO(tipoMetodologia);

								tipoMetodologia = new TipoMetodologia();
								tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGDBE"));
								tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGDBE"));
								metodologia.setMetodologia_LGD_BE(tipoMetodologia);
							}
						} else if (tipoObtenResult.getNombre_tipo_obtencion_resultados().toLowerCase()
								.equals(("Extrapolacion").toLowerCase())) {
							metodologia.setCarteras_extrapolacion(this.obtenerCarterasExtrapolacionporIdMetodologia(
									metodologia.getId_metodologia(), conn));
						}
					}
				} else if (tipoAmbito.getId_tipo_ambito().equals(2)) { // Inversiones
					// ?
				}

				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setNombre_variablemotor(rs.getString("NbrVariableMotorCartera"));
				cartera.setOrden(rs.getInt("NumOrden"));
				cartera.setFlag_pd6meses(rs.getBoolean("FlgPd6Meses"));

				if (tipoAmbito.getId_tipo_ambito().equals(1)) { // Creditos
					tipoMagnitud = new TipoMagnitud();
					tipoMagnitud.setId_tipo_magnitud(rs.getInt("CodTipoMagnitud"));
					tipoMagnitud.setNombre_tipo_magnitud(rs.getString("NbrTipoMagnitud"));
					cartera.setTipo_magnitud(tipoMagnitud);

					tipoCartera = new TipoCartera();
					tipoCartera.setId_tipo_cartera(rs.getInt("CodTipoCartera"));
					tipoCartera.setNombre_tipo_cartera(rs.getString("NbrTipoCartera"));
					tipoCartera.setNombre_variablemotor(rs.getString("NbrVariableMotorTipoCartera"));
					cartera.setTipo_cartera(tipoCartera);
				} else if (tipoAmbito.getId_tipo_ambito().equals(2)) { // Inversiones
					// ?
				}

				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				entidad.setNombre_variablemotor(rs.getString("NbrVariableMotorEntidad"));
				cartera.setEntidad(entidad);

				metodologia.setCartera(cartera);

				cartera_version.setMetodologia(metodologia);

				juegoEscenarios_version.setCartera_version(cartera_version);

				ejecucionAgregada.setJuego_escenarios_version(juegoEscenarios_version);

				// Repricing
				if (rs.getInt("CodEjecucionRepricing") != 0) {
					ejecucionRepricing = new EjecucionPrimerPaso();
					primerPasoRep = new PrimerPaso();

					ejecucionRepricing.setId_ejecucion(rs.getInt("CodEjecucionRepricing"));
					ejecucionRepricing.setCodmes(rs.getInt("CodMesRepricing"));

					primerPasoRep.setId_primerPaso(rs.getInt("CodPrimerPasoRepricing"));
					primerPasoRep.setNombre_primerPaso(rs.getString("NbrPrimerPasoRepricing"));
					primerPasoRep.setDescripcion_primerPaso(rs.getString("DesPrimerPasoRepricing"));
					ejecucionRepricing.setPrimerPaso(primerPasoRep);

					ejecucionRepricing.setMetodologia(
							ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia());

					ejecucionAgregada.setEjecucion_repricing(ejecucionRepricing);
				}

				// Ingresamos la ejecucion de modelo macro configuradaa
				sql_ejecModMacro = "select MMM.TipAjusteManual, E_MM.CodEjecucion as CodEjecucionModMacro, E_MM.CodMes as CodMesModMacro, "
						+ "PP.CodPrimerPaso, PP.NbrPrimerPaso, PP.DesPrimerPaso " + "from HE_MapeoModMacro MMM "
						+ "left join HE_Ejecucion E_MM on MMM.CodEjecucionModMacro = E_MM.CodEjecucion "
						+ "left join HE_PrimerPaso PP on E_MM.CodPrimerPaso = PP.CodPrimerPaso "
						+ "left join HE_Ejecucion E_PE on MMM.CodEjecucionPE = E_PE.CodEjecucion "
						+ "left join HE_EscenarioVersion EV on E_PE.CodEscenarioVersion = EV.CodEscenarioVersion "
						+ "where ROWNUM=1 and E_PE.CodTipoEjecucion = 1 and E_PE.CodMes = "
						+ ejecucionAgregada.getCodmes() + " and EV.CodJuegoEscenarios = "
						+ ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios();

				stmt_ejecModMacro = crearStatement(conn);
				rs_ejecModMacro = stmt_ejecModMacro.executeQuery(sql_ejecModMacro);

				ejecucionModMacro = new EjecucionPrimerPaso();
				primerPaso = new PrimerPaso();

				sinResultado = true;
				while (rs_ejecModMacro.next()) {
					sinResultado = false;
					ejecucionAgregada.setTipo_ajusteManual(rs_ejecModMacro.getInt("TipAjusteManual"));

					if (ejecucionAgregada.getTipo_ajusteManual().equals(0)) {
						ejecucionModMacro.setCodmes(rs_ejecModMacro.getInt("CodMesModMacro"));

						primerPaso.setId_primerPaso(rs_ejecModMacro.getInt("CodPrimerPaso"));
						primerPaso.setNombre_primerPaso(rs_ejecModMacro.getString("NbrPrimerPaso"));
						primerPaso.setDescripcion_primerPaso(rs_ejecModMacro.getString("DesPrimerPaso"));
						ejecucionModMacro.setPrimerPaso(primerPaso);

						ejecucionModMacro.setMetodologia(
								ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia());
					}
					ejecucionModMacro.setId_ejecucion(rs_ejecModMacro.getInt("CodEjecucionModMacro"));
				}
				cerrarResultSet(rs_ejecModMacro);
				cerrarStatement(stmt_ejecModMacro);

				if (sinResultado) {
					ejecucionAgregada.setTipo_ajusteManual(-1); // No requiere mod macro
				} else {
					if (ejecucionAgregada.getTipo_ajusteManual().equals(0)) {
						ejecucionAgregada.setEjecucion_modMacro(ejecucionModMacro);
					}
				}

				ejecucionesAgregada.add(ejecucionAgregada);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener simulaciones agregadas: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarResultSet(rs_ejecModMacro);
			cerrarStatement(stmt);
			cerrarStatement(stmt_ejecModMacro);
			cerrarConexion(conn);
		}
		return ejecucionesAgregada;
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotalesFase1() throws Exception {
		return this.obtenerEjecucionesPETotales(1, false);
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotalesFase2() throws Exception {
		return this.obtenerEjecucionesPETotales(2, false);
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotales() throws Exception {
		return this.obtenerEjecucionesPETotales(null, true); // Solo interesa el segundo param
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotales(Integer fase, Boolean ambos) throws Exception {
		ArrayList<EjecucionAgregadaPE> ejecucionesAgregada = new ArrayList<EjecucionAgregadaPE>();
		EjecucionAgregadaPE ejecucionAgregada = null;
		JuegoEscenarios_version juegoEscenarios_version = null;
		Cartera_version cartera_version = null;
		EstructuraMetodologica metodologia = null;
		Cartera cartera = null;
		TipoAmbito tipoAmbito = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "select E.CodMes, JEV.CodJuegoEscenarios, " + (ambos ? "E.CodTipoEjecucion, " : "")
				+ " min(E.TipEstadoValidacion) as EstadoValidacionMin,"
				+ " min(E.TipEstadoEjecucion) as EstadoEjecucionMin," + " max(E.FecEjecucion) as FecEjecucion,"
				+ " max(JEV.NbrJuegoEscenarios) as NbrJuegoEscenarios, "
				+ " max(JEV.DesJuegoEscenarios) as DesJuegoEscenarios, "
				+ " max(CV.CodCarteraVersion) as CodCarteraVersion," + " max(CV.NbrVersion) as NbrVersion, "
				+ " max(MC.CodMetodologia) as CodMetodologia," + " max(MC.NbrMetodologia) as NbrMetodologia, "
				+ " max(MC.NbrCodigoEstructura) as NbrCodigoEstructura, max(MC.NbrFuncPlumbr) as NbrFuncPlumb, "
				+ " max(NumPuertoPlumbr) as NumPuertoPlumbr, max(C.CodCartera) as CodCartera, max(C.NbrCartera) as NbrCartera,"
				+ " max(C.NbrVariableMotor) as NbrVariableMotorCartera, max(C.NumOrden) as NumOrden, "
				+ " max(C.FlgPd6Meses) as FlgPd6Meses, max(TA.CodTipoAmbito) as CodTipoAmbito, max(TA.NbrTipoAmbito) as NbrTipoAmbito "
				+ " from HE_Ejecucion E "
				+ " left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion"
				+ " left join HE_JuegoEscenariosVersion JEV on EV.CodJuegoEscenarios = JEV.CodJuegoEscenarios "
				+ " left join HE_CarteraVersion CV on JEV.CodCarteraVersion = CV.CodCarteraVersion "
				+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
				+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito " + " where E.CodTipoEjecucion in "
				+ (ambos ? "(1,2,7,8)" : (fase.equals(1) ? "(1,7)" : "(2,8)"))
				+ " group by E.codmes, JEV.CodJuegoEscenarios" + (ambos ? ", E.CodTipoEjecucion" : "")
				+ " order by E.codmes desc";

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ejecucionAgregada = new EjecucionAgregadaPE();
				juegoEscenarios_version = new JuegoEscenarios_version();
				cartera_version = new Cartera_version();
				metodologia = new EstructuraMetodologica();

				ejecucionAgregada.setCodmes(rs.getInt("CodMes"));
				ejecucionAgregada.setEstado_ejecucion(rs.getInt("EstadoEjecucionMin"));

				ejecucionAgregada.setDatetime_ejecucion(rs.getLong("FecEjecucion"));

				juegoEscenarios_version.setId_juego_escenarios(rs.getInt("CodJuegoEscenarios"));
				juegoEscenarios_version.setNombre_juego_escenarios(rs.getString("NbrJuegoEscenarios"));
				juegoEscenarios_version.setDescripcion_juego_escenarios(rs.getString("DesJuegoEscenarios"));
				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));

				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
				metodologia.setCodigo_estructura(rs.getString("NbrCodigoEstructura"));

				cartera = new Cartera();
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setNombre_variablemotor(rs.getString("NbrVariableMotorCartera"));
				cartera.setOrden(rs.getInt("NumOrden"));
				cartera.setFlag_pd6meses(rs.getBoolean("FlgPd6Meses"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				metodologia.setCartera(cartera);

				cartera_version.setMetodologia(metodologia);

				juegoEscenarios_version.setCartera_version(cartera_version);

				ejecucionAgregada.setJuego_escenarios_version(juegoEscenarios_version);

				// En caso sea ambos, llenamos atributo fase
				if (ambos) {
					ejecucionAgregada.setFase(rs.getInt("CodTipoEjecucion") % 6); // Incluye inv
				}

				ejecucionesAgregada.add(ejecucionAgregada);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener ejecuciones agregadas totales por fase: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return ejecucionesAgregada;
	}

	public void solicitarValidacion(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		String sql_ejecucion = "Update HE_Ejecucion set TipEstadoValidacion = ?, FecActualizacionTabla = ? "
				+ "where CodEjecucion in "
				+ "(select HE_Ejecucion.CodEjecucion from HE_Ejecucion left join HE_EscenarioVersion "
				+ "on HE_Ejecucion.CodEscenarioVersion = HE_EscenarioVersion.CodEscenarioVersion "
				+ "where HE_EscenarioVersion.CodJuegoEscenarios = ? and HE_Ejecucion.CodMes = ? "
				+ " and HE_Ejecucion.CodTipoEjecucion = ?)"; // Para simulacion bifasica
		// + " and HE_Ejecucion.CodTipoEjecucion in (1,7))";

		try {
			conn = obtenerConexion();

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
			pstmt_ejecucion.setInt(1, 0); // Seteamos Estado: validacion pendiente
			pstmt_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
			pstmt_ejecucion.setInt(3, ejecucionAgregada.getJuego_escenarios_version().getId_juego_escenarios());
			pstmt_ejecucion.setInt(4, ejecucionAgregada.getCodmes());
			pstmt_ejecucion
					.setInt(5,
							ejecucionAgregada.getJuego_escenarios_version().getCartera_version().getMetodologia()
									.getCartera().getTipo_ambito().getId_tipo_ambito().equals(1) ? 1
											: ejecucionAgregada.getFase() + 6);
			// en cred es 1 siempre
			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al solicitar validacion de la simulacion : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	// -------------------------------------------------------------------
	// ----------------------EJECUCIONES PRIMER PASO----------------------
	// -------------------------------------------------------------------

	public ArrayList<EjecucionPrimerPaso> obtenerEjecucionesPrimerPaso() throws Exception {
		ArrayList<EjecucionPrimerPaso> ejecucionesPrimerPaso = new ArrayList<EjecucionPrimerPaso>();
		EjecucionPrimerPaso ejecucionPrimerPaso = null;
		PrimerPaso primerPaso = null;
		EstructuraMetodologica metodologia = null;
		MotorCalculo motor = null;
		TipoFlujo tipoFlujo = null;
		TipoObtencionResultados tipoObtenResult = null;
		TipoMetodologia tipoMetodologia = null;
		TipoPeriodicidad tipoPeriodicidad = null;
		Cartera cartera = null;
		TipoAmbito tipoAmbito = null;
		TipoMagnitud tipoMagnitud = null;
		TipoCartera tipoCartera = null;
		Entidad entidad = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select EJ.CodEjecucion, EJ.CodMes, EJ.TipEstadoEjecucion, EJ.NumEjecucionAvance, EJ.FecEjecucion, "
				+ " PP.CodPrimerPaso, PP.NbrPrimerPaso, PP.NbrVariableMotor as NbrVariableMotorPrimerPaso, "
				+ " PP.DesPrimerPaso, PP.NumPeriodMeses, MPPM.NbrCodigoPrimerPaso, "
				+ " MC.CodMetodologia, MC.NbrMetodologia, MC.NbrCodigoEstructura, MC.NumBuckets, "
				+ " MC.NumLifeTime, MC.NumMaxPlazo, MC.FlgLgdUnica, MOC.CodMotor, MOC.NbrMotor, "
				+ "	TF.CodTipoFlujo, TF.NbrTipoFlujo, TOR.CodTipoObtenResult, TOR.NbrTipoObtenResult, "
				+ " TP.CodTipoPeriodicidad, TP.NbrTipoPeriodicidad, TP.NumValorPeriodicidad, "
				+ " TMPD.CodTipoMetodologia as CodTipoMetodologiaPD, TMPD.NbrTipoMetodologia as NbrTipoMetodologiaPD, TMPD.NbrVariableMotor as NbrVariableMotorPD, "
				+ " TMLGD.CodTipoMetodologia as CodTipoMetodologiaLGD, TMLGD.NbrTipoMetodologia as NbrTipoMetodologiaLGD, TMLGD.NbrVariableMotor as NbrVariableMotorLGD, "
				+ " TMLGDWO.CodTipoMetodologia as CodTipoMetodologiaLGDWO, TMLGDWO.NbrTipoMetodologia as NbrTipoMetodologiaLGDWO, TMLGDWO.NbrVariableMotor as NbrVariableMotorLGDWO, "
				+ " TMLGDBE.CodTipoMetodologia as CodTipoMetodologiaLGDBE, TMLGDBE.NbrTipoMetodologia as NbrTipoMetodologiaLGDBE, TMLGDBE.NbrVariableMotor as NbrVariableMotorLGDBE, "
				+ " TMEAD.CodTipoMetodologia as CodTipoMetodologiaEAD, TMEAD.NbrTipoMetodologia as NbrTipoMetodologiaEAD, TMEAD.NbrVariableMotor as NbrVariableMotorEAD, "
				+ " C.CodCartera, C.NbrCartera, C.NbrVariableMotor as NbrVariableMotorCartera, C.NumOrden, "
				+ " C.FlgPd6Meses, TA.CodTipoAmbito, TA.NbrTipoAmbito, TM.CodTipoMagnitud, TM.NbrTipoMagnitud, "
				+ " TC.CodTipoCartera, TC.NbrTipoCartera, TC.NbrVariableMotor as NbrVariableMotorTipoCartera, "
				+ " EN.CodEntidad, EN.NbrEntidad, EN.NbrVariableMotor as NbrVariableMotorEntidad "
				+ " from HE_Ejecucion EJ" + " left join HE_PrimerPaso PP on EJ.CodPrimerPaso = PP.CodPrimerPaso"
				+ " left join HE_MetodologiaCartera MC on EJ.CodMetodologia = MC.CodMetodologia"
				+ " left join HE_MapeoPrimerPasoMet MPPM "
				+ " on PP.CodPrimerPaso = MPPM.CodPrimerPaso and MC.CodMetodologia = MPPM.CodMetodologia "
				+ " left join ME_MotorCalculo MOC on MC.CodMotor = MOC.CodMotor"
				+ " left join ME_TipoFlujo TF on MC.CodTipoFlujo = TF.CodTipoFlujo "
				+ " left join ME_TipoObtencionResultados TOR on MC.CodTipoObtenResult = TOR.CodTipoObtenResult "
				+ " left join ME_TipoPeriodicidad TP on MC.CodTipoPeriodicidad = TP.CodTipoPeriodicidad "
				+ " left join ME_TipoMetodologia TMPD on MC.CodTipoMetodologiaPD = TMPD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TMEAD on MC.CodTipoMetodologiaEAD = TMEAD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TMLGD on MC.CodTipoMetodologiaLGD = TMLGD.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TMLGDWO on MC.CodTipoMetodologiaLGDWO = TMLGDWO.CodTipoMetodologia "
				+ " left join ME_TipoMetodologia TMLGDBE on MC.CodTipoMetodologiaLGDBE = TMLGDBE.CodTipoMetodologia "
				+ " left join HE_Cartera C on MC.CodCartera= C.CodCartera "
				+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
				+ " left join ME_TipoMagnitud TM on C.CodTipoMagnitud = TM.CodTipoMagnitud "
				+ " left join ME_TipoCartera TC on C.CodTipoCartera = TC.CodTipoCartera "
				+ " left join HE_Entidad EN on C.CodEntidad = EN.CodEntidad" + " where EJ.CodTipoEjecucion = 4" // Tipo:
																												// Primer
																												// Paso
				+ " order by EJ.CodMes desc";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ejecucionPrimerPaso = new EjecucionPrimerPaso();
				ejecucionPrimerPaso.setId_ejecucion(rs.getInt("CodEjecucion"));
				ejecucionPrimerPaso.setCodmes(rs.getInt("CodMes"));
				ejecucionPrimerPaso.setEstado_ejecucion(rs.getInt("TipEstadoEjecucion"));
				ejecucionPrimerPaso.setPorcentaje_avance(rs.getInt("NumEjecucionAvance"));
				ejecucionPrimerPaso.setDatetime_ejecucion(rs.getLong("FecEjecucion"));

				primerPaso = new PrimerPaso();
				primerPaso.setId_primerPaso(rs.getInt("CodPrimerPaso"));
				primerPaso.setNombre_primerPaso(rs.getString("NbrPrimerPaso"));
				primerPaso.setNombre_variablemotor(rs.getString("NbrVariableMotorPrimerPaso"));
				primerPaso.setDescripcion_primerPaso(rs.getString("DesPrimerPaso"));
				primerPaso.setCodigo_primerPaso(rs.getString("NbrCodigoPrimerPaso"));
				ejecucionPrimerPaso.setPrimerPaso(primerPaso);

				metodologia = new EstructuraMetodologica();
				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
				metodologia.setCodigo_estructura(rs.getString("NbrCodigoEstructura"));

				motor = new MotorCalculo();
				motor.setId_motor(rs.getInt("CodMotor"));
				motor.setNombre_motor(rs.getString("NbrMotor"));
				metodologia.setMotor(motor);

				tipoFlujo = new TipoFlujo();
				tipoFlujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipoFlujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));
				metodologia.setFlujo(tipoFlujo);

				if (tipoFlujo.getNombre_tipo_flujo().toLowerCase().equals(("Calculo PE").toLowerCase())) {
					tipoObtenResult = new TipoObtencionResultados();
					tipoObtenResult.setId_tipo_obtencion_resultados(rs.getInt("CodTipoObtenResult"));
					tipoObtenResult.setNombre_tipo_obtencion_resultados(rs.getString("NbrTipoObtenResult"));
					metodologia.setMetodo_resultados(tipoObtenResult);

					if (tipoObtenResult.getNombre_tipo_obtencion_resultados().toLowerCase()
							.equals(("Modelo interno").toLowerCase())) {
						metodologia.setNro_buckets(rs.getInt("NumBuckets"));
						metodologia.setLife_time(rs.getInt("NumLifeTime"));
						metodologia.setNro_maxPlazo(rs.getInt("NumMaxPlazo"));

						tipoPeriodicidad = new TipoPeriodicidad();
						tipoPeriodicidad.setId_tipo_periodicidad(rs.getInt("CodTipoPeriodicidad"));
						tipoPeriodicidad.setNombre_tipo_periodicidad(rs.getString("NbrTipoPeriodicidad"));
						tipoPeriodicidad.setValor_periodicidad(rs.getInt("NumValorPeriodicidad"));
						metodologia.setPeriodicidad(tipoPeriodicidad);

						tipoMetodologia = new TipoMetodologia();
						tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaPD"));
						tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaPD"));
						metodologia.setMetodologia_PD(tipoMetodologia);

						tipoMetodologia = new TipoMetodologia();
						tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaEAD"));
						tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaEAD"));
						metodologia.setMetodologia_EAD(tipoMetodologia);

						metodologia.setFlag_lgdUnica(rs.getBoolean("FlgLgdUnica"));

						if (metodologia.getFlag_lgdUnica()) {
							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGD"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGD"));
							metodologia.setMetodologia_LGD(tipoMetodologia);
						} else {
							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGDWO"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGDWO"));
							metodologia.setMetodologia_LGD_WO(tipoMetodologia);

							tipoMetodologia = new TipoMetodologia();
							tipoMetodologia.setId_tipo_metodologia(rs.getInt("CodTipoMetodologiaLGDBE"));
							tipoMetodologia.setNombre_tipo_metodologia(rs.getString("NbrTipoMetodologiaLGDBE"));
							metodologia.setMetodologia_LGD_BE(tipoMetodologia);
						}
					}
				}

				cartera = new Cartera();
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setNombre_variablemotor(rs.getString("NbrVariableMotorCartera"));
				cartera.setOrden(rs.getInt("NumOrden"));
				cartera.setFlag_pd6meses(rs.getBoolean("FlgPd6Meses"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				tipoMagnitud = new TipoMagnitud();
				tipoMagnitud.setId_tipo_magnitud(rs.getInt("CodTipoMagnitud"));
				tipoMagnitud.setNombre_tipo_magnitud(rs.getString("NbrTipoMagnitud"));
				cartera.setTipo_magnitud(tipoMagnitud);

				tipoCartera = new TipoCartera();
				tipoCartera.setId_tipo_cartera(rs.getInt("CodTipoCartera"));
				tipoCartera.setNombre_tipo_cartera(rs.getString("NbrTipoCartera"));
				tipoCartera.setNombre_variablemotor(rs.getString("NbrVariableMotorTipoCartera"));
				cartera.setTipo_cartera(tipoCartera);

				entidad = new Entidad();
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				entidad.setNombre_variablemotor(rs.getString("NbrVariableMotorEntidad"));
				cartera.setEntidad(entidad);

				metodologia.setCartera(cartera);

				ejecucionPrimerPaso.setMetodologia(metodologia);

				ejecucionesPrimerPaso.add(ejecucionPrimerPaso);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener ejecuciones de primer paso : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return ejecucionesPrimerPaso;
	}

	public ArrayList<PrimerPaso> obtenerPrimerosPasosPorIdMetodologia(Integer id_metodologia) throws Exception {
		ArrayList<PrimerPaso> primerosPasos = new ArrayList<PrimerPaso>();
		PrimerPaso primerPaso = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "Select distinct PP.CodPrimerPaso, PP.NbrPrimerPaso, PP.NbrVariableMotor, "
				+ "PP.DesPrimerPaso, PP.NumPeriodMeses " + "from HE_MapeoPrimerPasoMet MPM "
				+ "left join HE_PrimerPaso PP on MPM.CodPrimerPaso = PP.CodPrimerPaso "
				+ "where MPM.NbrCodigoPrimerPaso is not null " + "and MPM.CodMetodologia = " + id_metodologia;
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				primerPaso = new PrimerPaso();
				primerPaso.setId_primerPaso(rs.getInt("CodPrimerPaso"));
				primerPaso.setNombre_primerPaso(rs.getString("NbrPrimerPaso"));
				primerPaso.setNombre_variablemotor(rs.getString("NbrVariableMotor"));
				primerPaso.setDescripcion_primerPaso(rs.getString("DesPrimerPaso"));
				primerPaso.setPeriodicidad_meses(rs.getInt("NumPeriodMeses"));
				primerosPasos.add(primerPaso);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener primeros pasos por metodologia: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return primerosPasos;
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionPrimerPaso(Integer id_ejecucion) throws Exception {
		ArrayList<TablaMapeoEjecucion> tablasMapeo = new ArrayList<TablaMapeoEjecucion>();
		Connection conn = null;

		String sql = "select ME.CodMapeoTabla," + " TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata,"
				+ " A.CodAprovisionamiento, A.NbrFichero,"
				+ " CF.CodCargaFichero, CF.NbrCarga, CF.NumOrdenInicial, CF.NumOrdenFinal, CF.DesComentarioCalidad, CF.NumVersionCarga,"
				+ " F.CodFecha, F.NumCodMes," + " B.CodBloque, B.NbrBloque, B.NbrVariableMotor "
				+ " from HE_MapeoEjecucion ME"
				+ " left join HE_MapeoTablaBloque MTB on ME.CodMapeoTabla = MTB.CodMapeoTabla"
				+ " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput"
				+ " left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
				+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
				+ " left join HE_CargaFichero CF on CF.CodCargaFichero = ME.CodCargaFichero"
				+ " left join HE_Fecha F on CF.CodFecha = F.CodFecha"
				+ " where MTB.CodCarteraVersion is null and MTB.CodEscenarioVersion is null and MTB.FlgPrimerPaso = 1 "
				// No esta versionado por cartera/escenario version y tiene flag de primer paso
				+ " and ME.CodEjecucion = " + id_ejecucion;

		conn = obtenerConexion();
		tablasMapeo = obtenerTablasMapeo(sql, 1, false, conn, true, true);
		cerrarConexion(conn);

		return tablasMapeo;
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionPorIdPrimPasoMet(Integer id_primerPaso,
			Integer id_metodologia) throws Exception {
		ArrayList<TablaMapeoEjecucion> tablasMapeo = new ArrayList<TablaMapeoEjecucion>();
		Connection conn = null;

		String sql = "select MTB.CodMapeoTabla," + " TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata, "
				+ " A.CodAprovisionamiento, A.NbrFichero, " + " B.CodBloque, B.NbrBloque, B.NbrVariableMotor "
				+ " from HE_MapeoTablaBloque MTB "
				+ " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput"
				+ " left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
				+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
				+ " left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
				+ " left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque "
				+ " where MTB.CodCarteraVersion is null and MTB.CodEscenarioVersion is null and B.FlgTablaInput = 1"
				+ " and MTB.FlgPrimerPaso = 1" // Para primer paso
				+ " and TB.CodPrimerPaso = " + id_primerPaso + " and MTB.CodMetodologia = " + id_metodologia;

		conn = obtenerConexion();
		tablasMapeo = obtenerTablasMapeo(sql, 1, false, conn, false, true);
		cerrarConexion(conn);

		return tablasMapeo;
	}

	public void crearEjecucionPrimerPaso(EjecucionPrimerPaso ejecucionPrimerPaso) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		PreparedStatement pstmt_mapeo_ejecucion = null;
		Integer id_ejecucion = null;

		String sql_ejecucion = "Insert into HE_Ejecucion(CodEjecucion, CodMes, CodEscenarioVersion, CodTipoEjecucion,"
				+ " TipEstadoEjecucion, TipEstadoValidacion, TipEstadoAjustes, DesComentarioEjecucion, FecEjecucion,"
				+ " CodMetodologia, CodPrimerPaso, CodEjecucionVar, FlgRegulatorio, CodEjecucionModMac,"
				+ " CodEjecucionRepricing, CodCarteraVerOtros, FecActualizacionTabla) "
				+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		String sql_mapeo_ejecucion = "Insert into HE_MapeoEjecucion(CodEjecucion, CodMapeoTabla, CodCargaFichero,"
				+ " FecActualizacionTabla) values (?,?,?,?)";
		try {
			conn = obtenerConexion();
			id_ejecucion = obtenerSiguienteId("SEQ_HE_Ejecucion");

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
			pstmt_ejecucion.setInt(1, id_ejecucion);
			pstmt_ejecucion.setInt(2, ejecucionPrimerPaso.getCodmes());
			pstmt_ejecucion.setNull(3, java.sql.Types.INTEGER);
			pstmt_ejecucion.setInt(4, 4); // Tipo de ejecucion: Primer paso
			pstmt_ejecucion.setInt(5, ejecucionPrimerPaso.getEstado_ejecucion());
			pstmt_ejecucion.setInt(6, 0); // Estado validacion: Validacion pendiente
			pstmt_ejecucion.setInt(7, 0); // Estado ajuste: Sin ajuste
			pstmt_ejecucion.setString(8, ""); // Sin comentarios
			pstmt_ejecucion.setLong(9, ejecucionPrimerPaso.getDatetime_ejecucion());
			pstmt_ejecucion.setInt(10, ejecucionPrimerPaso.getMetodologia().getId_metodologia());
			pstmt_ejecucion.setInt(11, ejecucionPrimerPaso.getPrimerPaso().getId_primerPaso());
			pstmt_ejecucion.setNull(12, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(13, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(14, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(15, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(16, java.sql.Types.INTEGER);
			pstmt_ejecucion.setDate(17, new Date(System.currentTimeMillis()));

			pstmt_ejecucion.executeUpdate();

			for (TablaMapeoEjecucion tabla_mapeo_ejecucion : ejecucionPrimerPaso.getTablas_mapeo_ejecucion()) {
				pstmt_mapeo_ejecucion = conn.prepareStatement(sql_mapeo_ejecucion);
				pstmt_mapeo_ejecucion.setInt(1, id_ejecucion);
				pstmt_mapeo_ejecucion.setInt(2, tabla_mapeo_ejecucion.getId_mapeo_tabla());
				pstmt_mapeo_ejecucion.setInt(3, tabla_mapeo_ejecucion.getCarga().getId_cargafichero());
				pstmt_mapeo_ejecucion.setDate(4, new Date(System.currentTimeMillis()));

				pstmt_mapeo_ejecucion.executeUpdate();
				cerrarPreparedStatement(pstmt_mapeo_ejecucion);
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar una ejecucion de primer paso : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarPreparedStatement(pstmt_mapeo_ejecucion);
			cerrarConexion(conn);
		}
	}

	public ReplicaEjecucionesPrimerPaso replicarEjecucionesPrimerPaso(
			ReplicaEjecucionesPrimerPaso replicaEjecucionesPrimerPaso) throws Exception {
		for (EjecucionPrimerPaso ejecucionPrimerPaso : replicaEjecucionesPrimerPaso.getEjecuciones_replicar()) {
			ejecucionPrimerPaso.setEstado_ejecucion(0); // Seteamos el estado: no ejecutado
			ejecucionPrimerPaso.setCodmes(replicaEjecucionesPrimerPaso.getCodmes_replica());
			ejecucionPrimerPaso.setTablas_mapeo_ejecucion(
					this.obtenerTablasMapeoEjecucionPrimerPaso(ejecucionPrimerPaso.getId_ejecucion()));
			this.crearEjecucionPrimerPaso(ejecucionPrimerPaso);
		}

		// Returnamos replica de ejecuciones con mapeos completados
		return replicaEjecucionesPrimerPaso;
	}

	public void editarEjecucionPrimerPaso(EjecucionPrimerPaso ejecucionPrimerPaso) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		PreparedStatement pstmt_mapeo_ejecucion = null;
		String sql_ejecucion = "Update HE_Ejecucion set CodMes= ?, CodMetodologia = ?, CodPrimerPaso = ?, "
				+ "CodTipoEjecucion= ?, TipEstadoEjecucion= ?, FecEjecucion= ?, "
				+ "FecActualizacionTabla= ? where CodEjecucion = ?";

		String sql_mapeo_ejecucion = "Update HE_MapeoEjecucion set CodCargaFichero= ?, FecActualizacionTabla= ?"
				+ " where CodEjecucion= ? and CodMapeoTabla= ?";

		try {
			conn = obtenerConexion();
			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);

			pstmt_ejecucion.setInt(1, ejecucionPrimerPaso.getCodmes());
			pstmt_ejecucion.setInt(2, ejecucionPrimerPaso.getMetodologia().getId_metodologia());
			pstmt_ejecucion.setInt(3, ejecucionPrimerPaso.getPrimerPaso().getId_primerPaso());
			pstmt_ejecucion.setInt(4, 4); // Tipo de ejecucion: PrimerPaso
			pstmt_ejecucion.setInt(5, ejecucionPrimerPaso.getEstado_ejecucion());
			pstmt_ejecucion.setLong(6, ejecucionPrimerPaso.getDatetime_ejecucion());
			pstmt_ejecucion.setDate(7, new Date(System.currentTimeMillis()));
			pstmt_ejecucion.setInt(8, ejecucionPrimerPaso.getId_ejecucion());
			pstmt_ejecucion.executeUpdate();

			if (ejecucionPrimerPaso.getTablas_mapeo_ejecucion() != null) {
				for (TablaMapeoEjecucion tabla_mapeo_ejecucion : ejecucionPrimerPaso.getTablas_mapeo_ejecucion()) {
					pstmt_mapeo_ejecucion = conn.prepareStatement(sql_mapeo_ejecucion);
					pstmt_mapeo_ejecucion.setInt(1, tabla_mapeo_ejecucion.getCarga().getId_cargafichero());
					pstmt_mapeo_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
					pstmt_mapeo_ejecucion.setInt(3, ejecucionPrimerPaso.getId_ejecucion());
					pstmt_mapeo_ejecucion.setInt(4, tabla_mapeo_ejecucion.getId_mapeo_tabla());

					pstmt_mapeo_ejecucion.executeUpdate();
					cerrarPreparedStatement(pstmt_mapeo_ejecucion);
				}
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar ejecucion de primer paso : " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarPreparedStatement(pstmt_mapeo_ejecucion);
			cerrarConexion(conn);
		}
	}

	public void borrarEjecucionPrimerPaso(Integer id_ejecucion) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		String sql_ejecucion = "DELETE from HE_Ejecucion where CodEjecucion=? and CodTipoEjecucion= 4";

		try {
			conn = obtenerConexion();

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
			pstmt_ejecucion.setInt(1, id_ejecucion);
			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error al borrar una ejecucion de primer paso con id: " + id_ejecucion + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	/*
	 * public void ejecutarPrimerPaso(EjecucionPrimerPaso ejecucionPrimerPaso)
	 * throws Exception { Connection conn = null; PreparedStatement pstmt_ejecucion
	 * = null;
	 * 
	 * String sql_ejecucion =
	 * "Update HE_Ejecucion set TipEstadoEjecucion= ?, FecActualizacionTabla= ? " +
	 * "where CodEjecucion = ? and CodTipoEjecucion = ?";
	 * 
	 * try { conn = obtenerConexion();
	 * 
	 * pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
	 * pstmt_ejecucion.setInt(1, 2); // Seteamos Estado: Ejecutado (TEMPORAL)
	 * pstmt_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
	 * pstmt_ejecucion.setInt(3, ejecucionPrimerPaso.getId_ejecucion());
	 * pstmt_ejecucion.setInt(4, 4); // tipo: Primer paso
	 * pstmt_ejecucion.executeUpdate();
	 * 
	 * ejecutarCommit(conn);
	 * 
	 * } catch (Exception ex) { ejecutarRollback(conn);
	 * LOGGER.error("Error al ejecutar calculo de primer paso : " +
	 * ex.getMessage()); throw ex;
	 * 
	 * } finally { cerrarPreparedStatement(pstmt_ejecucion); cerrarConexion(conn); }
	 * }
	 */

	// -------------------------------------------------------------------
	// -------------------------PRIMEROS PASOS----------------------------
	// -------------------------------------------------------------------

	public ArrayList<PrimerPaso> obtenerPrimerosPasos() throws Exception {
		ArrayList<PrimerPaso> primerosPasos = new ArrayList<PrimerPaso>();
		PrimerPaso primerPaso = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select PP.CodPrimerPaso, PP.NbrPrimerPaso, PP.NbrVariableMotor, PP.DesPrimerPaso "
				+ "from HE_PrimerPaso PP order by PP.CodPrimerPaso";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				primerPaso = new PrimerPaso();
				primerPaso.setId_primerPaso(rs.getInt("CodPrimerPaso"));
				primerPaso.setNombre_primerPaso(rs.getString("NbrPrimerPaso"));
				primerPaso.setNombre_variablemotor(rs.getString("NbrVariableMotor"));
				primerPaso.setDescripcion_primerPaso(rs.getString("DesPrimerPaso"));
				primerosPasos.add(primerPaso);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener primeros pasos: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return primerosPasos;
	}

	public PrimerPasoParametrizado obtenerPrimerPasoParametrizado(Integer id_primerPaso, Integer id_metodologia)
			throws Exception {
		Connection conn = null;
		Statement stmt_primerPaso = null;
		Statement stmt_estructura = null;
		Statement stmt_bloquesParam = null;
		Statement stmt_camposParam = null;
		ResultSet rs_primerPaso = null;
		ResultSet rs_estructura = null;
		ResultSet rs_bloquesParam = null;
		ResultSet rs_camposParam = null;

		PrimerPasoParametrizado primerPasoParam = new PrimerPasoParametrizado();
		EstructuraMetodologica estructura = null;
		Cartera cartera = null;
		Entidad entidad = null;
		ArrayList<BloqueParametrizado> bloquesParam = new ArrayList<BloqueParametrizado>();
		BloqueParametrizado bloqueParam = null;
		TablaInput tablaInput = null;
		ArrayList<CampoParametrizado> camposParam = null;
		CampoParametrizado campoParam = null;
		CampoBloque campoBloque = null;
		TipoCampo tipoCampo = null;
		CampoInput campoInput = null;

		TipoBloque tipoBloque = null;
		TipoEjecucion tipoEjecucion = null;
		TipoFlujo tipoFlujo = null;
		PrimerPaso primerPaso = null;
		TipoObtencionResultados tipoObtenResult = null;

		String sql_primerPaso = "select CodPrimerPaso, NbrPrimerPaso, NbrVariableMotor, DesPrimerPaso "
				+ "from HE_PrimerPaso where CodPrimerPaso = " + id_primerPaso;

		String sql_estructura = "select MC.CodMetodologia, MC.NbrMetodologia, MC.DesMetodologia, "
				+ "C.CodCartera, C.NbrCartera, " + "E.CodEntidad, E.NbrEntidad " + "from HE_MetodologiaCartera MC "
				+ "left join HE_Cartera C on MC.CodCartera = C.CodCartera "
				+ "left join HE_Entidad E on C.CodEntidad = E.CodEntidad " + "where MC.CodMetodologia = "
				+ id_metodologia;

		String sql_bloquesParam = "select MTB.CodMapeoTabla, MTB.DesFiltroTabla, "
				+ "B.CodBloque, B.NbrBloque, B.NbrVariableMotor, B.FlgTablaInput, "
				+ "TB.CodTipoBloque, TB.NbrTipoBloque, TB.NbrVariableMotor, TB.FlgBucket, "
				+ "TE.CodTipoEjecucion, TE.NbrTipoEjecucion, " + "TF.CodTipoFlujo, TF.NbrTipoFlujo, "
				+ "PP.CodPrimerPaso, PP.NbrPrimerPaso, PP.NbrVariableMotor, PP.DesPrimerPaso, PP.NumPeriodMeses, "
				+ "TOR.CodTipoObtenResult, TOR.NbrTipoObtenResult, "
				+ "TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata, TI.FlgNormalizada, A.NbrFichero "
				+ "from HE_MapeoTablaBloque MTB "
				+ "left join HE_TablaInput TI on MTB.CodTablaInput = TI.CodTablaInput "
				+ "left join HE_Aprovisionamiento A on TI.CodAprovisionamiento = A.CodAprovisionamiento "
				+ "left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
				+ "left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque "
				+ "left join ME_TipoEjecucion TE on TB.CodTipoEjecucion = TE.CodTipoEjecucion "
				+ "left join ME_TipoFlujo TF on TE.CodTipoFlujo = TF.CodTipoFlujo "
				+ "left join ME_TipoObtencionResultados TOR on TB.CodTipoObtenResult = TOR.CodTipoObtenResult "
				+ "left join HE_PrimerPaso PP on TB.CodPrimerPaso = PP.CodPrimerPaso "
				+ "where MTB.FlgPrimerPaso = 1 and MTB.CodMetodologia = " + id_metodologia + " and TB.CodPrimerPaso = "
				+ id_primerPaso;

		String sql_camposParam = "select RCB.TxtValor, RCB.NumValor, "
				+ "CB.CodCampoBloque, CB.NbrCampo as NbrCampoBloque, " + "TC.CodTipoCampo, TC.NbrTipoCampo, "
				+ "CI.CodCampo, CI.NbrCampo as NbrCampoInput " + "from HE_RegistroCampoBloque RCB "
				+ "left join HE_CampoBloque CB on RCB.CodCampoBloque = CB.CodCampoBloque "
				+ "left join ME_TipoCampo TC on CB.CodTipoCampo = TC.CodTipoCampo "
				+ "left join HE_CampoInput CI on RCB.CodCampo = CI.CodCampo " + "where RCB.CodMapeoTabla = ";

		try {
			conn = obtenerConexion();

			stmt_primerPaso = crearStatement(conn);
			rs_primerPaso = stmt_primerPaso.executeQuery(sql_primerPaso);
			while (rs_primerPaso.next()) {
				primerPasoParam.setId_primerPaso(rs_primerPaso.getInt("CodPrimerPaso"));
				primerPasoParam.setNombre_primerPaso(rs_primerPaso.getString("NbrPrimerPaso"));
				primerPasoParam.setNombre_variablemotor(rs_primerPaso.getString("NbrVariableMotor"));
				primerPasoParam.setDescripcion_primerPaso(rs_primerPaso.getString("DesPrimerPaso"));
			}

			stmt_estructura = crearStatement(conn);
			rs_estructura = stmt_estructura.executeQuery(sql_estructura);
			while (rs_estructura.next()) {
				estructura = new EstructuraMetodologica();
				estructura.setId_metodologia(rs_estructura.getInt("CodMetodologia"));
				estructura.setNombre_metodologia(rs_estructura.getString("NbrMetodologia"));
				estructura.setDescripcion_metodologia(rs_estructura.getString("DesMetodologia"));

				cartera = new Cartera();
				cartera.setId_cartera(rs_estructura.getInt("CodCartera"));
				cartera.setNombre_cartera(rs_estructura.getString("NbrCartera"));

				entidad = new Entidad();
				entidad.setId_entidad(rs_estructura.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs_estructura.getString("NbrEntidad"));

				cartera.setEntidad(entidad);
				estructura.setCartera(cartera);

				primerPasoParam.setEstructura_metodologica(estructura);
			}

			stmt_bloquesParam = crearStatement(conn);
			rs_bloquesParam = stmt_bloquesParam.executeQuery(sql_bloquesParam);
			while (rs_bloquesParam.next()) {
				bloqueParam = new BloqueParametrizado();
				bloqueParam.setId_mapeo_tabla(rs_bloquesParam.getInt("CodMapeoTabla"));
				bloqueParam.setFiltro_tabla(rs_bloquesParam.getString("DesFiltroTabla"));

				bloqueParam.setId_bloque(rs_bloquesParam.getInt("CodBloque"));
				bloqueParam.setNombre_bloque(rs_bloquesParam.getString("NbrBloque"));
				bloqueParam.setVariable_motor(rs_bloquesParam.getString("NbrVariableMotor"));
				bloqueParam.setFlag_tabla_input(rs_bloquesParam.getBoolean("FlgTablaInput"));

				tipoBloque = new TipoBloque();
				tipoBloque.setId_tipo_bloque(rs_bloquesParam.getInt("CodTipoBloque"));
				tipoBloque.setNombre_tipo_bloque(rs_bloquesParam.getString("NbrTipoBloque"));
				tipoBloque.setNombre_variablemotor(rs_bloquesParam.getString("NbrVariableMotor"));
				tipoBloque.setFlag_bucket(rs_bloquesParam.getBoolean("FlgBucket"));

				tipoEjecucion = new TipoEjecucion();
				tipoEjecucion.setId_tipo_ejecucion(rs_bloquesParam.getInt("CodTipoEjecucion"));
				tipoEjecucion.setNombre_tipo_ejecucion(rs_bloquesParam.getString("NbrTipoEjecucion"));

				tipoFlujo = new TipoFlujo();
				tipoFlujo.setId_tipo_flujo(rs_bloquesParam.getInt("CodTipoFlujo"));
				tipoFlujo.setNombre_tipo_flujo(rs_bloquesParam.getString("NbrTipoFlujo"));
				tipoEjecucion.setTipo_flujo(tipoFlujo);
				tipoBloque.setTipo_ejecucion(tipoEjecucion);

				tipoObtenResult = new TipoObtencionResultados();
				tipoObtenResult.setId_tipo_obtencion_resultados(rs_bloquesParam.getInt("CodTipoObtenResult"));
				tipoObtenResult.setNombre_tipo_obtencion_resultados(rs_bloquesParam.getString("NbrTipoObtenResult"));
				tipoBloque.setTipo_obtencion_resultados(tipoObtenResult);

				primerPaso = new PrimerPaso();
				primerPaso.setId_primerPaso(rs_bloquesParam.getInt("CodPrimerPaso"));
				primerPaso.setNombre_primerPaso(rs_bloquesParam.getString("NbrPrimerPaso"));
				primerPaso.setNombre_variablemotor(rs_bloquesParam.getString("NbrVariableMotor"));
				primerPaso.setDescripcion_primerPaso(rs_bloquesParam.getString("DesPrimerPaso"));
				primerPaso.setPeriodicidad_meses(rs_bloquesParam.getInt("NumPeriodMeses"));
				tipoBloque.setPrimerPaso(primerPaso);

				bloqueParam.setTipo_bloque(tipoBloque);

				if (bloqueParam.getFlag_tabla_input()) {
					tablaInput = new TablaInput();
					tablaInput.setId_tablainput(rs_bloquesParam.getInt("CodTablaInput"));
					tablaInput.setNombre_metadata(rs_bloquesParam.getString("NbrMetadata"));
					tablaInput.setVersion_metadata(rs_bloquesParam.getInt("NumVersionMetadata"));
					tablaInput.setNombre_fichero(rs_bloquesParam.getString("NbrFichero"));
					tablaInput.setFlag_normalizada(rs_bloquesParam.getBoolean("FlgNormalizada"));
					bloqueParam.setTabla_input(tablaInput);
				}

				stmt_camposParam = crearStatement(conn);
				rs_camposParam = stmt_camposParam.executeQuery(sql_camposParam + bloqueParam.getId_mapeo_tabla());

				camposParam = new ArrayList<CampoParametrizado>();
				while (rs_camposParam.next()) {
					campoParam = new CampoParametrizado();

					campoBloque = new CampoBloque();
					campoBloque.setId_campo(rs_camposParam.getInt("CodCampoBloque"));
					campoBloque.setNombre_campo(rs_camposParam.getString("NbrCampoBloque"));

					tipoCampo = new TipoCampo();
					tipoCampo.setId_tipo_campo(rs_camposParam.getInt("CodTipoCampo"));
					tipoCampo.setNombre_tipo_campo(rs_camposParam.getString("NbrTipoCampo"));
					campoBloque.setTipo_campo(tipoCampo);

					campoParam.setCampo_bloque(campoBloque);

					if (bloqueParam.getFlag_tabla_input()) {
						campoInput = new CampoInput();
						campoInput.setId_campo(rs_camposParam.getInt("CodCampo"));
						campoInput.setNombre_campo(rs_camposParam.getString("NbrCampoInput"));

						campoParam.setCampo_input(campoInput);

					} else {
						campoParam.setNumValor(rs_camposParam.getDouble("NumValor"));
						campoParam.setTxtValor(rs_camposParam.getString("TxtValor"));
					}
					camposParam.add(campoParam);
				}
				bloqueParam.setCampos_parametrizado(camposParam);
				cerrarResultSet(rs_camposParam);
				cerrarStatement(stmt_camposParam);

				bloquesParam.add(bloqueParam);
			}

			primerPasoParam.setBloques_parametrizado(bloquesParam);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al obtener el primer paso parametrizado" + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_primerPaso);
			cerrarResultSet(rs_estructura);
			cerrarResultSet(rs_bloquesParam);
			cerrarResultSet(rs_camposParam);
			cerrarStatement(stmt_primerPaso);
			cerrarStatement(stmt_estructura);
			cerrarStatement(stmt_bloquesParam);
			cerrarStatement(stmt_camposParam);
			cerrarConexion(conn);
		}

		return primerPasoParam;
	}

	public void parametrizarPrimerPaso(PrimerPasoParametrizado primerPaso) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_mapeo_tabla = null;
		PreparedStatement pstmt_registro_campo = null;

		String sql_mapeo_tabla = "update HE_MapeoTablaBloque set CodTablaInput= ?, DesFiltroTabla=?, FecActualizacionTabla=?"
				+ " where FlgPrimerPaso = 1 and CodMapeoTabla= ?";

		String sql_registro_campo = "update HE_RegistroCampoBloque set CodCampo = ?, NumValor = ?, TxtValor = ?, "
				+ "FecActualizacionTabla = ? where CodCampoBloque = ? and CodMapeoTabla = ?";

		try {
			conn = obtenerConexion();
			pstmt_mapeo_tabla = conn.prepareStatement(sql_mapeo_tabla);

			for (BloqueParametrizado bloqueParam : primerPaso.getBloques_parametrizado()) {

				if (bloqueParam.getFlag_tabla_input()) {
					pstmt_mapeo_tabla.setInt(1, bloqueParam.getTabla_input().getId_tablainput());
					pstmt_mapeo_tabla.setString(2, bloqueParam.getFiltro_tabla());
				} else {
					pstmt_mapeo_tabla.setNull(1, java.sql.Types.INTEGER); // CodTablaInput en null
					pstmt_mapeo_tabla.setNull(2, java.sql.Types.VARCHAR); // Filtro tabla en null
				}

				pstmt_mapeo_tabla.setDate(3, new Date(System.currentTimeMillis()));
				pstmt_mapeo_tabla.setInt(4, bloqueParam.getId_mapeo_tabla());
				pstmt_mapeo_tabla.executeUpdate();

				for (CampoParametrizado campoParam : bloqueParam.getCampos_parametrizado()) {
					pstmt_registro_campo = conn.prepareStatement(sql_registro_campo);

					if (bloqueParam.getFlag_tabla_input()) {
						pstmt_registro_campo.setInt(1, campoParam.getCampo_input().getId_campo());
						pstmt_registro_campo.setNull(2, java.sql.Types.INTEGER); // NumValor en null
						pstmt_registro_campo.setNull(3, java.sql.Types.VARCHAR); // txtValor en null
					} else {
						pstmt_registro_campo.setNull(1, java.sql.Types.INTEGER); // CodCampoInput en null

						if (campoParam.getCampo_bloque().getTipo_campo().getId_tipo_campo().equals(1)) { // Numerico
							pstmt_registro_campo.setDouble(2, campoParam.getNumValor());
							pstmt_registro_campo.setNull(3, java.sql.Types.VARCHAR); // txtValor en null

						} else if (campoParam.getCampo_bloque().getTipo_campo().getId_tipo_campo().equals(2)) { // Texto
							pstmt_registro_campo.setNull(2, java.sql.Types.INTEGER); // NumValor en null
							pstmt_registro_campo.setString(3, campoParam.getTxtValor());
						}
					}

					pstmt_registro_campo.setDate(4, new Date(System.currentTimeMillis()));
					pstmt_registro_campo.setInt(5, campoParam.getCampo_bloque().getId_campo());
					pstmt_registro_campo.setInt(6, bloqueParam.getId_mapeo_tabla());
					pstmt_registro_campo.executeUpdate();
					cerrarPreparedStatement(pstmt_registro_campo);
				}
			}

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al parametrizar el primer paso" + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_mapeo_tabla);
			cerrarPreparedStatement(pstmt_registro_campo);
			cerrarConexion(conn);
		}
	}

	// Funcion para exportar tablas a CSV

	public void escribirCSV(String nombreFicheroOutput, String filtro, Integer ambito) throws Exception {

		if (ambito.equals(1)) {
			escribirCSV(ruta_exportacion_ficheros_cred, "HE_OUTPUTPROVISIONESCRED", nombreFicheroOutput, filtro, true,
					0, false);
		} else if (ambito.equals(2)) {
			escribirCSV(ruta_exportacion_ficheros_inv, "HE_OUTPUTPROVISIONESINV", nombreFicheroOutput, filtro, true, 16,
					true);
		}
	}

	public void escribirCSV(String ruta, String nombreTabla, String nombreFicheroOutput, Boolean cabecera)
			throws Exception {
		escribirCSV(ruta, nombreTabla, nombreFicheroOutput, "", cabecera, 0, false);
	}

	public void escribirCSV(String ruta, String nombreTabla, String nombreFicheroOutput, String filtro,
			Boolean cabecera, Integer offset_tabla, Boolean cabeceraComentario) throws Exception {

		Connection conn = null;
		Statement stmt = null;
		Statement stmt_values = null;
		ResultSet rs = null;
		ResultSet rs_values = null;

		// CSVWriter csvwriter = null;
		Integer n_column = 0;
		String charSep = ";"; // ojo en local es ";"
		// String lineSep = System.getProperty("line.separator");
		String lineSep = "\n";

		Double valornum = 0.0;

		String sql = "select * from " + nombreTabla + (filtro.equals("") ? "" : " where " + filtro);
		String sql_values;
		String archivo = ruta + "\\" + nombreFicheroOutput + ".csv";
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			try (BufferedWriter out = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(archivo), "ISO-8859-1"));) {
				// csvwriter = new CSVWriter(new FileWriter(ruta + nombreFicheroOutput +
				// ".csv"), ';');
//			csvwriter.writeAll(rs, true);
				n_column = rs.getMetaData().getColumnCount() - offset_tabla; // Quitamos campos de id's
				String[] names = new String[n_column];
				String[] names_select = new String[n_column];
				String[] values = new String[n_column];
				int[] types = new int[n_column];
				for (int i = 1; i <= n_column; i++) {

					if (cabeceraComentario) {
						names[i - 1] = this.obtenerComentarioCampo(conn, nombreTabla, i + offset_tabla);
						names_select[i - 1] = rs.getMetaData().getColumnName(i + offset_tabla);
					} else {
						names[i - 1] = rs.getMetaData().getColumnName(i + offset_tabla);
					}

					types[i - 1] = rs.getMetaData().getColumnType(i + offset_tabla);

					if (cabecera) {
						out.append(names[i - 1]);
						if (i < n_column) {
							out.append(charSep);
						}
						out.flush();
					}
				}
				if (cabecera) {

					out.append(lineSep);
				}
				// csvwriter.writeNext(names, false);

				// Definimos selects de la query para valores
				if (!cabeceraComentario) {
					names_select = names;
				}

				sql_values = "select " + String.join(",", names_select) + " from " + nombreTabla
						+ (filtro.equals("") ? "" : " where " + filtro);
				stmt_values = crearStatement(conn);
				rs_values = stmt_values.executeQuery(sql_values);

				while (rs_values.next()) {
					for (int i = 1; i <= n_column; i++) {

						switch (types[i - 1]) {
						case 2:

							valornum = rs_values.getDouble(i);
							if (rs_values.wasNull()) {
								values[i - 1] = "";
							} else if (valornum % 1 == 0) {
								values[i - 1] = String.valueOf(valornum.intValue());
							} else {
								values[i - 1] = String.valueOf(valornum);
							}
							break;
						case 12:
							values[i - 1] = "\"" + rs_values.getString(i) + "\"";
							if (rs_values.wasNull()) {
								values[i - 1] = "";
							}
							break;
						case 93:
							values[i - 1] = "\"" + String.valueOf(rs_values.getDate(i)) + "\"";
							if (rs_values.wasNull()) {
								values[i - 1] = "";
							}
							break;
						}

						if (values[i - 1] != null && !values[i - 1].equalsIgnoreCase("null")) {
							values[i - 1] = (values[i - 1].replaceAll(charSep, " ")).replaceAll(lineSep, " ");
						}
						out.append(values[i - 1]);
						if (i < n_column) {
							out.append(charSep);
						}
					}
					out.append(lineSep);
					// csvwriter.writeNext(values, false);
				}

			}
		} catch (Exception ex) {
			LOGGER.error("Error al escribir CSV: " + ex.getMessage());
			throw ex;
		} finally {
			// csvwriter.flush();
			// csvwriter.close();
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarResultSet(rs_values);
			cerrarStatement(stmt_values);
			cerrarConexion(conn);
		}
	}

	public String obtenerComentarioCampo(Connection conn, String nombreTabla, Integer posCampo) throws Exception {

		String comentarioCampo = null;
		Integer idx = null;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select CC.comments from all_tab_columns ATC "
				+ "left join (select * from all_col_comments where table_name=?) CC "
				+ "on ATC.column_name = CC.column_name " + "where ATC.table_name=?" + " order by ATC.column_id";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, nombreTabla);
			pstmt.setString(2, nombreTabla);
			rs = pstmt.executeQuery();

			idx = 1;
			while (rs.next()) {
				if (posCampo == idx) {
					comentarioCampo = rs.getString("comments");
					LOGGER.info(comentarioCampo);
				}
				idx += 1;
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener el comentario de un campo de la tabla: " + nombreTabla + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return comentarioCampo;
	}

	// Funcion para importar de CSV a la BD
	public void leerCSV(String ruta, String nombreTabla, String nombreFicheroInput, Boolean cabecera) throws Exception {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		BufferedReader br = null;
		// CSVReader csvreader = null;
		String sql = null;
		Integer n_column = 0;
		String[] names = null;
		String[] values = null;
		String linea;
		int[] types = null;
		Integer nro_linea = 0;
		ArrayList<String> values_stmt = new ArrayList<String>();
		PreparedStatement pstmt_registro = null;

		try {

			sql = "select * from " + nombreTabla + " where 1=2";

			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			n_column = rs.getMetaData().getColumnCount();
			names = new String[n_column];
			types = new int[n_column];
			for (int i = 1; i <= n_column; i++) {
				names[i - 1] = rs.getMetaData().getColumnName(i);
				types[i - 1] = rs.getMetaData().getColumnType(i);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al leer tabla para CSV: " + ex.getMessage());
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}

		for (int i = 0; i < names.length; i++) {
			values_stmt.add("?");
		}
		sql = "INSERT INTO " + nombreTabla + " values (" + String.join(",", values_stmt) + ")";

		File f = new File(ruta + nombreFicheroInput + ".csv");
		if (f.exists() && !f.isDirectory()) {

			try {

				br = new BufferedReader(new FileReader(ruta + nombreFicheroInput + ".csv"));
				// csvreader = new CSVReader(new FileReader(ruta + nombreFicheroInput +
				// ".csv"));
				conn = obtenerConexion();
				pstmt_registro = conn.prepareStatement(sql);

				while ((linea = br.readLine()) != null) {

					if (cabecera && nro_linea == 0) {
						nro_linea++;
					} else {
						nro_linea++;
						if (linea != null) {
							values = new String[names.length - 1];
							values = separarCampos(linea, "|").toArray(values);

							for (int i = 1; i <= values.length; i++) {

								if (types[i - 1] == 12) {
									if (!values[i - 1].isEmpty()) {
										pstmt_registro.setString(i, values[i - 1].replace("\"", ""));
									} else {
										pstmt_registro.setNull(i, Types.VARCHAR);
									}
								} else {
									if (!values[i - 1].isEmpty()) {
										pstmt_registro.setDouble(i, Double.valueOf(values[i - 1].replace("\"", "")));
									} else {
										pstmt_registro.setNull(i, Types.VARCHAR);
									}
								}
							}
							pstmt_registro.addBatch();
						}
						if (nro_linea % max_lote == 0) {
							pstmt_registro.executeBatch();
							LOGGER.info("Se han procesado " + nro_linea + " en la carga del fichero CSV ");
						}
					}
				}
				pstmt_registro.executeBatch();
				ejecutarCommit(conn);
				// f.delete();
			} catch (Exception ex) {
				ejecutarRollback(conn);
				LOGGER.error("Error al leer CSV: " + ex.getMessage());
			} finally {
				// csvreader.close();
				cerrarBufferedReader(br);
				cerrarPreparedStatement(pstmt_registro);
				cerrarConexion(conn);
			}
		}
	}

	// Ajuste de deltas
	public ArrayList<MapeoDeltaStage> obtenerMapeosDeltaStage(Integer id_ejecucion_f2base) throws Exception {
		MapeoDeltaStage mapeo_delta = null;
		ArrayList<MapeoDeltaStage> mapeos_delta = new ArrayList<MapeoDeltaStage>();
		Stage stage = null;

		Connection conn = null;
		Statement stmt_mapeos_deltaStage = null;
		ResultSet rs_mapeos_deltaStage = null;

		String sql_mapeos_deltaStage = "select MD.NumValorDeltaProm, MD.NumValorDeltaPromFinal, MD.FlgDeltaAjustado, "
				+ "S.CodStage, S.NbrStage, S.NbrVariableMotor, S.FlgObligatorio " + "from HE_MapeoDelta MD "
				+ "left join ME_Stage S on MD.CodStage = S.CodStage " + "where MD.CodEjecucionFase2 = "
				+ id_ejecucion_f2base;

		try {
			conn = obtenerConexion();

			stmt_mapeos_deltaStage = crearStatement(conn);
			rs_mapeos_deltaStage = stmt_mapeos_deltaStage.executeQuery(sql_mapeos_deltaStage);

			while (rs_mapeos_deltaStage.next()) {
				mapeo_delta = new MapeoDeltaStage();

				stage = new Stage();
				stage.setId_stage(rs_mapeos_deltaStage.getInt("CodStage"));
				stage.setNombre_stage(rs_mapeos_deltaStage.getString("NbrStage"));
				stage.setNombre_variable_motor(rs_mapeos_deltaStage.getString("NbrVariableMotor"));
				stage.setFlag_obligatorio(rs_mapeos_deltaStage.getBoolean("FlgObligatorio"));
				mapeo_delta.setStage(stage);

				mapeo_delta.setFlag_delta_ajustado(rs_mapeos_deltaStage.getBoolean("FlgDeltaAjustado"));
				mapeo_delta.setDelta_calculado(rs_mapeos_deltaStage.getDouble("NumValorDeltaProm"));
				mapeo_delta.setDelta_final(rs_mapeos_deltaStage.getDouble("NumValorDeltaPromFinal"));

				mapeos_delta.add(mapeo_delta);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener mapeo de deltas : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_mapeos_deltaStage);
			cerrarStatement(stmt_mapeos_deltaStage);
			cerrarConexion(conn);
		}
		return mapeos_delta;
	}

	public void ajustarDeltas(GrupoMapeosDelta grupoMapeosDelta) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_mapeos_deltaStage = null;
		PreparedStatement pstmt_ejecucion = null;

		Statement stmt_consultaIdent = null;
		ResultSet rs_consultaIdent = null;

		Integer codmes = null;
		Integer id_juego_escenarios = null;
		Integer id_tipo_ejecucion = null;

		String sql_mapeos_deltaStage = "Update HE_MapeoDelta set NumValorDeltaPromFinal = ?, NumValorDeltaProm = ?, "
				+ "FlgDeltaAjustado = ? where CodStage = ? and CodEjecucionFase2 = ?";

		String sql_consultaIdent = "select EV.CodJuegoEscenarios, E.Codmes, E.CodTipoEjecucion "
				+ "from HE_Ejecucion E "
				+ "left join HE_EscenarioVersion EV on E.CodEscenarioVersion = EV.CodEscenarioVersion "
				+ "where E.CodEjecucion = " + grupoMapeosDelta.getId_ejecucion_f2base();

		String sql_ejecucion = "Update HE_Ejecucion set TipEstadoAjustes = ? " + "where CodEjecucion in "
				+ "(select HE_Ejecucion.CodEjecucion from HE_Ejecucion left join HE_EscenarioVersion "
				+ "on HE_Ejecucion.CodEscenarioVersion = HE_EscenarioVersion.CodEscenarioVersion "
				+ "where HE_EscenarioVersion.CodJuegoEscenarios = ? and HE_Ejecucion.CodMes = ? "
				+ " and HE_Ejecucion.CodTipoEjecucion = ?)";

		try {
			conn = obtenerConexion();

			// Consultamos identificadores de la ejecucion asociada
			stmt_consultaIdent = crearStatement(conn);
			rs_consultaIdent = stmt_consultaIdent.executeQuery(sql_consultaIdent);

			while (rs_consultaIdent.next()) {
				id_juego_escenarios = rs_consultaIdent.getInt("CodJuegoEscenarios");
				codmes = rs_consultaIdent.getInt("Codmes");
				id_tipo_ejecucion = rs_consultaIdent.getInt("CodTipoEjecucion");
			}

			// Setea estado de ajuste, en reemplazo de los ajustes de registro output
			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
			pstmt_ejecucion.setInt(1, 2); // Estado: 'Ajustado sin aprobar'
			pstmt_ejecucion.setInt(2, id_juego_escenarios);
			pstmt_ejecucion.setInt(3, codmes);
			pstmt_ejecucion.setInt(4, id_tipo_ejecucion);
			pstmt_ejecucion.executeUpdate();

			for (MapeoDeltaStage mapeo_delta : grupoMapeosDelta.getMapeos_delta()) {
				pstmt_mapeos_deltaStage = conn.prepareStatement(sql_mapeos_deltaStage);
				pstmt_mapeos_deltaStage.setDouble(1, mapeo_delta.getDelta_final());
				pstmt_mapeos_deltaStage.setDouble(2, mapeo_delta.getDelta_calculado());
				pstmt_mapeos_deltaStage.setBoolean(3, mapeo_delta.getFlag_delta_ajustado());
				pstmt_mapeos_deltaStage.setInt(4, mapeo_delta.getStage().getId_stage());
				pstmt_mapeos_deltaStage.setInt(5, grupoMapeosDelta.getId_ejecucion_f2base());

				pstmt_mapeos_deltaStage.executeUpdate();
				cerrarPreparedStatement(pstmt_mapeos_deltaStage);
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar el ajuste de delta : " + ex.getMessage());
			throw ex;

		} finally {
			cerrarResultSet(rs_consultaIdent);
			cerrarStatement(stmt_consultaIdent);
			cerrarPreparedStatement(pstmt_mapeos_deltaStage);
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	public void editarEstadoEjecucion(EjecucionPrimerPasoMotor ejecucion, Integer estado) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		String sql_ejecucion = "Update HE_Ejecucion set "
				+ "TipEstadoEjecucion= ?, FecActualizacionTabla= ? where CodEjecucion = ?";

		try {
			conn = obtenerConexion();

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);

			pstmt_ejecucion.setInt(1, estado);
			pstmt_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
			pstmt_ejecucion.setInt(3, ejecucion.getId_ejecucion());

			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar estado en ejecucion primer paso: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	public void editarEstadoEjecucion(EjecucionMotorOtro ejecucion, Integer estado) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		String sql_ejecucion = "Update HE_Ejecucion set "
				+ "TipEstadoEjecucion= ?, FecActualizacionTabla= ? where CodEjecucion = ?";

		try {
			conn = obtenerConexion();

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);

			pstmt_ejecucion.setInt(1, estado);
			pstmt_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
			pstmt_ejecucion.setInt(3, ejecucion.getId_ejecucion());

			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar estado en ejecucion de tipo otro: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	public void editarEstadoEjecucion(EjecucionMotor ejecucion, Integer estado) throws Exception {

		Connection conn = null;

		String sql_ejecucion = "Update HE_Ejecucion set "
				+ "TipEstadoEjecucion= ?, FecActualizacionTabla= ? where CodEjecucion = ?";

		try {
			conn = obtenerConexion();

			for (GrupoBloquesEscenario grupo : ejecucion.getGrupos_bloquesParamEscenario()) {
				try (PreparedStatement pstmt_ejecucion = conn.prepareStatement(sql_ejecucion)) {

					pstmt_ejecucion.setInt(1, estado);
					pstmt_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
					pstmt_ejecucion.setInt(3, grupo.getId_ejecucion());

					pstmt_ejecucion.executeUpdate();
				}
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar estado en ejecucion agregada: " + ex.getMessage());
			throw ex;

		} finally {

			cerrarConexion(conn);
		}
	}

	public void editarEstadoEjecucion(EjecucionVariacion ejecucion, Integer estado) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		String sql_ejecucion = "Update HE_Ejecucion set "
				+ "TipEstadoEjecucion= ?, FecActualizacionTabla= ? where CodEjecucion = ?";

		try {
			conn = obtenerConexion();

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);

			pstmt_ejecucion.setInt(1, estado);
			pstmt_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
			pstmt_ejecucion.setInt(3, ejecucion.getId_ejecucion());

			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar estado en ejecucion : " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	public void editarAvanceEjecucion(EjecucionMotor ejecucion, Integer avance) throws Exception {

		Connection conn = null;

		String sql_ejecucion = "Update HE_Ejecucion set "
				+ "NumEjecucionAvance= ?, FecActualizacionTabla= ? where CodEjecucion = ?";

		try {
			conn = obtenerConexion();

			for (GrupoBloquesEscenario grupo : ejecucion.getGrupos_bloquesParamEscenario()) {
				try (PreparedStatement pstmt_ejecucion = conn.prepareStatement(sql_ejecucion)) {

					pstmt_ejecucion.setInt(1, avance);
					pstmt_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
					pstmt_ejecucion.setInt(3, grupo.getId_ejecucion());

					pstmt_ejecucion.executeUpdate();
				}
			}
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar el avance en ejecucion agregada: " + ex.getMessage());
			throw ex;

		} finally {

			cerrarConexion(conn);
		}
	}

	public void editarAvanceEjecucion(EjecucionMotorOtro ejecucion, Integer avance) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		String sql_ejecucion = "Update HE_Ejecucion set "
				+ "NumEjecucionAvance= ?, FecActualizacionTabla= ? where CodEjecucion = ?";

		try {
			conn = obtenerConexion();

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);

			pstmt_ejecucion.setInt(1, avance);
			pstmt_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
			pstmt_ejecucion.setInt(3, ejecucion.getId_ejecucion());

			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar el avance en ejecucion de tipo otro: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	public Integer obtenerNroEstadosEjecucion(Integer tipoEstadoEjecucion) throws Exception {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		Integer resultado = 0;

		String sql = "select count(*) as conteo from ("
				+ " select e.tipEstadoEjecucion, e.codmes,jev.codjuegoEscenarios " + " from HE_Ejecucion e"
				+ " left join he_escenarioversion ev on e.codescenarioversion=ev.codescenarioversion "
				+ " left join HE_juegoEscenariosVersion jev on jev.codjuegoescenarios=ev.codjuegoescenarios "
				+ " group by e.tipEstadoEjecucion, e.codmes,jev.codjuegoEscenarios)" + " where tipEstadoEjecucion= "
				+ tipoEstadoEjecucion;
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				resultado = rs.getInt("conteo");
			}
		} catch (Exception ex) {
			LOGGER.error(
					"Error al obtener numero estados tipo ejecucion " + tipoEstadoEjecucion + ": " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return resultado;
	}

	public void ejecutarCalculoDirecto(EjecucionMotor ejecucionMotor) throws Exception {
		ArrayList<String> arrayComunInicializacion = new ArrayList<String>();

		ArrayList<String> arrayEscenarioInicializacion = new ArrayList<String>();
		Connection conn = null;
		PreparedStatement pstmt_ejecucionDirecta = null;
		ArrayList<String> array_nombres_campos_insert = null;
		ArrayList<String> array_nombres_campos_select = null;
		String sql_registro_nombres_campos_insert = null;
		String sql_registro_nombres_campos_select = null;

		String nombre_tabla_no_normalizada = null;
		String sql_ejecucionDirecta = null;
		ArrayList<CampoParametrizado> camposParametrizados = null;
		String[] logLines = new String[2];
		Date fechaActual = new Date(System.currentTimeMillis());
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		logLines[0] = dtf.format(now) + " Inicio de carga directa ";
		arrayComunInicializacion.add("No aplica log de inicialización porque es carga directa. ");
		arrayEscenarioInicializacion.add("");
		try {
			conn = obtenerConexion();

			for (GrupoBloquesEscenario grupo : ejecucionMotor.getGrupos_bloquesParamEscenario()) {

				// Si es de ambito inversiones, escribe resultados unicamente para escenario
				// base
				if (ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
						.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Creditos")
						|| (ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia()
								.getCartera().getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Inversiones")
								&& grupo.getEscenario_version().getEscenario().getFlag_obligatorio())) {

					array_nombres_campos_insert = new ArrayList<String>();
					array_nombres_campos_select = new ArrayList<String>();
					camposParametrizados = grupo.getBloquesParam_escenarioVersion().get(0).getCampos_parametrizado();
					for (CampoParametrizado campoParametrizado : camposParametrizados) {
						if (campoParametrizado.getCampo_input().getNombre_campo() != null) {
							array_nombres_campos_insert.add(campoParametrizado.getCampo_bloque().getVariable_motor());
							array_nombres_campos_select
									.add("tabInp." + campoParametrizado.getCampo_input().getNombre_campo());
						}
					}
					sql_registro_nombres_campos_insert = String.join(",", array_nombres_campos_insert);
					sql_registro_nombres_campos_select = String.join(",", array_nombres_campos_select);

					nombre_tabla_no_normalizada = userName + ".HE_"
							+ grupo.getBloquesParam_escenarioVersion().get(0).getTabla_input().getNombre_fichero()
							+ "_V"
							+ grupo.getBloquesParam_escenarioVersion().get(0).getTabla_input().getVersion_metadata();

					sql_ejecucionDirecta = "insert into " + esquema + ".HE_OUTPUTPROVISIONES"
							+ (ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia()
									.getCartera().getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Creditos")
											? "CRED"
											: "INV")
							+ " (codejecucion,FECEJECUCION,NUMREGEJECUCION,codescenarioversion,codtipoejecucion,codjuegoescenarios,codcarteraversion, "
							+ "codescenario,codcartera,codentidad,fecactualizacion,"
							+ sql_registro_nombres_campos_insert + ")" + "select ?,?,ROWNUM,?,?,?,?,?,?,?,?,"
							+ sql_registro_nombres_campos_select + " from " + nombre_tabla_no_normalizada
							+ " tabInp where tabInp.codcargafichero =?"
							+ (grupo.getBloquesParam_escenarioVersion().get(0).getFiltro_tabla() != null
									? " and (" + grupo.getBloquesParam_escenarioVersion().get(0).getFiltro_tabla() + ")"
									: "");
					//LOGGER.info(sql_ejecucionDirecta);
					pstmt_ejecucionDirecta = conn.prepareStatement(sql_ejecucionDirecta);
					pstmt_ejecucionDirecta.setInt(1, grupo.getId_ejecucion());
					pstmt_ejecucionDirecta.setInt(2, ejecucionMotor.getCodmes());
					// pstmt_ejecucionDirecta.setInt(2, ejecucionMotor.getCodmes());
					pstmt_ejecucionDirecta.setInt(3, grupo.getEscenario_version().getId_escenario_version());
					pstmt_ejecucionDirecta.setInt(4, ejecucionMotor.getTipo_ejecucion().getId_tipo_ejecucion());
					pstmt_ejecucionDirecta.setInt(5,
							ejecucionMotor.getJuego_escenarios_version().getId_juego_escenarios());
					pstmt_ejecucionDirecta.setInt(6,
							ejecucionMotor.getJuego_escenarios_version().getCartera_version().getId_cartera_version());
					pstmt_ejecucionDirecta.setInt(7, grupo.getEscenario_version().getEscenario().getId_escenario());
					pstmt_ejecucionDirecta.setInt(8, ejecucionMotor.getJuego_escenarios_version().getCartera_version()
							.getMetodologia().getCartera().getId_cartera());
					pstmt_ejecucionDirecta.setInt(9, ejecucionMotor.getJuego_escenarios_version().getCartera_version()
							.getMetodologia().getCartera().getEntidad().getId_entidad());

					pstmt_ejecucionDirecta.setDate(10, new Date(System.currentTimeMillis()));

					pstmt_ejecucionDirecta.setInt(11,
							grupo.getBloquesParam_escenarioVersion().get(0).getCarga().getId_cargafichero());

					pstmt_ejecucionDirecta.executeUpdate();
					cerrarPreparedStatement(pstmt_ejecucionDirecta);
					LocalDateTime nowEnd = LocalDateTime.now();
					logLines[1] = dtf.format(nowEnd) + " Fin  de carga directa ";
					guardarLogEjecucionPE(logLines, grupo.getId_ejecucion(), false, "no pwd");

					guardarInicializacionEjecucionPE(arrayComunInicializacion, arrayEscenarioInicializacion,
							grupo.getId_ejecucion());

				}

			}
			ejecutarCommit(conn);

			if (this.obtenerEscenarios().size() == ejecucionMotor.getGrupos_bloquesParamEscenario().size()) {
				this.editarEstadoEjecucion(ejecucionMotor, 2);
			} else {
				this.editarEstadoEjecucion(ejecucionMotor, 3);
			}

			this.editarAvanceEjecucion(ejecucionMotor, 100);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al ejecutar calculo directo: " + ex.getMessage());
			throw ex;

		} finally {

			cerrarPreparedStatement(pstmt_ejecucionDirecta);
			cerrarConexion(conn);
		}

	}

	public void guardarLogEjecucionPE(String[] _log, Integer codEjecucion, Boolean existePwd, String txtPwd)
			throws Exception {

		Connection conn = null;
		String logsGuardar = null;

		PreparedStatement pstmt_ejecucion = null;
		String sql_ejecucion = "Update HE_Ejecucion set " + "DesLogMotor= ? where CodEjecucion = ?";

		try {
			conn = obtenerConexion();
			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);

			logsGuardar = String.join("\r\n", Arrays.asList(_log));

			// Ofuscamos pass de credenciales (en caso exista)
			if (existePwd.equals(true)) {
				logsGuardar = logsGuardar.replace(txtPwd, String.join("", Collections.nCopies(txtPwd.length(), "*")));
			}

			// pstmt_ejecucion.setString(1, String.join("\r\n", Arrays.asList(_log)));
			pstmt_ejecucion.setString(1, logsGuardar);
			pstmt_ejecucion.setInt(2, codEjecucion);

			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al guardar el log de la ejecucion " + codEjecucion + ": " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}

	}

	// -------------------------------------------------------------------
	// -------------------------EJECUCIONES OTROS-------------------------
	// -------------------------------------------------------------------

	public ArrayList<EstructuraMetodologica> obtenerEstructurasOtrosPorIdCartera(Integer id_cartera) throws Exception {
		ArrayList<EstructuraMetodologica> estructuras = null;
		EstructuraMetodologica estructura = null;
		TipoFlujo flujo = null;
		TipoObtencionResultados tipoObtencionResultados = null;
		Cartera cartera = null;
		Entidad entidad = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "Select Mc.Codmetodologia, Mc.Nbrmetodologia, Mc.Desmetodologia,Mc.Codtipoflujo, Mc.Codtipoobtenresult, "
				+ " C.Codcartera, C.Nbrcartera, E.Codentidad, E.Nbrentidad, Tf.NbrTipoFlujo "
				+ " From He_Metodologiacartera Mc " + " Left Join ME_TipoFlujo Tf on Mc.CodTipoFlujo = Tf.CodTipoFlujo "
				+ " Left Join He_Cartera C On Mc.Codcartera= C.Codcartera "
				+ " Left Join He_Entidad E On C.Codentidad= E.Codentidad "
				+ " Where Tf.NbrTipoFlujo = 'Otros' and Mc.Codcartera=" + id_cartera;

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			estructuras = new ArrayList<EstructuraMetodologica>();

			while (rs.next()) {
				estructura = new EstructuraMetodologica();
				estructura.setId_metodologia(rs.getInt("CodMetodologia"));
				estructura.setNombre_metodologia(rs.getString("NbrMetodologia"));
				estructura.setDescripcion_metodologia(rs.getString("DesMetodologia"));
				flujo = new TipoFlujo();
				flujo.setId_tipo_flujo(rs.getInt("Codtipoflujo"));
				estructura.setFlujo(flujo);
				tipoObtencionResultados = new TipoObtencionResultados();
				tipoObtencionResultados.setId_tipo_obtencion_resultados(rs.getInt("Codtipoobtenresult"));
				estructura.setMetodo_resultados(tipoObtencionResultados);
				estructura.setCarteras_extrapolacion(
						obtenerCarterasExtrapolacionporIdMetodologia(estructura.getId_metodologia(), conn));
				cartera = new Cartera();
				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));

				entidad = new Entidad();
				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				cartera.setEntidad(entidad);

				estructura.setCartera(cartera);

				estructuras.add(estructura);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener estructuras de flujo otros para la cartera con id " + id_cartera + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return estructuras;
	}

	public ArrayList<EjecucionOtro> obtenerEjecucionesOtros() throws Exception {
		ArrayList<EjecucionOtro> ejecuciones = new ArrayList<EjecucionOtro>();
		EjecucionOtro ejecucion = null;
		Cartera_version cartera_version = null;
		EstructuraMetodologica metodologia = null;
		MotorCalculo motor = null;
		TipoFlujo tipoFlujo = null;
		Cartera cartera = null;
		TipoAmbito tipoAmbito = null;
		TipoMagnitud tipoMagnitud = null;
		TipoCartera tipoCartera = null;
		Entidad entidad = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "select E.CodEjecucion, E.CodMes, E.TipEstadoEjecucion, E.NumEjecucionAvance, E.FecEjecucion,"
				+ " CV.CodCarteraVersion, CV.NbrVersion, "
				+ " MC.CodMetodologia, MC.NbrMetodologia, MC.NbrCodigoEstructura, MC.NbrFuncPlumbr, MC.NumPuertoPlumbr, "
				+ " MOC.CodMotor, MOC.NbrMotor, TF.CodTipoFlujo, TF.NbrTipoFlujo, "
				+ " C.CodCartera, C.NbrCartera, C.NbrVariableMotor as NbrVariableMotorCartera, C.NumOrden, C.FlgPd6Meses, "
				+ " TA.CodTipoAmbito, TA.NbrTipoAmbito, TM.CodTipoMagnitud, TM.NbrTipoMagnitud, TC.CodTipoCartera, TC.NbrTipoCartera,"
				+ " TC.NbrVariableMotor as NbrVariableMotorTipoCartera, "
				+ " E.CodEntidad, E.NbrEntidad, E.NbrVariableMotor as NbrVariableMotorEntidad "
				+ " from HE_Ejecucion E "
				+ " left join HE_CarteraVersion CV on E.CodCarteraVerOtros = CV.CodCarteraVersion "
				+ " left join HE_MetodologiaCartera MC on CV.CodMetodologia = MC.CodMetodologia "
				+ " left join ME_MotorCalculo MOC on MC.CodMotor = MOC.CodMotor"
				+ " left join ME_TipoFlujo TF on MC.CodTipoFlujo = TF.CodTipoFlujo "
				+ " left join HE_Cartera C on MC.CodCartera = C.CodCartera "
				+ " left join ME_TipoAmbito TA on C.CodTipoAmbito = TA.CodTipoAmbito "
				+ " left join ME_TipoMagnitud TM on C.CodTipoMagnitud = TM.CodTipoMagnitud "
				+ " left join ME_TipoCartera TC on C.CodTipoCartera = TC.CodTipoCartera "
				+ " left join HE_Entidad E on C.CodEntidad = E.CodEntidad " + " where E.CodTipoEjecucion in (6,10)" // Otros
																													// (incluido
																													// inversiones)
				+ " order by E.codmes desc";

		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ejecucion = new EjecucionOtro();
				cartera_version = new Cartera_version();
				metodologia = new EstructuraMetodologica();
				cartera = new Cartera();
				entidad = new Entidad();

				ejecucion.setId_ejecucion(rs.getInt("CodEjecucion"));
				ejecucion.setCodmes(rs.getInt("CodMes"));
				ejecucion.setEstado_ejecucion(rs.getInt("TipEstadoEjecucion"));
				ejecucion.setPorcentaje_avance(rs.getInt("NumEjecucionAvance")); // Se utiliza en estado: en
																					// ejecucion
				ejecucion.setDatetime_ejecucion(rs.getLong("FecEjecucion"));

				cartera_version.setId_cartera_version(rs.getInt("CodCarteraVersion"));
				cartera_version.setNombre_cartera_version(rs.getString("NbrVersion"));

				metodologia.setId_metodologia(rs.getInt("CodMetodologia"));
				metodologia.setNombre_metodologia(rs.getString("NbrMetodologia"));
				metodologia.setCodigo_estructura(rs.getString("NbrCodigoEstructura"));
				metodologia.setNombre_funcPlumbr(rs.getString("NbrFuncPlumbr"));
				metodologia.setPuerto_plumbr(rs.getInt("NumPuertoPlumbr"));

				motor = new MotorCalculo();
				motor.setId_motor(rs.getInt("CodMotor"));
				motor.setNombre_motor(rs.getString("NbrMotor"));
				metodologia.setMotor(motor);

				tipoFlujo = new TipoFlujo();
				tipoFlujo.setId_tipo_flujo(rs.getInt("CodTipoFlujo"));
				tipoFlujo.setNombre_tipo_flujo(rs.getString("NbrTipoFlujo"));
				metodologia.setFlujo(tipoFlujo);

				cartera.setId_cartera(rs.getInt("CodCartera"));
				cartera.setNombre_cartera(rs.getString("NbrCartera"));
				cartera.setNombre_variablemotor(rs.getString("NbrVariableMotorCartera"));
				cartera.setOrden(rs.getInt("NumOrden"));
				cartera.setFlag_pd6meses(rs.getBoolean("FlgPd6Meses"));

				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				cartera.setTipo_ambito(tipoAmbito);

				tipoMagnitud = new TipoMagnitud();
				tipoMagnitud.setId_tipo_magnitud(rs.getInt("CodTipoMagnitud"));
				tipoMagnitud.setNombre_tipo_magnitud(rs.getString("NbrTipoMagnitud"));
				cartera.setTipo_magnitud(tipoMagnitud);

				tipoCartera = new TipoCartera();
				tipoCartera.setId_tipo_cartera(rs.getInt("CodTipoCartera"));
				tipoCartera.setNombre_tipo_cartera(rs.getString("NbrTipoCartera"));
				tipoCartera.setNombre_variablemotor(rs.getString("NbrVariableMotorTipoCartera"));
				cartera.setTipo_cartera(tipoCartera);

				entidad.setId_entidad(rs.getInt("CodEntidad"));
				entidad.setNombre_entidad(rs.getString("NbrEntidad"));
				entidad.setNombre_variablemotor(rs.getString("NbrVariableMotorEntidad"));
				cartera.setEntidad(entidad);

				metodologia.setCartera(cartera);

				cartera_version.setMetodologia(metodologia);

				ejecucion.setCartera_version(cartera_version);

				ejecuciones.add(ejecucion);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener ejecuciones de flujo otros: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return ejecuciones;
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionOtrosPorCartVer(Integer id_cartera_version)
			throws Exception {
		ArrayList<TablaMapeoEjecucion> tablasMapeo = new ArrayList<TablaMapeoEjecucion>();
		Connection conn = null;

		String sql = "select MTB.CodMapeoTabla," + " TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata, "
				+ " A.CodAprovisionamiento, A.NbrFichero," + " B.CodBloque, B.NbrBloque, B.NbrVariableMotor "
				+ " from HE_MapeoTablaBloque MTB "
				+ " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput"
				+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
				+ " left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
				+ " left join ME_TipoBloque TB on B.CodTipoBloque = TB.CodTipoBloque "
				+ " where MTB.CodEscenarioVersion is null and B.FlgTablaInput = 1 " + " and MTB.CodCarteraVersion = "
				+ id_cartera_version;

		conn = obtenerConexion();
		tablasMapeo = obtenerTablasMapeo(sql, 1, true, conn, false, true);
		cerrarConexion(conn);

		return tablasMapeo;
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionOtros(Integer id_ejecucion) throws Exception {
		ArrayList<TablaMapeoEjecucion> tablasMapeo = new ArrayList<TablaMapeoEjecucion>();
		Connection conn = null;

		String sql = "select ME.CodMapeoTabla," + " TI.CodTablaInput, TI.NumVersionMetadata, TI.NbrMetadata,"
				+ " A.CodAprovisionamiento, A.NbrFichero,"
				+ " CF.CodCargaFichero, CF.NbrCarga, CF.NumOrdenInicial, CF.NumOrdenFinal, CF.DesComentarioCalidad, CF.NumVersionCarga,"
				+ " F.CodFecha, F.NumCodMes," + " B.CodBloque, B.NbrBloque, B.NbrVariableMotor "
				+ " from HE_MapeoEjecucion ME"
				+ " left join HE_MapeoTablaBloque MTB on ME.CodMapeoTabla = MTB.CodMapeoTabla"
				+ " left join HE_TablaInput TI on MTB.CodTablaInput= TI.CodTablaInput"
				+ " left join HE_Bloque B on MTB.CodBloque = B.CodBloque "
				+ " left join HE_Aprovisionamiento A on TI.CodAprovisionamiento= A.CodAprovisionamiento"
				+ " left join HE_CargaFichero CF on CF.CodCargaFichero = ME.CodCargaFichero"
				+ " left join HE_Fecha F on CF.CodFecha = F.CodFecha" + " where MTB.CodEscenarioVersion is null"
				+ " and ME.CodEjecucion = " + id_ejecucion;

		conn = obtenerConexion();
		tablasMapeo = obtenerTablasMapeo(sql, 1, true, conn, true, true);
		cerrarConexion(conn);

		return tablasMapeo;
	}

	public void crearEjecucionOtro(EjecucionOtro ejecucion) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		PreparedStatement pstmt_mapeo_ejecucion = null;
		Integer id_ejecucion = null;

		String sql_ejecucion = "Insert into HE_Ejecucion(CodEjecucion, CodMes, CodEscenarioVersion, CodTipoEjecucion,"
				+ " TipEstadoEjecucion, TipEstadoValidacion, TipEstadoAjustes, DesComentarioEjecucion, FecEjecucion,"
				+ " CodMetodologia, CodPrimerPaso, CodEjecucionVar, FlgRegulatorio, CodEjecucionModMac,"
				+ " CodEjecucionRepricing, CodCarteraVerOtros, FecActualizacionTabla) "
				+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		String sql_mapeo_ejecucion = "Insert into HE_MapeoEjecucion(CodEjecucion, CodMapeoTabla, CodCargaFichero,"
				+ " FecActualizacionTabla) values (?,?,?,?)";
		try {
			conn = obtenerConexion();
			id_ejecucion = obtenerSiguienteId("SEQ_HE_Ejecucion");

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
			pstmt_ejecucion.setInt(1, id_ejecucion);
			pstmt_ejecucion.setInt(2, ejecucion.getCodmes());
			pstmt_ejecucion.setNull(3, java.sql.Types.INTEGER);

			if (ejecucion.getCartera_version().getMetodologia().getCartera().getTipo_ambito().getId_tipo_ambito()
					.equals(2)) { // Inversiones
				pstmt_ejecucion.setInt(4, 10); // Tipo de ejecucion: Otros inv
			} else if (ejecucion.getCartera_version().getMetodologia().getCartera().getTipo_ambito().getId_tipo_ambito()
					.equals(1)) { // Creditos
				pstmt_ejecucion.setInt(4, 6); // Tipo de ejecucion: Otros cred
			}

			pstmt_ejecucion.setInt(5, ejecucion.getEstado_ejecucion());
			pstmt_ejecucion.setInt(6, 0); // Estado validacion: Validacion pendiente
			pstmt_ejecucion.setInt(7, 0); // Estado ajuste: Sin ajuste
			pstmt_ejecucion.setString(8, ""); // Sin comentarios
			pstmt_ejecucion.setLong(9, ejecucion.getDatetime_ejecucion());
			pstmt_ejecucion.setNull(10, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(11, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(12, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(13, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(14, java.sql.Types.INTEGER);
			pstmt_ejecucion.setNull(15, java.sql.Types.INTEGER);
			pstmt_ejecucion.setInt(16, ejecucion.getCartera_version().getId_cartera_version());
			pstmt_ejecucion.setDate(17, new Date(System.currentTimeMillis()));

			pstmt_ejecucion.executeUpdate();

			for (TablaMapeoEjecucion tabla_mapeo_ejecucion : ejecucion.getTablas_mapeo_ejecucion()) {
				pstmt_mapeo_ejecucion = conn.prepareStatement(sql_mapeo_ejecucion);
				pstmt_mapeo_ejecucion.setInt(1, id_ejecucion);
				pstmt_mapeo_ejecucion.setInt(2, tabla_mapeo_ejecucion.getId_mapeo_tabla());
				pstmt_mapeo_ejecucion.setInt(3, tabla_mapeo_ejecucion.getCarga().getId_cargafichero());
				pstmt_mapeo_ejecucion.setDate(4, new Date(System.currentTimeMillis()));

				pstmt_mapeo_ejecucion.executeUpdate();
				cerrarPreparedStatement(pstmt_mapeo_ejecucion);
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al agregar una ejecucion de tipo otros : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarPreparedStatement(pstmt_mapeo_ejecucion);
			cerrarConexion(conn);
		}
	}

	public ReplicaEjecucionesOtros replicarEjecucionesOtros(ReplicaEjecucionesOtros replicaEjecuciones)
			throws Exception {
		for (EjecucionOtro ejecucion : replicaEjecuciones.getEjecuciones_replicar()) {
			ejecucion.setEstado_ejecucion(0); // Seteamos el estado: no ejecutado
			ejecucion.setCodmes(replicaEjecuciones.getCodmes_replica());
			ejecucion
					.setTablas_mapeo_ejecucion(this.obtenerTablasMapeoEjecucionPrimerPaso(ejecucion.getId_ejecucion()));
			this.crearEjecucionOtro(ejecucion);
		}

		// Returnamos replica de ejecuciones con mapeos completados
		return replicaEjecuciones;
	}

	public void editarEjecucionOtro(EjecucionOtro ejecucion) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		PreparedStatement pstmt_mapeo_ejecucion = null;
		String sql_ejecucion = "Update HE_Ejecucion set CodMes= ?, CodCarteraVerOtros = ?, "
				+ "CodTipoEjecucion= ?, TipEstadoEjecucion= ?, FecEjecucion= ?, "
				+ "FecActualizacionTabla= ? where CodEjecucion = ?";

		String sql_mapeo_ejecucion = "Update HE_MapeoEjecucion set CodCargaFichero= ?, FecActualizacionTabla= ?"
				+ " where CodEjecucion= ? and CodMapeoTabla= ?";

		try {
			conn = obtenerConexion();
			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);

			pstmt_ejecucion.setInt(1, ejecucion.getCodmes());
			pstmt_ejecucion.setInt(2, ejecucion.getCartera_version().getId_cartera_version());

			if (ejecucion.getCartera_version().getMetodologia().getCartera().getTipo_ambito().getId_tipo_ambito()
					.equals(2)) { // Inversiones
				pstmt_ejecucion.setInt(3, 10); // Tipo de ejecucion: Otros inv
			} else if (ejecucion.getCartera_version().getMetodologia().getCartera().getTipo_ambito().getId_tipo_ambito()
					.equals(1)) { // Creditos
				pstmt_ejecucion.setInt(3, 6); // Tipo de ejecucion: Otros cred
			}

			pstmt_ejecucion.setInt(4, ejecucion.getEstado_ejecucion());
			pstmt_ejecucion.setLong(5, ejecucion.getDatetime_ejecucion());
			pstmt_ejecucion.setDate(6, new Date(System.currentTimeMillis()));
			pstmt_ejecucion.setInt(7, ejecucion.getId_ejecucion());
			pstmt_ejecucion.executeUpdate();

			if (ejecucion.getTablas_mapeo_ejecucion() != null) {
				for (TablaMapeoEjecucion tabla_mapeo_ejecucion : ejecucion.getTablas_mapeo_ejecucion()) {
					pstmt_mapeo_ejecucion = conn.prepareStatement(sql_mapeo_ejecucion);
					pstmt_mapeo_ejecucion.setInt(1, tabla_mapeo_ejecucion.getCarga().getId_cargafichero());
					pstmt_mapeo_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
					pstmt_mapeo_ejecucion.setInt(3, ejecucion.getId_ejecucion());
					pstmt_mapeo_ejecucion.setInt(4, tabla_mapeo_ejecucion.getId_mapeo_tabla());

					pstmt_mapeo_ejecucion.executeUpdate();
					cerrarPreparedStatement(pstmt_mapeo_ejecucion);
				}
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar ejecucion de tipo otros : " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarPreparedStatement(pstmt_mapeo_ejecucion);
			cerrarConexion(conn);
		}
	}

	public void borrarEjecucionOtros(Integer id_ejecucion) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		String sql_ejecucion = "DELETE from HE_Ejecucion where CodEjecucion=? and CodTipoEjecucion in (6,10)";

		try {
			conn = obtenerConexion();

			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);
			pstmt_ejecucion.setInt(1, id_ejecucion);
			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error al borrar una ejecucion de tipo otros con id: " + id_ejecucion + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	public void guardarLogEjecucion(EjecucionMotor ejecucion, String log) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;
		String sql_ejecucion = "Update HE_Ejecucion set "
				+ "DesLogMotor= ?, FecActualizacionTabla= ? where CodEjecucion = ?";

		try {
			conn = obtenerConexion();

			for (GrupoBloquesEscenario grupo : ejecucion.getGrupos_bloquesParamEscenario()) {
				pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);

				// Para inversiones, las ejecuciones que no son base no tienen log
				if (ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
						.getTipo_ambito().getNombre_tipo_ambito().equals("Inversiones")
						&& !grupo.getEscenario_version().getEscenario().getFlag_obligatorio()) {
					pstmt_ejecucion.setNull(1, java.sql.Types.VARCHAR);
				} else {
					pstmt_ejecucion.setString(1, log);
				}

				// pstmt_ejecucion.setString(1, log);
				pstmt_ejecucion.setDate(2, new Date(System.currentTimeMillis()));
				pstmt_ejecucion.setInt(3, grupo.getId_ejecucion());

				pstmt_ejecucion.executeUpdate();
				cerrarPreparedStatement(pstmt_ejecucion);
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al guardar el log de la ejecucion agregada: " + ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	// Para campos calculados con dependencia
	public void actualizarAnchoTablaInput(Integer valor, Integer id, Connection conn) throws Exception {
		PreparedStatement pstmt_numAnchoAnt = null;
		String sql_numAnchoAnt = "select NumAncho from HE_CampoInput where CodCampo = ?";
		ResultSet rs_numAnchoAnt = null;
		Integer numAnchoAnt = null;

		PreparedStatement pstmt = null;
		String sql = "UPDATE HE_CAMPOINPUT set NUMANCHO= ?  where CODCAMPO=?";
		try {
			// Obtenemos numAncho seteado anteriormente
			pstmt_numAnchoAnt = conn.prepareStatement(sql_numAnchoAnt);
			pstmt_numAnchoAnt.setInt(1, id);
			rs_numAnchoAnt = pstmt_numAnchoAnt.executeQuery();

			while (rs_numAnchoAnt.next()) {
				numAnchoAnt = rs_numAnchoAnt.getInt("NumAncho");
			}

			if (valor > numAnchoAnt) {
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, valor);
				pstmt.setInt(2, id);
				pstmt.executeUpdate();
				ejecutarCommit(conn);
			}
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar el ancho del campo  con id_Campo_Input: " + id + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_numAnchoAnt);
			cerrarPreparedStatement(pstmt);
			cerrarPreparedStatement(pstmt_numAnchoAnt);
		}
	}

	// Manejo de tokens (unico usuario concurrente)
	public Boolean verificarTokenAcceso(String id_usuario, String token) throws Exception {
		Boolean tokenCorrecto = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String tokenDB = null;
		String sql = "select NbrTokenSesion from HE_SesionUsuario where NbrIdentUsuario = ?";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id_usuario);
			rs = pstmt.executeQuery();

			tokenCorrecto = false;
			while (rs.next()) {
				tokenDB = rs.getString("NbrTokenSesion");
				if (tokenDB.equals(token)) {
					tokenCorrecto = true;
					break;
				}
			}

			if (!tokenCorrecto) {
				LOGGER.info("Token eval: " + token + " Longitud" + token.length());
				LOGGER.info("Token db: " + tokenDB + " Longitud" + tokenDB.length());
			}

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al verificar el token de acceso: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}

		return tokenCorrecto;
	}

	public Boolean verificarExpiracionToken(String id_usuario) throws Exception {
		Boolean tokenNoExpirado = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select FecLimiteToken from HE_SesionUsuario where NbrIdentUsuario = ?";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id_usuario);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				if (System.currentTimeMillis() >= rs.getLong("FecLimiteToken")) {
					tokenNoExpirado = false;
				} else {
					tokenNoExpirado = true;
				}
				break;
			}

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al verificar expiracion de token: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}

		return tokenNoExpirado;
	}

	public void actualizarExpiracionSesion(String id_usuario, Long timeStmpLimite) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Update HE_SesionUsuario set FecLimiteToken=?, "
				+ "FecActualizacionTabla=? where NbrIdentUsuario=?";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, timeStmpLimite);
			pstmt.setDate(2, new Date(System.currentTimeMillis()));
			pstmt.setString(3, id_usuario);
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al actualizar expiracion de sesion : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void reiniciarSesionUsuario(String id_usuario) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "Update HE_SesionUsuario set NbrTokenSesion=?, FecCreacionToken=?, FecLimiteToken=?, "
				+ "FecActualizacionTabla=? where NbrIdentUsuario=?";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, "");
			pstmt.setLong(2, 0);
			pstmt.setLong(3, 0);
			pstmt.setDate(4, new Date(System.currentTimeMillis()));
			pstmt.setString(5, id_usuario);
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al reiniciar sesion del usuario : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void registrarSesionUsuario(String id_usuario, String tokenLogueo) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_registro = null;
		PreparedStatement pstmt_actualiza = null;
		PreparedStatement pstmt_consultaExist = null;
		ResultSet rs_consultaExist = null;

		Boolean existeSesion = null;

		String sql_registro = "Insert into HE_SesionUsuario (CodSesionUsuario, NbrIdentUsuario, NbrTokenSesion, "
				+ "FecCreacionToken, FecLimiteToken, FecActualizacionTabla) values (?,?,?,?,?,?)";
		String sql_actualiza = "Update HE_SesionUsuario set NbrTokenSesion=?, FecCreacionToken=?, FecLimiteToken=?, "
				+ "FecActualizacionTabla=? where NbrIdentUsuario=?";
		String sql_consultaExist = "select CodSesionUsuario from HE_SesionUsuario where NbrIdentUsuario = ?";

		try {
			conn = obtenerConexion();

			// Verificamos si la sesion del usuario ya esta registrada
			pstmt_consultaExist = conn.prepareStatement(sql_consultaExist);
			pstmt_consultaExist.setString(1, id_usuario);
			rs_consultaExist = pstmt_consultaExist.executeQuery();

			existeSesion = false;
			while (rs_consultaExist.next()) {
				existeSesion = true;
				break;
			}

			if (!existeSesion) { // Registramos si no existe
				pstmt_registro = conn.prepareStatement(sql_registro);
				pstmt_registro.setInt(1, obtenerSiguienteId("SEQ_HE_SesionUsuario"));
				pstmt_registro.setString(2, id_usuario);
				pstmt_registro.setString(3, tokenLogueo);
				pstmt_registro.setLong(4, System.currentTimeMillis());
				pstmt_registro.setLong(5, System.currentTimeMillis() + PropertiesCompartido.lifetimeSesion);
				pstmt_registro.setDate(6, new Date(System.currentTimeMillis()));
				pstmt_registro.executeUpdate();
				ejecutarCommit(conn);

			} else { // Actualizamos existente
				pstmt_actualiza = conn.prepareStatement(sql_actualiza);
				pstmt_actualiza.setString(1, tokenLogueo);
				pstmt_actualiza.setLong(2, System.currentTimeMillis());
				pstmt_actualiza.setLong(3, System.currentTimeMillis() + PropertiesCompartido.lifetimeSesion);
				pstmt_actualiza.setDate(4, new Date(System.currentTimeMillis()));
				pstmt_actualiza.setString(5, id_usuario);
				pstmt_actualiza.executeUpdate();
				ejecutarCommit(conn);
			}

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al registrar sesion de usuario : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs_consultaExist);
			cerrarPreparedStatement(pstmt_consultaExist);
			cerrarPreparedStatement(pstmt_registro);
			cerrarPreparedStatement(pstmt_actualiza);
			cerrarConexion(conn);
		}
	}

	// Replica de estructura con versiones de cartera
	public void replicarEstructuraConCarterasVer(ReplicaEstructuraConCarVer replicaEstructuraConCarVer)
			throws Exception {

		Connection conn = null;
		conn = obtenerConexion();

		Integer id_metodologia_replicado = null;
		EstructuraMetodologica metodologia_conId = null;

		try {
			// Creamos la estructura replicada
			id_metodologia_replicado = this.crearMetodologia(replicaEstructuraConCarVer.getEstructura_replicar(), conn); // id
																															// nuevo

			// Completamos objeto de metolodogia
			metodologia_conId = replicaEstructuraConCarVer.getEstructura_replicar();
			metodologia_conId.setId_metodologia(id_metodologia_replicado);

			// Creamos las carteras version
			for (CarteraVersion_staging carteraVersionRep : replicaEstructuraConCarVer.getCarterasVer_replicar()) {
				// Completamos juegos de escenario
				carteraVersionRep.setJuegos_versionesEscenario(this
						.obtenerJuegosConEscenariosPorCarteraVersion(carteraVersionRep.getId_cartera_version(), conn));

				// Seteamos nueva metodologia
				carteraVersionRep.setMetodologia(metodologia_conId);

				// Replicamos cartera version
				this.replicarCarteraVersion(carteraVersionRep, true);
			}
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al replicar metodologia con carteras version :" + ex.getMessage());
			throw ex;
		} finally {
			cerrarConexion(conn);
		}
	}

// ------------ Equivalente de ejecucion calidad en SQL ---------------- //
	public void ejecutarCalidadDatos(CargaFichero carga) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// Archivo logs
		SimpleDateFormat dfLogCal = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss");
		Date timestmp = new Date(System.currentTimeMillis());
		String rutaLog = null;
		String fileLog = null;
		PrintWriter writer = null;

		// Variables
		ArrayList<CampoAprovisionamiento> camposTotal = null;
		CampoAprovisionamiento campoLlave = null;
		CampoAprovisionamiento campoSegm = null;
		ArrayList<CampoAprovisionamiento> camposEvid = null;
		ArrayList<SegmentoAprovisionamiento> segmentos = null;
		SegmentoAprovisionamiento segmentoSinSeg = null;
		ArrayList<CargaFichero> cargasAnt = null;
		CargaFichero cargaAnt = null;
		Integer codmesAnt = null;
		Boolean aplicaTablaAnt = null;
		Boolean aplicaTablaHist = null;
		Integer cuentaInputAct = null;
		Integer cuentaCampos = null;
		String prefTblsTemp = null;

		// Variables bucle segmentos
		Integer totalRegBucSeg = null;
		Boolean aplicaPruebasBucSeg = null;
		// Integer idxBucSeg = null;
		ArrayList<String> listaValoresSeg = null;
		Integer cuentaInputSinSeg = null;
		ArrayList<ReglaCalidadConCampo> reglasSegInd = null;
		Double valorInd = null;
		Boolean aplicaLimInf = null;
		Boolean aplicaLimSup = null;
		Double valorAct = null;
		Double valorAnt = null;
		Boolean campoSegEsTxt = null;

		try {
			conn = obtenerConexion();

			// Creamos archivo de logs
			rutaLog = rutaLogsCalidad + "\\fichero_" + carga.getTablaInput().getNombre_archivo() + "\\metadata_"
					+ carga.getTablaInput().getNombre_metadata() + "\\carga_" + carga.getNombre_carga();

			fileLog = rutaLog + "\\" + dfLogCal.format(timestmp) + ".txt";

			new File(rutaLog).mkdirs();
			new File(fileLog).createNewFile();
			writer = new PrintWriter(new FileOutputStream(fileLog, true));

			// Logueamos valores de ejecucion
			writer.println("*** Valores de la ejecucion de calidad");
			writer.println("codigo de carga: " + carga.getId_cargafichero());
			writer.println("codigo de aprovisionamiento: " + carga.getTablaInput().getId_aprovisionamiento());
			writer.println("nombre de tabla: " + carga.getTablaInput().getNombre_metadata());
			writer.println("codigo de tabla: " + carga.getTablaInput().getId_tablainput());
			writer.println("numero de version de metadata: " + carga.getTablaInput().getVersion_metadata());
			writer.println("fecha actual: " + carga.getFecha().getCodmes());
			writer.println("fase de ejecucion: " + carga.getFecha().getFase());

			camposTotal = this.obtenerCamposAprovPorIdAprov(carga.getTablaInput().getId_aprovisionamiento(), conn);
			camposEvid = new ArrayList<CampoAprovisionamiento>();
			for (CampoAprovisionamiento cmp : camposTotal) {
				if (cmp.getFlag_llave().equals(true)) { // Obtenemos campo llave
					campoLlave = cmp;
				}
				if (cmp.getFlag_segmentar().equals(true)) { // Obtenemos campo de segmentacion (en caso se haya
															// configurado)
					campoSegm = cmp;
				}
				if (cmp.getFlag_evidencia().equals(true)) { // Obtenemos campos de evidencia
					camposEvid.add(cmp);
				}
			}

			if (camposEvid.size() != 0) {
				writer.println("Existen " + camposEvid.size() + " campos seleccionados para guardar en evidencia");
				writer.println("Se completaran " + (10 - camposEvid.size()) + " campos a la tabla de evidencia");

				// Creamos array equivalencias de nombres de campos con campos numerados
			} else {
				writer.println("No hay campos seleccionados para evidencia");
			}

			// Obtenemos segmentos
			segmentos = this.obtenerSegmentosPorAprov(carga.getTablaInput().getId_aprovisionamiento());

			// Buscamos el segmento 'sin segmento'
			for (SegmentoAprovisionamiento seg : segmentos) {
				if (seg.getFlag_sinsegmento().equals(true)) {
					segmentoSinSeg = seg;
					writer.println("El codigo 'sin segmento' es " + segmentoSinSeg.getId_segmento());
				}
			}

			// En su defecto, si se configuraron segmentos
			if (segmentoSinSeg == null && segmentos.size() > 1) {
				writer.println("El numero de segmentos es " + segmentos.size());
			}

			// Buscamos carga anterior
			if (carga.getFecha().getCodmes() % 100 != 1) {
				codmesAnt = carga.getFecha().getCodmes() - 1;
			} else {
				codmesAnt = ((carga.getFecha().getCodmes() / 100) - 1) * 100 + 12;
			}
			cargasAnt = this.obtenerCargasAnteriores(carga, codmesAnt, conn);

			// Buscamos inmediata anterior
			if (cargasAnt != null && cargasAnt.size() > 0) {
				for (CargaFichero c : cargasAnt) {
					if (c.getFecha().getCodmes().equals(codmesAnt)) {
						cargaAnt = c;
					}
				}
			}

			writer.println("El mes anterior buscado es " + codmesAnt + " para la fase " + carga.getFecha().getFase());

			if (cargaAnt != null) {
				writer.println("El codigo de la carga del mes anterior es " + cargaAnt.getId_cargafichero());
			}

			// Buscamos si hay reglas de variacion
			aplicaTablaAnt = false;
			loopAplicaTablaAnt: for (CampoAprovisionamiento cmp : camposTotal) {
				if (cmp.getReglas() != null && cmp.getReglas().size() > 0) {
					for (ReglaCalidad reg : cmp.getReglas()) {
						if (reg.getTipo_analisis().equals(2) && reg.getFlag_aplica().equals(true)) {
							aplicaTablaAnt = true;
							break loopAplicaTablaAnt;
						}
					}
				}
			}

			// Buscamos si hay reglas con historico
			aplicaTablaHist = false;
			loopAplicaTablaHist: for (CampoAprovisionamiento cmp : camposTotal) {
				if (cmp.getReglas() != null && cmp.getReglas().size() > 0) {
					for (ReglaCalidad reg : cmp.getReglas()) {
						if (reg.getTipo_indicador().getId_tipo_indicador().equals(23)
								&& reg.getFlag_aplica().equals(true)) {
							aplicaTablaHist = true;
							break loopAplicaTablaHist;
						}
					}
				}
			}

			// Definimos prefijo de tablas temporales creadas
			prefTblsTemp = "TMPCAL" + carga.getId_cargafichero() + "_";

			// Creamos tabla de datos actual
			this.crearTablaTempCalidad(prefTblsTemp + "InputAct", carga, false, null);

			// Obtenemos cuenta de registros de input actual (y de campos)
			cuentaInputAct = this.obtenerCuentaRegistrosTabla(prefTblsTemp + "InputAct", false, null, conn);
			writer.println("El total de registros del fichero es " + cuentaInputAct);
			cuentaCampos = this.obtenerCamposporTablaInputVersion(carga.getTablaInput(), conn).size();

			// Guardamos en tabla output total
			this.guardarOutputFicheroTotal(carga.getId_cargafichero(), 1, cuentaInputAct, cuentaCampos);

			// Creamos tabla de datos anterior
			if (aplicaTablaAnt.equals(true) && cargaAnt != null) {
				this.crearTablaTempCalidad(prefTblsTemp + "InputAnt", cargaAnt, false, null);
			} else {
				// Lo creamos como copia de la actual
				this.crearTablaTempCalidad(prefTblsTemp + "InputAnt", carga, false, null);
				writer.println(
						"No se ha encontrado una carga para el mes anterior, se utilizara una copia del mes actual");
			}

			// Creamos tabla de datos historica (o anterior total)
			if (aplicaTablaHist.equals(true) && cargasAnt != null) {
				this.crearTablaTempCalidad(prefTblsTemp + "InputAntTotal", null, true, cargasAnt);
			} else {
				// Lo creamos como copia de la actual
				this.crearTablaTempCalidad(prefTblsTemp + "InputAntTotal", carga, false, null);
				writer.println(
						"No se ha encontrado informacion para meses anteriores, se utilizara una copia del mes actual");
			}

			// Formamos lista de valores de segmentos (si es que fue configurado)
			listaValoresSeg = new ArrayList<String>();
			if (campoSegm != null && segmentos.size() > 1) {
				for (SegmentoAprovisionamiento seg : segmentos) { // No considerar el 'sin segmento'
					if (!seg.getDescripcion().equalsIgnoreCase("Sin Segmento")) {
						if (campoSegm.getTipo_campo().getId_tipo_campo().equals(1)) {
							listaValoresSeg.add(seg.getDescripcion());
						} else {
							listaValoresSeg.add("'" + seg.getDescripcion() + "'");
						}
					}
				}
			}

			// idxBucSeg = -1; //Indice de bucle
			for (SegmentoAprovisionamiento seg : segmentos) {
				// idxBucSeg += 1;
				writer.println("Comienzo de bucle, segmento : " + seg.getDescripcion());
				aplicaPruebasBucSeg = false;
				totalRegBucSeg = 0;

				// Vemos tipo del campo de segmentacion
				if (campoSegm != null) {
					if (campoSegm.getTipo_campo().getId_tipo_campo().equals(1)) {
						campoSegEsTxt = false;
					} else {
						campoSegEsTxt = true;
					}
				}

				// Cuando no tiene segmentos
				if (campoSegm == null && segmentos.size() == 1) {
					this.duplicarTablaTempCalidad(prefTblsTemp + "InputAct", prefTblsTemp + "InputActSeg", false, null);
					this.duplicarTablaTempCalidad(prefTblsTemp + "InputAnt", prefTblsTemp + "InputAntSeg", false, null);
					this.duplicarTablaTempCalidad(prefTblsTemp + "InputAntTotal", prefTblsTemp + "InputAntTotalSeg",
							false, null);

					writer.println("El total de registros del fichero sin segmentos es 0");

					// Guardamos en tabla output total
					this.guardarOutputFicheroTotal(carga.getId_cargafichero(), 2, 0, cuentaCampos);

					aplicaPruebasBucSeg = true;
					totalRegBucSeg = cuentaInputAct;
				} else if (seg.getFlag_sinsegmento().equals(true)) { // idxBucSeg.equals(0) Solo el primero?
					this.duplicarTablaTempCalidad(prefTblsTemp + "InputAct", prefTblsTemp + "InputSinSeg", true,
							campoSegm.getNombre_campo() + " not in (" + String.join(",", listaValoresSeg) + ")");
					cuentaInputSinSeg = this.obtenerCuentaRegistrosTabla(prefTblsTemp + "InputSinSeg", false, null,
							conn);

					writer.println("El total de registros del fichero sin segmentos es " + cuentaInputSinSeg);

					// Guardamos en tabla output total
					this.guardarOutputFicheroTotal(carga.getId_cargafichero(), 2, cuentaInputSinSeg, cuentaCampos);
				} else {
					this.duplicarTablaTempCalidad(prefTblsTemp + "InputAct", prefTblsTemp + "InputActSeg", true,
							campoSegm.getNombre_campo() + " in (" + (campoSegEsTxt ? "'" : "") + seg.getDescripcion()
									+ (campoSegEsTxt ? "'" : "") + ")");
					this.duplicarTablaTempCalidad(prefTblsTemp + "InputAnt", prefTblsTemp + "InputAntSeg", true,
							campoSegm.getNombre_campo() + " in (" + (campoSegEsTxt ? "'" : "") + seg.getDescripcion()
									+ (campoSegEsTxt ? "'" : "") + ")");
					this.duplicarTablaTempCalidad(prefTblsTemp + "InputAntTotal", prefTblsTemp + "InputAntTotalSeg",
							true, campoSegm.getNombre_campo() + " in (" + (campoSegEsTxt ? "'" : "")
									+ seg.getDescripcion() + (campoSegEsTxt ? "'" : "") + ")");

					aplicaPruebasBucSeg = true;
					totalRegBucSeg = this.obtenerCuentaRegistrosTabla(prefTblsTemp + "InputActSeg", false, null, conn);
				}

				if (aplicaPruebasBucSeg.equals(true)) {
					for (TipoIndicador tInd : dic_tipo_indicador) {
						writer.println("Comienza el bucle para el indicador " + tInd.getNombre_indicador());

						// Obtenemos reglas para el segmento e indicador
						reglasSegInd = this.obtenerReglasCalidadConCampoPorIdSegInd(seg.getId_segmento(),
								tInd.getId_tipo_indicador(), conn);

						// Recorremos reglas
						for (ReglaCalidadConCampo reg : reglasSegInd) {
							valorInd = 0.0;
							aplicaLimInf = false;
							aplicaLimSup = false;
							if (reg.getTipo_indicador().getId_tipo_indicador().equals(1)
									&& reg.getTipo_analisis().equals(1)) {
								// Indicador 1: Nro de registros con datos (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								valorInd = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "InputActSeg", true,
										reg.getCampo_aprov().getNombre_campo() + " is not null", conn)).doubleValue();
								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(2)
									&& reg.getTipo_analisis().equals(1)) {
								// Indicador 2: Porcentaje de registros con datos (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								valorInd = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "InputActSeg", true,
										reg.getCampo_aprov().getNombre_campo() + " is not null", conn)).doubleValue();
								valorInd = valorInd * 100.0 / totalRegBucSeg;
								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(3)
									&& reg.getTipo_analisis().equals(2)) {
								// Indicador 3: Variacion de registros con datos (variacion)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								valorAct = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "InputActSeg", true,
										reg.getCampo_aprov().getNombre_campo() + " is not null", conn)).doubleValue();
								valorAnt = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "InputAntSeg", true,
										reg.getCampo_aprov().getNombre_campo() + " is not null", conn)).doubleValue();

								writer.println("El valor anterior de referencia es " + valorAnt);
								if (valorAnt.equals(0.0)) {
									valorInd = 0.0;
								} else {
									valorInd = (valorAct - valorAnt) * 100.0 / valorAnt;
								}

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(4)
									&& reg.getTipo_analisis().equals(1)) {
								// Indicador 4: Nro de registros nulos (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								this.crearTablaTempEvid(prefTblsTemp + "TmpEvidValores", prefTblsTemp + "InputActSeg",
										reg.getCampo_aprov(), camposEvid, true,
										reg.getCampo_aprov().getNombre_campo() + " is null");

								valorInd = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpEvidValores", false,
										null, conn)).doubleValue() - 1;

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(5)
									&& reg.getTipo_analisis().equals(1)) {
								// Indicador 5: Porcentaje de registros nulos (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								this.crearTablaTempEvid(prefTblsTemp + "TmpEvidValores", prefTblsTemp + "InputActSeg",
										reg.getCampo_aprov(), camposEvid, true,
										reg.getCampo_aprov().getNombre_campo() + " is null");

								valorInd = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpEvidValores", false,
										null, conn)).doubleValue() - 1;
								valorInd = valorInd * 100.0 / totalRegBucSeg;

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(6)
									&& reg.getTipo_analisis().equals(2)) {
								// Indicador 6: Variacion de registros nulos (variacion)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								valorAct = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "InputActSeg", true,
										reg.getCampo_aprov().getNombre_campo() + " is null", conn)).doubleValue();
								valorAnt = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "InputAntSeg", true,
										reg.getCampo_aprov().getNombre_campo() + " is null", conn)).doubleValue();

								writer.println("El valor anterior de referencia es " + valorAnt);
								if (valorAnt.equals(0.0)) {
									valorInd = 0.0;
								} else {
									valorInd = (valorAct - valorAnt) * 100.0 / valorAnt;
								}

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(7)
									&& reg.getTipo_analisis().equals(1)) {
								// Indicador 7: Nro de duplicados solo para llave (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								this.crearTablaDuplicadosCalidad(prefTblsTemp + "TmpDupCal",
										prefTblsTemp + "InputActSeg", campoLlave);

								this.crearTablaTempEvid(prefTblsTemp + "TmpEvidValores", prefTblsTemp + "TmpDupCal",
										reg.getCampo_aprov(), camposEvid, false, null);

								valorInd = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpEvidValores", false,
										null, conn)).doubleValue() - 1;

								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(8)
									&& reg.getTipo_analisis().equals(1)) {
								// Indicador 8: Porcentaje de duplicados solo para llave (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								this.crearTablaDuplicadosCalidad(prefTblsTemp + "TmpDupCal",
										prefTblsTemp + "InputActSeg", campoLlave);

								this.crearTablaTempEvid(prefTblsTemp + "TmpEvidValores", prefTblsTemp + "TmpDupCal",
										reg.getCampo_aprov(), camposEvid, false, null);

								valorInd = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpEvidValores", false,
										null, conn)).doubleValue() - 1;
								valorInd = valorInd * 100.0 / totalRegBucSeg;

								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(9)
									&& reg.getTipo_analisis().equals(2)) {
								// Indicador 9: Variacion de duplicados solo para llave (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								this.crearTablaDuplicadosCalidad(prefTblsTemp + "TmpDupCalAct",
										prefTblsTemp + "InputActSeg", campoLlave);
								this.crearTablaDuplicadosCalidad(prefTblsTemp + "TmpDupCalAnt",
										prefTblsTemp + "InputAntSeg", campoLlave);

								valorAct = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpDupCalAct", false, null,
										conn)).doubleValue();
								valorAnt = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpDupCalAnt", false, null,
										conn)).doubleValue();

								writer.println("El valor anterior de referencia es " + valorAnt);
								if (valorAnt.equals(0.0)) {
									valorInd = 0.0;
								} else {
									valorInd = (valorAct - valorAnt) * 100.0 / valorAnt;
								}

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(10)
									&& reg.getTipo_analisis().equals(1)
									&& reg.getCampo_aprov().getTipo_campo().getId_tipo_campo().equals(1)) {
								// Indicador 10: Indicador de suma, solo numericos (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								valorInd = this.obtenerSumaCampoRegistrosTabla(prefTblsTemp + "InputActSeg",
										reg.getCampo_aprov(), false, null, conn);

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(11)
									&& reg.getTipo_analisis().equals(2)
									&& reg.getCampo_aprov().getTipo_campo().getId_tipo_campo().equals(1)) {
								// Indicador 11: Indicador de variacion de suma, solo numericos (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								valorAct = this.obtenerSumaCampoRegistrosTabla(prefTblsTemp + "InputActSeg",
										reg.getCampo_aprov(), false, null, conn);
								valorAnt = this.obtenerSumaCampoRegistrosTabla(prefTblsTemp + "InputAntSeg",
										reg.getCampo_aprov(), false, null, conn);

								writer.println("El valor anterior de referencia es " + valorAnt);
								if (valorAnt.equals(0.0)) {
									valorInd = 0.0;
								} else {
									valorInd = (valorAct - valorAnt) * 100.0 / valorAnt;
								}

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(12)
									&& reg.getTipo_analisis().equals(1)
									&& reg.getCampo_aprov().getTipo_campo().getId_tipo_campo().equals(1)) {
								// Indicador 12: Indicador de promedio, solo numericos (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								valorInd = this.obtenerPromedioCampoRegistrosTabla(prefTblsTemp + "InputActSeg",
										reg.getCampo_aprov(), false, null, conn);

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(13)
									&& reg.getTipo_analisis().equals(2)
									&& reg.getCampo_aprov().getTipo_campo().getId_tipo_campo().equals(1)) {
								// Indicador 13: Indicador de variacion de promedio, solo numericos (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								valorAct = this.obtenerPromedioCampoRegistrosTabla(prefTblsTemp + "InputActSeg",
										reg.getCampo_aprov(), false, null, conn);
								valorAnt = this.obtenerPromedioCampoRegistrosTabla(prefTblsTemp + "InputAntSeg",
										reg.getCampo_aprov(), false, null, conn);

								writer.println("El valor anterior de referencia es " + valorAnt);
								if (valorAnt.equals(0.0)) {
									valorInd = 0.0;
								} else {
									valorInd = (valorAct - valorAnt) * 100.0 / valorAnt;
								}

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(14)
									&& reg.getTipo_analisis().equals(1) && campoLlave.getId_campo_aprovisionamiento()
											.equals(reg.getCampo_aprov().getId_campo_aprovisionamiento())) {
								// Indicador 14: Indicador de nuevo, solo para llave (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								this.crearTablaTempEvidNuevos(prefTblsTemp + "TmpEvidValores",
										prefTblsTemp + "InputActSeg", prefTblsTemp + "InputAntSeg",
										reg.getCampo_aprov(), camposEvid);

								valorInd = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpEvidValores", false,
										null, conn)).doubleValue() - 1;

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(15)
									&& reg.getTipo_analisis().equals(1) && campoLlave.getId_campo_aprovisionamiento()
											.equals(reg.getCampo_aprov().getId_campo_aprovisionamiento())) {
								// Indicador 15: Indicador cancelados, solo para llave (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								this.crearTablaTempEvidCancelados(prefTblsTemp + "TmpEvidValores",
										prefTblsTemp + "InputActSeg", prefTblsTemp + "InputAntSeg",
										reg.getCampo_aprov(), camposEvid);

								valorInd = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpEvidValores", false,
										null, conn)).doubleValue() - 1;

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(16)
									&& reg.getTipo_analisis().equals(1)
									&& reg.getCampo_aprov().getTipo_variable().getId_tipo_variable().equals(1)) {
								// Indicador 16: Categorias e histograma, solo discretas (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								this.crearTablaTempHist(prefTblsTemp + "TmpHist", prefTblsTemp + "InputActSeg",
										reg.getCampo_aprov());

								valorInd = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpHist", false, null,
										conn)).doubleValue();

								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(17)
									&& reg.getTipo_analisis().equals(2)
									&& reg.getCampo_aprov().getTipo_variable().getId_tipo_variable().equals(1)) {
								// Indicador 17: Categorias e histograma, solo discretas (variacion)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								this.crearTablaTempHist(prefTblsTemp + "TmpHistAct", prefTblsTemp + "InputActSeg",
										reg.getCampo_aprov());
								this.crearTablaTempHist(prefTblsTemp + "TmpHistAnt", prefTblsTemp + "InputAntSeg",
										reg.getCampo_aprov());

								valorAct = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpHistAct", false, null,
										conn)).doubleValue();
								valorAnt = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpHistAnt", false, null,
										conn)).doubleValue();

								writer.println("El valor anterior de referencia es " + valorAnt);
								if (valorAnt.equals(0.0)) {
									valorInd = 0.0;
								} else {
									valorInd = (valorAct - valorAnt) * 100.0 / valorAnt;
								}

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(18)
									&& reg.getTipo_analisis().equals(1)
									&& reg.getCampo_aprov().getTipo_variable().getId_tipo_variable().equals(1)) {
								// Indicador 18: Herfindahl, solo discretas (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								valorInd = (this.obtenerValorHerfindTabla(prefTblsTemp + "InputActSeg",
										reg.getCampo_aprov(), conn));
								valorInd = valorInd * 100.0;

								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(19)
									&& reg.getTipo_analisis().equals(1)
									&& reg.getCampo_aprov().getTipo_variable().getId_tipo_variable().equals(1)) {
								// Indicador 19: Indice de estabilidad, solo discretas (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								valorInd = (this.obtenerValorIndEstTabla(prefTblsTemp + "InputActSeg",
										prefTblsTemp + "InputAntSeg", reg.getCampo_aprov(), conn));

								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(20)
									&& reg.getTipo_analisis().equals(1)
									&& reg.getCampo_aprov().getTipo_campo().getId_tipo_campo().equals(1)) {
								// Indicador 20: Indicador de aciertos, solo para numericos (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								this.crearTablaTempEvid(prefTblsTemp + "TmpEvidValores", prefTblsTemp + "InputActSeg",
										reg.getCampo_aprov(), camposEvid, true,
										reg.getCampo_aprov().getNombre_campo() + " is not null and "
												+ reg.getCampo_aprov().getNombre_campo() + " <> 0");

								valorInd = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpEvidValores", false,
										null, conn)).doubleValue() - 1;

								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(21)
									&& reg.getTipo_analisis().equals(1)
									&& reg.getCampo_aprov().getTipo_campo().getId_tipo_campo().equals(1)) {
								// Indicador 21: Porcentaje de aciertos, solo para numericos (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								this.crearTablaTempEvid(prefTblsTemp + "TmpEvidValores", prefTblsTemp + "InputActSeg",
										reg.getCampo_aprov(), camposEvid, true,
										reg.getCampo_aprov().getNombre_campo() + " is not null and "
												+ reg.getCampo_aprov().getNombre_campo() + " <> 0");

								valorInd = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpEvidValores", false,
										null, conn)).doubleValue() - 1;
								valorInd = valorInd * 100.0 / totalRegBucSeg;

								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(22)
									&& reg.getTipo_analisis().equals(2)
									&& reg.getCampo_aprov().getTipo_campo().getId_tipo_campo().equals(1)) {
								// Indicador 22: Variacion de aciertos, solo para numericos (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								valorAct = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "InputActSeg", true,
										reg.getCampo_aprov().getNombre_campo() + " is not null and "
												+ reg.getCampo_aprov().getNombre_campo() + " <> 0",
										conn)).doubleValue();
								valorAnt = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "InputAntSeg", true,
										reg.getCampo_aprov().getNombre_campo() + " is not null and "
												+ reg.getCampo_aprov().getNombre_campo() + " <> 0",
										conn)).doubleValue();

								writer.println("El valor anterior de referencia es " + valorAnt);
								if (valorAnt.equals(0.0)) {
									valorInd = 0.0;
								} else {
									valorInd = (valorAct - valorAnt) * 100.0 / valorAnt;
								}

								aplicaLimInf = true;
								aplicaLimSup = true;
							} else if (reg.getTipo_indicador().getId_tipo_indicador().equals(23)
									&& reg.getTipo_analisis().equals(1)
									&& reg.getCampo_aprov().getTipo_campo().getId_tipo_campo().equals(2)) {
								// Indicador 23: Busqueda en historico, solo para texto (foto)
								writer.println("Calculo del indicador " + tInd.getNombre_indicador()
										+ " para la variable " + reg.getCampo_aprov().getNombre_campo()
										+ " en el segmento " + seg.getDescripcion());

								this.crearTablaTempEvidBusqHist(prefTblsTemp + "TmpEvidValores",
										prefTblsTemp + "InputActSeg", prefTblsTemp + "InputAntTotalSeg",
										reg.getCampo_aprov(), camposEvid);

								valorInd = (this.obtenerCuentaRegistrosTabla(prefTblsTemp + "TmpEvidValores", false,
										null, conn)).doubleValue() - 1;

								aplicaLimSup = true;
							}

							// Guardamos resultados por regla
							this.guardarOutput(prefTblsTemp + "TmpEvidValores", prefTblsTemp + "TmpHist",
									carga.getId_cargafichero(), seg.getId_segmento(),
									reg.getCampo_aprov().getId_campo_aprovisionamiento(),
									reg.getTipo_indicador().getId_tipo_indicador(), reg.getTipo_analisis(), valorInd,
									aplicaLimInf, aplicaLimSup, reg.getLimite_inferior_rojo(),
									reg.getLimite_inferior_ambar(), reg.getLimite_superior_ambar(),
									reg.getLimite_superior_rojo(), writer);
						}
					}
				}
			}

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al ejecutar la calidad de datos de la carga con id " + carga.getId_cargafichero()
					+ " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
			writer.close();

			// Borramos todas las tablas temporales creadas
			this.borrarTablasTmpCalidadPorPref(prefTblsTemp);
		}
	}

	public ArrayList<CampoAprovisionamiento> obtenerCamposAprovPorIdAprov(Integer id_aprovisionamiento, Connection conn)
			throws Exception {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<CampoAprovisionamiento> campos = null;
		CampoAprovisionamiento campo = null;
		TipoCampo tipocampo = null;
		TipoVariable tipovariable = null;

		String sql = "select CA.CodCampoAprovisionamiento, CA.CodTipoCampo, CA.CodTipoVariable, CA.NbrCampo,"
				+ " CA.FlgCampoOriginal, CA.FlgBorrarCampo, CA.FlgLlave, CA.FlgSegmentar, CA.FlgCritica, CA.FlgEvidencia,"
				+ " CA.DesFormulaNuevoCampoInput, CA.DesFormulaNuevoCampoSql, TC.NbrTipoCampo, TV.NbrTipoVariable"
				+ " from HE_CampoAprovisionamiento CA"
				+ " left join ME_TipoCampo TC on CA.CodTipoCampo = TC.CodTipoCampo"
				+ " left join ME_TipoVariable TV on CA.CodTipoVariable = TV.CodTipoVariable"
				+ " where CA.CodAprovisionamiento = ?";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_aprovisionamiento);
			rs = pstmt.executeQuery();

			campos = new ArrayList<CampoAprovisionamiento>();
			while (rs.next()) {
				campo = new CampoAprovisionamiento();
				tipocampo = new TipoCampo();
				tipovariable = new TipoVariable();
				campo.setTipo_campo(tipocampo);
				campo.setTipo_variable(tipovariable);

				campo.setId_campo_aprovisionamiento(rs.getInt("CodCampoAprovisionamiento"));
				tipocampo.setId_tipo_campo(rs.getInt("CodTipoCampo"));
				tipovariable.setId_tipo_variable(rs.getInt("CodTipoVariable"));
				campo.setNombre_campo(rs.getString("NbrCampo"));
				campo.setFlag_campo_original(rs.getBoolean("FlgCampoOriginal"));
				campo.setFlag_aprovisionar(!rs.getBoolean("FlgBorrarCampo"));
				campo.setFlag_llave(rs.getBoolean("FlgLlave"));
				campo.setFlag_segmentar(rs.getBoolean("FlgSegmentar"));
				campo.setFlag_critica(rs.getBoolean("FlgCritica"));
				campo.setFlag_evidencia(rs.getBoolean("FlgEvidencia"));
				campo.setFormula_nuevo_campo_input(rs.getString("DesFormulaNuevoCampoInput"));
				campo.setFormula_nuevo_campo_sql(rs.getString("DesFormulaNuevoCampoSql"));
				tipocampo.setNombre_tipo_campo(rs.getString("NbrTipoCampo"));
				tipovariable.setNombre_tipo_variable(rs.getString("NbrTipoVariable"));

				// Completamos reglas por cada campo
				campo.setReglas(this.obtenerReglasCalidadPorCampo(campo));

				campos.add(campo);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener los campos del aprov con id: " + id_aprovisionamiento + ". Error: "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return campos;
	}

//no se usa:
	public ArrayList<CargaFichero> obtenerCargasAnteriores(CargaFichero cargaAct, Integer codmesAnt, Connection conn)
			throws Exception {
		ArrayList<CargaFichero> cargasAnt = null;
		CargaFichero cargaAnt = null;

		Fecha fecha = null;
		TipoAmbito tipoAmbito = null;
		TablaInput tablaInput = null;
		ArrayList<CampoInput> campos_input = new ArrayList<CampoInput>();

		Statement stmt = null;
		ResultSet rs = null;
		String sql = "Select Cf.*, " + "case when Cf.NUMVERSIONCARGA=(Select Max(Cf_V2.Numversioncarga) "
				+ " 	From He_Cargafichero Cf_V2 " + " 	Where Cf_V2.Codtablainput=Cf.Codtablainput) "
				+ " then 1 else 0 end as flag_ultimaCarga,"
				+ " F.Numcodmes, F.NumFase, TA.CodTipoAmbito, TA.NbrTipoAmbito, "
				+ " A.Nbrfichero, A.Flgcuentaoperacion, " + " A.Codaprovisionamiento, " + " Tabi.Flgnormalizada, "
				+ " Tabi.Codtablainput, " + " Tabi.Numversionmetadata, Tabi.NbrMetadata" + " From He_Cargafichero Cf "
				+ " Left Join He_Fecha F On F.Codfecha= Cf.Codfecha "
				+ " Left Join Me_TipoAmbito TA On F.CodTipoAmbito = TA.CodTipoAmbito "
				+ " Left Join He_Tablainput Tabi On Tabi.Codtablainput=Cf.Codtablainput"
				+ " Left Join He_Aprovisionamiento A On A.Codaprovisionamiento=Tabi.Codaprovisionamiento "
				+ " where Tabi.CodTablaInput = " + cargaAct.getTablaInput().getId_tablainput()
				+ " and Cf.TipEstadoCarga = 2 and F.NumFase = " + cargaAct.getFecha().getFase() + " and F.NumCodmes <= "
				+ codmesAnt + " order by cf.codcargafichero desc";
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			cargasAnt = new ArrayList<CargaFichero>();
			while (rs.next()) {
				cargaAnt = new CargaFichero();
				cargaAnt.setId_cargafichero(rs.getInt("CODCARGAFICHERO"));
				cargaAnt.setNombre_carga(rs.getString("NBRCARGA"));
				cargaAnt.setFlag_oficial(rs.getBoolean("FLGOFICIAL"));
				cargaAnt.setOrden_inicial(rs.getInt("NUMORDENINICIAL"));
				cargaAnt.setOrden_final(rs.getInt("NUMORDENFINAL"));
				cargaAnt.setNombre_carga(rs.getString("NBRCARGA"));
				cargaAnt.setVersion_carga(rs.getInt("NUMVERSIONCARGA"));
				cargaAnt.setFlag_ultimaCarga(rs.getBoolean("flag_ultimaCarga"));
				cargaAnt.setTipoEstadoCarga(rs.getInt("TIPESTADOCARGA"));
				fecha = new Fecha();
				fecha.setCodmes(rs.getInt("numcodmes"));
				fecha.setFase(rs.getInt("NumFase"));
				fecha.setId_fecha(rs.getInt("CODFECHA"));
				tipoAmbito = new TipoAmbito();
				tipoAmbito.setId_tipo_ambito(rs.getInt("CodTipoAmbito"));
				tipoAmbito.setNombre_tipo_ambito(rs.getString("NbrTipoAmbito"));
				fecha.setTipo_ambito(tipoAmbito);
				cargaAnt.setFecha(fecha);
				tablaInput = new TablaInput();
				tablaInput.setId_aprovisionamiento(rs.getInt("codaprovisionamiento"));
				tablaInput.setId_tablainput(rs.getInt("codtablainput"));
				tablaInput.setNombre_fichero(rs.getString("nbrfichero"));
				tablaInput.setVersion_metadata(rs.getInt("numversionmetadata"));
				tablaInput.setNombre_metadata(rs.getString("NbrMetadata"));
				tablaInput.setRuta(rs.getString("NBRRUTAAPROVISIONAMIENTO"));
				tablaInput.setFlag_normalizada(rs.getBoolean("Flgnormalizada"));
				tablaInput.setFlag_operacion(rs.getBoolean("FLGCUENTAOPERACION"));
				campos_input = this.obtenerCamposporTablaInputVersion(tablaInput, conn);
				tablaInput.setCampos_input(campos_input);
				cargaAnt.setTablaInput(tablaInput);

				cargasAnt.add(cargaAnt);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener las cargas anteriores de la carga actual con id: "
					+ cargaAct.getId_cargafichero() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return cargasAnt;
	}

	public void crearTablaTempCalidad(String nombreTabla, CargaFichero cargaFichero, Boolean esListaCargas,
			ArrayList<CargaFichero> listaCargas) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt = null;

		String sql_crear_tabla_campos = "";
		ArrayList<String> array_crear_campos = new ArrayList<String>();

		String sql_lista_ids = "";
		ArrayList<String> array_lista_ids = new ArrayList<String>();

		CargaFichero cargaRef = null;

		// Completamos lista de campos
		if (esListaCargas.equals(true)) {
			cargaRef = listaCargas.get(0); // Obtenemos el primero
		} else {
			cargaRef = cargaFichero;
		}

		for (CampoInput campo : this.obtenerCamposporTablaInputVersion(cargaRef.getTablaInput())) {
			if (campo.getTipo_campo().getId_tipo_campo().equals(1)) {
				array_crear_campos.add(campo.getNombre_campo() /* + " NUMBER(34,12)" */);
			} else {
				// Reducimos tamanio de campos texto
				/*
				 * if (!campo.getAncho_campo().equals(0)) {
				 * array_crear_campos.add(campo.getNombre_campo() + " VARCHAR2(" +
				 * campo.getAncho_campo() + ")"); } else {
				 */ // Campos calculados?
				array_crear_campos.add(campo.getNombre_campo() /* + " VARCHAR2(1000)" */);
				// }
			}
		}
		sql_crear_tabla_campos = String.join(",", array_crear_campos);

		// Completamos lista de ids de carga
		if (esListaCargas.equals(true)) {
			for (CargaFichero carga : listaCargas) {
				array_lista_ids.add(carga.getId_cargafichero().toString());
			}
		} else {
			array_lista_ids.add(cargaFichero.getId_cargafichero().toString());
		}
		sql_lista_ids = String.join(",", array_lista_ids);

		String sql_crear_tabla = "CREATE TABLE " + userName + "." + nombreTabla + " AS " + "SELECT NumOrden, "
				+ sql_crear_tabla_campos + " FROM " + userName + ".HE_" + cargaRef.getTablaInput().getNombre_fichero()
				+ "_v" + cargaRef.getTablaInput().getVersion_metadata() + " WHERE CodCargaFichero in (" + sql_lista_ids
				+ ")";

		try {
			conn = obtenerConexion();

			// Borramos tabla si es que existe
			this.borrarTablaTmpCalidadSiExiste(nombreTabla);

			// Creacion
			pstmt = conn.prepareStatement(sql_crear_tabla);
			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear temporal de ejec. de calidad para la carga con id: "
					+ cargaRef.getId_cargafichero() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public Integer obtenerCuentaRegistrosTabla(String nombreTabla, Boolean aplicaCond, String cond, Connection conn)
			throws Exception {
		Integer cuenta = null;

		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select count(*) as cuenta from " + userName + "." + nombreTabla
				+ (aplicaCond == true ? (" where " + cond) : "");
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				cuenta = rs.getInt("cuenta");
			}

		} catch (Exception ex) {
			LOGGER.error(
					"Error al obtener la cuenta de registros de la tabla: " + nombreTabla + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return cuenta;
	}

	public void guardarOutputFicheroTotal(Integer id_carga, Integer tipo_resultado, Integer valor, Integer num_columnas)
			throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;

		String sql = "Insert into HE_OutputFicheroTotal (CodCargaFichero, TipResultadoFinal, NumValor, "
				+ "NumColumnas) values (?,?,?,?)";

		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_carga);
			pstmt.setInt(2, tipo_resultado);
			pstmt.setInt(3, valor);
			pstmt.setInt(4, num_columnas);
			pstmt.executeUpdate();
			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al  : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void duplicarTablaTempCalidad(String nombreTabla, String nombreTablaDup, Boolean aplicaCond, String cond)
			throws Exception {

		Connection conn = null;
		PreparedStatement pstmt = null;

		String sql = "CREATE TABLE " + userName + "." + nombreTablaDup + " AS SELECT * FROM " + userName + "."
				+ nombreTabla + (aplicaCond == true ? (" WHERE " + cond) : "");

		try {
			conn = obtenerConexion();

			// Borramos tabla si es que existe
			this.borrarTablaTmpCalidadSiExiste(nombreTablaDup);

			// Creacion
			pstmt = conn.prepareStatement(sql);
			pstmt.addBatch();
			pstmt.executeBatch();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error al duplicar tabla temporal de ejec. de calidad: " + nombreTabla + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public ArrayList<ReglaCalidadConCampo> obtenerReglasCalidadConCampoPorIdSegInd(Integer id_segmento,
			Integer id_tipo_indicador, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ReglaCalidadConCampo regla = null;
		CampoAprovisionamiento campoAprov = null;
		TipoIndicador tipoIndicador = null;
		TipoCampo tipoCampo = null;
		TipoVariable tipoVariable = null;
		ArrayList<ReglaCalidadConCampo> reglas = new ArrayList<ReglaCalidadConCampo>();

		String sql = "select RC.*, TI.NbrIndicador, CA.NbrCampo, TC.CodTipoCampo, TC.NbrTipoCampo, "
				+ "TV.CodTipoVariable, TV.NbrTipoVariable from HE_ReglaCalidad RC "
				+ "left join ME_TipoIndicador TI on RC.CodTipoIndicador = TI.CodTipoIndicador "
				+ "left join HE_CampoAprovisionamiento CA on RC.CodCampoAprov = CA.CodCampoAprovisionamiento "
				+ "left join ME_TipoCampo TC on CA.CodTipoCampo = TC.CodTipoCampo "
				+ "left join ME_TipoVariable TV on CA.CodTipoVariable = TV.CodTipoVariable "
				+ "where RC.FlgAplica = 1 and CA.FlgBorrarCampo = 0 and "
				+ "RC.CodSegmento = ? and RC.CodTipoIndicador = ?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_segmento);
			pstmt.setInt(2, id_tipo_indicador);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				regla = new ReglaCalidadConCampo();
				regla.setId_segmento(rs.getInt("CodSegmento"));
				regla.setTipo_analisis(rs.getInt("CodTipoAnalisis"));
				regla.setLimite_inferior_rojo(rs.getDouble("NumLimiteInferiorRojo"));
				regla.setLimite_inferior_ambar(rs.getDouble("NumLimiteInferiorAmbar"));
				regla.setLimite_superior_ambar(rs.getDouble("NumLimiteSuperiorAmbar"));
				regla.setLimite_superior_rojo(rs.getDouble("NumLimiteSuperiorRojo"));
				regla.setFlag_aplica(rs.getBoolean("FlgAplica"));

				tipoIndicador = new TipoIndicador();
				tipoIndicador.setId_tipo_indicador(rs.getInt("CodTipoIndicador"));
				tipoIndicador.setNombre_indicador(rs.getString("NbrIndicador"));
				regla.setTipo_indicador(tipoIndicador);

				campoAprov = new CampoAprovisionamiento();
				campoAprov.setId_campo_aprovisionamiento(rs.getInt("CodCampoAprov"));
				campoAprov.setNombre_campo(rs.getString("NbrCampo"));

				tipoCampo = new TipoCampo();
				tipoCampo.setId_tipo_campo(rs.getInt("CodTipoCampo"));
				tipoCampo.setNombre_tipo_campo(rs.getString("NbrTipoCampo"));
				campoAprov.setTipo_campo(tipoCampo);

				tipoVariable = new TipoVariable();
				tipoVariable.setId_tipo_variable(rs.getInt("CodTipoVariable"));
				tipoVariable.setNombre_tipo_variable(rs.getString("NbrTipoVariable"));
				campoAprov.setTipo_variable(tipoVariable);

				regla.setCampo_aprov(campoAprov);
				reglas.add(regla);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener las reglas calidad por segmento con id: " + id_segmento
					+ " e indicador con id: " + id_tipo_indicador + ". Error: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarPreparedStatement(pstmt);
		}
		return reglas;
	}

	public void crearTablaTempEvid(String nombreTabla, String tablaOrigen, CampoAprovisionamiento campoEval,
			ArrayList<CampoAprovisionamiento> camposEvid, Boolean aplicaCond, String cond) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt_crear_tabla = null;
		PreparedStatement pstmt_insert_cabecera = null;
		PreparedStatement pstmt_insert = null;
		Integer idx_insert = null;

		ArrayList<String> array_select_cmpEvid = new ArrayList<String>();
		String sql_lista_selectEvid = "";

		// Completamos lista de campos en select

		// Primero el campo de evaluacion
		if (campoEval.getTipo_campo().getId_tipo_campo().equals(1)) {
			// Convertimos a texto (limitamos a 20 numeros, 1 coma decimal)
			array_select_cmpEvid.add("SUBSTR(TO_CHAR(" + campoEval.getNombre_campo() + "), 1, 21) as NbrCampo0");
		} else {
			array_select_cmpEvid.add(campoEval.getNombre_campo() + " as NbrCampo0");
		}

		idx_insert = 0;
		for (CampoAprovisionamiento campo : camposEvid) {
			idx_insert += 1;
			if (campo.getTipo_campo().getId_tipo_campo().equals(1)) {
				// Convertimos a texto (limitamos a 20 numeros, 1 coma decimal)
				array_select_cmpEvid
						.add("SUBSTR(TO_CHAR(" + campo.getNombre_campo() + "), 1, 21) as NbrCampo" + idx_insert);
			} else {
				array_select_cmpEvid.add(campo.getNombre_campo() + " as NbrCampo" + idx_insert);
			}
		}

		// Resto como '-' (en caso sobre)
		while (idx_insert < 10) {
			idx_insert += 1;
			array_select_cmpEvid.add("'-' as NbrCampo" + idx_insert);
		}

		sql_lista_selectEvid = String.join(",", array_select_cmpEvid);

		String sql_crear_tabla = "CREATE TABLE " + userName + "." + nombreTabla
				+ " (NumOrden INTEGER NOT NULL, NbrCampo0 VARCHAR2(100), NbrCampo1 VARCHAR2(100), NbrCampo2 VARCHAR2(100), "
				+ "NbrCampo3 VARCHAR2(100), NbrCampo4 VARCHAR2(100), NbrCampo5 VARCHAR2(100), NbrCampo6 VARCHAR2(100), "
				+ "NbrCampo7 VARCHAR2(100), NbrCampo8 VARCHAR2(100), NbrCampo9 VARCHAR2(100), NbrCampo10 VARCHAR2(100))";

		String sql_insert_cabecera = "INSERT INTO " + userName + "." + nombreTabla
				+ " (NumOrden, NbrCampo0, NbrCampo1, NbrCampo2, "
				+ "NbrCampo3, NbrCampo4, NbrCampo5, NbrCampo6, NbrCampo7, NbrCampo8, NbrCampo9, NbrCampo10) values "
				+ "(?,?,?,?,?,?,?,?,?,?,?,?)";

		String sql_insert = "INSERT INTO " + userName + "." + nombreTabla
				+ " (NumOrden, NbrCampo0, NbrCampo1, NbrCampo2, "
				+ "NbrCampo3, NbrCampo4, NbrCampo5, NbrCampo6, NbrCampo7, NbrCampo8, NbrCampo9, NbrCampo10) "
				+ " SELECT NumOrden, " + sql_lista_selectEvid + " FROM " + userName + "." + tablaOrigen
				+ (aplicaCond == true ? (" WHERE " + cond) : "");

		try {
			conn = obtenerConexion();

			// Borramos tabla si es que existe
			this.borrarTablaTmpCalidadSiExiste(nombreTabla);

			// Creamos tabla
			pstmt_crear_tabla = conn.prepareStatement(sql_crear_tabla);
			pstmt_crear_tabla.addBatch();
			pstmt_crear_tabla.executeBatch();

			// Insertamos cabeceras como orden 0
			pstmt_insert_cabecera = conn.prepareStatement(sql_insert_cabecera);
			pstmt_insert_cabecera.setInt(1, 0);
			pstmt_insert_cabecera.setString(2, campoEval.getNombre_campo());

			idx_insert = 2;
			for (CampoAprovisionamiento cmpEvid : camposEvid) {
				idx_insert += 1;
				pstmt_insert_cabecera.setString(idx_insert, cmpEvid.getNombre_campo());
			}

			// Insertamos resto como '-' (en caso sobre)
			while (idx_insert < 12) {
				idx_insert += 1;
				pstmt_insert_cabecera.setString(idx_insert, "-");
			}
			pstmt_insert_cabecera.executeUpdate();

			// Insertamos datos de evidencia
			pstmt_insert = conn.prepareStatement(sql_insert);
			pstmt_insert.executeUpdate();

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear tabla temporal de evidencia : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_crear_tabla);
			cerrarPreparedStatement(pstmt_insert_cabecera);
			cerrarPreparedStatement(pstmt_insert);
			cerrarConexion(conn);
		}
	}

	public Boolean existeTablaTmpCalidad(String nombreTabla, Connection conn) throws Exception {
		Statement stmt_existe = null;
		ResultSet rs_existe = null;
		Boolean existeTabla = null;

		String sql_consulta_existe = "SELECT * FROM all_tables where table_name = '" + nombreTabla.toUpperCase()
				+ "' and owner = '" + userName.toUpperCase() + "'";

		try {
			// Verificamos si existe tabla
			stmt_existe = crearStatement(conn);
			rs_existe = stmt_existe.executeQuery(sql_consulta_existe);

			existeTabla = false;
			while (rs_existe.next()) {
				existeTabla = true;
			}
		} catch (Exception ex) {
			LOGGER.error("Error al revisar existencia de tabla : " + nombreTabla + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarStatement(stmt_existe);
			cerrarResultSet(rs_existe);
		}
		return existeTabla;
	}

	public void borrarTablaTmpCalidadSiExiste(String nombreTabla) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;

		String sql_borrar_tabla = "DROP TABLE " + userName + "." + nombreTabla + " CASCADE CONSTRAINTS";

		try {
			conn = obtenerConexion();

			// Borrarm la tabla en caso exista
			if (this.existeTablaTmpCalidad(nombreTabla, conn).equals(true)) {
				pstmt = conn.prepareStatement(sql_borrar_tabla);
				pstmt.addBatch();
				pstmt.executeBatch();
				ejecutarCommit(conn);
			}

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error al borrar la tabla temporal de calidad con nombre " + nombreTabla + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void crearTablaDuplicadosCalidad(String nombreTabla, String tablaOrigen, CampoAprovisionamiento campoLlave)
			throws Exception {

		Connection conn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt_borrar_campo = null;

		String sql = "CREATE TABLE " + userName + "." + nombreTabla + " AS SELECT T_ID.* FROM "
				+ "(SELECT ROWNUM as ID_FILA, T1.* from " + userName + "." + tablaOrigen + " T1) T_ID "
				+ "RIGHT JOIN (SELECT " + campoLlave.getNombre_campo() + ", COUNT(" + campoLlave.getNombre_campo()
				+ ") " + "FROM " + userName + "." + tablaOrigen + " GROUP BY " + campoLlave.getNombre_campo()
				+ " HAVING COUNT(" + campoLlave.getNombre_campo() + ") > 1) T_DUP " + "ON T_ID. "
				+ campoLlave.getNombre_campo() + " = T_DUP." + campoLlave.getNombre_campo()
				+ " WHERE T_ID.ID_FILA NOT IN (SELECT MIN(ID_FILA) from (SELECT ROWNUM as ID_FILA, " + "T2.* FROM "
				+ userName + "." + tablaOrigen + " T2) GROUP BY " + campoLlave.getNombre_campo() + ")";

		String sql_borrar_campo = "ALTER TABLE " + userName + "." + nombreTabla + " DROP COLUMN ID_FILA";

		try {
			conn = obtenerConexion();

			// Borramos tabla si es que existe
			this.borrarTablaTmpCalidadSiExiste(nombreTabla);

			// Creacion
			pstmt = conn.prepareStatement(sql);
			pstmt.addBatch();
			pstmt.executeBatch();

			// Borramos campo de id de filas
			pstmt_borrar_campo = conn.prepareStatement(sql_borrar_campo);
			pstmt_borrar_campo.addBatch();
			pstmt_borrar_campo.executeBatch();

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error al crear tabla temporal de duplicados de calidad: " + nombreTabla + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarPreparedStatement(pstmt_borrar_campo);
			cerrarConexion(conn);
		}
	}

	public Double obtenerSumaCampoRegistrosTabla(String nombreTabla, CampoAprovisionamiento campoEval,
			Boolean aplicaCond, String cond, Connection conn) throws Exception {
		Double suma = null;

		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select sum(" + campoEval.getNombre_campo() + ") as suma_campo from " + userName + "."
				+ nombreTabla + (aplicaCond == true ? (" where " + cond) : "");
		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				suma = rs.getDouble("suma_campo");
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener la suma de registros de la tabla: " + nombreTabla + " del campo: "
					+ campoEval.getNombre_campo() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return suma;
	}

	public Double obtenerPromedioCampoRegistrosTabla(String nombreTabla, CampoAprovisionamiento campoEval,
			Boolean aplicaCond, String cond, Connection conn) throws Exception {
		Double promedio = null;

		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select avg(" + campoEval.getNombre_campo() + ") as promedio_campo from " + userName + "."
				+ nombreTabla + (aplicaCond == true ? (" where " + cond) : "");

		try {
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				promedio = rs.getDouble("promedio_campo");
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener el promedio de registros de la tabla: " + nombreTabla + " del campo: "
					+ campoEval.getNombre_campo() + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return promedio;
	}

	public void crearTablaTempEvidNuevos(String nombreTabla, String tablaAct, String tablaAnt,
			CampoAprovisionamiento campoLlave, ArrayList<CampoAprovisionamiento> camposEvid) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt_crear_tabla = null;
		PreparedStatement pstmt_insert_cabecera = null;
		PreparedStatement pstmt_insert = null;
		Integer idx_insert = null;

		ArrayList<String> array_select_cmpEvid = new ArrayList<String>();
		String sql_lista_selectEvid = "";

		// Completamos lista de campos en select

		// Primero el campo de evaluacion (llave)
		if (campoLlave.getTipo_campo().getId_tipo_campo().equals(1)) {
			// Convertimos a texto (limitamos a 20 numeros, 1 coma decimal)
			array_select_cmpEvid.add("SUBSTR(TO_CHAR(" + campoLlave.getNombre_campo() + "), 1, 21) as NbrCampo0");
		} else {
			array_select_cmpEvid.add(campoLlave.getNombre_campo() + " as NbrCampo0");
		}

		idx_insert = 0;
		for (CampoAprovisionamiento campo : camposEvid) {
			idx_insert += 1;
			if (campo.getTipo_campo().getId_tipo_campo().equals(1)) {
				// Convertimos a texto (limitamos a 20 numeros, 1 coma decimal)
				array_select_cmpEvid
						.add("SUBSTR(TO_CHAR(" + campo.getNombre_campo() + "), 1, 21) as NbrCampo" + idx_insert);
			} else {
				array_select_cmpEvid.add(campo.getNombre_campo() + " as NbrCampo" + idx_insert);
			}
		}

		// Resto como '-' (en caso sobre)
		while (idx_insert < 10) {
			idx_insert += 1;
			array_select_cmpEvid.add("'-' as NbrCampo" + idx_insert);
		}

		sql_lista_selectEvid = String.join(",", array_select_cmpEvid);

		String sql_crear_tabla = "CREATE TABLE " + userName + "." + nombreTabla
				+ " (NumOrden INTEGER NOT NULL, NbrCampo0 VARCHAR2(100), NbrCampo1 VARCHAR2(100), NbrCampo2 VARCHAR2(100), "
				+ "NbrCampo3 VARCHAR2(100), NbrCampo4 VARCHAR2(100), NbrCampo5 VARCHAR2(100), NbrCampo6 VARCHAR2(100), "
				+ "NbrCampo7 VARCHAR2(100), NbrCampo8 VARCHAR2(100), NbrCampo9 VARCHAR2(100), NbrCampo10 VARCHAR2(100))";

		String sql_insert_cabecera = "INSERT INTO " + userName + "." + nombreTabla
				+ " (NumOrden, NbrCampo0, NbrCampo1, NbrCampo2, "
				+ "NbrCampo3, NbrCampo4, NbrCampo5, NbrCampo6, NbrCampo7, NbrCampo8, NbrCampo9, NbrCampo10) values "
				+ "(?,?,?,?,?,?,?,?,?,?,?,?)";

		String sql_insert = "INSERT INTO " + userName + "." + nombreTabla
				+ " (NumOrden, NbrCampo0, NbrCampo1, NbrCampo2, "
				+ "NbrCampo3, NbrCampo4, NbrCampo5, NbrCampo6, NbrCampo7, NbrCampo8, NbrCampo9, NbrCampo10) "
				+ " SELECT T1.* FROM (SELECT NumOrden, " + sql_lista_selectEvid + " FROM " + userName + "." + tablaAct
				+ ") T1 LEFT JOIN (SELECT NumOrden, " + sql_lista_selectEvid + " FROM " + userName + "." + tablaAnt
				+ ") T2 ON T1.NbrCampo0 = T2.NbrCampo0 " + "WHERE T2.NbrCampo0 is null";

		try {
			conn = obtenerConexion();

			// Borramos tabla si es que existe
			this.borrarTablaTmpCalidadSiExiste(nombreTabla);

			// Creamos tabla
			pstmt_crear_tabla = conn.prepareStatement(sql_crear_tabla);
			pstmt_crear_tabla.addBatch();
			pstmt_crear_tabla.executeBatch();

			// Insertamos cabeceras como orden 0
			pstmt_insert_cabecera = conn.prepareStatement(sql_insert_cabecera);
			pstmt_insert_cabecera.setInt(1, 0);
			pstmt_insert_cabecera.setString(2, campoLlave.getNombre_campo());

			idx_insert = 2;
			for (CampoAprovisionamiento cmpEvid : camposEvid) {
				idx_insert += 1;
				pstmt_insert_cabecera.setString(idx_insert, cmpEvid.getNombre_campo());
			}

			// Insertamos resto como '-' (en caso sobre)
			while (idx_insert < 12) {
				idx_insert += 1;
				pstmt_insert_cabecera.setString(idx_insert, "-");
			}
			pstmt_insert_cabecera.executeUpdate();

			// Insertamos datos de evidencia
			pstmt_insert = conn.prepareStatement(sql_insert);
			pstmt_insert.executeUpdate();

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear tabla temporal de evidencia para registros nuevos: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_crear_tabla);
			cerrarPreparedStatement(pstmt_insert_cabecera);
			cerrarPreparedStatement(pstmt_insert);
			cerrarConexion(conn);
		}
	}

	public void crearTablaTempEvidCancelados(String nombreTabla, String tablaAct, String tablaAnt,
			CampoAprovisionamiento campoLlave, ArrayList<CampoAprovisionamiento> camposEvid) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt_crear_tabla = null;
		PreparedStatement pstmt_insert_cabecera = null;
		PreparedStatement pstmt_insert = null;
		Integer idx_insert = null;

		ArrayList<String> array_select_cmpEvid = new ArrayList<String>();
		String sql_lista_selectEvid = "";

		// Completamos lista de campos en select

		// Primero el campo de evaluacion (llave)
		if (campoLlave.getTipo_campo().getId_tipo_campo().equals(1)) {
			// Convertimos a texto (limitamos a 20 numeros, 1 coma decimal)
			array_select_cmpEvid.add("SUBSTR(TO_CHAR(" + campoLlave.getNombre_campo() + "), 1, 21) as NbrCampo0");
		} else {
			array_select_cmpEvid.add(campoLlave.getNombre_campo() + " as NbrCampo0");
		}

		idx_insert = 0;
		for (CampoAprovisionamiento campo : camposEvid) {
			idx_insert += 1;
			if (campo.getTipo_campo().getId_tipo_campo().equals(1)) {
				// Convertimos a texto (limitamos a 20 numeros, 1 coma decimal)
				array_select_cmpEvid
						.add("SUBSTR(TO_CHAR(" + campo.getNombre_campo() + "), 1, 21) as NbrCampo" + idx_insert);
			} else {
				array_select_cmpEvid.add(campo.getNombre_campo() + " as NbrCampo" + idx_insert);
			}
		}

		// Resto como '-' (en caso sobre)
		while (idx_insert < 10) {
			idx_insert += 1;
			array_select_cmpEvid.add("'-' as NbrCampo" + idx_insert);
		}

		sql_lista_selectEvid = String.join(",", array_select_cmpEvid);

		String sql_crear_tabla = "CREATE TABLE " + userName + "." + nombreTabla
				+ " (NumOrden INTEGER NOT NULL, NbrCampo0 VARCHAR2(100), NbrCampo1 VARCHAR2(100), NbrCampo2 VARCHAR2(100), "
				+ "NbrCampo3 VARCHAR2(100), NbrCampo4 VARCHAR2(100), NbrCampo5 VARCHAR2(100), NbrCampo6 VARCHAR2(100), "
				+ "NbrCampo7 VARCHAR2(100), NbrCampo8 VARCHAR2(100), NbrCampo9 VARCHAR2(100), NbrCampo10 VARCHAR2(100))";

		String sql_insert_cabecera = "INSERT INTO " + userName + "." + nombreTabla
				+ " (NumOrden, NbrCampo0, NbrCampo1, NbrCampo2, "
				+ "NbrCampo3, NbrCampo4, NbrCampo5, NbrCampo6, NbrCampo7, NbrCampo8, NbrCampo9, NbrCampo10) values "
				+ "(?,?,?,?,?,?,?,?,?,?,?,?)";

		String sql_insert = "INSERT INTO " + userName + "." + nombreTabla
				+ " (NumOrden, NbrCampo0, NbrCampo1, NbrCampo2, "
				+ "NbrCampo3, NbrCampo4, NbrCampo5, NbrCampo6, NbrCampo7, NbrCampo8, NbrCampo9, NbrCampo10) "
				+ " SELECT T2.* FROM (SELECT NumOrden, " + sql_lista_selectEvid + " FROM " + userName + "." + tablaAct
				+ ") T1 RIGHT JOIN (SELECT NumOrden, " + sql_lista_selectEvid + " FROM " + userName + "." + tablaAnt
				+ ") T2 ON T1.NbrCampo0 = T2.NbrCampo0 " + "WHERE T1.NbrCampo0 is null";

		try {
			conn = obtenerConexion();

			// Borramos tabla si es que existe
			this.borrarTablaTmpCalidadSiExiste(nombreTabla);

			// Creamos tabla
			pstmt_crear_tabla = conn.prepareStatement(sql_crear_tabla);
			pstmt_crear_tabla.addBatch();
			pstmt_crear_tabla.executeBatch();

			// Insertamos cabeceras como orden 0
			pstmt_insert_cabecera = conn.prepareStatement(sql_insert_cabecera);
			pstmt_insert_cabecera.setInt(1, 0);
			pstmt_insert_cabecera.setString(2, campoLlave.getNombre_campo());

			idx_insert = 2;
			for (CampoAprovisionamiento cmpEvid : camposEvid) {
				idx_insert += 1;
				pstmt_insert_cabecera.setString(idx_insert, cmpEvid.getNombre_campo());
			}

			// Insertamos resto como '-' (en caso sobre)
			while (idx_insert < 12) {
				idx_insert += 1;
				pstmt_insert_cabecera.setString(idx_insert, "-");
			}
			pstmt_insert_cabecera.executeUpdate();

			// Insertamos datos de evidencia
			pstmt_insert = conn.prepareStatement(sql_insert);
			pstmt_insert.executeUpdate();

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear tabla temporal de evidencia para registros cancelados: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_crear_tabla);
			cerrarPreparedStatement(pstmt_insert_cabecera);
			cerrarPreparedStatement(pstmt_insert);
			cerrarConexion(conn);
		}
	}

	public void crearTablaTempHist(String nombreTabla, String tablaOrigen, CampoAprovisionamiento campoEval)
			throws Exception {

		Connection conn = null;
		PreparedStatement pstmt_hist = null;

		String sql_hist = "CREATE TABLE " + userName + "." + nombreTabla + " AS SELECT "
				+ (campoEval.getTipo_campo().getId_tipo_campo().equals(1)
						? "SUBSTR(TO_CHAR(" + campoEval.getNombre_campo() + "), 1, 21)"
						: campoEval.getNombre_campo())
				+ " AS DesCategoria, COUNT(" + campoEval.getNombre_campo() + ") AS NumCategoria" + " FROM " + userName
				+ "." + tablaOrigen + " WHERE " + campoEval.getNombre_campo() + " is not null GROUP BY "
				+ campoEval.getNombre_campo();

		try {
			conn = obtenerConexion();

			// Borramos tabla si es que existe
			this.borrarTablaTmpCalidadSiExiste(nombreTabla);

			// Creamos tabla de histograma
			pstmt_hist = conn.prepareStatement(sql_hist);
			pstmt_hist.addBatch();
			pstmt_hist.executeBatch();

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear tabla temporal de historigrama: " + nombreTabla + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_hist);
			cerrarConexion(conn);
		}
	}

	public Double obtenerValorHerfindTabla(String nombreTabla, CampoAprovisionamiento campoEval, Connection conn)
			throws Exception {
		Double valor_herfind = null;
		Integer cuenta = null;

		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;

		try {
			cuenta = this.obtenerCuentaRegistrosTabla(nombreTabla, false, null, conn);

			sql = "select sum(T.herfind) as v_herf from (select power(count(" + campoEval.getNombre_campo() + ") / "
					+ cuenta + ", 2) as herfind from " + userName + "." + nombreTabla + " where "
					+ campoEval.getNombre_campo() + " is not null group by " + campoEval.getNombre_campo() + ") T";

			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				valor_herfind = rs.getDouble("v_herf");
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener el indicador de herfindahl de la tabla: " + nombreTabla + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return valor_herfind;
	}

	public Double obtenerValorIndEstTabla(String nombreTablaAct, String nombreTablaAnt,
			CampoAprovisionamiento campoEval, Connection conn) throws Exception {
		Double valor_indEst = null;
		Integer cuentaAct = null;
		Integer cuentaAnt = null;

		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;

		try {
			cuentaAct = this.obtenerCuentaRegistrosTabla(nombreTablaAct, true,
					campoEval.getNombre_campo() + " is not null", conn);
			cuentaAnt = this.obtenerCuentaRegistrosTabla(nombreTablaAnt, true,
					campoEval.getNombre_campo() + " is not null", conn);

			sql = "select sum(T.ie) as v_indEst from (select (case when (t2.porcentaje is null or t2.porcentaje = 0) then 0 else "
					+ " ((case when (t1.porcentaje is null or t1.porcentaje = 0) then 0 else t1.porcentaje end) - t2.porcentaje)"
					+ "*LN((case when (t1.porcentaje is null or t1.porcentaje = 0) then 0 else t1.porcentaje end) / t2.porcentaje) end) as ie "
					+ "from (select " + campoEval.getNombre_campo() + ", (count(" + campoEval.getNombre_campo() + ") / "
					+ cuentaAct + ") as porcentaje from " + userName + "." + nombreTablaAct + " where "
					+ campoEval.getNombre_campo() + " is not null group by " + campoEval.getNombre_campo()
					+ ") t1 full join " + "(select " + campoEval.getNombre_campo() + ", (count("
					+ campoEval.getNombre_campo() + ") / " + cuentaAnt + ") as porcentaje from " + userName + "."
					+ nombreTablaAnt + " where " + campoEval.getNombre_campo() + " is not null group by "
					+ campoEval.getNombre_campo() + ") t2 on t1." + campoEval.getNombre_campo() + " = t2."
					+ campoEval.getNombre_campo() + ") T";

			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				valor_indEst = rs.getDouble("v_indEst");
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener el indice de estabilidad de la tabla: " + nombreTablaAct + " : "
					+ ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
		}
		return valor_indEst;
	}

	public void crearTablaTempEvidBusqHist(String nombreTabla, String tablaAct, String tablaAntTotal,
			CampoAprovisionamiento campoEval, ArrayList<CampoAprovisionamiento> camposEvid) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt_crear_tabla = null;
		PreparedStatement pstmt_insert_cabecera = null;
		PreparedStatement pstmt_insert = null;
		Integer idx_insert = null;

		ArrayList<String> array_select_cmpEvid = new ArrayList<String>();
		String sql_lista_selectEvid = "";
		String sql_campoEval = ""; // Solo el primero

		// Completamos lista de campos en select

		// Primero el campo de evaluacion (llave)
		if (campoEval.getTipo_campo().getId_tipo_campo().equals(1)) {
			// Convertimos a texto (limitamos a 20 numeros, 1 coma decimal)
			array_select_cmpEvid.add("SUBSTR(TO_CHAR(" + campoEval.getNombre_campo() + "), 1, 21) as NbrCampo0");
			sql_campoEval = "SUBSTR(TO_CHAR(" + campoEval.getNombre_campo() + "), 1, 21) as NbrCampo0";
		} else {
			array_select_cmpEvid.add(campoEval.getNombre_campo() + " as NbrCampo0");
			sql_campoEval = campoEval.getNombre_campo() + " as NbrCampo0";
		}

		idx_insert = 0;
		for (CampoAprovisionamiento campo : camposEvid) {
			idx_insert += 1;
			if (campo.getTipo_campo().getId_tipo_campo().equals(1)) {
				// Convertimos a texto (limitamos a 20 numeros, 1 coma decimal)
				array_select_cmpEvid
						.add("SUBSTR(TO_CHAR(" + campo.getNombre_campo() + "), 1, 21) as NbrCampo" + idx_insert);
			} else {
				array_select_cmpEvid.add(campo.getNombre_campo() + " as NbrCampo" + idx_insert);
			}
		}

		// Resto como '-' (en caso sobre)
		while (idx_insert < 10) {
			idx_insert += 1;
			array_select_cmpEvid.add("'-' as NbrCampo" + idx_insert);
		}

		sql_lista_selectEvid = String.join(",", array_select_cmpEvid);

		String sql_crear_tabla = "CREATE TABLE " + userName + "." + nombreTabla
				+ " (NumOrden INTEGER NOT NULL, NbrCampo0 VARCHAR2(100), NbrCampo1 VARCHAR2(100), NbrCampo2 VARCHAR2(100), "
				+ "NbrCampo3 VARCHAR2(100), NbrCampo4 VARCHAR2(100), NbrCampo5 VARCHAR2(100), NbrCampo6 VARCHAR2(100), "
				+ "NbrCampo7 VARCHAR2(100), NbrCampo8 VARCHAR2(100), NbrCampo9 VARCHAR2(100), NbrCampo10 VARCHAR2(100))";

		String sql_insert_cabecera = "INSERT INTO " + userName + "." + nombreTabla
				+ " (NumOrden, NbrCampo0, NbrCampo1, NbrCampo2, "
				+ "NbrCampo3, NbrCampo4, NbrCampo5, NbrCampo6, NbrCampo7, NbrCampo8, NbrCampo9, NbrCampo10) values "
				+ "(?,?,?,?,?,?,?,?,?,?,?,?)";

		String sql_insert = "INSERT INTO " + userName + "." + nombreTabla
				+ " (NumOrden, NbrCampo0, NbrCampo1, NbrCampo2, "
				+ "NbrCampo3, NbrCampo4, NbrCampo5, NbrCampo6, NbrCampo7, NbrCampo8, NbrCampo9, NbrCampo10) "
				+ " SELECT T1.* FROM (SELECT NumOrden, " + sql_lista_selectEvid + " FROM " + userName + "." + tablaAct
				+ ") T1 LEFT JOIN (SELECT DISTINCT " + sql_campoEval + " FROM " + userName + "." + tablaAntTotal
				+ ") T2 ON T1.NbrCampo0 = T2.NbrCampo0 " + "WHERE T2.NbrCampo0 is null";

		try {
			conn = obtenerConexion();

			// Borramos tabla si es que existe
			this.borrarTablaTmpCalidadSiExiste(nombreTabla);

			// Creamos tabla
			pstmt_crear_tabla = conn.prepareStatement(sql_crear_tabla);
			pstmt_crear_tabla.addBatch();
			pstmt_crear_tabla.executeBatch();

			// Insertamos cabeceras como orden 0
			pstmt_insert_cabecera = conn.prepareStatement(sql_insert_cabecera);
			pstmt_insert_cabecera.setInt(1, 0);
			pstmt_insert_cabecera.setString(2, campoEval.getNombre_campo());

			idx_insert = 2;
			for (CampoAprovisionamiento cmpEvid : camposEvid) {
				idx_insert += 1;
				pstmt_insert_cabecera.setString(idx_insert, cmpEvid.getNombre_campo());
			}

			// Insertamos resto como '-' (en caso sobre)
			while (idx_insert < 12) {
				idx_insert += 1;
				pstmt_insert_cabecera.setString(idx_insert, "-");
			}
			pstmt_insert_cabecera.executeUpdate();

			// Insertamos datos de evidencia
			pstmt_insert = conn.prepareStatement(sql_insert);
			pstmt_insert.executeUpdate();

			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al crear tabla temporal de evidencia para busqueda historica: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt_crear_tabla);
			cerrarPreparedStatement(pstmt_insert_cabecera);
			cerrarPreparedStatement(pstmt_insert);
			cerrarConexion(conn);
		}
	}

	public void guardarOutput(String tablaEvidTmp, String tablaHistTmp, Integer id_carga, Integer id_segmento,
			Integer id_campo, Integer id_indicador, Integer id_analisis, Double valor_indicador, Boolean aplicaLimInf,
			Boolean aplicaLimSup, Double valRojoInf, Double valAmbarInf, Double valAmbarSup, Double valRojoSup,
			PrintWriter writer) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt_evid_insert = null;
		PreparedStatement pstmt_histog_insert = null;
		Boolean flag_verde = null;
		Boolean flag_ambar = null;
		Boolean flag_rojo = null;

		String sql = "Insert into HE_OutputFicheroReglaCalidad (CodCargaFichero, CodSegmento, CodCampoAprov, CodTipoIndicador, "
				+ "CodTipoAnalisis, NumResultado, FlgVerde, FlgAmbar, FlgRojo) values (?,?,?,?,?,?,?,?,?)";

		String sql_evid_insert = "INSERT INTO HE_OutputEvidCalidad (CodCargaFichero, CodSegmento, CodCampoAprov, CodTipoIndicador, "
				+ "CodTipoAnalisis, NumOrden, NbrCampo0, NbrCampo1, NbrCampo2, NbrCampo3, NbrCampo4, NbrCampo5, NbrCampo6, NbrCampo7, "
				+ "NbrCampo8, NbrCampo9, NbrCampo10) SELECT " + id_carga + " AS CodCargaFichero, " + id_segmento
				+ " AS CodSegmento, " + id_campo + " AS CodCampoAprov, " + id_indicador + " AS CodTipoIndicador, "
				+ id_analisis + " AS CodTipoAnalisis, " + "T.* FROM " + userName + "." + tablaEvidTmp + " T";

		String sql_histog_insert = "INSERT INTO HE_OutputHistoCalidad (CodCargaFichero, CodSegmento, CodCampoAprov, CodTipoIndicador, "
				+ "CodTipoAnalisis, DesCategoria, NumCategoria) SELECT " + id_carga + " AS CodCargaFichero, "
				+ id_segmento + " AS CodSegmento, " + id_campo + " AS CodCampoAprov, " + id_indicador
				+ " AS CodTipoIndicador, " + id_analisis + " AS CodTipoAnalisis, " + "T.* FROM " + userName + "."
				+ tablaHistTmp + " T";

		try {
			writer.println("Valor del indicador : " + valor_indicador);
			writer.println("Valor del limite inferior rojo : " + valor_indicador);
			writer.println("Valor del limite inferior ambar : " + valAmbarInf);
			writer.println("Valor del limite superior rojo : " + valRojoSup);
			writer.println("Valor del limite superior ambar : " + valAmbarSup);

			// Hallamos flags
			flag_verde = false;
			flag_ambar = false;
			flag_rojo = false;

			if (aplicaLimInf.equals(false) && aplicaLimSup.equals(false)) {
				writer.println("El valor esta en verde");
				flag_verde = true;
			} else if (aplicaLimInf.equals(true) && aplicaLimSup.equals(false)) {
				if (valor_indicador <= valRojoInf) {
					writer.println("El valor esta en rojo inferior");
					flag_rojo = true;
				} else if (valor_indicador <= valAmbarInf) {
					writer.println("El valor esta en ambar inferior");
					flag_ambar = true;
				} else {
					writer.println("El valor esta en verde");
					flag_verde = true;
				}
			} else if (aplicaLimInf.equals(false) && aplicaLimSup.equals(true)) {
				if (valor_indicador >= valRojoSup) {
					writer.println("El valor esta en rojo superior");
					flag_rojo = true;
				} else if (valor_indicador >= valAmbarSup) {
					writer.println("El valor esta en ambar superior");
					flag_ambar = true;
				} else {
					writer.println("El valor esta en verde");
					flag_verde = true;
				}
			} else {
				if (valor_indicador <= valRojoInf) {
					writer.println("El valor esta en rojo inferior");
					flag_rojo = true;
				} else if (valor_indicador <= valAmbarInf) {
					writer.println("El valor esta en ambar inferior");
					flag_ambar = true;
				} else if (valor_indicador >= valRojoSup) {
					writer.println("El valor esta en rojo superior");
					flag_rojo = true;
				} else if (valor_indicador >= valAmbarSup) {
					writer.println("El valor esta en ambar superior");
					flag_ambar = true;
				} else {
					writer.println("El valor esta en verde");
					flag_verde = true;
				}
			}

			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id_carga);
			pstmt.setInt(2, id_segmento);
			pstmt.setInt(3, id_campo);
			pstmt.setInt(4, id_indicador);
			pstmt.setInt(5, id_analisis);
			pstmt.setDouble(6, valor_indicador);
			pstmt.setBoolean(7, flag_verde);
			pstmt.setBoolean(8, flag_ambar);
			pstmt.setBoolean(9, flag_rojo);
			pstmt.executeUpdate();

			// Insertamos datos de evidencia (restringimos por tipo indicador)
			if (this.existeTablaTmpCalidad(tablaEvidTmp, conn)
					&& (id_indicador.equals(4) || id_indicador.equals(5) || id_indicador.equals(7)
							|| id_indicador.equals(8) || id_indicador.equals(14) || id_indicador.equals(15)
							|| id_indicador.equals(20) || id_indicador.equals(21) || id_indicador.equals(23))) {
				writer.println("Aplica almacenar registros de evidencia");
				pstmt_evid_insert = conn.prepareStatement(sql_evid_insert);
				pstmt_evid_insert.executeUpdate();
			}

			// Insertamos datos de histograma (restringimos por tipo indicador)
			if (this.existeTablaTmpCalidad(tablaHistTmp, conn) && id_indicador.equals(16)) {
				writer.println("Aplica almacenar registros para histograma");
				pstmt_histog_insert = conn.prepareStatement(sql_histog_insert);
				pstmt_histog_insert.executeUpdate();
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al guardar la salida de calidad : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarPreparedStatement(pstmt_evid_insert);
			cerrarPreparedStatement(pstmt_histog_insert);
			cerrarConexion(conn);
		}
	}

	public void actualizarNuevaCargaFichero(CargaFichero cargaFichero) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt = null;
		conn = obtenerConexion();
		String sql_cf = "UPDATE HE_CargaFichero SET TIEMPODECARGA =? , TABLADESTINO= ?, NUMEROCAMPOS = ? WHERE CodCargaFichero = ?";
		try {

			pstmt = conn.prepareStatement(sql_cf);
			pstmt.setInt(1, cargaFichero.getTiempoDeCarga());
			pstmt.setString(2, cargaFichero.getTablaDestino());
			pstmt.setInt(3, cargaFichero.getNumeroCampos());
			pstmt.setInt(4, cargaFichero.getId_cargafichero());

			pstmt.executeUpdate();
			ejecutarCommit(conn);
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al acutalizar el fichero después de crear la nueva tabla " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
	}

	public void borrarTablasTmpCalidadPorPref(String prefijo) throws Exception {
		Connection conn = null;
		Statement stmt_tablas_pref = null;
		ResultSet rs_tablas_pref = null;
		String sql_borrar_tabla = null;

		String sql_tablas_pref = "SELECT table_name FROM all_tables where table_name like '" + prefijo.toUpperCase()
				+ "%' and owner = '" + userName.toUpperCase() + "'";

		try {
			conn = obtenerConexion();

			// Buscamos tablas con prefijo
			stmt_tablas_pref = crearStatement(conn);
			rs_tablas_pref = stmt_tablas_pref.executeQuery(sql_tablas_pref);

			while (rs_tablas_pref.next()) {
				// Borramos uno por uno
				sql_borrar_tabla = "DROP TABLE " + userName + "." + rs_tablas_pref.getString("table_name")
						+ " CASCADE CONSTRAINTS";
				try (PreparedStatement pstmt_borrar_tabla = conn.prepareStatement(sql_borrar_tabla)) {
					pstmt_borrar_tabla.addBatch();
					pstmt_borrar_tabla.executeBatch();
				}
			}

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error(
					"Error al borrar tablas temporales de calidad con prefijo " + prefijo + " : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarStatement(stmt_tablas_pref);
			cerrarResultSet(rs_tablas_pref);

			cerrarConexion(conn);
		}
	}

	// -------------------------- SAS --------------------------------------
	public RutaValida validarSAS(ConfiguracionSAS configuracion) throws Exception {
		RutaValida rutavalida = new RutaValida();
		rutavalida.setValido(true);
		String user = null;
		String pass = null;

		ConnectionInterface cx = null;
		ConnectionFactoryConfiguration cxfConfig = new ManualConnectionFactoryConfiguration(new BridgeServer(
				Server.CLSID_SAS, configuracion.getHost(), Integer.valueOf(configuracion.getPuerto())));
		ConnectionFactoryManager cxfManager = new ConnectionFactoryManager();
		ConnectionFactoryInterface cxf = cxfManager.getFactory(cxfConfig);
		try {
			if (configuracion.getUsuario() != null) {
				user = AES.decrypt(configuracion.getUsuario(), Claves.encriptadoRutaBD);
				pass = AES.decrypt(configuracion.getContrasena(), Claves.encriptadoRutaBD);
			} else {
				user = null;
				pass = null;
			}
			cx = cxf.getConnection(user, pass);
		} catch (ConnectionFactoryException cfe) {
			rutavalida.setValido(false);
			rutavalida.setObservacion("Error creando conexion SAS " + cfe.getMessage());
			LOGGER.error("Error creando conexion SAS " + cfe.getMessage());
			cfe.printStackTrace();
		} catch (Exception ex) {
			rutavalida.setValido(false);
			rutavalida.setObservacion("Error creando conexion SAS: " + ex.getMessage());
			LOGGER.error("Error al validar ruta, Error: " + ex.getMessage());
		}

		return rutavalida;
	}

	public ConfiguracionSAS obtenerConfiguracionSAS() {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String sql = "SELECT * FROM UMA_ConfiguracionServidor WHERE NumCodConf = 1";
		ConfiguracionSAS configuracion = new ConfiguracionSAS();
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				configuracion.setFlag_log(rs.getBoolean("BolModulo"));
				configuracion.setHost(rs.getString("VchHost"));
				configuracion.setPuerto(rs.getInt("NumPort"));
				configuracion.setUsuario(rs.getString("VchUsuario"));
				configuracion.setContrasena(rs.getString("VchPass"));
				configuracion.setRutaRaiz(rs.getString("VchConf1"));
				configuracion.setCodigoInicial(rs.getString("VchConf2"));
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener la configuracion de SAS: " + ex.getMessage());
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return configuracion;
	}

	public void editarConfiguracionSAS(ConfiguracionSAS configuracion) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
//		RutaValida rutaValida = new RutaValida();

		String sql = "UPDATE UMA_ConfiguracionServidor SET BolModulo= ?, VchHost= ?, NumPort= ?, VchConf1=?, VchConf2=?,VchUsuario=?, "
				+ "VchPass=?, fecactualizaciontabla = ? WHERE NumCodConf = 1";
		try {
//			rutaValida = validarSAS(configuracion);
//			if(rutaValida.getValido()) {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setBoolean(1, configuracion.getFlag_Log());
			pstmt.setString(2, configuracion.getHost());
			pstmt.setInt(3, configuracion.getPuerto());
			pstmt.setString(4, configuracion.getRutaRaiz());
			pstmt.setString(5, configuracion.getCodigoInicial());
			pstmt.setString(6, configuracion.getUsuario());
			pstmt.setString(7, configuracion.getContrasena());
			pstmt.setDate(8, new Date(System.currentTimeMillis()));

			pstmt.executeUpdate();
			ejecutarCommit(conn);
//			}

		} catch (Exception ex) {
			LOGGER.error("Error al editar la configuraci�n de SAS: " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
			cerrarConexion(conn);
		}
//		return rutaValida;
	}

	// Guardar
	public void guardarInicializacionEjecucionPE(ArrayList<String> lineasSubmit,
			ArrayList<String> lineasSubmitEscenario, Integer codEjecucion) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt_ejecucion = null;

		String sql_ejecucion = "Update HE_Ejecucion set " + "DesInicializacion= ? where CodEjecucion = ?";

		try {
			conn = obtenerConexion();
			pstmt_ejecucion = conn.prepareStatement(sql_ejecucion);

			pstmt_ejecucion.setString(1,
					String.join("\r\n", lineasSubmit) + "\r\n" + String.join("\r\n", lineasSubmitEscenario));
			pstmt_ejecucion.setInt(2, codEjecucion);

			pstmt_ejecucion.executeUpdate();

			ejecutarCommit(conn);

		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al guardar el codigo de inicializacion de la ejecucion " + codEjecucion + ": "
					+ ex.getMessage());
			throw ex;

		} finally {
			cerrarPreparedStatement(pstmt_ejecucion);
			cerrarConexion(conn);
		}
	}

	public ArrayList<LogCodInicializacion> obtenerLogsCodInicializacion(Integer NumCodMes, Integer id_juego_escenarios,
			Integer fase, Integer id_tipo_ambito) throws Exception {

		ArrayList<LogCodInicializacion> logsCodInicializacion = new ArrayList<LogCodInicializacion>();
		LogCodInicializacion logCodInicializacion = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select E.DesInicializacion from HE_Ejecucion E "
				+ "  LEFT JOIN HE_EscenarioVersion EV on E.CODESCENARIOVERSION = EV.CODESCENARIOVERSION "
				+ "  LEFT JOIN HE_ESCENARIOVERSION ESC on EV.CODESCENARIOVERSION = ESC.CODESCENARIOVERSION "
				+ " where E.CODMES = " + NumCodMes + " and EV.CODJUEGOESCENARIOS = " + id_juego_escenarios
				+ " and E.CODTIPOEJECUCION = " + (fase + (id_tipo_ambito.equals(2) ? 6 : 0))
				+ " and ESC.CODESCENARIO = ";
		// Tipos de ejecucion f1 y f2 para inversiones son 7 y 8
		try {
			conn = obtenerConexion();

			for (Escenario esc : this.obtenerEscenarios(conn)) {
				stmt = crearStatement(conn);
				rs = stmt.executeQuery(sql + esc.getId_escenario());

				logCodInicializacion = new LogCodInicializacion();
				logCodInicializacion.setEscenario(esc);

				while (rs.next()) {
					logCodInicializacion.setLog_cod_inicializacion(rs.getString("DesInicializacion"));
				}
				logsCodInicializacion.add(logCodInicializacion);
				cerrarResultSet(rs);
			}

		} catch (Exception ex) {
			LOGGER.error("Error al obtener logs por escenario de una ejecucion agregada : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return logsCodInicializacion;
	}

	public ArrayList<LogCodInicializacion> obtenerLogsCodInicializacionVariacion(Integer id_Ejecucion)
			throws Exception {

		ArrayList<LogCodInicializacion> logsCodInicializacion = new ArrayList<LogCodInicializacion>();
		LogCodInicializacion logCodInicializacion = null;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select E.DesInicializacion from HE_Ejecucion E " + " where E.CODEJECUCION =" + id_Ejecucion;
		// Tipos de ejecucion f1 y f2 para inversiones son 7 y 8
		try {
			conn = obtenerConexion();

			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);

			logCodInicializacion = new LogCodInicializacion();

			while (rs.next()) {
				logCodInicializacion.setLog_cod_inicializacion(rs.getString("DesInicializacion"));
			}
			logsCodInicializacion.add(logCodInicializacion);
			cerrarResultSet(rs);

		} catch (Exception ex) {
			LOGGER.error("Error al obtener logs por escenario de una ejecucion agregada : " + ex.getMessage());
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return logsCodInicializacion;
	}

	public ConfiguracionSAS obtenerConfiguracionSASDesencriptado() {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String user = null;
		String pass = null;

		String sql = "SELECT * FROM UMA_ConfiguracionServidor WHERE NumCodConf = 1";
		ConfiguracionSAS configuracion = new ConfiguracionSAS();
		try {
			conn = obtenerConexion();
			stmt = crearStatement(conn);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {

				configuracion.setFlag_log(rs.getBoolean("BolModulo"));
				configuracion.setHost(rs.getString("VchHost"));
				configuracion.setPuerto(rs.getInt("NumPort"));
				if (rs.getString("VchUsuario") != null) {
					user = AES.decrypt(rs.getString("VchUsuario"), Claves.encriptadoRutaBD);
					pass = AES.decrypt(rs.getString("VchPass"), Claves.encriptadoRutaBD);
				} else {
					user = null;
					pass = null;
				}
				configuracion.setUsuario(user);
				configuracion.setContrasena(pass);
				configuracion.setRutaRaiz(rs.getString("VchConf1"));
				configuracion.setCodigoInicial(rs.getString("VchConf2"));
			}
		} catch (Exception ex) {
			LOGGER.error("Error al obtener la configuracion de SAS: " + ex.getMessage());
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(stmt);
			cerrarConexion(conn);
		}
		return configuracion;
	}

	private String agregarPartitionByCodigoCargaFichero(CargaFichero cargaFichero) throws Exception {

		String sql = "ALTER TABLE " + userName + ".HE_" + cargaFichero.getTablaInput().getNombre_fichero() + "_V"
				+ cargaFichero.getTablaInput().getVersion_metadata()
				+ " modify PARTITION BY RANGE (CODCARGAFICHERO) ( \r\n";
		Integer lastCodCargaFichero = obtenerValorActualDeCargaFichero();
		Integer codigosCargaFicheroPoranio = 1300; // se calculó un promedio de los últimos 3 años
		for (int i = 1; i < 10; i++) {
			int value = lastCodCargaFichero + codigosCargaFicheroPoranio * i;
			sql = sql + " PARTITION partition" + String.valueOf(i) + " VALUES LESS THAN (" + value + "),";
		}
		sql = sql + " PARTITION partifinal VALUES LESS THAN  (maxvalue))";
		return sql;
	}

	private String agregarIndex(CargaFichero cargaFichero) throws Exception {

		String sql = null;

		sql = " create INDEX  " + "HE_" + cargaFichero.getTablaInput().getNombre_fichero() + "_n"
				+ cargaFichero.getTablaInput().getVersion_metadata() + " on " + userName + ".HE_"
				+ cargaFichero.getTablaInput().getNombre_fichero() + "_v"
				+ cargaFichero.getTablaInput().getVersion_metadata() + " (CodCargaFichero) TABLESPACE DBNIIFDATA_IDX NOLOGGING ";

		return sql;
	}

	

	private boolean validarFicheroEsBorrable(CargaFichero cargaFichero) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt = null;

		ResultSet rs = null;
		boolean isBorrable = false;
		String sql = "select count (*) " + " from HE_MapeoEjecucion ME "
				+ "	where ME.CodEjecucion in ( select distinct t1.codejecucion from he_ejecucion t1 WHERE t1.TIPESTADOEJECUCION <> 0 ) and ME.codcargafichero= ?";
		try {
			conn = obtenerConexion();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, cargaFichero.getId_cargafichero());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				isBorrable = rs.getInt(1) == 0 ? true : false;
			}

		} catch (Exception ex) {
			LOGGER.error("Error al ejecutar script para borrado");
			throw ex;
		} finally {
			cerrarResultSet(rs);
			cerrarStatement(pstmt);
			cerrarConexion(conn);
		}
		return isBorrable;

	}

	public void borrarRegistrosDeFicheroAprovisionado(CargaFichero cargaFichero, Connection conn) throws Exception {
		PreparedStatement pstmt = null;
		String sql_borrarRegistros = "DELETE FROM " + userName + ".HE_"
				+ cargaFichero.getTablaInput().getNombre_fichero() + "_V"
				+ cargaFichero.getTablaInput().getVersion_metadata() + " WHERE codcargafichero = ?";

		//LOGGER.info(sql_borrarRegistros);
		try {
			pstmt = conn.prepareStatement(sql_borrarRegistros);
			pstmt.setInt(1, cargaFichero.getId_cargafichero());

			pstmt.executeUpdate();
			ejecutarCommit(conn);
			LOGGER.info("Se borró correctamente de la tabla los registros del fichero");
		} catch (Exception ex) {
			ejecutarRollback(conn);
			LOGGER.error("Error al borrar los registros de la carga: " + cargaFichero.getNombre_carga()
					+ " de la tabla " + cargaFichero.getTablaInput().getNombre_fichero() + " / " + ex.getMessage());
			throw ex;
		} finally {
			cerrarPreparedStatement(pstmt);
		}
	}

}
