package sim.persistencia;

import java.util.ArrayList;
import java.util.HashMap;

import sim.persistencia.dic.TipoEjecucion;

public class EjecucionMotor {
	private Integer codmes; 
	private TipoEjecucion tipo_ejecucion;
	private JuegoEscenarios_version juego_escenarios_version;	
	private ArrayList<BloqueParametrizado> bloquesParam_carteraVersion;	
	private ArrayList<GrupoBloquesEscenario> grupos_bloquesParamEscenario;
	private EjecucionPrimerPaso ejecucion_modMacro;
	private Integer tipo_ajusteManual;
	private EjecucionPrimerPaso ejecucion_repricing;	

	//Ejecuciones historicas (con carga de bloque principal)
	private ArrayList<EjecucionHistorica> ejecuciones_hist;
	
	//Informacion de Staging
	private Boolean stage_interno;
	private Boolean stage_externo;
	private Boolean stage_sensibilidad;
	private Integer meses_sensibilidad;
	private TablaInput tablaInputExpresiones;
	private HashMap<Integer,Regla_Staging> reglasStaging;
	private HashMap<String,Expresion_Staging> expresionesStaging;
	private HashMap<Integer,Stage> stagesStaging;
	private ArrayList<EjecucionAgregadaPE> ejecuciones_extrapolacion; // Solo para metodologías extrapolación
	
	//Unicamente para variaciones
	private Integer codmesAnt_variacion;
	//private Integer id_ejecucionAnt_variacion; //Solo del base
	
	public EjecucionMotor() {
		reglasStaging = new HashMap<Integer,Regla_Staging>();
		expresionesStaging = new HashMap<String,Expresion_Staging>();
		stagesStaging = new HashMap<Integer,Stage>();
	}
	
	public Integer getCodmes() {
		return codmes;
	}
	public void setCodmes(Integer codmes) {
		this.codmes = codmes;
	}
	public ArrayList<BloqueParametrizado> getBloquesParam_carteraVersion() {
		return bloquesParam_carteraVersion;
	}
	public void setBloquesParam_carteraVersion(ArrayList<BloqueParametrizado> bloquesParam_carteraVersion) {
		this.bloquesParam_carteraVersion = bloquesParam_carteraVersion;
	}
	public JuegoEscenarios_version getJuego_escenarios_version() {
		return juego_escenarios_version;
	}
	public void setJuego_escenarios_version(JuegoEscenarios_version juego_escenarios_version) {
		this.juego_escenarios_version = juego_escenarios_version;
	}
	public ArrayList<GrupoBloquesEscenario> getGrupos_bloquesParamEscenario() {
		return grupos_bloquesParamEscenario;
	}
	public void setGrupos_bloquesParamEscenario(ArrayList<GrupoBloquesEscenario> grupos_bloquesParamEscenario) {
		this.grupos_bloquesParamEscenario = grupos_bloquesParamEscenario;
	}

	public Boolean getStage_externo() {
		return stage_externo;
	}

	public void setStage_externo(Boolean stage_externo) {
		this.stage_externo = stage_externo;
	}

	public Boolean getStage_interno() {
		return stage_interno;
	}

	public void setStage_interno(Boolean stage_interno) {
		this.stage_interno = stage_interno;
	}

	public Integer getMeses_sensibilidad() {
		return meses_sensibilidad;
	}

	public void setMeses_sensibilidad(Integer meses_sensibilidad) {
		this.meses_sensibilidad = meses_sensibilidad;
	}

	public Boolean getStage_sensibilidad() {
		return stage_sensibilidad;
	}

	public void setStage_sensibilidad(Boolean stage_sensibilidad) {
		this.stage_sensibilidad = stage_sensibilidad;
	}

	public TablaInput getTablaInputExpresiones() {
		return tablaInputExpresiones;
	}

	public void setTablaInputExpresiones(TablaInput tablaInputExpresiones) {
		this.tablaInputExpresiones = tablaInputExpresiones;
	}

	public HashMap<Integer, Regla_Staging> getReglasStaging() {
		return reglasStaging;
	}

	public void setReglasStaging(HashMap<Integer, Regla_Staging> reglasStaging) {
		this.reglasStaging = reglasStaging;
	}

	public HashMap<String, Expresion_Staging> getExpresionesStaging() {
		return expresionesStaging;
	}

	public void setExpresionesStaging(HashMap<String, Expresion_Staging> expresionesStaging) {
		this.expresionesStaging = expresionesStaging;
	}

	public HashMap<Integer, Stage> getStagesStaging() {
		return stagesStaging;
	}

	public void setStagesStaging(HashMap<Integer, Stage> stagesStaging) {
		this.stagesStaging = stagesStaging;
	}
	
	public void agregarReglaStaging(Integer id, Regla_Staging regla) {
		reglasStaging.put(id, regla);
	}
	
	public void agregarExpresionStaging(String id, Expresion_Staging stage) {
		expresionesStaging.put(id, stage);
	}
	
	public void agregarStageStaging(Integer id, Stage stage) {
		stagesStaging.put(id, stage);
	}

	public TipoEjecucion getTipo_ejecucion() {
		return tipo_ejecucion;
	}

	public void setTipo_ejecucion(TipoEjecucion tipo_ejecucion) {
		this.tipo_ejecucion = tipo_ejecucion;
	}

	public ArrayList<EjecucionAgregadaPE> getEjecuciones_extrapolacion() {
		return ejecuciones_extrapolacion;
	}

	public void setEjecuciones_extrapolacion(ArrayList<EjecucionAgregadaPE> ejecuciones_extrapolacion) {
		this.ejecuciones_extrapolacion = ejecuciones_extrapolacion;
	}

	public EjecucionPrimerPaso getEjecucion_modMacro() {
		return ejecucion_modMacro;
	}

	public void setEjecucion_modMacro(EjecucionPrimerPaso ejecucion_modMacro) {
		this.ejecucion_modMacro = ejecucion_modMacro;
	}

	public EjecucionPrimerPaso getEjecucion_repricing() {
		return ejecucion_repricing;
	}

	public void setEjecucion_repricing(EjecucionPrimerPaso ejecucion_repricing) {
		this.ejecucion_repricing = ejecucion_repricing;
	}

	public Integer getTipo_ajusteManual() {
		return tipo_ajusteManual;
	}

	public void setTipo_ajusteManual(Integer tipo_ajusteManual) {
		this.tipo_ajusteManual = tipo_ajusteManual;
	}

	public ArrayList<EjecucionHistorica> getEjecuciones_hist() {
		return ejecuciones_hist;
	}

	public void setEjecuciones_hist(ArrayList<EjecucionHistorica> ejecuciones_hist) {
		this.ejecuciones_hist = ejecuciones_hist;
	}

	public Integer getCodmesAnt_variacion() {
		return codmesAnt_variacion;
	}

	public void setCodmesAnt_variacion(Integer codmesAnt_variacion) {
		this.codmesAnt_variacion = codmesAnt_variacion;
	}

}
