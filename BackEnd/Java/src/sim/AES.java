package sim;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
 
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
 
public class AES {
 
	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(AES.class);
    private static SecretKeySpec secretKey;
    private static IvParameterSpec ivParameterSpec;
    private static final int keySize = 16;
    private static final int ivSize = 16;
    private static byte[] key;
    private static byte[] iv;
 
    private static void setKey(String myKey) {
        MessageDigest sha = null;
        try {
        	//Extract encrypted part
        	
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, keySize);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Error algoritmo desconocido: " + e.getMessage());
        } catch (UnsupportedEncodingException e) {
        	LOGGER.error("Error encoding no soportado: " + e.getMessage());
        }
    }
 
    public static String decrypt(String strToDecrypt, String secret) {
    //	LOGGER.info("Cadena encriptada: " + strToDecrypt);
        try {
        	//Extract iv
            iv = new byte[ivSize];
            byte[] encryptedIvTextBytes = secret.getBytes();;
            System.arraycopy(encryptedIvTextBytes, 0, iv, 0, iv.length);
            ivParameterSpec = new IvParameterSpec(iv);
            
            
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); 
            cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);
            
          //  LOGGER.info("Cadena desencriptada: " + new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt))));
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (Exception e) {
            LOGGER.error("Error while decrypting: " + e.toString());
        }
        return null;
    }
}