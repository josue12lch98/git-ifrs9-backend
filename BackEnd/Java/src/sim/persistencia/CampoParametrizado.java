package sim.persistencia;

public class CampoParametrizado {
	CampoInput campo_input;
	CampoBloque campo_bloque;
	Double numValor;
	String txtValor;
	
	public CampoParametrizado() {		
	}
	
	public CampoInput getCampo_input() {
		return campo_input;
	}
	public void setCampo_input(CampoInput campo_input) {
		this.campo_input = campo_input;
	}
	public CampoBloque getCampo_bloque() {
		return campo_bloque;
	}
	public void setCampo_bloque(CampoBloque campo_bloque) {
		this.campo_bloque = campo_bloque;
	}
	public Double getNumValor() {
		return numValor;
	}
	public void setNumValor(Double numValor) {
		this.numValor = numValor;
	}
	public String getTxtValor() {
		return txtValor;
	}
	public void setTxtValor(String txtValor) {
		this.txtValor = txtValor;
	}
}
