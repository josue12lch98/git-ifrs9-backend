package sim.persistencia;

import java.util.ArrayList;

public class ReplicaEjecucionesPrimerPaso {
	private Integer codmes_replica;	
	private ArrayList<EjecucionPrimerPaso> ejecuciones_replicar;
	
	public ReplicaEjecucionesPrimerPaso() {}
	
	public Integer getCodmes_replica() {
		return codmes_replica;
	}
	public void setCodmes_replica(Integer codmes_replica) {
		this.codmes_replica = codmes_replica;
	}
	public ArrayList<EjecucionPrimerPaso> getEjecuciones_replicar() {
		return ejecuciones_replicar;
	}
	public void setEjecuciones_replicar(ArrayList<EjecucionPrimerPaso> ejecuciones_replicar) {
		this.ejecuciones_replicar = ejecuciones_replicar;
	}
}
