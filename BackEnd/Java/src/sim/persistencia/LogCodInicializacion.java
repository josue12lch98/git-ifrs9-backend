package sim.persistencia;

public class LogCodInicializacion {

	private Escenario escenario;
	private String logsCodInicializacion;

	public Escenario getEscenario() {
		return escenario;
	}

	public void setEscenario(Escenario escenario) {
		this.escenario = escenario;
	}

	public String getLog_cod_inicializacion() {
		return logsCodInicializacion;
	}

	public void setLog_cod_inicializacion(String log_cod_inicializacion) {
		this.logsCodInicializacion = log_cod_inicializacion;
	}

	
}
