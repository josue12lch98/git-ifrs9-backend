package sim.persistencia;

public class ResultadoVariableMacro {
	private Integer id_metodologia;
	private Integer fecha_ejecucion;
	private Integer numPDBase;
	private Integer numPDOptimista;
	private Integer numPDAdverso;
	private Integer fecha_proyeccion;

	public Integer getId_metodologia() {
		return id_metodologia;
	}

	public void setId_metodologia(Integer id_metodologia) {
		this.id_metodologia = id_metodologia;
	}

	public Integer getFecha_ejecucion() {
		return fecha_ejecucion;
	}

	public void setFecha_ejecucion(Integer fecha_ejecucion) {
		this.fecha_ejecucion = fecha_ejecucion;
	}

	public Integer getNumPDBase() {
		return numPDBase;
	}

	public void setNumPDBase(Integer numPDBase) {
		this.numPDBase = numPDBase;
	}

	public Integer getNumPDOptimista() {
		return numPDOptimista;
	}

	public void setNumPDOptimista(Integer numPDOptimista) {
		this.numPDOptimista = numPDOptimista;
	}

	public Integer getNumPDAdverso() {
		return numPDAdverso;
	}

	public void setNumPDAdverso(Integer numPDAdverso) {
		this.numPDAdverso = numPDAdverso;
	}

	public Integer getFecha_proyeccion() {
		return fecha_proyeccion;
	}

	public void setFecha_proyeccion(Integer fecha_proyeccion) {
		this.fecha_proyeccion = fecha_proyeccion;
	}

}
