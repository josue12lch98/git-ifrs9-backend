package sim.persistencia;

public class PrimerPaso {
	private Integer id_primerPaso;
	private String nombre_primerPaso;
	private String nombre_variablemotor;
	private String descripcion_primerPaso;
	private Integer periodicidad_meses;
	
	//Solo para ejecuciones
	private String codigo_primerPaso;
	
	public PrimerPaso() {		
	}
		
	public Integer getId_primerPaso() {
		return id_primerPaso;
	}
	public void setId_primerPaso(Integer id_primerPaso) {
		this.id_primerPaso = id_primerPaso;
	}
	public String getNombre_primerPaso() {
		return nombre_primerPaso;
	}
	public void setNombre_primerPaso(String nombre_primerPaso) {
		this.nombre_primerPaso = nombre_primerPaso;
	}
	public String getDescripcion_primerPaso() {
		return descripcion_primerPaso;
	}
	public void setDescripcion_primerPaso(String descripcion_primerPaso) {
		this.descripcion_primerPaso = descripcion_primerPaso;
	}

	public String getNombre_variablemotor() {
		return nombre_variablemotor;
	}

	public void setNombre_variablemotor(String nombre_variablemotor) {
		this.nombre_variablemotor = nombre_variablemotor;
	}

	public Integer getPeriodicidad_meses() {
		return periodicidad_meses;
	}

	public void setPeriodicidad_meses(Integer periodicidad_meses) {
		this.periodicidad_meses = periodicidad_meses;
	}

	public String getCodigo_primerPaso() {
		return codigo_primerPaso;
	}

	public void setCodigo_primerPaso(String codigo_primerPaso) {
		this.codigo_primerPaso = codigo_primerPaso;
	}	
}
