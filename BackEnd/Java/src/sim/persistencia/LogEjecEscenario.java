package sim.persistencia;

public class LogEjecEscenario {
	private Escenario escenario;
	private String log_escenario;
	
	public Escenario getEscenario() {
		return escenario;
	}
	public void setEscenario(Escenario escenario) {
		this.escenario = escenario;
	}
	public String getLog_escenario() {
		return log_escenario;
	}
	public void setLog_escenario(String log_escenario) {
		this.log_escenario = log_escenario;
	}
}
