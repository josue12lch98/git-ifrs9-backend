package sim.persistencia;

public class Stage {

	private Integer id_stage;
	private String nombre_stage;
	private String nombre_variable_motor;
	private Boolean flag_obligatorio;
	
	public Stage() {
	}
	
	public Integer getId_stage() {
		return id_stage;
	}
	public void setId_stage(Integer id_stage) {
		this.id_stage = id_stage;
	}
	public String getNombre_stage() {
		return nombre_stage;
	}
	public void setNombre_stage(String nombre_stage) {
		this.nombre_stage = nombre_stage;
	}
	public Boolean getFlag_obligatorio() {
		return flag_obligatorio;
	}
	public void setFlag_obligatorio(Boolean flag_obligatorio) {
		this.flag_obligatorio = flag_obligatorio;
	}

	public String getNombre_variable_motor() {
		return nombre_variable_motor;
	}

	public void setNombre_variable_motor(String nombre_variable_motor) {
		this.nombre_variable_motor = nombre_variable_motor;
	}

	
	
}
