package sim.controlador;

import sim.AES;

import sim.BCrypt;
import sim.Claves;
import sim.PropertiesCompartido;
import sim.PropertiesWrapper;
import sim.autenticacion.ModuloLDAP;
import sim.calculo.ModuloSAS;
import sim.calculo.ModuloSASLocal;
import sim.calculo.ModuloMatlab;
import sim.calculo.ModuloR;
import sim.notificacion.ModuloCorreo;
import sim.persistencia.*;
import sim.persistencia.bd.ModuloDatos;
import sim.persistencia.bd.ModuloDatos_Factory;
import sim.persistencia.dic.FormatoInput;
import sim.persistencia.dic.MotorCalculo;
import sim.persistencia.dic.Pais;
import sim.persistencia.dic.TipoAmbito;
import sim.persistencia.dic.TipoBloque;
import sim.persistencia.dic.TipoCampo;
import sim.persistencia.dic.TipoCartera;
import sim.persistencia.dic.TipoEjecucion;
import sim.persistencia.dic.TipoFlujo;
import sim.persistencia.dic.TipoMagnitud;
import sim.persistencia.dic.TipoMetodologia;
import sim.persistencia.dic.TipoObtencionResultados;
import sim.persistencia.dic.TipoPeriodicidad;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Properties;
import java.lang.reflect.*;
import java.net.InetAddress;

//JJWT
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class Controlador {
	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(Controlador.class);
	private static Properties keyProperties;
	private static Properties properties;
	private static Controlador controlador;
	private static ModuloDatos md;
	private static ModuloMatlab matlab;
	private static ModuloR r;
	private static ModuloSAS sas;
	private static ModuloSASLocal sas2;
	private static ModuloCorreo correo;
	private static ModuloLDAP ldap;

	public Controlador() throws Exception {

		LOGGER.info("Creando controlador");

		// Se generan claves aleatorias
		try {
			LOGGER.info("Generando claves aleatorias");
			Claves.claveToken = BCrypt.gensalt(10);
			LOGGER.info("Claves generadas");
		} catch (Exception e) {
			LOGGER.fatal("Error al generar claves aleatorias!: " + e.getMessage());
		}

		// Se obtiene la clave y el algoritmo para desencriptar las propiedades
		InputStream isKey = null;

		// Traduccion propiedad 'magia'
		String valTxtByteArrMagia = null;
		String[] strArrMagia = null;
		byte[] byteArrMagia = null;
		int idx = 0;
		InputStream isProp = null;
		String dirConfig = System.getProperty("jboss.server.config.dir");
		// String ruta = "";
		String separador = "\\";
		try {
			LOGGER.info(dirConfig);

			LOGGER.info("Obteniendo la llave para desencriptar propiedades");
			isKey = new FileInputStream(dirConfig + separador + "keyseed.txt");
			// isKey = getClass().getResourceAsStream("../keyseed.txt");
			keyProperties = new Properties();
			keyProperties.load(isKey);

			LOGGER.info("Ubicacion de la semilla: " + keyProperties.getProperty("llave"));
			LOGGER.info("Â¿Semilla encriptada?: " + keyProperties.getProperty("encryptedSeed"));
			LOGGER.info("Obteniendo archivo de propiedades");
			isProp = getClass().getResourceAsStream("../sim.properties");
			isProp = new FileInputStream(dirConfig + separador + "sim.properties");
			if (keyProperties.getProperty("encryptedSeed") != null
					&& keyProperties.getProperty("encryptedSeed").toString().equalsIgnoreCase("true")) {

				// Pasamos de el valor texto de 'bytearray' de la property a un objeto bytearray
				valTxtByteArrMagia = keyProperties.getProperty("magia");

				// Removemos llaves (extremos)
				valTxtByteArrMagia = valTxtByteArrMagia.substring(1, valTxtByteArrMagia.length() - 1);

				// Separamos por comas y recorremos elementos para completar el bytearray
				strArrMagia = valTxtByteArrMagia.split(",");
				byteArrMagia = new byte[strArrMagia.length];
				idx = 0;
				for (String numStr : strArrMagia) {
					byteArrMagia[idx] = Integer.valueOf(numStr).byteValue();
					idx++;
				}

				properties = new PropertiesWrapper(keyProperties.getProperty("llave"), byteArrMagia, true);
				LOGGER.info("Properties inicializada con semilla encriptada");
			} else {
				// Pasamos de el valor texto de 'bytearray' de la property a un objeto bytearray
				valTxtByteArrMagia = keyProperties.getProperty("magia");

				// Removemos llaves (extremos)
				valTxtByteArrMagia = valTxtByteArrMagia.substring(1, valTxtByteArrMagia.length() - 1);

				// Separamos por comas y recorremos elementos para completar el bytearray
				strArrMagia = valTxtByteArrMagia.split(",");
				byteArrMagia = new byte[strArrMagia.length];
				idx = 0;
				for (String numStr : strArrMagia) {
					byteArrMagia[idx] = Integer.valueOf(numStr).byteValue();
					idx++;
				}

				properties = new PropertiesWrapper(keyProperties.getProperty("llave"), byteArrMagia, false);
				LOGGER.info("Properties inicializada");
			}
			LOGGER.info("Cargando archivo de propiedades");
			properties.load(isProp);
		} catch (Exception e) {
			LOGGER.fatal("No se ha podido cargar el fichero key/propiedades correctamente. Error: " + e.getMessage());
		} finally {
//	             try {
//	                    if (isKey != null) {
//	                           isKey.close();
//	                    }
//	             } catch (IOException e) {
//	                    LOGGER.error("Error al cerrar el InputStream del path key. Error: " + e.getMessage());
//	             }

			try {
				if (isProp != null) {
					isProp.close();
				}
			} catch (IOException e) {
				LOGGER.error("Error al cerrar el InputStream 1 properties. Error: " + e.getMessage());
			}
		}
		// Se setean los properties compartidos
		try {
			LOGGER.info("Seteando properties compartidas");

			PropertiesCompartido.lifetimeTokenAcceso = Integer.valueOf(properties.getProperty("LifetimeTokenAcceso"));
			PropertiesCompartido.lifetimeSesion = Integer.valueOf(properties.getProperty("LifetimeSesion"));
			PropertiesCompartido.protocol_client = properties.getProperty("protocol_client");
			PropertiesCompartido.host_client = properties.getProperty("host_client");
			PropertiesCompartido.ruta_logs = properties.getProperty("RutaLogs"); // Para registros dentro de
																					// LoggerWrapper
			LOGGER.info("Properties compartidas seteadas");
		} catch (Exception e) {
			LOGGER.error("Error al setear properties compartido!: " + e.getMessage());
		}

		// Se crea el modulo de datos
		try {
			md = new ModuloDatos_Factory().getMD(properties);
		} catch (Exception e) {
			LOGGER.fatal("Error al crear ModuloDatos: " + e.getMessage());
		}

		// Se crea el modulo de Correos
		try {
			if (properties.getProperty("Modulo_SMTP").equalsIgnoreCase("SI")) {
				correo = new ModuloCorreo(properties);
			} else {
				LOGGER.warn(
						"Notificaciones por correo desactivado, si se desea activar cambiar la propiedad a SI y reiniciar el servicio");
			}
		} catch (Exception e) {
			LOGGER.fatal("Error al crear Modulo Correos: " + e.getMessage());
		}

		// Se crea el modulo de LDAP
		try {
			if (properties.getProperty("Modulo_LDAP").equalsIgnoreCase("SI")) {
				ldap = new ModuloLDAP(properties);
			} else {
				LOGGER.warn(
						"Modulo LDAP desactivado, si se desea activar cambiar la propiedad a SI y reiniciar el servicio");
			}
		} catch (Exception e) {
			LOGGER.fatal("Error al crear Modulo LDAP: " + e.getMessage());
		}

		// Se crea el modulo de SAS
		try {
			// ConfiguracionSAS confSAS = md.obtenerConfiguracionSASDesencriptado();
			sas = new ModuloSAS(md, properties);
			if (properties.getProperty("Modulo_SAS").equalsIgnoreCase("SI")) {
				sas = new ModuloSAS(md, properties);
			} else {
				sas2 = new ModuloSASLocal(properties, md);
				LOGGER.warn(
						"Modulo SAS desactivado, si se desea activar cambiar la propiedad a SI y reiniciar el servicio");
			}

		} catch (Exception e) {
			LOGGER.fatal("Error al crear ModuloSAS: " + e.getMessage());
		}

		// Se crea el modulo de Matlab
		try {
			if (properties.getProperty("Modulo_Matlab").equalsIgnoreCase("SI")) {
				matlab = new ModuloMatlab(properties, md);
			} else {
				LOGGER.warn(
						"Modulo MATLAB desactivado, si se desea activiar cambiar la propiedad a SI y reiniciar el servicio");
			}
		} catch (Exception e) {
			LOGGER.fatal("Error al crear el Modulo MATLAB: " + e.getMessage());
		}

//		 Se crea el modulo de R
//		try {
//			if (properties.getProperty("Modulo_R").equalsIgnoreCase("SI")) {
//				r = new ModuloR(properties, md);
//			} else {
//				LOGGER.warn(
//						"Modulo R desactivado, si se desea activiar cambiar la propiedad a SI y reiniciar el servicio");
//			}
//		} catch (Exception e) {
//			LOGGER.fatal("Error al crear el Modulo R: " + e.getMessage());
//		}

		LOGGER.info("Controlador creado");

	}

	public static synchronized Controlador obtenerInstancia() throws Exception {
		if (controlador == null) {
			controlador = new Controlador();
		}
		return controlador;
	}

	// enviar notificacion
	private void enviarNotificacion(Integer id_tipoAccion, Integer id_tipoObjeto, String nombreObjeto,
			Integer id_tipoAmbito, ArrayList<String> destinatarios) {
		String mensaje = "";
		Boolean notificar = true;
		if (id_tipoAccion.equals(18) && id_tipoObjeto.equals(37)) {
			mensaje = "      EVENTO: Fichero *" + nombreObjeto + "* Aprovisionado";
		} else if (id_tipoAccion.equals(19) && id_tipoObjeto.equals(37)) {
			mensaje = "      EVENTO: Fichero *" + nombreObjeto + "* Aprobados";
		} else if (id_tipoAccion.equals(12) && id_tipoObjeto.equals(29)) {
			mensaje = "      EVENTO: Primer Paso *" + nombreObjeto + "* Ejecutado";
		} else if (id_tipoAccion.equals(12) && id_tipoObjeto.equals(19)) {
			mensaje = "      EVENTO: Ficheros PE fase 1 *" + nombreObjeto + "* Ejecutada";
		} else if (id_tipoAccion.equals(12) && id_tipoObjeto.equals(21)) {
			mensaje = "      EVENTO: Ficheros PE fase 2 *" + nombreObjeto + "* Ejecutada";
		} else if (id_tipoAccion.equals(12) && id_tipoObjeto.equals(23)) {
			mensaje = "      EVENTO: Variacion PE *" + nombreObjeto + "* Ejecutada";
		} else if (id_tipoAccion.equals(8) && id_tipoObjeto.equals(24)) {
			mensaje = "      EVENTO: Cálculo de Ejecución *" + nombreObjeto + "* Validado";
		} else if (id_tipoAccion.equals(8) && id_tipoObjeto.equals(24)) {
			mensaje = "      EVENTO: Cálculo de Ejecución *" + nombreObjeto + "*";
		} else if (id_tipoAccion.equals(10) && id_tipoObjeto.equals(24)) {
			mensaje = "      EVENTO: Cálculo de Ejecución *" + nombreObjeto + "* Oficializado";
		} else {
			LOGGER.warn("Notificacion no identificada, no se enviara correo");
			notificar = false;
		}
		if (notificar) {
			for (String buzon : destinatarios) {
				correo.enviarCorreo(buzon, mensaje);
			}
		}
	}

	// Comprobar si un Array contiene lo mismo que otro Array (no importa orden ni
	// duplicados)
	private static <T> boolean listEqualsIgnoreOrder(ArrayList<T> list1, ArrayList<T> list2) {
		return new HashSet<>(list1).equals(new HashSet<>(list2));
	}

	// Aprovisionamiento
	public ArrayList<Ruta> obtenerRutas(Integer id_tipo_ambito) throws Exception {
		return md.obtenerRutas(id_tipo_ambito);
	}

	public void crearRuta(Ruta ruta) throws Exception {
		md.crearRuta(ruta);
	}

	public RutaValida editarRuta(Ruta ruta) throws Exception {
		return md.editarRuta(ruta);
	}

	public void borrarRuta(Integer id_ruta) throws Exception {
		md.borrarRuta(id_ruta);
	}

	public void seleccionarRutas(ArrayList<Ruta> rutas) throws Exception {
		for (Ruta ruta : rutas) {
			md.editarRuta(ruta);
		}
	}

	public ArrayList<Aprovisionamiento> obtenerAprovisionamientoInput() throws Exception {
		return md.obtenerAprovisionamientoInput();
	}

	public ArrayList<CampoAprovisionamiento> obtenerCampos(Aprovisionamiento aprov) throws Exception {
		return md.obtenerCampos(aprov);
	}

	public ArrayList<ReglaCalidad> obtenerReglasCalidad(ReglaCalidad regla) throws Exception {
		return md.obtenerReglasCalidad(regla);
	}

	public void crearAprovisionamiento(Aprovisionamiento fichero) throws Exception {
		md.crearAprovisionamiento(fichero);
	}

	public void editarAprovisionamiento(Aprovisionamiento fichero) throws Exception {
		md.editarAprovisionamiento(fichero);
	}

	public void borrarAprovisionamiento(Integer id_aprovisionamiento) throws Exception {
		md.borrarAprovisionamiento(id_aprovisionamiento);
	}

	public Aprovisionamiento obtenerFicheroConCampos(Aprovisionamiento fichero) throws Exception {
		ArrayList<String> rutas = md.obtenerSoloRutasTexto(fichero.getTipo_ambito().getId_tipo_ambito(), true);
		return md.obtenerFicheroConCampos(fichero, rutas);
	}

	// Se usa para identificar los archivos en rutas configuradas
	public ArrayList<CargaFichero> obtenerCargasFicheros(Integer id_tipo_ambito) throws Exception {
		ArrayList<TablaInput> tablainputs = null;
		TablaInput ultimaTablaInput = null;
		// Flag original de campos de ficheros

		// Se obtiene las rutas en donde se buscaran los ficheros y luego se obtienen
		// todos los ficheros (con campos) de esas rutas
		ArrayList<CargaFichero> cargasFichero = md
				.obtenerCargasFicheros(md.obtenerSoloRutasTexto(id_tipo_ambito, false));
		try {
			// Se procesa fichero por fichero para mejor rendimiento de la memoria java
			for (CargaFichero cargaFichero : cargasFichero) {
				// Se asigna el tipo de ambito del fichero en el aprovisionamiento
				cargaFichero.getTablaInput().getTipo_ambito().setId_tipo_ambito(id_tipo_ambito);

				cargaFichero.setTablaInput(md.obtenerCamposdeCargaFichero(cargaFichero.getTablaInput()));
				// Se valida que el fichero no haya tenido errores, sino no podra ser procesado
				if (cargaFichero.getTablaInput().isFlag_error()) {
					LOGGER.warn("El fichero: " + cargaFichero.getTablaInput().getNombre_fichero()
							+ " no sera procesado porque se le encontraron errores");
				} else {
					// Se obtiene el ultimo aprovisionamiento (incluyendo campos) del fichero
					cargaFichero.setTablaInput(md.obtenerUltimoAprovisionamiento(cargaFichero.getTablaInput()));
					// Se evalua si tiene ultimo aprovisionamiento
					if (cargaFichero.getTablaInput().isFlag_tiene_aprov()) {
						// Se evalua si el fichero tiene los mismos campos que el ultimo
						// aprovisionamiento
						try {
							if (!listEqualsIgnoreOrder(
									cargaFichero.getTablaInput().obtenerNombresCamposAprov(false, true),
									cargaFichero.getTablaInput().obtenerNombresCamposFichero())) {
								cargaFichero.getTablaInput().setFlag_warn_definicion(true);
								cargaFichero.getTablaInput().setFlag_tiene_aprov(true);
								cargaFichero.getTablaInput().setFlag_warning(true);
								cargaFichero.getTablaInput()
										.setObservacion("Los campos son diferentes a la configuración");
							} else {
								tablainputs = md.obtenerTablasInputUltVerMetadataPorAprov(
										cargaFichero.getTablaInput().getId_aprovisionamiento());
								if (tablainputs.size() == 0) {
									cargaFichero.getTablaInput().setFlag_correcto(true);
									cargaFichero.getTablaInput().setFlag_campos_difer(true);
									cargaFichero.getTablaInput()
											.setObservacion("Nuevo fichero listo para aprovisionar");
									cargaFichero.getTablaInput().setVersion_metadata(1);
									cargaFichero.getTablaInput()
											.setFlag_normalizada(!cargaFichero.getTablaInput().getFlag_operacion());
									cargaFichero.setVersion_carga(1);
								} else {
									ultimaTablaInput = tablainputs.get(0);
									ultimaTablaInput
											.setCampos_input(md.obtenerCamposporTablaInputVersion(ultimaTablaInput));
									if (!listEqualsIgnoreOrder(
											cargaFichero.getTablaInput().obtenerNombresCamposAprovconTipo(),
											ultimaTablaInput.obtenerNombresCamposInputconTipo())) {
										cargaFichero.getTablaInput().setFlag_correcto(true);
										cargaFichero.getTablaInput().setFlag_campos_difer(true);
										cargaFichero.getTablaInput()
												.setObservacion("Se requiere definir la nueva metadata");
										cargaFichero.getTablaInput()
												.setVersion_metadata(ultimaTablaInput.getVersion_metadata() + 1);
										cargaFichero.getTablaInput()
												.setFlag_normalizada(!cargaFichero.getTablaInput().getFlag_operacion());
										cargaFichero.setVersion_carga(1);
									} else {
										cargaFichero.getTablaInput().setFlag_correcto(true);
										cargaFichero.getTablaInput().setFlag_campos_difer(false);
										cargaFichero.getTablaInput().setObservacion("Fichero listo para aprovisionar");
										cargaFichero.getTablaInput()
												.setCampos_input(ultimaTablaInput.getCampos_input());
										cargaFichero.getTablaInput()
												.setFlag_ajustado(ultimaTablaInput.getFlag_ajustado());
										cargaFichero.getTablaInput()
												.setId_tablainput(ultimaTablaInput.getId_tablainput());
										cargaFichero.getTablaInput()
												.setNombre_metadata(ultimaTablaInput.getNombre_metadata());
										cargaFichero.getTablaInput()
												.setVersion_metadata(ultimaTablaInput.getVersion_metadata());
									}
								}
							}
						} catch (Exception ex) {
							LOGGER.error("Error al comparar nombres campos fichero en el controlador: "
									+ cargaFichero.getTablaInput().getNombre_fichero());
							throw ex;
						}
					} else {
						// Si no tiene aprovisionamiento, entonces requiere definicion
						cargaFichero.getTablaInput().setFlag_definir_tabla(true);
						cargaFichero.getTablaInput().setFlag_tiene_aprov(false);
						cargaFichero.getTablaInput().setFlag_warning(true);
						cargaFichero.getTablaInput().setObservacion("No registrado para aprovisionar");
					}
				}
			}
		} catch (Exception exc) {
			LOGGER.error("Error al obtener ficheros: " + exc.getLocalizedMessage());
		}
		return cargasFichero;
	}

	public void editarFichero(Aprovisionamiento aprov) throws Exception {
		md.editarFichero(aprov);
	}

	public CargaFichero procesarCargaFichero(CargaFichero cargaFichero) throws Exception {
		ArrayList<CargaFichero> cargas_procesadas = new ArrayList<CargaFichero>();

		Date date = new Date(System.currentTimeMillis());// the date instance
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		ArrayList<String> nombres_carp = new ArrayList<String>();

		// Crear nueva ruta y eliminar la anterior
		String ruta = cargaFichero.getTablaInput().getRuta();
		String nueva_ruta = ruta + "\\" + "aprov_" + calendar.get(Calendar.YEAR)
				+ String.format("%02d", (calendar.get(Calendar.MONTH) + 1))
				+ String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
				+ String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY))
				+ String.format("%02d", calendar.get(Calendar.MINUTE));

		File carpeta = new File(ruta);

		File[] files = carpeta.listFiles();

		Arrays.sort(files);

		for (File file : carpeta.listFiles()) {
			if (file.isDirectory() && file.getName().length() > 6 && file.getName().substring(0, 6).equals("aprov_")) {
				nombres_carp.add(file.getName());
			}
		}

		Integer maxrespaldo = Integer.valueOf(properties.getProperty("MaxRespaldoAprov"));
		if (maxrespaldo == null) {
			maxrespaldo = 1;
		}

		for (int i = 0; (nombres_carp.size() - i) > (maxrespaldo - 1); i++) {
			for (File file : new File(ruta + "\\" + nombres_carp.get(i)).listFiles()) {
				if (file.delete()) {
					System.out.println("El archivo " + file.getName() + " se ha borrado correctamente");
				} else {
					System.out.println("El archivo " + file.getName() + " no se ha podido borrar");
				}
			}

			if (new File(ruta + "\\" + nombres_carp.get(i)).delete()) {
				System.out.println("La carpeta " + nombres_carp.get(i) + " se ha borrado correctamente");
			} else {
				System.out.println("La carpeta " + nombres_carp.get(i) + " no se ha podido borrar");
			}
		}
		new File(nueva_ruta).mkdirs();

		// Se procesa fichero por fichero para mejor rendimiento de la memoria java

		if (cargaFichero.getTablaInput().isFlag_campos_difer()) {
			LOGGER.info("Tabla input existe pero con modificaciones, se crea nueva tabla");
			long tiempoInicio = System.currentTimeMillis();
			cargaFichero = md.crearCargaTablaInput(cargaFichero);
			long tiempoFinal = System.currentTimeMillis();
			cargaFichero.setTiempoDeCarga((int) (tiempoFinal - tiempoInicio));
			cargaFichero.setNumeroCampos(cargaFichero.getTablaInput().getCampos_aprovisionamiento().size());
			cargaFichero.setTablaDestino("HE_" + cargaFichero.getTablaInput().getNombre_fichero().toUpperCase() + "_V"
					+ cargaFichero.getTablaInput().getVersion_metadata());
			md.actualizarNuevaCargaFichero(cargaFichero);
			LOGGER.info(
					"Actualización de tabla exitosa de Tabla input existe pero con modificaciones, se crea nueva tabla");

		} else {
			LOGGER.info("Tabla input existe, sin modificaciones, solo se incluyen registros");
			cargaFichero = md.insertarRegistrosCargaFichero(cargaFichero);
		}
		if (!cargaFichero.getTablaInput().getFlag_normalizada()) {
			// parte de SAS
			try {
				LOGGER.info("Iniciando ejecucion de codigos de calidad de datos en SAS");
				// LOGGER.info("Iniciando ejecucion de codigos de calidad de datos");
				if (sas != null) {
					sas.ejecutarCalidadDatos(cargaFichero);
				}
				// md.ejecutarCalidadDatos(cargaFichero); //TST CON SQL

				cargaFichero.setTipoEstadoCarga(1);
				md.editarCargaFichero(cargaFichero);
				LOGGER.info("Saliendo de módulo SAS calidad");
				// LOGGER.info("Saliendo de modulo de calidad");
			} catch (Exception ex) {
				LOGGER.error("Error al ejecutar codigo de calidad de datos en SAS:" + ex.getMessage());
				// LOGGER.error("Error al ejecutar codigo de calidad de datos:" +
				// ex.getMessage());
				cargaFichero.setTipoEstadoCarga(1);
				throw ex;
			}
		} else {
			cargaFichero.setTipoEstadoCarga(1);
			md.editarCargaFichero(cargaFichero);
		}
		cargaFichero.setFlag_ultimaCarga(true);
		cargas_procesadas.add(cargaFichero);

		// Mover Archivos
		Path temp = Files.move(Paths.get(ruta + "\\" + cargaFichero.getTablaInput().getNombre_archivo()),
				Paths.get(nueva_ruta + "\\" + cargaFichero.getTablaInput().getNombre_archivo()));

		if (temp != null) {
			System.out.println(
					"El archivo " + cargaFichero.getTablaInput().getNombre_archivo() + " se movió correctamente");
		} else {
			System.out.println("El archivo " + cargaFichero.getTablaInput().getNombre_archivo() + " tuvo un error");
		}

		return cargaFichero;
	}

	public void editarCargaFichero(CargaFichero cargaFichero) throws Exception {
		md.editarCargaFichero(cargaFichero);
	}

	public ArrayList<CargaFichero> obtenerCargasFicheroTotales() throws Exception {
		return md.obtenerCargasFicheroTotales();
	}

	public ArrayList<AjusteFilaInput> obtenerRegistrosdeCargaFichero(CargaFichero cargaFichero) throws Exception {
		return md.obtenerRegistrosdeCargaFichero(cargaFichero);
	}

	public void editarRegistrodeCargaFichero(AjusteFilaInput ajusteFilaInput) throws Exception {
		md.editarRegistrodeCargaFichero(ajusteFilaInput);
	}

	public void crearRegistrodeCargaFichero(AjusteFilaInput ajusteFilaInput) throws Exception {
		md.crearRegistrodeCargaFichero(ajusteFilaInput);
	}

	public void borrarRegistrosdeCargaFichero(ArrayList<AjusteFilaInput> ajustesFilaInput) throws Exception {
		md.borrarRegistrosdeCargaFichero(ajustesFilaInput);
	}

	public void editarNuevosCampos(Aprovisionamiento fichero) throws Exception {
		md.editarNuevosCampos(fichero);
	}

	public void editarReglasSegmento(SegmentoAprovisionamiento segmento) throws Exception {
		try {
			for (ReglaCalidad regla : segmento.getReglas()) {
				md.editarReglaSegmento(segmento.getId_segmento(), regla);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al editar reglas campo: " + ex.getMessage());
			throw ex;
		}
	}

	public void editarCamposAprovisionamiento(Aprovisionamiento aprov) throws Exception {
		md.editarCamposAprovisionamiento(aprov);
		md.editarCamposSegmento(aprov);
	}

	public void editarReglaCalidad(ReglaCalidad regla) throws Exception {
		md.editarReglaCalidad(regla);
	}

	public void replicarReglasCampo(ArrayList<CampoAprovisionamiento> campos) throws Exception {
		md.replicarReglasCampo(campos);
	}

	public void editarReglasCalidad(ArrayList<ReglaCalidad> reglas) throws Exception {
		md.editarReglasCalidad(reglas);
	}

	public void editarCamposSegmento(Aprovisionamiento aprov) throws Exception {
		md.editarCamposSegmento(aprov);
	}

	public void editarCampoSegmento(SegmentoAprovisionamiento segmento) throws Exception {
		md.editarCampoSegmento(segmento);
	}

	public void borrarCampoSegmento(Integer id_segmento) throws Exception {
		md.borrarCampoSegmento(id_segmento);
	}

	public ArrayList<ReglaCalidad> obtenerReglasCalidadPorCampo(CampoAprovisionamiento campo) throws Exception {
		return md.obtenerReglasCalidadPorCampo(campo);
	}

	public ArrayList<SegmentoAprovisionamiento> obtenerSegmentosPorAprov(Integer id_aprov) throws Exception {
		return md.obtenerSegmentosPorAprov(id_aprov);
	}

	public ArrayList<SegmentoAprovisionamiento> obtenerSegmentosPorCampoAprov(Integer id_campoaprov) throws Exception {
		return md.obtenerSegmentosPorCampoAprov(id_campoaprov);
	}

	public void aprobarCargaFichero(CargaFichero cargaFichero) throws Exception {
		md.aprobarCargaFichero(cargaFichero);
	}

	public void rechazarCargaFichero(CargaFichero cargaFichero) throws Exception {
		md.rechazarCargaFichero(cargaFichero);
	}

	public ArrayList<CargaFichero> actualizarEstadoCargaCalidad(ArrayList<CargaFichero> cargasFichero)
			throws Exception {
		return md.actualizarEstadoCargaCalidad(cargasFichero);
	}

	// Configuracion
	// Fecha

	public ArrayList<Fecha> obtenerFechas() throws Exception {
		return md.obtenerFechas();
	}

	public void crearFecha(Fecha fecha) throws Exception {
		md.crearFecha(fecha);
	}

	public void editarFecha(Fecha fecha) throws Exception {
		md.editarFecha(fecha);
	}

	// Métodos de Entidad

	public ArrayList<Entidad> obtenerEntidades() throws Exception {
		return md.obtenerEntidades();
	}

	/*
	 * public Entidad obtenerEntidad(Integer id_entidad) { return
	 * md.obtenerEntidad(id_entidad); }
	 */

	public ArrayList<Cartera> obtenerCarterasPorIdEntidad(Integer id_entidad) throws Exception {
		return md.obtenerCarterasPorIdEntidad(id_entidad);
	}

	public void crearEntidad(Entidad entidad) throws Exception {
		md.crearEntidad(entidad);
	}

	public void editarEntidad(Entidad entidad) throws Exception {
		md.editarEntidad(entidad);
	}

	public void borrarEntidad(Integer id_entidad) throws Exception {
		md.borrarEntidad(id_entidad);
	}

	public ArrayList<Pais> obtenerPaises() throws Exception {
		return md.obtenerPaises();
	}

	public void crearCartera(Cartera cartera) throws Exception {
		md.crearCartera(cartera);
	}

	public ArrayList<Cartera> obtenerCarteras() throws Exception {
		return md.obtenerCarteras();
	}

	public Cartera obtenerCartera(Integer id_cartera) throws Exception {
		return md.obtenerCartera(id_cartera);
	}

	public void borrarCartera(Integer id_cartera) throws Exception {
		md.borrarCartera(id_cartera);
	}

	public void editarCartera(Cartera cartera) throws Exception {
		md.editarCartera(cartera);
	}

	public ArrayList<TipoAmbito> obtenerTiposAmbito() throws Exception {
		return md.obtenerTiposAmbito();
	}

	public ArrayList<TipoMagnitud> obtenerTiposMagnitud(Integer id_tipo_ambito) throws Exception {
		return md.obtenerTiposMagnitud(id_tipo_ambito);
	}

	public ArrayList<TipoCartera> obtenerTiposCartera() throws Exception {
		return md.obtenerTiposCartera();
	}

	// Métodos de Escenario
	public ArrayList<Escenario> obtenerEscenarios() throws Exception {
		return md.obtenerEscenarios();
	}

	/*
	 * public Escenario obtenerEscenario(Integer id_escenario) { return
	 * md.obtenerEscenario(id_escenario); }
	 */

	public void crearEscenario(Escenario escenario) throws Exception {
		md.crearEscenario(escenario);
	}

	public void editarEscenario(Escenario escenario) throws Exception {
		md.editarEscenario(escenario);
	}

	public void borrarEscenario(Integer id_escenario) throws Exception {
		md.borrarEscenario(id_escenario);
	}

	// Parametrización
	public void crearCarteraVersion(CarteraVersion_staging cartera_version) throws Exception {
		md.crearCarteraVersion(cartera_version);
	}

	public void editarCarteraVersion(CarteraVersion_staging cartera_version) throws Exception {
		md.editarCarteraVersion(cartera_version);
	}

	public void replicarCarteraVersion(CarteraVersion_staging carteraVersion) throws Exception {
		md.replicarCarteraVersion(carteraVersion, false);
	}

	public ArrayList<CarteraVersion_staging> obtenerCarterasVersionStaging() throws Exception {
		return md.obtenerCarterasVersionStaging();
	}

	public ArrayList<CarteraVersion_staging> obtenerCarterasVersionStagingPorIdCartera(Integer id_cartera)
			throws Exception {
		return md.obtenerCarterasVersionStagingPorIdCartera(id_cartera);
	}

	public ArrayList<JuegoEscenarios_version> obtenerJuegosConEscenariosPorCarteraVersion(Integer id_cartera_version)
			throws Exception {
		return md.obtenerJuegosConEscenariosPorCarteraVersion(id_cartera_version);
	}

	public void borrarCarteraVersion(Integer id_version_cartera) throws Exception {
		md.borrarCarteraVersion(id_version_cartera);
	}

	// --------------------- Parametrización Variables ----------------------
	// --------------------- Made by Yisus --------------------------------
	public BloqueParametrizado obtenerBloqueBuckets(Integer id_cartera_version) throws Exception {
		return md.obtenerBloqueBuckets(id_cartera_version);
	}

	// --------------------- Parametrización Variables ----------------------
	// --------------------- Made by Yisus --------------------------------
	public ArrayList<BloqueParametrizado> obtenerBloquesParametrizadoPorCarteraVersion(Integer id_cartera_version)
			throws Exception {
		return md.obtenerBloquesParametrizadoPorCarteraVersion(id_cartera_version);
	}

	public BloqueParametrizado editarBloqueParametrizado(BloqueParametrizado bloqueParam) throws Exception {
		return md.editarBloqueParametrizado(bloqueParam);
	}

	public void editarBloquesParametrizados(ArrayList<BloqueParametrizado> bloquesParam) throws Exception {
		md.editarBloquesParametrizados(bloquesParam);
	}

	public ArrayList<TablaInput> obtenerTablasInput() throws Exception {
		return md.obtenerTablasInput();
	}

	public ArrayList<TablaInput> obtenerTablasInputPorIdCartera(Integer id_cartera) throws Exception {
		return md.obtenerTablasInputPorIdCartera(id_cartera);
	}

	public ArrayList<ResultadoVariableMacro> obtenerResultadosVariablesMacrosPorEstructura(Integer id_metodologia)
			throws Exception {
		return md.obtenerResultadosVariablesMacrosPorEstructura(id_metodologia);
	}

	// --------------------- Parametrización Staging ----------------------
	// --------------------- Made by Yisus --------------------------------
	public void editarStagingCarteraVersion(CarteraVersion_staging cartera_version) throws Exception {
		md.editarStagingCarteraVersion(cartera_version);
	}
	// ---------------------- Reglas ----------------------------------

	public void editarExpresionesStaging(ArrayList<Expresion_Staging> expresiones) throws Exception {
		md.editarExpresionesStaging(expresiones);
	}

	public ArrayList<Expresion_Staging> obtenerExpresionesStagingPorCarteraVersion(Integer id_version_Cartera)
			throws Exception {
		return md.obtenerExpresionesStagingPorCarteraVersion(id_version_Cartera);
	}

	// ---------------------- Reglas ----------------------------------

	public void editarReglasStaging(ArrayList<Regla_Staging> reglas) throws Exception {
		md.editarReglasStaging(reglas);
	}

	public ArrayList<Regla_Staging> obtenerReglasStagingPorCarteraVersion(Integer id_version_Cartera) throws Exception {
		return md.obtenerReglasStagingPorCarteraVersion(id_version_Cartera);
	}

	// Stage
	public void crearStage(Stage stage) throws Exception {
		md.crearStage(stage);
	}

	public ArrayList<Stage> obtenerStages() throws Exception {
		return md.obtenerStages();
	}

	public OutputModeloMacro obtenerOutputModeloMacro(OutputModeloMacro output) throws Exception {
		return md.obtenerOutputModeloMacro(output);
	}

	public Stage obtenerStage(Integer id_stage) throws Exception {
		return md.obtenerStage(id_stage);
	}

	public void editarStage(Stage stage) throws Exception {
		md.editarStage(stage);
	}

	public void borrarStage(Integer id_stage) throws Exception {
		md.borrarStage(id_stage);
	}

	// VARIABLE
	public void crearVariable(Variable variable) throws Exception {
		md.crearVariable(variable);
	}

	public Variable obtenerVariable(Integer id_variable) throws Exception {
		return md.obtenerVariable(id_variable);
	}

	public ArrayList<Variable> obtenerVariables() throws Exception {
		return md.obtenerVariables();
	}

	public void editarVariable(Variable variable) throws Exception {
		md.editarVariable(variable);
	}

	public void borrarVariable(Integer id_variable) throws Exception {
		md.borrarVariable(id_variable);
	}

	// Version Escenario
	/*
	 * public void crearEscenarioVersion(Escenario_version escenario_version) throws
	 * Exception { md.crearEscenarioVersion(escenario_version); }
	 */

	public ArrayList<Escenario_version> obtenerEscenariosVersionPorCarteraVersion(Integer id_cartera_version)
			throws Exception {
		return md.obtenerEscenariosVersionPorCarteraVersion(id_cartera_version);
	}

	public void editarEscenarioVersion(Escenario_version escenario_version) throws Exception {
		md.editarEscenarioVersion(escenario_version);
	}

	public void borrarEscenarioVersion(Integer id_escenario_version) throws Exception {
		md.borrarEscenarioVersion(id_escenario_version);
	}

	// BLOQUE
	public void crearBloque(Bloque bloque) throws Exception {
		md.crearBloque(bloque);
	}

	public ArrayList<CampoBloque> obtenerCamposBloquePorIdBloque(Integer id_bloque) throws Exception {
		return md.obtenerCamposBloquePorIdBloque(id_bloque);
	}

	public void borrarBloque(Integer id_bloque) throws Exception {
		md.borrarBloque(id_bloque);
	}

	public void editarBloque(Bloque bloque) throws Exception {
		md.editarBloque(bloque);
	}

	public ArrayList<Bloque> obtenerBloques() throws Exception {
		return md.obtenerBloques();
	}

	public ArrayList<FormatoInput> obtenerFormatosInput() throws Exception {
		return md.obtenerFormatosInput();
	}

	public ArrayList<TipoEjecucion> obtenerTiposEjecucion() throws Exception {
		return md.obtenerTiposEjecucion();
	}

	public ArrayList<TipoBloque> obtenerTiposBloque() throws Exception {
		return md.obtenerTiposBloque();
	}

	public void crearTipoBloque(TipoBloque tipobloque) throws Exception {
		md.crearTipoBloque(tipobloque);
	}

	/*
	 * public void editarTipoBloque(TipoBloque tipobloque) throws Exception {
	 * md.editarTipoBloque(tipobloque); }
	 */

	public void borrarTipoBloque(Integer id_tipobloque) throws Exception {
		md.borrarTipoBloque(id_tipobloque);
	}

	public ArrayList<TipoCampo> obtenerTiposCampo() throws Exception {
		return md.obtenerTiposCampo();
	}

	// METODOLOGÍA
	public ArrayList<EstructuraMetodologica> obtenerMetodologias() throws Exception {
		return md.obtenerMetodologias();
	}

	public EstructuraMetodologica obtenerMetodologia(Integer id_metodologia) throws Exception {
		return md.obtenerMetodologia(id_metodologia);
	}

	public ArrayList<TipoObtencionResultados> obtenerTiposObtencionResultados() throws Exception {
		return md.obtenerTiposObtencionResultados();
	}

	public ArrayList<TipoFlujo> obtenerTiposFlujo() throws Exception {
		return md.obtenerTiposFlujo();
	}

	public ArrayList<TipoPeriodicidad> obtenerTiposPeriodicidad() throws Exception {
		return md.obtenerTiposPeriodicidad();
	}

	public ArrayList<MotorCalculo> obtenerMotoresCalculo() throws Exception {
		return md.obtenerMotoresCalculo();
	}

	public ArrayList<TipoMetodologia> obtenerTiposMetodologia() throws Exception {
		return md.obtenerTiposMetodologia();
	}

	public void crearTipoMetodologia(TipoMetodologia metodologia) throws Exception {
		md.crearTipoMetodologia(metodologia);
	}

	public void editarTipoMetodologia(TipoMetodologia tipometodologia) throws Exception {
		md.editarTipoMetodologia(tipometodologia);
	}

	public void borrarTipoMetodologia(Integer id_tipometodologia) throws Exception {
		md.borrarTipoMetodologia(id_tipometodologia);
	}

	public void crearTipoCartera(TipoCartera tipocartera) throws Exception {
		md.crearTipoCartera(tipocartera);
	}

	public void editarTipoCartera(TipoCartera tipocartera) throws Exception {
		md.editarTipoCartera(tipocartera);
	}

	public void borrarTipoCartera(Integer id_tipocartera) throws Exception {
		md.borrarTipoCartera(id_tipocartera);
	}

	// public ArrayList<Bloque> obtenerBloquesporIdMetodologia(Integer
	// id_metodologia) throws Exception {
	// return md.obtenerBloquesporIdMetodologia(id_metodologia);
	// }

	public ArrayList<Cartera> obtenerCarterasExtrapolacionporIdMetodologia(Integer id_metodologia) throws Exception {
		return md.obtenerCarterasExtrapolacionporIdMetodologia(id_metodologia);
	}

	public void crearMetodologia(EstructuraMetodologica metodologia) throws Exception {
		md.crearMetodologia(metodologia);
	}

	public void editarMetodologia(EstructuraMetodologica metodologia) throws Exception {
		md.editarMetodologia(metodologia);
	}

	public void borrarMetodologia(Integer id_metodologia) throws Exception {
		md.borrarMetodologia(id_metodologia);
	}

	public void replicarEstructuraConCarterasVer(ReplicaEstructuraConCarVer replicaEstructuraConCarVer)
			throws Exception {
		md.replicarEstructuraConCarterasVer(replicaEstructuraConCarVer);
	}

	// Autenticacion
	public Usuario loguearUsuario(Usuario usuario) throws Exception {
		Usuario usuarioRet = null;
		Perfil perfilUsuario = null;
		Long expiracion = null;
		String id_usuario_desenc = null;
		String contrasena_usuario_desenc = null;

		try {
			// Desencriptamos credenciales
			// LOGGER.info("Desencriptando usuario");
			id_usuario_desenc = AES.decrypt(usuario.getId_usuario(), Claves.claveEncLogueo);
			// LOGGER.info("Desencriptando contrasena");
			contrasena_usuario_desenc = AES.decrypt(usuario.getContrasena(), Claves.claveEncLogueo);

			// Objeto que no deberia tener las credenciales de usuario porque retorna al
			// frontend
			usuarioRet = new Usuario();
            SimpleDateFormat dfFileName = new SimpleDateFormat("dd-MM-yyyy");
            Date hoy = new Date(System.currentTimeMillis());
            String fileEdit = null;
            String fileSeg = properties.getProperty("RutaLogs") + "\\Logs_Seg\\" + dfFileName.format(hoy) + ".txt";
            PrintWriter writer = null;
            
			// Comparamos valores hasheados de usuario y contrasena
			if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("adminCre") && contrasena_usuario_desenc.equals("adminCre")) {
				usuarioRet.setNombre_usuario("Developer User");
				usuarioRet.setId_usuario("adminCre");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_DEVELO_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);
            	
				
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("adminInv") && contrasena_usuario_desenc.equals("adminInv")) {
				usuarioRet.setNombre_usuario("Developer User (Inv.)");
				usuarioRet.setId_usuario("adminInv");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_DEVELOINV_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);
            	
				

				/*
				 * ---------- Provisional -> para probar todos los perfiles disponibles en
				 * desarrollo ------------
				 */
				// Creditos
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("adminSisCre") && contrasena_usuario_desenc.equals("adminSisCre")) {
				usuarioRet.setNombre_usuario("Demo Creditos");
				usuarioRet.setId_usuario("adminSisCre");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_ADMSIS_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            	
				
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("adminMetCre") && contrasena_usuario_desenc.equals("adminMetCre")) {
				usuarioRet.setNombre_usuario("Demo Creditos");
				usuarioRet.setId_usuario("adminMetCre");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_ADMMET_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            
				
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("analiMetCre") && contrasena_usuario_desenc.equals("analiMetCre")) {
				usuarioRet.setNombre_usuario("Demo Creditos");
				usuarioRet.setId_usuario("analiMetCre");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_ANAMET_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            	
				
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("analiDatCre") && contrasena_usuario_desenc.equals("analiDatCre")) {
				usuarioRet.setNombre_usuario("Demo Creditos");
				usuarioRet.setId_usuario("analiDatCre");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_ANADAT_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            	
				
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("validaCre") && contrasena_usuario_desenc.equals("validaCre")) {
				usuarioRet.setNombre_usuario("Demo Creditos");
				usuarioRet.setId_usuario("validaCre");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_VALIDA_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            	
				
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("aprobaCre") && contrasena_usuario_desenc.equals("aprobaCre")) {
				usuarioRet.setNombre_usuario("Demo Creditos");
				usuarioRet.setId_usuario("aprobaCre");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_APROBA_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            	
				
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("visitaCre") && contrasena_usuario_desenc.equals("visitaCre")) {
				usuarioRet.setNombre_usuario("Demo Creditos");
				usuarioRet.setId_usuario("visitaCre");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_VISITA_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            	
				
				// Inversiones
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("adminTiInv") && contrasena_usuario_desenc.equals("adminTiInv")) {
				usuarioRet.setNombre_usuario("Demo Inversiones");
				usuarioRet.setId_usuario("adminTiInv");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_IADMTI_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            	
				
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("adminMetInv") && contrasena_usuario_desenc.equals("adminMetInv")) {
				usuarioRet.setNombre_usuario("Demo Inversiones");
				usuarioRet.setId_usuario("adminMetInv");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_IADMME_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

				
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("analiGesInv") && contrasena_usuario_desenc.equals("analiGesInv")) {
				usuarioRet.setNombre_usuario("Demo Inversiones");
				usuarioRet.setId_usuario("analiGesInv");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_IANAGE_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            	

			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("aprobaInv") && contrasena_usuario_desenc.equals("aprobaInv")) {
				usuarioRet.setNombre_usuario("Demo Inversiones");
				usuarioRet.setId_usuario("aprobaInv");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_IAPROB_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

				
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("analiTiInv") && contrasena_usuario_desenc.equals("analiTiInv")) {
				usuarioRet.setNombre_usuario("Demo Inversiones");
				usuarioRet.setId_usuario("analiTiInv");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_IANATI_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            				
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("visitaInv") && contrasena_usuario_desenc.equals("visitaInv")) {
				usuarioRet.setNombre_usuario("Demo Inversiones");
				usuarioRet.setId_usuario("visitaInv");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_IVISIT_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            			
				/*
				 * -----------------------------------------------------------------------------
				 * -
				 */

				/*
				 * ---------- Provisional -> Usuarios admins indep. adicionales para pruebas
				 * simultaneas
				 */
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("adminCuro") && contrasena_usuario_desenc.equals("adminCuro")) {
				usuarioRet.setNombre_usuario("Curro");
				usuarioRet.setId_usuario("adminCuro");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_DEVELO_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            		
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("adminPplu") && contrasena_usuario_desenc.equals("adminPplu")) {
				usuarioRet.setNombre_usuario("PPLU");
				usuarioRet.setId_usuario("adminPplu");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_DEVELO_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            			
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("adminFernan") && contrasena_usuario_desenc.equals("adminFernan")) {
				usuarioRet.setNombre_usuario("Fernan");
				usuarioRet.setId_usuario("adminFernan");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_DEVELO_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

         			
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("adminHrsxC") && contrasena_usuario_desenc.equals("adminHrsxC")) {
				usuarioRet.setNombre_usuario("harisux");
				usuarioRet.setId_usuario("adminHrsxC");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_DEVELO_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            				
			} else if (properties.getProperty("Developer") != null
					&& properties.getProperty("Developer").equalsIgnoreCase("SI")
					&& id_usuario_desenc.equals("adminHrsxI") && contrasena_usuario_desenc.equals("adminHrsxI")) {
				usuarioRet.setNombre_usuario("harisux");
				usuarioRet.setId_usuario("adminHrsxI");
				perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_DEVELOINV_DES");
				usuarioRet.setPerfil_usuario(perfilUsuario);

            		

				/*
				 * -----------------------------------------------------------------------------
				 * -
				 */
			} else if (ldap != null) {
				if (id_usuario_desenc.equals("User.creditos")) {
					usuarioRet.setNombre_usuario("User.creditos");
					usuarioRet.setId_usuario("User.creditos");
					perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_DEVELO_DES");
					usuarioRet.setPerfil_usuario(perfilUsuario);

	            
				} else if (id_usuario_desenc.equals("User.inversiones")) {
					usuarioRet.setNombre_usuario("User.inversiones");
					usuarioRet.setId_usuario("User.inversiones");
					perfilUsuario = md.obtenerPerfilConPermisosPorNombreAD("NIIF_DEVELOINV_DES");
					usuarioRet.setPerfil_usuario(perfilUsuario);

	            			

				} else {
					usuarioRet = ldap.validarUsuario(usuario, id_usuario_desenc, contrasena_usuario_desenc);
					perfilUsuario = md
							.obtenerPerfilConPermisosPorNombreAD(usuarioRet.getPerfil_usuario().getNombre_perfil_ad());
					usuarioRet.setPerfil_usuario(perfilUsuario);

				}

			} else {
				throw new CustomException("LDAP no inicializado, no es posible ingresar. ");
			}

			// Generacion del token
			expiracion = System.currentTimeMillis() + Integer.valueOf(properties.getProperty("LifetimeTokenAcceso"));

			String token = Jwts.builder().setSubject(usuarioRet.getId_usuario()).setIssuer("APP_SIMIFRS9")
					.setIssuedAt(new Date(System.currentTimeMillis())).setExpiration(new Date(expiracion))
					.signWith(SignatureAlgorithm.HS512, Claves.claveToken).compact();

			usuarioRet.setToken(token);
			usuarioRet.setExpiracion(expiracion);

			// Registramos sesion de usuario en DB (en caso no exista)
			md.registrarSesionUsuario(usuarioRet.getId_usuario(), token);

		} catch (Exception ex) {
			throw ex; // Invalid Signing configuration / Couldn't convert Claims.
		}

		return usuarioRet;
	}

	// Administracion sistema
	// Metodos de tarea

	public ArrayList<Tarea> obtenerTareas() throws Exception {
		return md.obtenerTareas();
	}

	public void crearTarea(Tarea tarea) throws Exception {
		md.crearTarea(tarea);
	}

	public void editarTarea(Tarea tarea) throws Exception {
		md.editarTarea(tarea);
	}

	// Perfil y Rol

	public void crearPerfil(Perfil perfil) throws Exception {
		md.crearPerfil(perfil);
	}

	public void editarPerfil(Perfil perfil) throws Exception {
		md.editarPerfil(perfil);
	}

	public void borrarPerfil(Integer id_perfil) throws Exception {
		md.borrarPerfil(id_perfil);
	}

	public ArrayList<Perfil> obtenerPerfiles() throws Exception {
		return md.obtenerPerfiles();
	}

	public ArrayList<Permiso> obtenerPermisos() throws Exception {
		return md.obtenerPermisos();
	}

	public ArrayList<Perfil> obtenerPerfilesPermisos() throws Exception {
		return md.obtenerPerfilesPermisos();
	}

	public ArrayList<Permiso> obtenerPermisosPorIdPerfil(Integer id_perfil) throws Exception {
		return md.obtenerPermisosPorIdPerfil(id_perfil);
	}

	// Correos

	public void crearCorreo(Correo correo) throws Exception {
		md.crearCorreo(correo);
	}

	public void editarCorreo(Correo correo) throws Exception {
		md.editarCorreo(correo);
	}

	public void borrarCorreo(Integer id_correo) throws Exception {
		md.borrarCorreo(id_correo);
	}

	public ArrayList<Correo> obtenerCorreosConNotificaciones(Integer id_tipoAmbito) throws Exception {
		return md.obtenerCorreosConNotificaciones(id_tipoAmbito);
	}

	// Eventos
	public void crearEvento(Evento evento) throws Exception {
		Integer id_evento = md.crearEvento(evento);
		if (properties.getProperty("Modulo_SMTP").equalsIgnoreCase("SI")) {
			enviarNotificacion(evento.getTipo_accion().getId_tipo_accion(), evento.getTipo_objeto().getId_tipo_objeto(),
					evento.getNombre_objeto(), evento.getPerfil_usuario().getTipo_ambito().getId_tipo_ambito(),
					md.obtenerListaDestinatariosDeEvento(evento));
		} else {
			LOGGER.warn("El modulo de correos se encuentra desactivado, no se ha enviado la notificacion");
		}

		// ------- Registro eventos en archivo de logs -------- //
		// Obtenemos hostname de cliente (dado ip local)

		String hostname_local = (InetAddress.getByName(evento.getLocal_ip())).getHostName();

		// Creamos carpetas en caso no existan
		SimpleDateFormat dfFileName = new SimpleDateFormat("dd-MM-yyyy");
		Date hoy = new Date(System.currentTimeMillis());
		String fileAudit = properties.getProperty("RutaLogs") + "\\Logs_Audit\\" + dfFileName.format(hoy) + ".txt";
		String fileSeg = properties.getProperty("RutaLogs") + "\\Logs_Seg\\" + dfFileName.format(hoy) + ".txt";
		String fileEdit = null;
		String fileEdit2 = null;

		new File(properties.getProperty("RutaLogs") + "\\Logs_Audit").mkdirs();
		new File(properties.getProperty("RutaLogs") + "\\Logs_Seg").mkdirs();
		// new File(fileAudit).createNewFile();
		// new File(fileSeg).createNewFile();

		PrintWriter writer = null;
		PrintWriter writer2 = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		try {

			// En caso el objeto sea de perfil
			if (evento.getTipo_objeto().getNombre_tipo_objeto().equals("perfil")) { //Se usa ahora para el log de cierre de sesion
				fileEdit = fileSeg;
				new File(fileEdit).createNewFile(); // Creamos archivo en caso no exista
				writer = new PrintWriter(new FileOutputStream(fileEdit, true));
				writer.println("---------------TRANSACCION---------------");
				writer.println("***Estado***:                 Exitoso");
				writer.println("***ID del evento***:          " + id_evento);
				writer.println("***Nombre del evento***:      Cierre sesion");
				writer.println("***ID de usuario***:          " + evento.getId_usuario());
				writer.println("***Fecha***:                  " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
				writer.println("***Acción realizada***:       Cierre sesion");
				writer.println("-----------------------------------------");
				writer.close();
				
			} else {
				fileEdit = fileAudit;
				new File(fileEdit).createNewFile(); // Creamos archivo en caso no exista
				writer = new PrintWriter(new FileOutputStream(fileEdit, true));
				writer.println("***ID del evento***:         " + id_evento);
				writer.println("***Fecha de evento***:       " + dateFormat.format(evento.getFecha_evento()));
				writer.println("***Tipo de objeto***:        " + evento.getTipo_objeto().getNombre_tipo_objeto());
				writer.println("***Tipo de accion***:        " + evento.getTipo_accion().getNombre_tipo_accion());
				writer.println("***Nombre del objeto***:     " + evento.getNombre_objeto());
				writer.println("***Contenido***:             " + evento.getObjeto_json());
				writer.println("***Contenido actualizado***: " + evento.getActualizado_json());
				writer.println("***Contenido agregado***:    " + evento.getAgregado_json());
				writer.println("***Contenido borrado***:     " + evento.getBorrado_json());
				writer.println("***ID de usuario***:         " + evento.getId_usuario());
				writer.println("***Nombre del usuario***:    " + evento.getNombre_usuario());
				writer.println("***Perfil del usuario***:    " + evento.getPerfil_usuario().getNombre_perfil());
				writer.println("***Perfil AD***:             " + evento.getPerfil_usuario().getNombre_perfil_ad());
				writer.println("***IP Local***:    " + evento.getLocal_ip());
				writer.println("***Hostname Local***:    " + hostname_local);
				writer.println("--------------------------------------------------------------------------------");
				
				fileEdit2 = fileSeg;
				writer2 = new PrintWriter(new FileOutputStream(fileEdit2, true));
				writer2.println("---------------TRANSACCION---------------");
				writer2.println("***Estado***:                 Exitoso");
				writer2.println("***ID del evento***:          " + id_evento);
				writer2.println("***Nombre del evento***:      " + evento.getTipo_accion().getNombre_tipo_accion());
				writer2.println("***ID de usuario***:          " + evento.getId_usuario());
				writer2.println("***Fecha***:                  " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
				writer2.println("***Acción realizada***:       " + evento.getTipo_accion().getNombre_tipo_accion() + " -> " + evento.getTipo_objeto().getNombre_tipo_objeto() );
				writer2.println("-----------------------------------------");
				writer2.close();
			}


		} catch (Exception ex) {
			LOGGER.error("Error al editar archivo de logs " + fileEdit + " : " + ex);
			throw ex;
		} finally {
			if (writer != null)
				writer.close();
			LOGGER.info("Termina escritura sobre archivo logs " + fileEdit);
		}

	}

	public ArrayList<Evento> obtenerEventos(Integer id_ambito) throws Exception {
		return md.obtenerEventos(id_ambito);
	}

	public EventoDetalle obtenerEventoDetalle(Integer id_evento) throws Exception {
		return md.obtenerEventoDetalle(id_evento);
	}

	public void borrarTarea(Integer id_tarea) throws Exception {
		md.borrarTarea(id_tarea);
	}

	// Alertas
	public ArrayList<Alerta> obtenerAlertas() throws Exception {
		return md.obtenerAlertas();
	}

	// Progreso
	public ArrayList<EntidadProgreso> obtenerEntidadesProgreso(FiltroProgreso filtroProgreso) throws Exception {
		return md.obtenerEntidadesProgreso(filtroProgreso);
	}

	// pruebaSMTP
	public void pruebaSMTP() throws Exception {
		try {
			correo.pruebaSMTP();
		} catch (Exception ex) {
			LOGGER.error("Error al ejecutar pruebaSMTP: " + ex.getMessage());
		}
	}

	// Ejecucion
	public void ejecutarSAS(ArrayList<Escenario_version> ejecuciones) throws Exception {
		/* sas.ejecutarSAS(ejecuciones); */
	}

	public ArrayList<EstructuraMetodologica> obtenerEstructurasPorIdCartera(Integer id_cartera) throws Exception {
		return md.obtenerEstructurasPorIdCartera(id_cartera);
	}

	public ArrayList<EstructuraMetodologica> obtenerEstructurasCalcPePorIdCartera(Integer id_cartera) throws Exception {
		return md.obtenerEstructurasCalcPePorIdCartera(id_cartera);
	}

	public ArrayList<Cartera_version> obtenerCarterasVersionPorIdMetodologia(Integer id_metodologia) throws Exception {
		return md.obtenerCarterasVersionPorIdMetodologia(id_metodologia);
	}

	public JuegoEscenarios_version obtenerJuegoVigentePorCartera(Integer id_cartera) throws Exception {
		return md.obtenerJuegoVigentePorCartera(id_cartera);
	}

	public JuegoEscenarios_version obtenerJuegoVigentePorCarteraFase2(Integer id_cartera) throws Exception {
		return md.obtenerJuegoVigentePorCarteraFase2(id_cartera);
	}

	public EjecucionAgregadaPE obtenerEjecucionFase1OficialPorCarteraCodmes(Integer id_cartera, Integer codmes)
			throws Exception {
		return md.obtenerEjecucionFase1OficialPorCarteraCodmes(id_cartera, codmes);
	}

	public ArrayList<EjecucionPE> obtenerEjecucionesBaseFase1OficialPorCartera(Integer id_cartera) throws Exception {
		return md.obtenerEjecucionesBaseFase1OficialPorCartera(id_cartera);
	}

	public ArrayList<EjecucionPE> obtenerEjecucionesBaseOficialPorCarteraCodmesFase(Integer id_cartera, Integer codmes,
			Integer fase) throws Exception {
		return md.obtenerEjecucionesBaseOficialPorCarteraCodmesFase(id_cartera, codmes, fase);
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoVariacPorIdEjecVar(Integer id_ejecucion) throws Exception {
		return md.obtenerTablasMapeoVariacPorIdEjecVar(id_ejecucion);
	}

	public Integer obtenerCodmesActual_fase1(Integer id_tipo_ambito) throws Exception {
		return md.obtenerCodmesActual_fase1(id_tipo_ambito);
	}

	public Integer obtenerCodmesActual_fase2(Integer id_tipo_ambito) throws Exception {
		return md.obtenerCodmesActual_fase2(id_tipo_ambito);
	}

	public Integer obtenerCodmesActualPorFase(Integer fase, Integer id_tipo_ambito) throws Exception {
		return md.obtenerCodmesActualPorFase(fase, id_tipo_ambito);
	}

	public ArrayList<JuegoEscenarios_version> obtenerJuegosEscenariosVersionPorIdCarteraVersion(
			Integer id_cartera_version) throws Exception {
		return md.obtenerJuegosEscenariosVersionPorIdCarteraVersion(id_cartera_version);
	}

	public ArrayList<EjecucionPrimerPaso> obtenerEjecucionesModMacPorMetodologia(Integer id_metodologia)
			throws Exception {
		return md.obtenerEjecucionesModMacPorMetodologia(id_metodologia);
	}

	public ArrayList<EjecucionPrimerPaso> obtenerEjecucionesRepricingPorMetodologia(Integer id_metodologia)
			throws Exception {
		return md.obtenerEjecucionesRepricingPorMetodologia(id_metodologia);
	}

	public ArrayList<Escenario_version> obtenerEscenariosVersionPorIdJuegoEscenarios(Integer id_juego_escenarios)
			throws Exception {
		return md.obtenerEscenariosVersionPorIdJuegoEscenarios(id_juego_escenarios);
	}

	/*
	 * public void crearEjecucionPE_fase1(EjecucionPE ejecucionPE) throws Exception
	 * { md.crearEjecucionPE_fase1(ejecucionPE); }
	 * 
	 * public void replicarEjecucionesPE_fase1(ReplicaEjecucionesPE
	 * replicaEjecucionesPE) throws Exception {
	 * md.replicarEjecucionesPE_fase1(replicaEjecucionesPE); }
	 * 
	 * public void editarEjecucionPE_fase1(EjecucionPE ejecucionPE) throws Exception
	 * { md.editarEjecucionPE_fase1(ejecucionPE); }
	 * 
	 * public void borrarEjecucionPE_fase1(Integer id_ejecucion) throws Exception {
	 * md.borrarEjecucionPE_fase1(id_ejecucion); }
	 * 
	 * public void crearEjecucionPE_fase2(EjecucionPE ejecucionPE) throws Exception
	 * { md.crearEjecucionPE_fase2(ejecucionPE); }
	 * 
	 * public void replicarEjecucionesPE_fase2(ReplicaEjecucionesPE
	 * replicaEjecucionesPE) throws Exception {
	 * md.replicarEjecucionesPE_fase2(replicaEjecucionesPE); }
	 * 
	 * public void editarEjecucionPE_fase2(EjecucionPE ejecucionPE) throws Exception
	 * { md.editarEjecucionPE_fase2(ejecucionPE); }
	 * 
	 * public void borrarEjecucionPE_fase2(Integer id_ejecucion) throws Exception {
	 * md.borrarEjecucionPE_fase2(id_ejecucion); }
	 */

	public ArrayList<EjecucionVariacion> obtenerEjecucionesVariacion() throws Exception {
		return md.obtenerEjecucionesVariacion();
	}

	public void crearEjecucionVariacion(EjecucionVariacion ejecucionVariacion) throws Exception {
		md.crearEjecucionVariacion(ejecucionVariacion);
	}

	public void editarEjecucionVariacion(EjecucionVariacion ejecucionVariacion) throws Exception {
		md.editarEjecucionVariacion(ejecucionVariacion);
	}

	public void borrarEjecucionVariacion(Integer id_ejecucion) throws Exception {
		md.borrarEjecucionVariacion(id_ejecucion);
	}

	public void ejecutarCalculo_fase1(EjecucionAgregadaPE ejecucionPE) throws Exception {
		EjecucionMotor ejecucionMotor = null;
		EstructuraMetodologica metodologiaEjecucion = null;
		String log_calc = null;
		String log_err = null;
		String ruta_version = null;
		Boolean ejecSinBloques = false;
		String logLines = "";
		try {
			if (ejecucionPE.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Creditos")) {

				ejecucionMotor = md.convertirEjecucionPEAMotor(ejecucionPE, 1, false, false, false, null);

				metodologiaEjecucion = ejecucionMotor.getJuego_escenarios_version().getCartera_version()
						.getMetodologia();

				// actualizamos el estado a "en ejecucion"

				md.editarEstadoEjecucion(ejecucionMotor, 1);

				if (metodologiaEjecucion.getFlujo().getId_tipo_flujo().equals(1)
						&& metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(3)) {
					LOGGER.info("Iniciando ejecucion PE de carga directa para fase 1");

					md.ejecutarCalculoDirecto(ejecucionMotor);

					LOGGER.info("Finaliza ejecucion PE de carga directa para fase 1");
				} else if (metodologiaEjecucion.getFlujo().getId_tipo_flujo().equals(1)
						&& metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(2)) {
					ejecucionMotor.setEjecuciones_extrapolacion(md.obtenerEjecucionesOficialesExtrapolacion(
							metodologiaEjecucion.getCarteras_extrapolacion(), ejecucionMotor.getCodmes()));
					LOGGER.info("Iniciando ejecucion PE de extrapolacion para fase 1 en SAS ");
					if (sas != null) {
						sas.ejecutarCalculo(ejecucionMotor, false, false);
					} else {
						sas2.ejecutarCalculo(ejecucionMotor, false, false);
					}
					LOGGER.info("Finaliza ejecucion PE de extrapolacion para fase 1 en SAS ");
				} else {
					LOGGER.info("Iniciando ejecucion PE para fase 1 en SAS ");
					if (sas != null) {
						sas.ejecutarCalculo(ejecucionMotor, false, false);
					} else {
						sas2.ejecutarCalculo(ejecucionMotor, false, false);
					}
					LOGGER.info("Finaliza ejecucion PE para fase 1 en SAS ");
				}
				LOGGER.info("Finaliza ejecucion PE de fase 1 en SAS ");
			} else if (ejecucionPE.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Inversiones")) {

				LOGGER.info("Inicio  convertirEjecucionPEAMotor");
				ejecucionMotor = md.convertirEjecucionPEAMotor(ejecucionPE, 1, false, false, false, null);

				LOGGER.info("Fin  convertirEjecucionPEAMotor");
				metodologiaEjecucion = ejecucionMotor.getJuego_escenarios_version().getCartera_version()
						.getMetodologia();
				LOGGER.info("Inicio editarEstadoEjecucion");
				// actualizamos el estado a "en ejecucion"
				md.editarEstadoEjecucion(ejecucionMotor, 1);
				LOGGER.info("Fin editarEstadoEjecucion");
				if (metodologiaEjecucion.getFlujo().getId_tipo_flujo().equals(4)
						&& metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(3)) {
					LOGGER.info("Iniciando ejecucion PE de carga directa para fase 1");
					md.ejecutarCalculoDirecto(ejecucionMotor);
					LOGGER.info("Finaliza ejecucion PE de carga directa para fase 1");
				} /*
					 * else if (metodologiaEjecucion.getFlujo().getId_tipo_flujo() == 1 &&
					 * metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados()
					 * == 2) { ejecucionMotor.setEjecuciones_extrapolacion(md.
					 * obtenerEjecucionesOficialesExtrapolacion(
					 * metodologiaEjecucion.getCarteras_extrapolacion(),
					 * ejecucionMotor.getCodmes())); }
					 */
				else {
					LOGGER.info("Iniciando ejecucion PE para fase 1 en MATLAB ");
					// Unicamente si existen bloques a nivel cartera version
					if (ejecucionMotor.getBloquesParam_carteraVersion().size() != 0) {
						matlab.ejecutarCalculoInversiones(ejecucionMotor);
					} else {
						ejecSinBloques = true;
					}
					LOGGER.info("Finaliza ejecucion PE para fase 1 en MATLAB ");
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Error al ejecutar el calculo PE para fase 1:" + ex.getMessage());
			throw ex;
		} finally {
			// actualizamos el estado a ejecutado
			if (ejecSinBloques) {
				// actualizamos el estado a 'ejecutado sin bloques'
				LOGGER.info("editarEstadoEjecucion en finally ");
				md.editarEstadoEjecucion(ejecucionMotor, 4);
			} else {
				LOGGER.info("editarEstadoEjecucion en finally ");
				md.editarEstadoEjecucion(ejecucionMotor, 2);
			}

			// Sin avances en codigos de inversiones
			if (ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Inversiones")) {
				md.editarAvanceEjecucion(ejecucionMotor, 100);
			}

			// Logs para inversiones
			if (ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Inversiones")
					&& !(metodologiaEjecucion.getFlujo().getId_tipo_flujo().equals(4)
							&& metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(3))
					&& ejecucionMotor.getBloquesParam_carteraVersion().size() != 0) {
				// Guardamos logs en la BD
				ruta_version = ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia()
						.getCodigo_estructura()
						+ "\\\\" + ejecucionMotor.getCodmes() % 200000 + "\\\\"
						+ ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia()
								.getCartera().getNombre_cartera()
						+ "\\\\"
						+ ejecucionMotor.getJuego_escenarios_version().getCartera_version().getNombre_cartera_version()
						+ "\\\\" + ejecucionMotor.getJuego_escenarios_version().getNombre_juego_escenarios();

				log_calc = new String(Files.readAllBytes(Paths.get(ruta_version + "\\\\log_calc.txt")));
				log_err = new String(Files.readAllBytes(Paths.get(ruta_version + "\\\\log_err.txt")));

				md.guardarLogEjecucion(ejecucionMotor, log_calc + "\n\r" + log_err);
			}
		}
	}

	public void ejecutarSimulacion(EjecucionAgregadaPE ejecucionPE) throws Exception {
		EjecucionMotor ejecucionMotor = null;
		EstructuraMetodologica metodologiaEjecucion = null;
		String log_calc = null;
		String log_err = null;
		String ruta_version = null;
		Boolean ejecSinBloques = false;
		try {
			if (ejecucionPE.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Creditos")) {

				ejecucionMotor = md.convertirEjecucionPEAMotor(ejecucionPE,
						(ejecucionPE.getFase() == null ? 1 : ejecucionPE.getFase()), false, false, false, null);
				metodologiaEjecucion = ejecucionMotor.getJuego_escenarios_version().getCartera_version()
						.getMetodologia();
				// actualizamos el estado a "en ejecucion"
				md.editarEstadoEjecucion(ejecucionMotor, 1);
				md.editarAvanceEjecucion(ejecucionMotor, 0);

				if (metodologiaEjecucion.getFlujo().getId_tipo_flujo().equals(1)
						&& metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(3)) {
					LOGGER.info("Iniciando simulacion PE de carga directa");
					md.ejecutarCalculoDirecto(ejecucionMotor);
					LOGGER.info("Finaliza simulacion PE de carga directa");
				} else if (metodologiaEjecucion.getFlujo().getId_tipo_flujo().equals(1)
						&& metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(2)) {
					ejecucionMotor.setEjecuciones_extrapolacion(md.obtenerEjecucionesOficialesExtrapolacion(
							metodologiaEjecucion.getCarteras_extrapolacion(), ejecucionMotor.getCodmes()));
					LOGGER.info("Iniciando simulacion PE de extrapolacion en SAS ");
					if (sas != null) {
						sas.ejecutarCalculo(ejecucionMotor, false, false);
					} else {
						sas2.ejecutarCalculo(ejecucionMotor, false, false);
					}
					LOGGER.info("Finaliza simulacion PE de extrapolacion en SAS ");
				} else {
					LOGGER.info("Iniciando simulacion PE en SAS ");
					if (sas != null) {
						sas.ejecutarCalculo(ejecucionMotor, false, false);
					} else {
						sas2.ejecutarCalculo(ejecucionMotor, false, false);
					}
					LOGGER.info("Finaliza simulacion PE en SAS ");
				}

			} else if (ejecucionPE.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Inversiones")) {

				ejecucionMotor = md.convertirEjecucionPEAMotor(ejecucionPE,
						(ejecucionPE.getFase() == null ? 1 : ejecucionPE.getFase()), false, false, false, null);
				metodologiaEjecucion = ejecucionMotor.getJuego_escenarios_version().getCartera_version()
						.getMetodologia();

				// actualizamos el estado a "en ejecucion"
				md.editarEstadoEjecucion(ejecucionMotor, 1);
				md.editarAvanceEjecucion(ejecucionMotor, 0);

				if (metodologiaEjecucion.getFlujo().getId_tipo_flujo().equals(4)
						&& metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(3)) {
					LOGGER.info("Iniciando simulacion PE de carga directa");
					md.ejecutarCalculoDirecto(ejecucionMotor);
					LOGGER.info("Finaliza simulacion PE de carga directa");
					/*
					 * else if (metodologiaEjecucion.getFlujo().getId_tipo_flujo() == 1 &&
					 * metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados()
					 * == 2) { ejecucionMotor.setEjecuciones_extrapolacion(md.
					 * obtenerEjecucionesOficialesExtrapolacion(
					 * metodologiaEjecucion.getCarteras_extrapolacion(),
					 * ejecucionMotor.getCodmes())); sas.ejecutarCalculo(ejecucionMotor); }
					 */
				} else {
					LOGGER.info("Iniciando simulacion PE en MATLAB ");
					// Unicamente si existen bloques a nivel cartera version
					if (ejecucionMotor.getBloquesParam_carteraVersion().size() != 0) {
						matlab.ejecutarCalculoInversiones(ejecucionMotor);
					} else {
						ejecSinBloques = true;
					}
					LOGGER.info("Finaliza simulacion PE en MATLAB ");
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Error en la simulacion del calculo PE:" + ex.getMessage());
			throw ex;
		} finally {
			if (ejecSinBloques) {
				// actualizamos el estado a 'ejecutado sin bloques'
				md.editarEstadoEjecucion(ejecucionMotor, 4);
			} else {
				if (ejecucionMotor.getGrupos_bloquesParamEscenario().size() == md.obtenerEscenarios().size()) {
					// actualizamos el estado a ejecutado
					md.editarEstadoEjecucion(ejecucionMotor, 2);
				} else {
					// actualizamos el estado a ejecutado parcialmente (solo algunos escenarios)
					md.editarEstadoEjecucion(ejecucionMotor, 3);
				}
			}

			// Sin avances en codigos de inversiones
			if (ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Inversiones")) {
				md.editarAvanceEjecucion(ejecucionMotor, 100);
			}

			// Logs para inversiones (si no es de carga directa)
			if (ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Inversiones")
					&& !(metodologiaEjecucion.getFlujo().getId_tipo_flujo().equals(4)
							&& metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(3))
					&& ejecucionMotor.getBloquesParam_carteraVersion().size() != 0) {
				// Guardamos logs en la BD
				ruta_version = ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia()
						.getCodigo_estructura()
						+ "\\\\" + ejecucionMotor.getCodmes() % 200000 + "\\\\"
						+ ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia()
								.getCartera().getNombre_cartera()
						+ "\\\\"
						+ ejecucionMotor.getJuego_escenarios_version().getCartera_version().getNombre_cartera_version()
						+ "\\\\" + ejecucionMotor.getJuego_escenarios_version().getNombre_juego_escenarios();

				log_calc = new String(Files.readAllBytes(Paths.get(ruta_version + "\\\\log_calc.txt")));
				log_err = new String(Files.readAllBytes(Paths.get(ruta_version + "\\\\log_err.txt")));

				md.guardarLogEjecucion(ejecucionMotor, log_calc + "\n\r" + log_err);
			}
		}
	}

	public void ejecutarCalculo_fase2(EjecucionAgregadaPE ejecucionPE) throws Exception {
		EjecucionMotor ejecucionMotor = null;
		EstructuraMetodologica metodologiaEjecucion = null;
		String log_calc = null;
		String log_err = null;
		String ruta_version = null;
		Boolean ejecSinBloques = false;
		try {
			if (ejecucionPE.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Creditos")) {
				LOGGER.info("Inicio convertirEjecucionPEAMotor en fase 2");
				ejecucionMotor = md.convertirEjecucionPEAMotor(ejecucionPE, 2, false, false, false, null);
				metodologiaEjecucion = ejecucionMotor.getJuego_escenarios_version().getCartera_version()
						.getMetodologia();

				LOGGER.info(" actualizamos el estado a \"en ejecucion");
				// actualizamos el estado a "en ejecucion"
				md.editarEstadoEjecucion(ejecucionMotor, 1);
				md.editarAvanceEjecucion(ejecucionMotor, 0);

				if (metodologiaEjecucion.getFlujo().getId_tipo_flujo().equals(1)
						&& metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(3)) {
					LOGGER.info("Iniciando ejecucion PE de carga directa para fase 2");
					md.ejecutarCalculoDirecto(ejecucionMotor);
					LOGGER.info("Finaliza ejecucion PE de carga directa para fase 2");
				} else if (metodologiaEjecucion.getFlujo().getId_tipo_flujo().equals(1)
						&& metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(2)) {
					ejecucionMotor.setEjecuciones_extrapolacion(md.obtenerEjecucionesOficialesExtrapolacion(
							metodologiaEjecucion.getCarteras_extrapolacion(), ejecucionMotor.getCodmes()));
					LOGGER.info("Iniciando ejecucion PE de extrapolacion para fase 2 en SAS ");
					if (sas != null) {
						sas.ejecutarCalculo(ejecucionMotor, false, false);
					} else {
						sas2.ejecutarCalculo(ejecucionMotor, false, false);
					}
					LOGGER.info("Finaliza ejecucion PE de extrapolacion para fase 2 en SAS ");
				} else {
					LOGGER.info("Iniciando ejecucion PE para fase 2 en SAS ");
					if (sas != null) {
						sas.ejecutarCalculo(ejecucionMotor, false, false);
					} else {
						sas2.ejecutarCalculo(ejecucionMotor, false, false);
					}
					LOGGER.info("Finaliza ejecucion PE para fase 2 en SAS ");
				}

			} else if (ejecucionPE.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Inversiones")) {
				LOGGER.info("Inicio convertirEjecucionPEAMotor ");
				ejecucionMotor = md.convertirEjecucionPEAMotor(ejecucionPE, 2, false, false, false, null);
				LOGGER.info("Inicio convertirEjecucionPEAMotor ");
				metodologiaEjecucion = ejecucionMotor.getJuego_escenarios_version().getCartera_version()
						.getMetodologia();

				// actualizamos el estado a "en ejecucion"
				md.editarEstadoEjecucion(ejecucionMotor, 1);

				if (metodologiaEjecucion.getFlujo().getId_tipo_flujo().equals(4)
						&& metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(3)) {
					LOGGER.info("Iniciando ejecucion PE de carga directa para fase 2");
					md.ejecutarCalculoDirecto(ejecucionMotor);
					LOGGER.info("Finaliza ejecucion PE de carga directa para fase 2");
					/*
					 * else if (metodologiaEjecucion.getFlujo().getId_tipo_flujo() == 1 &&
					 * metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados()
					 * == 2) { ejecucionMotor.setEjecuciones_extrapolacion(md.
					 * obtenerEjecucionesOficialesExtrapolacion(
					 * metodologiaEjecucion.getCarteras_extrapolacion(),
					 * ejecucionMotor.getCodmes())); sas.ejecutarCalculo(ejecucionMotor); }
					 */
				} else {
					LOGGER.info("Iniciando ejecucion PE de fase 2 en MATLAB ");
					// Unicamente si existen bloques a nivel cartera version
					if (ejecucionMotor.getBloquesParam_carteraVersion().size() != 0) {
						matlab.ejecutarCalculoInversiones(ejecucionMotor);
					} else {
						ejecSinBloques = true;
					}
					LOGGER.info("Finaliza ejecucion PE de fase 2 en MATLAB ");
				}

				LOGGER.info("Finaliza ejecucion PE de fase 2 en MATLAB ");
			}
		} catch (Exception ex) {
			LOGGER.error("Error al ejecutar el calculo PE de fase 2:" + ex.getMessage());
			throw ex;
		} finally {

			if (ejecSinBloques) {
				// actualizamos el estado a 'ejecutado sin bloques'
				md.editarEstadoEjecucion(ejecucionMotor, 4);
			} else {
				md.editarEstadoEjecucion(ejecucionMotor, 2);
			}

			// Sin avances en codigos de inversiones
			if (ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Inversiones")) {
				md.editarAvanceEjecucion(ejecucionMotor, 100);
			}

			// Logs para inversiones
			if (ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
					.getTipo_ambito().getNombre_tipo_ambito().equalsIgnoreCase("Inversiones")
					&& !(metodologiaEjecucion.getFlujo().getId_tipo_flujo().equals(4)
							&& metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados().equals(3))
					&& ejecucionMotor.getBloquesParam_carteraVersion().size() != 0) {
				// Guardamos logs en la BD
				ruta_version = ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia()
						.getCodigo_estructura()
						+ "\\\\" + ejecucionMotor.getCodmes() % 200000 + "\\\\"
						+ ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia()
								.getCartera().getNombre_cartera()
						+ "\\\\"
						+ ejecucionMotor.getJuego_escenarios_version().getCartera_version().getNombre_cartera_version()
						+ "\\\\" + ejecucionMotor.getJuego_escenarios_version().getNombre_juego_escenarios();

				log_calc = new String(Files.readAllBytes(Paths.get(ruta_version + "\\\\log_calc.txt")));
				log_err = new String(Files.readAllBytes(Paths.get(ruta_version + "\\\\log_err.txt")));

				md.guardarLogEjecucion(ejecucionMotor, log_calc + "\n\r" + log_err);
			}
		}
	}

	public void ejecutarVariacion(EjecucionVariacion ejecucionVariacion) throws Exception {
		try {
			LOGGER.info("Iniciando ejecucion de variacion en SAS");

			// Borramos resultados por si existen
			md.borrarResultadosVariacion(ejecucionVariacion.getId_ejecucion());

			// actualizamos el estado a "en ejecucion"
			md.editarEstadoEjecucion(ejecucionVariacion, 1);

			EjecucionAgregadaPE ejecucion = md.obtenerEjecucionesAgregadaPEPorFaseCodmesJuego(
					ejecucionVariacion.getEjecucion().getFase(), ejecucionVariacion.getEjecucion().getCodmes(),
					ejecucionVariacion.getEjecucion().getEscenario_version().getJuego_escenarios_version()
							.getId_juego_escenarios());

			EjecucionAgregadaPE ejecucionModMacro = md.obtenerEjecucionesAgregadaPEPorFaseCodmesJuego(
					ejecucionVariacion.getEjecucionAnt_modMac().getFase(),
					ejecucionVariacion.getEjecucionAnt_modMac().getCodmes(), ejecucionVariacion.getEjecucionAnt_modMac()
							.getEscenario_version().getJuego_escenarios_version().getId_juego_escenarios());

			EjecucionMotor ejecucionMotor = md.convertirEjecucionPEAMotor(ejecucion,
					ejecucionVariacion.getEjecucion().getFase(), false, true, true,
					ejecucionVariacion.getId_ejecucion());
			EjecucionMotor ejecucionMotorModMacro = md.convertirEjecucionPEAMotor(ejecucionModMacro,
					ejecucionVariacion.getEjecucion().getFase(), false, true, false, null);

			Integer metodo_ejec = ejecucionMotor.getJuego_escenarios_version().getCartera_version().getMetodologia()
					.getMetodo_resultados().getId_tipo_obtencion_resultados();
			Integer metodo_ejec_mm = ejecucionMotorModMacro.getJuego_escenarios_version().getCartera_version()
					.getMetodologia().getMetodo_resultados().getId_tipo_obtencion_resultados();

			// Fijamos tipo ejecucion a variaciones
			/*
			 * TipoEjecucion tipoEjecVar = null; for (TipoEjecucion te :
			 * md.obtenerTiposEjecucion()) { if (te.getId_tipo_ejecucion().equals(3)) {
			 * tipoEjecVar = te; } } ejecucionMotor.setTipo_ejecucion(tipoEjecVar);
			 */

			// Indicamos codmes y id_ejecucion (solo base) de la ejecucion oficial anterior
			// (la q tiene asociada el mod. macro)
			ejecucionMotor.setCodmesAnt_variacion(ejecucionMotorModMacro.getCodmes());

			/*
			 * for (GrupoBloquesEscenario gbemm :
			 * ejecucionMotorModMacro.getGrupos_bloquesParamEscenario()) { if
			 * (gbemm.getEscenario_version().getEscenario().getFlag_obligatorio()) { // Solo
			 * base ejecucionMotor.setId_ejecucionAnt_variacion(gbemm.getId_ejecucion()); }
			 * }
			 */

			// Eliminamos los escenarios que no son base
			// int idx = 0;
			/*
			 * Iterator<GrupoBloquesEscenario> iter =
			 * ejecucionMotor.getGrupos_bloquesParamEscenario().iterator(); while
			 * (iter.hasNext()) { GrupoBloquesEscenario gbe = iter.next(); if
			 * (!gbe.getEscenario_version().getEscenario().getFlag_obligatorio()) { // Si no
			 * es base eliminamos iter.remove(); } }
			 */

			// GrupoBloquesEscenario gbe_pds_dif =
			// ejecucionMotor.getGrupos_bloquesParamEscenario().get(0);

			GrupoBloquesEscenario gbemm = null;
			for (GrupoBloquesEscenario gbeact_f : ejecucionMotor.getGrupos_bloquesParamEscenario()) {
//				if (gbeact_f.getEscenario_version().getEscenario().getFlag_obligatorio()) { // Solo base
//					gbe_pds_dif = gbeact_f;
//				}

				// Modelo Macro solo si es Modelo Interno ambos
				if (ejecucionVariacion.getEjecucion().getFase().equals(1)) {
					if (metodo_ejec.equals(1) && metodo_ejec_mm.equals(1) && ejecucionMotor.getTipo_ajusteManual() != -1
							&& ejecucionMotorModMacro.getTipo_ajusteManual() != -1) { // Solo en fase 1
						LOGGER.info("aplica pd macro variaciones");
						// Hallamos el grupo de escenarios bloque para el escenario base (de la ejec. de
						// modelo macro a asociar)
						// GrupoBloquesEscenario gbemm = null;
						for (GrupoBloquesEscenario gbemm_f : ejecucionMotorModMacro.getGrupos_bloquesParamEscenario()) {
							if (gbemm_f.getEscenario_version().getEscenario().getId_escenario()
									.equals(gbeact_f.getEscenario_version().getEscenario().getId_escenario())) {
								gbemm = gbemm_f;
							}
						}

						// Calculamos las diferencias de pd's finales para cada fecha (entre las
						// proyecciones de la ejec. asociada
						// contra la ejec. a asociar el modelo macro) -> Se entiende como actual -
						// anterior
						for (AjustePdProyec ajs_dif : gbeact_f.getAjustesPdProy()) {

							for (AjustePdProyec ajs_mm : gbemm.getAjustesPdProy()) {
								if (ajs_dif.getFecha_proyeccion().equals(ajs_mm.getFecha_proyeccion())) {
									ajs_dif.setPd_final(ajs_dif.getPd_final() - ajs_mm.getPd_final());
								}
							}
						}

					} else {
						gbeact_f.setAjustesPdProy(null);
					}
				} else {
					ejecucionMotor.setTipo_ajusteManual(-1);
				}

				gbeact_f.setId_ejecucion(ejecucionVariacion.getId_ejecucion());
			}
			// Reemplazamos el ajuste de modelo macro unicamente para el escenario base (el
			// unico)
			// y reemplazamos el id ejecucion de la variacion para los 3 escenarios

			// ejecucionMotor.getGrupos_bloquesParamEscenario().get(0).setAjustesPdProy(gbe_pds_dif.getAjustesPdProy());
			// ejecucionMotor.getGrupos_bloquesParamEscenario().get(0).setId_ejecucion(ejecucionVariacion.getId_ejecucion());

//			for (GrupoBloquesEscenario gbeact_b : ejecucionMotor.getGrupos_bloquesParamEscenario()) {
//				if (gbeact_b.getEscenario_version().getEscenario().getFlag_obligatorio()) { // Solo base
//					gbeact_b.setAjustesPdProy(gbe_pds_dif.getAjustesPdProy());					
//				}
//				gbeact_b.setId_ejecucion(ejecucionVariacion.getId_ejecucion());
//			}				

			if (sas != null) {
				sas.ejecutarCalculo(ejecucionMotor, true, ejecucionVariacion.getFlag_regulatorio());
			} else {
				sas2.ejecutarCalculo(ejecucionMotor, true, ejecucionVariacion.getFlag_regulatorio());
			}

			LOGGER.info("Finaliza ejecucion de variacion en SAS");
		} catch (Exception ex) {
			LOGGER.error("Error al ejecutar el calculo de variacion en SAS:" + ex.getMessage());
			throw ex;
		} finally {
			// actualizamos el estado a ejecutado
			md.editarEstadoEjecucion(ejecucionVariacion, 2);
		}
	}

	// Ajustes Gasto

	public ArrayList<AjusteGasto> obtenerAjustesGasto() throws Exception {
		return md.obtenerAjustesGasto();
	}

	public void crearAjusteGasto(AjusteGasto ajusteGasto) throws Exception {
		md.crearAjusteGasto(ajusteGasto);
	}

	public void editarAjusteGasto(AjusteGasto ajusteGasto) throws Exception {
		md.editarAjusteGasto(ajusteGasto);
	}

	public void borrarAjusteGasto(Integer id_ajusteGasto) throws Exception {
		md.borrarAjusteGasto(id_ajusteGasto);
	}

	// Ajustes delta
	public ArrayList<MapeoDeltaStage> obtenerMapeosDeltaStage(Integer id_ejecucion_f2base) throws Exception {
		return md.obtenerMapeosDeltaStage(id_ejecucion_f2base);
	}

	public void ajustarDeltas(GrupoMapeosDelta grupoMapeosDelta) throws Exception {
		md.ajustarDeltas(grupoMapeosDelta);
	}

	// Ejecuciones validacion
	public ArrayList<EjecucionValidacion> obtenerEjecucionesValidacion() throws Exception {
		return md.obtenerEjecucionesValidacion();
	}

	public ArrayList<LogEjecEscenario> obtenerLogsEjecucionAgregada(Integer codmes, Integer id_juego_escenarios,
			Integer fase, Integer id_tipo_ambito) throws Exception {
		return md.obtenerLogsEjecucionAgregada(codmes, id_juego_escenarios, fase, id_tipo_ambito);
	}

	public LogEjecEscenario obtenerLogEjecucionPorIdEjec(Integer id_ejecucion) throws Exception {
		return md.obtenerLogEjecucionPorIdEjec(id_ejecucion);
	}

	public void editarEjecucionValidacion(EjecucionValidacion ejecucionValidacion) throws Exception {
		md.editarEjecucionValidacion(ejecucionValidacion);
	}

	public void solicitarAjustesEjecucionValidacion(EjecucionValidacion ejecucionValidacion) throws Exception {
		md.solicitarAjustesEjecucionValidacion(ejecucionValidacion);
	}

	public void exportarResultado(EjecucionValidacion ejecucionValidacion) throws Exception {
		EjecucionAgregadaPE ejecucion = md.obtenerEjecucionesAgregadaPEPorFaseCodmesJuego(ejecucionValidacion.getFase(),
				ejecucionValidacion.getCodmes(),
				ejecucionValidacion.getJuego_escenarios_version().getId_juego_escenarios());

		// Completamos mapeos de ejecucion agregada
		Integer id_ejec_modMac = null;

		if (ejecucionValidacion.getFase().equals(1)) {

			if (ejecucion.getEjecucion_modMacro() == null) {
				id_ejec_modMac = -1;
			} else {
				id_ejec_modMac = ejecucion.getEjecucion_modMacro().getId_ejecucion();
			}

			ejecucion.setMapeo_cargasModMac_ejecucion(md.obtenerMapeoCargasModMacEjecucion(ejecucion.getCodmes(),
					ejecucion.getJuego_escenarios_version().getId_juego_escenarios(), ejecucion.getTipo_ajusteManual(),
					id_ejec_modMac, ejecucionValidacion.getFase(), false));

		} else if (ejecucionValidacion.getFase().equals(2)) {
			ejecucion.setMapeo_cargasModMac_ejecucion(md.obtenerMapeoCargasModMacEjecucion(ejecucion.getCodmes(),
					ejecucion.getJuego_escenarios_version().getId_juego_escenarios(), -1, -1,
					ejecucionValidacion.getFase(), false));
		}

		String nombreFicheroOutput = "cartera_"
				+ ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera()
						.getNombre_cartera().replaceAll("\\s+", "")
				+ "_juego_"
				+ ejecucion.getJuego_escenarios_version().getNombre_juego_escenarios().replaceAll("\\s+", "") + "_"
				+ ejecucion.getCodmes() + "f" + ejecucionValidacion.getFase();

		String filtro = "CodEjecucion in (";
		ArrayList<String> ids_escenario = new ArrayList<String>();
		for (MapeoCargasModMacEscVer map : ejecucion.getMapeo_cargasModMac_ejecucion().getMapeosCargasModMacEsc()) {
			ids_escenario.add(map.getId_ejecucion().toString());
		}
		filtro += String.join(",", ids_escenario) + ")";

		md.escribirCSV(nombreFicheroOutput, filtro, ejecucionValidacion.getJuego_escenarios_version()
				.getCartera_version().getMetodologia().getCartera().getTipo_ambito().getId_tipo_ambito());
	}

	// Ajuste por Registro
	public ArrayList<EjecucionAjustes> obtenerEjecucionesAjustes() throws Exception {
		return md.obtenerEjecucionesAjustes();
	}

	public ArrayList<CampoOutputCredito> obtenerCamposOutputCredito() throws Exception {
		return md.obtenerCamposOutputCredito();
	}

	public ArrayList<RegistroOutputCredito> obtenerRegistrosOutputCredito(ConsultaRegistrosOutput consulta)
			throws Exception {
		return md.obtenerRegistrosOutputCredito(consulta);
	}

	public void ajustarRegistrosOutputCredito(GrupoRegistrosOutput grupoRegistroOutput) throws Exception {
		md.ajustarRegistrosOutputCredito(grupoRegistroOutput);
	}

	public InfoValidacionAjustes obtenerInfoValidacionAjustes(Integer id_ejecucion) throws Exception {
		return md.obtenerInfoValidacionAjustes(id_ejecucion);
	}

	// Ejecuciones PE agregadas
	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesAgregadaPE_fase1(Integer id_tipo_ambito) throws Exception {
		return md.obtenerEjecucionesAgregadaPE_fase1(id_tipo_ambito);
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesAgregadaPE_fase2(Integer id_tipo_ambito) throws Exception {
		return md.obtenerEjecucionesAgregadaPE_fase2(id_tipo_ambito);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucion_fase1(Integer codmes, Integer id_juego_escenarios,
			Integer tipo_ajusteManual, Integer id_ejecucion_modMac) throws Exception {
		return md.obtenerMapeoCargasModMacEjecucion_fase1(codmes, id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMac);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucion_fase2(Integer codmes, Integer id_juego_escenarios)
			throws Exception {
		return md.obtenerMapeoCargasModMacEjecucion_fase2(codmes, id_juego_escenarios);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacSimulacion(Integer codmes, Integer fase,
			Integer id_juego_escenarios, Integer tipo_ajusteManual, Integer id_ejecucion_modMac) throws Exception {
		return md.obtenerMapeoCargasModMacSimulacion(codmes, fase, id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMac);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase1(
			Integer id_juego_escenarios, Integer tipo_ajusteManual, Integer id_ejecucion_modMacro) throws Exception {
		return md.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase1(id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMacro);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase2(
			Integer id_juego_escenarios, Integer tipo_ajusteManual, Integer id_ejecucion_modMacro) throws Exception {
		return md.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase2(id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMacro);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenariosSimulacion(
			Integer id_juego_escenarios, Integer tipo_ajusteManual, Integer id_ejecucion_modMacro, Integer fase)
			throws Exception {
		return md.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenariosSimulacion(id_juego_escenarios,
				tipo_ajusteManual, id_ejecucion_modMacro, fase);
	}

	public void crearEjecucionAgregadaPE_fase1(EjecucionAgregadaPE ejecucionAgregadaPE) throws Exception {
		md.crearEjecucionAgregadaPE_fase1(ejecucionAgregadaPE);
	}

	public void crearEjecucionAgregadaPE_fase2(EjecucionAgregadaPE ejecucionAgregadaPE) throws Exception {
		md.crearEjecucionAgregadaPE_fase2(ejecucionAgregadaPE);
	}

	public ReplicaEjecucionesAgregadasPE replicarEjecucionesAgregadasPE_fase1(
			ReplicaEjecucionesAgregadasPE replicaEjecucionesPE) throws Exception {
		return md.replicarEjecucionesAgregadasPE_fase1(replicaEjecucionesPE);
	}

	public ReplicaEjecucionesAgregadasPE replicarEjecucionesAgregadasPE_fase2(
			ReplicaEjecucionesAgregadasPE replicaEjecucionesPE) throws Exception {
		return md.replicarEjecucionesAgregadasPE_fase2(replicaEjecucionesPE);
	}

	public void editarEjecucionAgregadaPE_fase1(EjecucionAgregadaPE ejecucionAgregadaPE) throws Exception {
		md.editarEjecucionAgregadaPE_fase1(ejecucionAgregadaPE);
	}

	public void editarEjecucionAgregadaPE_fase2(EjecucionAgregadaPE ejecucionAgregadaPE) throws Exception {
		md.editarEjecucionAgregadaPE_fase2(ejecucionAgregadaPE);
	}

	public void editarSimulacionAgregadaPE(EjecucionAgregadaPE ejecucionAgregadaPE) throws Exception {
		md.editarSimulacionAgregadaPE(ejecucionAgregadaPE);
	}

	public void borrarEjecucionAgregadaPE_fase1(Integer codmes, Integer id_juego_escenarios) throws Exception {
		md.borrarEjecucionAgregadaPE_fase1(codmes, id_juego_escenarios);
	}

	public void borrarEjecucionAgregadaPE_fase2(Integer codmes, Integer id_juego_escenarios) throws Exception {
		md.borrarEjecucionAgregadaPE_fase2(codmes, id_juego_escenarios);
	}

	public void borrarSimulacionBifAgregadaPE(Integer codmes, Integer fase, Integer id_juego_escenarios)
			throws Exception {
		md.borrarSimulacionBifAgregadaPE(codmes, fase, id_juego_escenarios);
	}

	// Simulaciones agregadas PE
	public ArrayList<EjecucionAgregadaPE> obtenerSimulacionesAgregadaPE(Boolean incluyeF2) throws Exception {
		return md.obtenerSimulacionesAgregadaPE(incluyeF2);
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotalesFase1() throws Exception {
		return md.obtenerEjecucionesPETotalesFase1();
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotalesFase2() throws Exception {
		return md.obtenerEjecucionesPETotalesFase2();
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotales() throws Exception {
		return md.obtenerEjecucionesPETotales();
	}

	public void crearSimulacionAgregadaPE(EjecucionAgregadaPE ejecucionAgregadaPE) throws Exception {
		md.crearSimulacionAgregadaPE(ejecucionAgregadaPE);
	}

	public ReplicaEjecucionesAgregadasPE replicarSimulacionesAgregadasPE(
			ReplicaEjecucionesAgregadasPE replicaEjecucionesPE) throws Exception {
		return md.replicarSimulacionesAgregadasPE(replicaEjecucionesPE);
	}

	public void solicitarValidacion(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		md.solicitarValidacion(ejecucionAgregada);
	}

	// -------------------------------------------------------------------
	// ----------------EJECUCION DE PRIMEROS PASOS------------------------
	// -------------------------------------------------------------------
	public ArrayList<EjecucionPrimerPaso> obtenerEjecucionesPrimerPaso() throws Exception {
		return md.obtenerEjecucionesPrimerPaso();
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionPrimerPaso(Integer id_ejecucion) throws Exception {
		return md.obtenerTablasMapeoEjecucionPrimerPaso(id_ejecucion);
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionPorIdPrimPasoMet(Integer id_primerPaso,
			Integer id_metodologia) throws Exception {
		return md.obtenerTablasMapeoEjecucionPorIdPrimPasoMet(id_primerPaso, id_metodologia);
	}

	public ArrayList<PrimerPaso> obtenerPrimerosPasosPorIdMetodologia(Integer id_metodologia) throws Exception {
		return md.obtenerPrimerosPasosPorIdMetodologia(id_metodologia);
	}

	public void crearEjecucionPrimerPaso(EjecucionPrimerPaso ejecucionPrimerPaso) throws Exception {
		md.crearEjecucionPrimerPaso(ejecucionPrimerPaso);
	}

	public ReplicaEjecucionesPrimerPaso replicarEjecucionesPrimerPaso(
			ReplicaEjecucionesPrimerPaso replicaEjecucionesPrimerPaso) throws Exception {
		return md.replicarEjecucionesPrimerPaso(replicaEjecucionesPrimerPaso);
	}

	public void editarEjecucionPrimerPaso(EjecucionPrimerPaso ejecucionPrimerPaso) throws Exception {
		md.editarEjecucionPrimerPaso(ejecucionPrimerPaso);
	}

	public void borrarEjecucionPrimerPaso(Integer id_ejecucion) throws Exception {
		md.borrarEjecucionPrimerPaso(id_ejecucion);
	}

	public void ejecutarPrimerPaso(EjecucionPrimerPaso ejecucionPrimerPaso) throws Exception {

		EjecucionPrimerPasoMotor ejecucionPP = null;

		try {

			ejecucionPP = md.convertirEjecucionPrimerPasoAMotor(ejecucionPrimerPaso);

			// actualizamos el estado a "en ejecucion"
			md.editarEstadoEjecucion(ejecucionPP, 1);

			LOGGER.info("Iniciando ejecucion de primer paso en SAS ");
			if (sas != null) {
				sas.ejecutarPrimerPaso(ejecucionPP);
			} else {
				sas2.ejecutarPrimerPaso(ejecucionPP);
			}

			LOGGER.info("Finaliza ejecucion primer paso en SAS ");
		} catch (Exception ex) {
			LOGGER.error("Error al ejecutar el primer paso en SAS:" + ex.getMessage());
		} finally {
			// actualizamos el estado a "ajecutado"
			md.editarEstadoEjecucion(ejecucionPP, 2);
		}
	}

	// -------------------------------------------------------------------
	// -------------------------PRIMEROS PASOS----------------------------
	// -------------------------------------------------------------------
	public ArrayList<PrimerPaso> obtenerPrimerosPasos() throws Exception {
		return md.obtenerPrimerosPasos();
	}

	public PrimerPasoParametrizado obtenerPrimerPasoParametrizado(Integer id_primerPaso, Integer id_metodologia)
			throws Exception {
		return md.obtenerPrimerPasoParametrizado(id_primerPaso, id_metodologia);
	}

	public void parametrizarPrimerPaso(PrimerPasoParametrizado primerPasoParam) throws Exception {
		md.parametrizarPrimerPaso(primerPasoParam);
	}

	// -------------------------------------------------------------------
	// ----------------------------MATLAB---------------------------------
	// -------------------------------------------------------------------

	// EJECUCION DE OTROS
	public ArrayList<EstructuraMetodologica> obtenerEstructurasOtrosPorIdCartera(Integer id_cartera) throws Exception {
		return md.obtenerEstructurasOtrosPorIdCartera(id_cartera);
	}

	public ArrayList<EjecucionOtro> obtenerEjecucionesOtros() throws Exception {
		return md.obtenerEjecucionesOtros();
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionOtros(Integer id_ejecucion) throws Exception {
		return md.obtenerTablasMapeoEjecucionOtros(id_ejecucion);
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionOtrosPorCartVer(Integer id_cartera_version)
			throws Exception {
		return md.obtenerTablasMapeoEjecucionOtrosPorCartVer(id_cartera_version);
	}

	public void crearEjecucionOtro(EjecucionOtro ejecucion) throws Exception {
		md.crearEjecucionOtro(ejecucion);
	}

	public ReplicaEjecucionesOtros replicarEjecucionesOtros(ReplicaEjecucionesOtros replicaEjecuciones)
			throws Exception {
		return md.replicarEjecucionesOtros(replicaEjecuciones);
	}

	public void editarEjecucionOtro(EjecucionOtro ejecucion) throws Exception {
		md.editarEjecucionOtro(ejecucion);
	}

	public void borrarEjecucionOtros(Integer id_ejecucion) throws Exception {
		md.borrarEjecucionOtros(id_ejecucion);
	}

	public void ejecutarCalculoOtros(EjecucionOtro ejecucion) throws Exception {
		EjecucionMotorOtro ejecucionMotor = null;

		try {
			if (ejecucion.getCartera_version().getMetodologia().getCartera().getTipo_ambito().getNombre_tipo_ambito()
					.equalsIgnoreCase("Creditos")) {
				/*
				 * ejecucionMotor = md.convertirEjecucionPEAMotor(ejecucionPE, 1, false, false);
				 * metodologiaEjecucion =
				 * ejecucionMotor.getJuego_escenarios_version().getCartera_version()
				 * .getMetodologia(); LOGGER.info("Iniciando simulación PE en SAS ");
				 * 
				 * // actualizamos el estado a "en ejecucion"
				 * md.editarEstadoEjecucion(ejecucionMotor, 1);
				 * 
				 * if (metodologiaEjecucion.getFlujo().getId_tipo_flujo() == 1 &&
				 * metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados()
				 * == 3) { md.ejecutarCalculoDirecto(ejecucionMotor); } else if
				 * (metodologiaEjecucion.getFlujo().getId_tipo_flujo() == 1 &&
				 * metodologiaEjecucion.getMetodo_resultados().getId_tipo_obtencion_resultados()
				 * == 2) { ejecucionMotor.setEjecuciones_extrapolacion(md.
				 * obtenerEjecucionesOficialesExtrapolacion(
				 * metodologiaEjecucion.getCarteras_extrapolacion(),
				 * ejecucionMotor.getCodmes())); sas.ejecutarCalculo(ejecucionMotor); } else {
				 * sas.ejecutarCalculo(ejecucionMotor); }
				 * LOGGER.info("Finaliza simulación PE en SAS ");
				 */
			} else if (ejecucion.getCartera_version().getMetodologia().getCartera().getTipo_ambito()
					.getNombre_tipo_ambito().equalsIgnoreCase("Inversiones")) {
				if (ejecucion.getCartera_version().getMetodologia().getMotor().getNombre_motor()
						.equalsIgnoreCase("R")) {
					LOGGER.info("Iniciando ejecucion de tipo otros en R ");
					ejecucionMotor = md.convertirEjecucionOtroAMotor(ejecucion);

					// actualizamos el estado a "en ejecucion"
					md.editarEstadoEjecucion(ejecucionMotor, 1);

					r.ejecutarCalculoOtros(ejecucionMotor);
					LOGGER.info("Finaliza ejecucion de tipo otros en R ");
				}
			}

		} catch (Exception ex) {
			LOGGER.error("Error en el ejecutar un calculo de tipo otros:" + ex.getMessage());
			throw ex;
		} finally {
			md.editarEstadoEjecucion(ejecucionMotor, 2);

			// Sin avances en codigos de inversiones
			if (ejecucion.getCartera_version().getMetodologia().getCartera().getTipo_ambito().getNombre_tipo_ambito()
					.equalsIgnoreCase("Inversiones")) {
				md.editarAvanceEjecucion(ejecucionMotor, 100);
			}

			/*
			 * if (ejecucionMotor.getGrupos_bloquesParamEscenario().size() ==
			 * md.obtenerEscenarios().size()) { // actualizamos el estado a ejecutado
			 * md.editarEstadoEjecucion(ejecucionMotor, 2); } else { // actualizamos el
			 * estado a ejecutado parcialmente (solo algunos escenarios)
			 * md.editarEstadoEjecucion(ejecucionMotor, 3); }
			 */
		}
	}

	// Manejo de tokens (unico usuario concurrente)
	public Boolean verificarTokenAcceso(String id_usuario, String token) throws Exception {
		return md.verificarTokenAcceso(id_usuario, token);
	}

	public Boolean verificarExpiracionToken(String id_usuario) throws Exception {
		return md.verificarExpiracionToken(id_usuario);
	}

	public void actualizarExpiracionSesion(String id_usuario, Long timeStmpLimite) throws Exception {
		md.actualizarExpiracionSesion(id_usuario, timeStmpLimite);
	}

	public void reiniciarSesionUsuario(String id_usuario) throws Exception {
		md.reiniciarSesionUsuario(id_usuario);
	}

	// Validacion de posible sql, html, script injection
	@SuppressWarnings("unchecked")
	public void validarInjectObjeto(Object obj, Boolean validaSql, Boolean validaHtml) throws Exception {
		Class<?> cl = obj.getClass();
		Field[] flds = cl.getDeclaredFields();

		// Recorremos atributos
		for (Field f : flds) {
			f.setAccessible(true); // Para acceder a atributos privados
			// LOGGER.info("Field types: " + f.getType().toGenericString());
			// LOGGER.info(" (Atributo: " + f.getName() /*+ " | Valor: " +
			// f.get(obj).toString() + ")"*/);
			if (f.getType().toGenericString().toLowerCase().contains("string")) {
				// Valida posible html/script injection
				if (validaHtml) {
					if (f.get(obj) != null && (f.get(obj).toString().toLowerCase().contains("<")
							|| f.get(obj).toString().toLowerCase().contains(">"))) {
						LOGGER.warn("Riesgo de Injection! -> Atributo: "
								+ f.getName());/*
												 * + " - Valor: " + f.get(obj).toString());
												 */
						throw new CustomException("Se encontró una vulnerabilidad en la petición!" + " (Atributo: "
								+ f.getName()); /* + " | Valor: " + f.get(obj).toString() + ")"); */
					}
				}

				// Valida posible sql injection
				if (validaSql) {
					if (f.get(obj) != null && (f.get(obj).toString().toLowerCase().contains("drop")
							|| f.get(obj).toString().toLowerCase().contains("alter")
							|| f.get(obj).toString().toLowerCase().contains("create")
							|| f.get(obj).toString().toLowerCase().contains("select")
							|| f.get(obj).toString().toLowerCase().contains("update")
							|| f.get(obj).toString().toLowerCase().contains("insert into")
							|| f.get(obj).toString().toLowerCase().contains("grant")
							|| f.get(obj).toString().toLowerCase().contains("revoke"))) {
						LOGGER.warn("Riesgo de Injection! -> Atributo: " + f.getName() + " - Valor: "
								+ f.get(obj).toString());
						throw new CustomException("Se encontró una vulnerabilidad en la petición!" + " (Atributo: "
								+ f.getName()); /* + " | Valor: " + f.get(obj).toString() + ")"); */
					}
				}
			} else if (!f.getType().toGenericString().toLowerCase().contains("integer")
					&& !f.getType().toGenericString().toLowerCase().contains("int")
					&& !f.getType().toGenericString().toLowerCase().contains("double")
					&& !f.getType().toGenericString().toLowerCase().contains("long")
					&& !f.getType().toGenericString().toLowerCase().contains("boolean")) {

				if (f.getType().toGenericString().toLowerCase().contains("arraylist")) {
					if (f.get(obj) != null) {
						for (Object objAr : (ArrayList<Object>) f.get(obj)) {
							if (!(objAr instanceof Integer)) {
								this.validarInjectObjeto(objAr, validaSql, validaHtml);
							}
						}
					}
				} else {
					if (f.get(obj) != null) {
						this.validarInjectObjeto(f.get(obj), validaSql, validaHtml);
					}
				}
			}
		}
	}

	public void validarRutaSegura(String ruta) throws Exception {
		if (properties.getProperty("Rutas_local") != null
				&& properties.getProperty("Rutas_local").equalsIgnoreCase("SI")) {
			// No aplicar esta validacion cuando se esta en modo de rutas local
		} else {
			if (ruta.toUpperCase().contains("C:")) {
				throw new CustomException("No se puede utilizar el disco C, porque es una ruta protegida del servidor");
			}
		}
	}

	

	public ConfiguracionSAS obtenerConfiguracionSAS() throws Exception {
		return md.obtenerConfiguracionSAS();
	}

	public RutaValida editarConfiguracionSAS(ConfiguracionSAS conf) throws Exception {
		RutaValida rutaValida = new RutaValida();
		try {
			rutaValida = md.validarSAS(conf);
			if (rutaValida.getValido()) {
//				py.editarConfiguracionTeradata(conf);
				sas.editarConfiguracion(conf);
				md.editarConfiguracionSAS(conf);
			}
		} catch (Exception ex) {
			LOGGER.error("Error al editar la configuración de SAS: " + ex.getMessage());
			throw ex;
		}

		return rutaValida;
	}

	public RutaValida validarSAS(ConfiguracionSAS conf) throws Exception {
		return md.validarSAS(conf);
	}

	public ArrayList<LogCodInicializacion> obtenerLogsCodInicializacion(Integer codmes, Integer id_juego_escenarios,
			Integer fase, Integer id_tipo_ambito) throws Exception {
		return md.obtenerLogsCodInicializacion(codmes, id_juego_escenarios, fase, id_tipo_ambito);
	}

	public ArrayList<LogCodInicializacion> obtenerLogsCodInicializacionVariacion(Integer id_Ejecucion)
			throws Exception {
		return md.obtenerLogsCodInicializacionVariacion(id_Ejecucion);
	}

	public CargaFichero obtenerCamposporTablaInputVersion(CargaFichero cargaFichero) throws Exception {
		ArrayList<CampoInput> camposInput = new ArrayList<CampoInput>();
		camposInput = md.obtenerCamposporTablaInputVersion(cargaFichero.getTablaInput());

		cargaFichero.getTablaInput().setCampos_input(camposInput);

		return cargaFichero;
	}

}