package sim.ws.rest;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ws.rs.core.Application;

@Startup
@Singleton
public class RestfulServices extends Application {
 
	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(RestfulServices.class);
    private static RestfulServices restfulservices = obtenerInstancia(); 
	
    private RestfulServices() {
    	LOGGER.info("Inicializando Restful Services");
    	try {
	    	new AprovisionamientoService();
	    	new ConfiguracionService();
	    	new ParametrizacionService();
	    	new AdminSistemaService();
	    	new EventosService();
	    	new InicioService();
	    	new EjecucionesService();
	    	LOGGER.info("Restful Services iniciados");
    	} catch (Exception ex) {
    		LOGGER.fatal("Error al inicializar los services: " + ex.getMessage());
    	}
    }
    
    public static synchronized RestfulServices obtenerInstancia() {
    	if (restfulservices == null) {
    		restfulservices = new RestfulServices();
		}
		return restfulservices;
    }

}