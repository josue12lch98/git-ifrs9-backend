package sim.persistencia;

public class Regla_Staging {
	private Integer id_regla;
	private Integer id_cartera_version;
	private String nombre_regla;
	private String formula_input;
	private String formula_sql;
	private Integer id_nodo_cumple;
	private Integer id_nodo_no_cumple;
	private Boolean flag_stage_nodo_cumple;
	private Boolean flag_stage_nodo_no_cumple;
	private Integer orden;

	public Regla_Staging() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId_regla() {
		return id_regla;
	}

	public void setId_regla(Integer id_regla) {
		this.id_regla = id_regla;
	}

	public Integer getId_cartera_version() {
		return id_cartera_version;
	}

	public void setId_cartera_version(Integer id_cartera_version) {
		this.id_cartera_version = id_cartera_version;
	}

	public String getNombre_regla() {
		return nombre_regla;
	}

	public void setNombre_regla(String nombre_regla) {
		this.nombre_regla = nombre_regla;
	}

	public String getFormula_input() {
		return formula_input;
	}

	public void setFormula_input(String formula_input) {
		this.formula_input = formula_input;
	}

	public String getFormula_sql() {
		return formula_sql;
	}

	public void setFormula_sql(String formula_sql) {
		this.formula_sql = formula_sql;
	}

	public Integer getId_nodo_cumple() {
		return id_nodo_cumple;
	}

	public void setId_nodo_cumple(Integer id_nodo_cumple) {
		this.id_nodo_cumple = id_nodo_cumple;
	}

	public Integer getId_nodo_no_cumple() {
		return id_nodo_no_cumple;
	}

	public void setId_nodo_no_cumple(Integer id_nodo_no_cumple) {
		this.id_nodo_no_cumple = id_nodo_no_cumple;
	}

	public Boolean getFlag_stage_nodo_cumple() {
		return flag_stage_nodo_cumple;
	}

	public void setFlag_stage_nodo_cumple(Boolean flag_stage_nodo_cumple) {
		this.flag_stage_nodo_cumple = flag_stage_nodo_cumple;
	}

	public Boolean getFlag_stage_nodo_no_cumple() {
		return flag_stage_nodo_no_cumple;
	}

	public void setFlag_stage_nodo_no_cumple(Boolean flag_stage_nodo_no_cumple) {
		this.flag_stage_nodo_no_cumple = flag_stage_nodo_no_cumple;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

}
