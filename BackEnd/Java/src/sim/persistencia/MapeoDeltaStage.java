package sim.persistencia;

public class MapeoDeltaStage {
	private Stage stage;	
	private Double delta_calculado;
	private Double delta_final;
	private Boolean flag_delta_ajustado;
	
	public MapeoDeltaStage() {
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public Double getDelta_calculado() {
		return delta_calculado;
	}

	public void setDelta_calculado(Double delta_calculado) {
		this.delta_calculado = delta_calculado;
	}

	public Boolean getFlag_delta_ajustado() {
		return flag_delta_ajustado;
	}

	public void setFlag_delta_ajustado(Boolean flag_delta_ajustado) {
		this.flag_delta_ajustado = flag_delta_ajustado;
	}

	public Double getDelta_final() {
		return delta_final;
	}

	public void setDelta_final(Double delta_final) {
		this.delta_final = delta_final;
	}	
	
}
