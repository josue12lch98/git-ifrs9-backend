package sim.persistencia.dic;

public class TipoCartera {
	private Integer id_tipo_cartera;
	private String nombre_tipo_cartera;
	private String nombre_variablemotor;
	private TipoAmbito tipo_ambito;
	
	public TipoCartera() {
	}

	public Integer getId_tipo_cartera() {
		return id_tipo_cartera;
	}

	public void setId_tipo_cartera(Integer id_tipo_cartera) {
		this.id_tipo_cartera = id_tipo_cartera;
	}

	public String getNombre_tipo_cartera() {
		return nombre_tipo_cartera;
	}

	public void setNombre_tipo_cartera(String nombre_tipo_cartera) {
		this.nombre_tipo_cartera = nombre_tipo_cartera;
	}

	public String getNombre_variablemotor() {
		return nombre_variablemotor;
	}

	public void setNombre_variablemotor(String nombre_variablemotor) {
		this.nombre_variablemotor = nombre_variablemotor;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}

	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}
}
