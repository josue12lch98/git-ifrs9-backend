package sim.persistencia;

import sim.persistencia.dic.TipoAmbito;

public class Ruta {
	
	//Datos basicos de una ruta de aprovisionamiento
	private Integer id_ruta;
	private String ruta;
	private TipoAmbito tipo_ambito;
	private Boolean flag_activa;
	private Boolean flag_maestro;
	private String descripcion;
	private Boolean permisosEnRuta;
	//Constructor
	public Ruta() {
	}

	//Getters y setters
	public Integer getId_ruta() {
		return id_ruta;
	}
	public void setId_ruta(Integer id_ruta) {
		this.id_ruta = id_ruta;
	}

	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}
	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}

	public Boolean getFlag_activa() {
		return flag_activa;
	}
	public void setFlag_activa(Boolean flag_activa) {
		this.flag_activa = flag_activa;
	}

	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getFlag_maestro() {
		return flag_maestro;
	}

	public void setFlag_maestro(Boolean flag_maestro) {
		this.flag_maestro = flag_maestro;
	}

	public Boolean getPermisosEnRuta() {
		return permisosEnRuta;
	}

	public void setPermisosEnRuta(Boolean permisosEnRuta) {
		this.permisosEnRuta = permisosEnRuta;
	}

}
