package sim;

import sim.persistencia.Evento;

public class Objeto_Evento {

	private Evento evento;
	private Object clase;
	
	public Objeto_Evento(Evento evento, Object clase) {
		this.evento = evento;
		this.clase = clase;
	}
	public Evento getEvento() {
		return evento;
	}
	public void setEvento(Evento evento) {
		this.evento = evento;
	}
	
	public Object getClase() {
		return clase;
	}
	public void setClase(Object clase) {
		this.clase = clase;
	}
	
}
