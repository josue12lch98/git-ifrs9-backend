package sim.persistencia;

import java.util.ArrayList;

public class GrupoMapeosDelta {
	
	private Integer id_ejecucion_f2base; //ejecucion de fase 2 - solo para escenario base
	private ArrayList<MapeoDeltaStage> mapeos_delta;
	
	public GrupoMapeosDelta() {		
	}

	public ArrayList<MapeoDeltaStage> getMapeos_delta() {
		return mapeos_delta;
	}

	public void setMapeos_delta(ArrayList<MapeoDeltaStage> mapeos_delta) {
		this.mapeos_delta = mapeos_delta;
	}

	public Integer getId_ejecucion_f2base() {
		return id_ejecucion_f2base;
	}

	public void setId_ejecucion_f2base(Integer id_ejecucion_f2base) {
		this.id_ejecucion_f2base = id_ejecucion_f2base;
	}	
}
