package sim.persistencia;

import java.util.ArrayList;

public class JuegoEscenarios_version {
	private Integer id_juego_escenarios;
	private Cartera_version cartera_version;
	private String nombre_juego_escenarios;
	private String descripcion_juego_escenarios;
		
	//Unicamente para parametrizacion	
	private ArrayList<Escenario_version> escenarios_version;
	
	public JuegoEscenarios_version() {		
	}
	
	public Integer getId_juego_escenarios() {
		return id_juego_escenarios;
	}
	public void setId_juego_escenarios(Integer id_juego_escenarios) {
		this.id_juego_escenarios = id_juego_escenarios;
	}
	public Cartera_version getCartera_version() {
		return cartera_version;
	}
	public void setCartera_version(Cartera_version cartera_version) {
		this.cartera_version = cartera_version;
	}
	public String getNombre_juego_escenarios() {
		return nombre_juego_escenarios;
	}
	public void setNombre_juego_escenarios(String nombre_juego_escenarios) {
		this.nombre_juego_escenarios = nombre_juego_escenarios;
	}
	public String getDescripcion_juego_escenarios() {
		return descripcion_juego_escenarios;
	}
	public void setDescripcion_juego_escenarios(String descripcion_juego_escenarios) {
		this.descripcion_juego_escenarios = descripcion_juego_escenarios;
	}

	public ArrayList<Escenario_version> getEscenarios_version() {
		return escenarios_version;
	}

	public void setEscenarios_version(ArrayList<Escenario_version> escenarios_version) {
		this.escenarios_version = escenarios_version;
	}
}
