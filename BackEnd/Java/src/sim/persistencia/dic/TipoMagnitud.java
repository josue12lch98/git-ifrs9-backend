package sim.persistencia.dic;

public class TipoMagnitud {
	private Integer id_tipo_magnitud;
	private String nombre_tipo_magnitud;
	private TipoAmbito tipo_ambito;
	
	public TipoMagnitud() {
	}
	
	public Integer getId_tipo_magnitud() {
		return id_tipo_magnitud;
	}
	public void setId_tipo_magnitud(Integer id_tipo_magnitud) {
		this.id_tipo_magnitud = id_tipo_magnitud;
	}
	public String getNombre_tipo_magnitud() {
		return nombre_tipo_magnitud;
	}
	public void setNombre_tipo_magnitud(String nombre_tipo_magnitud) {
		this.nombre_tipo_magnitud = nombre_tipo_magnitud;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}

	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}
	
}
