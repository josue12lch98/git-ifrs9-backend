package sim.persistencia;

import java.util.ArrayList;
import java.util.Calendar;

public class CargaFichero {
	private Integer id_cargafichero;
	private Fecha fecha;
	private Integer version_carga;
	private String nombre_carga;
	private Integer orden_inicial;
	private Integer orden_final;
	private Integer reproceso;
	private String comentario_calidad;
	private Integer fecha_carga;
	private Calendar fecha_calculo;
	private TablaInput tablaInput;
	private ArrayList<OutputReglaCalidad> outputsReglaCalidad;
	private Boolean flag_oficial;
	private ArrayList<Boolean> error_campo;
	private Boolean flag_ultimaCarga;
	private Integer tipoEstadoCarga;
	private String filtroAjuste;
	private Integer contadorAlertasRojas;
	private Integer contadorAlertasAmarillas;
	private Integer contadorRegistros;
	private String codUsuario;
	private Integer tiempoDeCarga;
	private String tablaDestino;
	private Integer numeroCampos;

	public CargaFichero() {
		tablaInput = new TablaInput();
		fecha = new Fecha();
//		fecha_calculo = new Calendar();
		setFlag_oficial(false);
		filtroAjuste = "";
		tipoEstadoCarga = 0;
		contadorRegistros = 0;
		tiempoDeCarga=0;
		numeroCampos=1;
		tablaDestino="";
		
	}

	/*
	 * private String nombre_tabla; private Integer id_version_metadata; private
	 * String descripcion_version_metadata; private Integer id_version_carga;
	 * private Integer fecha_referencia; private String descripcion_version_carga;
	 * private boolean flag_ultima_tabla; private boolean flag_ajustado; private
	 * Integer id_cartera; private Integer id_entidad;
	 */

	public Boolean getFlag_oficial() {
		return flag_oficial;
	}

	public void setFlag_oficial(Boolean flag_oficial) {
		this.flag_oficial = flag_oficial;
	}

	public Integer getVersion_carga() {
		return version_carga;
	}

	public Integer getId_cargafichero() {
		return id_cargafichero;
	}

	public void setId_cargafichero(Integer id_cargafichero) {
		this.id_cargafichero = id_cargafichero;
	}

	public String getNombre_carga() {
		return nombre_carga;
	}

	public void setNombre_carga(String nombre_carga) {
		this.nombre_carga = nombre_carga;
	}

	public Integer getOrden_inicial() {
		return orden_inicial;
	}

	public void setOrden_inicial(Integer orden_inicial) {
		this.orden_inicial = orden_inicial;
	}

	public Integer getOrden_final() {
		return orden_final;
	}

	public void setOrden_final(Integer orden_final) {
		this.orden_final = orden_final;
	}

	public Integer getReproceso() {
		return reproceso;
	}

	public void setReproceso(Integer reproceso) {
		this.reproceso = reproceso;
	}

	public String getComentario_calidad() {
		return comentario_calidad;
	}

	public void setComentario_calidad(String comentario_calidad) {
		this.comentario_calidad = comentario_calidad;
	}

	public Integer getFecha_carga() {
		return fecha_carga;
	}

	public void setFecha_carga(Integer fecha_carga) {
		this.fecha_carga = fecha_carga;
	}

	public void setVersion_carga(Integer version_carga) {
		this.version_carga = version_carga;
	}

	public Fecha getFecha() {
		return fecha;
	}

	public void setFecha(Fecha fecha) {
		this.fecha = fecha;
	}

	public ArrayList<OutputReglaCalidad> getOutputsReglaCalidad() {
		return outputsReglaCalidad;
	}

	public void setOutputsReglaCalidad(ArrayList<OutputReglaCalidad> outputsReglaCalidad) {
		this.outputsReglaCalidad = outputsReglaCalidad;
	}

	public TablaInput getTablaInput() {
		return tablaInput;
	}

	public void setTablaInput(TablaInput tablaInput) {
		this.tablaInput = tablaInput;
	}

	public ArrayList<Boolean> getError_campo() {
		return error_campo;
	}

	public void setError_campo(ArrayList<Boolean> error_campo) {
		this.error_campo = error_campo;
	}

	public Boolean getFlag_ultimaCarga() {
		return flag_ultimaCarga;
	}

	public void setFlag_ultimaCarga(Boolean flag_ultimaCarga) {
		this.flag_ultimaCarga = flag_ultimaCarga;
	}

	public Integer getTipoEstadoCarga() {
		return tipoEstadoCarga;
	}

	public void setTipoEstadoCarga(Integer tipoEstadoCarga) {
		this.tipoEstadoCarga = tipoEstadoCarga;
	}

	public String getFiltroAjuste() {
		return filtroAjuste;
	}

	public void setFiltroAjuste(String filtroAjuste) {
		this.filtroAjuste = filtroAjuste;
	}

	public Integer getContadorAlertasAmarillas() {
		return contadorAlertasAmarillas;
	}

	public void setContadorAlertasAmarillas(Integer contadorAlertasAmarillas) {
		this.contadorAlertasAmarillas = contadorAlertasAmarillas;
	}

	public Integer getContadorAlertasRojas() {
		return contadorAlertasRojas;
	}

	public void setContadorAlertasRojas(Integer contadorAlertasRojas) {
		this.contadorAlertasRojas = contadorAlertasRojas;
	}

	public Integer getContadorRegistros() {
		return contadorRegistros;
	}

	public void setContadorRegistros(Integer contadorRegistros) {
		this.contadorRegistros = contadorRegistros;
	}

	public Calendar getFecha_calculo() {
		return fecha_calculo;
	}

	public void setFecha_calculo(Calendar fecha_calculo) {
		this.fecha_calculo = fecha_calculo;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public Integer getTiempoDeCarga() {
		return tiempoDeCarga;
	}

	public void setTiempoDeCarga(Integer tiempoDeCarga) {
		this.tiempoDeCarga = tiempoDeCarga;
	}

	public String getTablaDestino() {
		return tablaDestino;
	}

	public void setTablaDestino(String tablaDestino) {
		this.tablaDestino = tablaDestino;
	}

	public Integer getNumeroCampos() {
		return numeroCampos;
	}

	public void setNumeroCampos(Integer numeroCampos) {
		this.numeroCampos = numeroCampos;
	}

//	public Integer getFechaCalculoNumerica() {
//		return this.fecha_calculo.get(Calendar.YEAR) * 10000 + (this.fecha_calculo.get(Calendar.MONTH) + 1) * 100
//				+ this.fecha_calculo.get(Calendar.DAY_OF_MONTH);
//	}
}