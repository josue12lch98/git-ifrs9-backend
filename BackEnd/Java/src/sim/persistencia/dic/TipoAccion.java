package sim.persistencia.dic;

public class TipoAccion {
	private Integer id_tipo_accion;
	private String nombre_tipo_accion;
	
	public TipoAccion() {
	}

	public Integer getId_tipo_accion() {
		return id_tipo_accion;
	}

	public void setId_tipo_accion(Integer id_tipo_accion) {
		this.id_tipo_accion = id_tipo_accion;
	}

	public String getNombre_tipo_accion() {
		return nombre_tipo_accion;
	}

	public void setNombre_tipo_accion(String nombre_tipo_accion) {
		this.nombre_tipo_accion = nombre_tipo_accion;
	}
	
	
}
