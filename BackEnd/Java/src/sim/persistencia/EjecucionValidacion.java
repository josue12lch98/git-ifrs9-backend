package sim.persistencia;

public class EjecucionValidacion {	
	private Integer codmes; 
	private Integer fase;
	private JuegoEscenarios_version juego_escenarios_version;
	private Integer estado_validacion;
	private String log_motor;
	private Integer estadoAgregado_ajustes; // 0: Todos en 'Sin ajustes' (sin ajustes solicitados), 
											// 1: Todos en 'ajustado sin aprobar' (ajustes listos)
											// 2: Otro (Ajustes sin completar)
											// 3: Todos en 'ajustado y aprobado' (ajustes aprobados)
	private String comentario_ejecucion;
	
	public EjecucionValidacion() {		
	}
	
	public Integer getCodmes() {
		return codmes;
	}
	public void setCodmes(Integer codmes) {
		this.codmes = codmes;
	}
	public Integer getFase() {
		return fase;
	}
	public void setFase(Integer fase) {
		this.fase = fase;
	}
	public JuegoEscenarios_version getJuego_escenarios_version() {
		return juego_escenarios_version;
	}
	public void setJuego_escenarios_version(JuegoEscenarios_version juego_escenarios_version) {
		this.juego_escenarios_version = juego_escenarios_version;
	}
	public Integer getEstado_validacion() {
		return estado_validacion;
	}
	public void setEstado_validacion(Integer estado_validacion) {
		this.estado_validacion = estado_validacion;
	}

	public String getComentario_ejecucion() {
		return comentario_ejecucion;
	}

	public void setComentario_ejecucion(String comentario_ejecucion) {
		this.comentario_ejecucion = comentario_ejecucion;
	}
	
	public Integer getEstadoAgregado_ajustes() {
		return estadoAgregado_ajustes;
	}

	public void setEstadoAgregado_ajustes(Integer estadoAgregado_ajustes) {
		this.estadoAgregado_ajustes = estadoAgregado_ajustes;
	}

	public String getLog_motor() {
		return log_motor;
	}

	public void setLog_motor(String log_motor) {
		this.log_motor = log_motor;
	}	
}
