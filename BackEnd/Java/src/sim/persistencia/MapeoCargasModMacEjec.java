package sim.persistencia;

import java.util.ArrayList;

public class MapeoCargasModMacEjec {	
	private ArrayList<TablaMapeoEjecucion> tablas_mapeo_carteraVersion;
	private ArrayList<MapeoCargasModMacEscVer> mapeosCargasModMacEsc;
	
	public MapeoCargasModMacEjec() {}

	public ArrayList<TablaMapeoEjecucion> getTablas_mapeo_carteraVersion() {
		return tablas_mapeo_carteraVersion;
	}

	public void setTablas_mapeo_carteraVersion(ArrayList<TablaMapeoEjecucion> tablas_mapeo_carteraVersion) {
		this.tablas_mapeo_carteraVersion = tablas_mapeo_carteraVersion;
	}

	public ArrayList<MapeoCargasModMacEscVer> getMapeosCargasModMacEsc() {
		return mapeosCargasModMacEsc;
	}

	public void setMapeosCargasModMacEsc(ArrayList<MapeoCargasModMacEscVer> mapeosCargasModMacEsc) {
		this.mapeosCargasModMacEsc = mapeosCargasModMacEsc;
	}
	
}
