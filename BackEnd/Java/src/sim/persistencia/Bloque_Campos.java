package sim.persistencia;
import java.util.ArrayList;

public class Bloque_Campos extends Bloque {
	private ArrayList <CampoBloque> campos;

	public Bloque_Campos() {
		super();
	}

	public ArrayList<CampoBloque> getCampos() {
		return campos;
	}

	public void setCampos(ArrayList<CampoBloque> campos) {
		this.campos = campos;
	}
	
}
