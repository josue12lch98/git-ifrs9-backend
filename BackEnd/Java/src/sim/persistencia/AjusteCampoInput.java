package sim.persistencia;

public class AjusteCampoInput {
	private String nombre_campo;
	private Boolean flag_numero;
	private Boolean flag_texto;
	private Double valor_numero;
	private String valor_texto;

	public AjusteCampoInput() {
		setFlag_numero(false);
		setFlag_texto(false);
	}

	public String getNombre_campo() {
		return nombre_campo;
	}

	public void setNombre_campo(String nombre_campo) {
		this.nombre_campo = nombre_campo;
	}

	public Boolean getFlag_numero() {
		return flag_numero;
	}

	public void setFlag_numero(Boolean flag_numero) {
		this.flag_numero = flag_numero;
	}

	public Boolean getFlag_texto() {
		return flag_texto;
	}

	public void setFlag_texto(Boolean flag_texto) {
		this.flag_texto = flag_texto;
	}

	public Double getValor_numero() {
		return valor_numero;
	}

	public void setValor_numero(Double valor_numero) {
		this.valor_numero = valor_numero;
	}

	public String getValor_texto() {
		return valor_texto;
	}

	public void setValor_texto(String valor_texto) {
		this.valor_texto = valor_texto;
	}

}
