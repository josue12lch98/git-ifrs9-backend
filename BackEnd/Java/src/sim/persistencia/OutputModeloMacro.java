package sim.persistencia;

public class OutputModeloMacro {
	private Integer FecEjecucion;
	private Integer CodCarteraVersion;
	private Integer CodEscenarioVersion;
	private String NbrFactor;
	private Integer NumNivel1;
	private Integer NumNivel2;
	private Integer NumNivel3;
	private Integer NumNivel4;
	private Integer NumNivel5;
	private Integer NumNivel6;
	
	
	public OutputModeloMacro() {
		
	}
	
	public Integer getFecEjecucion() {
		return FecEjecucion;
	}
	public void setFecEjecucion(Integer fecEjecucion) {
		FecEjecucion = fecEjecucion;
	}
	public Integer getCodCarteraVersion() {
		return CodCarteraVersion;
	}
	public void setCodCarteraVersion(Integer codCarteraVersion) {
		CodCarteraVersion = codCarteraVersion;
	}
	public Integer getCodEscenarioVersion() {
		return CodEscenarioVersion;
	}
	public void setCodEscenarioVersion(Integer codEscenarioVersion) {
		CodEscenarioVersion = codEscenarioVersion;
	}
	public String getNbrFactor() {
		return NbrFactor;
	}
	public void setNbrFactor(String nbrFactor) {
		NbrFactor = nbrFactor;
	}

	public Integer getNumNivel1() {
		return NumNivel1;
	}

	public void setNumNivel1(Integer numNivel1) {
		NumNivel1 = numNivel1;
	}

	public Integer getNumNivel2() {
		return NumNivel2;
	}

	public void setNumNivel2(Integer numNivel2) {
		NumNivel2 = numNivel2;
	}

	public Integer getNumNivel3() {
		return NumNivel3;
	}

	public void setNumNivel3(Integer numNivel3) {
		NumNivel3 = numNivel3;
	}

	public Integer getNumNivel4() {
		return NumNivel4;
	}

	public void setNumNivel4(Integer numNivel4) {
		NumNivel4 = numNivel4;
	}

	public Integer getNumNivel5() {
		return NumNivel5;
	}

	public void setNumNivel5(Integer numNivel5) {
		NumNivel5 = numNivel5;
	}

	public Integer getNumNivel6() {
		return NumNivel6;
	}

	public void setNumNivel6(Integer numNivel6) {
		NumNivel6 = numNivel6;
	}	
}
