package sim.persistencia;

public class EventoDetalle {
	
	private Integer id_evento;
	private String objeto_json;
	private String actualizado_json;
	private String agregado_json;
	private String borrado_json;
	
	public Integer getId_evento() {
		return id_evento;
	}
	public void setId_evento(Integer id_evento) {
		this.id_evento = id_evento;
	}
	public String getObjeto_json() {
		return objeto_json;
	}
	public void setObjeto_json(String objeto_json) {
		this.objeto_json = objeto_json;
	}
	public String getActualizado_json() {
		return actualizado_json;
	}
	public void setActualizado_json(String actualizado_json) {
		this.actualizado_json = actualizado_json;
	}
	public String getAgregado_json() {
		return agregado_json;
	}
	public void setAgregado_json(String agregado_json) {
		this.agregado_json = agregado_json;
	}
	public String getBorrado_json() {
		return borrado_json;
	}
	public void setBorrado_json(String borrado_json) {
		this.borrado_json = borrado_json;
	}
	

}
