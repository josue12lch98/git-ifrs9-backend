package sim.persistencia;

public class Notificacion {
	private Integer id_correo;
	private Integer id_tipoObjeto;
	private Integer id_tipoAccion;

	public Integer getId_correo() {
		return id_correo;
	}

	public void setId_correo(Integer id_correo) {
		this.id_correo = id_correo;
	}

	public Integer getId_tipoObjeto() {
		return id_tipoObjeto;
	}

	public void setId_tipoObjeto(Integer id_tipoObjeto) {
		this.id_tipoObjeto = id_tipoObjeto;
	}

	public Integer getId_tipoAccion() {
		return id_tipoAccion;
	}

	public void setId_tipoAccion(Integer id_tipoAccion) {
		this.id_tipoAccion = id_tipoAccion;
	}

}
