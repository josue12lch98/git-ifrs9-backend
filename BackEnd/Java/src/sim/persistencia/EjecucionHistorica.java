package sim.persistencia;

import java.util.ArrayList;

public class EjecucionHistorica {
       ArrayList<MapeoCargasModMacEscVer> ejecuciones_escVer;
       CargaFichero cargaBloqPrincipal_hist;
       
       public EjecucionHistorica() {}    
       
       public CargaFichero getCargaBloqPrincipal_hist() {
             return cargaBloqPrincipal_hist;
       }

       public void setCargaBloqPrincipal_hist(CargaFichero cargaBloqPrincipal_hist) {
             this.cargaBloqPrincipal_hist = cargaBloqPrincipal_hist;
       }      
       
       public ArrayList<MapeoCargasModMacEscVer> getEjecuciones_escVer() {
             return ejecuciones_escVer;
       }

       public void setEjecuciones_escVer(ArrayList<MapeoCargasModMacEscVer> ejecuciones_escVer) {
             this.ejecuciones_escVer = ejecuciones_escVer;
       }
}

