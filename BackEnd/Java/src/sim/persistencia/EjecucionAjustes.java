package sim.persistencia;

public class EjecucionAjustes {
	private Integer id_ejecucion;
	private Integer codmes; 
	private Integer fase;
	private Escenario_version escenario_version;	
	private Integer estado_ajustes;	
	
	public EjecucionAjustes() {		
	}
	
	public Integer getId_ejecucion() {
		return id_ejecucion;
	}
	public void setId_ejecucion(Integer id_ejecucion) {
		this.id_ejecucion = id_ejecucion;
	}
	public Integer getCodmes() {
		return codmes;
	}
	public void setCodmes(Integer codmes) {
		this.codmes = codmes;
	}

	public Escenario_version getEscenario_version() {
		return escenario_version;
	}

	public void setEscenario_version(Escenario_version escenario_version) {
		this.escenario_version = escenario_version;
	}

	public Integer getEstado_ajustes() {
		return estado_ajustes;
	}

	public void setEstado_ajustes(Integer estado_ajustes) {
		this.estado_ajustes = estado_ajustes;
	}
	
	public Integer getFase() {
		return fase;
	}

	public void setFase(Integer fase) {
		this.fase = fase;
	}
}
