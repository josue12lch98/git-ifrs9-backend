package sim.ws.rest;

import java.util.ArrayList;

//import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import sim.Contenedor;
import sim.persistencia.Alerta;
import sim.persistencia.EntidadProgreso;
import sim.persistencia.FiltroProgreso;
import sim.persistencia.Tarea;
import sim.persistencia.Usuario;

import sim.ws.rest.JwtRequiereToken;

@Path("/inicio")
public class InicioService {
	private Contenedor contenedor = Contenedor.obtenerInstancia();

	// Autenticar Usuario
	@POST
	@Path("/loguearUsuario")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Usuario loguearUsuario(Usuario usuario) throws Exception {
		contenedor.validarInjectObjeto(usuario, true, true);
		return contenedor.loguearUsuario(usuario);
	}

	// Obtener tareas planificadas
	@GET
	@Path("/obtenerTareas")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Tarea> obtenerTareas() throws Exception {
		return contenedor.obtenerTareas();
	}

	// Editar tareas planificadas
	@PUT
	@Path("/editarTarea")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarTarea(Tarea tarea) throws Exception {
		contenedor.validarInjectObjeto(tarea, true, true);
		contenedor.editarTarea(tarea);
	}

	// Obtener alertas del sistema (configuraciones pendientes)
	@GET
	@Path("/obtenerAlertas")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Alerta> obtenerAlertas() throws Exception {
		return contenedor.obtenerAlertas();
	}

	// Obtener el progreso de las entidad-carteras por fase
	@POST
	@Path("/obtenerEntidadesProgreso")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EntidadProgreso> obtenerEntidadesProgreso(FiltroProgreso filtroProgreso) throws Exception {
		contenedor.validarInjectObjeto(filtroProgreso, true, true);
		return contenedor.obtenerEntidadesProgreso(filtroProgreso);
	}

}
