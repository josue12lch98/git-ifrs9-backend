package sim.persistencia;

import java.util.ArrayList;

public class TablaMapeoEjecucion {
	private Integer id_mapeo_tabla;	
	private TablaInput tabla_input;
	private CargaFichero carga;
	private ArrayList<CargaFichero> opciones_carga;
	private ArrayList<Integer> opciones_codmes;
	
	//Agregamos informacion de bloque
	private Bloque bloque;
		
	public TablaMapeoEjecucion() {		
	}
	
	public Integer getId_mapeo_tabla() {
		return id_mapeo_tabla;
	}
	public void setId_mapeo_tabla(Integer id_mapeo_tabla) {
		this.id_mapeo_tabla = id_mapeo_tabla;
	}
	
	public CargaFichero getCarga() {
		return carga;
	}

	public void setCarga(CargaFichero carga) {
		this.carga = carga;
	}

	public ArrayList<CargaFichero> getOpciones_carga() {
		return opciones_carga;
	}

	public void setOpciones_carga(ArrayList<CargaFichero> opciones_carga) {
		this.opciones_carga = opciones_carga;
	}

	public TablaInput getTabla_input() {
		return tabla_input;
	}

	public void setTabla_input(TablaInput tabla_input) {
		this.tabla_input = tabla_input;
	}

	public ArrayList<Integer> getOpciones_codmes() {
		return opciones_codmes;
	}

	public void setOpciones_codmes(ArrayList<Integer> opciones_codmes) {
		this.opciones_codmes = opciones_codmes;
	}

	public Bloque getBloque() {
		return bloque;
	}

	public void setBloque(Bloque bloque) {
		this.bloque = bloque;
	}	
}
