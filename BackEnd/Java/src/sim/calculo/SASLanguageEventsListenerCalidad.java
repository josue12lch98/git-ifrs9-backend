package sim.calculo;

import com.sas.iom.SAS.IWorkspace;
import com.sas.iom.SASEvents.*;
import com.sas.services.connection.ConnectionInterface;

import sim.persistencia.CargaFichero;
import sim.persistencia.bd.ModuloDatos;

public class SASLanguageEventsListenerCalidad  extends _ILanguageEventsImplBase {

	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(SASLanguageEventsListenerCalidad.class);
	private String idWorkspace;
	private ConnectionInterface cx;
	private ModuloDatos md;
	private String SAS_rutalocal; 
	private Boolean finalizado; 
	private CargaFichero cargaFichero;
	
	public SASLanguageEventsListenerCalidad(CargaFichero cargaFichero, ConnectionInterface cx, IWorkspace iWorkspace, ModuloDatos md, String SAS_rutalocal) {
		this.cx = cx;
		idWorkspace=iWorkspace.UniqueIdentifier();
		this.md=md;
		this.finalizado=false;
		this.SAS_rutalocal=SAS_rutalocal;
		this.cargaFichero=cargaFichero;
		LOGGER.info("Listener SAS creado workspace " + idWorkspace);
	}
	
	// implement declared methods in com.sas.iom.SASEvents.ILanguageEvents
	public void ProcStart(java.lang.String procname) {
		/* sin evento */ 
	}
	public void SubmitComplete(int sasrc) { 
		if (finalizado) {
			if (!SAS_rutalocal.equals("") ) {
				//importamos las tablas generadas en SAS
				try {
					//md.leerCSV(SAS_rutalocal,"HE_OutputFicheroReglaCalidad","HE_OutputFicheroReglaCalidad");
					//md.leerCSV(SAS_rutalocal,"HE_OutputFicheroTotal","HE_OutputFicheroTotal");
					//md.leerCSV(SAS_rutalocal,"HE_OutputHistoCalidad","HE_OutputHistoCalidad");
					//md.leerCSV(SAS_rutalocal,"HE_OutputEvidCalidad","HE_OutputEvidCalidad");
				} catch (Exception e) {
					LOGGER.error("Error  al importar tablas generadas en SAS: " + e.getMessage());
				}
			}
			
			try {
				cargaFichero.setTipoEstadoCarga(1);
				md.editarCargaFichero(cargaFichero);
			} catch (Exception e) {
				LOGGER.error("Error al actualizar CargaFichero luego de proceso de calidad: " + e.getMessage());
			}
			
			LOGGER.info("Submit final completado, se cierra workspace " + idWorkspace);
			cx.close();
			
		} else {
			LOGGER.info("Submit completado, ejecutando siguiente.");
		}
	}
	public void ProcComplete(java.lang.String procname) {
		//el proc format indicará el fin de la ejecución
		if (procname.equalsIgnoreCase("FORMAT")) {
			finalizado=true;
		}
	}
	public void DatastepStart() { 
		/* sin evento */ 
	}
	public void DatastepComplete() { 
		/* sin evento */ 
	}
	public void StepError() { 
		try {
			cargaFichero.setTipoEstadoCarga(4);
			md.editarCargaFichero(cargaFichero);
		} catch (Exception e) {
			LOGGER.error("Error al actualizar CargaFichero luego de proceso de calidad: " + e.getMessage());
		}
		
		LOGGER.error("Error en ejecución, se cierra workspace " + idWorkspace);
		cx.close();
	}

}
