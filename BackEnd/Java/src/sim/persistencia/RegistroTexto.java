package sim.persistencia;

public class RegistroTexto extends Registro {
	
	private String texto;
	
	public RegistroTexto(Integer orden, String texto) {
		super(orden);
		this.texto = texto;
	}

	@Override
	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	@Override
	public Double getNumero() throws Exception {
		throw new Exception("Metodo getNumero no pertenece a esta clase RegistroTexto");
	}

}
