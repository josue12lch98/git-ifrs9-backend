package sim.persistencia.dic;


public class TipoMetodologia {
	private Integer id_tipo_metodologia;
	private String nombre_tipo_metodologia;
	private String nombre_variablemotor;
	private TipoAmbito tipo_ambito;
	
	public TipoMetodologia() {
	}
	
	public Integer getId_tipo_metodologia() {
		return id_tipo_metodologia;
	}
	public void setId_tipo_metodologia(Integer id_tipo_metodologia) {
		this.id_tipo_metodologia = id_tipo_metodologia;
	}
	public String getNombre_tipo_metodologia() {
		return nombre_tipo_metodologia;
	}
	public void setNombre_tipo_metodologia(String nombre_tipo_metodologia) {
		this.nombre_tipo_metodologia = nombre_tipo_metodologia;
	}
	public String getNombre_variablemotor() {
		return nombre_variablemotor;
	}
	public void setNombre_variablemotor(String nombre_variablemotor) {
		this.nombre_variablemotor = nombre_variablemotor;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}

	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}
}
