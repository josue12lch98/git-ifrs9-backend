package sim;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jboss.logging.Logger;
import sim.PropertiesCompartido;

@SuppressWarnings("rawtypes")
public class LoggerWrapper {

    public StatusError status = sim.StatusError.getInstance();
    private DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private final String name;
    private Logger log;
    private DateFormat df_fileName = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * Creates a new instance of LoggerWrapper
     */
    protected LoggerWrapper(String name) {
        this.name = name;
        log = Logger.getLogger(name);
    }

	protected LoggerWrapper(Class clazz) {
        this.name = clazz.getName();
        log = Logger.getLogger(clazz);
    }

    public String getName() {
        return name;
    }

    public boolean isDebugEnabled() {
        return log.isDebugEnabled();
    }

    public void debug(Object message) {
        Date d = new Date();
        log.debug("(" + df.format(d) + ") - " + message);
    }

    public void debug(Object message, Throwable t) {
        Date d = new Date();
        log.debug("(" + df.format(d) + ") - " + message, t);
    }

    public boolean isInfoEnabled() {
    	return log.isInfoEnabled();
    }

    public void info(Object message) {
        Date d = new Date();
        log.info("(" + df.format(d) + ") - " + message);
        status.INFO();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogs("INFO: " + "[" + this.name + "] " + message);
    }
    public void infoSeg(Object message) {
        Date d = new Date();
        log.info("(" + df.format(d) + ") - " + message);
        status.INFO();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogsSeg("INFO: " + "[" + this.name + "] " + message);
    }
    public void info(Object message, Throwable t) {
        Date d = new Date();
        log.info("(" + df.format(d) + ") - " + message, t);
        status.INFO();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogs("INFO: " + "[" + this.name + "] " + message);
    }
    public void infoSeg(Object message, Throwable t) {
        Date d = new Date();
        log.info("(" + df.format(d) + ") - " + message, t);
        status.INFO();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogsSeg("INFO: " + "[" + this.name + "] " + message);
    }
    public void warn(Object message) {
        Date d = new Date();
        log.warn("(" + df.format(d) + ") - " + message);
        status.WARNING();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogs("WARN: " + "[" + this.name + "] " + message);
    }
    public void warnSeg(Object message) {
        Date d = new Date();
        log.warn("(" + df.format(d) + ") - " + message);
        status.WARNING();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogsSeg("WARN: " + "[" + this.name + "] " + message);
    }
    public void warn(Object message, Throwable t) {
        Date d = new Date();
        log.warn("(" + df.format(d) + ") - " + message, t);
        status.WARNING();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogs("WARN: " + "[" + this.name + "] " + message);
    }
    public void warnSeg(Object message, Throwable t) {
        Date d = new Date();
        log.warn("(" + df.format(d) + ") - " + message, t);
        status.WARNING();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogsSeg("WARN: " + "[" + this.name + "] " + message);
    }
    public void error(Object message) {
        Date d = new Date();
        log.error("(" + df.format(d) + ") - " + message);
        status.ERROR();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogs("ERROR: " + "[" + this.name + "] " + message);
    }
    public void errorSeg(Object message) {
        Date d = new Date();
        log.error("(" + df.format(d) + ") - " + message);
        status.ERROR();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogsSeg("ERROR: " + "[" + this.name + "] " + message);
    }
    public void error(Object message, Throwable t) {
        Date d = new Date();
        log.error("(" + df.format(d) + ") - " + message, t);
        status.ERROR();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogs("ERROR: " + "[" + this.name + "] " + message);
    }
    public void errorSeg(Object message, Throwable t) {
        Date d = new Date();
        log.error("(" + df.format(d) + ") - " + message, t);
        status.ERROR();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogsSeg("ERROR: " + "[" + this.name + "] " + message);
    }
    public void fatal(Object message) {
        Date d = new Date();
        log.fatal("(" + df.format(d) + ") - " + message);
        status.FATAL();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogs("FATAL: " + "[" + this.name + "] " + message);
    }
    public void fatalSeg(Object message) {
        Date d = new Date();
        log.fatal("(" + df.format(d) + ") - " + message);
        status.FATAL();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogsSeg("FATAL: " + "[" + this.name + "] " + message);
    }
    public void fatal(Object message, Throwable t) {
        Date d = new Date();
        log.fatal("(" + df.format(d) + ") - " + message, t);
        status.FATAL();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogs("FATAL: " + "[" + this.name + "] " + message);
    }
    public void fatalSeg(Object message, Throwable t) {
        Date d = new Date();
        log.fatal("(" + df.format(d) + ") - " + message, t);
        status.FATAL();
        
        //Escribimos sobre archivo de logs
        this.escribirArchivoLogsSeg("FATAL: " + "[" + this.name + "] " + message);
    }
    public static LoggerWrapper getLogger(String name) {
        LoggerWrapper logger = new LoggerWrapper(name);
        return logger;
    }

    public static LoggerWrapper getLogger(Class clazz) {
        LoggerWrapper logger = new LoggerWrapper(clazz);
        return logger;
    }
    
    // ------- Registro eventos en archivo de logs -------- //	
    protected void escribirArchivoLogs(String msg) {  
 		// Creamos el archivo en caso no exista
    	if (PropertiesCompartido.ruta_logs != null) {
    		Date d = new Date();
    		String fileEdit = PropertiesCompartido.ruta_logs + "\\Logs_App\\" + df_fileName.format(d) + ".txt";		
     		
     		PrintWriter writer = null;     		
     		try { 	
     			new File(PropertiesCompartido.ruta_logs + "\\Logs_App").mkdirs();
     	 		new File(fileEdit).createNewFile();
     			writer = new PrintWriter(new FileOutputStream(fileEdit, true));
     			writer.println(df.format(d) + " " + msg); 			
     		} catch(Exception ex) {
     			 log.error("(" + df.format(d) + ") - " + ex.getMessage());
     		} finally {
     			if(writer!=null) writer.close();
     		}
    	}
    }
    protected void escribirArchivoLogsSeg(String msg) {  
 		// Creamos el archivo en caso no exista
    	if (PropertiesCompartido.ruta_logs != null) {
    		Date d = new Date();
    		String fileEdit = PropertiesCompartido.ruta_logs + "\\Logs_Seg\\" + df_fileName.format(d) + ".txt";		
     		
     		PrintWriter writer = null;     		
     		try { 	
     			new File(PropertiesCompartido.ruta_logs + "\\Logs_Seg").mkdirs();
     	 		new File(fileEdit).createNewFile();
     			writer = new PrintWriter(new FileOutputStream(fileEdit, true));
     			writer.println(df.format(d) + " " + msg); 			
     		} catch(Exception ex) {
     			 log.error("(" + df.format(d) + ") - " + ex.getMessage());
     		} finally {
     			if(writer!=null) writer.close();
     		}
    	}
    }
}