package sim.persistencia;

import java.util.ArrayList;

public class PrimerPasoConfigurado {
	private Integer id_primerPaso;
	private String nombre_primerPaso;
	private String codigo_primerPaso;
	private ArrayList<Bloque> bloques_primerPaso;		
	
	public PrimerPasoConfigurado() {		
	}
	
	public ArrayList<Bloque> getBloques_primerPaso() {
		return bloques_primerPaso;
	}
	public void setBloques_primerPaso(ArrayList<Bloque> bloques_primerPaso) {
		this.bloques_primerPaso = bloques_primerPaso;
	}
	public String getCodigo_primerPaso() {
		return codigo_primerPaso;
	}
	public void setCodigo_primerPaso(String codigo_primerPaso) {
		this.codigo_primerPaso = codigo_primerPaso;
	}

	public String getNombre_primerPaso() {
		return nombre_primerPaso;
	}

	public void setNombre_primerPaso(String nombre_primerPaso) {
		this.nombre_primerPaso = nombre_primerPaso;
	}

	public Integer getId_primerPaso() {
		return id_primerPaso;
	}

	public void setId_primerPaso(Integer id_primerPaso) {
		this.id_primerPaso = id_primerPaso;
	}
}
