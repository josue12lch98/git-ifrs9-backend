package sim.persistencia;

import java.util.ArrayList;

public class CarteraVersion_staging extends Cartera_version {
	private Boolean stage_interno;
	private Boolean stage_externo;
	private Boolean stage_sensibilidad;
	private Integer meses_sensibilidad;
	private Boolean flag_expresion;
	private Boolean flag_regla;
	private TablaInput tablaInputExpresiones;
	private ArrayList<Regla_Staging> reglasStaging;
	private ArrayList<Expresion_Staging> expresionesStaging;
	
	private ArrayList<JuegoEscenarios_version> juegos_versionesEscenario;

	private Boolean conDependencias;
	private Boolean conDependenciasOficial;
	
	public CarteraVersion_staging() {
		super();
	}

	public Boolean getStage_interno() {
		return stage_interno;
	}

	public void setStage_interno(Boolean stage_interno) {
		this.stage_interno = stage_interno;
	}

	public Boolean getStage_externo() {
		return stage_externo;
	}

	public void setStage_externo(Boolean stage_externo) {
		this.stage_externo = stage_externo;
	}

	public Boolean getStage_sensibilidad() {
		return stage_sensibilidad;
	}

	public void setStage_sensibilidad(Boolean stage_sensibilidad) {
		this.stage_sensibilidad = stage_sensibilidad;
	}

	public Integer getMeses_sensibilidad() {
		return meses_sensibilidad;
	}

	public void setMeses_sensibilidad(Integer meses_sensibilidad) {
		this.meses_sensibilidad = meses_sensibilidad;
	}

	public Boolean getFlag_expresion() {
		return flag_expresion;
	}

	public void setFlag_expresion(Boolean flag_expresion) {
		this.flag_expresion = flag_expresion;
	}

	public Boolean getFlag_regla() {
		return flag_regla;
	}

	public void setFlag_regla(Boolean flag_regla) {
		this.flag_regla = flag_regla;
	}

	public ArrayList<Regla_Staging> getReglasStaging() {
		return reglasStaging;
	}

	public void setReglasStaging(ArrayList<Regla_Staging> reglasStaging) {
		this.reglasStaging = reglasStaging;
	}

	public ArrayList<Expresion_Staging> getExpresionesStaging() {
		return expresionesStaging;
	}

	public void setExpresionesStaging(ArrayList<Expresion_Staging> expresionesStaging) {
		this.expresionesStaging = expresionesStaging;
	}

	public TablaInput getTablaInputExpresiones() {
		return tablaInputExpresiones;
	}

	public void setTablaInputExpresiones(TablaInput tablaInputExpresiones) {
		this.tablaInputExpresiones = tablaInputExpresiones;
	}

	public ArrayList<JuegoEscenarios_version> getJuegos_versionesEscenario() {
		return juegos_versionesEscenario;
	}

	public void setJuegos_versionesEscenario(ArrayList<JuegoEscenarios_version> juegos_versionesEscenario) {
		this.juegos_versionesEscenario = juegos_versionesEscenario;
	}

	public Boolean getConDependencias() {
		return conDependencias;
	}

	public void setConDependencias(Boolean conDependencias) {
		this.conDependencias = conDependencias;
	}

	public Boolean getConDependenciasOficial() {
		return conDependenciasOficial;
	}

	public void setConDependenciasOficial(Boolean conDependenciasOficial) {
		this.conDependenciasOficial = conDependenciasOficial;
	}

}
