package sim.persistencia.dic;

import sim.persistencia.PrimerPaso;

public class TipoBloque {
	private Integer id_tipo_bloque;
	private String nombre_tipo_bloque;
	private String nombre_variablemotor;
	private Boolean flag_bucket;
	private Boolean flag_modMacro;
	private TipoEjecucion tipo_ejecucion;
	private PrimerPaso primerPaso;
	private TipoObtencionResultados tipo_obtencion_resultados;	
	
	public TipoBloque() {
	}

	public Integer getId_tipo_bloque() {
		return id_tipo_bloque;
	}

	public void setId_tipo_bloque(Integer id_tipo_bloque) {
		this.id_tipo_bloque = id_tipo_bloque;
	}

	public String getNombre_tipo_bloque() {
		return nombre_tipo_bloque;
	}

	public void setNombre_tipo_bloque(String nombre_tipo_bloque) {
		this.nombre_tipo_bloque = nombre_tipo_bloque;
	}

	public Boolean getFlag_bucket() {
		return flag_bucket;
	}

	public void setFlag_bucket(Boolean flag_bucket) {
		this.flag_bucket = flag_bucket;
	}	

	public String getNombre_variablemotor() {
		return nombre_variablemotor;
	}

	public void setNombre_variablemotor(String nombre_variablemotor) {
		this.nombre_variablemotor = nombre_variablemotor;
	}
	
	public PrimerPaso getPrimerPaso() {
		return primerPaso;
	}

	public void setPrimerPaso(PrimerPaso primerPaso) {
		this.primerPaso = primerPaso;
	}

	public TipoEjecucion getTipo_ejecucion() {
		return tipo_ejecucion;
	}

	public void setTipo_ejecucion(TipoEjecucion tipo_ejecucion) {
		this.tipo_ejecucion = tipo_ejecucion;
	}

	public TipoObtencionResultados getTipo_obtencion_resultados() {
		return tipo_obtencion_resultados;
	}

	public void setTipo_obtencion_resultados(TipoObtencionResultados tipo_obtencion_resultados) {
		this.tipo_obtencion_resultados = tipo_obtencion_resultados;
	}

	public Boolean getFlag_modMacro() {
		return flag_modMacro;
	}

	public void setFlag_modMacro(Boolean flag_modMacro) {
		this.flag_modMacro = flag_modMacro;
	}
	
}
