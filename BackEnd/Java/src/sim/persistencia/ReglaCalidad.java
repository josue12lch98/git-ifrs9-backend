package sim.persistencia;

import sim.persistencia.dic.TipoIndicador;

public class ReglaCalidad {
	
	private Integer id_segmento;
	private Integer id_campoaprov;
	private TipoIndicador tipo_indicador;
	private Integer tipo_analisis;
	private Double limite_inferior_rojo;
	private Double limite_inferior_ambar;
	private Double limite_superior_ambar;
	private Double limite_superior_rojo;
	private Boolean flag_aplica;

	//Se inserta este valor solo para select de obtenerReglasCalidad
	private Integer id_tipocampo;
	//Constructor
	public ReglaCalidad() {
		tipo_indicador = new TipoIndicador();
	}
	
	public ReglaCalidad(TipoIndicador tipo_indicador) {
		this.tipo_indicador = tipo_indicador;
	}

	public ReglaCalidad(Integer id_segmento, Integer id_campoaprov, TipoIndicador tipo_indicador, Integer id_tipoanalisis) {
		this.id_segmento= id_segmento;
		this.id_campoaprov= id_campoaprov;
		this.tipo_indicador = tipo_indicador;
		this.tipo_analisis = id_tipoanalisis;
	}



	//Getters y Setters
	public TipoIndicador getTipo_indicador() {
		return tipo_indicador;
	}
	public void setTipo_indicador(TipoIndicador tipo_indicador) {
		this.tipo_indicador = tipo_indicador;
	}

	public Double getLimite_inferior_rojo() {
		return limite_inferior_rojo;
	}
	public void setLimite_inferior_rojo(Double limite_inferior_rojo) {
		this.limite_inferior_rojo = limite_inferior_rojo;
	}

	public Double getLimite_inferior_ambar() {
		return limite_inferior_ambar;
	}
	public void setLimite_inferior_ambar(Double limite_inferior_ambar) {
		this.limite_inferior_ambar = limite_inferior_ambar;
	}

	public Double getLimite_superior_ambar() {
		return limite_superior_ambar;
	}
	public void setLimite_superior_ambar(Double limite_superior_ambar) {
		this.limite_superior_ambar = limite_superior_ambar;
	}

	public Double getLimite_superior_rojo() {
		return limite_superior_rojo;
	}
	public void setLimite_superior_rojo(Double limite_superior_rojo) {
		this.limite_superior_rojo = limite_superior_rojo;
	}

	public Boolean getFlag_aplica() {
		return flag_aplica;
	}
	public void setFlag_aplica(Boolean flag_aplica) {
		this.flag_aplica = flag_aplica;
	}

	public Integer getId_segmento() {
		return id_segmento;
	}

	public void setId_segmento(Integer id_segmento) {
		this.id_segmento = id_segmento;
	}

	public Integer getId_campoaprov() {
		return id_campoaprov;
	}

	public void setId_campoaprov(Integer id_campoaprov) {
		this.id_campoaprov = id_campoaprov;
	}

	public Integer getTipo_analisis() {
		return tipo_analisis;
	}

	public void setTipo_analisis(Integer tipo_analisis) {
		this.tipo_analisis = tipo_analisis;
	}

	public Integer getId_tipocampo() {
		return id_tipocampo;
	}

	public void setId_tipocampo(Integer id_tipocampo) {
		this.id_tipocampo = id_tipocampo;
	}

	
	
}