package sim.ws.rest;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import sim.ws.rest.JwtRequiereToken;
import sim.ws.rest.JwtRefrescaToken;

import sim.persistencia.*;
import sim.Contenedor;

@Path("/aprovisionamiento")
public class AprovisionamientoService {

	private static Contenedor contenedor = Contenedor.obtenerInstancia();

	// ------------- Configurar Aprovisionamiento -----------------
	// ------------------------- Rutas ----------------------------

	@GET
	@Path("/obtenerRutas/{id_tipo_ambito}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Ruta> obtenerRutas(@PathParam("id_tipo_ambito") Integer id_tipo_ambito) throws Exception {
		return contenedor.obtenerRutas(id_tipo_ambito);
	}

	@POST
	@Path("/crearRuta")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void crearRuta(Ruta ruta) throws Exception {
		contenedor.validarInjectObjeto(ruta, true, true);
		contenedor.validarRutaSegura(ruta.getRuta());
		contenedor.crearRuta(ruta);
	}

	@PUT
	@Path("/editarRuta")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public RutaValida editarRuta(Ruta ruta) throws Exception {
		contenedor.validarInjectObjeto(ruta, true, true);
		contenedor.validarRutaSegura(ruta.getRuta());
		return contenedor.editarRuta(ruta);
	}

	@DELETE
	@Path("/borrarRuta/{id_ruta}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarRuta(@PathParam("id_ruta") Integer id_ruta) throws Exception {
		contenedor.borrarRuta(id_ruta);
	}

	@PUT
	@Path("/seleccionarRutas")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void seleccionarRutas(ArrayList<Ruta> rutas) throws Exception {
		contenedor.validarInjectObjeto(rutas, true, true);
		contenedor.seleccionarRutas(rutas);
	}

	// ------------- Configurar Aprovisionamiento -----------------
	// ------------------- Aprovisionamiento ----------------------

	@POST
	@Path("/obtenerFicheroConCampos")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Aprovisionamiento obtenerFicheroConCampos(Aprovisionamiento fichero) throws Exception {
		return contenedor.obtenerFicheroConCampos(fichero);
	}

	@POST
	@Path("/crearAprovisionamiento")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearAprovisionamiento(Aprovisionamiento fichero) throws Exception {
		contenedor.crearAprovisionamiento(fichero);
	}

	@PUT
	@Path("/editarAprovisionamiento")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarAprovisionamiento(Aprovisionamiento aprovisionamiento) throws Exception {
		contenedor.editarAprovisionamiento(aprovisionamiento);
	}

	@DELETE
	@Path("/borrarAprovisionamiento/{id_aprovisionamiento}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarAprovisionamiento(@PathParam("id_aprovisionamiento") Integer id_aprovisionamiento)
			throws Exception {
		contenedor.borrarAprovisionamiento(id_aprovisionamiento);
	}

	// -------------- Proceso de aprovisionar --------------------------

	@GET
	@Path("/obtenerCargasFicheros/{id_tipo_ambito}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<CargaFichero> obtenerCargasFicheros(@PathParam("id_tipo_ambito") Integer id_tipo_ambito)
			throws Exception {
		return contenedor.obtenerCargasFicheros(id_tipo_ambito);
	}

	// -----------------------------------------------------------------

	@PUT
	@Path("/EditarFichero")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarFichero(Aprovisionamiento aprov) throws Exception {
		contenedor.editarFichero(aprov);
	}

	// CRUD Aprovisionamiento-Fichero
	@GET
	@Path("/obtenerAprovisionamientoInput")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Aprovisionamiento> obtenerAprovisionamientoInput() throws Exception {
		return contenedor.obtenerAprovisionamientoInput();
	}

	@POST
	@Path("/obtenerCamposAprovisionamiento")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<CampoAprovisionamiento> obtenerCampos(Aprovisionamiento aprov) throws Exception {
		return contenedor.obtenerCampos(aprov);
	}

	@PUT
	@Path("/EditarComparacionVersion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarComparacionVersion(Aprovisionamiento fichero) throws Exception {
		contenedor.editarNuevosCampos(fichero);
	}

	@POST
	@Path("/procesarCargaFichero")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CargaFichero procesarCargaFichero(CargaFichero cargaFichero) throws Exception {
		contenedor.validarInjectObjeto(cargaFichero, true, true);
		return contenedor.procesarCargaFichero(cargaFichero);
	}

	@PUT
	@Path("/editarCargaFichero")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarCargaFichero(CargaFichero cargaFichero) throws Exception {
		contenedor.validarInjectObjeto(cargaFichero, true, true);
		contenedor.editarCargaFichero(cargaFichero);
	}

	@POST
	@Path("/aprobarCargaFichero")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void aprobarCargaFichero(CargaFichero cargaFichero) throws Exception {
		//contenedor.validarInjectObjeto(cargaFichero, true, false);
		contenedor.aprobarCargaFichero(cargaFichero);
	}

	@POST
	@Path("/rechazarCargaFichero")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void rechazarCargaFichero(CargaFichero cargaFichero) throws Exception {
	//	contenedor.validarInjectObjeto(cargaFichero, true, true);
		contenedor.rechazarCargaFichero(cargaFichero);
	}

	@POST
	@Path("/actualizarEstadoCargaCalidad")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<CargaFichero> actualizarEstadoCargaCalidad(ArrayList<CargaFichero> cargasFichero)
			throws Exception {
		contenedor.validarInjectObjeto(cargasFichero, true, true);
		return contenedor.obtenerCargasFicheroTotales();
	}

	@GET
	@Path("/obtenerCargasFicheroTotales")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<CargaFichero> obtenerCargasFicheroTotales() throws Exception {
		return contenedor.obtenerCargasFicheroTotales();
	}

	@POST
	@Path("/obtenerRegistrosdeCargaFichero")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<AjusteFilaInput> obtenerRegistrosdeCargaFichero(CargaFichero cargaFichero) throws Exception {
		contenedor.validarInjectObjeto(cargaFichero, true, true);
		return contenedor.obtenerRegistrosdeCargaFichero(cargaFichero);
	}

	@PUT
	@Path("/editarRegistrodeCargaFichero")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarRegistrodeCargaFichero(AjusteFilaInput ajusteFilaInput) throws Exception {
		contenedor.validarInjectObjeto(ajusteFilaInput, true, true);
		contenedor.editarRegistrodeCargaFichero(ajusteFilaInput);
	}

	@POST
	@Path("/crearRegistrodeCargaFichero")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void crearRegistrodeCargaFichero(AjusteFilaInput ajusteFilaInput) throws Exception {
		contenedor.validarInjectObjeto(ajusteFilaInput, true, true);
		contenedor.crearRegistrodeCargaFichero(ajusteFilaInput);
	}

	@POST
	@Path("/borrarRegistrosdeCargaFichero")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void borrarRegistrosdeCargaFichero(ArrayList<AjusteFilaInput> ajustesFilaInput) throws Exception {
		contenedor.validarInjectObjeto(ajustesFilaInput, true, true);
		contenedor.borrarRegistrosdeCargaFichero(ajustesFilaInput);
	}

	@PUT
	@Path("/editarNuevoCampo")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarNuevosCampos(Aprovisionamiento fichero) throws Exception {
		contenedor.validarInjectObjeto(fichero, true, true);
		contenedor.editarNuevosCampos(fichero);
	}

	@DELETE
	@Path("/borrarSegmento/{id_segmento}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarCampoSegmento(@PathParam("id_segmento") Integer id_segmento) throws Exception {
		contenedor.borrarCampoSegmento(id_segmento);
	}

	@PUT
	@Path("/editarCamposAprovisionamientoSegmento")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarCamposAprovisionamientoSegmento(Aprovisionamiento aprov) throws Exception {
		contenedor.validarInjectObjeto(aprov, true, true);
		contenedor.editarCamposAprovisionamiento(aprov);
		//contenedor.limpiarCamposSegmento(aprov);
	}

	@PUT
	@Path("/editarCamposSegmento")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarCamposSegmento(Aprovisionamiento aprov) throws Exception {
		contenedor.validarInjectObjeto(aprov, true, true);
		contenedor.editarCamposSegmento(aprov);
	}

	@PUT
	@Path("/editarSegmento")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarCampoSegmento(SegmentoAprovisionamiento segmento) throws Exception {
		contenedor.validarInjectObjeto(segmento, true, true);
		contenedor.editarCampoSegmento(segmento);
	}

	@PUT
	@Path("/editarReglasSegmento")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarReglasSegmento(SegmentoAprovisionamiento segmento) throws Exception {
		contenedor.validarInjectObjeto(segmento, true, true);
		contenedor.editarReglasSegmento(segmento);
	}

	@PUT
	@Path("/editarCamposAprovisionamiento")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarCamposAprovisionamiento(Aprovisionamiento aprov) throws Exception {
		contenedor.validarInjectObjeto(aprov, true, true);
		contenedor.editarCamposAprovisionamiento(aprov);
	}

	@POST
	@Path("/obtenerReglasCalidadPorCampo")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<ReglaCalidad> obtenerReglasCalidadPorCampo(CampoAprovisionamiento campo) throws Exception {
		contenedor.validarInjectObjeto(campo, true, false); //Formula podria contener comparativos
		return contenedor.obtenerReglasCalidadPorCampo(campo);
	}

	@GET
	@Path("/obtenerSegmentosAprov/{id_aprov}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<SegmentoAprovisionamiento> obtenerSegmentosPorAprov(@PathParam("id_aprov") Integer id_aprov)
			throws Exception {
		return contenedor.obtenerSegmentosPorAprov(id_aprov);
	}

	@GET
	@Path("/obtenerSegmentosPorCampoAprov/{id_campoaprov}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<SegmentoAprovisionamiento> obtenerSegmentosPorCampoAprov(
			@PathParam("id_campoaprov") Integer id_campoaprov) throws Exception {
		return contenedor.obtenerSegmentosPorCampoAprov(id_campoaprov);
	}

	@POST
	@Path("/obtenerReglasCalidad")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<ReglaCalidad> obtenerReglasCalidad(ReglaCalidad regla) throws Exception {
		contenedor.validarInjectObjeto(regla, true, true);
		return contenedor.obtenerReglasCalidad(regla);
	}

	@PUT
	@Path("/replicarReglasCalidad")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void replicarReglasCampo(ArrayList<CampoAprovisionamiento> campos) throws Exception {
		contenedor.validarInjectObjeto(campos, true, false); //Formula podria contener comparativos
		contenedor.replicarReglasCampo(campos);
	}

	@PUT
	@Path("/editarReglasCalidad")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarReglasCalidad(ArrayList<ReglaCalidad> reglas) throws Exception {
		contenedor.validarInjectObjeto(reglas, true, true);
		contenedor.editarReglasCalidad(reglas);
	}

	@PUT
	@Path("/editarReglaCalidad")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarReglaCalidad(ReglaCalidad regla) throws Exception {
		contenedor.validarInjectObjeto(regla, true, true);
		contenedor.editarReglaCalidad(regla);
	}
	@POST
	@Path("/obtenerCamposporTablaInputVersion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public CargaFichero obtenerCamposporTablaInputVersion(CargaFichero cargaFichero) throws Exception {
		
		
		cargaFichero = contenedor.obtenerCamposporTablaInputVersion(cargaFichero);
		
		return cargaFichero;
	}
}
