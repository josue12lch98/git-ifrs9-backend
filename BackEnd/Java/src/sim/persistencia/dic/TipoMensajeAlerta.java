package sim.persistencia.dic;

public class TipoMensajeAlerta {
	private Integer id_tipo_mensaje_alerta;
	private String nombre_tipo_mensaje_alerta;
	
	public TipoMensajeAlerta() {
	}

	public Integer getId_tipo_mensaje_alerta() {
		return id_tipo_mensaje_alerta;
	}

	public void setId_tipo_mensaje_alerta(Integer id_tipo_mensaje_alerta) {
		this.id_tipo_mensaje_alerta = id_tipo_mensaje_alerta;
	}

	public String getNombre_tipo_mensaje_alerta() {
		return nombre_tipo_mensaje_alerta;
	}

	public void setNombre_tipo_mensaje_alerta(String nombre_tipo_mensaje_alerta) {
		this.nombre_tipo_mensaje_alerta = nombre_tipo_mensaje_alerta;
	}

	
}
