package sim.persistencia;

public class CampoBloque extends Campo {
	
	private Integer id_bloque;
	private String variable_motor;

	//Constructor
	public CampoBloque() {
		super();
	}

	//Getters y Setters
	public Integer getId_bloque() {
		return id_bloque;
	}
	public void setId_bloque(Integer id_bloque) {
		this.id_bloque = id_bloque;
	}

	public String getVariable_motor() {
		return variable_motor;
	}
	public void setVariable_motor(String variable_motor) {
		this.variable_motor = variable_motor;
	}

}