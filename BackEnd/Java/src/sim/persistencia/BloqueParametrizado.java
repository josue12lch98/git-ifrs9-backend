package sim.persistencia;

import java.util.ArrayList;

public class BloqueParametrizado extends Bloque {
	private Integer id_mapeo_tabla;
	private Cartera_version cartera_version;
	private Escenario_version escenario_version;
	private String filtro_tabla;
	private Boolean flag_tabla_input;
	private TablaInput tabla_input;
	private ArrayList<CampoParametrizado> campos_parametrizado;

	//Unicamente para ejecuciones
	private CargaFichero carga;
	
	public BloqueParametrizado() {		
	}	
	public Integer getId_mapeo_tabla() {
		return id_mapeo_tabla;
	}
	public void setId_mapeo_tabla(Integer id_mapeo_tabla) {
		this.id_mapeo_tabla = id_mapeo_tabla;
	}	
	public Escenario_version getEscenario_version() {
		return escenario_version;
	}
	public void setEscenario_version(Escenario_version escenario_version) {
		this.escenario_version = escenario_version;
	}
	public String getFiltro_tabla() {
		return filtro_tabla;
	}
	public void setFiltro_tabla(String filtro_tabla) {
		this.filtro_tabla = filtro_tabla;
	}
	public Boolean getFlag_tabla_input() {
		return flag_tabla_input;
	}
	public void setFlag_tabla_input(Boolean flag_tabla_input) {
		this.flag_tabla_input = flag_tabla_input;
	}
	public TablaInput getTabla_input() {
		return tabla_input;
	}
	public void setTabla_input(TablaInput tabla_input) {
		this.tabla_input = tabla_input;
	}
	public ArrayList<CampoParametrizado> getCampos_parametrizado() {
		return campos_parametrizado;
	}
	public void setCampos_parametrizado(ArrayList<CampoParametrizado> campos_parametrizado) {
		this.campos_parametrizado = campos_parametrizado;
	}
	public CargaFichero getCarga() {
		return carga;
	}

	public void setCarga(CargaFichero carga) {
		this.carga = carga;
	}
	
	public Cartera_version getCartera_version() {
		return cartera_version;
	}
	public void setCartera_version(Cartera_version cartera_version) {
		this.cartera_version = cartera_version;
	}
}
