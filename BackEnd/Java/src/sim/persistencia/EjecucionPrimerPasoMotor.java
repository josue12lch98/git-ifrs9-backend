package sim.persistencia;

import java.util.ArrayList;

public class EjecucionPrimerPasoMotor {
	private Integer id_ejecucion;
	private Integer codmes;
	private PrimerPaso primerPaso;
	private EstructuraMetodologica metodologia;
	private ArrayList<BloqueParametrizado> bloquesParam;
	
	public Integer getId_ejecucion() {
		return id_ejecucion;
	}
	public void setId_ejecucion(Integer id_ejecucion) {
		this.id_ejecucion = id_ejecucion;
	}
	public PrimerPaso getPrimerPaso() {
		return primerPaso;
	}
	public void setPrimerPaso(PrimerPaso primerPaso) {
		this.primerPaso = primerPaso;
	}
	public Integer getCodmes() {
		return codmes;
	}
	public void setCodmes(Integer codmes) {
		this.codmes = codmes;
	}
	public EstructuraMetodologica getMetodologia() {
		return metodologia;
	}
	public void setMetodologia(EstructuraMetodologica metodologia) {
		this.metodologia = metodologia;
	}
	public ArrayList<BloqueParametrizado> getBloquesParam() {
		return bloquesParam;
	}
	public void setBloquesParam(ArrayList<BloqueParametrizado> bloquesParam) {
		this.bloquesParam = bloquesParam;
	}	
}
