package sim.persistencia;

import sim.persistencia.dic.TipoCampo;

public abstract class Campo {
	
	private	Integer id_campo;
	private	String nombre_campo;
	private TipoCampo tipo_campo; //Indica si es numero o texto
	
	private Integer ancho_campo;
	
	//Constructor
	public Campo() {
		tipo_campo = new TipoCampo();
	}
	
	//Getters y Setters
	public Campo(Integer id_campo, String nombre_campo, TipoCampo tipo_campo) {
		this.id_campo = id_campo;
		this.nombre_campo = nombre_campo;
		this.tipo_campo = tipo_campo;
	}
	
	//Getters y Setters
	public Integer getId_campo() {
		return id_campo;
	}
	public void setId_campo(Integer id_campo) {
		this.id_campo = id_campo;
	}
	
	public String getNombre_campo() {
		return nombre_campo;
	}
	public void setNombre_campo(String nombre_campo) {
		this.nombre_campo = nombre_campo;
	}
	
	public TipoCampo getTipo_campo() {
		return tipo_campo;
	}
	public void setTipo_campo(TipoCampo tipo_campo) {
		this.tipo_campo = tipo_campo;
	}

	public Integer getAncho_campo() {
		return ancho_campo;
	}

	public void setAncho_campo(Integer ancho_campo) {
		this.ancho_campo = ancho_campo;
	}
	

}