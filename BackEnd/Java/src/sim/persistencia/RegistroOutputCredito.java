package sim.persistencia;

import java.util.ArrayList;

public class RegistroOutputCredito {
	
	private ArrayList<CampoValor> campos_valores;	
	private String descripcion_ajuste;	
	private String cod_operacion;
	private String cod_cliente;
	private Double valorPE_ajustado;
	private Stage stage_ajustado;
	private Double valorPD_ajustado;
	
	public RegistroOutputCredito() {		
	}

	public ArrayList<CampoValor> getCampos_valores() {
		return campos_valores;
	}

	public void setCampos_valores(ArrayList<CampoValor> campos_valores) {
		this.campos_valores = campos_valores;
	}

	public String getDescripcion_ajuste() {
		return descripcion_ajuste;
	}

	public void setDescripcion_ajuste(String descripcion_ajuste) {
		this.descripcion_ajuste = descripcion_ajuste;
	}	

	public Double getValorPE_ajustado() {
		return valorPE_ajustado;
	}

	public void setValorPE_ajustado(Double valorPE_ajustado) {
		this.valorPE_ajustado = valorPE_ajustado;
	}

	public String getCod_operacion() {
		return cod_operacion;
	}

	public void setCod_operacion(String cod_operacion) {
		this.cod_operacion = cod_operacion;
	}

	public String getCod_cliente() {
		return cod_cliente;
	}

	public void setCod_cliente(String cod_cliente) {
		this.cod_cliente = cod_cliente;
	}

	public Double getValorPD_ajustado() {
		return valorPD_ajustado;
	}

	public void setValorPD_ajustado(Double valorPD_ajustado) {
		this.valorPD_ajustado = valorPD_ajustado;
	}

	public Stage getStage_ajustado() {
		return stage_ajustado;
	}

	public void setStage_ajustado(Stage stage_ajustado) {
		this.stage_ajustado = stage_ajustado;
	}	
}
