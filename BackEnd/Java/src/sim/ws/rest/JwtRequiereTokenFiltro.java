package sim.ws.rest;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.Priorities;

import io.jsonwebtoken.Claims;
//JJWT
import io.jsonwebtoken.Jwts;
import sim.Claves;
import sim.Contenedor;
import sim.ws.rest.JwtRequiereToken;

@Provider
@JwtRequiereToken
@Priority(Priorities.AUTHENTICATION)
public class JwtRequiereTokenFiltro implements ContainerRequestFilter {
	
	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(JwtRequiereTokenFiltro.class);
	private static Contenedor contenedor = Contenedor.obtenerInstancia();

	@Override
	public void filter(ContainerRequestContext request) throws IOException {       
       String token = null;
       String headerAutenticacion = null;
       Claims claimsToken = null;
       Integer tipoError = null;       
       
        try {                  
        	// Obtenemos el header de autenticacion
    		headerAutenticacion = request.getHeaderString(HttpHeaders.AUTHORIZATION);
    		
    		// Revisa si el header es correcto
            if (headerAutenticacion == null || !headerAutenticacion.startsWith("bearer ")) {
                LOGGER.warn("#### Header invalido! : " + headerAutenticacion);
                throw new NotAuthorizedException("No se dispone de un header de autenticacion!");
            }
            
            //Validamos token            
            //Extraemos token del header
            token = headerAutenticacion.substring("Bearer".length()).trim();            
            
            claimsToken = Jwts.parser().setSigningKey(Claves.claveToken).parseClaimsJws(token).getBody();
            
            //Validamos que el token sea ultimo (registrado en DB)
            try { 
            	 if (contenedor.verificarTokenAcceso(claimsToken.getSubject(), token) == false) {
            		tipoError = 1;            		
                 	throw new Exception("Token inconsistente!");
                 } else if (contenedor.verificarExpiracionToken(claimsToken.getSubject()) == false) {                	  
                	tipoError = 2;
                	contenedor.reiniciarSesionUsuario(claimsToken.getSubject());
                  	throw new Exception("Token expirado!");
                 }
            } catch (Exception e_db) {
            	if (tipoError == null) {
            		throw new NotAuthorizedException("Error al verificar token!");
            	} else if (tipoError.equals(1)) {
            		LOGGER.warn("#### Token inconsistente!");  
            		throw new NotAuthorizedException("Token inconsistente!");            		
            	} else if (tipoError.equals(2)) {
            		LOGGER.warn("#### Token expirado!!");   
            		throw new NotAuthorizedException("Token expirado!");
            	}   
            }           
            
            LOGGER.debug("#### Token valido : " + token);

        } catch (Exception e) {
        	LOGGER.warn("#### Token invalido : " + token);        	
        	request.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        	throw e;
        } 
	}
	
}
