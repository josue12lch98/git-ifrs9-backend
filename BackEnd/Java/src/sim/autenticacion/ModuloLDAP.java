package sim.autenticacion;

import java.io.File;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Hashtable;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.ws.rs.NotAuthorizedException;

import sim.persistencia.Usuario;
import java.util.Properties;

public class ModuloLDAP {
	
	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(ModuloLDAP.class);
	
	// ldap factory and connection info
	private static final String initialContextFactory = "com.sun.jndi.ldap.LdapCtxFactory";
	private static final String securityAuthentication = "simple";
	private static String ldapUrl;
	private static Properties properties;
	//private static String rutaCacert;
	//private static String passTrustStore;
	
	// ldap service user
	private static String securityPrincipal;
	private static String securityCredentials;

    // necessary to authenticate an user
	private static String searchUserPath;
	private static final String searchUserAttributeFilter = "sAMAccountName";
	private static final String searchRoleAttributeFilter = "memberOf";
	private static String searchRoleFilter;
	private static final String searchRoleFilterD = "IFRS_"; //para desarrollo de bcp, los grupos de red de creditos son IFRS_
	private static String searchEnviroment;
	private static String searchLogs;
	
	// user domain, just in simple case
	private static String loginUserDomain;
	
	public ModuloLDAP(Properties properties) throws Exception {
		LOGGER.info("Creando ModuloLDAP");
		//se capturan las properties
		searchLogs = properties.getProperty("RutaLogs");
		securityPrincipal = properties.getProperty("BIND_USER");
		securityCredentials = properties.getProperty("BIND_ENCRIPTADO");
		searchUserPath = properties.getProperty("SEARCH_USER_PATH");
		searchRoleFilter = properties.getProperty("SEARCH_ROLE_FILTER");
		searchEnviroment = properties.getProperty("SEARCH_ENVIROMENT");
		
		
		if (searchEnviroment==null) {
			LOGGER.warn("No hay establecido un parametro de ambiente (desarrollo, calidad, produccion) para el filtro del nombre del perfil/rol/grupo");
		}
		loginUserDomain = properties.getProperty("LOGIN_USER_DOMAIN");
		ldapUrl = properties.getProperty("LDAP_URL");
		//rutaCacert = properties.getProperty("LDAP_RutaCacerts");
		//passTrustStore = properties.getProperty("TrustStore_ENCRIPTADO");
		LOGGER.info("ModuloLDAP creado");
	}
	
	/* Mi banco requiere soportar diversos url de ldap para alta disponibilidad, 
	 * dado que posiblemente no tienen un servidor balanceador
	 * 
	private LdapContext disponibilidadLDAP() {
		
		//Concatenar IFS probando los distintos URL que pueden estar separados por comas, splice al ldapURL

	}
	*/
	
	private LdapContext autenticarContextLDAP(String ldap_url, String usuario_dn, String contrasena) throws Exception {
	    // Tries to create the service context
		Hashtable<String,String> ldapProperties = new Hashtable<String,String>();
		LdapContext ldapContext = null;
		try {
			//se prueba la conexion con el ldap, utilizando el usuario del servicio
			ldapProperties.put(Context.INITIAL_CONTEXT_FACTORY, initialContextFactory);
			ldapProperties.put(Context.PROVIDER_URL, ldap_url);
			ldapProperties.put(Context.SECURITY_AUTHENTICATION, securityAuthentication);
			ldapProperties.put(Context.SECURITY_PRINCIPAL, usuario_dn);
			ldapProperties.put(Context.SECURITY_CREDENTIALS, contrasena);
			
			//En caso sea LDAPS
			if (ldap_url.toUpperCase().contains("LDAPS")) {
				ldapProperties.put(Context.SECURITY_PROTOCOL, "ssl");
				
				//Actualizamos trusted CA's?
				/*if (rutaCacert != null) {
					//System.setProperty("javax.net.ssl.keyStore", rutaCacert);
		            System.setProperty("javax.net.ssl.trustStore", rutaCacert);
				}				
				if (passTrustStore != null) {
					System.setProperty("javax.net.ssl.trustStorePassword", passTrustStore);
				}				
				//Habilitamos Debug de conexiones SSL
				System.setProperty("javax.net.debug", "all");*/
			}			
          		
			ldapContext  = new InitialLdapContext(ldapProperties,null);
			/*
			 * Un detalle a tener en cuenta es que por defecto SUN JNDI envía el control MANAGEDSAIT al LDAP. 
			 * Esto hace que no funcionen correctamente las búsquedas de miembros en grupos dinámicos. 
			 * Para que el LDAP devuelva los miembros de los grupos dinámicos hay que hacer que la JNDI de Sun no envíe el control MANAGEDSAIT, 
			 * lo cual se consigue añadiendo la propiedad java.naming.referral con valor follow al entorno.
			 */
			//dirContext.addToEnvironment("java.naming.referral","follow"); 
			LOGGER.info("Usuario autenticado mediante LDAP: " + usuario_dn);
			
		} catch (Exception ex) {
			LOGGER.error("Error al autenticar usuario mediante LDAP: " + ex.getMessage());
			//throw new NotAuthorizedException("Las credenciales no son validas!");
			throw ex;
		}
		return ldapContext;
	}
	
	private void cerrarContext(LdapContext context) {
        try {
        	if (context != null) {
        		context.close();
        	}
        } catch (Exception e) {
            LOGGER.error("Error al cerrar context del LDAP: " + e.getMessage());
        }
	}
	
    private String getCN(String cnName) {
        if (cnName != null && cnName.toUpperCase().startsWith("CN=")) {
            cnName = cnName.substring(3);
        }
        int position = cnName.indexOf(',');
        if (position == -1) {
            return cnName.trim();
        } else {
            return (cnName.substring(0, position)).trim();
        }
    }
	
	public Usuario validarUsuario(Usuario usuario, String id_usuario_desenc, String contrasena_usuario_desenc) throws Exception {
        LdapContext ldapContext = null;
        LdapContext ldapUserContext = null;
        SearchControls controls = new SearchControls();
        SimpleDateFormat dfFileName = new SimpleDateFormat("dd-MM-yyyy");
        Date hoy = new Date(System.currentTimeMillis());
        String fileEdit = null;
        String fileSeg =  searchLogs + "\\Logs_Seg\\" + dfFileName.format(hoy) + ".txt";
        PrintWriter writer = null;
        
	    try {
	    	ldapContext = autenticarContextLDAP(ldapUrl,securityPrincipal,securityCredentials);
	        if (ldapContext != null) {      	  	
	        	
	            // Obtener el DN del usuario
	        	LOGGER.info("Setea atributos a retornar, alcance de search, filtros y controles");
	        	controls.setReturningAttributes(new String[] { "distinguishedName", "name", searchRoleAttributeFilter });
	        	controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
	            
	        	String userFilter = "(" + searchUserAttributeFilter + "=" + id_usuario_desenc + ")";
	        	LOGGER.info("Filtro: " + userFilter);
	        	LOGGER.info("Search User Path: " + searchUserPath);
	            NamingEnumeration<SearchResult> answers = ldapContext.search(searchUserPath, userFilter, controls);
	            
	            if (answers.hasMoreElements()) {
	            	SearchResult result = answers.nextElement();
	            	
		            usuario.setUser_dn(result.getNameInNamespace());
		            LOGGER.info("Se obtiene el DN: " + usuario.getUser_dn());
		            
		            usuario.setNombre_usuario(result.getAttributes().get("name").toString().substring(5));
		            LOGGER.info("Se obtiene el nombre de usuario: " + usuario.getNombre_usuario());
	            
	                Attribute attr = result.getAttributes().get(searchRoleAttributeFilter);
	                //Certificación y Producción BCP
	                if (searchEnviroment!=null) {
		                for (int i=0; i < attr.size() ;i++) {	                	
		                	LOGGER.info("Atributo CN : "+getCN(attr.get(i).toString()));
		                	if(getCN(attr.get(i).toString()).contains(searchRoleFilter) && getCN(attr.get(i).toString()).contains(searchEnviroment)) {
		                		
		                		usuario.getPerfil_usuario().setNombre_perfil_ad(getCN(attr.get(i).toString()));
		                	}
		                }
		            //Desarrollo BCP
	                } else if (searchEnviroment!=null && searchEnviroment.equalsIgnoreCase("_DES")) {
		                for (int i=0; i < attr.size() ;i++) {	                	
		                	LOGGER.info("Atributo CN : "+getCN(attr.get(i).toString()));
		                	if(getCN(attr.get(i).toString()).contains(searchRoleFilterD) || getCN(attr.get(i).toString()).contains(searchRoleFilter)) {
		                		usuario.getPerfil_usuario().setNombre_perfil_ad(getCN(attr.get(i).toString()));
		                	}
		                }
		            //Mi Banco, no se diferencia por ambiente el ROL/PERFIl
	                } else {
		                for (int i=0; i < attr.size() ;i++) {	                	
		                	if(getCN(attr.get(i).toString()).startsWith(searchRoleFilterD)) {
		                		usuario.getPerfil_usuario().setNombre_perfil_ad(getCN(attr.get(i).toString()));
		                	}
		                }
	                }
		            LOGGER.info("Se obtiene y asigna el perfil: " + usuario.getPerfil_usuario().getNombre_perfil_ad());
		            Boolean credenciales_validas = false;
		            //VALIDA LA CONTRASEÑA DEL USUARIO
		            try {
		            	ldapUserContext = autenticarContextLDAP(ldapUrl,usuario.getUser_dn(),contrasena_usuario_desenc);
		            	credenciales_validas = true;
						fileEdit = fileSeg;
						new File(fileEdit).createNewFile(); // Creamos archivo en caso no exista
						writer = new PrintWriter(new FileOutputStream(fileEdit, true));
						writer.println("---------------TRANSACCION---------------");
						writer.println("***Estado***:                 Exitoso");
						writer.println("***ID del evento***:          0");
						writer.println("***Nombre del evento***:      Login");
						writer.println("***ID de usuario***:          " + id_usuario_desenc);
						writer.println("***Fecha***:                  " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
						writer.println("***Acción realizada***:       Intento de login");
						writer.println("-----------------------------------------");
						writer.close();
						
		            } catch (Exception ex1) {
		            	credenciales_validas = false;
		            	LOGGER.error("Error al autenticar forma con dn del usuario: " + ex1.getMessage());
		            }
		            
		            try {
		            	ldapUserContext = autenticarContextLDAP(ldapUrl,id_usuario_desenc+"@"+loginUserDomain,contrasena_usuario_desenc);
		            	credenciales_validas = true;
		            	fileEdit = fileSeg;
						new File(fileEdit).createNewFile(); // Creamos archivo en caso no exista
						writer = new PrintWriter(new FileOutputStream(fileEdit, true));
						writer.println("---------------TRANSACCION---------------");
						writer.println("***Estado***:                 Exitoso");
						writer.println("***ID del evento***:          0");
						writer.println("***Nombre del evento***:      Login");
						writer.println("***ID de usuario***:          " + id_usuario_desenc);
						writer.println("***Fecha***:                  " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
						writer.println("***Acción realizada***:       Intento de login");
						writer.println("-----------------------------------------");
						writer.close();
		            } catch (Exception ex1) {
		            	credenciales_validas = false;
		            	LOGGER.error("Error al autenticar forma con id@domain: " + ex1.getMessage());
		            }
		            
		            if (credenciales_validas == false) {
		            	LOGGER.error("Las credenciales no son validas");
		            	throw new NotAuthorizedException("Las credenciales no son validas");
		            } else if (usuario.getPerfil_usuario().getNombre_perfil_ad() == null || usuario.getPerfil_usuario().getNombre_perfil_ad().isEmpty()) {
		            	LOGGER.error("El usuario no tiene un perfil asignado para la solucion estrategica NIIF9 en el AD");
		            	throw new NotAuthorizedException("El usuario no tiene un perfil asignado para la solucion estrategica NIIF9 en el AD");
		            }
	            } else {
	            	throw new NotAuthorizedException("El usuario no se encuentra en el AD");
	            }
	        } else {
				LOGGER.fatal("No se ha logrado autenticar al usuario generico de la aplicacion."
						+ "Las consultas y autenticacion mediante LDAP no podrán ser procesadas."
						+ "Revise si esta disponible el servicio del AD mediante LDAP"
						+ "Revise y modifique las propiedades de configuracion de LDAP, luego reinicie el servicio de BackEnd.");
	        }
	    } catch (Exception e) {
	        LOGGER.error("Error al validar usuario: " + id_usuario_desenc + ". Error: " + e.getMessage());
	        fileEdit = fileSeg;
			new File(fileEdit).createNewFile(); // Creamos archivo en caso no exista
			writer = new PrintWriter(new FileOutputStream(fileEdit, true));
			writer.println("---------------TRANSACCION---------------");
			writer.println("***Estado***:                 No Exitoso");
			writer.println("***ID del evento***:          0");
			writer.println("***Nombre del evento***:      Login");
			writer.println("***ID de usuario***:          " + id_usuario_desenc);
			writer.println("***Fecha***:                  " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			writer.println("***Acción realizada***:       Intento de login");
			writer.println("-----------------------------------------");
			writer.close();
			
			throw e;
			
	    } finally {
	    	cerrarContext(ldapUserContext);
	    	cerrarContext(ldapContext);
	    }
	    //Se limpia las credenciales del usuario
	    usuario.setContrasena(null);
	    return usuario;
	}
}