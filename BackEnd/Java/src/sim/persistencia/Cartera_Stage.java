package sim.persistencia;

import java.util.ArrayList;

public class Cartera_Stage extends Cartera{

	private ArrayList<Stage> stages;

	public ArrayList<Stage> getStages() {
		return stages;
	}

	public void setStages(ArrayList<Stage> stages) {
		this.stages = stages;
	}
	
}
