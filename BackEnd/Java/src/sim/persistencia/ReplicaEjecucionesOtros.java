package sim.persistencia;

import java.util.ArrayList;

public class ReplicaEjecucionesOtros {
	private Integer codmes_replica;	
	private ArrayList<EjecucionOtro> ejecuciones_replicar;
	
	public ReplicaEjecucionesOtros() {}

	public ArrayList<EjecucionOtro> getEjecuciones_replicar() {
		return ejecuciones_replicar;
	}

	public void setEjecuciones_replicar(ArrayList<EjecucionOtro> ejecuciones_replicar) {
		this.ejecuciones_replicar = ejecuciones_replicar;
	}

	public Integer getCodmes_replica() {
		return codmes_replica;
	}

	public void setCodmes_replica(Integer codmes_replica) {
		this.codmes_replica = codmes_replica;
	}
}
