package sim.persistencia.dic;

public class TipoEvento {
	private Integer id_tipo_evento;
	private String nombre_tipo_evento;
	
	public TipoEvento() {		
	}
	
	public Integer getId_tipo_evento() {
		return id_tipo_evento;
	}
	public void setId_tipo_evento(Integer id_tipo_evento) {
		this.id_tipo_evento = id_tipo_evento;
	}
	public String getNombre_tipo_evento() {
		return nombre_tipo_evento;
	}
	public void setNombre_tipo_evento(String nombre_tipo_evento) {
		this.nombre_tipo_evento = nombre_tipo_evento;
	}
}
