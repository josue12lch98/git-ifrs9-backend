package sim.persistencia;
import java.sql.Date;

import sim.persistencia.dic.TipoAmbito;

public class Tarea {
	private Integer id_tarea;
	private String nombre_tarea;
	private String nombre_corto;
	private Date fecha_inicio;
	private Date fecha_limite;
	private Boolean flag_finalizado;
	private TipoAmbito tipo_ambito;
	
	public Integer getId_tarea() {
		return id_tarea;
	}
	public void setId_tarea(Integer id_tarea) {
		this.id_tarea = id_tarea;
	}
	public String getNombre_tarea() {
		return nombre_tarea;
	}
	public void setNombre_tarea(String nombre_tarea) {
		this.nombre_tarea = nombre_tarea;
	}	
	public Date getFecha_inicio() {
		return fecha_inicio;
	}
	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}
	
	public Date getFecha_limite() {
		return fecha_limite;
	}
	public void setFecha_limite(Date fecha_limite) {
		this.fecha_limite = fecha_limite;
	}
	public Boolean getFlag_finalizado() {
		return flag_finalizado;
	}
	public void setFlag_finalizado(Boolean flag_finalizado) {
		this.flag_finalizado = flag_finalizado;
	}
	public String getNombre_corto() {
		return nombre_corto;
	}
	public void setNombre_corto(String nombre_corto) {
		this.nombre_corto = nombre_corto;
	}
	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}
	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}
		
}
