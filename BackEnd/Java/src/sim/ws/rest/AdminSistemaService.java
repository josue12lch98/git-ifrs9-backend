package sim.ws.rest;

import java.io.File;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import sim.ws.rest.JwtRequiereToken;
import sim.ws.rest.JwtRefrescaToken;

import sim.Contenedor;
import sim.persistencia.*;

@Path("/adminSistema")
public class AdminSistemaService {
	private static Contenedor contenedor = Contenedor.obtenerInstancia();

	// -------------------------------------------------------------------
	// --------------------------ROL Y PERFIL-----------------------------
	// -------------------------------------------------------------------

	@POST
	@Path("/crearPerfil")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearPerfil(Perfil perfil) throws Exception {
		contenedor.validarInjectObjeto(perfil, true, true);
		contenedor.crearPerfil(perfil);
	}

	@PUT
	@Path("/editarPerfil")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarPerfil(Perfil perfil) throws Exception {
		contenedor.validarInjectObjeto(perfil, true, true);
		contenedor.editarPerfil(perfil);
	}

	@DELETE
	@Path("/borrarPerfil/{id}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarPerfil(@PathParam("id") Integer id_perfil) throws Exception {
		contenedor.borrarPerfil(id_perfil);
	}

	@GET
	@Path("/obtenerPermisos")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Permiso> obtenerPermisos() throws Exception {
		ArrayList<Permiso> permisos = new ArrayList<Permiso>();
		permisos = contenedor.obtenerPermisos();
		return permisos;
	}

	@GET
	@Path("/obtenerPerfiles")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Perfil> obtenerPerfiles() throws Exception {
		ArrayList<Perfil> perfiles = new ArrayList<Perfil>();
		perfiles = contenedor.obtenerPerfiles();
		return perfiles;
	}

	@GET
	@Path("/obtenerPerfilesPermisos")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Perfil> obtenerPerfilesPermisos() throws Exception {
		ArrayList<Perfil> perfiles_permisos = new ArrayList<Perfil>();
		perfiles_permisos = contenedor.obtenerPerfilesPermisos();
		return perfiles_permisos;
	}

	@GET
	@Path("/obtenerPermisosPorIdPerfil/{id_perfil}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Permiso> obtenerPermisosPorIdPerfil(@PathParam("id_perfil") Integer id_perfil) {
		ArrayList<Permiso> permisos = new ArrayList<Permiso>();
		try {
			permisos = contenedor.obtenerPermisosPorIdPerfil(id_perfil);
		} catch (Exception e) {
			e.getMessage().toString();
		}
		return permisos;
	}

	// -------------------------------------------------------------------
	// ----------------------------TAREAS------------------------------
	// -------------------------------------------------------------------
	@GET
	@Path("/obtenerTareas")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Tarea> obtenerTareas() throws Exception {
		ArrayList<Tarea> tareas = new ArrayList<Tarea>();
		tareas = contenedor.obtenerTareas();
		return tareas;
	}

	@POST
	@Path("/crearTarea")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearTarea(Tarea tarea) throws Exception {
		contenedor.validarInjectObjeto(tarea, true, true);
		contenedor.crearTarea(tarea);
	}

	@PUT
	@Path("/editarTarea")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarTarea(Tarea tarea) throws Exception {
		contenedor.editarTarea(tarea);
	}

	@DELETE
	@Path("/borrarTarea/{id}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarTarea(@PathParam("id") Integer id_tarea) throws Exception {
		contenedor.borrarTarea(id_tarea);
	}

	// -------------------------------------------------------------------
	// -----------------------------FECHAS--------------------------------
	// -------------------------------------------------------------------

	@GET
	@Path("/obtenerFechas")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Fecha> obtenerFechas() throws Exception {
		ArrayList<Fecha> fechas = new ArrayList<Fecha>();
		fechas = contenedor.obtenerFechas();
		return fechas;
	}

	@POST
	@Path("/crearFecha")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearFecha(Fecha fecha) throws Exception {
		contenedor.validarInjectObjeto(fecha, true, true);
		contenedor.crearFecha(fecha);
	}

	@PUT
	@Path("/editarFecha")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarFecha(Fecha fecha) throws Exception {
		contenedor.validarInjectObjeto(fecha, true, true);
		contenedor.editarFecha(fecha);
	}

	// -------------------------------------------------------------------
	// ----------------------CORREO Y NOTIFICACION------------------------
	// -------------------------------------------------------------------

	@POST
	@Path("/crearCorreo")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearCorreo(Correo correo) throws Exception {
		contenedor.validarInjectObjeto(correo, true, true);
		contenedor.crearCorreo(correo);
	}

	@PUT
	@Path("/editarCorreo")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarCorreo(Correo correo) throws Exception {
		contenedor.validarInjectObjeto(correo, true, true);
		contenedor.editarCorreo(correo);
	}

	@DELETE
	@Path("/borrarCorreo/{id}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarCorreo(@PathParam("id") Integer id_correo) throws Exception {
		contenedor.borrarCorreo(id_correo);
	}

	@GET
	@Path("/obtenerCorreosConNotificaciones/{id}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Correo> obtenerCorreosConNotificaciones(@PathParam("id") Integer id_tipoAmbito) throws Exception {
		ArrayList<Correo> correos_notificaciones = new ArrayList<Correo>();
		correos_notificaciones = contenedor.obtenerCorreosConNotificaciones(id_tipoAmbito);
		return correos_notificaciones;
	}
	@GET
	@Path("/obtenerLog")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response obtenerLog(@QueryParam("file") String file) throws Exception {
		File fileDownload = new File(file);
		ResponseBuilder response = Response.ok((Object) fileDownload);
		response.header("Content-Disposition", "attachment;filename=" + file);
		return response.build();
	}
	
	// ---------------SAS ----------------// 
	@PUT
	@Path("/editarConfiguracionSAS")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public RutaValida editarConfiguracionSAS(ConfiguracionSAS configuracion) throws Exception {
		contenedor.validarInjectObjeto(configuracion, false, true);
		return contenedor.editarConfiguracionSAS(configuracion);
	}
	
	@GET
	@Path("/obtenerConfiguracionSAS")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ConfiguracionSAS obtenerConfiguracionSAS() throws Exception {
		ConfiguracionSAS configuracion = new ConfiguracionSAS();
		configuracion = contenedor.obtenerConfiguracionSAS();
		return configuracion;
	}
	
	@PUT
	@Path("/validarSAS")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public RutaValida validarSAS(ConfiguracionSAS configuracion) throws Exception {
		contenedor.validarInjectObjeto(configuracion, false, true);
		return contenedor.validarSAS(configuracion);
	}

}
