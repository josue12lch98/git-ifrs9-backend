package sim.persistencia;


public class Escenario {

	private Integer id_escenario;
	private String nombre_escenario;
	private String nombre_variable_motor;
	private Boolean flag_obligatorio;
	
	public Escenario() {
	}
	
	public Integer getId_escenario() {
		return id_escenario;
	}
	public void setId_escenario(Integer id_escenario) {
		this.id_escenario = id_escenario;
	}
	public String getNombre_escenario() {
		return nombre_escenario;
	}
	public void setNombre_escenario(String nombre_escenario) {
		this.nombre_escenario = nombre_escenario;
	}

	public String getNombre_variable_motor() {
		return nombre_variable_motor;
	}

	public void setNombre_variable_motor(String nombre_variable_motor) {
		this.nombre_variable_motor = nombre_variable_motor;
	}

	public Boolean getFlag_obligatorio() {
		return flag_obligatorio;
	}

	public void setFlag_obligatorio(Boolean flag_obligatorio) {
		this.flag_obligatorio = flag_obligatorio;
	}

}
