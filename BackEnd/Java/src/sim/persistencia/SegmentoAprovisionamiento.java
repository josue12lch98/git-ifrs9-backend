package sim.persistencia;

import java.util.ArrayList;

public class SegmentoAprovisionamiento {

	private Integer id_segmento;
	private Integer id_aprov;
	private String descripcion;
	private Boolean flag_aplicanreglas;
	private Boolean flag_sinsegmento;
	// Para analizar los segmente de que anlisis es
	private Integer id_tipoanalisis;
	private Boolean flag_aplica;
	
	private ArrayList<ReglaCalidad> reglas;// Reglas de calidad que aplican a cada campo

	// Constructor
	public SegmentoAprovisionamiento() {
	}

	public SegmentoAprovisionamiento(Integer id_segmento, String descripcion) {
		this.id_segmento = id_segmento;
		this.descripcion = descripcion;
		this.reglas = new ArrayList<ReglaCalidad>();
	}

	// Getters y Setters
	public Integer getId_segmento() {
		return id_segmento;
	}

	public void setId_segmento(Integer id_segmento) {
		this.id_segmento = id_segmento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public ArrayList<ReglaCalidad> getReglas() {
		return reglas;
	}

	public void setReglas(ArrayList<ReglaCalidad> reglas) {
		this.reglas = reglas;
	}

	// Agregar reglas al Segmento CampoAprovisionamiento
	public void agregarRegla(ReglaCalidad regla) {
		reglas.add(regla);
	}

	public Boolean getFlag_aplicanreglas() {
		return flag_aplicanreglas;
	}

	public void setFlag_aplicanreglas(Boolean flag_aplicanreglas) {
		this.flag_aplicanreglas = flag_aplicanreglas;
	}

	public Integer getId_tipoanalisis() {
		return id_tipoanalisis;
	}

	public void setId_tipoanalisis(Integer id_tipoanalisis) {
		this.id_tipoanalisis = id_tipoanalisis;
	}

	public Integer getId_aprov() {
		return id_aprov;
	}

	public void setId_aprov(Integer id_aprov) {
		this.id_aprov = id_aprov;
	}

	public Boolean getFlag_sinsegmento() {
		return flag_sinsegmento;
	}
	
	public void setFlag_sinsegmento(Boolean flag_sinsegmento) {
		this.flag_sinsegmento = flag_sinsegmento;
	}

	public Boolean getFlag_aplica() {
		return flag_aplica;
	}

	public void setFlag_aplica(Boolean flag_aplica) {
		this.flag_aplica = flag_aplica;
	}
	
	
}
