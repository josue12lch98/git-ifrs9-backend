package sim;

import org.jboss.resteasy.resteasy_jaxrs.i18n.Messages;
import org.jboss.resteasy.spi.CorsHeaders;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import java.io.IOException;

@Provider
@PreMatching
public class CorsFilter implements ContainerRequestFilter, ContainerResponseFilter {

	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(CorsFilter.class);

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String origin = requestContext.getHeaderString(CorsHeaders.ORIGIN);

		// En caso de metodos get o head, el browser no brinda el header origin
		if (!requestContext.getMethod().equalsIgnoreCase("GET")
				&& !requestContext.getMethod().equalsIgnoreCase("HEAD")) {
			if (origin == null) {
				LOGGER.warn("Origen nulo, estableciendo context cors.failure en true");
				requestContext.setProperty("cors.failure", true);
				throw new ForbiddenException(Messages.MESSAGES.originNotAllowed(origin));
				// return;
			}
			if (requestContext.getMethod().equalsIgnoreCase("OPTIONS")) {
				LOGGER.debug("Leyendo cabecera options");
				preflight(origin, requestContext);
			} else {
				checkOrigin(requestContext, origin);
			}
		}
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		String origin = requestContext.getHeaderString(CorsHeaders.ORIGIN);
		if (origin == null || requestContext.getMethod().equalsIgnoreCase("OPTIONS")
				|| requestContext.getProperty("cors.failure") != null) {
			// don't do anything if origin is null, its an OPTIONS request, or cors.failure
			// is set
			return;
		}
		responseContext.getHeaders().putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, origin);
		// responseContext.getHeaders().putSingle(CorsHeaders.VARY, CorsHeaders.ORIGIN);

		// Allow credentials: true
		responseContext.getHeaders().putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");

		// Expose headers
		responseContext.getHeaders().putSingle(CorsHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "*");

	}

	protected void preflight(String origin, ContainerRequestContext requestContext) throws IOException {
		checkOrigin(requestContext, origin);
		Response.ResponseBuilder builder = Response.ok();
		builder.header(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, origin);
		// builder.header(CorsHeaders.VARY, CorsHeaders.ORIGIN);
		builder.header(CorsHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
		String requestMethods = requestContext.getHeaderString(CorsHeaders.ACCESS_CONTROL_REQUEST_METHOD);

		builder.header(CorsHeaders.ACCESS_CONTROL_ALLOW_METHODS, requestMethods);

		String allowHeaders = requestContext.getHeaderString(CorsHeaders.ACCESS_CONTROL_REQUEST_HEADERS);

		builder.header(CorsHeaders.ACCESS_CONTROL_ALLOW_HEADERS, allowHeaders);

		builder.header(CorsHeaders.ACCESS_CONTROL_MAX_AGE, "3600");

		requestContext.abortWith(builder.build());
	}

	protected void checkOrigin(ContainerRequestContext requestContext, String origin) {
		if (!origin.equalsIgnoreCase(PropertiesCompartido.protocol_client + PropertiesCompartido.host_client) ) {
			requestContext.setProperty("cors.failure", true);
			LOGGER.info("Origen permitido: " + PropertiesCompartido.protocol_client + PropertiesCompartido.host_client);
			throw new ForbiddenException(Messages.MESSAGES.originNotAllowed(origin));
		} else {
			LOGGER.debug("Origen reconocido");
		}
	}
}