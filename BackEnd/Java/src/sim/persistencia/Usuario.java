package sim.persistencia;

public class Usuario {
	private String id_usuario;
	private String user_dn;
	private String nombre_usuario;
	private Perfil perfil_usuario;
	private String contrasena;
	private String msg_cierre;
	
	//Token
	private String token;
	private Long expiracion;
	
		
	public Usuario() {	
		perfil_usuario = new Perfil();
	}

	public String getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getUser_dn() {
		return user_dn;
	}

	public void setUser_dn(String user_dn) {
		this.user_dn = user_dn;
	}

	public String getNombre_usuario() {
		return nombre_usuario;
	}

	public void setNombre_usuario(String nombre_usuario) {
		this.nombre_usuario = nombre_usuario;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getExpiracion() {
		return expiracion;
	}

	public void setExpiracion(Long expiracion) {
		this.expiracion = expiracion;
	}

	public Perfil getPerfil_usuario() {
		return perfil_usuario;
	}

	public void setPerfil_usuario(Perfil perfil_usuario) {
		this.perfil_usuario = perfil_usuario;
	}

}
