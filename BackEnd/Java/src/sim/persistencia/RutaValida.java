package sim.persistencia;

public class RutaValida {
	private Boolean valido;
	private String observacion;
	
	public RutaValida() {
		
	}
	public Boolean getValido() {
		return valido;
	}
	public void setValido(Boolean valido) {
		this.valido = valido;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
}
