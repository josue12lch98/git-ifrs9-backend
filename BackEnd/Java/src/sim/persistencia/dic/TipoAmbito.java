package sim.persistencia.dic;


public class TipoAmbito {
	private Integer id_tipo_ambito;
	private String nombre_tipo_ambito;
	
	public TipoAmbito() {
	}
	
	public Integer getId_tipo_ambito() {
		return id_tipo_ambito;
	}
	public void setId_tipo_ambito(Integer id_tipo_ambito) {
		this.id_tipo_ambito = id_tipo_ambito;
	}
	public String getNombre_tipo_ambito() {
		return nombre_tipo_ambito;
	}
	public void setNombre_tipo_ambito(String nombre_tipo_ambito) {
		this.nombre_tipo_ambito = nombre_tipo_ambito;
	}

	
}
