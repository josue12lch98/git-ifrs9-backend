package sim.persistencia;

import java.util.ArrayList;

public class Fichero extends CargaFichero {

	// Datos basicos de un archivo <--> fichero
	private Integer id_fichero;
	
	
	
	private String ruta_archivo; // Es la ruta de donde ha sido obtenido el archivo, no incluye el nombre del
									// archivo.
	private String nombre_archivo; // Nombre del archivo que representa al fichero de datos, incluye la extension
									// del archivo.

	
	private ArrayList<Cartera> carteras;

	// Constructor
	public Fichero() {
		super();
	}

	// Getters y Setters
	public String getRuta_archivo() {
		return ruta_archivo;
	}

	public void setRuta_archivo(String ruta_archivo) {
		this.ruta_archivo = ruta_archivo;
	}

	public String getNombre_archivo() {
		return nombre_archivo;
	}

	public void setNombre_archivo(String nombre_archivo) {
		this.nombre_archivo = nombre_archivo;
	}


	// Setear automaticamente el nombre del fichero a partir del nombre del archivo,
	// requerira que este haya sido seteado inicialmente
	public void crearNombreTabla() throws Exception {
		this.setNombre_carga(nombre_archivo.split("\\.")[0]);
	}
/*
	
	// Obtener solamente la lista de nombres de campos
	public ArrayList<String> obtenerNombresCamposFichero() throws Exception {
		ArrayList<String> nombres = new ArrayList<String>();
		for (CampoFichero campo : this.campos_fichero) {
			nombres.add(campo.getNombre_campo());
		}
		return nombres;
	}
*/
	public Integer getId_fichero() {
		return id_fichero;
	}

	public void setId_fichero(Integer id_fichero) {
		this.id_fichero = id_fichero;
	}
	
	public ArrayList<Cartera> getCarteras() {
		return carteras;
	}

	public void setCarteras(ArrayList<Cartera> carteras) {
		this.carteras = carteras;
	}

}