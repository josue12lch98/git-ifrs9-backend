package sim.persistencia;

public class OutputDescriptivo {
	private	Integer id_aprovisionamiento;
	private	Integer id_campo_aprovisionamiento;
	private	String id_output;
	private	String descripcion_output;
	private	Double resultado;
	
	public Integer getId_aprovisionamiento() {
		return id_aprovisionamiento;
	}
	public void setId_aprovisionamiento(Integer id_aprovisionamiento) {
		this.id_aprovisionamiento = id_aprovisionamiento;
	}
	public Integer getId_campo_aprovisionamiento() {
		return id_campo_aprovisionamiento;
	}
	public void setId_campo_aprovisionamiento(Integer id_campo_aprovisionamiento) {
		this.id_campo_aprovisionamiento = id_campo_aprovisionamiento;
	}
	public String getId_output() {
		return id_output;
	}
	public void setId_output(String id_output) {
		this.id_output = id_output;
	}
	public String getDescripcion_output() {
		return descripcion_output;
	}
	public void setDescripcion_output(String descripcion_output) {
		this.descripcion_output = descripcion_output;
	}
	public Double getResultado() {
		return resultado;
	}
	public void setResultado(Double resultado) {
		this.resultado = resultado;
	}

}
