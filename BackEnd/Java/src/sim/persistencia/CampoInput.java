package sim.persistencia;

import java.util.ArrayList;

public class CampoInput extends Campo {

	private Integer id_tabla; //Indica id de la tabla input
	private ArrayList<Registro> registros; //Registros de una tabla input SOLO para mostrar y utilizar en los ajustes expertos
	
	
	//Constructor
	public CampoInput() {
		super();
		registros = new ArrayList<Registro>();
	}

	//Getters y Setters
	public Integer getId_tabla() {
		return id_tabla;
	}
	public void setId_tabla(Integer id_tabla) {
		this.id_tabla = id_tabla;
	}

	public ArrayList<Registro> getRegistros() {
		return registros;
	}
	public void setRegistros(ArrayList<Registro> registros) {
		this.registros = registros;
	}
	
	//Agregar registros al campo input de una tabla
	public void agregarRegistro(Registro registro) {
		registros.add(registro);
	}

}