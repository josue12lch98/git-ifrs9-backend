package sim.persistencia;

import java.util.ArrayList;

public class EjecucionPrimerPaso {
	private Integer id_ejecucion;
	private Integer codmes;
	private Integer estado_ejecucion;	
	private Integer porcentaje_avance;
	private Long datetime_ejecucion;
	private PrimerPaso primerPaso;
	private EstructuraMetodologica metodologia;
	private ArrayList<TablaMapeoEjecucion> tablas_mapeo_ejecucion;	
	
	public EjecucionPrimerPaso() {		
	}

	public Integer getId_ejecucion() {
		return id_ejecucion;
	}

	public void setId_ejecucion(Integer id_ejecucion) {
		this.id_ejecucion = id_ejecucion;
	}

	public Integer getEstado_ejecucion() {
		return estado_ejecucion;
	}

	public void setEstado_ejecucion(Integer estado_ejecucion) {
		this.estado_ejecucion = estado_ejecucion;
	}

	public Long getDatetime_ejecucion() {
		return datetime_ejecucion;
	}

	public void setDatetime_ejecucion(Long datetime_ejecucion) {
		this.datetime_ejecucion = datetime_ejecucion;
	}

	public EstructuraMetodologica getMetodologia() {
		return metodologia;
	}

	public void setMetodologia(EstructuraMetodologica metodologia) {
		this.metodologia = metodologia;
	}

	public PrimerPaso getPrimerPaso() {
		return primerPaso;
	}

	public void setPrimerPaso(PrimerPaso primerPaso) {
		this.primerPaso = primerPaso;
	}

	public Integer getCodmes() {
		return codmes;
	}

	public void setCodmes(Integer codmes) {
		this.codmes = codmes;
	}

	public ArrayList<TablaMapeoEjecucion> getTablas_mapeo_ejecucion() {
		return tablas_mapeo_ejecucion;
	}

	public void setTablas_mapeo_ejecucion(ArrayList<TablaMapeoEjecucion> tablas_mapeo_ejecucion) {
		this.tablas_mapeo_ejecucion = tablas_mapeo_ejecucion;
	}

	public Integer getPorcentaje_avance() {
		return porcentaje_avance;
	}

	public void setPorcentaje_avance(Integer porcentaje_avance) {
		this.porcentaje_avance = porcentaje_avance;
	}	
}
