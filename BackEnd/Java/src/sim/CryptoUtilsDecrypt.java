package sim;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;



public class CryptoUtilsDecrypt {
	private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    public static String decrypt(File inputFile, byte[] key)
            throws CryptoException {
    	
        byte[] semillaByte = doCrypto(Cipher.DECRYPT_MODE, inputFile, key);
        return new String(semillaByte);
    }
 
    private static byte[] doCrypto(int cipherMode, File inputFile, byte[] key) throws CryptoException {
        try {

            Key secretKey = new SecretKeySpec(key, ALGORITHM);
          
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
          
            cipher.init(cipherMode, secretKey, new IvParameterSpec(new byte[16]));
          
            @SuppressWarnings("resource")
			FileInputStream inputStream = new FileInputStream(inputFile);
            
            byte[] inputBytes = new byte[(int) inputFile.length()];
            
            inputStream.read(inputBytes);
            
            return cipher.doFinal(inputBytes);
             
        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException | InvalidAlgorithmParameterException ex) {
            throw new CryptoException("Error encrypting/decrypting file", ex);
        }
    }
}
