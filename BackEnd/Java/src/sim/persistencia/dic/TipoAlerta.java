package sim.persistencia.dic;

public class TipoAlerta {
	private Integer id_tipo_alerta;
	private String nombre_tipo_alerta;
	private TipoMensajeAlerta tipo_mensaje_alerta;
	private TipoObjeto tipo_objeto;
	private TipoObjeto tipo_objeto_causa;
	private TipoAccion tipo_accion;
	private String seccion_alerta;
	private String sql_alerta;
	private TipoAmbito tipo_ambito;
	
	public TipoAlerta() {		
	}

	public Integer getId_tipo_alerta() {
		return id_tipo_alerta;
	}

	public void setId_tipo_alerta(Integer id_tipo_alerta) {
		this.id_tipo_alerta = id_tipo_alerta;
	}

	public TipoMensajeAlerta getTipo_mensaje_alerta() {
		return tipo_mensaje_alerta;
	}

	public void setTipo_mensaje_alerta(TipoMensajeAlerta tipo_mensaje_alerta) {
		this.tipo_mensaje_alerta = tipo_mensaje_alerta;
	}

	public TipoObjeto getTipo_objeto() {
		return tipo_objeto;
	}

	public void setTipo_objeto(TipoObjeto tipo_objeto) {
		this.tipo_objeto = tipo_objeto;
	}

	public TipoObjeto getTipo_objeto_causa() {
		return tipo_objeto_causa;
	}

	public void setTipo_objeto_causa(TipoObjeto tipo_objeto_causa) {
		this.tipo_objeto_causa = tipo_objeto_causa;
	}

	public String getSeccion_alerta() {
		return seccion_alerta;
	}

	public void setSeccion_alerta(String seccion_alerta) {
		this.seccion_alerta = seccion_alerta;
	}

	public String getSql_alerta() {
		return sql_alerta;
	}

	public void setSql_alerta(String sql_alerta) {
		this.sql_alerta = sql_alerta;
	}

	public TipoAccion getTipo_accion() {
		return tipo_accion;
	}

	public void setTipo_accion(TipoAccion tipo_accion) {
		this.tipo_accion = tipo_accion;
	}

	public String getNombre_tipo_alerta() {
		return nombre_tipo_alerta;
	}

	public void setNombre_tipo_alerta(String nombre_tipo_alerta) {
		this.nombre_tipo_alerta = nombre_tipo_alerta;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}

	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}	
}
