package sim.persistencia.dic;

public class MotorCalculo {
	private Integer id_motor;
	private String nombre_motor;
	
	public MotorCalculo() {
	}
	
	public Integer getId_motor() {
		return id_motor;
	}
	public void setId_motor(Integer id_motor) {
		this.id_motor = id_motor;
	}
	public String getNombre_motor() {
		return nombre_motor;
	}
	public void setNombre_motor(String nombre_motor) {
		this.nombre_motor = nombre_motor;
	}
	
}
