package sim.persistencia;

import java.util.ArrayList;

import sim.persistencia.dic.MotorCalculo;
import sim.persistencia.dic.TipoFlujo;
import sim.persistencia.dic.TipoMetodologia;
import sim.persistencia.dic.TipoObtencionResultados;
import sim.persistencia.dic.TipoPeriodicidad;

public class EstructuraMetodologica {
	private Integer id_metodologia;
	private String nombre_metodologia;
	private Cartera cartera;
	private String descripcion_metodologia;
	private MotorCalculo motor;
	private String nombre_funcPlumbr;
	private Integer puerto_plumbr;
	private String codigo_estructura;
	private TipoFlujo flujo;

	// Flujo: Presupuesto
	private ArrayList<Bloque> bloques_presupuesto;

	// Flujo: Otros
	private ArrayList<Bloque> bloques_otros;

	// Flujo: Calculo PE
	private TipoObtencionResultados metodo_resultados;

	// Metodo resultados: extrapolacion
	private ArrayList<Bloque> bloques_extrapolacion;
	private ArrayList<Cartera> carteras_extrapolacion;

	// Metodo resultados: resultado directo
	private ArrayList<Bloque> bloques_resultDirecto;

	// Metodo resultados: Modelo interno
	private Integer nro_buckets;
	private Integer life_time;
	private Integer nro_maxPlazo;
	private TipoPeriodicidad periodicidad;
	private TipoMetodologia metodologia_PD;
	private TipoMetodologia metodologia_EAD;
	private Boolean flag_lgdUnica;

	// LGD Unica
	private TipoMetodologia metodologia_LGD;

	// LGD WO y BE
	private TipoMetodologia metodologia_LGD_WO;
	private TipoMetodologia metodologia_LGD_BE;

	private Bloque bloque_principal;

	private ArrayList<Bloque> bloques_general;
	private ArrayList<Bloque> bloques_escenario;
	private ArrayList<PrimerPasoConfigurado> grupo_primerosPasos;

	private Boolean conDependencias;
	
	private Boolean conDependenciasOficial;

	public EstructuraMetodologica() {
		this.cartera = new Cartera();
//		this.bloquesCalculo_general = new ArrayList<Bloque>();
//		this.bloquesCalculo_PD = new ArrayList<Bloque>();
//		this.bloquesCalculo_EAD = new ArrayList<Bloque>();
//		this.bloquesCalculo_LGD = new ArrayList<Bloque>();
//		this.bloquesCalculo_escenario = new ArrayList<Bloque>();
//		this.bloquesProcPrevios_repricing = new ArrayList<Bloque>();
//		this.bloquesProcPrevios_modeloMacro = new ArrayList<Bloque>();
//		this.bloquesProcPrevios_matricesPD = new ArrayList<Bloque>();
//		this.bloquesExtrapolacion_general = new ArrayList<Bloque>();
	}

	public ArrayList<Bloque> obtenerBloquesTotales() {
		ArrayList<Bloque> bloquesTotales = new ArrayList<Bloque>();

		if (this.bloques_presupuesto != null) {
			for (Bloque bloque : this.bloques_presupuesto) {
				bloquesTotales.add(bloque);
			}
		}

		if (this.bloques_otros != null) {
			for (Bloque bloque : this.bloques_otros) {
				bloquesTotales.add(bloque);
			}
		}

		if (this.bloques_extrapolacion != null) {
			for (Bloque bloque : this.bloques_extrapolacion) {
				bloquesTotales.add(bloque);
			}
		}

		if (this.bloques_resultDirecto != null) {
			for (Bloque bloque : this.bloques_resultDirecto) {
				bloquesTotales.add(bloque);
			}
		}

		if (this.bloques_general != null) {
			for (Bloque bloque : this.bloques_general) {
				bloquesTotales.add(bloque);
			}
		}

		if (this.bloques_escenario != null) {
			for (Bloque bloque : this.bloques_escenario) {
				bloquesTotales.add(bloque);
			}
		}

		if (this.grupo_primerosPasos != null) {

			for (PrimerPasoConfigurado primerpaso : this.grupo_primerosPasos) {
				if (primerpaso.getBloques_primerPaso() != null) {
					for (Bloque bloque : primerpaso.getBloques_primerPaso()) {
						bloquesTotales.add(bloque);
					}
				}
			}
		}

		if (this.bloque_principal != null) {
			bloquesTotales.add(bloque_principal);
		}		

		return bloquesTotales;
	}

	public ArrayList<Bloque> obtenerBloquesTotalesSinPrimerosPasos() {
		ArrayList<Bloque> bloquesTotales = new ArrayList<Bloque>();

		if (this.bloques_presupuesto != null) {
			for (Bloque bloque : this.bloques_presupuesto) {
				bloquesTotales.add(bloque);
			}
		}

		if (this.bloques_otros != null) {
			for (Bloque bloque : this.bloques_otros) {
				bloquesTotales.add(bloque);
			}
		}

		if (this.bloques_extrapolacion != null) {
			for (Bloque bloque : this.bloques_extrapolacion) {
				bloquesTotales.add(bloque);
			}
		}

		if (this.bloques_resultDirecto != null) {
			for (Bloque bloque : this.bloques_resultDirecto) {
				bloquesTotales.add(bloque);
			}
		}

		if (this.bloques_general != null) {
			for (Bloque bloque : this.bloques_general) {
				bloquesTotales.add(bloque);
			}
		}

		if (this.bloques_escenario != null) {
			for (Bloque bloque : this.bloques_escenario) {
				bloquesTotales.add(bloque);
			}
		}

		if (this.bloque_principal != null) {
			bloquesTotales.add(bloque_principal);
		}		

		return bloquesTotales;
	}

	public ArrayList<Bloque> obtenerBloquesPrimerosPasos() {
		ArrayList<Bloque> bloquesPrimerosPasos = new ArrayList<Bloque>();

		if (this.grupo_primerosPasos != null) {
			for (PrimerPasoConfigurado primerpaso : this.grupo_primerosPasos) {
				if (primerpaso.getBloques_primerPaso() != null) {
					for (Bloque bloque : primerpaso.getBloques_primerPaso()) {
						bloquesPrimerosPasos.add(bloque);
					}
				}
			}
		}

		return bloquesPrimerosPasos;
	}

	/*
	 * public void setBloquesTotales(ArrayList<Bloque> bloques, TipoFlujo tipoFlujo,
	 * TipoObtencionResultados tipoObtencionResultados) {
	 * 
	 * PrimerPasoConfigurado primerPasoConfigurado = null;
	 * 
	 * if (tipoFlujo.getNombre_tipo_flujo().toLowerCase().equals(("Presupuesto").
	 * toLowerCase())) { for (Bloque bloque : bloques) {
	 * this.bloquesPresupuesto_general.add(bloque); } } else if
	 * (tipoFlujo.getNombre_tipo_flujo().toLowerCase().equals(("Otros").toLowerCase(
	 * ))) { for (Bloque bloque : bloques) { this.bloquesOtros_general.add(bloque);
	 * } } else if
	 * (tipoFlujo.getNombre_tipo_flujo().toLowerCase().equals(("Calculo PE").
	 * toLowerCase())) { if
	 * (tipoObtencionResultados.getNombre_tipo_obtencion_resultados().equals(
	 * "Extrapolacion")) { for (Bloque bloque : bloques) {
	 * this.bloquesExtrapol_general.add(bloque); } } else if
	 * (tipoObtencionResultados.getNombre_tipo_obtencion_resultados().
	 * equals("Resultado directo")) { for (Bloque bloque : bloques) {
	 * this.bloquesDirecto_resultados.add(bloque); } } else if
	 * (tipoObtencionResultados.getNombre_tipo_obtencion_resultados().
	 * equals("Modelo interno")) { for (Bloque bloque : bloques) { if
	 * (bloque.getTipo_bloque().getNombre_tipo_bloque().equals("General")) {
	 * this.bloquesCalculoModInt_general.add(bloque); } else if
	 * (bloque.getTipo_bloque().getNombre_tipo_bloque().equals("Escenario")) {
	 * this.getBloquesCalculoModInt_escenario().add(bloque); } else if
	 * (bloque.getTipo_bloque().getNombre_tipo_bloque().equals("Primer paso")) {
	 * //...?? } } } }
	 * 
	 * }
	 */

	public Integer getId_metodologia() {
		return id_metodologia;
	}

	public void setId_metodologia(Integer id_metodologia) {
		this.id_metodologia = id_metodologia;
	}

	public String getNombre_metodologia() {
		return nombre_metodologia;
	}

	public void setNombre_metodologia(String nombre_metodologia) {
		this.nombre_metodologia = nombre_metodologia;
	}

	public String getDescripcion_metodologia() {
		return descripcion_metodologia;
	}

	public void setDescripcion_metodologia(String descripcion_metodologia) {
		this.descripcion_metodologia = descripcion_metodologia;
	}

	public Integer getNro_buckets() {
		return nro_buckets;
	}

	public void setNro_buckets(Integer nro_buckets) {
		this.nro_buckets = nro_buckets;
	}

	public Integer getLife_time() {
		return life_time;
	}

	public void setLife_time(Integer life_time) {
		this.life_time = life_time;
	}

	public MotorCalculo getMotor() {
		return motor;
	}

	public void setMotor(MotorCalculo motor) {
		this.motor = motor;
	}

	public TipoObtencionResultados getMetodo_resultados() {
		return metodo_resultados;
	}

	public void setMetodo_resultados(TipoObtencionResultados metodo_resultados) {
		this.metodo_resultados = metodo_resultados;
	}

	public TipoMetodologia getMetodologia_PD() {
		return metodologia_PD;
	}

	public void setMetodologia_PD(TipoMetodologia metodologia_PD) {
		this.metodologia_PD = metodologia_PD;
	}

	public TipoMetodologia getMetodologia_LGD_WO() {
		return metodologia_LGD_WO;
	}

	public void setMetodologia_LGD_WO(TipoMetodologia metodologia_LGD_WO) {
		this.metodologia_LGD_WO = metodologia_LGD_WO;
	}

	public TipoMetodologia getMetodologia_LGD_BE() {
		return metodologia_LGD_BE;
	}

	public void setMetodologia_LGD_BE(TipoMetodologia metodologia_LGD_BE) {
		this.metodologia_LGD_BE = metodologia_LGD_BE;
	}

	public TipoMetodologia getMetodologia_EAD() {
		return metodologia_EAD;
	}

	public void setMetodologia_EAD(TipoMetodologia metodologia_EAD) {
		this.metodologia_EAD = metodologia_EAD;
	}

	public ArrayList<Cartera> getCarteras_extrapolacion() {
		return carteras_extrapolacion;
	}

	public void setCarteras_extrapolacion(ArrayList<Cartera> carteras_extrapolacion) {
		this.carteras_extrapolacion = carteras_extrapolacion;
	}

	public Cartera getCartera() {
		return cartera;
	}

	public void setCartera(Cartera cartera) {
		this.cartera = cartera;
	}

	public TipoFlujo getFlujo() {
		return flujo;
	}

	public void setFlujo(TipoFlujo flujo) {
		this.flujo = flujo;
	}

	public TipoPeriodicidad getPeriodicidad() {
		return periodicidad;
	}

	public void setPeriodicidad(TipoPeriodicidad periodicidad) {
		this.periodicidad = periodicidad;
	}

	public Boolean getFlag_lgdUnica() {
		return flag_lgdUnica;
	}

	public void setFlag_lgdUnica(Boolean flag_lgdUnica) {
		this.flag_lgdUnica = flag_lgdUnica;
	}

	public TipoMetodologia getMetodologia_LGD() {
		return metodologia_LGD;
	}

	public void setMetodologia_LGD(TipoMetodologia metodologia_LGD) {
		this.metodologia_LGD = metodologia_LGD;
	}

	public ArrayList<PrimerPasoConfigurado> getGrupo_primerosPasos() {
		return grupo_primerosPasos;
	}

	public void setGrupo_primerosPasos(ArrayList<PrimerPasoConfigurado> grupo_primerosPasos) {
		this.grupo_primerosPasos = grupo_primerosPasos;
	}

	public ArrayList<Bloque> getBloques_presupuesto() {
		return bloques_presupuesto;
	}

	public void setBloques_presupuesto(ArrayList<Bloque> bloques_presupuesto) {
		this.bloques_presupuesto = bloques_presupuesto;
	}

	public ArrayList<Bloque> getBloques_otros() {
		return bloques_otros;
	}

	public void setBloques_otros(ArrayList<Bloque> bloques_otros) {
		this.bloques_otros = bloques_otros;
	}

	public ArrayList<Bloque> getBloques_extrapolacion() {
		return bloques_extrapolacion;
	}

	public void setBloques_extrapolacion(ArrayList<Bloque> bloques_extrapolacion) {
		this.bloques_extrapolacion = bloques_extrapolacion;
	}

	public ArrayList<Bloque> getBloques_resultDirecto() {
		return bloques_resultDirecto;
	}

	public void setBloques_resultDirecto(ArrayList<Bloque> bloques_resultDirecto) {
		this.bloques_resultDirecto = bloques_resultDirecto;
	}

	public ArrayList<Bloque> getBloques_general() {
		return bloques_general;
	}

	public void setBloques_general(ArrayList<Bloque> bloques_general) {
		this.bloques_general = bloques_general;
	}

	public ArrayList<Bloque> getBloques_escenario() {
		return bloques_escenario;
	}

	public void setBloques_escenario(ArrayList<Bloque> bloques_escenario) {
		this.bloques_escenario = bloques_escenario;
	}

	public Integer getNro_maxPlazo() {
		return nro_maxPlazo;
	}

	public void setNro_maxPlazo(Integer nro_maxPlazo) {
		this.nro_maxPlazo = nro_maxPlazo;
	}

	public String getCodigo_estructura() {
		return codigo_estructura;
	}

	public void setCodigo_estructura(String codigo_estructura) {
		this.codigo_estructura = codigo_estructura;
	}

	public Bloque getBloque_principal() {
		return bloque_principal;
	}

	public void setBloque_principal(Bloque bloque_principal) {
		this.bloque_principal = bloque_principal;
	}

	public Boolean getConDependencias() {
		return conDependencias;
	}

	public void setConDependencias(Boolean conDependencias) {
		this.conDependencias = conDependencias;
	}

	public Boolean getConDependenciasOficial() {
		return conDependenciasOficial;
	}

	public void setConDependenciasOficial(Boolean conDependenciasOficial) {
		this.conDependenciasOficial = conDependenciasOficial;
	}

	public String getNombre_funcPlumbr() {
		return nombre_funcPlumbr;
	}

	public void setNombre_funcPlumbr(String nombre_funcPlumbr) {
		this.nombre_funcPlumbr = nombre_funcPlumbr;
	}

	public Integer getPuerto_plumbr() {
		return puerto_plumbr;
	}

	public void setPuerto_plumbr(Integer puerto_plumbr) {
		this.puerto_plumbr = puerto_plumbr;
	}

}
