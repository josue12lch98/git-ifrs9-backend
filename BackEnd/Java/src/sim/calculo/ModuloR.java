package sim.calculo;

import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Properties;

import sim.persistencia.BloqueParametrizado;
import sim.persistencia.CampoParametrizado;
import sim.persistencia.EjecucionMotorOtro;
import sim.persistencia.bd.ModuloDatos;

public class ModuloR {
	protected static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(ModuloMatlab.class);

	// Datos para la conexion de MATLAB con BBDD
	private static String R_JDBC_userName;
	private static String R_JDBC_encriptado;
	private static String R_JDBC_driver;
	private static String R_JDBC_dbUri;
	private static String R_JDBC_RutaJar;
	private static String R_NombreInic;
	private static String R_PlumbrHost;

	// private ModuloDatos md;

	public ModuloR(Properties properties, ModuloDatos md) throws Exception {
		LOGGER.info("Creando ModuloR");

		// Se leen las propiedades para el DataSource por DRIVER
		R_JDBC_driver = properties.getProperty("JDBC_driverName");
		R_JDBC_userName = properties.getProperty("JDBC_userName");
		R_JDBC_encriptado = properties.getProperty("JDBC_encriptado");
		R_JDBC_dbUri = "jdbc:oracle:thin:@//" + properties.getProperty("JDBC_host") + ":"
				+ properties.getProperty("JDBC_port") + "/" + properties.getProperty("JDBC_serviceName");

		R_JDBC_RutaJar = properties.getProperty("R_JDBC_RutaJar");
		R_NombreInic = properties.getProperty("R_NombreInic");
		R_PlumbrHost = properties.getProperty("R_PlumbrHost");

		LOGGER.info("ModuloR creado");
	}

	public void ejecutarCalculoOtros(EjecucionMotorOtro ejecucion) throws Exception {
		
		LOGGER.info("Definiendo ruta y main");
		String Ruta = ejecucion.getCartera_version().getMetodologia().getCodigo_estructura(); 
		String Ruta_version = Ruta + "\\\\" + ejecucion.getCodmes() % 200000 + "\\\\" 
				+ ejecucion.getCartera_version().getNombre_cartera_version();
	
		// Creamos la carpeta de la version en caso no exista
		new File(Ruta_version).mkdirs();
				
		// Generamos archivo de inicializacion
		PrintWriter writer = new PrintWriter(Ruta_version + "\\" + R_NombreInic + ".r", "UTF-8");
		//writer.println("rm(list = ls()");
		
		// Seteamos identificadores de la ejecucion
		//writer.println("year = '" + (ejecucion.getCodmes() % 200000) / 100); // Formato: '18'
		//writer.println("mes = '" + (ejecucion.getCodmes() % 200000) % 100); // Formato: '12'
		writer.println("date_forecast <- '" + ejecucion.getCodmes() % 200000 + "'"); // Formato: '1903'
		writer.println("subsidiaria <- '" + ejecucion.getCartera_version().getMetodologia()
															.getCartera().getNombre_cartera() + "'");
		writer.println("subsidiaria_ver <- '" + ejecucion.getCartera_version().getNombre_cartera_version() + "'");
		
		writer.println("message(\"Se definieron identificadores del calculo\\n\")");

		// Conexion a BD
		writer.println("library(RJDBC)");
		writer.println("jdriver <- JDBC(\"" + R_JDBC_driver + "\", \"" + R_JDBC_RutaJar.replace("\\", "/")
				+ "\", identifier.quote=\"`\"" + ")"); //Reemplazamos backslash a slash
		writer.println("conn <- dbConnect(jdriver, \"" + R_JDBC_dbUri + "\", \"" + R_JDBC_userName 
											+ "\", \"" + R_JDBC_encriptado + "\")");
		
		writer.println("message(\"Se conecto a la DB\\n\")");

		// Creacion de rutas de input, intermedias y output
		writer.println("ruta_version <- paste(date_forecast, '/', subsidiaria_ver, sep=\"\")");
		writer.println("dir_common <- '" + Ruta.replace("\\", "/") + "'");
		
		writer.println("dir_input <- paste(dir_common, '/', ruta_version, '/input/', sep=\"\")");
		writer.println("dir.create(dir_input)");

		writer.println("dir_inter <- paste(dir_common, '/', ruta_version, '/intermedio/', sep=\"\")");
		writer.println("dir.create(dir_inter)");

		writer.println("dir_output <- paste(dir_common, '/', ruta_version, '/output/', sep=\"\")");
		writer.println("dir.create(dir_output)");
		
		writer.println("message(\"Se crearon rutas de input, intermedio y output\\n\")");

		// Definicion de bloques (de tabla input y valores)
		ArrayList<String> defcampos = null;
		Integer contCampos = null;

		writer.println("source(paste(dir_common, \"/obtener_tabla_oracle.r\", sep=\"\"))");
		for (BloqueParametrizado bloque : ejecucion.getBloquesParam()) {
			if (!bloque.getFlag_tabla_input()) { // bloques de valores

				// Definimos campos de valores (numerico o texto)
				contCampos = 0;
				defcampos = new ArrayList<String>();
				for (CampoParametrizado campo : bloque.getCampos_parametrizado()) {
					if (campo.getCampo_bloque().getTipo_campo().getId_tipo_campo() == 1) { // numerico
						defcampos.add("'" + campo.getCampo_bloque().getVariable_motor() + "','"
								+ campo.getNumValor().toString() + "'");
					} else if (campo.getCampo_bloque().getTipo_campo().getId_tipo_campo() == 2) { // texto
						defcampos.add(
								"'" + campo.getCampo_bloque().getVariable_motor() + "','" + campo.getTxtValor() + "'");
					}
					contCampos++;
				}
				writer.println(bloque.getVariable_motor() + " <- matrix(c(" + String.join(",", defcampos) 
											+ "), nrow = " + contCampos.toString() + ", ncol = 2, byrow = TRUE)");

			} else { // bloques de tabla input

				// Definimos entrada (como fichero intermedio)
				writer.println(bloque.getVariable_motor() + " <- \"" + bloque.getVariable_motor() + "\"");

				// Leemos la tabla input
				writer.println("obtener_tabla_oracle(dir_input, " + bloque.getVariable_motor() + ", '"
						+ R_JDBC_userName + "', '" + bloque.getTabla_input().getNombre_fichero() + "', '"
						+ bloque.getTabla_input().getVersion_metadata().toString() + "', '("
						+ bloque.getCarga().getId_cargafichero().toString() + ")', '"
						+ (bloque.getFiltro_tabla() != null ? bloque.getFiltro_tabla() : "") + "', "
						+ (bloque.getFiltro_tabla() != null ? "TRUE" : "FALSE") + ", conn)");

				// Definimos nombres de campos en tabla
				contCampos = 0;
				defcampos = new ArrayList<String>();
				for (CampoParametrizado campo : bloque.getCampos_parametrizado()) {
					defcampos.add("'" + campo.getCampo_bloque().getVariable_motor() + "', '"
							+ campo.getCampo_input().getNombre_campo().toUpperCase() + "', '"
							+ campo.getCampo_bloque().getTipo_campo().getId_tipo_campo().toString() + "'");
					contCampos++;
				}
				writer.println(bloque.getVariable_motor() + "_CAMP <- matrix(c(" + String.join(",", defcampos) 
									+ "), nrow = " + contCampos.toString() + ", ncol = 3, byrow = TRUE)");

			}
		}
		writer.println("message(\"Se definieron todos los bloques requeridos\\n\")");
		
		writer.close();

		//Invocamos al metodo de Plumbr como cliente http
			//Seteamos la ruta del archivo de inicializacion como parametro
		
		//Generamos ruta version URL safe
		URLConnection conn = new URL(R_PlumbrHost + ":" 
				+ ejecucion.getCartera_version().getMetodologia().getPuerto_plumbr().toString() + "/" 
				+ ejecucion.getCartera_version().getMetodologia().getNombre_funcPlumbr() + "?" + String.format(
				"rutaInic=%s", URLEncoder.encode(Ruta_version.replace("\\", "/") 
						+ "//" + R_NombreInic + ".r", "UTF-8").replace("%2F", "/").replace("%3A", ":")))
				.openConnection();
				//El encoder pasa los caracteres "/" a "%2F"!
				//El encoder pasa los caracteres ":" a "%3A"!
		conn.setRequestProperty("Accept-Charset", "UTF-8");
		InputStream res = conn.getInputStream();		
		LOGGER.info(res);
	}

}
