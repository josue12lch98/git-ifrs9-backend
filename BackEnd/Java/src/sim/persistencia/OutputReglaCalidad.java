package sim.persistencia;

public class OutputReglaCalidad {
	private ReglaCalidad reglaCalidad;
	private Integer numResultado;
	private Boolean flgVerde;
	private Boolean flgAmbar;
	private Boolean flgRojo;

	public OutputReglaCalidad(ReglaCalidad reglaCalidad, Integer numResultado, Boolean flgVerde, Boolean flgAmbar,
			Boolean flgRojo) {
		super();
		this.reglaCalidad = reglaCalidad;
		this.numResultado = numResultado;
		this.flgVerde = flgVerde;
		this.flgAmbar = flgAmbar;
		this.flgRojo = flgRojo;
	}

	public ReglaCalidad getReglaCalidad() {
		return reglaCalidad;
	}

	public void setReglaCalidad(ReglaCalidad reglaCalidad) {
		this.reglaCalidad = reglaCalidad;
	}

	public Integer getNumResultado() {
		return numResultado;
	}

	public void setNumResultado(Integer numResultado) {
		this.numResultado = numResultado;
	}

	public Boolean getFlgVerde() {
		return flgVerde;
	}

	public void setFlgVerde(Boolean flgVerde) {
		this.flgVerde = flgVerde;
	}

	public Boolean getFlgAmbar() {
		return flgAmbar;
	}

	public void setFlgAmbar(Boolean flgAmbar) {
		this.flgAmbar = flgAmbar;
	}

	public Boolean getFlgRojo() {
		return flgRojo;
	}

	public void setFlgRojo(Boolean flgRojo) {
		this.flgRojo = flgRojo;
	}

}
