package sim.calculo;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Properties;

import com.mathworks.engine.MatlabEngine;

import sim.persistencia.BloqueParametrizado;
import sim.persistencia.CampoParametrizado;
import sim.persistencia.EjecucionMotor;
import sim.persistencia.GrupoBloquesEscenario;
import sim.persistencia.bd.ModuloDatos;

public class ModuloMatlab {
	protected static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(ModuloMatlab.class);

	// Datos para la conexion de MATLAB con BBDD
	private static String MATLAB_JDBC_dataSource;
	private static String MATLAB_JDBC_schema;
	private static String MATLAB_JDBC_userName;
	private static String MATLAB_JDBC_encriptado;
	private static String MATLAB_JDBC_driver;
	private static String MATLAB_JDBC_dbUri;
	private static String MATLAB_JDBC_RutaJar;
	private static String MATLAB_NombreMain;
	private static String MATLAB_NombreInic;
	//private static String MATLAB_NombreResult;
	//private static MatlabEngine MATLAB_Engine;

	// private ModuloDatos md;

	public ModuloMatlab(Properties properties, ModuloDatos md) throws Exception {
		LOGGER.info("Creando ModuloMatlab");

		// Se leen las propiedades para el DataSource por DRIVER
		MATLAB_JDBC_driver = properties.getProperty("JDBC_driverName");
		MATLAB_JDBC_dataSource = properties.getProperty("JDBC_serviceName");
		MATLAB_JDBC_schema = properties.getProperty("JDBC_schema");
		MATLAB_JDBC_userName = properties.getProperty("JDBC_userName");
		MATLAB_JDBC_encriptado = properties.getProperty("JDBC_encriptado");
		MATLAB_JDBC_dbUri = "jdbc:oracle:thin:" + properties.getProperty("JDBC_userName") + "/"
				+ properties.getProperty("JDBC_encriptado") + "@" + properties.getProperty("JDBC_host") + ":"
				+ properties.getProperty("JDBC_port") + "/" + properties.getProperty("JDBC_sid");		
		//":" + properties.getProperty("JDBC_sid"); 

		MATLAB_JDBC_RutaJar = properties.getProperty("MATLAB_JDBC_RutaJar");
		MATLAB_NombreMain = properties.getProperty("MATLAB_NombreMain");
		MATLAB_NombreInic = properties.getProperty("MATLAB_NombreInic");
		//MATLAB_NombreResult = properties.getProperty("MATLAB_NombreResult");

		// Iniciamos matlab engine
		/*try {
			MATLAB_Engine = MatlabEngine.startMatlab();
		} catch (Exception ex) {
			LOGGER.fatal("Error al iniciar el matlab engine: " + ex.getMessage());
		}*/

		LOGGER.info("ModuloMatlab creado");
	}

	public void cerrarMatlabEngine() throws Exception {
		//MATLAB_Engine.close();
	}

	public void ejecutarCalculoInversiones(EjecucionMotor ejecucion) throws Exception {
		LOGGER.info("Definiendo ruta y main");
		String Ruta = ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia()
				.getCodigo_estructura(); 	
		
		String Ruta_version = Ruta + "\\\\" + ejecucion.getCodmes() % 200000 
							+ "\\\\" + ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia().getCartera().getNombre_cartera()
							+ "\\\\" + ejecucion.getJuego_escenarios_version().getCartera_version().getNombre_cartera_version()
							+ "\\\\" + ejecucion.getJuego_escenarios_version().getNombre_juego_escenarios();

		// Creamos la carpeta de la version en caso no exista
		new File(Ruta_version).mkdirs();
		
		
		// Generamos archivo de inicializacion
		PrintWriter writer = null;
		SimpleDateFormat df = null;
		try { 
			writer = new PrintWriter(Ruta_version + "\\\\" + MATLAB_NombreInic + ".m", "UTF-8");
			//writer.println("diary(" + "strcat('" + Ruta_version + "', '\\\\log_calc.txt'));");
			writer.println("clear all;");
			writer.println("clc;");
			
			// Para mostrado detallado de output (log)
			//writer.println("echo on");
			//writer.println("echo on all");
			
			//Logueo de timestamp
			df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
			writer.println("fprintf(\"	Timestamp de ejecucion: " 
											+ df.format((new Date(System.currentTimeMillis()))) + "\\n\\n\");");
	
			// Seteamos identificadores de la ejecucion
			writer.println("fecha = '" + ejecucion.getCodmes() % 200000 + "';"); // Formato: '1901'
			writer.println("subsidiaria = '" + ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia()
					.getCartera().getNombre_cartera() + "';");
			writer.println("subsidiaria_ver = '"
					+ ejecucion.getJuego_escenarios_version().getCartera_version().getNombre_cartera_version() + "';");
			writer.println("juego_ver = '" + ejecucion.getJuego_escenarios_version().getNombre_juego_escenarios() + "';");
	
			// Parametros para escribir en tabla output
			writer.println("tipo_ejecucion = '" + (ejecucion.getTipo_ejecucion().getId_tipo_ejecucion() - 6) + "';"); 
											// Se usa como fase (1 o 2)
			writer.println(
					"codigo_juego_ver = '" + ejecucion.getJuego_escenarios_version().getId_juego_escenarios() + "';");
			writer.println("codigo_subsidiaria_ver = '"
					+ ejecucion.getJuego_escenarios_version().getCartera_version().getId_cartera_version() + "';");
			writer.println("codigo_subsidiaria = '" + ejecucion.getJuego_escenarios_version().getCartera_version()
					.getMetodologia().getCartera().getId_cartera() + "';");
			writer.println("codigo_entidad = '" + ejecucion.getJuego_escenarios_version().getCartera_version()
					.getMetodologia().getCartera().getEntidad().getId_entidad() + "';");
			
			writer.println("fprintf(\"Se definieron identificadores del calculo\\n\");");
	
			// Seteamos pesos de escenarios
			ArrayList<String> pesos_escenariosArr = new ArrayList<String>();
	
			for (GrupoBloquesEscenario grupoEsc : ejecucion.getGrupos_bloquesParamEscenario()) {
				if (grupoEsc.getEscenario_version().getEscenario().getNombre_escenario().equalsIgnoreCase("Base")) {
	
					// Parametros para escribir en tabla output, solo un resultado para base
					// (ponderado)
					writer.println("codigo_ejecucion = '" + grupoEsc.getId_ejecucion() + "';");
					writer.println(
							"codigo_escenario_ver = '" + grupoEsc.getEscenario_version().getId_escenario_version() + "';");
					writer.println("codigo_escenario = '" + grupoEsc.getEscenario_version().getEscenario().getId_escenario()
							+ "';");
	
					pesos_escenariosArr.add("'W2', '" + grupoEsc.getEscenario_version().getPeso() / 100 + "'");
				} else if (grupoEsc.getEscenario_version().getEscenario().getNombre_escenario()
						.equalsIgnoreCase("Adverso")) {
					pesos_escenariosArr.add("'W1', '" + grupoEsc.getEscenario_version().getPeso() / 100 + "'");
				} else if (grupoEsc.getEscenario_version().getEscenario().getNombre_escenario()
						.equalsIgnoreCase("Optimo")) {
					pesos_escenariosArr.add("'W3', '" + grupoEsc.getEscenario_version().getPeso() / 100 + "'");
				}
			}
	
			writer.println("W = {" + String.join(";", pesos_escenariosArr) + "};");
			writer.println("fprintf(\"Se definieron los pesos de escenarios\\n\");");
			
			// Conexion a BD
			writer.println("javaclasspath('" + MATLAB_JDBC_RutaJar + "');");
			writer.println("datasource = '" + MATLAB_JDBC_dataSource + "';");
			writer.println("schema = '" + MATLAB_JDBC_schema + "';");
			writer.println("username = '" + MATLAB_JDBC_userName + "';");
			writer.println("encriptado = '" + MATLAB_JDBC_encriptado + "';");
			//writer.println("% encriptado = '" + String.join("", Collections.nCopies(MATLAB_JDBC_encriptado.length(), "*")) + "';");			
			writer.println("driver = '" + MATLAB_JDBC_driver + "';");
			writer.println("url = '" + MATLAB_JDBC_dbUri + "';");
			writer.println("conn = database(datasource, username, encriptado, driver, url);");
			writer.println("fprintf(\"Se conecto a la DB\\n\");");
	
			// Cambio a ruta a codigo
			writer.println("cd('" + Ruta + "');");
	
			// Creacion de rutas de input, intermedias y output
			writer.println("ruta_version = strcat(fecha, '\\', subsidiaria, '\\', subsidiaria_ver, '\\', juego_ver);");
			writer.println("dir_input = strcat(pwd, '\\', ruta_version, '\\input\\');");
			writer.println("if exist(dir_input, 'dir') == 7 rmdir(dir_input, 's'); end"); //Borramos por si existen archivos
			writer.println("mkdir(dir_input);");
	
			writer.println("dir_inter = strcat(pwd, '\\', ruta_version, '\\intermedio\\');");
			writer.println("if exist(dir_inter, 'dir') == 7 rmdir(dir_inter, 's'); end"); //Borramos por si existen archivos
			writer.println("mkdir(dir_inter);");
	
			writer.println("dir_output = strcat(pwd, '\\', ruta_version, '\\output\\');");
			writer.println("if exist(dir_output, 'dir') == 7 rmdir(dir_output, 's'); end"); //Borramos por si existen archivos
			writer.println("mkdir(dir_output);");
			
			writer.println("fprintf(\"Se crearon las rutas de input, intermedio y output\\n\");");
	
			//Espacio para log
			writer.println("fprintf(\"\\n\");");
			
			// Definicion de bloques (de tabla input y valores)
			ArrayList<String> defcampos = null;
	
			for (BloqueParametrizado bloque : ejecucion.getBloquesParam_carteraVersion()) {
				if (!bloque.getFlag_tabla_input()) { // bloques de valores
	
					//Logueo
					writer.println("fprintf(\"Se define el input de valores " + bloque.getVariable_motor() + "\\n\");");
	
					// Definimos campos de valores (numerico o texto)
					defcampos = new ArrayList<String>();
					for (CampoParametrizado campo : bloque.getCampos_parametrizado()) {
						if (campo.getCampo_bloque().getTipo_campo().getId_tipo_campo() == 1
								&& campo.getNumValor() != null) { // numerico
							defcampos.add("'" + campo.getCampo_bloque().getVariable_motor() + "','"
									+ campo.getNumValor().toString() + "'");
							
							//Logueo
							writer.println("fprintf(\"	- Variable:  " + campo.getCampo_bloque().getVariable_motor() 
															+ " | Valor: " + campo.getNumValor().toString() + "\\n\");");
							
						} else if (campo.getCampo_bloque().getTipo_campo().getId_tipo_campo() == 2
								&& campo.getTxtValor() != null) { // texto
							defcampos.add(
									"'" + campo.getCampo_bloque().getVariable_motor() + "','" + campo.getTxtValor() + "'");
							
							//Logueo
							writer.println("fprintf(\"	- Variable:  " + campo.getCampo_bloque().getVariable_motor() 
															+ " | Valor: " + campo.getTxtValor() + "\\n\");");
						}	
					}
					writer.println(bloque.getVariable_motor() + " = {" + String.join(";", defcampos) + "};");					
					
					//Espacio para log
					writer.println("fprintf(\"\\n\");");
					
				} else { // bloques de tabla input
	
					// Definimos entrada (como fichero intermedio)
					/*
					 * eng.eval(bloque.getVariable_motor() + "_in = strcat('" +
					 * bloque.getVariable_motor() + "_in', '_" + ejecucion.getCodmes() + "', '_" +
					 * ejecucion.getJuego_escenarios_version().getCartera_version().getMetodologia()
					 * .getCartera() .getNombre_cartera() + "', '_" +
					 * ejecucion.getJuego_escenarios_version().getCartera_version().
					 * getNombre_cartera_version() + "', '_" +
					 * ejecucion.getJuego_escenarios_version().getNombre_juego_escenarios() + "')");
					 */
					
					//Logueo
					writer.println("fprintf(\"Se define el input " + bloque.getVariable_motor() 
										+ " asociado a [fichero: " 
										+ bloque.getTabla_input().getNombre_fichero() + " | metadata: "
										+ bloque.getTabla_input().getNombre_metadata() + " | carga: "
										+ bloque.getCarga().getNombre_carga() + "]\\n\");");
										
					writer.println(bloque.getVariable_motor() + " = '" + bloque.getVariable_motor() + "';");
	
					// Leemos la tabla input
					ArrayList<String> codigos_carga = new ArrayList<String>();
					codigos_carga.add(bloque.getCarga().getId_cargafichero().toString());
	
					// Agregamos cargas historicas para el bloque principal
					/*if (bloque.getTipo_bloque().getNombre_tipo_bloque().equalsIgnoreCase("Principal")) {
						for (EjecucionHistorica ejecHist : ejecucion.getEjecuciones_hist()) {
							codigos_carga.add(ejecHist.getCargaBloqPrincipal_hist().getId_cargafichero().toString());
						}
					}*/
	
					writer.println("obtener_tabla_oracle(dir_input, " + bloque.getVariable_motor() + ", '"
							+ MATLAB_JDBC_userName + "', '" + bloque.getTabla_input().getNombre_fichero() + "', '"
							+ bloque.getTabla_input().getVersion_metadata().toString() + "', '("
							+ String.join(",", codigos_carga) + ")', '"
							+ (bloque.getFiltro_tabla() != null ? bloque.getFiltro_tabla() : "") + "', "
							+ (bloque.getFiltro_tabla() != null ? "true" : "false") + ", "
							+ (bloque.getFormato_input().getNombre_formato_input().equals("txt") ? "true" : "false")
							+ ", conn);");
					
					//Espacio para log
					writer.println("fprintf(\"\\n\");");
					
					// Definimos nombres de campos en tabla
					defcampos = new ArrayList<String>();
					for (CampoParametrizado campo : bloque.getCampos_parametrizado()) {
						if (campo.getCampo_input().getNombre_campo() != null) {
							defcampos.add("'" + campo.getCampo_bloque().getVariable_motor() + "', '"
									+ campo.getCampo_input().getNombre_campo().toUpperCase() + "', '"
									+ campo.getCampo_bloque().getTipo_campo().getId_tipo_campo().toString() + "'");
						}					
					}
					writer.println(bloque.getVariable_motor() + "_CMP = {" + String.join(";", defcampos) + "};");
	
				}
			}
			writer.println("fprintf(\"Se definieron todos los bloques necesarios\\n\");");
			//writer.println("diary ('off');");			
		} catch (Exception ex) {
			LOGGER.error("Error al generar archivo de incializacion MATLAB: " + ex);
			throw ex;
		} finally {
			writer.close();
			LOGGER.info("Termina de escribir archivo de inicializacion");
		}
						
		MatlabEngine MATLAB_Engine = null;
		Writer out = null;
		Writer err = null;
		try {
			//Crea archivos para los Logs
			out = new FileWriter(Ruta_version + "\\\\log_calc.txt");
			err = new FileWriter(Ruta_version + "\\\\log_err.txt");
			
			LOGGER.info("Archivos logs creados en:" + Ruta_version);
			
			// Ejecucion matlab
			MATLAB_Engine = MatlabEngine.startMatlab();
			
			MATLAB_Engine.eval("fprintf(\"Ejecutando Matlab\\n\")", out, err);
		
			// Configuramos path para el codigo e inicializacion
			MATLAB_Engine.eval("addpath(genpath('" + Ruta + "'))", out, err); // Las funciones deberian estar en la misma ruta
			MATLAB_Engine.eval("addpath(genpath('" + Ruta_version + "'))", out, err);
			
			//Captura de logs con diary (rodeamos con try-catch)
			//MATLAB_Engine.eval("diary(" + "strcat('" + Ruta_version + "', '\\log_calc.txt'))");	
			//MATLAB_Engine.eval("try"); 		
			
			LOGGER.info("Evalua inicializacion de la herramienta");				
			MATLAB_Engine.eval("fprintf(\"Inicializando codigo\\n\")", out, err);
			//MATLAB_Engine.eval(Ruta_version + "\\\\" + MATLAB_NombreInic);
			//MATLAB_Engine.putVariable("encriptado", MATLAB_JDBC_encriptado); //Para no mostrar pwd
			MATLAB_Engine.eval(MATLAB_NombreInic, out, err);			
			MATLAB_Engine.eval("fprintf(\"Termino de inicializar el codigo\\n\")", out, err);
		
			LOGGER.info("Evalua main");
			MATLAB_Engine.eval("fprintf(\"Ejecutando codigo\\n\")", out, err);		
			MATLAB_Engine.eval(MATLAB_NombreMain, out, err);
			
			MATLAB_Engine.eval("fprintf(\"Termino el calculo PE en Matlab\\n\")", out, err);
			LOGGER.info("Finaliza calculo matlab");
		} catch (Exception ex) {
			LOGGER.error("Error durante lanzamiento del software MATLAB: " + ex);
			throw ex;			
		} finally {
			//Cerramos writers para logs y el matlab engine
			MATLAB_Engine.quit();
			out.close();
			err.close();
		}


	}

}
