package sim.persistencia;

import sim.persistencia.dic.TipoAmbito;

public class Permiso {

	private int id_permiso;
	private String nombre_permiso;	
	private int nivel;
	private String nombre_permiso_padre;
	private boolean flag_boton;
	private boolean flag_ultimaJerarquia;
	private String ruta;
	private TipoAmbito tipo_ambito;
	
	private String nombre_traducido; //Solo para front-end

	public Permiso() {
	}

	public int getId_permiso() {
		return id_permiso;
	}

	public void setId_permiso(int id_permiso) {
		this.id_permiso = id_permiso;
	}

	public String getNombre_permiso() {
		return nombre_permiso;
	}

	public void setNombre_permiso(String nombre_permiso) {
		this.nombre_permiso = nombre_permiso;
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}	

	public boolean isFlag_boton() {
		return flag_boton;
	}

	public void setFlag_boton(boolean flag_boton) {
		this.flag_boton = flag_boton;
	}

	public boolean isFlag_ultimaJerarquia() {
		return flag_ultimaJerarquia;
	}

	public void setFlag_ultimaJerarquia(boolean flag_ultimaJerarquia) {
		this.flag_ultimaJerarquia = flag_ultimaJerarquia;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getNombre_traducido() {
		return nombre_traducido;
	}

	public void setNombre_traducido(String nombre_traducido) {
		this.nombre_traducido = nombre_traducido;
	}

	public String getNombre_permiso_padre() {
		return nombre_permiso_padre;
	}

	public void setNombre_permiso_padre(String nombre_permiso_padre) {
		this.nombre_permiso_padre = nombre_permiso_padre;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}

	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}

}
