package sim.persistencia;

import java.util.ArrayList;

public class ReplicaEjecucionesAgregadasPE {
	private Integer codmes_replica;	
	private ArrayList<EjecucionAgregadaPE> ejecuciones_replicar;
	
	//Solo se usa en simulaciones para inversiones
	private Integer fase_replica;
	
	public ReplicaEjecucionesAgregadasPE() {		
	}

	public Integer getCodmes_replica() {
		return codmes_replica;
	}

	public void setCodmes_replica(Integer codmes_replica) {
		this.codmes_replica = codmes_replica;
	}

	public ArrayList<EjecucionAgregadaPE> getEjecuciones_replicar() {
		return ejecuciones_replicar;
	}

	public void setEjecuciones_replicar(ArrayList<EjecucionAgregadaPE> ejecuciones_replicar) {
		this.ejecuciones_replicar = ejecuciones_replicar;
	}

	public Integer getFase_replica() {
		return fase_replica;
	}

	public void setFase_replica(Integer fase_replica) {
		this.fase_replica = fase_replica;
	}
}
