package sim.ws.rest;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import sim.ws.rest.JwtRequiereToken;
import sim.ws.rest.JwtRefrescaToken;

import sim.Contenedor;
import sim.persistencia.AjusteGasto;
import sim.persistencia.CampoInput;
import sim.persistencia.CampoOutputCredito;
import sim.persistencia.CargaFichero;
import sim.persistencia.Cartera;
import sim.persistencia.Cartera_version;
import sim.persistencia.ConsultaRegistrosOutput;
import sim.persistencia.EjecucionAgregadaPE;
import sim.persistencia.EjecucionAjustes;
import sim.persistencia.EjecucionOtro;
import sim.persistencia.EjecucionPE;
import sim.persistencia.EjecucionPrimerPaso;
import sim.persistencia.EjecucionValidacion;
import sim.persistencia.EjecucionVariacion;
import sim.persistencia.Entidad;
import sim.persistencia.Escenario_version;
import sim.persistencia.EstructuraMetodologica;
import sim.persistencia.GrupoMapeosDelta;
import sim.persistencia.GrupoRegistrosOutput;
import sim.persistencia.InfoValidacionAjustes;
import sim.persistencia.JuegoEscenarios_version;
import sim.persistencia.LogCodInicializacion;
import sim.persistencia.LogEjecEscenario;
import sim.persistencia.MapeoCargasModMacEjec;
import sim.persistencia.MapeoDeltaStage;
import sim.persistencia.OutputModeloMacro;
import sim.persistencia.PrimerPaso;
import sim.persistencia.RegistroOutputCredito;
import sim.persistencia.ReplicaEjecucionesAgregadasPE;
import sim.persistencia.ReplicaEjecucionesOtros;
import sim.persistencia.ReplicaEjecucionesPrimerPaso;
import sim.persistencia.Stage;
import sim.persistencia.TablaMapeoEjecucion;
import sim.persistencia.dic.TipoEjecucion;

@Path("/ejecuciones")
public class EjecucionesService {

	private static Contenedor contenedor = Contenedor.obtenerInstancia();

	// -------------------------------------------------------------------
	// --------------------------EJECUCIONES PE---------------------------
	// -------------------------------------------------------------------

	@GET
	@Path("/obtenerEntidades")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Entidad> obtenerEntidades() throws Exception {
		ArrayList<Entidad> entidades = new ArrayList<Entidad>();
		entidades = contenedor.obtenerEntidades();
		return entidades;
	}

	@GET
	@Path("/obtenerCarterasPorIdEntidad/{id_entidad}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Cartera> obtenerCarterasPorIdEntidad(@PathParam("id_entidad") Integer id_entidad)
			throws Exception {
		ArrayList<Cartera> carteras = new ArrayList<Cartera>();
		carteras = contenedor.obtenerCarterasPorIdEntidad(id_entidad);
		return carteras;
	}

	@GET
	@Path("/obtenerEstructurasPorIdCartera/{id_cartera}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EstructuraMetodologica> obtenerEstructurasPorIdCartera(@PathParam("id_cartera") Integer id_cartera)
			throws Exception {
		ArrayList<EstructuraMetodologica> estructuras = new ArrayList<EstructuraMetodologica>();
		estructuras = contenedor.obtenerEstructurasPorIdCartera(id_cartera);
		return estructuras;
	}
	
	@GET
	@Path("/obtenerEstructurasCalcPePorIdCartera/{id_cartera}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EstructuraMetodologica> obtenerEstructurasCalcPePorIdCartera(@PathParam("id_cartera") Integer id_cartera)
			throws Exception {
		ArrayList<EstructuraMetodologica> estructuras = new ArrayList<EstructuraMetodologica>();
		estructuras = contenedor.obtenerEstructurasCalcPePorIdCartera(id_cartera);
		return estructuras;
	}

	@GET
	@Path("/obtenerCarterasVersionPorIdMetodologia/{id_metodologia}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Cartera_version> obtenerCarterasVersionPorIdMetodologia(
			@PathParam("id_metodologia") Integer id_metodologia) throws Exception {
		ArrayList<Cartera_version> carterasVersion = new ArrayList<Cartera_version>();
		carterasVersion = contenedor.obtenerCarterasVersionPorIdMetodologia(id_metodologia);
		return carterasVersion;
	}

	@GET
	@Path("/obtenerJuegoVigentePorCartera/{id_cartera}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public JuegoEscenarios_version obtenerJuegoVigentePorCartera(@PathParam("id_cartera") Integer id_cartera)
			throws Exception {
		JuegoEscenarios_version juegoEscenarios = new JuegoEscenarios_version();
		juegoEscenarios = contenedor.obtenerJuegoVigentePorCartera(id_cartera);
		return juegoEscenarios;
	}
	
	@GET
	@Path("/obtenerJuegoVigentePorCarteraFase2/{id_cartera}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public JuegoEscenarios_version obtenerJuegoVigentePorCarteraFase2(@PathParam("id_cartera") Integer id_cartera)
			throws Exception {
		JuegoEscenarios_version juegoEscenarios = new JuegoEscenarios_version();
		juegoEscenarios = contenedor.obtenerJuegoVigentePorCarteraFase2(id_cartera);
		return juegoEscenarios;
	}

	@GET
	@Path("/obtenerEjecucionFase1OficialPorCarteraCodmes/{id_cartera}/{codmes}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public EjecucionAgregadaPE obtenerEjecucionFase1OficialPorCarteraCodmes(@PathParam("id_cartera") Integer id_cartera,
			@PathParam("codmes") Integer codmes) throws Exception {
		EjecucionAgregadaPE ejecucion = new EjecucionAgregadaPE();
		ejecucion = contenedor.obtenerEjecucionFase1OficialPorCarteraCodmes(id_cartera, codmes);
		return ejecucion;
	}

	@GET
	@Path("/obtenerEjecucionesBaseFase1OficialPorCartera/{id_cartera}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionPE> obtenerEjecucionesBaseFase1OficialPorCartera(
			@PathParam("id_cartera") Integer id_cartera) throws Exception {
		ArrayList<EjecucionPE> ejecuciones = new ArrayList<EjecucionPE>();
		ejecuciones = contenedor.obtenerEjecucionesBaseFase1OficialPorCartera(id_cartera);
		return ejecuciones;
	}
	
	@GET
	@Path("/obtenerEjecucionesBaseOficialPorCarteraCodmesFase/{id_cartera}/{codmes}/{fase}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionPE> obtenerEjecucionesBaseOficialPorCarteraCodmesFase(
			@PathParam("id_cartera") Integer id_cartera, @PathParam("codmes") Integer codmes,
			@PathParam("fase") Integer fase) throws Exception {
		ArrayList<EjecucionPE> ejecuciones = new ArrayList<EjecucionPE>();
		ejecuciones = contenedor.obtenerEjecucionesBaseOficialPorCarteraCodmesFase(id_cartera, codmes, fase);
		return ejecuciones;
	}
	
	@GET
	@Path("/obtenerTablasMapeoVariacPorIdEjecVar/{id_ejecucion}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoVariacPorIdEjecVar(
			@PathParam("id_ejecucion") Integer id_ejecucion) throws Exception {
		ArrayList<TablaMapeoEjecucion> tablasMapeoEjecucion = new ArrayList<TablaMapeoEjecucion>();
		contenedor = Contenedor.obtenerInstancia();
		tablasMapeoEjecucion = contenedor.obtenerTablasMapeoVariacPorIdEjecVar(id_ejecucion);
		return tablasMapeoEjecucion;
	}


	@GET
	@Path("/obtenerTiposEjecucion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TipoEjecucion> obtenerTiposEjecucion() throws Exception {
		ArrayList<TipoEjecucion> tiposEjecucion = new ArrayList<TipoEjecucion>();
		tiposEjecucion = contenedor.obtenerTiposEjecucion();
		return tiposEjecucion;
	}

	@GET
	@Path("/obtenerCodmesActual_fase1/{id_tipo_ambito}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public Integer obtenerCodmesActual_fase1(@PathParam("id_tipo_ambito") Integer id_tipo_ambito) throws Exception {
		Integer codmesActual = null;
		codmesActual = contenedor.obtenerCodmesActual_fase1(id_tipo_ambito);
		return codmesActual;
	}

	@GET
	@Path("/obtenerCodmesActual_fase2/{id_tipo_ambito}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public Integer obtenerCodmesActual_fase2(@PathParam("id_tipo_ambito") Integer id_tipo_ambito) throws Exception {
		Integer codmesActual = null;
		codmesActual = contenedor.obtenerCodmesActual_fase2(id_tipo_ambito);
		return codmesActual;
	}

	@GET
	@Path("/obtenerCodmesActualPorFase/{fase}/{id_tipo_ambito}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public Integer obtenerCodmesActualPorFase(@PathParam("fase") Integer fase, 
			@PathParam("id_tipo_ambito") Integer id_tipo_ambito) throws Exception {
		Integer codmesActual = null;
		codmesActual = contenedor.obtenerCodmesActualPorFase(fase, id_tipo_ambito);
		return codmesActual;
	}

	@GET
	@Path("/obtenerJuegosEscenariosVersionPorIdCarteraVersion/{id_cartera_version}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<JuegoEscenarios_version> obtenerJuegosEscenariosVersionPorIdCarteraVersion(
			@PathParam("id_cartera_version") Integer id_cartera_version) throws Exception {
		ArrayList<JuegoEscenarios_version> juegoEscenariosVersion = new ArrayList<JuegoEscenarios_version>();
		juegoEscenariosVersion = contenedor.obtenerJuegosEscenariosVersionPorIdCarteraVersion(id_cartera_version);
		return juegoEscenariosVersion;
	}

	@GET
	@Path("/obtenerEjecucionesModMacPorMetodologia/{id_metodologia}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionPrimerPaso> obtenerEjecucionesPrimerPasoPorMetodologia(
			@PathParam("id_metodologia") Integer id_metodologia) throws Exception {
		ArrayList<EjecucionPrimerPaso> ejecuciones = new ArrayList<EjecucionPrimerPaso>();
		ejecuciones = contenedor.obtenerEjecucionesModMacPorMetodologia(id_metodologia);
		return ejecuciones;
	}

	@GET
	@Path("/obtenerEjecucionesRepricingPorMetodologia/{id_metodologia}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionPrimerPaso> obtenerEjecucionesRepricingPorMetodologia(
			@PathParam("id_metodologia") Integer id_metodologia) throws Exception {
		ArrayList<EjecucionPrimerPaso> ejecuciones = new ArrayList<EjecucionPrimerPaso>();
		ejecuciones = contenedor.obtenerEjecucionesRepricingPorMetodologia(id_metodologia);
		return ejecuciones;
	}

	@GET
	@Path("/obtenerEscenariosVersionPorIdJuegoEscenarios/{id_juego_escenarios}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Escenario_version> obtenerEscenariosVersionPorIdJuegoEscenarios(
			@PathParam("id_juego_escenarios") Integer id_juego_escenarios) throws Exception {
		ArrayList<Escenario_version> escenariosVersion = new ArrayList<Escenario_version>();
		escenariosVersion = contenedor.obtenerEscenariosVersionPorIdJuegoEscenarios(id_juego_escenarios);
		return escenariosVersion;
	}

	@PUT
	@Path("/ejecutarCalculo_fase1")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void ejecutarCalculo_fase1(EjecucionAgregadaPE ejecucionPE) throws Exception {
		contenedor.validarInjectObjeto(ejecucionPE, true, true);
		contenedor.ejecutarCalculo_fase1(ejecucionPE);
	}

	@PUT
	@Path("/ejecutarSimulacion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void ejecutarSimulacion(EjecucionAgregadaPE ejecucionPE) throws Exception {
		contenedor.validarInjectObjeto(ejecucionPE, true, true);
		contenedor.ejecutarSimulacion(ejecucionPE);
	}

	@PUT
	@Path("/ejecutarCalculo_fase2")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void ejecutarCalculo_fase2(EjecucionAgregadaPE ejecucionPE) throws Exception {
		contenedor.validarInjectObjeto(ejecucionPE, true, true);
		contenedor.ejecutarCalculo_fase2(ejecucionPE);
	}

	// -------------------------------------------------------------------
	// -----------------------EJECUCIONES VARIACION-----------------------
	// -------------------------------------------------------------------

	@GET
	@Path("/obtenerEjecucionesVariacion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionVariacion> obtenerEjecucionesVariacion() throws Exception {
		ArrayList<EjecucionVariacion> ejecucionesVariacion = new ArrayList<EjecucionVariacion>();
		ejecucionesVariacion = contenedor.obtenerEjecucionesVariacion();
		return ejecucionesVariacion;
	}

	@POST
	@Path("/crearEjecucionVariacion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearEjecucionVariacion(EjecucionVariacion ejecucionVariacion) throws Exception {
		contenedor.validarInjectObjeto(ejecucionVariacion, true, true);
		contenedor.crearEjecucionVariacion(ejecucionVariacion);
	}

	@PUT
	@Path("/editarEjecucionVariacion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarEjecucionVariacion(EjecucionVariacion ejecucionVariacion) throws Exception {
		contenedor.validarInjectObjeto(ejecucionVariacion, true, true);
		contenedor.editarEjecucionVariacion(ejecucionVariacion);
	}

	@PUT
	@Path("/ejecutarVariacion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void ejecutarVariacion(EjecucionVariacion ejecucionVariacion) throws Exception {
		contenedor.validarInjectObjeto(ejecucionVariacion, true, true);
		contenedor.ejecutarVariacion(ejecucionVariacion);
	}

	@DELETE
	@Path("/borrarEjecucionVariacion/{id}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarEntidad(@PathParam("id") Integer id_ejecucion) throws Exception {
		contenedor.borrarEjecucionVariacion(id_ejecucion);
	}

	// -------------------------------------------------------------------
	// ---------------------EJECUCIONES VALIDACION------------------------
	// -------------------------------------------------------------------

	@GET
	@Path("/obtenerEjecucionesValidacion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionValidacion> obtenerEjecucionesValidacion() throws Exception {
		ArrayList<EjecucionValidacion> ejecucionesValidacion = new ArrayList<EjecucionValidacion>();
		ejecucionesValidacion = contenedor.obtenerEjecucionesValidacion();
		return ejecucionesValidacion;
	}
	
	@GET
	@Path("/obtenerLogsEjecucionAgregada/{codmes}/{id_juego_escenarios}/{fase}/{id_tipo_ambito}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<LogEjecEscenario> obtenerLogsEjecucionAgregada(@PathParam("codmes") Integer codmes, 
			@PathParam("id_juego_escenarios") Integer id_juego_escenarios, @PathParam("fase") Integer fase, 
			@PathParam("id_tipo_ambito") Integer id_tipo_ambito) throws Exception {
		ArrayList<LogEjecEscenario> logs = new ArrayList<LogEjecEscenario>();
		logs = contenedor.obtenerLogsEjecucionAgregada(codmes, id_juego_escenarios, fase, id_tipo_ambito);
		return logs;
	}
	
	@GET
	@Path("/obtenerLogEjecucionPorIdEjec/{id_ejecucion}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public LogEjecEscenario obtenerLogEjecucionPorIdEjec(@PathParam("id_ejecucion") Integer id_ejecucion) throws Exception {
		contenedor = Contenedor.obtenerInstancia();
		return contenedor.obtenerLogEjecucionPorIdEjec(id_ejecucion);
	}

	@PUT
	@Path("/editarEjecucionValidacion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarEjecucionValidacion(EjecucionValidacion ejecucionValidacion) throws Exception {
		contenedor.validarInjectObjeto(ejecucionValidacion, true, true);
		contenedor.editarEjecucionValidacion(ejecucionValidacion);
	}

	@PUT
	@Path("/solicitarAjustesEjecucionValidacion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void solicitarAjustesEjecucionValidacion(EjecucionValidacion ejecucionValidacion) throws Exception {
		contenedor.validarInjectObjeto(ejecucionValidacion, true, true);
		contenedor.solicitarAjustesEjecucionValidacion(ejecucionValidacion);
	}

	@PUT
	@Path("/exportarResultado")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void exportarResultado(EjecucionValidacion ejecucion) throws Exception {
		contenedor.validarInjectObjeto(ejecucion, true, true);
		contenedor.exportarResultado(ejecucion);
	}

	// -------------------------------------------------------------------
	// ---------------------------AJUSTE GASTO----------------------------
	// -------------------------------------------------------------------

	@GET
	@Path("/obtenerAjustesGasto")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<AjusteGasto> obtenerAjustesGasto() throws Exception {
		ArrayList<AjusteGasto> ajustesGasto = new ArrayList<AjusteGasto>();
		ajustesGasto = contenedor.obtenerAjustesGasto();
		return ajustesGasto;
	}

	@POST
	@Path("/crearAjusteGasto")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearAjusteGasto(AjusteGasto ajusteGasto) throws Exception {
		contenedor.validarInjectObjeto(ajusteGasto, true, true);
		contenedor.crearAjusteGasto(ajusteGasto);
	}

	@PUT
	@Path("/editarAjusteGasto")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarAjusteGasto(AjusteGasto ajusteGasto) throws Exception {
		contenedor.validarInjectObjeto(ajusteGasto, true, true);
		contenedor.editarAjusteGasto(ajusteGasto);
	}

	@DELETE
	@Path("/borrarAjusteGasto/{id}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarAjusteGasto(@PathParam("id") Integer id_ajusteGasto) throws Exception {
		contenedor.borrarAjusteGasto(id_ajusteGasto);
	}

	// -------------------------------------------------------------------
	// ------------------EJECUCIONES AJUSTE OUTPUT------------------------
	// -------------------------------------------------------------------

	// Ajuste por Registro
	@GET
	@Path("/obtenerEjecucionesAjustes")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionAjustes> obtenerEjecucionesAjustes() throws Exception {
		return contenedor.obtenerEjecucionesAjustes();
	}

	@GET
	@Path("/obtenerInfoValidacionAjustes/{id_ejecucion}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public InfoValidacionAjustes obtenerInfoValidacionAjustes(@PathParam("id_ejecucion") Integer id_ejecucion)
			throws Exception {
		return contenedor.obtenerInfoValidacionAjustes(id_ejecucion);
	}

	@GET
	@Path("/obtenerCamposOutputCredito")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<CampoOutputCredito> obtenerCamposOutputCredito() throws Exception {
		return contenedor.obtenerCamposOutputCredito();
	}

	@POST
	@Path("/obtenerRegistrosOutputCredito")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<RegistroOutputCredito> obtenerRegistrosOutputCredito(
			ConsultaRegistrosOutput consultaRegistrosOutput) throws Exception {
		return contenedor.obtenerRegistrosOutputCredito(consultaRegistrosOutput);
	}

	@GET
	@Path("/obtenerStages")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Stage> obtenerStages() throws Exception {
		return contenedor.obtenerStages();
	}

	@PUT
	@Path("/ajustarRegistrosOutputCredito")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void ajustarRegistrosOutputCredito(GrupoRegistrosOutput grupoRegistroOutput) throws Exception {
		contenedor.validarInjectObjeto(grupoRegistroOutput, true, true);
		contenedor.ajustarRegistrosOutputCredito(grupoRegistroOutput);
	}

	@POST
	@Path("/obtenerOutputModeloMacro")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public OutputModeloMacro obtenerOutputModeloMacro(OutputModeloMacro output) throws Exception {
		contenedor.validarInjectObjeto(output, true, true);
		return contenedor.obtenerOutputModeloMacro(output);
	}

	// -------------------------------------------------------------------
	// ---------------------EJECUCIONES PE AGREGADAS----------------------
	// -------------------------------------------------------------------
	@GET
	@Path("/obtenerEjecucionesAgregadaPE_fase1/{id_tipo_ambito}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesAgregadaPE_fase1(@PathParam("id_tipo_ambito") Integer id_tipo_ambito) throws Exception {
		ArrayList<EjecucionAgregadaPE> ejecucionesAgregadaPE = new ArrayList<EjecucionAgregadaPE>();
		ejecucionesAgregadaPE = contenedor.obtenerEjecucionesAgregadaPE_fase1(id_tipo_ambito);
		return ejecucionesAgregadaPE;
	}

	@GET
	@Path("/obtenerEjecucionesAgregadaPE_fase2/{id_tipo_ambito}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesAgregadaPE_fase2(@PathParam("id_tipo_ambito") Integer id_tipo_ambito) throws Exception {
		ArrayList<EjecucionAgregadaPE> ejecucionesAgregadaPE = new ArrayList<EjecucionAgregadaPE>();
		ejecucionesAgregadaPE = contenedor.obtenerEjecucionesAgregadaPE_fase2(id_tipo_ambito);
		return ejecucionesAgregadaPE;
	}

	@GET
	@Path("/obtenerMapeoCargasModMacEjecucion_fase1/{codmes}/{id_juego_escenarios}/{tipo_ajusteManual}/{id_ejecucion_modMacro}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucion_fase1(@PathParam("codmes") Integer codmes,
			@PathParam("id_juego_escenarios") Integer id_juego_escenarios,
			@PathParam("tipo_ajusteManual") Integer tipo_ajusteManual,
			@PathParam("id_ejecucion_modMacro") Integer id_ejecucion_modMacro) throws Exception {
		MapeoCargasModMacEjec mapeoCargasModMacEjecucion = new MapeoCargasModMacEjec();
		mapeoCargasModMacEjecucion = contenedor.obtenerMapeoCargasModMacEjecucion_fase1(codmes, id_juego_escenarios,
				tipo_ajusteManual, id_ejecucion_modMacro);
		return mapeoCargasModMacEjecucion;
	}

	@GET
	@Path("/obtenerMapeoCargasModMacEjecucion_fase2/{codmes}/{id_juego_escenarios}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucion_fase2(@PathParam("codmes") Integer codmes,
			@PathParam("id_juego_escenarios") Integer id_juego_escenarios) throws Exception {
		MapeoCargasModMacEjec mapeoCargasModMacEjecucion = new MapeoCargasModMacEjec();
		mapeoCargasModMacEjecucion = contenedor.obtenerMapeoCargasModMacEjecucion_fase2(codmes, id_juego_escenarios);
		return mapeoCargasModMacEjecucion;
	}

	@GET
	@Path("/obtenerMapeoCargasModMacSimulacion/{codmes}/{fase}/{id_juego_escenarios}/{tipo_ajusteManual}/{id_ejecucion_modMacro}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public MapeoCargasModMacEjec obtenerMapeoCargasModMacSimulacion(@PathParam("codmes") Integer codmes,
			@PathParam("fase") Integer fase,
			@PathParam("id_juego_escenarios") Integer id_juego_escenarios,
			@PathParam("tipo_ajusteManual") Integer tipo_ajusteManual,
			@PathParam("id_ejecucion_modMacro") Integer id_ejecucion_modMacro) throws Exception {
		MapeoCargasModMacEjec mapeoCargasModMacEjecucion = new MapeoCargasModMacEjec();
		mapeoCargasModMacEjecucion = contenedor.obtenerMapeoCargasModMacSimulacion(codmes, fase, id_juego_escenarios,
				tipo_ajusteManual, id_ejecucion_modMacro);
		return mapeoCargasModMacEjecucion;
	}

	@GET
	@Path("/obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase1/{id_juego_escenarios}/{tipo_ajusteManual}/{id_ejecucion_modMacro}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase1(
			@PathParam("id_juego_escenarios") Integer id_juego_escenarios,
			@PathParam("tipo_ajusteManual") Integer tipo_ajusteManual,
			@PathParam("id_ejecucion_modMacro") Integer id_ejecucion_modMacro) throws Exception {
		MapeoCargasModMacEjec mapeoCargasEjecucion = new MapeoCargasModMacEjec();
		mapeoCargasEjecucion = contenedor.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase1(
				id_juego_escenarios, tipo_ajusteManual, id_ejecucion_modMacro);
		return mapeoCargasEjecucion;
	}

	@GET
	@Path("/obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase2/{id_juego_escenarios}/{tipo_ajusteManual}/{id_ejecucion_modMacro}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase2(
			@PathParam("id_juego_escenarios") Integer id_juego_escenarios,
			@PathParam("tipo_ajusteManual") Integer tipo_ajusteManual,
			@PathParam("id_ejecucion_modMacro") Integer id_ejecucion_modMacro) throws Exception {
		MapeoCargasModMacEjec mapeoCargasEjecucion = new MapeoCargasModMacEjec();
		mapeoCargasEjecucion = contenedor.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase2(
				id_juego_escenarios, tipo_ajusteManual, id_ejecucion_modMacro);
		return mapeoCargasEjecucion;
	}
	
	@GET
	@Path("/obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenariosSimulacion/{id_juego_escenarios}/{tipo_ajusteManual}/{id_ejecucion_modMacro}/{fase}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenariosSimulacion(
			@PathParam("id_juego_escenarios") Integer id_juego_escenarios,
			@PathParam("tipo_ajusteManual") Integer tipo_ajusteManual,
			@PathParam("id_ejecucion_modMacro") Integer id_ejecucion_modMacro,
			@PathParam("fase") Integer fase) throws Exception {
		MapeoCargasModMacEjec mapeoCargasEjecucion = new MapeoCargasModMacEjec();
		mapeoCargasEjecucion = contenedor.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenariosSimulacion(
				id_juego_escenarios, tipo_ajusteManual, id_ejecucion_modMacro, fase);
		return mapeoCargasEjecucion;
	}

	@POST
	@Path("/crearEjecucionAgregadaPE_fase1")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearEjecucionAgregadaPE_fase1(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		contenedor.validarInjectObjeto(ejecucionAgregada, true, true);
		contenedor.crearEjecucionAgregadaPE_fase1(ejecucionAgregada);
	}

	@POST
	@Path("/crearEjecucionAgregadaPE_fase2")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearEjecucionAgregadaPE_fase2(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		contenedor.validarInjectObjeto(ejecucionAgregada, true, true);
		contenedor.crearEjecucionAgregadaPE_fase2(ejecucionAgregada);
	}

	@POST
	@Path("/replicarEjecucionesAgregadasPE_fase1")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ReplicaEjecucionesAgregadasPE replicarEjecucionesPE_fase1(ReplicaEjecucionesAgregadasPE replicaEjecucionesPE)
			throws Exception {
		contenedor.validarInjectObjeto(replicaEjecucionesPE, true, true);
		return contenedor.replicarEjecucionesAgregadasPE_fase1(replicaEjecucionesPE);
	}

	@POST
	@Path("/replicarEjecucionesAgregadasPE_fase2")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ReplicaEjecucionesAgregadasPE replicarEjecucionesAgregadasPE_fase2(
			ReplicaEjecucionesAgregadasPE replicaEjecucionesPE) throws Exception {
		contenedor.validarInjectObjeto(replicaEjecucionesPE, true, true);
		return contenedor.replicarEjecucionesAgregadasPE_fase2(replicaEjecucionesPE);
	}

	@PUT
	@Path("/editarEjecucionAgregadaPE_fase1")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarEjecucionAgregadaPE_fase1(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		contenedor.validarInjectObjeto(ejecucionAgregada, true, true);
		contenedor.editarEjecucionAgregadaPE_fase1(ejecucionAgregada);
	}

	@PUT
	@Path("/editarEjecucionAgregadaPE_fase2")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarEjecucionAgregadaPE_fase2(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		contenedor.validarInjectObjeto(ejecucionAgregada, true, true);
		contenedor.editarEjecucionAgregadaPE_fase2(ejecucionAgregada);
	}

	@PUT
	@Path("/editarSimulacionAgregadaPE")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarSimulacionAgregadaPE(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		contenedor.validarInjectObjeto(ejecucionAgregada, true, true);
		contenedor.editarSimulacionAgregadaPE(ejecucionAgregada);
	}

	@DELETE
	@Path("/borrarEjecucionAgregadaPE_fase1/{codmes}/{id_juego_escenarios}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarEjecucionAgregadaPE_fase1(@PathParam("codmes") Integer codmes,
			@PathParam("id_juego_escenarios") Integer id_juego_escenarios) throws Exception {
		contenedor.borrarEjecucionAgregadaPE_fase1(codmes, id_juego_escenarios);
	}

	@DELETE
	@Path("/borrarEjecucionAgregadaPE_fase2/{codmes}/{id_juego_escenarios}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarEjecucionAgregadaPE_fase2(@PathParam("codmes") Integer codmes,
			@PathParam("id_juego_escenarios") Integer id_juego_escenarios) throws Exception {
		contenedor.borrarEjecucionAgregadaPE_fase2(codmes, id_juego_escenarios);
	}
	
	@DELETE
	@Path("/borrarSimulacionBifAgregadaPE/{codmes}/{fase}/{id_juego_escenarios}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarSimulacionBifAgregadaPE(@PathParam("codmes") Integer codmes,
			@PathParam("fase") Integer fase,
			@PathParam("id_juego_escenarios") Integer id_juego_escenarios) throws Exception {
		contenedor.borrarSimulacionBifAgregadaPE(codmes, fase, id_juego_escenarios);
	}

	// Ajustes delta
	@GET
	@Path("/obtenerMapeosDeltaStage/{id_ejecucion_f2base}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<MapeoDeltaStage> obtenerMapeosDeltaStage(
			@PathParam("id_ejecucion_f2base") Integer id_ejecucion_f2base) throws Exception {
		ArrayList<MapeoDeltaStage> mapeoDeltas = new ArrayList<MapeoDeltaStage>();
		mapeoDeltas = contenedor.obtenerMapeosDeltaStage(id_ejecucion_f2base);
		return mapeoDeltas;
	}

	@PUT
	@Path("/ajustarDeltas")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void ajustarDeltas(GrupoMapeosDelta grupoMapeosDelta) throws Exception {
		contenedor.validarInjectObjeto(grupoMapeosDelta, true, true);
		contenedor.ajustarDeltas(grupoMapeosDelta);
	}

	// -------------------------------------------------------------------
	// ---------------------SIMULACIONES PE AGREGADAS---------------------
	// -------------------------------------------------------------------

	@GET
	@Path("/obtenerSimulacionesAgregadaPE/{incluyeF2}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionAgregadaPE> obtenerSimulacionesAgregadaPE(
			@PathParam("incluyeF2") Boolean incluyeF2) throws Exception {
		ArrayList<EjecucionAgregadaPE> ejecucionesAgregadaPE = new ArrayList<EjecucionAgregadaPE>();
		ejecucionesAgregadaPE = contenedor.obtenerSimulacionesAgregadaPE(incluyeF2);
		return ejecucionesAgregadaPE;
	}

	@GET
	@Path("/obtenerEjecucionesPETotalesFase1")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotalesFase1() throws Exception {
		ArrayList<EjecucionAgregadaPE> ejecucionesAgregadaPE = new ArrayList<EjecucionAgregadaPE>();
		ejecucionesAgregadaPE = contenedor.obtenerEjecucionesPETotalesFase1();
		return ejecucionesAgregadaPE;
	}
	
	@GET
	@Path("/obtenerEjecucionesPETotalesFase2")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotalesFase2() throws Exception {
		ArrayList<EjecucionAgregadaPE> ejecucionesAgregadaPE = new ArrayList<EjecucionAgregadaPE>();
		ejecucionesAgregadaPE = contenedor.obtenerEjecucionesPETotalesFase2();
		return ejecucionesAgregadaPE;
	}
	
	@GET
	@Path("/obtenerEjecucionesPETotales")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotales() throws Exception {
		ArrayList<EjecucionAgregadaPE> ejecucionesAgregadaPE = new ArrayList<EjecucionAgregadaPE>();
		ejecucionesAgregadaPE = contenedor.obtenerEjecucionesPETotales();
		return ejecucionesAgregadaPE;
	}

	@POST
	@Path("/crearSimulacionAgregadaPE")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearSimulacionAgregadaPE(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		contenedor.validarInjectObjeto(ejecucionAgregada, true, true);
		contenedor.crearSimulacionAgregadaPE(ejecucionAgregada);
	}

	@POST
	@Path("/replicarSimulacionesAgregadasPE")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ReplicaEjecucionesAgregadasPE replicarSimulacionesAgregadasPE(
			ReplicaEjecucionesAgregadasPE replicaEjecucionesPE) throws Exception {
		contenedor.validarInjectObjeto(replicaEjecucionesPE, true, true);
		return contenedor.replicarSimulacionesAgregadasPE(replicaEjecucionesPE);
	}

	@PUT
	@Path("/solicitarValidacion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void solicitarValidacion(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		contenedor.validarInjectObjeto(ejecucionAgregada, true, true);
		contenedor.solicitarValidacion(ejecucionAgregada);
	}

	// -------------------------------------------------------------------
	// ----------------------EJECUCIONES PRIMER PASO----------------------
	// -------------------------------------------------------------------

	@GET
	@Path("/obtenerEjecucionesPrimerPaso")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionPrimerPaso> obtenerEjecucionesPrimerPaso() throws Exception {
		return contenedor.obtenerEjecucionesPrimerPaso();
	}

	@GET
	@Path("/obtenerPrimerosPasosPorIdMetodologia/{id_metodologia}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<PrimerPaso> obtenerPrimerosPasosPorIdMetodologia(
			@PathParam("id_metodologia") Integer id_metodologia) throws Exception {
		return contenedor.obtenerPrimerosPasosPorIdMetodologia(id_metodologia);
	}

	@GET
	@Path("/obtenerTablasMapeoEjecucionPrimerPaso/{id_ejecucion}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionPrimerPaso(
			@PathParam("id_ejecucion") Integer id_ejecucion) throws Exception {
		return contenedor.obtenerTablasMapeoEjecucionPrimerPaso(id_ejecucion);
	}

	@GET
	@Path("/obtenerTablasMapeoEjecucionPorIdPrimPasoMet/{id_primerPaso}/{id_metodologia}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionPorIdPrimPasoMet(
			@PathParam("id_primerPaso") Integer id_primerPaso, @PathParam("id_metodologia") Integer id_metodologia)
			throws Exception {
		return contenedor.obtenerTablasMapeoEjecucionPorIdPrimPasoMet(id_primerPaso, id_metodologia);
	}

	@POST
	@Path("/crearEjecucionPrimerPaso")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearEjecucionPrimerPaso(EjecucionPrimerPaso ejecucionPrimerPaso) throws Exception {
		contenedor.validarInjectObjeto(ejecucionPrimerPaso, true, true);
		contenedor.crearEjecucionPrimerPaso(ejecucionPrimerPaso);
	}

	@POST
	@Path("/replicarEjecucionesPrimerPaso")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ReplicaEjecucionesPrimerPaso replicarEjecucionesPrimerPaso(
			ReplicaEjecucionesPrimerPaso replicaEjecucionesPrimerPaso) throws Exception {
		contenedor.validarInjectObjeto(replicaEjecucionesPrimerPaso, true, true);
		return contenedor.replicarEjecucionesPrimerPaso(replicaEjecucionesPrimerPaso);
	}

	@PUT
	@Path("/editarEjecucionPrimerPaso")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarEjecucionPrimerPaso(EjecucionPrimerPaso ejecucionPrimerPaso) throws Exception {
		contenedor.validarInjectObjeto(ejecucionPrimerPaso, true, true);
		contenedor.editarEjecucionPrimerPaso(ejecucionPrimerPaso);
	}

	@DELETE
	@Path("/borrarEjecucionPrimerPaso/{id_ejecucion}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarEjecucionPrimerPaso(@PathParam("id_ejecucion") Integer id_ejecucion) throws Exception {
		contenedor.borrarEjecucionPrimerPaso(id_ejecucion);
	}

	@PUT
	@Path("/ejecutarPrimerPaso")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void ejecutarPrimerPaso(EjecucionPrimerPaso ejecucionPrimerPaso) throws Exception {
		contenedor.validarInjectObjeto(ejecucionPrimerPaso, true, true);
		contenedor.ejecutarPrimerPaso(ejecucionPrimerPaso);
	}
	
	
	@GET
	@Path("/obtenerLogsCodInicializacion/{codmes}/{id_juego_escenarios}/{fase}/{id_tipo_ambito}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<LogCodInicializacion> obtenerLogsCodInicializacion(@PathParam("codmes") Integer codmes, 
			@PathParam("id_juego_escenarios") Integer id_juego_escenarios, @PathParam("fase") Integer fase, 
			@PathParam("id_tipo_ambito") Integer id_tipo_ambito) throws Exception {
		ArrayList<LogCodInicializacion> logs = new ArrayList<LogCodInicializacion>();
		logs = contenedor.obtenerLogsCodInicializacion(codmes, id_juego_escenarios, fase, id_tipo_ambito);
		return logs;
	}
	@GET
	@Path("/obtenerLogsCodInicializacionVariacion/{id_Ejecucion}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<LogCodInicializacion> obtenerLogsCodInicializacionVariacion(@PathParam("id_Ejecucion") Integer id_Ejecucion) throws Exception {
		ArrayList<LogCodInicializacion> logs = new ArrayList<LogCodInicializacion>();
		logs = contenedor.obtenerLogsCodInicializacionVariacion(id_Ejecucion);
		
		return logs;
	}
	

	// -------------------------------------------------------------------
	// ---------------------- EJECUCIONES OTROS --------------------------
	// -------------------------------------------------------------------

	@GET
	@Path("/obtenerEstructurasOtrosPorIdCartera/{id_cartera}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EstructuraMetodologica> obtenerEstructurasOtrosPorIdCartera(@PathParam("id_cartera") Integer id_cartera)
			throws Exception {
		return contenedor.obtenerEstructurasOtrosPorIdCartera(id_cartera);
	}
	
	@GET
	@Path("/obtenerEjecucionesOtros")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EjecucionOtro> obtenerEjecucionesOtros() throws Exception {
		return contenedor.obtenerEjecucionesOtros();
	}

	@GET
	@Path("/obtenerTablasMapeoEjecucionOtros/{id_ejecucion}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionOtros(
			@PathParam("id_ejecucion") Integer id_ejecucion) throws Exception {
		return contenedor.obtenerTablasMapeoEjecucionOtros(id_ejecucion);
	}

	@GET
	@Path("/obtenerTablasMapeoEjecucionOtrosPorCartVer/{id_cartera_version}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionOtrosPorCartVer(
			@PathParam("id_cartera_version") Integer id_cartera_version)
			throws Exception {
		return contenedor.obtenerTablasMapeoEjecucionOtrosPorCartVer(id_cartera_version);
	}

	@POST
	@Path("/crearEjecucionOtro")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearEjecucionOtro(EjecucionOtro ejecucion) throws Exception {
		contenedor.validarInjectObjeto(ejecucion, true, true);
		contenedor.crearEjecucionOtro(ejecucion);
	}

	@POST
	@Path("/replicarEjecucionesOtros")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ReplicaEjecucionesOtros replicarEjecucionesOtros(
			ReplicaEjecucionesOtros replicaEjecuciones) throws Exception {
		contenedor.validarInjectObjeto(replicaEjecuciones, true, true);
		return contenedor.replicarEjecucionesOtros(replicaEjecuciones);
	}

	@PUT
	@Path("/editarEjecucionOtro")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarEjecucionOtro(EjecucionOtro ejecucion) throws Exception {
		contenedor.validarInjectObjeto(ejecucion, true, true);
		contenedor.editarEjecucionOtro(ejecucion);
	}

	@DELETE
	@Path("/borrarEjecucionOtro/{id_ejecucion}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarEjecucionOtros(@PathParam("id_ejecucion") Integer id_ejecucion) throws Exception {
		contenedor.borrarEjecucionOtros(id_ejecucion);
	}

	@PUT
	@Path("/ejecutarCalculoOtros")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void ejecutarCalculoOtros(EjecucionOtro ejecucion) throws Exception {
		contenedor.validarInjectObjeto(ejecucion, true, true);
		contenedor.ejecutarCalculoOtros(ejecucion);
	}
}
