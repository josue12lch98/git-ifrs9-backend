package sim.persistencia;

public class CampoValor {
	private String nombre;
	private Double valor_numero;
	private String valor_texto;
	private Integer tipo_dato; //1: numerico, 2: texto
	
	public CampoValor() {		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getTipo_dato() {
		return tipo_dato;
	}

	public void setTipo_dato(Integer tipo_dato) {
		this.tipo_dato = tipo_dato;
	}	

	public String getValor_texto() {
		return valor_texto;
	}

	public void setValor_texto(String valor_texto) {
		this.valor_texto = valor_texto;
	}

	public Double getValor_numero() {
		return valor_numero;
	}

	public void setValor_numero(Double valor_numero) {
		this.valor_numero = valor_numero;
	}

	
}
