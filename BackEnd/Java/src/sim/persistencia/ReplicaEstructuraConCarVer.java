package sim.persistencia;

import java.util.ArrayList;

public class ReplicaEstructuraConCarVer {
	private EstructuraMetodologica estructura_replicar;	
	private ArrayList<CarteraVersion_staging> carterasVer_replicar;
	
	public ReplicaEstructuraConCarVer() {		
	}

	public EstructuraMetodologica getEstructura_replicar() {
		return estructura_replicar;
	}

	public void setEstructura_replicar(EstructuraMetodologica estructura_replicar) {
		this.estructura_replicar = estructura_replicar;
	}

	public ArrayList<CarteraVersion_staging> getCarterasVer_replicar() {
		return carterasVer_replicar;
	}

	public void setCarterasVer_replicar(ArrayList<CarteraVersion_staging> carterasVer_replicar) {
		this.carterasVer_replicar = carterasVer_replicar;
	}
}
