package sim.persistencia;

import java.util.ArrayList;

public class MapeoCargasModMacEscVer {
	
	private Integer id_ejecucion;
	private Escenario_version escenario_version;		
	private ArrayList<TablaMapeoEjecucion> tablas_mapeo_escenarioVersion;
		
	private Boolean flag_ejecutar; //Unicamente para simulaciones
	
	//Para modelo macro
	private ArrayList<AjustePdProyec> ajustesPdProy;
	
	public MapeoCargasModMacEscVer() {}

	public Escenario_version getEscenario_version() {
		return escenario_version;
	}

	public void setEscenario_version(Escenario_version escenario_version) {
		this.escenario_version = escenario_version;
	}

	public ArrayList<TablaMapeoEjecucion> getTablas_mapeo_escenarioVersion() {
		return tablas_mapeo_escenarioVersion;
	}

	public void setTablas_mapeo_escenarioVersion(ArrayList<TablaMapeoEjecucion> tablas_mapeo_escenarioVersion) {
		this.tablas_mapeo_escenarioVersion = tablas_mapeo_escenarioVersion;
	}

	public Integer getId_ejecucion() {
		return id_ejecucion;
	}

	public void setId_ejecucion(Integer id_ejecucion) {
		this.id_ejecucion = id_ejecucion;
	}

	public Boolean getFlag_ejecutar() {
		return flag_ejecutar;
	}

	public void setFlag_ejecutar(Boolean flag_ejecutar) {
		this.flag_ejecutar = flag_ejecutar;
	}

	public ArrayList<AjustePdProyec> getAjustesPdProy() {
		return ajustesPdProy;
	}

	public void setAjustesPdProy(ArrayList<AjustePdProyec> ajustesPdProy) {
		this.ajustesPdProy = ajustesPdProy;
	}	
}
