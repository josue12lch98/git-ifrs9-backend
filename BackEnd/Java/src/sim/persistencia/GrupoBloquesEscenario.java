package sim.persistencia;

import java.util.ArrayList;

public class GrupoBloquesEscenario {

	private Integer id_ejecucion;
	private ArrayList<BloqueParametrizado> bloquesParam_escenarioVersion;
	private Escenario_version escenario_version; 
	
	private Boolean flag_ejecutar; //Unicamente para simulaciones
	
	//Para modelo macro
	private ArrayList<AjustePdProyec> ajustesPdProy;
		
	public GrupoBloquesEscenario() {		
	}

	public ArrayList<BloqueParametrizado> getBloquesParam_escenarioVersion() {
		return bloquesParam_escenarioVersion;
	}

	public void setBloquesParam_escenarioVersion(ArrayList<BloqueParametrizado> bloquesParam_escenarioVersion) {
		this.bloquesParam_escenarioVersion = bloquesParam_escenarioVersion;
	}

	public Escenario_version getEscenario_version() {
		return escenario_version;
	}

	public void setEscenario_version(Escenario_version escenario_version) {
		this.escenario_version = escenario_version;
	}

	public Integer getId_ejecucion() {
		return id_ejecucion;
	}

	public void setId_ejecucion(Integer id_ejecucion) {
		this.id_ejecucion = id_ejecucion;
	}

	public Boolean getFlag_ejecutar() {
		return flag_ejecutar;
	}

	public void setFlag_ejecutar(Boolean flag_ejecutar) {
		this.flag_ejecutar = flag_ejecutar;
	}

	public ArrayList<AjustePdProyec> getAjustesPdProy() {
		return ajustesPdProy;
	}

	public void setAjustesPdProy(ArrayList<AjustePdProyec> ajustesPdProy) {
		this.ajustesPdProy = ajustesPdProy;
	}
}
