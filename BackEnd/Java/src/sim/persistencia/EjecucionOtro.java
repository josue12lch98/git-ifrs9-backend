package sim.persistencia;

import java.util.ArrayList;

public class EjecucionOtro {
	private Integer id_ejecucion;
	private Integer codmes; 
	private Cartera_version cartera_version;
	private Integer estado_ejecucion;	
	private Integer porcentaje_avance;
	private Long datetime_ejecucion;	
	
	private ArrayList<TablaMapeoEjecucion> tablas_mapeo_ejecucion;	
	
	public EjecucionOtro() {		
	}

	public Integer getCodmes() {
		return codmes;
	}

	public void setCodmes(Integer codmes) {
		this.codmes = codmes;
	}

	public Cartera_version getCartera_version() {
		return cartera_version;
	}

	public void setCartera_version(Cartera_version cartera_version) {
		this.cartera_version = cartera_version;
	}

	public Integer getEstado_ejecucion() {
		return estado_ejecucion;
	}

	public void setEstado_ejecucion(Integer estado_ejecucion) {
		this.estado_ejecucion = estado_ejecucion;
	}

	public Long getDatetime_ejecucion() {
		return datetime_ejecucion;
	}

	public void setDatetime_ejecucion(Long datetime_ejecucion) {
		this.datetime_ejecucion = datetime_ejecucion;
	}

	public Integer getPorcentaje_avance() {
		return porcentaje_avance;
	}

	public void setPorcentaje_avance(Integer porcentaje_avance) {
		this.porcentaje_avance = porcentaje_avance;
	}

	public ArrayList<TablaMapeoEjecucion> getTablas_mapeo_ejecucion() {
		return tablas_mapeo_ejecucion;
	}

	public void setTablas_mapeo_ejecucion(ArrayList<TablaMapeoEjecucion> tablas_mapeo_ejecucion) {
		this.tablas_mapeo_ejecucion = tablas_mapeo_ejecucion;
	}

	public Integer getId_ejecucion() {
		return id_ejecucion;
	}

	public void setId_ejecucion(Integer id_ejecucion) {
		this.id_ejecucion = id_ejecucion;
	}
}
