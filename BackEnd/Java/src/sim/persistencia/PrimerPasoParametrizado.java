package sim.persistencia;

import java.util.ArrayList;

public class PrimerPasoParametrizado extends PrimerPaso {
	private EstructuraMetodologica estructura_metodologica;
	private ArrayList<BloqueParametrizado> bloques_parametrizado;
	
	public PrimerPasoParametrizado() {		
	}
	
	public EstructuraMetodologica getEstructura_metodologica() {
		return estructura_metodologica;
	}
	public void setEstructura_metodologica(EstructuraMetodologica estructura_metodologica) {
		this.estructura_metodologica = estructura_metodologica;
	}
	public ArrayList<BloqueParametrizado> getBloques_parametrizado() {
		return bloques_parametrizado;
	}
	public void setBloques_parametrizado(ArrayList<BloqueParametrizado> bloques_parametrizado) {
		this.bloques_parametrizado = bloques_parametrizado;
	}
}
