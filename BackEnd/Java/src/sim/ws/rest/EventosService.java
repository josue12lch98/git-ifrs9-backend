package sim.ws.rest;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import sim.ws.rest.JwtRequiereToken;
import sim.ws.rest.JwtRefrescaToken;

import sim.Contenedor;
import sim.persistencia.Evento;
import sim.persistencia.EventoDetalle;

@Path("/eventos")
public class EventosService {
	
	private static Contenedor contenedor = Contenedor.obtenerInstancia();

	// -------------------------------------------------------------------
		// ----------------------------EVENTOS------------------------------
		// -------------------------------------------------------------------
		@POST
		@Path("/crearEvento")
		@JwtRequiereToken
		@JwtRefrescaToken
		@Consumes(MediaType.APPLICATION_JSON)
		public void crearEvento(Evento evento) throws Exception {
			//contenedor.validarInjectObjeto(evento, true, true);
			contenedor.crearEvento(evento);
		}
		
		@GET
		@Path("/obtenerEventos/{id_tipo_ambito}")
		@JwtRequiereToken
		@JwtRefrescaToken
		@Produces(MediaType.APPLICATION_JSON)
		public ArrayList<Evento> obtenerEventos(@PathParam("id_tipo_ambito") Integer id_tipo_ambito) throws Exception {
			ArrayList<Evento> eventos = new ArrayList<Evento>();
			eventos = contenedor.obtenerEventos(id_tipo_ambito);
			return eventos;
		}

		@GET
		@Path("/obtenerEventoDetalle/{id_evento}")
		@JwtRequiereToken
		@JwtRefrescaToken
		@Produces(MediaType.APPLICATION_JSON)
		public EventoDetalle obtenerEventoDetalle(@PathParam("id_evento") Integer id_evento) throws Exception {
			return  contenedor.obtenerEventoDetalle(id_evento);
		}
}
