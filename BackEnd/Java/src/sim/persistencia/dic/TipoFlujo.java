package sim.persistencia.dic;

public class TipoFlujo {
	private Integer id_tipo_flujo;
	private String nombre_tipo_flujo;
	private TipoAmbito tipo_ambito;
	
	public TipoFlujo() {
	}

	public Integer getId_tipo_flujo() {
		return id_tipo_flujo;
	}

	public void setId_tipo_flujo(Integer id_tipo_flujo) {
		this.id_tipo_flujo = id_tipo_flujo;
	}

	public String getNombre_tipo_flujo() {
		return nombre_tipo_flujo;
	}

	public void setNombre_tipo_flujo(String nombre_tipo_flujo) {
		this.nombre_tipo_flujo = nombre_tipo_flujo;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}

	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}
}
