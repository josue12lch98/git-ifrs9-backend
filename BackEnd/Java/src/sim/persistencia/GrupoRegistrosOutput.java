package sim.persistencia;

import java.util.ArrayList;

public class GrupoRegistrosOutput {
	private Integer id_ejecucion;
	private Integer codmes;
	private Integer fase;
	private Escenario_version escenario_version;	
	private ArrayList<RegistroOutputCredito> registros_output;
	
	public GrupoRegistrosOutput() {		
	}
	
	public Integer getCodmes() {
		return codmes;
	}
	public void setCodmes(Integer codmes) {
		this.codmes = codmes;
	}
	public Integer getFase() {
		return fase;
	}
	public void setFase(Integer fase) {
		this.fase = fase;
	}
	public Escenario_version getEscenario_version() {
		return escenario_version;
	}
	public void setEscenario_version(Escenario_version escenario_version) {
		this.escenario_version = escenario_version;
	}	
	public ArrayList<RegistroOutputCredito> getRegistros_output() {
		return registros_output;
	}
	public void setRegistros_output(ArrayList<RegistroOutputCredito> registros_output) {
		this.registros_output = registros_output;
	}

	public Integer getId_ejecucion() {
		return id_ejecucion;
	}

	public void setId_ejecucion(Integer id_ejecucion) {
		this.id_ejecucion = id_ejecucion;
	}
}
