package sim.persistencia;

public class Expresion_Staging {
	private Integer id_expresion;
	private Integer id_cartera_version;
	private String nombre_expresion;
	private String formula_input;
	private String formula_sql;
	private Boolean flag_editar;
	
	public Expresion_Staging() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getId_expresion() {
		return id_expresion;
	}
	public void setId_expresion(Integer id_expresion) {
		this.id_expresion = id_expresion;
	}
	public Integer getId_cartera_version() {
		return id_cartera_version;
	}
	public void setId_cartera_version(Integer id_cartera_version) {
		this.id_cartera_version = id_cartera_version;
	}
	public String getNombre_expresion() {
		return nombre_expresion;
	}
	public void setNombre_expresion(String nombre_expresion) {
		this.nombre_expresion = nombre_expresion;
	}
	public String getFormula_input() {
		return formula_input;
	}
	public void setFormula_input(String formula_input) {
		this.formula_input = formula_input;
	}
	public String getFormula_sql() {
		return formula_sql;
	}
	public void setFormula_sql(String formula_sql) {
		this.formula_sql = formula_sql;
	}
	public Boolean getFlag_editar() {
		return flag_editar;
	}
	public void setFlag_editar(Boolean flag_editar) {
		this.flag_editar = flag_editar;
	}
	
	
}
