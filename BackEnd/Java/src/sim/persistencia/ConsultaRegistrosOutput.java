package sim.persistencia;

import java.util.ArrayList;

public class ConsultaRegistrosOutput {
	private Integer id_ejecucion;
	private ArrayList<CampoOutputCredito> campos_output_credito;
	
	public ConsultaRegistrosOutput() {		
	}	

	public ArrayList<CampoOutputCredito> getCampos_output_credito() {
		return campos_output_credito;
	}

	public void setCampos_output_credito(ArrayList<CampoOutputCredito> campos_output_credito) {
		this.campos_output_credito = campos_output_credito;
	}

	public Integer getId_ejecucion() {
		return id_ejecucion;
	}

	public void setId_ejecucion(Integer id_ejecucion) {
		this.id_ejecucion = id_ejecucion;
	}	

	
}
