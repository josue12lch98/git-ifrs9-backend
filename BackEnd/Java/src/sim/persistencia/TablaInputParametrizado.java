package sim.persistencia;

public class TablaInputParametrizado {
	private Integer id_tabla;
	private FicheroInput fichero_input;
	private Integer version_metadata;		
	private String nombre_metadata;		

	public TablaInputParametrizado() {		
	}
	
	public Integer getId_tabla() {
		return id_tabla;
	}
	public void setId_tabla(Integer id_tabla) {
		this.id_tabla = id_tabla;
	}
			
	public String getNombre_metadata() {
		return nombre_metadata;
	}
	public void setNombre_metadata(String nombre_metadata) {
		this.nombre_metadata = nombre_metadata;
	}

	public Integer getVersion_metadata() {
		return version_metadata;
	}

	public void setVersion_metadata(Integer version_metadata) {
		this.version_metadata = version_metadata;
	}

	public FicheroInput getFichero_input() {
		return fichero_input;
	}

	public void setFichero_input(FicheroInput fichero_input) {
		this.fichero_input = fichero_input;
	}
	
}
