package sim.ws.rest;

import java.io.IOException;
//import java.io.ObjectInputStream;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import io.jsonwebtoken.Claims;
//JJWT
import io.jsonwebtoken.Jwts;
import sim.Claves;
import sim.Contenedor;
import sim.PropertiesCompartido;

import com.vladium.utils.IObjectProfileNode;
import com.vladium.utils.ObjectProfiler;

@Provider
@JwtRefrescaToken
//@Priority(Priorities.AUTHENTICATION)
public class JwtRefrescaTokenFiltro implements ContainerResponseFilter {

	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(JwtRefrescaTokenFiltro.class);
	private static Contenedor contenedor = Contenedor.obtenerInstancia();
	
	@Override
	public void filter(ContainerRequestContext req, ContainerResponseContext res) throws IOException {
		
		//Refresco de token
		Long expiracion = null;
		
		//Token anterior
		String tokenAnt = null;
	    String headerAuth = null;
	    Claims claimsTokenAnt = null;
	    IObjectProfileNode profile = null;
	    Integer tamanioResp = 0;
	    
	    //Decod. objeto request
	    //ObjectInputStream ois = null;
	    //Object objetoReq = null;
	    //Boolean existe_reqInpStr = true;
	    //Integer tamanioInpStr = 0;
	    
		/*try {
			ois = new ObjectInputStream(req.getEntityStream());
			objetoReq = ois.readObject();
		} catch (Exception ex) {
			//LOGGER.error("Error al convertir inputstream: " + ex.getMessage());
			existe_reqInpStr = false;
		}	
		if (existe_reqInpStr) {
			profile = ObjectProfiler.profile(objetoReq);
			tamanioInpStr = profile.size();
		}*/
    	 
	    
	    //Notificamos todos los request entrantes (logs)	    
	    LOGGER.info("--------------TRANSACCION-----------------");
	    LOGGER.info("***Metodo***:                     " + req.getMethod());
	    LOGGER.info("***Ruta***:                       " + req.getUriInfo().getPath());	     
	    LOGGER.info("***Tamaño de payload request***:  " + (req.getHeaderString(HttpHeaders.CONTENT_LENGTH) 
	   	     		 == null ? "0":req.getHeaderString(HttpHeaders.CONTENT_LENGTH)) + " bytes"); //tamanioInpStr	   
	   	
	    if (res.getEntity() != null) {
	    	profile = ObjectProfiler.profile(res.getEntity());	
	    	tamanioResp = profile.size(); 
	    }	        
		LOGGER.info("***Tamaño de payload response***: " + tamanioResp + " bytes");
	    LOGGER.info("***Status***:                     " + Response.Status.fromStatusCode(res.getStatus()).toString());
	    LOGGER.info("------------------------------------------");
	    
		// Obtenemos el header de autenticacion
	    headerAuth = req.getHeaderString(HttpHeaders.AUTHORIZATION);
		           
        // Extraemos token del header (ya esta validado en el otro filtro ...)
        tokenAnt = headerAuth.substring("Bearer".length()).trim();   
        
        // Obtenemos informacion del token
        claimsTokenAnt = Jwts.parser().setSigningKey(Claves.claveToken).parseClaimsJws(tokenAnt).getBody();
				
		//Tiempo de refresco
		expiracion = System.currentTimeMillis() + PropertiesCompartido.lifetimeSesion;	
		
		//Actualizamos token de refresco en DB
		try {			
			//Refrescamos tiempo limite (mantenemos token de acceso)
			contenedor.actualizarExpiracionSesion(claimsTokenAnt.getSubject(), expiracion);		
		} catch (Exception e) {
			//Invalidamos token de refresco
			tokenAnt = ""; 
			LOGGER.error("Error al refrescar token: " + e.getMessage());
			throw new NotAuthorizedException("Error al refrescar token!" + e.getMessage());			
		} finally {
			res.getHeaders().add("nuevoToken", tokenAnt); //Mantenemos mismo token (refresco por tiempos en DB)
			res.getHeaders().add("nuevaExpiracion", expiracion);
		}		
	}

}