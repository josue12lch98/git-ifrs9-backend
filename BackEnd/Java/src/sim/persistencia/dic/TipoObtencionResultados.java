package sim.persistencia.dic;


public class TipoObtencionResultados {
	private Integer id_tipo_obtencion_resultados;
	private String nombre_tipo_obtencion_resultados;
	
	public TipoObtencionResultados() {
	}
	
	public Integer getId_tipo_obtencion_resultados() {
		return id_tipo_obtencion_resultados;
	}
	public void setId_tipo_obtencion_resultados(Integer id_tipo_obtencion_resultados) {
		this.id_tipo_obtencion_resultados = id_tipo_obtencion_resultados;
	}
	public String getNombre_tipo_obtencion_resultados() {
		return nombre_tipo_obtencion_resultados;
	}
	public void setNombre_tipo_obtencion_resultados(String nombre_tipo_obtencion_resultados) {
		this.nombre_tipo_obtencion_resultados = nombre_tipo_obtencion_resultados;
	}
	
}
