package sim;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import org.jasypt.intf.cli.JasyptPBEStringDecryptionCLI;

public class PropertiesWrapper extends Properties {
	
	private static final long serialVersionUID = 1L;
	private static String semilla;
	private static String rutaSemilla;
	private static Boolean semillaEncriptada;
	private static byte[] key;
	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(PropertiesWrapper.class);

	public PropertiesWrapper (String ruta, byte[] magic, Boolean enc) {
		super();
		rutaSemilla = ruta;
		key = magic;//.getBytes();
		semillaEncriptada = enc;
 		inicializarSemilla();
	}
	
	private void inicializarSemilla() {
		if (semillaEncriptada) {
			desencriptarSemilla();
		} else {
			try {
				semilla = new String ( Files.readAllBytes( Paths.get(rutaSemilla) ) );
			} catch (Exception ex) {
				LOGGER.error("Error al leer la semilla: " + ex.getMessage());
			}
		}
	}
	
	private void desencriptarSemilla() {
		try {
			File encryptedFile = new File(rutaSemilla);
			semilla = CryptoUtilsDecrypt.decrypt(encryptedFile, key);
		} catch (Exception ex) {
			LOGGER.error("Error al desencriptar semilla: " + ex.getMessage());
		}
	}
	
	@Override
    public String getProperty(String propKey){
		String retorno = null;
        Object oValue = super.get(propKey);
        String propValue = (oValue instanceof String) ? (String)oValue : null;
        //LOGGER.info("Valor de la propiedad: " + propValue);
        if ((propValue == null)) {
        	retorno = null;
        } else if (propValue.contains("ENC(") && propValue.contains(")")) {
        	try {
        		retorno = propValue.substring(4).substring(0, propValue.length()-5);
        		//LOGGER.info("Texto a desencriptar: " + retorno);
        		String[] args = {"decrypt","input=" + retorno,"password=" + semilla,"algorithm=" + "PBEWITHHMACSHA512ANDAES_256"};
        		retorno = JasyptPBEStringDecryptionCLI.main(args);
        		//LOGGER.info("Texto desencriptado: " + retorno);
        	} catch (Exception ex) {
        		LOGGER.error("Error al desencriptar la propiedad: " + ex.getMessage());	
        	}
        } else {
        	retorno = propValue;
        }
        return retorno;
    }

}
