package sim.persistencia;

import java.util.ArrayList;

import sim.persistencia.dic.FormatoInput;
import sim.persistencia.dic.TipoBloque;

public class Bloque {
	private Integer id_bloque;
	private String nombre_bloque;
	private String variable_motor;
	private Boolean flag_tabla_input;
	private FormatoInput formato_input;
	private TipoBloque tipo_bloque;
	private ArrayList<CampoBloque> campos_bloque;

	private Boolean conDependencias;
	private Boolean conDependenciasOficial;
	
	public Bloque() {
	}

	public Integer getId_bloque() {
		return id_bloque;
	}

	public void setId_bloque(Integer id_bloque) {
		this.id_bloque = id_bloque;
	}

	public String getNombre_bloque() {
		return nombre_bloque;
	}

	public void setNombre_bloque(String nombre_bloque) {
		this.nombre_bloque = nombre_bloque;
	}

	public String getVariable_motor() {
		return variable_motor;
	}

	public void setVariable_motor(String variable_motor) {
		this.variable_motor = variable_motor;
	}

	public Boolean getFlag_tabla_input() {
		return flag_tabla_input;
	}

	public void setFlag_tabla_input(Boolean flag_tabla_input) {
		this.flag_tabla_input = flag_tabla_input;
	}

	public TipoBloque getTipo_bloque() {
		return tipo_bloque;
	}

	public void setTipo_bloque(TipoBloque tipo_bloque) {
		this.tipo_bloque = tipo_bloque;
	}

	public ArrayList<CampoBloque> getCampos_bloque() {
		return campos_bloque;
	}

	public void setCampos_bloque(ArrayList<CampoBloque> campo_bloque) {
		this.campos_bloque = campo_bloque;
	}

	public Boolean getConDependencias() {
		return conDependencias;
	}

	public void setConDependencias(Boolean conDependencias) {
		this.conDependencias = conDependencias;
	}

	public Boolean getConDependenciasOficial() {
		return conDependenciasOficial;
	}

	public void setConDependenciasOficial(Boolean conDependenciasOficial) {
		this.conDependenciasOficial = conDependenciasOficial;
	}

	public FormatoInput getFormato_input() {
		return formato_input;
	}

	public void setFormato_input(FormatoInput formato_input) {
		this.formato_input = formato_input;
	}	
}
