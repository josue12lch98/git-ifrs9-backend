package sim.persistencia;

import java.util.ArrayList;

import sim.persistencia.dic.TipoAmbito;

public class Perfil {

	private int id_perfil;	
	private String nombre_perfil;
	private String nombre_perfil_ad;
	private Boolean flag_obligatorio;
	private TipoAmbito tipo_ambito;
	public ArrayList<Permiso> permisos;
	
	public Perfil() {
	}
	
	public int getId_perfil() {
		return id_perfil;
	}
	public void setId_perfil(int id_perfil) {
		this.id_perfil = id_perfil;
	}
	public String getNombre_perfil() {
		return nombre_perfil;
	}
	public void setNombre_perfil(String nombre_perfil) {
		this.nombre_perfil = nombre_perfil;
	}
	
	public Boolean getFlag_obligatorio() {
		return flag_obligatorio;
	}

	public void setFlag_obligatorio(Boolean flag_obligatorio) {
		this.flag_obligatorio = flag_obligatorio;
	}

	public ArrayList<Permiso> getPermisos() {
		return permisos;
	}

	public void setPermisos(ArrayList<Permiso> permisos) {
		this.permisos = permisos;
	}

	public String getNombre_perfil_ad() {
		return nombre_perfil_ad;
	}

	public void setNombre_perfil_ad(String nombre_perfil_ad) {
		this.nombre_perfil_ad = nombre_perfil_ad;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}

	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}	
	
}
