package sim.persistencia.dic;

public class TipoObjeto {
	
	private Integer id_tipo_objeto;
	private String nombre_tipo_objeto;
	
	public TipoObjeto() {		
	}

	public String getNombre_tipo_objeto() {
		return nombre_tipo_objeto;
	}

	public void setNombre_tipo_objeto(String nombre_tipo_objeto) {
		this.nombre_tipo_objeto = nombre_tipo_objeto;
	}

	public Integer getId_tipo_objeto() {
		return id_tipo_objeto;
	}

	public void setId_tipo_objeto(Integer id_tipo_objeto) {
		this.id_tipo_objeto = id_tipo_objeto;
	}

}
