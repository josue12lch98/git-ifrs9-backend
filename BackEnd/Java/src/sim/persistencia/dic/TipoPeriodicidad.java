package sim.persistencia.dic;

public class TipoPeriodicidad {
	private Integer id_tipo_periodicidad;
	private String nombre_tipo_periodicidad;
	private Integer valor_periodicidad;
	
	public TipoPeriodicidad() {
	}
	
	public Integer getId_tipo_periodicidad() {
		return id_tipo_periodicidad;
	}
	public void setId_tipo_periodicidad(Integer id_tipo_periodicidad) {
		this.id_tipo_periodicidad = id_tipo_periodicidad;
	}
	public String getNombre_tipo_periodicidad() {
		return nombre_tipo_periodicidad;
	}
	public void setNombre_tipo_periodicidad(String nombre_tipo_periodicidad) {
		this.nombre_tipo_periodicidad = nombre_tipo_periodicidad;
	}

	public Integer getValor_periodicidad() {
		return valor_periodicidad;
	}
	public void setValor_periodicidad(Integer valor_periodicidad) {
		this.valor_periodicidad = valor_periodicidad;
	}
	
}
