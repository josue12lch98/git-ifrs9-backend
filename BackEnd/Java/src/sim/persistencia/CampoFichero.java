package sim.persistencia;

public class CampoFichero extends Campo {

	private Boolean flag_fecha_fichero; // Indica si el campo contiene la fecha del fichero

	// Constructor
	public CampoFichero() {
		super();
	}

	// Getters y Setters
	public Boolean getFlag_fecha_fichero() {
		return flag_fecha_fichero;
	}

	public void setFlag_fecha_fichero(Boolean flag_fecha_fichero) {
		this.flag_fecha_fichero = flag_fecha_fichero;
	}

}