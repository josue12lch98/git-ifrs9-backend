package sim.persistencia;

import java.util.ArrayList;

public class EntidadProgreso {
	private String nombre_entidad;
	private Integer id_entidad;
	private ArrayList<CarteraProgreso> carteras_progreso;

	public ArrayList<CarteraProgreso> getCarteras_progreso() {
		return carteras_progreso;
	}

	public void setCarteras_progreso(ArrayList<CarteraProgreso> carteras_progreso) {
		this.carteras_progreso = carteras_progreso;
	}

	public Integer getId_entidad() {
		return id_entidad;
	}

	public void setId_entidad(Integer id_entidad) {
		this.id_entidad = id_entidad;
	}

	public String getNombre_entidad() {
		return nombre_entidad;
	}

	public void setNombre_entidad(String nombre_entidad) {
		this.nombre_entidad = nombre_entidad;
	}

}
