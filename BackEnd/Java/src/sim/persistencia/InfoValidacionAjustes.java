package sim.persistencia;

import java.util.ArrayList;

public class InfoValidacionAjustes {
	private String comentarios_validacion;
	private ArrayList<AjusteRegistro> ajustes_registro;
	
	public InfoValidacionAjustes() {		
	}

	public String getComentarios_validacion() {
		return comentarios_validacion;
	}

	public void setComentarios_validacion(String comentarios_validacion) {
		this.comentarios_validacion = comentarios_validacion;
	}

	public ArrayList<AjusteRegistro> getAjustes_registro() {
		return ajustes_registro;
	}

	public void setAjustes_registro(ArrayList<AjusteRegistro> ajustes_registro) {
		this.ajustes_registro = ajustes_registro;
	}
}
