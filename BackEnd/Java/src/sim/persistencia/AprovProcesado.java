package sim.persistencia;

import java.util.ArrayList;

public class AprovProcesado {
	private Integer nro_lineas;
	private ArrayList<Boolean> error_campos;
	
	public AprovProcesado() {
		
	}

	public Integer getNro_lineas() {
		return nro_lineas;
	}

	public void setNro_lineas(Integer nro_lineas) {
		this.nro_lineas = nro_lineas;
	}

	public ArrayList<Boolean> getError_campos() {
		return error_campos;
	}

	public void setError_campos(ArrayList<Boolean> error_campos) {
		this.error_campos = error_campos;
	}
	
	
}
