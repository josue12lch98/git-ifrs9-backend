package sim.persistencia.dic;

public class FormatoInput {
	private Integer id_formato_input;
	private String nombre_formato_input;
	
	public FormatoInput() {
	}

	public Integer getId_formato_input() {
		return id_formato_input;
	}

	public void setId_formato_input(Integer id_formato_input) {
		this.id_formato_input = id_formato_input;
	}

	public String getNombre_formato_input() {
		return nombre_formato_input;
	}

	public void setNombre_formato_input(String nombre_formato_input) {
		this.nombre_formato_input = nombre_formato_input;
	}	
}
