package sim.persistencia;

import java.util.ArrayList;

public class TablaInput extends Aprovisionamiento {
	private Integer id_tablainput;
	private Integer version_metadata;
	private String nombre_metadata;
	private Boolean flag_ajustado;
	private String comentario_ajuste;
	private Integer estado_ajuste;
	private Boolean flag_normalizada;
	private ArrayList<CampoInput> campos_input; // Campos del aprovisionamiento

	public Integer getId_tablainput() {
		return id_tablainput;
	}

	public void setId_tablainput(Integer id_tablainput) {
		this.id_tablainput = id_tablainput;
	}

	public Integer getVersion_metadata() {
		return version_metadata;
	}

	public void setVersion_metadata(Integer version_metadata) {
		this.version_metadata = version_metadata;
	}

	public String getNombre_metadata() {
		return nombre_metadata;
	}

	public void setNombre_metadata(String nombre_metadata) {
		this.nombre_metadata = nombre_metadata;
	}

	public Boolean getFlag_ajustado() {
		return flag_ajustado;
	}

	public void setFlag_ajustado(Boolean flag_ajustado) {
		this.flag_ajustado = flag_ajustado;
	}

	public String getComentario_ajuste() {
		return comentario_ajuste;
	}

	public void setComentario_ajuste(String comentario_ajuste) {
		this.comentario_ajuste = comentario_ajuste;
	}

	public Integer getEstado_ajuste() {
		return estado_ajuste;
	}

	public void setEstado_ajuste(Integer estado_ajuste) {
		this.estado_ajuste = estado_ajuste;
	}

	public Boolean getFlag_normalizada() {
		return flag_normalizada;
	}

	public void setFlag_normalizada(Boolean flag_normalizada) {
		this.flag_normalizada = flag_normalizada;
	}

	public ArrayList<CampoInput> getCampos_input() {
		return campos_input;
	}

	public void setCampos_input(ArrayList<CampoInput> campos_input) {
		this.campos_input = campos_input;
	}

	// Obtener solamente la lista de nombres de campos aprovisionamiento
	public ArrayList<String> obtenerNombresCamposInput() throws Exception {
		ArrayList<String> nombres = new ArrayList<String>();
		for (CampoInput campo : this.campos_input) {
			nombres.add(campo.getNombre_campo());
		}
		return nombres;
	}
	// Obtener solamente la lista de nombres de campos aprovisionamiento con Tipo
		public ArrayList<String> obtenerNombresCamposInputconTipo() throws Exception {
			ArrayList<String> nombres = new ArrayList<String>();
			for (CampoInput campo : this.campos_input) {
				nombres.add(campo.getNombre_campo()+campo.getTipo_campo().getId_tipo_campo());
			}
			return nombres;
		}
}
