package sim.persistencia;

import sim.persistencia.dic.TipoAmbito;
import sim.persistencia.dic.TipoCartera;
import sim.persistencia.dic.TipoMagnitud;

public class Cartera {
	
	private Integer id_cartera;
	private Entidad entidad;
	private String nombre_cartera;
	private String nombre_variablemotor;
	private Integer orden;
	private TipoAmbito tipo_ambito;
	private TipoMagnitud tipo_magnitud;
	private TipoCartera tipo_cartera;
	private Boolean flag_pd6meses;

	public Cartera() {
	}

	public Integer getId_cartera() {
		return id_cartera;
	}

	public void setId_cartera(Integer id_cartera) {
		this.id_cartera = id_cartera;
	}

	public Entidad getEntidad() {
		return entidad;
	}

	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	public String getNombre_cartera() {
		return nombre_cartera;
	}

	public void setNombre_cartera(String nombre_cartera) {
		this.nombre_cartera = nombre_cartera;
	}

	public TipoAmbito getTipo_ambito() {
		return tipo_ambito;
	}

	public void setTipo_ambito(TipoAmbito tipo_ambito) {
		this.tipo_ambito = tipo_ambito;
	}

	public TipoMagnitud getTipo_magnitud() {
		return tipo_magnitud;
	}

	public void setTipo_magnitud(TipoMagnitud tipo_magnitud) {
		this.tipo_magnitud = tipo_magnitud;
	}	

	public String getNombre_variablemotor() {
		return nombre_variablemotor;
	}

	public void setNombre_variablemotor(String nombre_variablemotor) {
		this.nombre_variablemotor = nombre_variablemotor;
	}

	public TipoCartera getTipo_cartera() {
		return tipo_cartera;
	}

	public void setTipo_cartera(TipoCartera tipo_cartera) {
		this.tipo_cartera = tipo_cartera;
	}	

	public Boolean getFlag_pd6meses() {
		return flag_pd6meses;
	}

	public void setFlag_pd6meses(Boolean flag_pd6meses) {
		this.flag_pd6meses = flag_pd6meses;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

}
