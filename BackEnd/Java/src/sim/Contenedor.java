package sim;

import java.util.ArrayList;

import sim.controlador.Controlador;
import sim.persistencia.*;
import sim.persistencia.dic.*;

public class Contenedor {

	private static final sim.LoggerWrapper LOGGER = sim.LoggerWrapper.getLogger(Contenedor.class);
	private static Contenedor contenedor;
	private static Controlador oc;

	private Contenedor() {
		LOGGER.info("Creando contenedor");
		try {
			oc = Controlador.obtenerInstancia();
		} catch (Exception e) {
			LOGGER.fatal("Error al construir Controlador: " + e.getMessage());
		}
		LOGGER.info("Contenedor creado");
	}

	public static synchronized Contenedor obtenerInstancia() {
		if (contenedor == null) {
			contenedor = new Contenedor();
		}
		return contenedor;
	}

	// Aprovisionamiento
	public ArrayList<Ruta> obtenerRutas(Integer id_tipo_ambito) throws Exception {
		return oc.obtenerRutas(id_tipo_ambito);
	}

	public void crearRuta(Ruta ruta) throws Exception {
		oc.crearRuta(ruta);
	}

	public RutaValida editarRuta(Ruta ruta) throws Exception {
		return oc.editarRuta(ruta);
	}

	public void borrarRuta(Integer id_ruta) throws Exception {
		oc.borrarRuta(id_ruta);
	}

	public void seleccionarRutas(ArrayList<Ruta> rutas) throws Exception {
		oc.seleccionarRutas(rutas);
	}

	public Aprovisionamiento obtenerFicheroConCampos(Aprovisionamiento fichero) throws Exception {
		return oc.obtenerFicheroConCampos(fichero);
	}

	public ArrayList<ReglaCalidad> obtenerReglasCalidad(ReglaCalidad regla) throws Exception {
		return oc.obtenerReglasCalidad(regla);
	}

	public void crearAprovisionamiento(Aprovisionamiento fichero) throws Exception {
		oc.crearAprovisionamiento(fichero);
	}

	public void editarAprovisionamiento(Aprovisionamiento fichero) throws Exception {
		oc.editarAprovisionamiento(fichero);
	}

	public void borrarAprovisionamiento(Integer id_aprovisionamiento) throws Exception {
		oc.borrarAprovisionamiento(id_aprovisionamiento);
	}

	public ArrayList<CargaFichero> obtenerCargasFicheros(Integer id_tipo_ambito) throws Exception {
		return oc.obtenerCargasFicheros(id_tipo_ambito);
	}

	public void editarFichero(Aprovisionamiento aprov) throws Exception {
		oc.editarFichero(aprov);
	}

	public ArrayList<Aprovisionamiento> obtenerAprovisionamientoInput() throws Exception {
		return oc.obtenerAprovisionamientoInput();
	}

	public ArrayList<CampoAprovisionamiento> obtenerCampos(Aprovisionamiento aprov) throws Exception {
		return oc.obtenerCampos(aprov);
	}

	public CargaFichero procesarCargaFichero(CargaFichero cargaFichero) throws Exception {
		return oc.procesarCargaFichero(cargaFichero);
	}

	public void editarCargaFichero(CargaFichero cargaFichero) throws Exception {
		oc.editarCargaFichero(cargaFichero);
	}

	public ArrayList<CargaFichero> obtenerCargasFicheroTotales() throws Exception {
		return oc.obtenerCargasFicheroTotales();
	}

	public ArrayList<AjusteFilaInput> obtenerRegistrosdeCargaFichero(CargaFichero cargaFichero) throws Exception {
		return oc.obtenerRegistrosdeCargaFichero(cargaFichero);
	}

	public void editarRegistrodeCargaFichero(AjusteFilaInput ajusteFilaInput) throws Exception {
		oc.editarRegistrodeCargaFichero(ajusteFilaInput);
	}

	public void crearRegistrodeCargaFichero(AjusteFilaInput ajusteFilaInput) throws Exception {
		oc.crearRegistrodeCargaFichero(ajusteFilaInput);
	}

	public void borrarRegistrosdeCargaFichero(ArrayList<AjusteFilaInput> ajustesFilaInput) throws Exception {
		oc.borrarRegistrosdeCargaFichero(ajustesFilaInput);
	}

	public void editarNuevosCampos(Aprovisionamiento fichero) throws Exception {
		oc.editarNuevosCampos(fichero);
	}

	public void editarReglasSegmento(SegmentoAprovisionamiento segmento) throws Exception {
		oc.editarReglasSegmento(segmento);
	}

	public void editarCamposAprovisionamiento(Aprovisionamiento aprov) throws Exception {
		oc.editarCamposAprovisionamiento(aprov);
	}

	public void editarReglaCalidad(ReglaCalidad regla) throws Exception {
		oc.editarReglaCalidad(regla);
	}

	public void replicarReglasCampo(ArrayList<CampoAprovisionamiento> campos) throws Exception {
		oc.replicarReglasCampo(campos);
	}

	public void editarReglasCalidad(ArrayList<ReglaCalidad> reglas) throws Exception {
		oc.editarReglasCalidad(reglas);
	}

	public void editarCamposSegmento(Aprovisionamiento aprov) throws Exception {
		oc.editarCamposSegmento(aprov);
	}

	public void editarCampoSegmento(SegmentoAprovisionamiento segmento) throws Exception {
		oc.editarCampoSegmento(segmento);
	}

	public void borrarCampoSegmento(Integer id_segmento) throws Exception {
		oc.borrarCampoSegmento(id_segmento);
	}

	public ArrayList<ReglaCalidad> obtenerReglasCalidadPorCampo(CampoAprovisionamiento campo) throws Exception {
		return oc.obtenerReglasCalidadPorCampo(campo);
	}

	public ArrayList<SegmentoAprovisionamiento> obtenerSegmentosPorAprov(Integer id_aprov) throws Exception {
		return oc.obtenerSegmentosPorAprov(id_aprov);
	}

	public ArrayList<SegmentoAprovisionamiento> obtenerSegmentosPorCampoAprov(Integer id_campoaprov) throws Exception {
		return oc.obtenerSegmentosPorCampoAprov(id_campoaprov);
	}

	public void aprobarCargaFichero(CargaFichero cargaFichero) throws Exception {
		oc.aprobarCargaFichero(cargaFichero);
	}

	public void rechazarCargaFichero(CargaFichero cargaFichero) throws Exception {
		oc.rechazarCargaFichero(cargaFichero);
	}

	public ArrayList<CargaFichero> actualizarEstadoCargaCalidad(ArrayList<CargaFichero> cargasFichero)
			throws Exception {
		return oc.actualizarEstadoCargaCalidad(cargasFichero);
	}

	// Configuracion
	// Fechas
	public ArrayList<Fecha> obtenerFechas() throws Exception {
		return oc.obtenerFechas();
	}

	public void crearFecha(Fecha fecha) throws Exception {
		oc.crearFecha(fecha);
	}

	public void editarFecha(Fecha fecha) throws Exception {
		oc.editarFecha(fecha);
	}

	// Entidad
	public ArrayList<Entidad> obtenerEntidades() throws Exception {
		return oc.obtenerEntidades();
	}

	public void crearEntidad(Entidad entidad) throws Exception {
		oc.crearEntidad(entidad);
	}

	public void editarEntidad(Entidad entidad) throws Exception {
		oc.editarEntidad(entidad);
	}

	public void borrarEntidad(Integer id_entidad) throws Exception {
		oc.borrarEntidad(id_entidad);
	}

	public ArrayList<Pais> obtenerPaises() throws Exception {
		return oc.obtenerPaises();
	}

	// Cartera
	public void crearCartera(Cartera cartera) throws Exception {
		oc.crearCartera(cartera);
	}

	public ArrayList<Cartera> obtenerCarterasPorIdEntidad(Integer id_entidad) throws Exception {
		return oc.obtenerCarterasPorIdEntidad(id_entidad);
	}

	public ArrayList<Cartera> obtenerCarteras() throws Exception {
		return oc.obtenerCarteras();
	}

	public ArrayList<TipoAmbito> obtenerTiposAmbito() throws Exception {
		return oc.obtenerTiposAmbito();
	}

	public ArrayList<TipoMagnitud> obtenerTiposMagnitud(Integer id_tipo_ambito) throws Exception {
		return oc.obtenerTiposMagnitud(id_tipo_ambito);
	}

	public ArrayList<TipoCartera> obtenerTiposCartera() throws Exception {
		return oc.obtenerTiposCartera();
	}

	// ----------------------------Version Parametria---------------------
	public void crearCarteraVersion(CarteraVersion_staging carteraVersion) throws Exception {
		oc.crearCarteraVersion(carteraVersion);
	}

	public void editarCarteraVersion(CarteraVersion_staging carteraVersion) throws Exception {
		oc.editarCarteraVersion(carteraVersion);
	}

	public void replicarCarteraVersion(CarteraVersion_staging carteraVersion) throws Exception {
		oc.replicarCarteraVersion(carteraVersion);
	}

	public ArrayList<CarteraVersion_staging> obtenerCarterasVersionStaging() throws Exception {
		return oc.obtenerCarterasVersionStaging();
	}

	public ArrayList<CarteraVersion_staging> obtenerCarterasVersionStagingPorIdCartera(Integer id_cartera)
			throws Exception {
		return oc.obtenerCarterasVersionStagingPorIdCartera(id_cartera);
	}

	public ArrayList<JuegoEscenarios_version> obtenerJuegosConEscenariosPorCarteraVersion(Integer id_cartera_version)
			throws Exception {
		return oc.obtenerJuegosConEscenariosPorCarteraVersion(id_cartera_version);
	}

	public void borrarCarteraVersion(Integer id_cartera_version) throws Exception {
		oc.borrarCarteraVersion(id_cartera_version);
	}

	// --------------------- Parametrización Variables ----------------------
	// --------------------- Made by Yisus --------------------------------
	public BloqueParametrizado obtenerBloqueBuckets(Integer id_cartera_version) throws Exception {
		return oc.obtenerBloqueBuckets(id_cartera_version);
	}

	public BloqueParametrizado editarBloqueParametrizado(BloqueParametrizado bloqueParam) throws Exception {
		return oc.editarBloqueParametrizado(bloqueParam);
	}

	public void editarBloquesParametrizados(ArrayList<BloqueParametrizado> bloquesParam) throws Exception {
		oc.editarBloquesParametrizados(bloquesParam);
	}

	// --------------------- Parametrización Variables ----------------------
	// --------------------- Made by Yisus --------------------------------
	public ArrayList<BloqueParametrizado> obtenerBloquesParametrizadoPorCarteraVersion(Integer id_cartera_version)
			throws Exception {
		return oc.obtenerBloquesParametrizadoPorCarteraVersion(id_cartera_version);
	}

	public ArrayList<TablaInput> obtenerTablasInput() throws Exception {
		return oc.obtenerTablasInput();
	}

	public ArrayList<TablaInput> obtenerTablasInputPorIdCartera(Integer id_cartera) throws Exception {
		return oc.obtenerTablasInputPorIdCartera(id_cartera);
	}

	public ArrayList<ResultadoVariableMacro> obtenerResultadosVariablesMacrosPorEstructura(Integer id_metodologia)
			throws Exception {
		return oc.obtenerResultadosVariablesMacrosPorEstructura(id_metodologia);
	}

	// --------------------- Parametrización Staging ----------------------
	// --------------------- Made by Yisus --------------------------------
	public void editarStagingCarteraVersion(CarteraVersion_staging carteraVersion) throws Exception {
		oc.editarStagingCarteraVersion(carteraVersion);
	}
	// ---------------------- Expresiones ----------------------------------

	public void editarExpresionesStaging(ArrayList<Expresion_Staging> expresiones) throws Exception {
		oc.editarExpresionesStaging(expresiones);
	}

	public ArrayList<Expresion_Staging> obtenerExpresionesStagingPorCarteraVersion(Integer id_version_Cartera)
			throws Exception {
		return oc.obtenerExpresionesStagingPorCarteraVersion(id_version_Cartera);
	}
	// ---------------------- Reglas ----------------------------------

	public void editarReglasStaging(ArrayList<Regla_Staging> reglas) throws Exception {
		oc.editarReglasStaging(reglas);
	}

	public ArrayList<Regla_Staging> obtenerReglasStagingPorCarteraVersion(Integer id_version_Cartera) throws Exception {
		return oc.obtenerReglasStagingPorCarteraVersion(id_version_Cartera);
	}

	// Métodos Escenario
	public ArrayList<Escenario> obtenerEscenarios() throws Exception {
		return oc.obtenerEscenarios();
	}

	public void crearEscenario(Escenario escenario) throws Exception {
		oc.crearEscenario(escenario);
	}

	public void editarEscenario(Escenario escenario) throws Exception {
		oc.editarEscenario(escenario);
	}

	public void borrarEscenario(Integer id_escenario) throws Exception {
		oc.borrarEscenario(id_escenario);
	}

	public Cartera obtenerCartera(Integer id_cartera) throws Exception {
		return oc.obtenerCartera(id_cartera);
	}

	public void borrarCartera(Integer id_cartera) throws Exception {
		oc.borrarCartera(id_cartera);
	}

	public void editarCartera(Cartera cartera) throws Exception {
		oc.editarCartera(cartera);
	}

	// Stage
	public void crearStage(Stage stage) throws Exception {
		oc.crearStage(stage);
	}

	public ArrayList<Stage> obtenerStages() throws Exception {
		return oc.obtenerStages();
	}

	public OutputModeloMacro obtenerOutputModeloMacro(OutputModeloMacro output) throws Exception {
		return oc.obtenerOutputModeloMacro(output);
	}

	public Stage obtenerStage(Integer id_stage) throws Exception {
		return oc.obtenerStage(id_stage);
	}

	public void editarStage(Stage stage) throws Exception {
		oc.editarStage(stage);
	}

	public void borrarStage(Integer id_stage) throws Exception {
		oc.borrarStage(id_stage);
	}

	// VARIABLE
	public void crearVariable(Variable variable) throws Exception {
		oc.crearVariable(variable);
	}

	public Variable obtenerVariable(Integer id_variable) throws Exception {
		return oc.obtenerVariable(id_variable);
	}

	public ArrayList<Variable> obtenerVariables() throws Exception {
		return oc.obtenerVariables();
	}

	public void editarVariable(Variable variable) throws Exception {
		oc.editarVariable(variable);
	}

	public void borrarVariable(Integer id_variable) throws Exception {
		oc.borrarVariable(id_variable);
	}

	public ArrayList<Escenario_version> obtenerEscenariosVersionPorCarteraVersion(Integer id_cartera_version)
			throws Exception {
		return oc.obtenerEscenariosVersionPorCarteraVersion(id_cartera_version);
	}

	public void editarEscenarioVersion(Escenario_version escenario_version) throws Exception {
		oc.editarEscenarioVersion(escenario_version);
	}

	public void borrarEscenarioVersion(Integer id_version) throws Exception {
		oc.borrarEscenarioVersion(id_version);
	}

	// BLOQUE
	public void crearBloque(Bloque bloque) throws Exception {
		oc.crearBloque(bloque);
	}

	public ArrayList<CampoBloque> obtenerCamposBloquePorIdBloque(Integer id_bloque) throws Exception {
		return oc.obtenerCamposBloquePorIdBloque(id_bloque);
	}

	public void borrarBloque(Integer id_bloque) throws Exception {
		oc.borrarBloque(id_bloque);
	}

	public void editarBloque(Bloque bloque) throws Exception {
		oc.editarBloque(bloque);
	}

	public ArrayList<Bloque> obtenerBloques() throws Exception {
		return oc.obtenerBloques();
	}

	public ArrayList<FormatoInput> obtenerFormatosInput() throws Exception {
		return oc.obtenerFormatosInput();
	}

	public ArrayList<TipoEjecucion> obtenerTiposEjecucion() throws Exception {
		return oc.obtenerTiposEjecucion();
	}

	public ArrayList<TipoFlujo> obtenerTiposFlujo() throws Exception {
		return oc.obtenerTiposFlujo();
	}

	public ArrayList<TipoCampo> obtenerTiposCampo() throws Exception {
		return oc.obtenerTiposCampo();
	}

	public ArrayList<TipoBloque> obtenerTiposBloque() throws Exception {
		return oc.obtenerTiposBloque();
	}

	public void crearTipoBloque(TipoBloque tipobloque) throws Exception {
		oc.crearTipoBloque(tipobloque);
	}

	public void borrarTipoBloque(Integer id_tipobloque) throws Exception {
		oc.borrarTipoBloque(id_tipobloque);
	}

	// METODOLOGÍA
	public ArrayList<EstructuraMetodologica> obtenerMetodologias() throws Exception {
		return oc.obtenerMetodologias();
	}

	public EstructuraMetodologica obtenerMetodologia(Integer id_metodologia) throws Exception {
		return oc.obtenerMetodologia(id_metodologia);
	}

	public ArrayList<TipoObtencionResultados> obtenerTiposObtencionResultados() throws Exception {
		return oc.obtenerTiposObtencionResultados();
	}

	public ArrayList<TipoPeriodicidad> obtenerTiposPeriodicidad() throws Exception {
		return oc.obtenerTiposPeriodicidad();
	}

	public ArrayList<MotorCalculo> obtenerMotoresCalculo() throws Exception {
		return oc.obtenerMotoresCalculo();
	}

	public ArrayList<TipoMetodologia> obtenerTiposMetodologia() throws Exception {
		return oc.obtenerTiposMetodologia();
	}

	public void crearTipoMetodologia(TipoMetodologia metodologia) throws Exception {
		oc.crearTipoMetodologia(metodologia);
	}

	public void editarTipoMetodologia(TipoMetodologia tipometodologia) throws Exception {
		oc.editarTipoMetodologia(tipometodologia);
	}

	public void borrarTipoMetodologia(Integer id_tipometodologia) throws Exception {
		oc.borrarTipoMetodologia(id_tipometodologia);
	}

	public void crearTipoCartera(TipoCartera tipocartera) throws Exception {
		oc.crearTipoCartera(tipocartera);
	}

	public void editarTipoCartera(TipoCartera tipocartera) throws Exception {
		oc.editarTipoCartera(tipocartera);
	}

	public void borrarTipoCartera(Integer id_tipocartera) throws Exception {
		oc.borrarTipoCartera(id_tipocartera);
	}

	public ArrayList<Cartera> obtenerCarterasExtrapolacionporIdMetodologia(Integer id_metodologia) throws Exception {
		return oc.obtenerCarterasExtrapolacionporIdMetodologia(id_metodologia);
	}

	public void crearMetodologia(EstructuraMetodologica metodologia) throws Exception {
		oc.crearMetodologia(metodologia);
	}

	public void editarMetodologia(EstructuraMetodologica metodologia) throws Exception {
		oc.editarMetodologia(metodologia);
	}

	public void borrarMetodologia(Integer id_metodologia) throws Exception {
		oc.borrarMetodologia(id_metodologia);
	}
	
	public void replicarEstructuraConCarterasVer(
			ReplicaEstructuraConCarVer replicaEstructuraConCarVer) throws Exception {
		oc.replicarEstructuraConCarterasVer(replicaEstructuraConCarVer);
	}

	// Autenticacion
	public Usuario loguearUsuario(Usuario usuario) throws Exception {
		return oc.loguearUsuario(usuario);
	}
	
	// Administracion sistema
	// Tarea
	public ArrayList<Tarea> obtenerTareas() throws Exception {
		return oc.obtenerTareas();
	}

	public void crearTarea(Tarea tarea) throws Exception {
		oc.crearTarea(tarea);
	}

	public void editarTarea(Tarea tarea) throws Exception {
		oc.editarTarea(tarea);
	}

	public void borrarTarea(Integer id_tarea) throws Exception {
		oc.borrarTarea(id_tarea);
	}

	// Perfil/Rol - Permisos
	public void crearPerfil(Perfil perfil) throws Exception {
		oc.crearPerfil(perfil);
	}

	public void editarPerfil(Perfil perfil) throws Exception {
		oc.editarPerfil(perfil);
	}

	public void borrarPerfil(Integer id_perfil) throws Exception {
		oc.borrarPerfil(id_perfil);
	}

	public ArrayList<Perfil> obtenerPerfiles() throws Exception {
		return oc.obtenerPerfiles();
	}

	public ArrayList<Permiso> obtenerPermisos() throws Exception {
		return oc.obtenerPermisos();
	}

	public ArrayList<Perfil> obtenerPerfilesPermisos() throws Exception {
		return oc.obtenerPerfilesPermisos();
	}

	public ArrayList<Permiso> obtenerPermisosPorIdPerfil(Integer id_perfil) throws Exception {
		return oc.obtenerPermisosPorIdPerfil(id_perfil);
	}

	// Correos
	public void crearCorreo(Correo correo) throws Exception {
		oc.crearCorreo(correo);
	}

	public void editarCorreo(Correo correo) throws Exception {
		oc.editarCorreo(correo);
	}

	public void borrarCorreo(Integer id_correo) throws Exception {
		oc.borrarCorreo(id_correo);
	}

	public ArrayList<Correo> obtenerCorreosConNotificaciones(Integer id_tipoAmbito) throws Exception {
		return oc.obtenerCorreosConNotificaciones(id_tipoAmbito);
	}

	// Eventos
	public void crearEvento(Evento evento) throws Exception {
		oc.crearEvento(evento);
	}

	public ArrayList<Evento> obtenerEventos(Integer id_ambito) throws Exception {
		return oc.obtenerEventos(id_ambito);
	}

	public EventoDetalle obtenerEventoDetalle(Integer id_evento) throws Exception {
		return oc.obtenerEventoDetalle(id_evento);
	}
	
	// Alertas
	public ArrayList<Alerta> obtenerAlertas() throws Exception {
		return oc.obtenerAlertas();
	}

	// Progreso
	public ArrayList<EntidadProgreso> obtenerEntidadesProgreso(FiltroProgreso filtroProgreso) throws Exception {
		return oc.obtenerEntidadesProgreso(filtroProgreso);
	}

	// Ejecucion
	public ArrayList<EstructuraMetodologica> obtenerEstructurasPorIdCartera(Integer id_cartera) throws Exception {
		return oc.obtenerEstructurasPorIdCartera(id_cartera);
	}

	public ArrayList<EstructuraMetodologica> obtenerEstructurasCalcPePorIdCartera(Integer id_cartera) throws Exception {
		return oc.obtenerEstructurasCalcPePorIdCartera(id_cartera);
	}

	public ArrayList<Cartera_version> obtenerCarterasVersionPorIdMetodologia(Integer id_metodologia) throws Exception {
		return oc.obtenerCarterasVersionPorIdMetodologia(id_metodologia);
	}

	public JuegoEscenarios_version obtenerJuegoVigentePorCartera(Integer id_cartera) throws Exception {
		return oc.obtenerJuegoVigentePorCartera(id_cartera);
	}
	
	public JuegoEscenarios_version obtenerJuegoVigentePorCarteraFase2(Integer id_cartera) throws Exception {
		return oc.obtenerJuegoVigentePorCarteraFase2(id_cartera);
	}

	public EjecucionAgregadaPE obtenerEjecucionFase1OficialPorCarteraCodmes(Integer id_cartera, Integer codmes)
			throws Exception {
		return oc.obtenerEjecucionFase1OficialPorCarteraCodmes(id_cartera, codmes);
	}

	public ArrayList<EjecucionPE> obtenerEjecucionesBaseFase1OficialPorCartera(Integer id_cartera) throws Exception {
		return oc.obtenerEjecucionesBaseFase1OficialPorCartera(id_cartera);
	}
	
	public ArrayList<EjecucionPE> obtenerEjecucionesBaseOficialPorCarteraCodmesFase(Integer id_cartera, Integer codmes, Integer fase) throws Exception {
		return oc.obtenerEjecucionesBaseOficialPorCarteraCodmesFase(id_cartera, codmes, fase);
	}
	
	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoVariacPorIdEjecVar(Integer id_ejecucion) throws Exception {
		return oc.obtenerTablasMapeoVariacPorIdEjecVar(id_ejecucion);
	}

	public Integer obtenerCodmesActual_fase1(Integer id_tipo_ambito) throws Exception {
		return oc.obtenerCodmesActual_fase1(id_tipo_ambito);
	}

	public Integer obtenerCodmesActual_fase2(Integer id_tipo_ambito) throws Exception {
		return oc.obtenerCodmesActual_fase2(id_tipo_ambito);
	}

	public Integer obtenerCodmesActualPorFase(Integer fase, Integer id_tipo_ambito) throws Exception {
		return oc.obtenerCodmesActualPorFase(fase, id_tipo_ambito);
	}

	public ArrayList<JuegoEscenarios_version> obtenerJuegosEscenariosVersionPorIdCarteraVersion(
			Integer id_cartera_version) throws Exception {
		return oc.obtenerJuegosEscenariosVersionPorIdCarteraVersion(id_cartera_version);
	}

	public ArrayList<EjecucionPrimerPaso> obtenerEjecucionesModMacPorMetodologia(Integer id_metodologia)
			throws Exception {
		return oc.obtenerEjecucionesModMacPorMetodologia(id_metodologia);
	}

	public ArrayList<EjecucionPrimerPaso> obtenerEjecucionesRepricingPorMetodologia(Integer id_metodologia)
			throws Exception {
		return oc.obtenerEjecucionesRepricingPorMetodologia(id_metodologia);
	}

	public ArrayList<Escenario_version> obtenerEscenariosVersionPorIdJuegoEscenarios(Integer id_juego_escenarios)
			throws Exception {
		return oc.obtenerEscenariosVersionPorIdJuegoEscenarios(id_juego_escenarios);
	}

	public void ejecutarCalculo_fase1(EjecucionAgregadaPE ejecucionPE) throws Exception {
		oc.ejecutarCalculo_fase1(ejecucionPE);
	}

	public void ejecutarSimulacion(EjecucionAgregadaPE ejecucionPE) throws Exception {
		oc.ejecutarSimulacion(ejecucionPE);
	}

	public void ejecutarCalculo_fase2(EjecucionAgregadaPE ejecucionPE) throws Exception {
		oc.ejecutarCalculo_fase2(ejecucionPE);
	}

	public void ejecutarVariacion(EjecucionVariacion ejecucionVariacion) throws Exception {
		oc.ejecutarVariacion(ejecucionVariacion);
	}

	// Ajustes Gasto
	public ArrayList<AjusteGasto> obtenerAjustesGasto() throws Exception {
		return oc.obtenerAjustesGasto();
	}

	public void crearAjusteGasto(AjusteGasto ajusteGasto) throws Exception {
		oc.crearAjusteGasto(ajusteGasto);
	}

	public void editarAjusteGasto(AjusteGasto ajusteGasto) throws Exception {
		oc.editarAjusteGasto(ajusteGasto);
	}

	public void borrarAjusteGasto(Integer id_ajusteGasto) throws Exception {
		oc.borrarAjusteGasto(id_ajusteGasto);
	}

	// Ajustes delta
	public ArrayList<MapeoDeltaStage> obtenerMapeosDeltaStage(Integer id_ejecucion_f2base) throws Exception {
		return oc.obtenerMapeosDeltaStage(id_ejecucion_f2base);
	}

	public void ajustarDeltas(GrupoMapeosDelta grupoMapeosDelta) throws Exception {
		oc.ajustarDeltas(grupoMapeosDelta);
	}

	// Variaciones
	public ArrayList<EjecucionVariacion> obtenerEjecucionesVariacion() throws Exception {
		return oc.obtenerEjecucionesVariacion();
	}

	public void crearEjecucionVariacion(EjecucionVariacion ejecucionVariacion) throws Exception {
		oc.crearEjecucionVariacion(ejecucionVariacion);
	}

	public void editarEjecucionVariacion(EjecucionVariacion ejecucionVariacion) throws Exception {
		oc.editarEjecucionVariacion(ejecucionVariacion);
	}

	public void borrarEjecucionVariacion(Integer id_ejecucion) throws Exception {
		oc.borrarEjecucionVariacion(id_ejecucion);
	}

	// Ejecuciones validacion
	public ArrayList<EjecucionValidacion> obtenerEjecucionesValidacion() throws Exception {
		return oc.obtenerEjecucionesValidacion();
	}
	
	public ArrayList<LogEjecEscenario> obtenerLogsEjecucionAgregada(Integer codmes, Integer id_juego_escenarios, 
			Integer fase, Integer id_tipo_ambito) throws Exception {
		return oc.obtenerLogsEjecucionAgregada(codmes, id_juego_escenarios, fase, id_tipo_ambito);
	}
	
	public LogEjecEscenario obtenerLogEjecucionPorIdEjec(Integer id_ejecucion) throws Exception {
		return oc.obtenerLogEjecucionPorIdEjec(id_ejecucion);
	}

	public void editarEjecucionValidacion(EjecucionValidacion ejecucionValidacion) throws Exception {
		oc.editarEjecucionValidacion(ejecucionValidacion);
	}

	public void solicitarAjustesEjecucionValidacion(EjecucionValidacion ejecucionValidacion) throws Exception {
		oc.solicitarAjustesEjecucionValidacion(ejecucionValidacion);
	}

	public void exportarResultado(EjecucionValidacion ejecucionValidacion) throws Exception {
		oc.exportarResultado(ejecucionValidacion);
	}

	// EJECUCIONES AJUSTE OUTPUT

	// Ajuste por Registro
	public ArrayList<EjecucionAjustes> obtenerEjecucionesAjustes() throws Exception {
		return oc.obtenerEjecucionesAjustes();
	}

	public ArrayList<CampoOutputCredito> obtenerCamposOutputCredito() throws Exception {
		return oc.obtenerCamposOutputCredito();
	}

	public ArrayList<RegistroOutputCredito> obtenerRegistrosOutputCredito(ConsultaRegistrosOutput consulta)
			throws Exception {
		return oc.obtenerRegistrosOutputCredito(consulta);
	}

	public void ajustarRegistrosOutputCredito(GrupoRegistrosOutput grupoRegistroOutput) throws Exception {
		oc.ajustarRegistrosOutputCredito(grupoRegistroOutput);
	}

	public InfoValidacionAjustes obtenerInfoValidacionAjustes(Integer id_ejecucion) throws Exception {
		return oc.obtenerInfoValidacionAjustes(id_ejecucion);
	}

	// Ejecuciones agregadas PE
	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesAgregadaPE_fase1(Integer id_tipo_ambito) throws Exception {
		return oc.obtenerEjecucionesAgregadaPE_fase1(id_tipo_ambito);
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesAgregadaPE_fase2(Integer id_tipo_ambito) throws Exception {
		return oc.obtenerEjecucionesAgregadaPE_fase2(id_tipo_ambito);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucion_fase1(Integer codmes, Integer id_juego_escenarios,
			Integer tipo_ajusteManual, Integer id_ejecucion_modMacro) throws Exception {
		return oc.obtenerMapeoCargasModMacEjecucion_fase1(codmes, id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMacro);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucion_fase2(Integer codmes, Integer id_juego_escenarios)
			throws Exception {
		return oc.obtenerMapeoCargasModMacEjecucion_fase2(codmes, id_juego_escenarios);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacSimulacion(Integer codmes, Integer fase, Integer id_juego_escenarios,
			Integer tipo_ajusteManual, Integer id_ejecucion_modMacro) throws Exception {
		return oc.obtenerMapeoCargasModMacSimulacion(codmes, fase, id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMacro);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase1(
			Integer id_juego_escenarios, Integer tipo_ajusteManual, Integer id_ejecucion_modMacro) throws Exception {
		return oc.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase1(id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMacro);
	}

	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase2(
			Integer id_juego_escenarios, Integer tipo_ajusteManual, Integer id_ejecucion_modMacro) throws Exception {
		return oc.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenarios_fase2(id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMacro);
	}
	
	public MapeoCargasModMacEjec obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenariosSimulacion(
			Integer id_juego_escenarios, Integer tipo_ajusteManual, Integer id_ejecucion_modMacro, Integer fase) throws Exception {
		return oc.obtenerMapeoCargasModMacEjecucionPorIdJuegoEscenariosSimulacion(id_juego_escenarios, tipo_ajusteManual,
				id_ejecucion_modMacro, fase);
	}

	public void crearEjecucionAgregadaPE_fase1(EjecucionAgregadaPE ejecucionAgregadaPE) throws Exception {
		oc.crearEjecucionAgregadaPE_fase1(ejecucionAgregadaPE);
	}

	public void crearEjecucionAgregadaPE_fase2(EjecucionAgregadaPE ejecucionAgregadaPE) throws Exception {
		oc.crearEjecucionAgregadaPE_fase2(ejecucionAgregadaPE);
	}

	public ReplicaEjecucionesAgregadasPE replicarEjecucionesAgregadasPE_fase1(
			ReplicaEjecucionesAgregadasPE replicaEjecucionesPE) throws Exception {
		return oc.replicarEjecucionesAgregadasPE_fase1(replicaEjecucionesPE);
	}

	public ReplicaEjecucionesAgregadasPE replicarEjecucionesAgregadasPE_fase2(
			ReplicaEjecucionesAgregadasPE replicaEjecucionesPE) throws Exception {
		return oc.replicarEjecucionesAgregadasPE_fase2(replicaEjecucionesPE);
	}

	public void editarEjecucionAgregadaPE_fase1(EjecucionAgregadaPE ejecucionAgregadaPE) throws Exception {
		oc.editarEjecucionAgregadaPE_fase1(ejecucionAgregadaPE);
	}

	public void editarEjecucionAgregadaPE_fase2(EjecucionAgregadaPE ejecucionAgregadaPE) throws Exception {
		oc.editarEjecucionAgregadaPE_fase2(ejecucionAgregadaPE);
	}

	public void editarSimulacionAgregadaPE(EjecucionAgregadaPE ejecucionAgregadaPE) throws Exception {
		oc.editarSimulacionAgregadaPE(ejecucionAgregadaPE);
	}

	public void borrarEjecucionAgregadaPE_fase1(Integer codmes, Integer id_juego_escenarios) throws Exception {
		oc.borrarEjecucionAgregadaPE_fase1(codmes, id_juego_escenarios);
	}

	public void borrarEjecucionAgregadaPE_fase2(Integer codmes, Integer id_juego_escenarios) throws Exception {
		oc.borrarEjecucionAgregadaPE_fase2(codmes, id_juego_escenarios);
	}
	
	public void borrarSimulacionBifAgregadaPE(Integer codmes, Integer fase, Integer id_juego_escenarios) throws Exception {
		oc.borrarSimulacionBifAgregadaPE(codmes, fase, id_juego_escenarios);
	}

	// Simulaciones agregadas PE
	public ArrayList<EjecucionAgregadaPE> obtenerSimulacionesAgregadaPE(Boolean incluyeF2) throws Exception {
		return oc.obtenerSimulacionesAgregadaPE(incluyeF2);
	}

	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotalesFase1() throws Exception {
		return oc.obtenerEjecucionesPETotalesFase1();
	}
	
	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotalesFase2() throws Exception {
		return oc.obtenerEjecucionesPETotalesFase2();
	}
	
	public ArrayList<EjecucionAgregadaPE> obtenerEjecucionesPETotales() throws Exception {
		return oc.obtenerEjecucionesPETotales();
	}

	public void crearSimulacionAgregadaPE(EjecucionAgregadaPE ejecucionAgregadaPE) throws Exception {
		oc.crearSimulacionAgregadaPE(ejecucionAgregadaPE);
	}

	public ReplicaEjecucionesAgregadasPE replicarSimulacionesAgregadasPE(
			ReplicaEjecucionesAgregadasPE replicaEjecucionesPE) throws Exception {
		return oc.replicarSimulacionesAgregadasPE(replicaEjecucionesPE);
	}

	public void solicitarValidacion(EjecucionAgregadaPE ejecucionAgregada) throws Exception {
		oc.solicitarValidacion(ejecucionAgregada);
	}

	// EJECUCION DE PRIMEROS PASOS
	public ArrayList<EjecucionPrimerPaso> obtenerEjecucionesPrimerPaso() throws Exception {
		return oc.obtenerEjecucionesPrimerPaso();
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionPrimerPaso(Integer id_ejecucion) throws Exception {
		return oc.obtenerTablasMapeoEjecucionPrimerPaso(id_ejecucion);
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionPorIdPrimPasoMet(Integer id_primerPaso,
			Integer id_metodologia) throws Exception {
		return oc.obtenerTablasMapeoEjecucionPorIdPrimPasoMet(id_primerPaso, id_metodologia);
	}

	public ArrayList<PrimerPaso> obtenerPrimerosPasosPorIdMetodologia(Integer id_metodologia) throws Exception {
		return oc.obtenerPrimerosPasosPorIdMetodologia(id_metodologia);
	}

	public void crearEjecucionPrimerPaso(EjecucionPrimerPaso ejecucionPrimerPaso) throws Exception {
		oc.crearEjecucionPrimerPaso(ejecucionPrimerPaso);
	}

	public ReplicaEjecucionesPrimerPaso replicarEjecucionesPrimerPaso(
			ReplicaEjecucionesPrimerPaso replicaEjecucionesPrimerPaso) throws Exception {
		return oc.replicarEjecucionesPrimerPaso(replicaEjecucionesPrimerPaso);
	}

	public void editarEjecucionPrimerPaso(EjecucionPrimerPaso ejecucionPrimerPaso) throws Exception {
		oc.editarEjecucionPrimerPaso(ejecucionPrimerPaso);
	}

	public void borrarEjecucionPrimerPaso(Integer id_ejecucion) throws Exception {
		oc.borrarEjecucionPrimerPaso(id_ejecucion);
	}

	public void ejecutarPrimerPaso(EjecucionPrimerPaso ejecucionPrimerPaso) throws Exception {
		oc.ejecutarPrimerPaso(ejecucionPrimerPaso);
	}

	// CONFIGURACION DE PRIMEROS PASOS
	public ArrayList<PrimerPaso> obtenerPrimerosPasos() throws Exception {
		return oc.obtenerPrimerosPasos();
	}

	public PrimerPasoParametrizado obtenerPrimerPasoParametrizado(Integer id_primerPaso, Integer id_metodologia)
			throws Exception {
		return oc.obtenerPrimerPasoParametrizado(id_primerPaso, id_metodologia);
	}

	public void parametrizarPrimerPaso(PrimerPasoParametrizado primerPasoParam) throws Exception {
		oc.parametrizarPrimerPaso(primerPasoParam);
	}

	// EJECUCION DE OTROS
	public ArrayList<EstructuraMetodologica> obtenerEstructurasOtrosPorIdCartera(Integer id_cartera) throws Exception {
		return oc.obtenerEstructurasOtrosPorIdCartera(id_cartera);
	}

	public ArrayList<EjecucionOtro> obtenerEjecucionesOtros() throws Exception {
		return oc.obtenerEjecucionesOtros();
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionOtros(Integer id_ejecucion) throws Exception {
		return oc.obtenerTablasMapeoEjecucionOtros(id_ejecucion);
	}

	public ArrayList<TablaMapeoEjecucion> obtenerTablasMapeoEjecucionOtrosPorCartVer(Integer id_cartera_version)
			throws Exception {
		return oc.obtenerTablasMapeoEjecucionOtrosPorCartVer(id_cartera_version);
	}

	public void crearEjecucionOtro(EjecucionOtro ejecucion) throws Exception {
		oc.crearEjecucionOtro(ejecucion);
	}

	public ReplicaEjecucionesOtros replicarEjecucionesOtros(ReplicaEjecucionesOtros replicaEjecuciones)
			throws Exception {
		return oc.replicarEjecucionesOtros(replicaEjecuciones);
	}

	public void editarEjecucionOtro(EjecucionOtro ejecucion) throws Exception {
		oc.editarEjecucionOtro(ejecucion);
	}

	public void borrarEjecucionOtros(Integer id_ejecucion) throws Exception {
		oc.borrarEjecucionOtros(id_ejecucion);
	}

	public void ejecutarCalculoOtros(EjecucionOtro ejecucion) throws Exception {
		oc.ejecutarCalculoOtros(ejecucion);
	}
	
	//Manejo de tokens (unico usuario concurrente)
	public Boolean verificarTokenAcceso(String id_usuario, String token) throws Exception {
		return oc.verificarTokenAcceso(id_usuario, token);
	}
	
	public Boolean verificarExpiracionToken(String id_usuario) throws Exception {
		return oc.verificarExpiracionToken(id_usuario);
	}
	
	public void actualizarExpiracionSesion(String id_usuario, Long timeStmpLimite) throws Exception {
		oc.actualizarExpiracionSesion(id_usuario, timeStmpLimite);
	}
	
	public void reiniciarSesionUsuario(String id_usuario) throws Exception {
		oc.reiniciarSesionUsuario(id_usuario);
	}

	//Validacion de posible sql, html, script injection
	public void validarInjectObjeto(Object obj, Boolean validaSql, Boolean validaHtml) throws Exception {
		oc.validarInjectObjeto(obj, validaSql, validaHtml);
	}
	
	public void validarRutaSegura(String ruta) throws Exception {
		oc.validarRutaSegura(ruta);
	}

	
	

	public ConfiguracionSAS obtenerConfiguracionSAS() throws Exception {
		return oc.obtenerConfiguracionSAS();
	}
	
	public RutaValida editarConfiguracionSAS(ConfiguracionSAS conf) throws Exception {
		return oc.editarConfiguracionSAS(conf);
	}
	
	public RutaValida validarSAS(ConfiguracionSAS conf) throws Exception {
		return oc.validarSAS(conf);
	}
	public ArrayList<LogCodInicializacion> obtenerLogsCodInicializacion(Integer codmes, Integer id_juego_escenarios, 
			Integer fase, Integer id_tipo_ambito) throws Exception {
		return oc.obtenerLogsCodInicializacion(codmes, id_juego_escenarios, fase, id_tipo_ambito);
	}
	public ArrayList<LogCodInicializacion> obtenerLogsCodInicializacionVariacion(Integer id_Ejecucion) throws Exception {
		return oc.obtenerLogsCodInicializacionVariacion(id_Ejecucion);
	}
	public CargaFichero obtenerCamposporTablaInputVersion(CargaFichero cargaFichero) throws Exception {
		return oc.obtenerCamposporTablaInputVersion(cargaFichero);
	}
}