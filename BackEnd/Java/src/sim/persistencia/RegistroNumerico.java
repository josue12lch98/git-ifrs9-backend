package sim.persistencia;

public class RegistroNumerico extends Registro {
	
	private Double numero;

	public RegistroNumerico(Integer orden, Double numero) {
		super(orden);
		this.numero = numero;
	}

	@Override
	public Double getNumero() {
		return numero;
	}

	public void setNumero(Double numero) {
		this.numero = numero;
	}

	@Override
	public String getTexto() throws Exception {
		throw new Exception("Metodo getTexto no pertenece a esta clase RegistroNumerico");
	}

}
