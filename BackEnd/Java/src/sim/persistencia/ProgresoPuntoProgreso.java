package sim.persistencia;

public class ProgresoPuntoProgreso {
	private Integer porcentage_progreso;
	private String nombre_punto_progreso;
	private String detalle_punto_progreso;

	public ProgresoPuntoProgreso(Integer porcentage_progreso, String nombre_punto_progreso) {
		this.porcentage_progreso = porcentage_progreso;
		this.nombre_punto_progreso = nombre_punto_progreso;
	}

	public Integer getPorcentage_progreso() {
		return porcentage_progreso;
	}

	public void setPorcentage_progreso(Integer porcentage_progreso) {
		this.porcentage_progreso = porcentage_progreso;
	}

	public String getNombre_punto_progreso() {
		return nombre_punto_progreso;
	}

	public void setNombre_punto_progreso(String nombre_punto_progreso) {
		this.nombre_punto_progreso = nombre_punto_progreso;
	}

	public String getDetalle_punto_progreso() {
		return detalle_punto_progreso;
	}

	public void setDetalle_punto_progreso(String detalle_punto_progreso) {
		this.detalle_punto_progreso = detalle_punto_progreso;
	}

}
