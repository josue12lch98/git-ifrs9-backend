package sim.persistencia;

public class ConfiguracionSAS {
	private boolean flag_Log;
	private String host;
	private Integer puerto;
	private String usuario;
	private String contrasena;
	private String rutaRaiz;
	private String codigoInicial;
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public Integer getPuerto() {
		return puerto;
	}
	public void setPuerto(Integer puerto) {
		this.puerto = puerto;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getRutaRaiz() {
		return rutaRaiz;
	}
	public void setRutaRaiz(String rutaRaiz) {
		this.rutaRaiz = rutaRaiz;
	}
	public String getCodigoInicial() {
		return codigoInicial;
	}
	public void setCodigoInicial(String codigoInicial) {
		this.codigoInicial = codigoInicial;
	}
	public Boolean getFlag_Log() {
		return flag_Log;
	}
	public void setFlag_log(boolean flag_log) {
		this.flag_Log = flag_log;
	}
	
	
	
}
