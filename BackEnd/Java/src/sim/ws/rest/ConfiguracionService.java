package sim.ws.rest;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import sim.ws.rest.JwtRequiereToken;
import sim.ws.rest.JwtRefrescaToken;

import sim.Contenedor;
import sim.persistencia.*;
import sim.persistencia.dic.*;

@Path("/configuracion")
public class ConfiguracionService {

	private static Contenedor contenedor = Contenedor.obtenerInstancia();

	// -------------------------------------------------------------------
	// ----------------------------ENTIDADES------------------------------
	// -------------------------------------------------------------------

	@GET
	@Path("/obtenerEntidades")
	@JwtRequiereToken	
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Entidad> obtenerEntidades() throws Exception {
		ArrayList<Entidad> entidades = new ArrayList<Entidad>();
		entidades = contenedor.obtenerEntidades();
		return entidades;
	}

	@POST
	@Path("/crearEntidad")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearEntidad(Entidad entidad) throws Exception {
		contenedor.validarInjectObjeto(entidad, true, true);
		contenedor.crearEntidad(entidad);
	}

	@PUT
	@Path("/editarEntidad")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarEntidad(Entidad entidad) throws Exception {
		contenedor.validarInjectObjeto(entidad, true, true);
		contenedor.editarEntidad(entidad);
	}

	@DELETE
	@Path("/borrarEntidad/{id}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarEntidad(@PathParam("id") Integer id_entidad) throws Exception {
		contenedor.borrarEntidad(id_entidad);
	}

	@GET
	@Path("/obtenerPaises")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Pais> obtenerPaises() throws Exception {
		ArrayList<Pais> paises = new ArrayList<Pais>();
		paises = contenedor.obtenerPaises();
		return paises;
	}

	// -------------------------------------------------------------------
	// ----------------------------CARTERA--------------------------------
	// -------------------------------------------------------------------

	@POST
	@Path("/crearCartera")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearCartera(Cartera cartera) throws Exception {
		contenedor.validarInjectObjeto(cartera, true, true);
		contenedor.crearCartera(cartera);
	}

	@GET
	@Path("/obtenerCarteras")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Cartera> obtenerCarteras() throws Exception {
		ArrayList<Cartera> carteras = new ArrayList<Cartera>();
		carteras = contenedor.obtenerCarteras();
		return carteras;
	}

	@GET
	@Path("/obtenerCarterasPorIdEntidad/{id_entidad}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Cartera> obtenerCarterasPorIdEntidad(@PathParam("id_entidad") Integer id_entidad)
			throws Exception {
		ArrayList<Cartera> carteras = new ArrayList<Cartera>();
		carteras = contenedor.obtenerCarterasPorIdEntidad(id_entidad);
		return carteras;
	}

	@GET
	@Path("/obtenerTiposAmbito")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TipoAmbito> obtenerTiposAmbito() throws Exception {
		ArrayList<TipoAmbito> tiposAmbito = new ArrayList<TipoAmbito>();
		tiposAmbito = contenedor.obtenerTiposAmbito();
		return tiposAmbito;
	}

	@GET
	@Path("/obtenerTiposMagnitud/{id_tipo_ambito}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TipoMagnitud> obtenerTiposMagnitud(@PathParam("id_tipo_ambito") Integer id_tipo_ambito)
			throws Exception {
		ArrayList<TipoMagnitud> tipoMagnitud = new ArrayList<TipoMagnitud>();
		tipoMagnitud = contenedor.obtenerTiposMagnitud(id_tipo_ambito);
		return tipoMagnitud;
	}

	@GET
	@Path("/obtenerTiposCartera")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TipoCartera> obtenerTiposCartera() throws Exception {
		ArrayList<TipoCartera> tipoCartera = new ArrayList<TipoCartera>();
		tipoCartera = contenedor.obtenerTiposCartera();
		return tipoCartera;
	}

	// --------------------------------------ESCENARIO---------------------------------------------
	@GET
	@Path("/obtenerEscenarios")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Escenario> obtenerEscenarios() throws Exception {
		ArrayList<Escenario> escenarios = new ArrayList<Escenario>();
		escenarios = contenedor.obtenerEscenarios();
		return escenarios;
	}

	/*
	 * @GET
	 * 
	 * @Path("/obtenerEscenario/{id_escenario}")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public Escenario
	 * obtenerEscenario(@PathParam("id_escenario") Integer id_escenario){ Escenario
	 * escenario = new Escenario(); try { contenedor =
	 * Contenedor.obtenerInstancia(); escenario =
	 * contenedor.obtenerEscenario(id_escenario); } catch (Exception e) {
	 * e.getMessage().toString(); } return escenario; }
	 */

	@POST
	@Path("/crearEscenario")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearEscenario(Escenario escenario) throws Exception {
		contenedor.validarInjectObjeto(escenario, true, true);
		contenedor.crearEscenario(escenario);
	}

	@PUT
	@Path("/editarEscenario")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarEscenario(Escenario escenario) throws Exception {
		contenedor.validarInjectObjeto(escenario, true, true);
		contenedor.editarEscenario(escenario);
	}

	@DELETE
	@Path("/borrarEscenario/{id}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarEscenario(@PathParam("id") Integer id_escenario) throws Exception {
		contenedor.borrarEscenario(id_escenario);
	}

	// ---------------ESTRUCTURA METODOLOGICA--------------------

	@GET
	@Path("/obtenerCartera/{id_cartera}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public Cartera obtenerCartera(@PathParam("id_cartera") Integer id_cartera) throws Exception {
		return contenedor.obtenerCartera(id_cartera);
	}

	@PUT
	@Path("/editarCartera")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarCartera(Cartera cartera) throws Exception {
		contenedor.validarInjectObjeto(cartera, true, true);
		contenedor.editarCartera(cartera);
	}

	@DELETE
	@Path("/borrarCartera/{id}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarCartera(@PathParam("id") Integer id_cartera) throws Exception {
		contenedor.borrarCartera(id_cartera);
	}

	// -------------------------------------------------------------------
	// -----------------------------STAGE---------------------------------
	// -------------------------------------------------------------------

	@POST
	@Path("/crearStage")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearStage(Stage stage) throws Exception {
		contenedor.validarInjectObjeto(stage, true, true);
		contenedor.crearStage(stage);
	}

	@GET
	@Path("/obtenerStages")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Stage> obtenerStages() throws Exception {
		ArrayList<Stage> stages = new ArrayList<Stage>();
		stages = contenedor.obtenerStages();
		return stages;
	}

	@GET
	@Path("/obtenerStage/{id_stage}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public Stage obtenerStage(@PathParam("id_stage") Integer id_stage) throws Exception {
		Stage stage = new Stage();
		stage = contenedor.obtenerStage(id_stage);
		return stage;
	}

	@PUT
	@Path("/editarStage")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarStage(Stage stage) throws Exception {
		contenedor.validarInjectObjeto(stage, true, true);
		contenedor.editarStage(stage);
	}

	@DELETE
	@Path("/borrarStage/{id}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarStage(@PathParam("id") Integer id_stage) throws Exception {
		if (id_stage == 1 || id_stage == 2 || id_stage == 3) {
		} else {
			contenedor.borrarStage(id_stage);
		}
	}

	// -------------------------------------------------------------------
	// --------------------------VARIABLES--------------------------------
	// -------------------------------------------------------------------

	@POST
	@Path("/crearVariable")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearVariable(Variable variable) throws Exception {
		contenedor.validarInjectObjeto(variable, true, true);
		contenedor.crearVariable(variable);
	}

	@GET
	@Path("/obtenerVariable/{id_variable}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public Variable obtenerVariable(@PathParam("id_variable") Integer id_variable) throws Exception {
		Variable variable = new Variable();
		variable = contenedor.obtenerVariable(id_variable);
		return variable;
	}

	@GET
	@Path("/obtenerVariables")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Variable> obtenerVariables() throws Exception {
		ArrayList<Variable> variables = new ArrayList<Variable>();
		variables = contenedor.obtenerVariables();
		return variables;
	}

	@PUT
	@Path("/editarVariable")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarVariable(Variable variable) throws Exception {
		contenedor.validarInjectObjeto(variable, true, true);
		contenedor.editarVariable(variable);
	}

	@DELETE
	@Path("/borrarVariable/{id_variable}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarVariable(@PathParam("id_variable") Integer id_variable) throws Exception {
		contenedor.borrarVariable(id_variable);
	}

	// -------------------------------------------------------------------
	// ----------------------------BLOQUE---------------------------------
	// -------------------------------------------------------------------

	@POST
	@Path("/crearBloque")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearBloque(Bloque bloque) throws Exception {
		contenedor.validarInjectObjeto(bloque, true, true);
		contenedor.crearBloque(bloque);
	}

	@GET
	@Path("/obtenerCamposBloquePorIdBloque/{id_tipo_ambito}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<CampoBloque> obtenerCamposBloquePorIdBloque(@PathParam("id_tipo_ambito") Integer id_bloque)
			throws Exception {
		ArrayList<CampoBloque> camposBloque = new ArrayList<CampoBloque>();
		camposBloque = contenedor.obtenerCamposBloquePorIdBloque(id_bloque);
		return camposBloque;
	}

	@DELETE
	@Path("/borrarBloque/{id_bloque}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarBloque(@PathParam("id_bloque") Integer id_bloque) throws Exception {
		contenedor.borrarBloque(id_bloque);
	}

	@PUT
	@Path("/editarBloque")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarBloque(Bloque bloque) throws Exception {
		contenedor.validarInjectObjeto(bloque, true, true);
		contenedor.editarBloque(bloque);
	}

	@GET
	@Path("/obtenerBloques")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Bloque> obtenerBloques() throws Exception {
		ArrayList<Bloque> bloques = new ArrayList<Bloque>();
		bloques = contenedor.obtenerBloques();
		return bloques;
	}
	
	@GET
	@Path("/obtenerFormatosInput")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<FormatoInput> obtenerFormatosInput() throws Exception {
		ArrayList<FormatoInput> formatosInput = new ArrayList<FormatoInput>();
		formatosInput = contenedor.obtenerFormatosInput();
		return formatosInput;
	}
	
	@GET
	@Path("/obtenerTiposEjecucion")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TipoEjecucion> obtenerTiposEjecucion() throws Exception {
		ArrayList<TipoEjecucion> tiposEjecucion = new ArrayList<TipoEjecucion>();
		tiposEjecucion = contenedor.obtenerTiposEjecucion();
		return tiposEjecucion;
	}

	@GET
	@Path("/obtenerTiposBloque")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TipoBloque> obtenerTiposBloque() throws Exception {
		ArrayList<TipoBloque> tiposBloque = new ArrayList<TipoBloque>();
		tiposBloque = contenedor.obtenerTiposBloque();
		return tiposBloque;
	}

	@GET
	@Path("/obtenerTiposFlujo")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TipoFlujo> obtenerTiposFlujo() throws Exception {
		ArrayList<TipoFlujo> tiposFlujo = new ArrayList<TipoFlujo>();
		tiposFlujo = contenedor.obtenerTiposFlujo();
		return tiposFlujo;
	}

	@POST
	@Path("/crearTipoBloque")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearTipoBloque(TipoBloque tipobloque) throws Exception {
		contenedor.validarInjectObjeto(tipobloque, true, true);
		contenedor.crearTipoBloque(tipobloque);
	}

	@DELETE
	@Path("/borrarTipoBloque/{id_tipobloque}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarTipoBloque(@PathParam("id_tipobloque") Integer id_tipobloque) throws Exception {
		contenedor.borrarTipoBloque(id_tipobloque);
	}

	@GET
	@Path("/obtenerTiposCampo")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TipoCampo> obtenerTiposCampo() throws Exception {
		ArrayList<TipoCampo> tiposCampo = new ArrayList<TipoCampo>();
		tiposCampo = contenedor.obtenerTiposCampo();
		return tiposCampo;
	}

	// ----------------------------------------------------------------------
	// ----------------------------METODOLOGÍA-------------------------------
	// ----------------------------------------------------------------------

	@GET
	@Path("/obtenerTiposObtencionResultados")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TipoObtencionResultados> obtenerTiposObtencionResultados() throws Exception {
		ArrayList<TipoObtencionResultados> tiposObtencionResultados = new ArrayList<TipoObtencionResultados>();
		tiposObtencionResultados = contenedor.obtenerTiposObtencionResultados();
		return tiposObtencionResultados;
	}

	@GET
	@Path("/obtenerTiposPeriodicidad")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TipoPeriodicidad> obtenerTiposPeriodicidad() throws Exception {
		ArrayList<TipoPeriodicidad> tiposPeriodicidad = new ArrayList<TipoPeriodicidad>();
		tiposPeriodicidad = contenedor.obtenerTiposPeriodicidad();
		return tiposPeriodicidad;
	}

	@GET
	@Path("/obtenerMotoresCalculo")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<MotorCalculo> obtenerMotoresCalculo() throws Exception {
		ArrayList<MotorCalculo> motoresCalculo = new ArrayList<MotorCalculo>();
		motoresCalculo = contenedor.obtenerMotoresCalculo();
		return motoresCalculo;
	}

	@GET
	@Path("/obtenerTiposMetodologia")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TipoMetodologia> obtenerTiposMetodologia() throws Exception {
		ArrayList<TipoMetodologia> tiposMetodologia = new ArrayList<TipoMetodologia>();
		tiposMetodologia = contenedor.obtenerTiposMetodologia();
		return tiposMetodologia;
	}

	@POST
	@Path("/crearTipoMetodologia")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearTipoMetodologia(TipoMetodologia tipometodologia) throws Exception {
		contenedor.validarInjectObjeto(tipometodologia, true, true);
		contenedor.crearTipoMetodologia(tipometodologia);
	}

	@DELETE
	@Path("/borrarTipoMetodologia/{id_tipometodologia}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarTipoMetodologia(@PathParam("id_tipometodologia") Integer id_tipometodologia) throws Exception {
		contenedor.borrarTipoMetodologia(id_tipometodologia);
	}

	@PUT
	@Path("/editarTipoMetodologia")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarTipoMetodologia(TipoMetodologia tipometodologia) throws Exception {
		contenedor.validarInjectObjeto(tipometodologia, true, true);
		contenedor.editarTipoMetodologia(tipometodologia);
	}

	// ------------------------------

	@POST
	@Path("/crearTipoCartera")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearTipoCartera(TipoCartera tipocartera) throws Exception {
		contenedor.validarInjectObjeto(tipocartera, true, true);
		contenedor.crearTipoCartera(tipocartera);
	}

	@DELETE
	@Path("/borrarTipoCartera/{id_tipocartera}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarTipoCartera(@PathParam("id_tipocartera") Integer id_tipocartera) throws Exception {
		contenedor.borrarTipoCartera(id_tipocartera);
	}

	@PUT
	@Path("/editarTipoCartera")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarTipoCartera(TipoCartera tipocartera) throws Exception {
		contenedor.validarInjectObjeto(tipocartera, true, true);
		contenedor.editarTipoCartera(tipocartera);
	}

	// ------------------------------

	@POST
	@Path("/crearMetodologia")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearMetodologia(EstructuraMetodologica metodologia) throws Exception {
		contenedor.validarInjectObjeto(metodologia, true, true);
		contenedor.crearMetodologia(metodologia);
	}

	@GET
	@Path("/obtenerMetodologias")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<EstructuraMetodologica> obtenerMetodologias() throws Exception {
		ArrayList<EstructuraMetodologica> metodologias = new ArrayList<EstructuraMetodologica>();
		metodologias = contenedor.obtenerMetodologias();
		return metodologias;
	}

	@PUT
	@Path("/editarMetodologia")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void editarMetodologia(EstructuraMetodologica metodologia) throws Exception {
		contenedor.validarInjectObjeto(metodologia, true, true);
		contenedor.editarMetodologia(metodologia);
	}

	@GET
	@Path("/obtenerCarterasExtrapolacionporIdMetodologia/{id_metodologia}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Cartera> obtenerCarterasExtrapolacionporIdMetodologia(
			@PathParam("id_metodologia") Integer id_metodologia) throws Exception {
		ArrayList<Cartera> carterasExtrapolacion = new ArrayList<Cartera>();
		carterasExtrapolacion = contenedor.obtenerCarterasExtrapolacionporIdMetodologia(id_metodologia);
		return carterasExtrapolacion;
	}

	@DELETE
	@Path("/borrarMetodologia/{id_metodologia}")
	@JwtRequiereToken
	@JwtRefrescaToken
	public void borrarMetodologia(@PathParam("id_metodologia") Integer id_metodologia) throws Exception {
		contenedor.borrarMetodologia(id_metodologia);
	}

	@GET
	@Path("/obtenerMetodologia/{id_metodologia}")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public EstructuraMetodologica obtenerMetodologia(@PathParam("id_metodologia") Integer id_metodologia)
			throws Exception {
		EstructuraMetodologica metodologia = new EstructuraMetodologica();
		metodologia = contenedor.obtenerMetodologia(id_metodologia);
		return metodologia;
	}
	
	@POST
	@Path("/replicarEstructuraConCarterasVer")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Consumes(MediaType.APPLICATION_JSON)
	public void replicarEstructuraConCarterasVer(ReplicaEstructuraConCarVer replicaEstructuraConCarVer)
			throws Exception {
		//contenedor.validarInjectObjeto(replicaEstructuraConCarVer, true, true);
		contenedor.replicarEstructuraConCarterasVer(replicaEstructuraConCarVer);
	}
	
	// -------------------------------------------------------------------
	// -------------------------PRIMEROS PASOS----------------------------
	// -------------------------------------------------------------------
	@GET
	@Path("/obtenerPrimerosPasos")
	@JwtRequiereToken
	@JwtRefrescaToken
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<PrimerPaso> obtenerPrimerosPasos() throws Exception {
		ArrayList<PrimerPaso> primerosPasos = new ArrayList<PrimerPaso>();
		primerosPasos = contenedor.obtenerPrimerosPasos();
		return primerosPasos;
	}
}
