package sim.persistencia.dic;

public class TipoCampo {
	private Integer id_tipo_campo;
	private String nombre_tipo_campo;
	
	public TipoCampo() {
	}
	
	public TipoCampo(Integer id_tipo_campo, String nombre_tipo_campo) {
		this.id_tipo_campo = id_tipo_campo;
		this.nombre_tipo_campo = nombre_tipo_campo;
	}

	public Integer getId_tipo_campo() {
		return id_tipo_campo;
	}
	public void setId_tipo_campo(Integer id_tipo_campo) {
		this.id_tipo_campo = id_tipo_campo;
	}
	public String getNombre_tipo_campo() {
		return nombre_tipo_campo;
	}
	public void setNombre_tipo_campo(String nombre_tipo_campo) {
		this.nombre_tipo_campo = nombre_tipo_campo;
	}

}
