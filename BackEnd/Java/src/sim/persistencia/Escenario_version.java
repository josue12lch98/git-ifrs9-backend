package sim.persistencia;

public class Escenario_version {
	private Integer id_escenario_version;
	private JuegoEscenarios_version juego_escenarios_version;
	private Escenario escenario;	
	private Double peso;
	
	public Escenario_version() {		
	}
	
	public Integer getId_escenario_version() {
		return id_escenario_version;
	}
	public void setId_escenario_version(Integer id_escenario_version) {
		this.id_escenario_version = id_escenario_version;
	}
	
	public Escenario getEscenario() {
		return escenario;
	}
	public void setEscenario(Escenario escenario) {
		this.escenario = escenario;
	}	
	
	public Double getPeso() {
		return peso;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public JuegoEscenarios_version getJuego_escenarios_version() {
		return juego_escenarios_version;
	}

	public void setJuego_escenarios_version(JuegoEscenarios_version juego_escenarios_version) {
		this.juego_escenarios_version = juego_escenarios_version;
	}	
}
