package sim.persistencia;

public abstract class Registro {
	
	private Integer orden;
	
	public Registro(Integer orden) {
		this.orden = orden;
	}

	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	
	public abstract Double getNumero() throws Exception;

	public abstract String getTexto() throws Exception;
}
