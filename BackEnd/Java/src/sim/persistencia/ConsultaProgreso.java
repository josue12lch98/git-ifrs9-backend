package sim.persistencia;

public class ConsultaProgreso {

	//Olitas
	private Integer id_cartera;
	private String nombre_cartera;
	private Fecha fecha;
	
	public ConsultaProgreso() {
		
	}

	public Integer getId_cartera() {
		return id_cartera;
	}

	public void setId_cartera(Integer id_cartera) {
		this.id_cartera = id_cartera;
	}

	public String getNombre_cartera() {
		return nombre_cartera;
	}

	public void setNombre_cartera(String nombre_cartera) {
		this.nombre_cartera = nombre_cartera;
	}

	public Fecha getFecha() {
		return fecha;
	}

	public void setFecha(Fecha fecha) {
		this.fecha = fecha;
	}
	
}
