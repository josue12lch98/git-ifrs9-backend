package sim.persistencia;

public class AjusteGasto {
	private Integer id_ajusteGasto;
	private Fecha fecha;
	private String tipo_ajuste;
	private String detalle_ajuste;
	private Cartera cartera;
	private Double prov_NIF9;
	private Double prov_local;
	private String comentario;	
	
	public Fecha getFecha() {
		return fecha;
	}
	public void setFecha(Fecha fecha) {
		this.fecha = fecha;
	}
	public String getTipo_ajuste() {
		return tipo_ajuste;
	}
	public void setTipo_ajuste(String tipo_ajuste) {
		this.tipo_ajuste = tipo_ajuste;
	}
	public String getDetalle_ajuste() {
		return detalle_ajuste;
	}
	public void setDetalle_ajuste(String detalle_ajuste) {
		this.detalle_ajuste = detalle_ajuste;
	}
	public Cartera getCartera() {
		return cartera;
	}
	public void setCartera(Cartera cartera) {
		this.cartera = cartera;
	}
	public Double getProv_NIF9() {
		return prov_NIF9;
	}
	public void setProv_NIF9(Double prov_NIF9) {
		this.prov_NIF9 = prov_NIF9;
	}
	public Double getProv_local() {
		return prov_local;
	}
	public void setProv_local(Double prov_local) {
		this.prov_local = prov_local;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Integer getId_ajusteGasto() {
		return id_ajusteGasto;
	}
	public void setId_ajusteGasto(Integer id_ajusteGasto) {
		this.id_ajusteGasto = id_ajusteGasto;
	}

	
}