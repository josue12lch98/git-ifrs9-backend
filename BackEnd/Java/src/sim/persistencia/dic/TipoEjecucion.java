package sim.persistencia.dic;


public class TipoEjecucion {
	private Integer id_tipo_ejecucion;
	private String nombre_tipo_ejecucion;
	private TipoFlujo tipo_flujo;
	
	public TipoEjecucion () {	
	}

	public Integer getId_tipo_ejecucion() {
		return id_tipo_ejecucion;
	}

	public void setId_tipo_ejecucion(Integer id_tipo_ejecucion) {
		this.id_tipo_ejecucion = id_tipo_ejecucion;
	}

	public String getNombre_tipo_ejecucion() {
		return nombre_tipo_ejecucion;
	}

	public void setNombre_tipo_ejecucion(String nombre_tipo_ejecucion) {
		this.nombre_tipo_ejecucion = nombre_tipo_ejecucion;
	}

	public TipoFlujo getTipo_flujo() {
		return tipo_flujo;
	}

	public void setTipo_flujo(TipoFlujo tipo_flujo) {
		this.tipo_flujo = tipo_flujo;
	}
	
}
