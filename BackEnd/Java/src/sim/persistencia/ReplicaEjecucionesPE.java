package sim.persistencia;

import java.util.ArrayList;

public class ReplicaEjecucionesPE {
	private Integer codmes_replica;	
	private ArrayList<EjecucionPE> ejecuciones_replicar;
	
	public ReplicaEjecucionesPE() {		
	}
	
	public Integer getCodmes_replica() {
		return codmes_replica;
	}
	public void setCodmes_replica(Integer codmes_replica) {
		this.codmes_replica = codmes_replica;
	}
	public ArrayList<EjecucionPE> getEjecuciones_replicar() {
		return ejecuciones_replicar;
	}
	public void setEjecuciones_replicar(ArrayList<EjecucionPE> ejecuciones_replicar) {
		this.ejecuciones_replicar = ejecuciones_replicar;
	}
}
