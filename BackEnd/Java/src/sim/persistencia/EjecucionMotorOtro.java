package sim.persistencia;

import java.util.ArrayList;

public class EjecucionMotorOtro {
	private Integer id_ejecucion;
	private Integer codmes; 
	private Cartera_version cartera_version;
	private ArrayList<BloqueParametrizado> bloquesParam;	

	public EjecucionMotorOtro() {	
	}

	public Integer getId_ejecucion() {
		return id_ejecucion;
	}

	public void setId_ejecucion(Integer id_ejecucion) {
		this.id_ejecucion = id_ejecucion;
	}

	public Integer getCodmes() {
		return codmes;
	}

	public void setCodmes(Integer codmes) {
		this.codmes = codmes;
	}

	public Cartera_version getCartera_version() {
		return cartera_version;
	}

	public void setCartera_version(Cartera_version cartera_version) {
		this.cartera_version = cartera_version;
	}

	public ArrayList<BloqueParametrizado> getBloquesParam() {
		return bloquesParam;
	}

	public void setBloquesParam(ArrayList<BloqueParametrizado> bloquesParam) {
		this.bloquesParam = bloquesParam;
	}
	
}
