package sim.persistencia;

public class Cartera_version {
	private Integer id_cartera_version;
	private EstructuraMetodologica metodologia;
	private String nombre_cartera_version;
	private String descripcion;
	
	public Cartera_version() {
		this.metodologia = new EstructuraMetodologica();
	}
	
	public Integer getId_cartera_version() {
		return id_cartera_version;
	}
	public void setId_cartera_version(Integer id_cartera_version) {
		this.id_cartera_version = id_cartera_version;
	}
	public EstructuraMetodologica getMetodologia() {
		return metodologia;
	}
	public void setMetodologia(EstructuraMetodologica metodologia) {
		this.metodologia = metodologia;
	}	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre_cartera_version() {
		return nombre_cartera_version;
	}

	public void setNombre_cartera_version(String nombre_cartera_version) {
		this.nombre_cartera_version = nombre_cartera_version;
	}
	
}
