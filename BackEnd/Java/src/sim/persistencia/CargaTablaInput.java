package sim.persistencia;

public class CargaTablaInput {
	private Integer version_carga;
	private String nombre_carga;
	private Integer codmes_tabla;
	
	public String getNombre_carga() {
		return nombre_carga;
	}
	public void setNombre_carga(String nombre_carga) {
		this.nombre_carga = nombre_carga;
	}	
	
	public Integer getVersion_carga() {
		return version_carga;
	}
	public void setVersion_carga(Integer version_carga) {
		this.version_carga = version_carga;
	}
	public Integer getCodmes_tabla() {
		return codmes_tabla;
	}
	public void setCodmes_tabla(Integer codmes_tabla) {
		this.codmes_tabla = codmes_tabla;
	}
}
